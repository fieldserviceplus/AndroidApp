package com.fieldwise.utils;

import com.fieldwise.models.ModelChemical;
import com.fieldwise.models.ModelProduct;

import java.util.ArrayList;
import java.util.List;

public class Singleton {

    private static Singleton singleton;

    public static ArrayList<ModelProduct> listWorkOrderProduct = new ArrayList<ModelProduct>();
    public static ArrayList<ModelChemical> listWorkOrderChemical = new ArrayList<ModelChemical>();

    public static ArrayList<ModelProduct> listEstimateProduct = new ArrayList<ModelProduct>();

    public static ArrayList<ModelProduct> listInvoiceProduct = new ArrayList<ModelProduct>();

    public static List<String> listCalendarFilterType = new ArrayList<>();
    public static List<String> listCalendarFilterStatus = new ArrayList<>();
    public static List<String> listCalendarFilterPriority = new ArrayList<>();
    public static List<String> listCalendarAssignedToID = new ArrayList<>();

    public static Singleton getInstance() {
        if (singleton == null) {
            singleton = new Singleton();
        } else {
            return singleton;
        }
        return singleton;
    }

}