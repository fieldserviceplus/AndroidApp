package com.fieldwise.utils;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface APIInterface {

    //-----Auth API----------//
    //Login
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.LOGIN_EP)
    Call<ResponseBody> getLogin(@PartMap Map<String, RequestBody> map);

    //Sign Up
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.SIGNUP_EP)
    Call<ResponseBody> setSignUp(@PartMap Map<String, RequestBody> map);

    //Forgot Password
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.FORGOT_EP)
    Call<ResponseBody> getForgot(@PartMap Map<String, RequestBody> map);

    //-----Home API----------//
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.HOME_GET_SCHEDULE)
    Call<ResponseBody> getHomeSchedule(@HeaderMap Map<String, String> headers,
                                       @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.HOME_GET_TASK)
    Call<ResponseBody> getHomeTask(@HeaderMap Map<String, String> headers,
                                   @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.HOME_GET_RECENT)
    Call<ResponseBody> getHomeRecent(@HeaderMap Map<String, String> headers,
                                     @PartMap Map<String, RequestBody> map);

    //-----Account API----------//
    //Recent Account Screen
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_ACCOUNT_SPINNER)
    Call<ResponseBody> getAccountsSpinner(@HeaderMap Map<String, String> headers,
                                          @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_ACCOUNT)
    Call<ResponseBody> getRecentAccounts(@HeaderMap Map<String, String> headers,
                                         @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_ACCOUNT_BY_ID)
    Call<ResponseBody> getRecentAccountsById(@HeaderMap Map<String, String> headers,
                                             @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.FILTER_ACCOUNT_VIEW_FIELDS)
    Call<ResponseBody> getAccFilter(@HeaderMap Map<String, String> headers,
                                    @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.FILTER_ACCOUNT)
    Call<ResponseBody> addAccFilter(@HeaderMap Map<String, String> headers,
                                    @PartMap Map<String, RequestBody> map);

    //Detailed Account Screen
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_ACCOUNT_ALL_USERS)
    Call<ResponseBody> getAccAllUsers(@HeaderMap Map<String, String> headers,
                                      @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_ACCOUNT_CONTACTS)
    Call<ResponseBody> getAccContacts(@HeaderMap Map<String, String> headers,
                                      @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_ACCOUNT_ACCOUNT_TYPE)
    Call<ResponseBody> getAccAccountType(@HeaderMap Map<String, String> headers,
                                         @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_ACCOUNT_DETAIL_CREATE_EP)
    Call<ResponseBody> createAccountDetailed(@HeaderMap Map<String, String> headers,
                                             @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_ACCOUNT_DETAIL_EDIT_EP)
    Call<ResponseBody> editAccountDetailed(@HeaderMap Map<String, String> headers,
                                           @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_ACCOUNT_DETAIL_BY_ID_EP)
    Call<ResponseBody> getAccountDetailed(@HeaderMap Map<String, String> headers,
                                          @PartMap Map<String, RequestBody> map);

    //Related Account Screen
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_ACCOUNT_LIST)
    Call<ResponseBody> getRelatedAccountList(@HeaderMap Map<String, String> headers,
                                             @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_ACCOUNT_WORK_ORDER)
    Call<ResponseBody> getRelatedAccountWorkOrders(@HeaderMap Map<String, String> headers,
                                                   @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_ACCOUNT_CONTACT)
    Call<ResponseBody> getRelatedAccountContacts(@HeaderMap Map<String, String> headers,
                                                 @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_ACCOUNT_EVENT)
    Call<ResponseBody> getRelatedAccountEvents(@HeaderMap Map<String, String> headers,
                                               @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_ACCOUNT_ESTIMATE)
    Call<ResponseBody> getRelatedAccountEstimates(@HeaderMap Map<String, String> headers,
                                                  @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_ACCOUNT_INVOICE)
    Call<ResponseBody> getRelatedAccountInvoices(@HeaderMap Map<String, String> headers,
                                                 @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_ACCOUNT_FILE)
    Call<ResponseBody> getRelatedAccountFiles(@HeaderMap Map<String, String> headers,
                                              @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_ACCOUNT_LOCATION)
    Call<ResponseBody> getRelatedAccountLocation(@HeaderMap Map<String, String> headers,
                                                 @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_ACCOUNT_TASK)
    Call<ResponseBody> getRelatedAccountTasks(@HeaderMap Map<String, String> headers,
                                              @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_ACCOUNT_PRODUCT)
    Call<ResponseBody> getRelatedAccountProducts(@HeaderMap Map<String, String> headers,
                                                 @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_ACCOUNT_CHEMICAL)
    Call<ResponseBody> getRelatedAccountChemicals(@HeaderMap Map<String, String> headers,
                                                  @PartMap Map<String, RequestBody> map);

    //-----Contact API----------//
    //Recent Contact Screen
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_CONTACT_SPINNER)
    Call<ResponseBody> getContactsSpinner(@HeaderMap Map<String, String> headers,
                                          @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_CONTACT)
    Call<ResponseBody> getRecentContacts(@HeaderMap Map<String, String> headers,
                                         @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_CONTACT_BY_ID)
    Call<ResponseBody> getRecentContactsById(@HeaderMap Map<String, String> headers,
                                             @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.FILTER_CONTACT_VIEW_FIELDS)
    Call<ResponseBody> getContactFilter(@HeaderMap Map<String, String> headers,
                                        @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.FILTER_CONTACT)
    Call<ResponseBody> addContactFilter(@HeaderMap Map<String, String> headers,
                                        @PartMap Map<String, RequestBody> map);

    //Detailed Contact Screen
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.CONTACT_ASSIGNED_TO)
    Call<ResponseBody> getContactAssignedTo(@HeaderMap Map<String, String> headers,
                                            @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.CONTACT_ACCOUNT)
    Call<ResponseBody> getContactAccount(@HeaderMap Map<String, String> headers,
                                         @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.CONTACT_TITLE)
    Call<ResponseBody> getContactTitle(@HeaderMap Map<String, String> headers,
                                       @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.CONTACT_LEAD_SOURCE)
    Call<ResponseBody> getContactLeadSource(@HeaderMap Map<String, String> headers,
                                            @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.CONTACT_SALUTATION)
    Call<ResponseBody> getContactSalutation(@HeaderMap Map<String, String> headers,
                                            @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_CONTACT_DETAIL_CREATE_EP)
    Call<ResponseBody> createContactDetailed(@HeaderMap Map<String, String> headers,
                                             @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_CONTACT_DETAIL_EDIT_EP)
    Call<ResponseBody> editContactDetailed(@HeaderMap Map<String, String> headers,
                                           @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_CONTACT_DETAIL_BY_ID_EP)
    Call<ResponseBody> getContactDetailed(@HeaderMap Map<String, String> headers,
                                          @PartMap Map<String, RequestBody> map);

    //Related Contact Screen
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_CONTACT_LIST)
    Call<ResponseBody> getRelatedContactList(@HeaderMap Map<String, String> headers,
                                             @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_CONTACT_WORK_ORDER)
    Call<ResponseBody> getRelatedContactWorkOrders(@HeaderMap Map<String, String> headers,
                                                   @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_CONTACT_EVENT)
    Call<ResponseBody> getRelatedContactEvents(@HeaderMap Map<String, String> headers,
                                               @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_CONTACT_ESTIMATE)
    Call<ResponseBody> getRelatedContactEstimates(@HeaderMap Map<String, String> headers,
                                                  @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_CONTACT_INVOICE)
    Call<ResponseBody> getRelatedContactInvoices(@HeaderMap Map<String, String> headers,
                                                 @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_CONTACT_FILE)
    Call<ResponseBody> getRelatedContactFiles(@HeaderMap Map<String, String> headers,
                                              @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_CONTACT_NOTE)
    Call<ResponseBody> getRelatedContactNote(@HeaderMap Map<String, String> headers,
                                             @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_CONTACT_TASK)
    Call<ResponseBody> getRelatedContactTasks(@HeaderMap Map<String, String> headers,
                                              @PartMap Map<String, RequestBody> map);

    //-----Calendar API----------//
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.CALENDAR_ASSIGNED_TO)
    Call<ResponseBody> getCalendarAssignedTo(@HeaderMap Map<String, String> headers,
                                             @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.CALENDAR_FILTER)
    Call<ResponseBody> getCalendarFilters(@HeaderMap Map<String, String> headers,
                                          @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.CALENDAR_EVENTS)
    Call<ResponseBody> getCalendarEvents(@HeaderMap Map<String, String> headers,
                                         @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.CALENDAR_MAP)
    Call<ResponseBody> getMapEvents(@HeaderMap Map<String, String> headers,
                                    @PartMap Map<String, RequestBody> map);

    //-----Work Order API
    //Recent Work Order Screen
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_WORK_ORDER_SPINNER)
    Call<ResponseBody> getWorkOrdersSpinner(@HeaderMap Map<String, String> headers,
                                            @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_WORK_ORDER)
    Call<ResponseBody> getRecentWorkOrders(@HeaderMap Map<String, String> headers,
                                           @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_WORK_ORDER_BY_ID)
    Call<ResponseBody> getRecentWorkOrdersById(@HeaderMap Map<String, String> headers,
                                               @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.FILTER_WORK_ORDER_VIEW_FIELDS)
    Call<ResponseBody> getWOFilter(@HeaderMap Map<String, String> headers,
                                   @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.FILTER_WORK_ORDER)
    Call<ResponseBody> addWOFilter(@HeaderMap Map<String, String> headers,
                                   @PartMap Map<String, RequestBody> map);

    //Detailed Work Order Screen
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_WORK_ORDER_ALL_USERS)
    Call<ResponseBody> getWorkOrderAllUsers(@HeaderMap Map<String, String> headers,
                                            @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_WORK_ORDER_ACCOUNTS)
    Call<ResponseBody> getWorkOrderAccounts(@HeaderMap Map<String, String> headers,
                                            @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_WORK_ORDER_CONTACTS)
    Call<ResponseBody> getWorkOrderContacts(@HeaderMap Map<String, String> headers,
                                            @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_WORK_ORDER_PARENT_WORK_ORDERS)
    Call<ResponseBody> getWorkOrderParentWorkOrders(@HeaderMap Map<String, String> headers,
                                                    @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_WORK_ORDER_TYPES)
    Call<ResponseBody> getWorkOrderTypes(@HeaderMap Map<String, String> headers,
                                         @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_WORK_ORDER_STATUS)
    Call<ResponseBody> getWorkOrderStatus(@HeaderMap Map<String, String> headers,
                                          @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_WORK_ORDER_PRIORITIES)
    Call<ResponseBody> getWorkOrderPriorities(@HeaderMap Map<String, String> headers,
                                              @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_WORK_ORDER_CATEGORIES)
    Call<ResponseBody> getWorkOrderCategories(@HeaderMap Map<String, String> headers,
                                              @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_WORK_ORDER_DETAIL_CREATE_EP)
    Call<ResponseBody> createWorkOrderDetailed(@HeaderMap Map<String, String> headers,
                                               @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_WORK_ORDER_DETAIL_EDIT_EP)
    Call<ResponseBody> editWorkOrderDetailed(@HeaderMap Map<String, String> headers,
                                             @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_WORK_ORDER_DETAIL_BY_ID_EP)
    Call<ResponseBody> getWorkOrderDetailed(@HeaderMap Map<String, String> headers,
                                            @PartMap Map<String, RequestBody> map);

    //Related Work Order Screen
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_WORK_ORDER_LIST)
    Call<ResponseBody> getRelatedWorkOrderList(@HeaderMap Map<String, String> headers,
                                               @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_WORK_ORDER_LINE_ITEM)
    Call<ResponseBody> getRelatedWorkOrderLineItem(@HeaderMap Map<String, String> headers,
                                                   @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_WORK_ORDER_CHEMICAL)
    Call<ResponseBody> getRelatedWorkOrderChemical(@HeaderMap Map<String, String> headers,
                                                   @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_WORK_ORDER_EVENT)
    Call<ResponseBody> getRelatedWorkOrderEvent(@HeaderMap Map<String, String> headers,
                                                @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_WORK_ORDER_FILE)
    Call<ResponseBody> getRelatedWorkOrderFile(@HeaderMap Map<String, String> headers,
                                               @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_WORK_ORDER_TASK)
    Call<ResponseBody> getRelatedWorkOrderTask(@HeaderMap Map<String, String> headers,
                                               @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_WORK_ORDER_INVOICE)
    Call<ResponseBody> getRelatedWorkOrderInvoice(@HeaderMap Map<String, String> headers,
                                                  @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_WORK_ORDER_NOTE)
    Call<ResponseBody> getRelatedWorkOrderNote(@HeaderMap Map<String, String> headers,
                                               @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_WORK_ORDER_GET_PRODUCT)
    Call<ResponseBody> getRelatedWorkOrderGetProduct(@HeaderMap Map<String, String> headers,
                                                     @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.WORK_ORDER_PRODUCT_FAMILY)
    Call<ResponseBody> getWorkOrderProductFamily(@HeaderMap Map<String, String> headers,
                                                 @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.WORK_ORDER_ADD_LINEITEMS)
    Call<ResponseBody> addWorkOrderLineItem(@HeaderMap Map<String, String> headers,
                                            @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_WORK_ORDER_GET_CHEMICAL)
    Call<ResponseBody> getRelatedWorkOrderGetChemical(@HeaderMap Map<String, String> headers,
                                                      @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.WORK_ORDER_UNIT_OF_MEASUREMENT)
    Call<ResponseBody> getRelatedWorkOrderGetUnitOfMeasurement(@HeaderMap Map<String, String> headers,
                                                               @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.WORK_ORDER_ADD_CHEMICALS)
    Call<ResponseBody> addWorkOrderChemical(@HeaderMap Map<String, String> headers,
                                            @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.WORK_ORDER_ADD_SIGNATURE)
    Call<ResponseBody> addWorkOrderSignature(@HeaderMap Map<String, String> headers,
                                             @PartMap Map<String, RequestBody> map);

    //-----Estimate API----------//
    //Recent Estimate Screen
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_ESTIMATE_SPINNER)
    Call<ResponseBody> getEstimatesSpinner(@HeaderMap Map<String, String> headers,
                                           @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_ESTIMATE)
    Call<ResponseBody> getRecentEstimates(@HeaderMap Map<String, String> headers,
                                          @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_ESTIMATE_BY_ID)
    Call<ResponseBody> getRecentEstimateById(@HeaderMap Map<String, String> headers,
                                             @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.FILTER_ESTIMATE_VIEW_FIELDS)
    Call<ResponseBody> getEstimateFilter(@HeaderMap Map<String, String> headers,
                                         @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.FILTER_ESTIMATE)
    Call<ResponseBody> addEstimateFilter(@HeaderMap Map<String, String> headers,
                                         @PartMap Map<String, RequestBody> map);

    //Estimate API - Detailed Estimate
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_ESTIMATE_OWNERS)
    Call<ResponseBody> getEstimateOwners(@HeaderMap Map<String, String> headers,
                                         @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_ESTIMATE_ACCOUNTS)
    Call<ResponseBody> getEstimateAccounts(@HeaderMap Map<String, String> headers,
                                           @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_ESTIMATE_CONTACTS)
    Call<ResponseBody> getEstimateContacts(@HeaderMap Map<String, String> headers,
                                           @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_ESTIMATE_STATUS)
    Call<ResponseBody> getEstimateStatus(@HeaderMap Map<String, String> headers,
                                         @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_ESTIMATE_DETAIL_CREATE_EP)
    Call<ResponseBody> createEstimateDetailed(@HeaderMap Map<String, String> headers,
                                              @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_ESTIMATE_DETAIL_EDIT_EP)
    Call<ResponseBody> editEstimateDetailed(@HeaderMap Map<String, String> headers,
                                            @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_ESTIMATE_DETAIL_BY_ID_EP)
    Call<ResponseBody> getEstimateDetailed(@HeaderMap Map<String, String> headers,
                                           @PartMap Map<String, RequestBody> map);

    //Related Estimate Screen
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_ESTIMATE_LIST)
    Call<ResponseBody> getRelatedEstimateList(@HeaderMap Map<String, String> headers,
                                              @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_ESTIMATE_LINE_ITEM)
    Call<ResponseBody> getRelatedEstimateLineItem(@HeaderMap Map<String, String> headers,
                                                  @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_ESTIMATE_EVENT)
    Call<ResponseBody> getRelatedEstimateEvent(@HeaderMap Map<String, String> headers,
                                               @PartMap Map<String, RequestBody> map);


    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_ESTIMATE_TASK)
    Call<ResponseBody> getRelatedEstimateTask(@HeaderMap Map<String, String> headers,
                                              @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_ESTIMATE_FILE)
    Call<ResponseBody> getRelatedEstimateFile(@HeaderMap Map<String, String> headers,
                                              @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_ESTIMATE_NOTE)
    Call<ResponseBody> getRelatedEstimateNote(@HeaderMap Map<String, String> headers,
                                              @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_ESTIMATE_GET_PRODUCT)
    Call<ResponseBody> getRelatedEstimateGetProduct(@HeaderMap Map<String, String> headers,
                                                    @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.ESTIMATE_PRODUCT_FAMILY)
    Call<ResponseBody> getEstimateProductFamily(@HeaderMap Map<String, String> headers,
                                                @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.ESTIMATE_ADD_LINEITEMS)
    Call<ResponseBody> addEstimateLineItem(@HeaderMap Map<String, String> headers,
                                           @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.ESTIMATE_ADD_SIGNATURE)
    Call<ResponseBody> addEstimateSignature(@HeaderMap Map<String, String> headers,
                                            @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.ESTIMATE_CONVERT_TO_WORK_ORDER)
    Call<ResponseBody> addConvertToWorkOrder(@HeaderMap Map<String, String> headers,
                                             @PartMap Map<String, RequestBody> map);

    //-----Invoice API----------//
    //Recent Invoice Screen
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_INVOICE_SPINNER)
    Call<ResponseBody> getInvoicesSpinner(@HeaderMap Map<String, String> headers,
                                          @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_INVOICE)
    Call<ResponseBody> getRecentInvoices(@HeaderMap Map<String, String> headers,
                                         @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_INVOICE_BY_ID)
    Call<ResponseBody> getRecentInvoicesById(@HeaderMap Map<String, String> headers,
                                             @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.FILTER_INVOICE_VIEW_FIELDS)
    Call<ResponseBody> getInvoiceFilter(@HeaderMap Map<String, String> headers,
                                        @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.FILTER_INVOICE)
    Call<ResponseBody> addInvoiceFilter(@HeaderMap Map<String, String> headers,
                                        @PartMap Map<String, RequestBody> map);

    //Detailed Estimate Screen
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_INVOICE_OWNERS)
    Call<ResponseBody> getInvoiceOwners(@HeaderMap Map<String, String> headers,
                                        @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_INVOICE_ACCOUNTS)
    Call<ResponseBody> getInvoiceAccounts(@HeaderMap Map<String, String> headers,
                                          @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_INVOICE_PARENT_WORK_ORDERS)
    Call<ResponseBody> getInvoiceWorkOrders(@HeaderMap Map<String, String> headers,
                                            @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_INVOICE_CONTACTS)
    Call<ResponseBody> getInvoiceContacts(@HeaderMap Map<String, String> headers,
                                          @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_INVOICE_STATUS)
    Call<ResponseBody> getInvoiceStatus(@HeaderMap Map<String, String> headers,
                                        @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_INVOICE_PAYMENT_TERMS)
    Call<ResponseBody> getInvoicePaymentTerms(@HeaderMap Map<String, String> headers,
                                              @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_INVOICE_DETAIL_CREATE_EP)
    Call<ResponseBody> createInvoiceDetailed(@HeaderMap Map<String, String> headers,
                                             @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_INVOICE_DETAIL_EDIT_EP)
    Call<ResponseBody> editInvoiceDetailed(@HeaderMap Map<String, String> headers,
                                           @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_INVOICE_DETAIL_BY_ID_EP)
    Call<ResponseBody> getInvoiceDetailed(@HeaderMap Map<String, String> headers,
                                          @PartMap Map<String, RequestBody> map);

    //Related Invoice Screen
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_INVOICE_LIST)
    Call<ResponseBody> getRelatedInvoiceList(@HeaderMap Map<String, String> headers,
                                             @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_INVOICE_EVENT)
    Call<ResponseBody> getRelatedInvoiceEvent(@HeaderMap Map<String, String> headers,
                                              @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_INVOICE_FILE)
    Call<ResponseBody> getRelatedInvoiceFile(@HeaderMap Map<String, String> headers,
                                             @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_INVOICE_LINE_ITEM)
    Call<ResponseBody> getRelatedInvoiceLineItem(@HeaderMap Map<String, String> headers,
                                                 @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_INVOICE_NOTE)
    Call<ResponseBody> getRelatedInvoiceNote(@HeaderMap Map<String, String> headers,
                                             @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_INVOICE_TASK)
    Call<ResponseBody> getRelatedInvoiceTask(@HeaderMap Map<String, String> headers,
                                             @PartMap Map<String, RequestBody> map);


    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RELATED_INVOICE_GET_PRODUCT)
    Call<ResponseBody> getRelatedInvoiceGetProduct(@HeaderMap Map<String, String> headers,
                                                   @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.INVOICE_PRODUCT_FAMILY)
    Call<ResponseBody> getInvoiceProductFamily(@HeaderMap Map<String, String> headers,
                                               @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.INVOICE_ADD_LINEITEMS)
    Call<ResponseBody> addInvoiceLineItem(@HeaderMap Map<String, String> headers,
                                          @PartMap Map<String, RequestBody> map);

    //-----File API----------//
    //Recent File Screen
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_FILE_SPINNER)
    Call<ResponseBody> getFilesSpinner(@HeaderMap Map<String, String> headers,
                                       @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_FILE)
    Call<ResponseBody> getRecentFiles(@HeaderMap Map<String, String> headers,
                                      @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_FILE_BY_ID)
    Call<ResponseBody> getRecentFileById(@HeaderMap Map<String, String> headers,
                                         @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.FILTER_FILE_VIEW_FIELDS)
    Call<ResponseBody> getFileFilter(@HeaderMap Map<String, String> headers,
                                     @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.FILTER_FILE)
    Call<ResponseBody> addFileFilter(@HeaderMap Map<String, String> headers,
                                     @PartMap Map<String, RequestBody> map);

    //Detailed File Screen
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_FILE_ASSIGNED_TO)
    Call<ResponseBody> getFileAssignedTo(@HeaderMap Map<String, String> headers,
                                         @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_FILE_DETAIL_CREATE_EP)
    Call<ResponseBody> createFileDetailedWithFile(@HeaderMap Map<String, String> headers,
                                                  @PartMap Map<String, RequestBody> map,
                                                  @Part MultipartBody.Part file);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_FILE_DETAIL_EDIT_EP)
    Call<ResponseBody> editFileDetailed(@HeaderMap Map<String, String> headers,
                                        @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_FILE_DETAIL_EDIT_EP)
    Call<ResponseBody> editFileDetailedWithFile(@HeaderMap Map<String, String> headers,
                                                @PartMap Map<String, RequestBody> map,
                                                @Part MultipartBody.Part file);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_FILE_DETAIL_BY_ID_EP)
    Call<ResponseBody> getFileDetailed(@HeaderMap Map<String, String> headers,
                                       @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_FILE_RELATED_TO)
    Call<ResponseBody> getFileRelatedTo(@HeaderMap Map<String, String> headers,
                                        @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_FILE_WHAT)
    Call<ResponseBody> getFileWhat(@HeaderMap Map<String, String> headers,
                                   @PartMap Map<String, RequestBody> map);


    //-----Task API----------//
    //Recent Task Screen
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_TASK_SPINNER)
    Call<ResponseBody> getTasksSpinner(@HeaderMap Map<String, String> headers,
                                       @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_TASK)
    Call<ResponseBody> getRecentTasks(@HeaderMap Map<String, String> headers,
                                      @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RECENT_TASK_BY_ID)
    Call<ResponseBody> getRecentTaskById(@HeaderMap Map<String, String> headers,
                                         @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.FILTER_TASK_VIEW_FIELDS)
    Call<ResponseBody> getTaskFilter(@HeaderMap Map<String, String> headers,
                                     @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.FILTER_TASK)
    Call<ResponseBody> addTaskFilter(@HeaderMap Map<String, String> headers,
                                     @PartMap Map<String, RequestBody> map);

    //Detailed Task Screen
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_TASK_ASSIGNED_TO)
    Call<ResponseBody> getTaskAssignedTo(@HeaderMap Map<String, String> headers,
                                         @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_TASK_WHO)
    Call<ResponseBody> getTaskWho(@HeaderMap Map<String, String> headers,
                                  @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_TASK_WHAT)
    Call<ResponseBody> getTaskWhat(@HeaderMap Map<String, String> headers,
                                   @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_TASK_RELATED_TO)
    Call<ResponseBody> getTaskRelatedTo(@HeaderMap Map<String, String> headers,
                                        @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_TASK_TYPE)
    Call<ResponseBody> getTaskType(@HeaderMap Map<String, String> headers,
                                   @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_TASK_STATUS)
    Call<ResponseBody> getTaskStatus(@HeaderMap Map<String, String> headers,
                                     @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_TASK_PRIORITY)
    Call<ResponseBody> getTaskPriority(@HeaderMap Map<String, String> headers,
                                       @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_TASK_DETAIL_CREATE_EP)
    Call<ResponseBody> createTaskDetailed(@HeaderMap Map<String, String> headers,
                                          @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_TASK_DETAIL_EDIT_EP)
    Call<ResponseBody> editTaskDetailed(@HeaderMap Map<String, String> headers,
                                        @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_TASK_DETAIL_BY_ID_EP)
    Call<ResponseBody> getTaskDetailed(@HeaderMap Map<String, String> headers,
                                       @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_TASK_MARK_COMPLETED)
    Call<ResponseBody> setMarkCompleted(@HeaderMap Map<String, String> headers,
                                        @PartMap Map<String, RequestBody> map);

    //-----Common API----------//
    //Email API
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILED_EMAIL_TEMPLATE)
    Call<ResponseBody> getEmailTemplate(@HeaderMap Map<String, String> headers,
                                        @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.SEND_EMAIL)
    Call<ResponseBody> sendEmail(@HeaderMap Map<String, String> headers,
                                 @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.SEND_EMAIL)
    Call<ResponseBody> sendEmailWithFile(@HeaderMap Map<String, String> headers,
                                         @PartMap Map<String, RequestBody> map,
                                         @Part MultipartBody.Part file);

    //Create View API
    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.CREATE_VIEW)
    Call<ResponseBody> addCreateView(@HeaderMap Map<String, String> headers,
                                     @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.COPY_VIEW)
    Call<ResponseBody> addCopyView(@HeaderMap Map<String, String> headers,
                                   @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DELETE_VIEW)
    Call<ResponseBody> addDeleteView(@HeaderMap Map<String, String> headers,
                                     @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.RENAME_VIEW)
    Call<ResponseBody> addRenameView(@HeaderMap Map<String, String> headers,
                                     @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.SHARING_VIEW)
    Call<ResponseBody> addSharingView(@HeaderMap Map<String, String> headers,
                                      @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.FILTER_VIEW)
    Call<ResponseBody> addFilterView(@HeaderMap Map<String, String> headers,
                                     @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DISPLAY_COLUMN_VIEW)
    Call<ResponseBody> addDisplayColumnView(@HeaderMap Map<String, String> headers,
                                            @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.GET_VIEW_DETAILS)
    Call<ResponseBody> getViewDetails(@HeaderMap Map<String, String> headers,
                                      @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.SHORT_CUSTOM_VIEW)
    Call<ResponseBody> shortCustomView(@HeaderMap Map<String, String> headers,
                                       @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DELETE_OBJECT)
    Call<ResponseBody> deleteObject(@HeaderMap Map<String, String> headers,
                                    @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.CREATE_NOTE)
    Call<ResponseBody> addNewNote(@HeaderMap Map<String, String> headers,
                                  @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.WORK_ORDER_CONVERT_TO_INVOICE)
    Call<ResponseBody> addConvertToInvoice(@HeaderMap Map<String, String> headers,
                                           @PartMap Map<String, RequestBody> map);


    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILS_USER)
    Call<ResponseBody> getUserDetails(@HeaderMap Map<String, String> headers,
                                      @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILS_PRODUCT)
    Call<ResponseBody> getProductDetails(@HeaderMap Map<String, String> headers,
                                         @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILS_EVENT)
    Call<ResponseBody> getEventDetails(@HeaderMap Map<String, String> headers,
                                       @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.DETAILS_NOTE)
    Call<ResponseBody> getNoteDetails(@HeaderMap Map<String, String> headers,
                                      @PartMap Map<String, RequestBody> map);


    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.EVENT_STATUS)
    Call<ResponseBody> getEventStatus(@HeaderMap Map<String, String> headers,
                                      @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.EVENT_PRIORITY)
    Call<ResponseBody> getEventPriority(@HeaderMap Map<String, String> headers,
                                        @PartMap Map<String, RequestBody> map);


    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.EVENT_TYPE)
    Call<ResponseBody> getEventType(@HeaderMap Map<String, String> headers,
                                    @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.CREATE_EVENT)
    Call<ResponseBody> createEvent(@HeaderMap Map<String, String> headers,
                                   @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.GET_TEMPLATE)
    Call<ResponseBody> getTemplateList(@HeaderMap Map<String, String> headers,
                                   @PartMap Map<String, RequestBody> map);

    @Multipart
    @Headers({"key:" + Cons.BASE_KEY})
    @POST(Cons.CREATE_DOCUMENT)
    Call<ResponseBody> createDocument(@HeaderMap Map<String, String> headers,
                                   @PartMap Map<String, RequestBody> map);

}