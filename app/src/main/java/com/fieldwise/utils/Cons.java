package com.fieldwise.utils;

public class Cons {

    //public static final String BASE_URL = "http://bcat.org.uk/fieldserviceplus/API/";
    public static final String BASE_URL = "http://beforesubmit.com/fieldserviceplus/API/";
    public static final String BASE_KEY = "03d612662bf82f471741773cf9bd5a79";

    //Auth
    public static final String LOGIN_EP = "User/Auth/Login/";
    public static final String SIGNUP_EP = "User/Auth/Signup/";
    public static final String FORGOT_EP = "User/Auth/ForgotPassword/";

    //Home
    public static final String HOME_GET_SCHEDULE = "Home/Data/GetSchedule/";
    public static final String HOME_GET_TASK = "Home/Data/GetTasks/";
    public static final String HOME_GET_RECENT = "Home/Data/GetRecents/";

    //Account
    public static final String RECENT_ACCOUNT_SPINNER = "Account/View/GetViews/";
    public static final String RECENT_ACCOUNT = "Account/AcActions/RecentAccounts/";
    public static final String RECENT_ACCOUNT_BY_ID = "Account/View/ViewAccountList/";

    public static final String FILTER_ACCOUNT_VIEW_FIELDS = "Account/View/GetAccountViewFields/";
    public static final String FILTER_ACCOUNT = "Account/View/AccountFilter/";

    public static final String DETAILED_ACCOUNT_ALL_USERS = "User/Data/GetAllUsers/";
    public static final String DETAILED_ACCOUNT_CONTACTS = "Contact/Data/GetContacts/";
    public static final String DETAILED_ACCOUNT_ACCOUNT_TYPE = "Account/Data/GetAccountType/";
    public static final String DETAILED_ACCOUNT_DETAIL_CREATE_EP = "Account/AcActions/CreateAccount/";
    public static final String DETAILED_ACCOUNT_DETAIL_EDIT_EP = "Account/AcActions/EditAccount/";
    public static final String DETAILED_ACCOUNT_DETAIL_BY_ID_EP = "Account/AcActions/AccountDetails/";

    public static final String RELATED_ACCOUNT_LIST = "Account/AcActions/AccountRelatedList/";
    public static final String RELATED_ACCOUNT_WORK_ORDER = "Account/AcActions/AccountRelatedWorkOrder/";
    public static final String RELATED_ACCOUNT_CONTACT = "Account/AcActions/AccountRelatedContact/";
    public static final String RELATED_ACCOUNT_EVENT = "Account/AcActions/AccountRelatedEvent/";
    public static final String RELATED_ACCOUNT_ESTIMATE = "Account/AcActions/AccountRelatedEstimate/";
    public static final String RELATED_ACCOUNT_INVOICE = "Account/AcActions/AccountRelatedInvoice/";
    public static final String RELATED_ACCOUNT_FILE = "Account/AcActions/AccountRelatedFile/";
    public static final String RELATED_ACCOUNT_LOCATION = "Account/AcActions/AccountRelatedLocation/";
    public static final String RELATED_ACCOUNT_TASK = "Account/AcActions/AccountRelatedTask/";
    public static final String RELATED_ACCOUNT_PRODUCT = "Account/AcActions/AccountRelatedProduct/";
    public static final String RELATED_ACCOUNT_CHEMICAL = "Account/AcActions/AccountRelatedChemical/";

    //Contact
    public static final String RECENT_CONTACT_SPINNER = "Contact/View/GetViews/";
    public static final String RECENT_CONTACT = "Contact/CTActions/RecentContacts/";
    public static final String RECENT_CONTACT_BY_ID = "Contact/View/ViewContactList/";

    public static final String FILTER_CONTACT_VIEW_FIELDS = "Contact/View/GetContactViewFields/";
    public static final String FILTER_CONTACT = "Contact/View/ContactFilter/";

    public static final String CONTACT_ASSIGNED_TO = "User/Data/GetAllUsers/";
    public static final String CONTACT_ACCOUNT = "Account/Data/GetAccounts/";
    public static final String CONTACT_TITLE = "Contact/Data/GetTitlesOfContact/";
    public static final String CONTACT_LEAD_SOURCE = "Contact/Data/GetLeadSources/";
    public static final String CONTACT_SALUTATION = "Contact/Data/GetSalutations/";
    public static final String DETAILED_CONTACT_DETAIL_CREATE_EP = "Contact/CTActions/CreateContact/";
    public static final String DETAILED_CONTACT_DETAIL_EDIT_EP = "Contact/CTActions/EditContact/";
    public static final String DETAILED_CONTACT_DETAIL_BY_ID_EP = "Contact/CTActions/ContactDetails/";

    public static final String RELATED_CONTACT_LIST = "Contact/CTActions/ContactRelatedList/";
    public static final String RELATED_CONTACT_WORK_ORDER = "Contact/CTActions/ContactRelatedWorkOrder/";
    public static final String RELATED_CONTACT_EVENT = "Contact/CTActions/ContactRelatedEvent/";
    public static final String RELATED_CONTACT_ESTIMATE = "Contact/CTActions/ContactRelatedEstimate/";
    public static final String RELATED_CONTACT_INVOICE = "Contact/CTActions/ContactRelatedInvoice/";
    public static final String RELATED_CONTACT_FILE = "Contact/CTActions/ContactRelatedFile/";
    public static final String RELATED_CONTACT_NOTE = "Contact/CTActions/ContactRelatedNote/";
    public static final String RELATED_CONTACT_TASK = "Contact/CTActions/ContactRelatedTask/";

    //Calendar
    public static final String CALENDAR_ASSIGNED_TO = "User/Data/GetAllUsers/";
    public static final String CALENDAR_FILTER = "Calendar/CalendarView/CalendarFilterData/";
    public static final String CALENDAR_EVENTS = "Calendar/CalendarView/GetEventTaskByDates/";
    public static final String CALENDAR_MAP = "Calendar/CalendarView/GetWorkOrderByDate/";

    //Work Order
    public static final String RECENT_WORK_ORDER_SPINNER = "WorkOrder/View/GetViews/";
    public static final String RECENT_WORK_ORDER = "WorkOrder/WoActions/RecentWorkOrders/";
    public static final String RECENT_WORK_ORDER_BY_ID = "WorkOrder/View/ViewWorkOrderList/";
    public static final String FILTER_WORK_ORDER_VIEW_FIELDS = "WorkOrder/View/GetWorkOrderViewFields/";
    public static final String FILTER_WORK_ORDER = "WorkOrder/View/WorkOrderFilter/";

    public static final String DETAILED_WORK_ORDER_ALL_USERS = "User/Data/GetAllUsers/";
    public static final String DETAILED_WORK_ORDER_ACCOUNTS = "Account/Data/GetAccounts/";
    public static final String DETAILED_WORK_ORDER_CONTACTS = "Contact/Data/GetContacts/";
    public static final String DETAILED_WORK_ORDER_PARENT_WORK_ORDERS = "WorkOrder/Data/GetParentWorkOrders/";
    public static final String DETAILED_WORK_ORDER_TYPES = "WorkOrder/Data/GetWorkOrderTypes/";
    public static final String DETAILED_WORK_ORDER_STATUS = "WorkOrder/Data/GetWorkOrderStatus/";
    public static final String DETAILED_WORK_ORDER_PRIORITIES = "WorkOrder/Data/GetWorkOrderPriorities/";
    public static final String DETAILED_WORK_ORDER_CATEGORIES = "WorkOrder/Data/GetWorkOrderCategories/";
    public static final String DETAILED_WORK_ORDER_DETAIL_CREATE_EP = "WorkOrder/WoActions/CreateWorkOrder/";
    public static final String DETAILED_WORK_ORDER_DETAIL_EDIT_EP = "WorkOrder/WoActions/EditWorkOrder/";
    public static final String DETAILED_WORK_ORDER_DETAIL_BY_ID_EP = "WorkOrder/WoActions/WorkOrderDetails/";

    public static final String RELATED_WORK_ORDER_LIST = "WorkOrder/WoActions/WorkOrderRelatedList/";
    public static final String RELATED_WORK_ORDER_LINE_ITEM = "WorkOrder/WoActions/WorkOrderRelatedLineItem/";
    public static final String RELATED_WORK_ORDER_CHEMICAL = "WorkOrder/WoActions/WorkOrderRelatedChemical/";
    public static final String RELATED_WORK_ORDER_EVENT = "WorkOrder/WoActions/WorkOrderRelatedEvent/";
    public static final String RELATED_WORK_ORDER_FILE = "WorkOrder/WoActions/WorkOrderRelatedFile/";
    public static final String RELATED_WORK_ORDER_TASK = "WorkOrder/WoActions/WorkOrderRelatedTask/";
    public static final String RELATED_WORK_ORDER_INVOICE = "WorkOrder/WoActions/WorkOrderRelatedInvoice/";
    public static final String RELATED_WORK_ORDER_NOTE = "WorkOrder/WoActions/WorkOrderRelatedNote/";

    public static final String RELATED_WORK_ORDER_GET_PRODUCT = "WorkOrder/WoActions/GetProducts/";
    public static final String WORK_ORDER_PRODUCT_FAMILY = "WorkOrder/WoActions/GetProductFamily/";
    public static final String WORK_ORDER_ADD_LINEITEMS = "WorkOrder/WoActions/SaveWorkorderLineItems/";

    public static final String RELATED_WORK_ORDER_GET_CHEMICAL = "WorkOrder/WoActions/GetChemicals/";
    public static final String WORK_ORDER_UNIT_OF_MEASUREMENT = "WorkOrder/WoActions/GetUnitOfMeasurement/";
    public static final String WORK_ORDER_ADD_CHEMICALS = "WorkOrder/WoActions/SaveWorkorderChemicals/";

    public static final String WORK_ORDER_ADD_SIGNATURE = "WorkOrder/WoActions/SaveWorkOrderSignature/";

    public static final String WORK_ORDER_CONVERT_TO_INVOICE = "WorkOrder/WoActions/ConvertToInvoice/";

    //Estimate
    public static final String RECENT_ESTIMATE_SPINNER = "Estimate/View/GetViews/";
    public static final String RECENT_ESTIMATE = "Estimate/ETActions/RecentEstimates/";
    public static final String RECENT_ESTIMATE_BY_ID = "Estimate/View/ViewEstimateList/";
    public static final String FILTER_ESTIMATE_VIEW_FIELDS = "Estimate/View/GetEstimateViewFields/";
    public static final String FILTER_ESTIMATE = "Estimate/View/EstimateFilter/";

    public static final String DETAILED_ESTIMATE_OWNERS = "User/Data/GetAllUsers/";
    public static final String DETAILED_ESTIMATE_ACCOUNTS = "Account/Data/GetAccounts/";
    public static final String DETAILED_ESTIMATE_CONTACTS = "Contact/Data/GetContacts/";
    public static final String DETAILED_ESTIMATE_STATUS = "Estimate/Data/GetEstimateStatus/";
    public static final String DETAILED_ESTIMATE_DETAIL_CREATE_EP = "Estimate/ETActions/CreateEstimate/";
    public static final String DETAILED_ESTIMATE_DETAIL_EDIT_EP = "Estimate/ETActions/EditEstimate/";
    public static final String DETAILED_ESTIMATE_DETAIL_BY_ID_EP = "Estimate/ETActions/EstimateDetails/";

    public static final String RELATED_ESTIMATE_LIST = "Estimate/ETActions/EstimateRelatedList/";
    public static final String RELATED_ESTIMATE_LINE_ITEM = "Estimate/ETActions/EstimateRelatedLineItem/";
    public static final String RELATED_ESTIMATE_EVENT = "Estimate/ETActions/EstimateRelatedEvent/";
    public static final String RELATED_ESTIMATE_TASK = "Estimate/ETActions/EstimateRelatedTask/";
    public static final String RELATED_ESTIMATE_FILE = "Estimate/ETActions/EstimateRelatedFile/";
    public static final String RELATED_ESTIMATE_NOTE = "Estimate/ETActions/EstimateRelatedNote/";

    public static final String RELATED_ESTIMATE_GET_PRODUCT = "Estimate/ETActions/GetProducts/";
    public static final String ESTIMATE_PRODUCT_FAMILY = "WorkOrder/WoActions/GetProductFamily/";
    public static final String ESTIMATE_ADD_LINEITEMS = "Estimate/ETActions/SaveEstimateLineItems/";

    public static final String ESTIMATE_ADD_SIGNATURE = "Estimate/ETActions/SaveEstimateSignature/";

    public static final String ESTIMATE_CONVERT_TO_WORK_ORDER = "Estimate/ETActions/ConvertToWorkOrder/";

    //Invoice
    public static final String RECENT_INVOICE_SPINNER = "Invoice/View/GetViews/";
    public static final String RECENT_INVOICE = "Invoice/INActions/RecentInvoices";
    public static final String RECENT_INVOICE_BY_ID = "Invoice/View/ViewInvoiceList/";
    public static final String FILTER_INVOICE_VIEW_FIELDS = "Invoice/View/GetInvoiceViewFields/";
    public static final String FILTER_INVOICE = "Invoice/View/InvoiceFilter/";

    public static final String DETAILED_INVOICE_OWNERS = "User/Data/GetAllUsers";
    public static final String DETAILED_INVOICE_ACCOUNTS = "Account/Data/GetAccounts/";
    public static final String DETAILED_INVOICE_CONTACTS = "Contact/Data/GetContacts/";
    public static final String DETAILED_INVOICE_PARENT_WORK_ORDERS = "WorkOrder/Data/GetParentWorkOrders/";
    public static final String DETAILED_INVOICE_STATUS = "Invoice/Data/GetInvoiceStatus/";
    public static final String DETAILED_INVOICE_PAYMENT_TERMS = "Invoice/Data/GetInvoicePaymentTerms/";
    public static final String DETAILED_INVOICE_DETAIL_CREATE_EP = "Invoice/INActions/CreateInvoice/";
    public static final String DETAILED_INVOICE_DETAIL_EDIT_EP = "Invoice/INActions/EditInvoice/";
    public static final String DETAILED_INVOICE_DETAIL_BY_ID_EP = "Invoice/INActions/InvoiceDetails/";

    public static final String RELATED_INVOICE_LIST = "Invoice/INActions/InvoiceRelatedList/";
    public static final String RELATED_INVOICE_EVENT = "Invoice/INActions/InvoiceRelatedEvent/";
    public static final String RELATED_INVOICE_FILE = "Invoice/INActions/InvoiceRelatedFile/";
    public static final String RELATED_INVOICE_LINE_ITEM = "Invoice/INActions/InvoiceRelatedLineItem/";
    public static final String RELATED_INVOICE_NOTE = "Invoice/INActions/InvoiceRelatedNote/";
    public static final String RELATED_INVOICE_TASK = "Invoice/INActions/InvoiceRelatedTask/";

    public static final String RELATED_INVOICE_GET_PRODUCT = "Invoice/INActions/GetProducts/";
    public static final String INVOICE_PRODUCT_FAMILY = "WorkOrder/WoActions/GetProductFamily/";
    public static final String INVOICE_ADD_LINEITEMS = "Invoice/INActions/SaveInvoiceLineItems/";

    //File
    public static final String RECENT_FILE_SPINNER = "File/View/GetViews/";
    public static final String RECENT_FILE = "File/FLActions/RecentFiles/";
    public static final String RECENT_FILE_BY_ID = "File/View/ViewFileList/";
    public static final String FILTER_FILE_VIEW_FIELDS = "File/View/GetFileViewFields/";
    public static final String FILTER_FILE = "File/View/FileFilter/";

    public static final String DETAILED_FILE_ASSIGNED_TO = "User/Data/GetAllUsers/";
    public static final String DETAILED_FILE_DETAIL_CREATE_EP = "File/FLActions/CreateFile/";
    public static final String DETAILED_FILE_DETAIL_EDIT_EP = "File/FLActions/EditFile/";
    public static final String DETAILED_FILE_DETAIL_BY_ID_EP = "File/FLActions/FileDetails/";

    public static final String DETAILED_FILE_RELATED_TO = "File/Data/GetRelatedObjList/";
    public static final String DETAILED_FILE_WHAT = "File/Data/GetRelatedToList/";

    //Task
    public static final String RECENT_TASK_SPINNER = "Task/View/GetViews/";
    public static final String RECENT_TASK = "Task/TKActions/RecentTasks/";
    public static final String RECENT_TASK_BY_ID = "Task/View/ViewTaskList/";
    public static final String FILTER_TASK_VIEW_FIELDS = "Task/View/GetTaskViewFields/";
    public static final String FILTER_TASK = "Task/View/TaskFilter/";

    public static final String DETAILED_TASK_ASSIGNED_TO = "User/Data/GetAllUsers/";
    public static final String DETAILED_TASK_WHO = "Contact/Data/GetContacts/";
    public static final String DETAILED_TASK_WHAT = "Task/Data/GetRelatedToList/";
    public static final String DETAILED_TASK_RELATED_TO = "Task/Data/GetRelatedObjList/";
    public static final String DETAILED_TASK_TYPE = "Task/Data/GetTaskType/";
    public static final String DETAILED_TASK_STATUS = "Task/Data/GetTaskStatus/";
    public static final String DETAILED_TASK_PRIORITY = "Task/Data/GetTaskPriority/";
    public static final String DETAILED_TASK_DETAIL_CREATE_EP = "Task/TKActions/CreateTask/";
    public static final String DETAILED_TASK_DETAIL_EDIT_EP = "Task/TKActions/EditTask/";
    public static final String DETAILED_TASK_DETAIL_BY_ID_EP = "Task/TKActions/TaskDetails/";

    public static final String DETAILED_TASK_MARK_COMPLETED = "Task/TKActions/MarkATask/";

    //Common
    public static final String DETAILED_EMAIL_TEMPLATE = "Common/Data/GetEmailTemplates/";
    public static final String SEND_EMAIL = "Common/Actions/SendEmail/";

    public static final String CREATE_VIEW = "Common/View/CreateNewView/";
    public static final String COPY_VIEW = "Common/View/CopyCustomView/";
    public static final String DELETE_VIEW = "Common/View/DeleteCustomView/";
    public static final String RENAME_VIEW = "Common/View/RenameCustomView/";
    public static final String SHARING_VIEW = "Common/View/EditSharingCustomView/";
    /*?*/public static final String FILTER_VIEW = "Common/View/EditFiltersCustomView/";
    /*?*/public static final String DISPLAY_COLUMN_VIEW = "Common/View/EditDisplayedColumnsCustomView/";
    public static final String GET_VIEW_DETAILS = "Common/View/GetCustomViewDetails/";
    public static final String SHORT_CUSTOM_VIEW = "Common/View/SortCustomView";

    public static final String DELETE_OBJECT = "Common/Actions/DeleteObject/";
    public static final String CREATE_NOTE = "Common/Actions/CreateNote/";
    /*?*/public static final String DETAILS_USER = "Common/Actions/UserDetails/";
    public static final String DETAILS_PRODUCT = "Common/Actions/ProductDetails/";
    public static final String DETAILS_EVENT = "Common/Actions/EventDetails/";
    public static final String DETAILS_NOTE = "Common/Actions/NoteDetails/";

    /*?*/public static final String CREATE_NEW_FILE = "Common/Actions/CreateNewFile/";
    public static final String EVENT_STATUS = "Common/Data/GetEventStatus";
    public static final String EVENT_PRIORITY = "Common/Data/GetEventPriorities";
    public static final String EVENT_TYPE = "Common/Data/GetEventTypes";
    public static final String CREATE_EVENT = "Common/Actions/CreateEvent";

    public static final String GET_TEMPLATE = "Common/Data/GetGenDocTemplates";
    public static final String CREATE_DOCUMENT = "Common/Actions/GenerateDocument";

    public static final String IS_LoggedIn = "IsLoggedIn";

    public static final String KEY_TOKEN = "token";
    public static final String KEY_ORGANIZATION_ID = "OrganizationID";
    public static final String KEY_DeviceType = "DeviceType";
    public static final String KEY_DeviceUDID = "DeviceUDID";

    public static final String KEY_AboutMe = "AboutMe";
    public static final String KEY_AccessNotes = "AccessNotes";
    public static final String KEY_AccessType = "AccessType";
    public static final String KEY_Account = "Account";
    public static final String KEY_AccountID = "AccountID";
    public static final String KEY_AccountName = "AccountName";
    public static final String KEY_AccountNo = "AccountNo";
    public static final String KEY_AccountType = "AccountType";
    public static final String KEY_AccountTypeID = "AccountTypeID";
    public static final String KEY_AccountTypeName = "AccountTypeName";
    public static final String KEY_AccountViewID = "AccountViewID";
    public static final String KEY_AccountViewName = "AccountViewName";
    public static final String KEY_AdditionalInformation = "AdditionalInformation";
    public static final String KEY_AdditionalNotes_ARR = "AdditionalNotes[]";
    public static final String KEY_Address = "Address";
    public static final String KEY_Alias = "Alias";
    public static final String KEY_ApplicationAmount = "ApplicationAmount";
    public static final String KEY_ApplicationAmount_ARR = "ApplicationAmount[]";
    public static final String KEY_ApplicationArea_ARR = "ApplicationArea[]";
    public static final String KEY_ApplicationUnitOfMeasure = "ApplicationUnitOfMeasure";
    public static final String KEY_ApplicationUnitOfMeasure_ARR = "ApplicationUnitOfMeasure[]";
    public static final String KEY_AssignedTo = "AssignedTo";
    public static final String KEY_AssignedToName = "AssignedToName";
    public static final String KEY_BCCEmailID = "BCCEmailID";
    public static final String KEY_BillingAddress = "BillingAddress";
    public static final String KEY_BillingCity = "BillingCity";
    public static final String KEY_BillingCountry = "BillingCountry";
    public static final String KEY_BillingLatitude = "BillingLatitude";
    public static final String KEY_BillingLongitude = "BillingLongitude";
    public static final String KEY_BillingName = "BillingName";
    public static final String KEY_BillingPostalCode = "BillingPostalCode";
    public static final String KEY_BillingState = "BillingState";
    public static final String KEY_BillingStreet = "BillingStreet";
    public static final String KEY_BirthDate = "BirthDate";
    public static final String KEY_CCEmailID = "CCEmailID";
    public static final String KEY_CallDisposition = "CallDisposition";
    public static final String KEY_CategoryName = "CategoryName";
    public static final String KEY_ChemicalLineItemNo = "ChemicalLineItemNo";
    public static final String KEY_ChemicalName = "ChemicalName";
    public static final String KEY_ChemicalNo = "ChemicalNo";
    public static final String KEY_City = "City";
    public static final String KEY_CityName = "CityName";
    public static final String KEY_ColorCode = "ColorCode";
    public static final String KEY_CompanyName = "CompanyName";
    public static final String KEY_Contact = "Contact";
    public static final String KEY_ContactID = "ContactID";
    public static final String KEY_ContactName = "ContactName";
    public static final String KEY_ContactNo = "ContactNo";
    public static final String KEY_ContactViewID = "ContactViewID";
    public static final String KEY_ContactViewName = "ContactViewName";
    public static final String KEY_ContentType = "ContentType";
    public static final String KEY_Country = "Country";
    public static final String KEY_CountryName = "CountryName";
    public static final String KEY_CreatedBy = "CreatedBy";
    public static final String KEY_CreatedDate = "CreatedDate";
    public static final String KEY_Date = "Date";
    public static final String KEY_DatePurchased = "DatePurchased";
    public static final String KEY_DefaultGrpNotificationFreq = "DefaultGrpNotificationFreq";
    public static final String KEY_DefaultQuantity = "DefaultQuantity";
    public static final String KEY_DepartmentName = "DepartmentName";
    public static final String KEY_Description = "Description";
    public static final String KEY_Discount = "Discount";
    public static final String KEY_Discount_ARR = "Discount[]";
    public static final String KEY_DivisionName = "DivisionName";
    public static final String KEY_DoNotCall = "DoNotCall";
    public static final String KEY_DueDate = "DueDate";
    public static final String KEY_ETNo = "ET#";
    public static final String KEY_Email = "Email";
    public static final String KEY_EmailAttachment = "EmailAttachment[]";
    public static final String KEY_EmailBody = "EmailBody";
    public static final String KEY_EmailOptOut = "EmailOptOut";
    public static final String KEY_EmailSubject = "EmailSubject";
    public static final String KEY_EmailTemplate = "EmailTemplate";
    public static final String KEY_EmailTemplateID = "EmailTemplateID";
    public static final String KEY_End = "End";
    public static final String KEY_EndDate = "EndDate";
    public static final String KEY_EndTime = "EndTime";
    public static final String KEY_Ends = "Ends";
    public static final String KEY_EndsAfterOccurrences = "EndsAfterOccurrences";
    public static final String KEY_EndsOnDate = "EndsOnDate";
    public static final String KEY_EstimateID = "EstimateID";
    public static final String KEY_EstimateLineNo = "EstimateLineNo";
    public static final String KEY_EstimateName = "EstimateName";
    public static final String KEY_EstimateNo = "EstimateNo";
    public static final String KEY_EstimateStatus = "EstimateStatus";
    public static final String KEY_EstimateStatusID = "EstimateStatusID";
    public static final String KEY_EstimateToAddress = "EstimateToAddress";
    public static final String KEY_EstimateToName = "EstimateToName";
    public static final String KEY_EstimateViewID = "EstimateViewID";
    public static final String KEY_EstimateViewName = "EstimateViewName";
    public static final String KEY_EventEndDate = "EventEndDate";
    public static final String KEY_EventEndTime = "EventEndTime";
    public static final String KEY_EventID = "EventID";
    public static final String KEY_EventStartDate = "EventStartDate";
    public static final String KEY_EventStartTime = "EventStartTime";
    public static final String KEY_EventStatus = "EventStatus";
    public static final String KEY_EventTypeName = "EventTypeName";
    public static final String KEY_ExpirationDate = "ExpirationDate";
    public static final String KEY_Fax = "Fax";
    public static final String KEY_FieldName = "FieldName";
    public static final String KEY_FileID = "FileID";
    public static final String KEY_FileName = "FileName";
    public static final String KEY_FileViewID = "FileViewID";
    public static final String KEY_FileViewName = "FileViewName";
    public static final String KEY_Filter = "Filter";
    public static final String KEY_FilterConditions = "FilterConditions[]";
    public static final String KEY_FilterFields = "FilterFields[]";
    public static final String KEY_FilterValues = "FilterValues[]";
    public static final String KEY_SpecifyFieldsDisplay = "SpecifyFieldsDisplay[]";
    public static final String KEY_FirstName = "FirstName";
    public static final String KEY_FromDate = "FromDate";
    public static final String KEY_FromEmailID = "FromEmailID";
    public static final String KEY_FromName = "FromName";
    public static final String KEY_FullName = "FullName";
    public static final String KEY_GrandTotal = "GrandTotal";
    public static final String KEY_ID = "ID";
    public static final String KEY_INNo = "IN#";
    public static final String KEY_Industry = "Industry";
    public static final String KEY_IntervalEvery = "IntervalEvery";
    public static final String KEY_InvoiceDate = "InvoiceDate";
    public static final String KEY_InvoiceID = "InvoiceID";
    public static final String KEY_InvoiceLineNo = "InvoiceLineNo";
    public static final String KEY_InvoiceNo = "InvoiceNo";
    public static final String KEY_InvoiceNumber = "InvoiceNumber";
    public static final String KEY_InvoicePaymentTermID = "InvoicePaymentTermID";
    public static final String KEY_InvoiceStatus = "InvoiceStatus";
    public static final String KEY_InvoiceStatusID = "InvoiceStatusID";
    public static final String KEY_InvoiceViewID = "InvoiceViewID";
    public static final String KEY_InvoiceViewName = "InvoiceViewName";
    public static final String KEY_IsActive = "IsActive";
    public static final String KEY_IsDeleted = "IsDeleted";
    public static final String KEY_IsListPriceEditable = "IsListPriceEditable";
    public static final String KEY_IsQuantityEditable = "IsQuantityEditable";
    public static final String KEY_IsRecurring = "IsRecurring";
    public static final String KEY_LastActivityDate = "LastActivityDate";
    public static final String KEY_LastModifiedBy = "LastModifiedBy";
    public static final String KEY_LastModifiedDate = "LastModifiedDate";
    public static final String KEY_LastName = "LastName";
    public static final String KEY_LastServiceDate = "LastServiceDate";
    public static final String KEY_Latitude = "Latitude";
    public static final String KEY_LeadSource = "LeadSource";
    public static final String KEY_LeadSourceID = "LeadSourceID";
    public static final String KEY_LeadSourceName = "LeadSourceName";
    public static final String KEY_LineItemCount = "LineItemCount";
    public static final String KEY_LineItemNo = "LineItemNo";
    public static final String KEY_ListPrice = "ListPrice";
    public static final String KEY_ListPrice_ARR = "ListPrice[]";
    public static final String KEY_LocationID = "LocationID";
    public static final String KEY_LocationStreet = "LocationStreet";
    public static final String KEY_Longitude = "Longitude";
    public static final String KEY_MailingAddress = "MailingAddress";
    public static final String KEY_MailingCity = "MailingCity";
    public static final String KEY_MailingCountry = "MailingCountry";
    public static final String KEY_MailingLatitude = "MailingLatitude";
    public static final String KEY_MailingLongitude = "MailingLongitude";
    public static final String KEY_MailingPostalCode = "MailingPostalCode";
    public static final String KEY_MailingState = "MailingState";
    public static final String KEY_Manager = "Manager";
    public static final String KEY_Measure = "Measure";
    public static final String KEY_MobileNo = "MobileNo";
    public static final String KEY_Name = "Name";
    public static final String KEY_NetTotal = "NetTotal";
    public static final String KEY_NetTotal_ARR = "NetTotal[]";
    public static final String KEY_NoteID = "NoteID";
    public static final String KEY_Notes = "Notes";
    public static final String KEY_Owner = "Owner";
    public static final String KEY_OwnerName = "OwnerName";
    public static final String KEY_ParentWorkOrder = "ParentWorkOrder";
    public static final String KEY_ParentWorkOrderName = "ParentWorkOrderName";
    public static final String KEY_Password = "Password";
    public static final String KEY_PaymentTerms = "PaymentTerms";
    public static final String KEY_Phone = "Phone";
    public static final String KEY_PhoneNo = "PhoneNo";
    public static final String KEY_PopUpReminder = "PopUpReminder";
    public static final String KEY_PopupReminder = "PopUpReminder";
    public static final String KEY_PostalCode = "PostalCode";
    public static final String KEY_PreferredTechnician = "PreferredTechnician";
    public static final String KEY_PreferredTechnicianName = "PreferredTechnicianName";
    public static final String KEY_PrimaryContact = "PrimaryContact";
    public static final String KEY_PrimaryContactName = "PrimaryContactName";
    public static final String KEY_Priority = "Priority";
    public static final String KEY_ProductCode = "ProductCode";
    public static final String KEY_ProductFamily = "ProductFamily";
    public static final String KEY_ProductFamilyID = "ProductFamilyID";
    public static final String KEY_ProductID = "ProductID";
    public static final String KEY_ProductName = "ProductName";
    public static final String KEY_Product_ARR = "Product[]";
    public static final String KEY_ProfilePic = "vProfilePic";
    public static final String KEY_Quantity = "Quantity";
    public static final String KEY_Quantity_ARR = "Quantity[]";
    public static final String KEY_QuoteName = "QuoteName";
    public static final String KEY_QuoteNumber = "QuoteNumber";
    public static final String KEY_ReceiveAdminEmails = "ReceiveAdminEmails";
    public static final String KEY_RelatedObjID = "RelatedObjID";
    public static final String KEY_RelatedObjNo = "RelatedObjNo";
    public static final String KEY_RelatedTo = "RelatedTo";
    public static final String KEY_RelatedToName = "RelatedToName";
    public static final String KEY_RepeatEvery = "RepeatEvery";
    public static final String KEY_RepeatOn = "RepeatOn";
    public static final String KEY_Salutation = "Salutation";
    public static final String KEY_SalutationID = "SalutationID";
    public static final String KEY_ShapeType = "ShapeType";
    public static final String KEY_ShippingAddress = "ShippingAddress";
    public static final String KEY_ShippingCity = "ShippingCity";
    public static final String KEY_ShippingCountry = "ShippingCountry";
    public static final String KEY_ShippingHandling = "ShippingHandling";
    public static final String KEY_ShippingLatitude = "ShippingLatitude";
    public static final String KEY_ShippingLongitude = "ShippingLongitude";
    public static final String KEY_ShippingName = "ShippingName";
    public static final String KEY_ShippingPostalCode = "ShippingPostalCode";
    public static final String KEY_ShippingState = "ShippingState";
    public static final String KEY_ShowTasks = "ShowTasks";
    public static final String KEY_Signature = "Signature";
    public static final String KEY_Site = "Site";
    public static final String KEY_SortByField = "SortByField";
    public static final String KEY_SortByValue = "SortByValue";
    public static final String KEY_SpecialNotes = "SpecialNotes";
    public static final String KEY_Start = "Start";
    public static final String KEY_StartDate = "StartDate";
    public static final String KEY_StartOn = "StartOn";
    public static final String KEY_StartTime = "StartTime";
    public static final String KEY_State = "State";
    public static final String KEY_StateName = "StateName";
    public static final String KEY_Status = "Status";
    public static final String KEY_Street = "Street";
    public static final String KEY_SubTotal = "SubTotal";
    public static final String KEY_SubTotal_ARR = "SubTotal[]";
    public static final String KEY_Subject = "Subject";
    public static final String KEY_TaskID = "TaskID";
    public static final String KEY_TaskPriority = "TaskPriority";
    public static final String KEY_TaskPriorityID = "TaskPriorityID";
    public static final String KEY_TaskStatus = "TaskStatus";
    public static final String KEY_TaskStatusID = "TaskStatusID";
    public static final String KEY_TaskType = "TaskType";
    public static final String KEY_TaskTypeID = "TaskTypeID";
    public static final String KEY_TaskViewID = "TaskViewID";
    public static final String KEY_TaskViewName = "TaskViewName";
    public static final String KEY_Tax = "Tax";
    public static final String KEY_Taxable = "Taxable";
    public static final String KEY_Taxable_ARR = "Taxable[]";
    public static final String KEY_TestConcentration = "TestConcentration";
    public static final String KEY_TestConcentration_ARR = "TestConcentration[]";
    public static final String KEY_TestedUnitOfMeasure = "TestedUnitOfMeasure";
    public static final String KEY_TestedUnitOfMeasure_ARR = "TestedUnitOfMeasure[]";
    public static final String KEY_Time = "Time";
    public static final String KEY_Title = "Title";
    public static final String KEY_TitleOfPeopleID = "TitleOfPeopleID";
    public static final String KEY_ToDate = "ToDate";
    public static final String KEY_ToEmailID = "ToEmailID";
    public static final String KEY_TotalPrice = "TotalPrice";
    public static final String KEY_Type = "Type";
    public static final String KEY_UnitOfMeasure = "UnitOfMeasure";
    public static final String KEY_UnitPrice = "UnitPrice";
    public static final String KEY_UnitPrice_ARR = "UnitPrice[]";
    public static final String KEY_UserID = "UserID";
    public static final String KEY_WOCategory = "WOCategory";
    public static final String KEY_WOEndTime = "WOEndTime";
    public static final String KEY_WONo = "WO#";
    public static final String KEY_WOPriority = "WOPriority";
    public static final String KEY_WOStartTime = "WOStartTime";
    public static final String KEY_WOStatus = "WOStatus";
    public static final String KEY_WO_AssignedTo_ARR = "AssignetTo[]";
    public static final String KEY_WO_Priority_ARR = "WOPriority[]";
    public static final String KEY_WO_Status_ARR = "WOStatus[]";
    public static final String KEY_WO_Type_ARR = "WorkOrderType[]";
    public static final String KEY_What = "What";
    public static final String KEY_WhatName = "WhatName";
    public static final String KEY_Who = "Who";
    public static final String KEY_WorkOrder = "WorkOrder";
    public static final String KEY_WorkOrderCategory = "WOCategory";
    public static final String KEY_WorkOrderCategoryID = "WOCategoryID";
    public static final String KEY_WorkOrderID = "WorkOrderID";
    public static final String KEY_WorkOrderName = "WorkOrderName";
    public static final String KEY_WorkOrderNo = "WorkOrderNo";
    public static final String KEY_WorkOrderPriority = "WOPriority";
    public static final String KEY_WorkOrderPriorityID = "WOPriorityID";
    public static final String KEY_WorkOrderStatus = "WOStatus";
    public static final String KEY_WorkOrderStatusID = "WOStatusID";
    public static final String KEY_WorkOrderSubject = "WorkOrderSubject";
    public static final String KEY_WorkOrderType = "WorkOrderType";
    public static final String KEY_WorkOrderTypeID = "WorkOrderTypeID";
    public static final String KEY_WorkOrderTypeName = "WorkOrderTypeName";
    public static final String KEY_WorkOrderViewID = "WorkOrderViewID";
    public static final String KEY_WorkOrderViewName = "WorkOrderViewName";
    public static final String KEY_Object = "Object";
    public static final String KEY_ViewName = "ViewName";
    public static final String KEY_RestrictVisibility = "RestrictVisibility";
    public static final String KEY_ViewID = "ViewID";
    public static final String KEY_Body = "Body";
    public static final String KEY_CreatedByName = "CreatedByName";
    public static final String KEY_LastModifiedByName = "LastModifiedByName";
    public static final String KEY_IsAllDayEvent = "IsAllDayEvent";
    public static final String KEY_EventPriority = "EventPriority";
    public static final String KEY_ProductCost = "ProductCost";
    public static final String KEY_ProductFamilyName = "ProductFamilyName";
    public static final String KEY_EventType = "EventType";
    public static final String KEY_EventTypeID = "EventTypeID";
    public static final String KEY_EventPriorityID = "EventPriorityID";

    public static final String KEY_GenDocTemplateID = "GenDocTemplateID";
    public static final String KEY_TemplateName = "TemplateName";
    public static final String KEY_OutputFormat = "OutputFormat";
    public static final String KEY_SaveToObject = "SaveToObject";
    public static final String KEY_ObjectID = "ObjectID";
    public static final String KEY_GenerateDocumentURL = "GenerateDocumentURL";
}