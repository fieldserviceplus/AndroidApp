package com.fieldwise.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

public class Utl {

    public static void showToast(Context ctx, String message) {
        Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
    }

    public static void clearSP(Context ctx) {
        SharedPreferences sp = ctx.getSharedPreferences("MY_Pref", Context.MODE_PRIVATE);
        sp.edit().clear().commit();
    }

    public static void setSPStr(Context ctx, String key, String value) {
        SharedPreferences sp = ctx.getSharedPreferences("MY_Pref", Context.MODE_PRIVATE);
        sp.edit().putString(key, value).commit();
    }

    public static String getSPStr(Context ctx, String key) {
        SharedPreferences sp = ctx.getSharedPreferences("MY_Pref", Context.MODE_PRIVATE);
        String value = sp.getString(key, "");
        return value;
    }

    public static void setSPBol(Context ctx, String key, boolean value) {
        SharedPreferences sp = ctx.getSharedPreferences("MY_Pref", Context.MODE_PRIVATE);
        sp.edit().putBoolean(key, value).commit();
    }

    public static boolean getSPBol(Context ctx, String key) {
        SharedPreferences sp = ctx.getSharedPreferences("MY_Pref", Context.MODE_PRIVATE);
        boolean value = sp.getBoolean(key, false);
        return value;
    }

    public static void setSPInt(Context ctx, String key, int value) {
        SharedPreferences sp = ctx.getSharedPreferences("MY_Pref", Context.MODE_PRIVATE);
        sp.edit().putInt(key, value).commit();
    }

    public static int getSPInt(Context ctx, String key) {
        SharedPreferences sp = ctx.getSharedPreferences("MY_Pref", Context.MODE_PRIVATE);
        int value = sp.getInt(key, 0);
        return value;
    }

}