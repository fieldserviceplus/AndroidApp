package com.fieldwise.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.fieldwise.R
import com.fieldwise.adapters.AdapterRecentContacts
import com.fieldwise.models.ModelDynamic
import com.fieldwise.utils.APIClient
import com.fieldwise.utils.APIInterface
import com.fieldwise.utils.Cons
import com.fieldwise.utils.Utl
import com.mindorks.placeholderview.PlaceHolderView

import org.json.JSONException
import org.json.JSONObject

import java.io.IOException
import java.util.ArrayList
import java.util.HashMap
import java.util.LinkedHashMap

import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentContactViewAllKT : Fragment(), AdapterRecentContacts.ClickListen {

    var apiInterface = APIClient.getClient().create(APIInterface::class.java)

    private var viewAllType: String? = null
    private var contactId: String? = null

    private var list: MutableList<ModelDynamic>? = null
    private var phv: PlaceHolderView? = null

    var clickWorkOrderClick: ClickListenContactRelatedViewAllWorkOrderOpen? = null

    interface ClickListenContactRelatedViewAllWorkOrderOpen {
        fun callbackContactRelatedViewAllWorkOrderOpen(model: ModelDynamic)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_view_all, container, false)

        apiInterface = APIClient.getClient().create(APIInterface::class.java)

        clickWorkOrderClick = activity as ClickListenContactRelatedViewAllWorkOrderOpen?

        viewAllType = arguments!!.getString("view_all_type")
        val bundle = this.arguments
        if (bundle != null) {
            contactId = bundle.getString(Cons.KEY_ContactID, "")
        }

        phv = view.findViewById<View>(R.id.phv) as PlaceHolderView

        list = ArrayList()

        if (viewAllType == getString(R.string.work_order)) {
            getRelatedContactWorkOrders(contactId)
        } else if (viewAllType == getString(R.string.event)) {
            getRelatedContactEvents(contactId)
        } else if (viewAllType == getString(R.string.estimate)) {
            getRelatedContactEstimates(contactId)
        } else if (viewAllType == getString(R.string.invoice)) {
            getRelatedContactInvoices(contactId)
        } else if (viewAllType == getString(R.string.file)) {
            getRelatedContactFiles(contactId)
        } else if (viewAllType == getString(R.string.note)) {
            getRelatedContactNotes(contactId)
        } else if (viewAllType == getString(R.string.task)) {
            getRelatedContactTasks(contactId)
        }

        return view

    }

    private fun getRelatedContactWorkOrders(contactId: String?) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactID] = RequestBody.create(MediaType.parse("text/plain"), contactId!!)
        val response = apiInterface.getRelatedContactWorkOrders(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_RecAcc" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            list!!.clear()
                            phv!!.removeAllViews()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val iterator = dataObject.keys()
                                val model = ModelDynamic()
                                val map = LinkedHashMap<String, String>()
                                while (iterator.hasNext()) {
                                    val key = iterator.next()
                                    val value = dataObject.optString(key)
                                    map[key] = value
                                    model.map = map
                                }
                                list!!.add(model)
                            }
                            for (i in list!!.indices) {
                                phv!!.addView(AdapterRecentContacts(activity, this@FragmentContactViewAllKT, list!![i]))
                            }
                        }
                    } catch (e: JSONException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getRelatedContactEvents(contactId: String?) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactID] = RequestBody.create(MediaType.parse("text/plain"), contactId!!)
        val response = apiInterface.getRelatedContactEvents(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_RecAcc" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            list!!.clear()
                            phv!!.removeAllViews()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val iterator = dataObject.keys()
                                val model = ModelDynamic()
                                val map = LinkedHashMap<String, String>()
                                while (iterator.hasNext()) {
                                    val key = iterator.next()
                                    val value = dataObject.optString(key)
                                    map[key] = value
                                    model.map = map
                                }
                                list!!.add(model)
                            }
                            for (i in list!!.indices) {
                                phv!!.addView(AdapterRecentContacts(activity, list!![i]))
                            }
                        }
                    } catch (e: JSONException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getRelatedContactEstimates(contactId: String?) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactID] = RequestBody.create(MediaType.parse("text/plain"), contactId!!)
        val response = apiInterface.getRelatedContactEstimates(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_RecAcc" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            list!!.clear()
                            phv!!.removeAllViews()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val iterator = dataObject.keys()
                                val model = ModelDynamic()
                                val map = LinkedHashMap<String, String>()
                                while (iterator.hasNext()) {
                                    val key = iterator.next()
                                    val value = dataObject.optString(key)
                                    map[key] = value
                                    model.map = map
                                }
                                list!!.add(model)
                            }
                            for (i in list!!.indices) {
                                phv!!.addView(AdapterRecentContacts(activity, list!![i]))
                            }
                        }
                    } catch (e: JSONException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getRelatedContactInvoices(contactId: String?) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactID] = RequestBody.create(MediaType.parse("text/plain"), contactId!!)
        val response = apiInterface.getRelatedContactInvoices(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_RecAcc" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            list!!.clear()
                            phv!!.removeAllViews()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val iterator = dataObject.keys()
                                val model = ModelDynamic()
                                val map = LinkedHashMap<String, String>()
                                while (iterator.hasNext()) {
                                    val key = iterator.next()
                                    val value = dataObject.optString(key)
                                    map[key] = value
                                    model.map = map
                                }
                                list!!.add(model)
                            }
                            for (i in list!!.indices) {
                                phv!!.addView(AdapterRecentContacts(activity, list!![i]))
                            }
                        }
                    } catch (e: JSONException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getRelatedContactFiles(contactId: String?) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactID] = RequestBody.create(MediaType.parse("text/plain"), contactId!!)
        val response = apiInterface.getRelatedContactFiles(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_RecAcc" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            list!!.clear()
                            phv!!.removeAllViews()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val iterator = dataObject.keys()
                                val model = ModelDynamic()
                                val map = LinkedHashMap<String, String>()
                                while (iterator.hasNext()) {
                                    val key = iterator.next()
                                    val value = dataObject.optString(key)
                                    map[key] = value
                                    model.map = map
                                }
                                list!!.add(model)
                            }
                            for (i in list!!.indices) {
                                phv!!.addView(AdapterRecentContacts(activity, list!![i]))
                            }
                        }
                    } catch (e: JSONException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getRelatedContactNotes(contactId: String?) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactID] = RequestBody.create(MediaType.parse("text/plain"), contactId!!)
        val response = apiInterface.getRelatedContactNote(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_RecAcc" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            list!!.clear()
                            phv!!.removeAllViews()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val iterator = dataObject.keys()
                                val model = ModelDynamic()
                                val map = LinkedHashMap<String, String>()
                                while (iterator.hasNext()) {
                                    val key = iterator.next()
                                    val value = dataObject.optString(key)
                                    map[key] = value
                                    model.map = map
                                }
                                list!!.add(model)
                            }
                            for (i in list!!.indices) {
                                phv!!.addView(AdapterRecentContacts(activity, list!![i]))
                            }
                        }
                    } catch (e: JSONException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getRelatedContactTasks(contactId: String?) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactID] = RequestBody.create(MediaType.parse("text/plain"), contactId!!)
        val response = apiInterface.getRelatedContactTasks(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_RecAcc" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            list!!.clear()
                            phv!!.removeAllViews()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val iterator = dataObject.keys()
                                val model = ModelDynamic()
                                val map = LinkedHashMap<String, String>()
                                while (iterator.hasNext()) {
                                    val key = iterator.next()
                                    val value = dataObject.optString(key)
                                    map[key] = value
                                    model.map = map
                                }
                                list!!.add(model)
                            }
                            for (i in list!!.indices) {
                                phv!!.addView(AdapterRecentContacts(activity, list!![i]))
                            }
                        }
                    } catch (e: JSONException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    override fun callbackContact(model: ModelDynamic) {
        if (clickWorkOrderClick != null) {
            clickWorkOrderClick!!.callbackContactRelatedViewAllWorkOrderOpen(model)
        }
    }

}