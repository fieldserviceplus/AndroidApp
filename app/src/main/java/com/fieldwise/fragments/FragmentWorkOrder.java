package com.fieldwise.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.adapters.AdapterRecentWorkOrders;
import com.fieldwise.add.AdapterShortView;
import com.fieldwise.models.ModelDynamic;
import com.fieldwise.models.ModelSpinner;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;
import com.mindorks.placeholderview.PlaceHolderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentWorkOrder extends Fragment implements
        AdapterRecentWorkOrders.ClickListen,
        AdapterShortView.ClickListenShortView {

    public APIInterface apiInterface;

    private String strWorkOrderView,
            strViewID, strViewName, strSpecifyFieldsDisplay, strRestrictVisibility;

    private List<ModelSpinner> listWorkOrderSpinner;
    private ArrayAdapter<ModelSpinner> adapterWorkOrderSpinner;
    private Spinner spinnerWorkOrderSpinner;

    private List<ModelDynamic> listRecentWorkOrdersDetailed;
    private LinearLayout layoutRecentWorkOrders;
    private PlaceHolderView phvRecentWorkOrders;
    private TextView textViewWorkOrderHeader;

    private LinearLayout layoutRenameView;
    private Button buttonRenameViewCancel, buttonRenameViewSave;
    private EditText editTextRenameViewName;

    private LinearLayout layoutSharingView;
    private Button buttonSharingViewCancel, buttonSharingViewSave;
    private RadioGroup radioGroupSharingRestrictVisibility;
    private RadioButton radioButtonSharingMe, radioButtonSharingEveryone;

    private LinearLayout layoutShortView;
    private Button buttonShortViewCancel, buttonShortViewSave;
    private PlaceHolderView phvShortView;
    private List<String> listShortFieldName = new ArrayList<>();
    private List<Integer> listShortPosition = new ArrayList<>();

    private ImageButton buttonFilter, buttonSort, buttonAdd, buttonMore;

    private boolean isFilter = false;

    public ClickListenWorkOrder click;

    public interface ClickListenWorkOrder {
        void callbackWorkOrder(ModelDynamic model);
    }

    public ClickListenWorkOrderFilter clickFilter;

    public interface ClickListenWorkOrderFilter {
        void callbackWorkOrderFilter(String workOrderView);
    }

    public ClickListenCreateView clickCreateView;

    public interface ClickListenCreateView {
        void callbackWorkOrderCreateView(String objectName);
    }

    public ClickListenCopyView clickCopyView;

    public interface ClickListenCopyView {
        void callbackWorkOrderCopyView();
    }

    public ClickListenDeleteView clickDeleteView;

    public interface ClickListenDeleteView {
        void callbackWorkOrderDeleteView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_work_order, container, false);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        click = (ClickListenWorkOrder) getActivity();
        clickFilter = (ClickListenWorkOrderFilter) getActivity();
        clickCreateView = (ClickListenCreateView) getActivity();
        clickCopyView = (ClickListenCopyView) getActivity();
        clickDeleteView = (ClickListenDeleteView) getActivity();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            isFilter = bundle.getBoolean("isWorkOrderFilter", false);
        }

        spinnerWorkOrderSpinner = (Spinner) view.findViewById(R.id.spinner_work_order_spinner);
        layoutRecentWorkOrders = (LinearLayout) view.findViewById(R.id.layout_recent_work_orders);
        phvRecentWorkOrders = (PlaceHolderView) view.findViewById(R.id.phv_recent_work_orders);
        textViewWorkOrderHeader = (TextView) view.findViewById(R.id.textview_work_order_title);

        buttonFilter = (ImageButton) view.findViewById(R.id.button_filter);
        buttonSort = (ImageButton) view.findViewById(R.id.button_sort);
        buttonAdd = (ImageButton) view.findViewById(R.id.button_add);
        buttonMore = (ImageButton) view.findViewById(R.id.button_more);

        layoutRenameView = (LinearLayout) view.findViewById(R.id.layout_rename_view);
        buttonRenameViewCancel = (Button) view.findViewById(R.id.button_rename_view_cancel);
        buttonRenameViewSave = (Button) view.findViewById(R.id.button_rename_view_save);
        editTextRenameViewName = (EditText) view.findViewById(R.id.edittext_rename_view_name);

        layoutSharingView = (LinearLayout) view.findViewById(R.id.layout_sharing_view);
        buttonSharingViewCancel = (Button) view.findViewById(R.id.button_sharing_view_cancel);
        buttonSharingViewSave = (Button) view.findViewById(R.id.button_sharing_view_save);
        radioGroupSharingRestrictVisibility = (RadioGroup) view.findViewById(R.id.radio_group_sharing_view_restrict_visibility);
        radioButtonSharingMe = (RadioButton) view.findViewById(R.id.radio_button_sharing_view_me);
        radioButtonSharingEveryone = (RadioButton) view.findViewById(R.id.radio_button_sharing_view_everyone);

        layoutShortView = (LinearLayout) view.findViewById(R.id.layout_short_view);
        buttonShortViewCancel = (Button) view.findViewById(R.id.button_short_view_cancel);
        buttonShortViewSave = (Button) view.findViewById(R.id.button_short_view_save);
        phvShortView = (PlaceHolderView) view.findViewById(R.id.phv_short_view);

        listWorkOrderSpinner = new ArrayList<>();
        listRecentWorkOrdersDetailed = new ArrayList<>();
        adapterWorkOrderSpinner = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listWorkOrderSpinner);
        adapterWorkOrderSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerWorkOrderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position != 0 && !isFilter) {
                    getRecentWorkOrder(listWorkOrderSpinner.get(position).getId());
                    textViewWorkOrderHeader.setText(listWorkOrderSpinner.get(position).getName());
                } else if (position == 0 && !isFilter) {
                    getRecentWorkOrder("");
                    textViewWorkOrderHeader.setText(getString(R.string.work_orders_recent));
                }
                if (spinnerWorkOrderSpinner.getSelectedItemPosition() > 9) {
                    getViewDetails(listWorkOrderSpinner.get(position).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        getWorkOrderSpinner("");
        if (isFilter) {
            strWorkOrderView = bundle.getString(Cons.KEY_WorkOrderViewID, "");
            String strFields = bundle.getString(Cons.KEY_FilterFields, "");
            String strConditions = bundle.getString(Cons.KEY_FilterConditions, "");
            String strValues = bundle.getString(Cons.KEY_FilterValues, "");
            addFilter(strWorkOrderView, strFields, strConditions, strValues, "Subject", "asc");
        } else {
            getRecentWorkOrder("");
        }

        buttonFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickFilter != null) {
                    int position = spinnerWorkOrderSpinner.getSelectedItemPosition();
                    if (position == 1) {
                        clickFilter.callbackWorkOrderFilter("MyOpenWorkOrdersToday");
                    } else if (position == 2) {
                        clickFilter.callbackWorkOrderFilter("MyOpenWorkOrdersThisWeek");
                    } else if (position == 3) {
                        clickFilter.callbackWorkOrderFilter("AllOpenWorkOrdersToday");
                    } else if (position == 4) {
                        clickFilter.callbackWorkOrderFilter("AllOpenWorkOrdersThisWeek");
                    } else if (position == 5) {
                        clickFilter.callbackWorkOrderFilter("WorkOrdersCreatedToday");
                    } else if (position == 6) {
                        clickFilter.callbackWorkOrderFilter("WorkOrdersCreatedThisWeek");
                    } else if (position == 7) {
                        clickFilter.callbackWorkOrderFilter("UnscheduledWorkOrders");
                    } else if (position == 8) {
                        clickFilter.callbackWorkOrderFilter("UnassignedWorkOrders");
                    } else {
                        Utl.showToast(getActivity(), "Select View");
                    }
                }
            }
        });

        buttonSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutShortView.setVisibility(View.VISIBLE);
                int pos = spinnerWorkOrderSpinner.getSelectedItemPosition();
                String id = listWorkOrderSpinner.get(pos).getId();
                getShortViewList(getString(R.string.workorder), id);
                buttonShortViewCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listShortFieldName.clear();
                        listShortPosition.clear();
                        phvShortView.removeAllViews();
                        layoutShortView.setVisibility(View.GONE);
                    }
                });
                buttonShortViewSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String sortByField = "";
                        String sortByValue = "";
                        for (int i = 0; i < listShortFieldName.size(); i++) {
                            int pos = listShortPosition.get(i);
                            if (pos == 1) {
                                sortByField = listShortFieldName.get(i);
                                sortByValue = "asc";
                            } else if (pos == 2) {
                                sortByField = listShortFieldName.get(i);
                                sortByValue = "desc";
                            }
                        }
                        setShortView(getString(R.string.workorder), id, sortByField, sortByValue);
                    }
                });
            }
        });

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickCreateView != null) {
                    clickCreateView.callbackWorkOrderCreateView(getString(R.string.workorder));
                }
            }
        });

        buttonMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore();
            }
        });

        return view;
    }

    private void setSpinnerDropDownHeight(List<ModelSpinner> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 70);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    public int getListIndex(List<ModelSpinner> list, String name) {
        for (int i = 0; i < list.size(); i++) {
            ModelSpinner model = list.get(i);
            if (name.equals(model.getName())) {
                return i;
            }
        }
        return -1;
    }

    private void getWorkOrderSpinner(String viewName) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getWorkOrdersSpinner(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WorkOrderSpin" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listWorkOrderSpinner.clear();
                            ModelSpinner m = new ModelSpinner();
                            m.setId("0");
                            m.setName("Select View");
                            listWorkOrderSpinner.add(m);
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_WorkOrderViewID));
                                model.setName(dataObject.getString(Cons.KEY_WorkOrderViewName));
                                listWorkOrderSpinner.add(model);
                            }
                            spinnerWorkOrderSpinner.setAdapter(adapterWorkOrderSpinner);
                            setSpinnerDropDownHeight(listWorkOrderSpinner, spinnerWorkOrderSpinner);
                            spinnerWorkOrderSpinner.setSelection(0);
                            if (isFilter) {
                                if (strWorkOrderView.equals("MyOpenWorkOrdersToday")) {
                                    spinnerWorkOrderSpinner.setSelection(1);
                                    textViewWorkOrderHeader.setText(listWorkOrderSpinner.get(1).getName());
                                } else if (strWorkOrderView.equals("MyOpenWorkOrdersThisWeek")) {
                                    spinnerWorkOrderSpinner.setSelection(2);
                                    textViewWorkOrderHeader.setText(listWorkOrderSpinner.get(2).getName());
                                } else if (strWorkOrderView.equals("AllOpenWorkOrdersToday")) {
                                    spinnerWorkOrderSpinner.setSelection(3);
                                    textViewWorkOrderHeader.setText(listWorkOrderSpinner.get(3).getName());
                                } else if (strWorkOrderView.equals("AllOpenWorkOrdersThisWeek")) {
                                    spinnerWorkOrderSpinner.setSelection(4);
                                    textViewWorkOrderHeader.setText(listWorkOrderSpinner.get(4).getName());
                                } else if (strWorkOrderView.equals("WorkOrdersCreatedToday")) {
                                    spinnerWorkOrderSpinner.setSelection(5);
                                    textViewWorkOrderHeader.setText(listWorkOrderSpinner.get(5).getName());
                                } else if (strWorkOrderView.equals("WorkOrdersCreatedThisWeek")) {
                                    spinnerWorkOrderSpinner.setSelection(6);
                                    textViewWorkOrderHeader.setText(listWorkOrderSpinner.get(6).getName());
                                } else if (strWorkOrderView.equals("UnscheduledWorkOrders")) {
                                    spinnerWorkOrderSpinner.setSelection(7);
                                    textViewWorkOrderHeader.setText(listWorkOrderSpinner.get(7).getName());
                                } else if (strWorkOrderView.equals("UnassignedWorkOrders")) {
                                    spinnerWorkOrderSpinner.setSelection(8);
                                    textViewWorkOrderHeader.setText(listWorkOrderSpinner.get(8).getName());
                                } else {
                                    spinnerWorkOrderSpinner.setSelection(0);
                                    textViewWorkOrderHeader.setText(getString(R.string.work_orders_recent));
                                }
                            } else {
                                if (!TextUtils.isEmpty(viewName)) {
                                    for (int i = 0; i < listWorkOrderSpinner.size(); i++) {
                                        String name = listWorkOrderSpinner.get(i).getName();
                                        if (name.equals(viewName)) {
                                            spinnerWorkOrderSpinner.setSelection(i);
                                            textViewWorkOrderHeader.setText(viewName);
                                        }
                                    }
                                } else {
                                    spinnerWorkOrderSpinner.setSelection(0);
                                    textViewWorkOrderHeader.setText(getString(R.string.work_orders_recent));
                                }
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getViewDetails(String viewID) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_Object, RequestBody.create(MediaType.parse("text/plain"), getString(R.string.workorder)));
        map.put(Cons.KEY_ViewID, RequestBody.create(MediaType.parse("text/plain"), viewID));
        Call<ResponseBody> response = apiInterface.getViewDetails(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_ViewDetails" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                        } else if (responseCode == 1) {
                            JSONObject dataObject = object.getJSONObject("data");
                            strViewID = dataObject.getString(Cons.KEY_ViewID);
                            strViewName = dataObject.getString(Cons.KEY_ViewName);
                            strSpecifyFieldsDisplay = dataObject.getString("SpecifyFieldsDisplay");
                            strRestrictVisibility = dataObject.getString(Cons.KEY_RestrictVisibility);
                            /*
                            JSONArray dataDisplayedColumns = object.getJSONArray("DisplayedColumns");
                            for (int i = 0; i < dataDisplayedColumns.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                            }
                            JSONArray dataFilters = object.getJSONArray("Filters");
                            for (int i = 0; i < dataFilters.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                            }
                            */
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRecentWorkOrder(final String workOrderViewID) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        if (!workOrderViewID.equalsIgnoreCase("")) {
            map.put(Cons.KEY_WorkOrderViewID, RequestBody.create(MediaType.parse("text/plain"), workOrderViewID));
        }
        Call<ResponseBody> response;
        if (workOrderViewID.equalsIgnoreCase("")) {
            response = apiInterface.getRecentWorkOrders(header, map);
        } else {
            response = apiInterface.getRecentWorkOrdersById(header, map);
        }
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_RecWO" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listRecentWorkOrdersDetailed.clear();
                            phvRecentWorkOrders.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                Iterator<String> iterator = dataObject.keys();
                                ModelDynamic model = new ModelDynamic();
                                Map<String, String> map = new LinkedHashMap<>();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    String value = dataObject.optString(key);
                                    map.put(key, value);
                                    model.setMap(map);
                                }
                                listRecentWorkOrdersDetailed.add(model);
                            }
                            layoutRecentWorkOrders.setVisibility(View.VISIBLE);
                            for (int i = 0; i < listRecentWorkOrdersDetailed.size(); i++) {
                                phvRecentWorkOrders.addView(new AdapterRecentWorkOrders(getActivity(), FragmentWorkOrder.this, listRecentWorkOrdersDetailed.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void addFilter(String workOrderViewID,
                           String filterFields,
                           String filterConditions,
                           String filterValues,
                           String sortByField,
                           String sortByValue) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_WorkOrderViewID, RequestBody.create(MediaType.parse("text/plain"), workOrderViewID));
        map.put(Cons.KEY_FilterFields, RequestBody.create(MediaType.parse("text/plain"), filterFields));
        map.put(Cons.KEY_FilterConditions, RequestBody.create(MediaType.parse("text/plain"), filterConditions));
        map.put(Cons.KEY_FilterValues, RequestBody.create(MediaType.parse("text/plain"), filterValues));
        map.put(Cons.KEY_SortByField, RequestBody.create(MediaType.parse("text/plain"), sortByField));
        map.put(Cons.KEY_SortByValue, RequestBody.create(MediaType.parse("text/plain"), sortByValue));
        Call<ResponseBody> response = apiInterface.addWOFilter(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Filter" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listRecentWorkOrdersDetailed.clear();
                            phvRecentWorkOrders.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                Iterator<String> iterator = dataObject.keys();
                                ModelDynamic model = new ModelDynamic();
                                Map<String, String> map = new LinkedHashMap<>();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    String value = dataObject.optString(key);
                                    map.put(key, value);
                                    model.setMap(map);
                                }
                                listRecentWorkOrdersDetailed.add(model);
                            }
                            layoutRecentWorkOrders.setVisibility(View.VISIBLE);
                            for (int i = 0; i < listRecentWorkOrdersDetailed.size(); i++) {
                                phvRecentWorkOrders.addView(new AdapterRecentWorkOrders(getActivity(), FragmentWorkOrder.this, listRecentWorkOrdersDetailed.get(i)));
                            }
                            isFilter = false;
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    @Override
    public void callbackWorkOrder(ModelDynamic model) {
        if (click != null) {
            click.callbackWorkOrder(model);
        }
    }

    private void dialogMore() {
        final Dialog dialogMore = new Dialog(getActivity(), R.style.DialogFullScreen);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMore.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        dialogMore.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMore.setCancelable(true);
        dialogMore.setCanceledOnTouchOutside(true);
        dialogMore.setContentView(R.layout.dialog_more_recent);
        TextView buttonMore1 = (TextView) dialogMore.findViewById(R.id.button_more_1);
        TextView buttonMore2 = (TextView) dialogMore.findViewById(R.id.button_more_2);
        TextView buttonMore3 = (TextView) dialogMore.findViewById(R.id.button_more_3);
        TextView buttonMore4 = (TextView) dialogMore.findViewById(R.id.button_more_4);
        TextView buttonMore5 = (TextView) dialogMore.findViewById(R.id.button_more_5);
        TextView buttonMore6 = (TextView) dialogMore.findViewById(R.id.button_more_6);
        TextView buttonMore7 = (TextView) dialogMore.findViewById(R.id.button_more_7);
        View buttonView1 = (View) dialogMore.findViewById(R.id.view_more_1);
        View buttonView2 = (View) dialogMore.findViewById(R.id.view_more_2);
        View buttonView3 = (View) dialogMore.findViewById(R.id.view_more_3);
        View buttonView4 = (View) dialogMore.findViewById(R.id.view_more_4);
        View buttonView5 = (View) dialogMore.findViewById(R.id.view_more_5);
        View buttonView6 = (View) dialogMore.findViewById(R.id.view_more_6);
        View buttonView7 = (View) dialogMore.findViewById(R.id.view_more_7);
        Button buttonMoreClose = (Button) dialogMore.findViewById(R.id.button_more_close);
        if (spinnerWorkOrderSpinner.getSelectedItemPosition() < 9) {
            buttonMore2.setVisibility(View.GONE);
            buttonMore3.setVisibility(View.GONE);
            buttonMore4.setVisibility(View.GONE);
            buttonMore5.setVisibility(View.GONE);
            buttonMore6.setVisibility(View.GONE);
            buttonMore7.setVisibility(View.GONE);
            buttonView2.setVisibility(View.GONE);
            buttonView3.setVisibility(View.GONE);
            buttonView4.setVisibility(View.GONE);
            buttonView5.setVisibility(View.GONE);
            buttonView6.setVisibility(View.GONE);
            buttonView7.setVisibility(View.GONE);
        } else {
            buttonMore2.setVisibility(View.VISIBLE);
            buttonMore3.setVisibility(View.VISIBLE);
            buttonMore4.setVisibility(View.VISIBLE);
            buttonMore5.setVisibility(View.VISIBLE);
            buttonMore6.setVisibility(View.VISIBLE);
            buttonMore7.setVisibility(View.VISIBLE);
            buttonView2.setVisibility(View.VISIBLE);
            buttonView3.setVisibility(View.VISIBLE);
            buttonView4.setVisibility(View.VISIBLE);
            buttonView5.setVisibility(View.VISIBLE);
            buttonView6.setVisibility(View.VISIBLE);
            buttonView7.setVisibility(View.VISIBLE);
        }
        buttonMore1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        buttonMore2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                int pos = spinnerWorkOrderSpinner.getSelectedItemPosition();
                String id = listWorkOrderSpinner.get(pos).getId();
                addCopyView(getString(R.string.workorder), id);
            }
        });
        buttonMore3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                int pos = spinnerWorkOrderSpinner.getSelectedItemPosition();
                String id = listWorkOrderSpinner.get(pos).getId();
                String name = listWorkOrderSpinner.get(pos).getName();
                layoutRenameView.setVisibility(View.VISIBLE);
                editTextRenameViewName.setText(name);
                buttonRenameViewCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editTextRenameViewName.setText("");
                        layoutRenameView.setVisibility(View.GONE);
                    }
                });
                buttonRenameViewSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String rename = editTextRenameViewName.getText().toString().trim();
                        if (TextUtils.isEmpty(rename)) {
                            Utl.showToast(getActivity(), "Enter View Name");
                        } else {
                            addRenameView(getString(R.string.workorder), id, rename);
                        }
                    }
                });
            }
        });
        buttonMore4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                int pos = spinnerWorkOrderSpinner.getSelectedItemPosition();
                String id = listWorkOrderSpinner.get(pos).getId();
                layoutSharingView.setVisibility(View.VISIBLE);
                if (strRestrictVisibility.equals("VisibleOnlyToMe")) {
                    radioButtonSharingMe.setChecked(true);
                } else {
                    radioButtonSharingEveryone.setChecked(true);
                }
                buttonSharingViewCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        layoutSharingView.setVisibility(View.GONE);
                    }
                });
                buttonSharingViewSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String restrictVisibility = "VisibleToEveryone";
                        if (radioButtonSharingMe.isChecked()) {
                            restrictVisibility = "VisibleOnlyToMe";
                        } else {
                            restrictVisibility = "VisibleToEveryone";
                        }
                        addSharingView(getString(R.string.workorder), id, restrictVisibility);
                    }
                });
            }
        });
        buttonMore5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        buttonMore6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        buttonMore7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                int pos = spinnerWorkOrderSpinner.getSelectedItemPosition();
                String id = listWorkOrderSpinner.get(pos).getId();
                addDeleteView(getString(R.string.workorder), id);
            }
        });
        buttonMoreClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
            }
        });
        dialogMore.show();
        dialogMore.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialogMore.getWindow().setDimAmount(0.5f);
        dialogMore.getWindow().setAttributes(lp);
    }

    private void addCopyView(String objectName,
                             String viewID) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_Object, RequestBody.create(MediaType.parse("text/plain"), objectName));
        map.put(Cons.KEY_ViewID, RequestBody.create(MediaType.parse("text/plain"), viewID));
        Call<ResponseBody> response = apiInterface.addCopyView(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_CreateView" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            if (clickCopyView != null) {
                                clickCopyView.callbackWorkOrderCopyView();
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void addDeleteView(String objectName,
                               String viewID) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_Object, RequestBody.create(MediaType.parse("text/plain"), objectName));
        map.put(Cons.KEY_ViewID, RequestBody.create(MediaType.parse("text/plain"), viewID));
        Call<ResponseBody> response = apiInterface.addDeleteView(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_CreateView" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            if (clickDeleteView != null) {
                                clickDeleteView.callbackWorkOrderDeleteView();
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void addRenameView(String objectName, String viewID, String viewName) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_Object, RequestBody.create(MediaType.parse("text/plain"), objectName));
        map.put(Cons.KEY_ViewID, RequestBody.create(MediaType.parse("text/plain"), viewID));
        map.put(Cons.KEY_ViewName, RequestBody.create(MediaType.parse("text/plain"), viewName));
        Call<ResponseBody> response = apiInterface.addRenameView(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_RenameView" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            editTextRenameViewName.setText("");
                            layoutRenameView.setVisibility(View.GONE);
                            getWorkOrderSpinner(viewName);
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void addSharingView(String objectName, String viewID, String restrictVisibility) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_Object, RequestBody.create(MediaType.parse("text/plain"), objectName));
        map.put(Cons.KEY_ViewID, RequestBody.create(MediaType.parse("text/plain"), viewID));
        map.put(Cons.KEY_RestrictVisibility, RequestBody.create(MediaType.parse("text/plain"), restrictVisibility));
        Call<ResponseBody> response = apiInterface.addSharingView(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_SharingView" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            layoutSharingView.setVisibility(View.GONE);
                            getViewDetails(strViewID);
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getShortViewList(String objectName, String viewID) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_Object, RequestBody.create(MediaType.parse("text/plain"), objectName));
        map.put(Cons.KEY_ViewID, RequestBody.create(MediaType.parse("text/plain"), viewID));
        Call<ResponseBody> response = apiInterface.getViewDetails(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listShortFieldName.clear();
                            listShortPosition.clear();
                            JSONObject objectData = object.getJSONObject("data");
                            JSONArray DisplayedColumns = objectData.getJSONArray("DisplayedColumns");
                            for (int i = 0; i < DisplayedColumns.length(); i++) {
                                JSONObject FieldName = DisplayedColumns.getJSONObject(i);
                                String strFieldName = FieldName.getString("FieldName");
                                listShortFieldName.add(strFieldName);
                                listShortPosition.add(0);
                            }
                            listShortPosition.set(0, 1);
                            for (int i = 0; i < listShortFieldName.size(); i++) {
                                phvShortView.addView(new AdapterShortView(getActivity(), FragmentWorkOrder.this, i, listShortPosition, listShortFieldName));
                            }
                            for (int i = 0; i < listShortFieldName.size(); i++) {
                                Log.d("TAG_listShortPosition_" + i, "" + listShortPosition.get(i));
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void setShortView(String objectName, String viewID, String sortByField, String sortByValue) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_Object, RequestBody.create(MediaType.parse("text/plain"), objectName));
        map.put(Cons.KEY_ViewID, RequestBody.create(MediaType.parse("text/plain"), viewID));
        map.put(Cons.KEY_SortByField, RequestBody.create(MediaType.parse("text/plain"), sortByField));
        map.put(Cons.KEY_SortByValue, RequestBody.create(MediaType.parse("text/plain"), sortByValue));
        Call<ResponseBody> response = apiInterface.shortCustomView(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listShortFieldName.clear();
                            listShortPosition.clear();
                            phvShortView.removeAllViews();
                            layoutShortView.setVisibility(View.GONE);
                            Utl.showToast(getActivity(), strMessage);
                            int pos = spinnerWorkOrderSpinner.getSelectedItemPosition();
                            getRecentWorkOrder(listWorkOrderSpinner.get(pos).getId());
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    @Override
    public void callbackShortView(int pos, String objectName) {
        for (int i = 0; i < listShortFieldName.size(); i++) {
            if (pos == i) {
                listShortPosition.set(i, 1);
            } else {
                listShortPosition.set(i, 0);
            }
        }
        phvShortView.getViewAdapter().notifyDataSetChanged();
        for (int i = 0; i < listShortFieldName.size(); i++) {
            Log.d("TAG_listShortPosition_" + i, "" + listShortPosition.get(i));
        }
    }

    @Override
    public void callbackShortViewToggle(int pos, String objectName, boolean checked) {
        for (int i = 0; i < listShortFieldName.size(); i++) {
            if (pos == i) {
                if (checked) {
                    listShortPosition.set(i, 1);
                } else {
                    listShortPosition.set(i, 2);
                }
            } else {
                listShortPosition.set(i, 0);
            }
        }
        for (int i = 0; i < listShortFieldName.size(); i++) {
            Log.d("TAG_listShortPosition_" + i, "" + listShortPosition.get(i));
        }
    }

}