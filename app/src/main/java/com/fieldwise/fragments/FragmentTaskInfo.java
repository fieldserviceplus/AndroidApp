package com.fieldwise.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.activities.ActivityMain;
import com.fieldwise.adapters.AdapterAssignedTo;
import com.fieldwise.models.ModelSpinner;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;
import com.mindorks.placeholderview.PlaceHolderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class FragmentTaskInfo extends Fragment implements
        AdapterAssignedTo.AddAssignedToClickListen {

    private static final int PERMISSION_REQUEST_CODE = 200;

    public APIInterface apiInterface;

    private String taskId;

    private String strSubject,
            strAssignedTo, strAssignedToNew, strAssignedToName, strAssignedToNameNew,
            strRelatedTo,
            strWhat, strWhatName,
            strWho, strWhoName,
            strDueDate, strDueDateNew,
            strDescription,
            strEmail,
            strPhone,
            strType, strTypeName,
            strStatus, strStatusName,
            strPriority, strPriorityName,
            strCreatedDate, strLastModifiedDate, strCreatedBy, strLastModifiedBy,
            strPhoneNo;

    private boolean isCloneMode = false;
    private boolean isAssignedToClicked = false;
    private boolean isDueDateClicked = false;

    private RelativeLayout layoutAssignedTo,
            layoutRelatedTo,
            layoutWhat, layoutWho,
            layoutType, layoutStatus, layoutPriority;

    private LinearLayout layoutSystemInfo;

    private Spinner spinnerRelatedTo,
            spinnerWhat, spinnerWho,
            spinnerType, spinnerStatus, spinnerPriority;

    private List<ModelSpinner> listAssignedTo,
            listRelatedTo,
            listWhat, listWho,
            listType, listStatus, listPriority;

    private ArrayAdapter<ModelSpinner> adapterRelatedTo,
            adapterWhat, adapterWho,
            adapterType, adapterStatus, adapterPriority;

    private EditText editTextSubject,
            edittextAssignedTo,
            edittextDescription,
            edittextEmail,
            edittextPhone;

    private TextView textviewSubject,
            textviewAssignedTo,
            textviewRelatedTo,
            textviewWhat,
            textviewWho,
            edittextDueDate, textviewDueDate,
            textviewDescription,
            textviewEmail,
            textviewPhone,
            textviewType,
            textviewStatus,
            textviewPriority,
            textviewCreatedDate,
            textviewCreatedBy,
            textviewLastModifiedDate,
            textviewLastModifiedBy;

    private LinearLayout layoutNavigation, layoutSaveCancel;
    private ImageButton buttonCall, buttonComment, buttonDate, buttonEdit, buttonMore;
    private Button buttonSave, buttonCancel;

    private ImageButton buttonAssignedTo;
    private LinearLayout layoutAssignedToAdd;
    private EditText edittextAssignedToAddSearch;
    private PlaceHolderView phvAssignedToAdd;

    public CreateFileListener clickCrateFile;

    public interface CreateFileListener {
        void callbackTaskCreateFile(String strObject);
    }

    public GoToRecent clickGoToRecent;

    public interface GoToRecent {
        void callbackGoToRecentTask(String strObject);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_task_detailed, container, false);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        clickCrateFile = (CreateFileListener) getActivity();
        clickGoToRecent = (GoToRecent) getActivity();

        layoutAssignedTo = (RelativeLayout) view.findViewById(R.id.layout_assigned_to);
        layoutRelatedTo = (RelativeLayout) view.findViewById(R.id.layout_related_to);
        layoutWhat = (RelativeLayout) view.findViewById(R.id.layout_what);
        layoutWho = (RelativeLayout) view.findViewById(R.id.layout_who);
        layoutType = (RelativeLayout) view.findViewById(R.id.layout_type);
        layoutStatus = (RelativeLayout) view.findViewById(R.id.layout_status);
        layoutPriority = (RelativeLayout) view.findViewById(R.id.layout_priority);
        layoutSystemInfo = (LinearLayout) view.findViewById(R.id.layout_system_info);

        spinnerRelatedTo = (Spinner) view.findViewById(R.id.spinner_related_to);
        spinnerWhat = (Spinner) view.findViewById(R.id.spinner_what);
        spinnerWho = (Spinner) view.findViewById(R.id.spinner_who);
        spinnerType = (Spinner) view.findViewById(R.id.spinner_type);
        spinnerStatus = (Spinner) view.findViewById(R.id.spinner_status);
        spinnerPriority = (Spinner) view.findViewById(R.id.spinner_priority);

        editTextSubject = (EditText) view.findViewById(R.id.edittext_subject);
        edittextAssignedTo = (EditText) view.findViewById(R.id.edittext_assigned_to);
        edittextDescription = (EditText) view.findViewById(R.id.edittext_description);
        edittextEmail = (EditText) view.findViewById(R.id.edittext_email);
        edittextPhone = (EditText) view.findViewById(R.id.edittext_phone);

        textviewSubject = (TextView) view.findViewById(R.id.textview_subject);
        textviewAssignedTo = (TextView) view.findViewById(R.id.textview_assigned_to);
        textviewRelatedTo = (TextView) view.findViewById(R.id.textview_related_to);
        textviewWhat = (TextView) view.findViewById(R.id.textview_what);
        textviewWho = (TextView) view.findViewById(R.id.textview_who);
        edittextDueDate = (TextView) view.findViewById(R.id.edittext_due_date);
        textviewDueDate = (TextView) view.findViewById(R.id.textview_due_date);
        textviewDescription = (TextView) view.findViewById(R.id.textview_description);
        textviewEmail = (TextView) view.findViewById(R.id.textview_email);
        textviewPhone = (TextView) view.findViewById(R.id.textview_phone);
        textviewType = (TextView) view.findViewById(R.id.textview_type);
        textviewStatus = (TextView) view.findViewById(R.id.textview_status);
        textviewPriority = (TextView) view.findViewById(R.id.textview_priority);
        textviewCreatedDate = (TextView) view.findViewById(R.id.textview_created_date);
        textviewCreatedBy = (TextView) view.findViewById(R.id.textview_created_by);
        textviewLastModifiedDate = (TextView) view.findViewById(R.id.textview_last_modified_date);
        textviewLastModifiedBy = (TextView) view.findViewById(R.id.textview_last_modified_by);

        layoutNavigation = (LinearLayout) view.findViewById(R.id.layout_navigation);
        buttonCall = (ImageButton) view.findViewById(R.id.button_call);
        buttonComment = (ImageButton) view.findViewById(R.id.button_comment);
        buttonDate = (ImageButton) view.findViewById(R.id.button_date);
        buttonEdit = (ImageButton) view.findViewById(R.id.button_edit);
        buttonMore = (ImageButton) view.findViewById(R.id.button_more);
        layoutSaveCancel = (LinearLayout) view.findViewById(R.id.layout_save_cancel);
        buttonSave = (Button) view.findViewById(R.id.button_save);
        buttonCancel = (Button) view.findViewById(R.id.button_cancel);

        buttonAssignedTo = (ImageButton) view.findViewById(R.id.button_assigned_to);
        layoutAssignedToAdd = (LinearLayout) view.findViewById(R.id.layout_assigned_to_add);
        edittextAssignedToAddSearch = (EditText) view.findViewById(R.id.edittext_assigned_to_add_search);
        phvAssignedToAdd = (PlaceHolderView) view.findViewById(R.id.phv_assigned_to_add);

        listAssignedTo = new ArrayList<>();
        listRelatedTo = new ArrayList<>();
        listWhat = new ArrayList<>();
        listWho = new ArrayList<>();
        listType = new ArrayList<>();
        listStatus = new ArrayList<>();
        listPriority = new ArrayList<>();
        adapterRelatedTo = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listRelatedTo);
        adapterRelatedTo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterWhat = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listWhat);
        adapterWhat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterWho = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listWho);
        adapterWho.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterType = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listType);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterStatus = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listStatus);
        adapterStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterPriority = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listPriority);
        adapterPriority.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        editMode(false, false);

        taskId = getArguments().getString(Cons.KEY_TaskID);

        getTaskDetailed(taskId);

        getAssignedTo();
        getRelatedTo();
        getWho();
        getType();
        getStatue();
        getPriority();

        buttonAssignedTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isAssignedToClicked = true;
                layoutAssignedToAdd.setVisibility(View.VISIBLE);
                listAssignedTo.clear();
                getAssignedTo();
            }
        });

        spinnerRelatedTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String str = listRelatedTo.get(position).getName();
                getWhat(str);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        edittextDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                Date date = null;
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                if (isDueDateClicked) {
                    try {
                        date = sdf.parse(strDueDateNew);
                    } catch (ParseException ex) {
                        Log.v("Exception", ex.getLocalizedMessage());
                    }
                } else {
                    try {
                        date = sdf.parse(strDueDate);
                    } catch (ParseException ex) {
                        Log.v("Exception", ex.getLocalizedMessage());
                    }
                }
                calendar.setTime(date);
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                final int hour = calendar.get(Calendar.HOUR);
                final int minute = calendar.get(Calendar.MINUTE);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        isDueDateClicked = true;
                        strDueDateNew = setDate(d, m, y);
                        edittextDueDate.setText(formatDate(strDueDateNew));
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        buttonDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMarkCompleted();
            }
        });

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editMode(true, true);
            }
        });

        buttonMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String subject = editTextSubject.getText().toString().trim();
                String assignedTo = "";
                if (isAssignedToClicked) {
                    assignedTo = strAssignedToNew;
                } else {
                    assignedTo = strAssignedTo;
                }
                String relatedTo = listRelatedTo.get(spinnerRelatedTo.getSelectedItemPosition()).getName();
                String what = listWhat.get(spinnerWhat.getSelectedItemPosition()).getId();
                String who = listWho.get(spinnerWho.getSelectedItemPosition()).getId();
                String dueDate = "";
                if (isDueDateClicked) {
                    dueDate = strDueDateNew;
                } else {
                    dueDate = strDueDate;
                }
                String description = edittextDescription.getText().toString().trim();
                String email = edittextEmail.getText().toString().trim();
                String phone = edittextPhone.getText().toString().trim();
                String type = listType.get(spinnerType.getSelectedItemPosition()).getId();
                String status = listStatus.get(spinnerStatus.getSelectedItemPosition()).getId();
                String priority = listPriority.get(spinnerPriority.getSelectedItemPosition()).getId();

                if (TextUtils.isEmpty(subject)) {
                    Utl.showToast(getActivity(), "Enter Subject");
                } else if (!TextUtils.isEmpty(email)) {
                    if (!Patterns.EMAIL_ADDRESS.matcher(strEmail).matches()) {
                        Utl.showToast(getActivity(), getString(R.string.invalid_email));
                    }
                } else {
                    if (isCloneMode) {
                        /*
                        createTask(subject,
                                assignedTo,
                                relatedTo,
                                what,
                                who,
                                startDate, endDate, dueDate,
                                description,
                                email,
                                phone,
                                type,
                                status,
                                priority,
                                strRecurring, startOn,
                                repeatEvery, intervalEvery,
                                repeatOn.toString(), ends, endsOn, endsAfter, recurrenceStartTime, recurrenceEndTime);
                                */
                    } else {
                        editTask(taskId,
                                subject,
                                assignedTo,
                                relatedTo,
                                what,
                                who,
                                dueDate,
                                description,
                                email,
                                phone,
                                type,
                                status,
                                priority);
                    }
                }

            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCloneMode = false;
                editMode(false, true);
            }
        });

        edittextAssignedToAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvAssignedToAdd.removeAllViews();
                    ArrayList<ModelSpinner> modle = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listAssignedTo) {
                        if (m.getName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvAssignedToAdd
                                .addView(new AdapterAssignedTo(getActivity(), FragmentTaskInfo.this, i, modle.get(i)));
                    }
                } else {
                    phvAssignedToAdd.removeAllViews();
                    for (int i = 0; i < listAssignedTo.size(); i++) {
                        phvAssignedToAdd
                                .addView(new AdapterAssignedTo(getActivity(), FragmentTaskInfo.this, i, listAssignedTo.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        buttonDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMarkCompleted();
            }
        });

        return view;

    }

    private void editMode(boolean bolEdit, boolean bolResponse) {
        if (!bolEdit) {
            editTextSubject.setVisibility(View.GONE);
            textviewSubject.setVisibility(View.VISIBLE);

            layoutAssignedTo.setVisibility(View.GONE);
            textviewAssignedTo.setVisibility(View.VISIBLE);

            layoutRelatedTo.setVisibility(View.GONE);
            spinnerRelatedTo.setVisibility(View.GONE);
            spinnerRelatedTo.setAdapter(null);
            textviewRelatedTo.setVisibility(View.VISIBLE);

            layoutWhat.setVisibility(View.GONE);
            spinnerWhat.setVisibility(View.GONE);
            spinnerWhat.setAdapter(null);
            textviewWhat.setVisibility(View.VISIBLE);

            layoutWho.setVisibility(View.GONE);
            spinnerWho.setVisibility(View.GONE);
            spinnerWho.setAdapter(null);
            textviewWho.setVisibility(View.VISIBLE);

            edittextDescription.setVisibility(View.GONE);
            textviewDescription.setVisibility(View.VISIBLE);

            edittextEmail.setVisibility(View.GONE);
            textviewEmail.setVisibility(View.VISIBLE);

            edittextPhone.setVisibility(View.GONE);
            textviewPhone.setVisibility(View.VISIBLE);

            edittextDueDate.setVisibility(View.GONE);
            textviewDueDate.setVisibility(View.VISIBLE);

            layoutStatus.setVisibility(View.GONE);
            spinnerStatus.setVisibility(View.GONE);
            spinnerStatus.setAdapter(null);
            textviewStatus.setVisibility(View.VISIBLE);

            layoutType.setVisibility(View.GONE);
            spinnerType.setVisibility(View.GONE);
            spinnerType.setAdapter(null);
            textviewType.setVisibility(View.VISIBLE);

            layoutPriority.setVisibility(View.GONE);
            spinnerPriority.setVisibility(View.GONE);
            spinnerPriority.setAdapter(null);
            textviewPriority.setVisibility(View.VISIBLE);

            buttonCall.setEnabled(true);
            buttonComment.setEnabled(true);
            buttonDate.setEnabled(true);
            buttonEdit.setEnabled(true);

            layoutSystemInfo.setVisibility(View.VISIBLE);
            layoutSaveCancel.setVisibility(View.GONE);
            layoutNavigation.setVisibility(View.VISIBLE);
            if (bolResponse) {
                setViewInfo();
            }
        } else {

            editTextSubject.setVisibility(View.VISIBLE);
            textviewSubject.setVisibility(View.GONE);

            layoutAssignedTo.setVisibility(View.VISIBLE);
            textviewAssignedTo.setVisibility(View.GONE);

            layoutRelatedTo.setVisibility(View.VISIBLE);
            spinnerRelatedTo.setVisibility(View.VISIBLE);
            spinnerRelatedTo.setAdapter(adapterRelatedTo);
            textviewRelatedTo.setVisibility(View.GONE);

            layoutWho.setVisibility(View.VISIBLE);
            spinnerWho.setVisibility(View.VISIBLE);
            spinnerWho.setAdapter(adapterWho);
            textviewWho.setVisibility(View.GONE);

            layoutWhat.setVisibility(View.VISIBLE);
            spinnerWhat.setVisibility(View.VISIBLE);
            spinnerWhat.setAdapter(adapterWhat);
            textviewWhat.setVisibility(View.GONE);

            edittextDescription.setVisibility(View.VISIBLE);
            textviewDescription.setVisibility(View.GONE);

            edittextEmail.setVisibility(View.VISIBLE);
            textviewEmail.setVisibility(View.GONE);

            edittextPhone.setVisibility(View.VISIBLE);
            textviewPhone.setVisibility(View.GONE);

            edittextDueDate.setVisibility(View.VISIBLE);
            textviewDueDate.setVisibility(View.GONE);

            layoutType.setVisibility(View.VISIBLE);
            spinnerType.setVisibility(View.VISIBLE);
            spinnerType.setAdapter(adapterType);
            textviewType.setVisibility(View.GONE);

            layoutStatus.setVisibility(View.VISIBLE);
            spinnerStatus.setVisibility(View.VISIBLE);
            spinnerStatus.setAdapter(adapterStatus);
            textviewStatus.setVisibility(View.GONE);

            layoutPriority.setVisibility(View.VISIBLE);
            spinnerPriority.setVisibility(View.VISIBLE);
            spinnerPriority.setAdapter(adapterPriority);
            textviewPriority.setVisibility(View.GONE);

            buttonCall.setEnabled(false);
            buttonComment.setEnabled(false);
            buttonDate.setEnabled(false);
            buttonEdit.setEnabled(false);

            layoutSystemInfo.setVisibility(View.GONE);
            layoutSaveCancel.setVisibility(View.VISIBLE);
            layoutNavigation.setVisibility(View.GONE);
            if (bolResponse) {
                setEditInfo();
            }
        }
    }

    private void cloneMode() {
        editTextSubject.setVisibility(View.VISIBLE);
        textviewSubject.setVisibility(View.GONE);

        layoutAssignedTo.setVisibility(View.VISIBLE);
        textviewAssignedTo.setVisibility(View.GONE);

        layoutRelatedTo.setVisibility(View.VISIBLE);
        spinnerRelatedTo.setVisibility(View.VISIBLE);
        spinnerRelatedTo.setAdapter(adapterRelatedTo);
        textviewRelatedTo.setVisibility(View.GONE);

        layoutWho.setVisibility(View.VISIBLE);
        spinnerWho.setVisibility(View.VISIBLE);
        spinnerWho.setAdapter(adapterWho);
        textviewWho.setVisibility(View.GONE);

        layoutWhat.setVisibility(View.VISIBLE);
        spinnerWhat.setVisibility(View.VISIBLE);
        spinnerWhat.setAdapter(adapterWhat);
        textviewWhat.setVisibility(View.GONE);

        edittextDescription.setVisibility(View.VISIBLE);
        textviewDescription.setVisibility(View.GONE);

        edittextEmail.setVisibility(View.VISIBLE);
        textviewEmail.setVisibility(View.GONE);

        edittextPhone.setVisibility(View.VISIBLE);
        textviewPhone.setVisibility(View.GONE);

        edittextDueDate.setVisibility(View.VISIBLE);
        textviewDueDate.setVisibility(View.GONE);

        layoutType.setVisibility(View.VISIBLE);
        spinnerType.setVisibility(View.VISIBLE);
        spinnerType.setAdapter(adapterType);
        textviewType.setVisibility(View.GONE);

        layoutStatus.setVisibility(View.VISIBLE);
        spinnerStatus.setVisibility(View.VISIBLE);
        spinnerStatus.setAdapter(adapterStatus);
        textviewStatus.setVisibility(View.GONE);

        layoutPriority.setVisibility(View.VISIBLE);
        spinnerPriority.setVisibility(View.VISIBLE);
        spinnerPriority.setAdapter(adapterPriority);
        textviewPriority.setVisibility(View.GONE);

        buttonCall.setEnabled(false);
        buttonComment.setEnabled(false);
        buttonDate.setEnabled(false);
        buttonEdit.setEnabled(false);

        layoutSystemInfo.setVisibility(View.GONE);
        layoutSaveCancel.setVisibility(View.VISIBLE);
        layoutNavigation.setVisibility(View.GONE);
        setCloneInfo();
    }

    private void setViewInfo() {
        ActivityMain.textviewCenter.setText(strSubject);
        textviewSubject.setText(strSubject);
        textviewAssignedTo.setText(strAssignedToName);
        textviewRelatedTo.setText(strRelatedTo);
        textviewWhat.setText(strWhatName);
        textviewWho.setText(strWhoName);
        textviewDueDate.setText(formatDate(strDueDate));
        textviewDescription.setText(strDescription);
        textviewEmail.setText(strEmail);
        textviewPhone.setText(strPhone);
        textviewType.setText(strTypeName);
        textviewStatus.setText(strStatusName);
        textviewPriority.setText(strPriorityName);
        textviewCreatedDate.setText(strCreatedDate);
        textviewCreatedBy.setText(strCreatedBy);
        textviewLastModifiedDate.setText(strLastModifiedDate);
        textviewLastModifiedBy.setText(strLastModifiedBy);
    }

    private void setEditInfo() {
        editTextSubject.setText(strSubject);
        if (isAssignedToClicked) {
            edittextAssignedTo.setText(strAssignedToNameNew);
        } else {
            edittextAssignedTo.setText(strAssignedToName);
        }
        setSpinnerDropDownHeight(listRelatedTo, spinnerRelatedTo);
        setSpinnerDropDownHeight(listWhat, spinnerWhat);
        setSpinnerDropDownHeight(listWho, spinnerWho);
        spinnerRelatedTo.setSelection(getListIndex(listRelatedTo, strRelatedTo));
        spinnerWhat.setSelection(getListIndex(listWhat, strWhatName));
        spinnerWho.setSelection(getListIndex(listWho, strWhoName));
        if (isDueDateClicked) {
            edittextDueDate.setText(formatDate(strDueDateNew));
        } else {
            edittextDueDate.setText(formatDate(strDueDate));
        }
        edittextDescription.setText(strDescription);
        edittextEmail.setText(strEmail);
        edittextPhone.setText(strPhone);
        setSpinnerDropDownHeight(listType, spinnerType);
        spinnerType.setSelection(getListIndex(listType, strTypeName));
        setSpinnerDropDownHeight(listStatus, spinnerStatus);
        spinnerStatus.setSelection(getListIndex(listStatus, strStatusName));
        setSpinnerDropDownHeight(listPriority, spinnerPriority);
        spinnerPriority.setSelection(getListIndex(listPriority, strPriorityName));
    }

    private void setCloneInfo() {
        isCloneMode = true;
        ActivityMain.textviewCenter.setText(getString(R.string.task_create));
        editTextSubject.setText(strSubject);
        if (isAssignedToClicked) {
            edittextAssignedTo.setText(strAssignedToNameNew);
        } else {
            edittextAssignedTo.setText(strAssignedToName);
        }
        setSpinnerDropDownHeight(listRelatedTo, spinnerRelatedTo);
        setSpinnerDropDownHeight(listWhat, spinnerWhat);
        setSpinnerDropDownHeight(listWho, spinnerWho);
        spinnerRelatedTo.setSelection(getListIndex(listRelatedTo, strRelatedTo));
        spinnerWhat.setSelection(getListIndex(listWhat, strWhatName));
        spinnerWho.setSelection(getListIndex(listWho, strWhoName));
        if (isDueDateClicked) {
            edittextDueDate.setText(formatDate(strDueDateNew));
        } else {
            edittextDueDate.setText(formatDate(strDueDate));
        }
        edittextDescription.setText(strDescription);
        edittextEmail.setText(strEmail);
        edittextPhone.setText(strPhone);
        setSpinnerDropDownHeight(listType, spinnerType);
        spinnerType.setSelection(getListIndex(listType, strTypeName));
        setSpinnerDropDownHeight(listStatus, spinnerStatus);
        spinnerStatus.setSelection(getListIndex(listStatus, strStatusName));
        setSpinnerDropDownHeight(listPriority, spinnerPriority);
        spinnerPriority.setSelection(getListIndex(listPriority, strPriorityName));
    }

    private void setSpinnerDropDownHeight(List<ModelSpinner> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 70);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    public int getListIndex(List<ModelSpinner> list, String name) {
        for (int i = 0; i < list.size(); i++) {
            ModelSpinner model = list.get(i);
            if (name.equals(model.getName())) {
                return i;
            }
        }
        return -1;
    }

    public String formatDate(String time) {
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String setDate(int d, int m, int y) {
        String time = "" + (m + 1) + "/" + d + "/" + y;
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "MM/dd/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void dialogMore() {
        final Dialog dialogMore = new Dialog(getActivity(), R.style.DialogFullScreen);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMore.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        dialogMore.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMore.setCancelable(true);
        dialogMore.setCanceledOnTouchOutside(true);
        dialogMore.setContentView(R.layout.dialog_more);

        TextView buttonMoreCall = (TextView) dialogMore.findViewById(R.id.button_more_call);
        TextView buttonMoreText = (TextView) dialogMore.findViewById(R.id.button_more_text);
        TextView buttonMoreEmail = (TextView) dialogMore.findViewById(R.id.button_more_email);
        TextView buttonMoreEdit = (TextView) dialogMore.findViewById(R.id.button_more_edit);
        TextView buttonMoreSign = (TextView) dialogMore.findViewById(R.id.button_more_sign);
        TextView buttonMoreDoc = (TextView) dialogMore.findViewById(R.id.button_more_doc);
        TextView buttonMoreNewLine = (TextView) dialogMore.findViewById(R.id.button_more_new_line);
        TextView buttonMoreChemical = (TextView) dialogMore.findViewById(R.id.button_more_new_chemical);
        TextView buttonMoreEvent = (TextView) dialogMore.findViewById(R.id.button_more_new_event);
        TextView buttonMoreInvoice = (TextView) dialogMore.findViewById(R.id.button_more_new_invoice);
        TextView buttonMoreFile = (TextView) dialogMore.findViewById(R.id.button_more_new_file);
        TextView buttonMoreNote = (TextView) dialogMore.findViewById(R.id.button_more_new_note);
        TextView buttonMoreTask = (TextView) dialogMore.findViewById(R.id.button_more_new_task);
        TextView buttonMoreConvert = (TextView) dialogMore.findViewById(R.id.button_more_convert);
        TextView buttonMoreClone = (TextView) dialogMore.findViewById(R.id.button_more_clone);
        TextView buttonMoreDelete = (TextView) dialogMore.findViewById(R.id.button_more_delete);
        View buttomViewCall = (View) dialogMore.findViewById(R.id.view_more_call);
        View buttomViewText = (View) dialogMore.findViewById(R.id.view_more_text);
        View buttomViewEmail = (View) dialogMore.findViewById(R.id.view_more_email);
        View buttomViewEdit = (View) dialogMore.findViewById(R.id.view_more_edit);
        View buttomViewSign = (View) dialogMore.findViewById(R.id.view_more_sign);
        View buttomViewDoc = (View) dialogMore.findViewById(R.id.view_more_doc);
        View buttomViewNewLine = (View) dialogMore.findViewById(R.id.view_more_new_line);
        View buttomViewChemical = (View) dialogMore.findViewById(R.id.view_more_new_chemical);
        View buttomViewEvent = (View) dialogMore.findViewById(R.id.view_more_new_event);
        View buttomViewInvoice = (View) dialogMore.findViewById(R.id.view_more_new_invoice);
        View buttomViewFile = (View) dialogMore.findViewById(R.id.view_more_new_file);
        View buttomViewNote = (View) dialogMore.findViewById(R.id.view_more_new_note);
        View buttomViewTask = (View) dialogMore.findViewById(R.id.view_more_new_task);
        View buttomViewConvert = (View) dialogMore.findViewById(R.id.view_more_convert);
        View buttomViewClone = (View) dialogMore.findViewById(R.id.view_more_clone);
        Button buttonMoreClose = (Button) dialogMore.findViewById(R.id.button_more_close);

        buttonMoreSign.setVisibility(View.GONE);
        buttomViewSign.setVisibility(View.GONE);
        buttonMoreDoc.setVisibility(View.GONE);
        buttomViewDoc.setVisibility(View.GONE);
        buttonMoreNewLine.setVisibility(View.GONE);
        buttomViewNewLine.setVisibility(View.GONE);
        buttonMoreChemical.setVisibility(View.GONE);
        buttomViewChemical.setVisibility(View.GONE);
        buttonMoreEvent.setVisibility(View.GONE);
        buttomViewEvent.setVisibility(View.GONE);
        buttonMoreInvoice.setVisibility(View.GONE);
        buttomViewInvoice.setVisibility(View.GONE);
        buttonMoreFile.setVisibility(View.GONE);
        buttomViewFile.setVisibility(View.GONE);
        buttonMoreNote.setVisibility(View.GONE);
        buttomViewNote.setVisibility(View.GONE);
        buttonMoreTask.setVisibility(View.GONE);
        buttomViewTask.setVisibility(View.GONE);

        buttonMoreEmail.setText(getString(R.string.mark_completed));
        buttonMoreConvert.setText(getString(R.string.email));
        buttonMoreClone.setText(getString(R.string.clone_task));
        buttonMoreDelete.setText(getString(R.string.delete_task));

        buttonMoreCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    if (!TextUtils.isEmpty(strPhoneNo)) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + strPhoneNo));
                        startActivity(callIntent);
                    } else {
                        Utl.showToast(getActivity(), "No Phone Number Available.");
                    }
                }
            }
        });

        buttonMoreText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    if (!TextUtils.isEmpty(strPhoneNo)) {
                        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                        smsIntent.setData(Uri.parse("sms:" + strPhoneNo));
                        startActivity(smsIntent);
                    } else {
                        Utl.showToast(getActivity(), "No Phone Number Available.");
                    }
                }
            }
        });

        buttonMoreEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMarkCompleted();
            }
        });

        buttonMoreEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editMode(true, true);
                dialogMore.dismiss();
            }
        });

        buttonMoreFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickCrateFile != null) {
                    clickCrateFile.callbackTaskCreateFile(getString(R.string.task));
                }
                dialogMore.dismiss();
            }
        });

        buttonMoreClone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cloneMode();
                dialogMore.dismiss();
            }
        });

        buttonMoreDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteObject();
                dialogMore.dismiss();
            }
        });

        buttonMoreClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
            }
        });

        dialogMore.show();
        dialogMore.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialogMore.getWindow().setDimAmount(0.5f);
        dialogMore.getWindow().setAttributes(lp);
    }

    private void getAssignedTo() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getTaskAssignedTo(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Invoice_AllU" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAssignedTo.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_UserID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listAssignedTo.add(model);
                            }
                            for (int i = 0; i < listAssignedTo.size(); i++) {
                                phvAssignedToAdd
                                        .addView(new AdapterAssignedTo(getActivity(), FragmentTaskInfo.this, i, listAssignedTo.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedTo() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getTaskRelatedTo(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Invoice_AllU" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listRelatedTo.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId("" + i);
                                model.setName(dataObject.getString(Cons.KEY_Name));
                                listRelatedTo.add(model);
                            }
                            spinnerRelatedTo.setAdapter(adapterRelatedTo);
                            setSpinnerDropDownHeight(listRelatedTo, spinnerRelatedTo);
                            spinnerRelatedTo.setSelection(0);
                            getWhat(listRelatedTo.get(0).getName());
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getWhat(String relatedTo) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), relatedTo));
        Call<ResponseBody> response = apiInterface.getTaskWhat(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Invoice_AllU" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listWhat.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_ID));
                                model.setName(dataObject.getString(Cons.KEY_Name));
                                listWhat.add(model);
                            }
                            spinnerWhat.setAdapter(adapterWhat);
                            setSpinnerDropDownHeight(listWhat, spinnerWhat);
                            spinnerWhat.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getWho() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getTaskWho(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Invoice_AllU" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listWho.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_ContactID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listWho.add(model);
                            }
                            spinnerWho.setAdapter(adapterWho);
                            setSpinnerDropDownHeight(listWho, spinnerWho);
                            spinnerWho.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getType() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getTaskType(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Invoice_AllU" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listType.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_TaskTypeID));
                                model.setName(dataObject.getString(Cons.KEY_TaskType));
                                listType.add(model);
                            }
                            spinnerType.setAdapter(adapterType);
                            setSpinnerDropDownHeight(listType, spinnerType);
                            spinnerType.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getStatue() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getTaskStatus(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Invoice_AllU" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listStatus.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_TaskStatusID));
                                model.setName(dataObject.getString(Cons.KEY_TaskStatus));
                                listStatus.add(model);
                            }
                            spinnerStatus.setAdapter(adapterStatus);
                            setSpinnerDropDownHeight(listStatus, spinnerStatus);
                            spinnerStatus.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getPriority() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getTaskPriority(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Invoice_AllU" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listPriority.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_TaskPriorityID));
                                model.setName(dataObject.getString(Cons.KEY_Priority));
                                listPriority.add(model);
                            }
                            spinnerPriority.setAdapter(adapterPriority);
                            setSpinnerDropDownHeight(listPriority, spinnerPriority);
                            spinnerPriority.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getTaskDetailed(final String taskId) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_TaskID, RequestBody.create(MediaType.parse("text/plain"), taskId));
        Call<ResponseBody> response;
        response = apiInterface.getTaskDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_AccInfo" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            JSONObject dataObject = object.getJSONObject("data");
                            strSubject = dataObject.getString(Cons.KEY_Subject);
                            strAssignedTo = dataObject.getString(Cons.KEY_AssignedTo);
                            strAssignedToName = dataObject.getString(Cons.KEY_AssignedToName);
                            strRelatedTo = dataObject.getString(Cons.KEY_RelatedTo);
                            strWhat = dataObject.getString(Cons.KEY_What);
                            strWhatName = dataObject.getString(Cons.KEY_RelatedToName);
                            strWho = dataObject.getString(Cons.KEY_Who);
                            strWhoName = dataObject.getString(Cons.KEY_WhatName);
                            strDueDate = dataObject.getString(Cons.KEY_DueDate);
                            strDescription = dataObject.getString(Cons.KEY_Description);
                            strEmail = dataObject.getString(Cons.KEY_Email);
                            strPhone = dataObject.getString(Cons.KEY_Phone);
                            strType = dataObject.getString(Cons.KEY_TaskTypeID);
                            strTypeName = dataObject.getString(Cons.KEY_TaskType);
                            strStatus = dataObject.getString(Cons.KEY_TaskStatusID);
                            strStatusName = dataObject.getString(Cons.KEY_TaskStatus);
                            strPriority = dataObject.getString(Cons.KEY_TaskPriorityID);
                            strPriorityName = dataObject.getString(Cons.KEY_TaskPriority);
                            strCreatedDate = dataObject.getString(Cons.KEY_CreatedDate);
                            strCreatedBy = dataObject.getString(Cons.KEY_CreatedBy);
                            strLastModifiedDate = dataObject.getString(Cons.KEY_LastModifiedDate);
                            strLastModifiedBy = dataObject.getString(Cons.KEY_LastModifiedBy);
                            strPhoneNo = dataObject.getString(Cons.KEY_Phone);
                            editMode(false, true);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_11", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_12", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_13", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_14", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void createTask(String subject,
                            String assignedTo,
                            String relatedTo,
                            String what,
                            String who,
                            String startDate, String endDate, String dueDate,
                            String description,
                            String email,
                            String phone,
                            String type,
                            String status,
                            String priority,
                            String strRecurring, String startOn,
                            String repeatEvery, String intervalEvery, String repeatOn,
                            String ends, String endsOn, String endsAfter,
                            String recurrenceStartTime, String recurrenceEndTime) {

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_Subject, RequestBody.create(MediaType.parse("text/plain"), subject));
        map.put(Cons.KEY_AssignedTo, RequestBody.create(MediaType.parse("text/plain"), assignedTo));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), relatedTo));
        map.put(Cons.KEY_What, RequestBody.create(MediaType.parse("text/plain"), what));
        map.put(Cons.KEY_Who, RequestBody.create(MediaType.parse("text/plain"), who));
        map.put(Cons.KEY_StartDate, RequestBody.create(MediaType.parse("text/plain"), startDate));
        map.put(Cons.KEY_EndDate, RequestBody.create(MediaType.parse("text/plain"), endDate));
        map.put(Cons.KEY_DueDate, RequestBody.create(MediaType.parse("text/plain"), dueDate));
        if (!TextUtils.isEmpty(description)) {
            map.put(Cons.KEY_Description, RequestBody.create(MediaType.parse("text/plain"), description));
        }
        if (!TextUtils.isEmpty(email)) {
            map.put(Cons.KEY_Email, RequestBody.create(MediaType.parse("text/plain"), email));
        }
        if (!TextUtils.isEmpty(phone)) {
            map.put(Cons.KEY_Phone, RequestBody.create(MediaType.parse("text/plain"), phone));
        }
        map.put(Cons.KEY_TaskType, RequestBody.create(MediaType.parse("text/plain"), type));
        map.put(Cons.KEY_TaskStatus, RequestBody.create(MediaType.parse("text/plain"), status));
        map.put(Cons.KEY_TaskPriority, RequestBody.create(MediaType.parse("text/plain"), priority));

        map.put(Cons.KEY_IsRecurring, RequestBody.create(MediaType.parse("text/plain"), strRecurring));
        map.put(Cons.KEY_StartOn, RequestBody.create(MediaType.parse("text/plain"), startOn));
        map.put(Cons.KEY_RepeatEvery, RequestBody.create(MediaType.parse("text/plain"), repeatEvery));
        map.put(Cons.KEY_RepeatOn, RequestBody.create(MediaType.parse("text/plain"), repeatOn));
        map.put(Cons.KEY_IntervalEvery, RequestBody.create(MediaType.parse("text/plain"), intervalEvery));
        map.put(Cons.KEY_Ends, RequestBody.create(MediaType.parse("text/plain"), ends));
        map.put(Cons.KEY_EndsOnDate, RequestBody.create(MediaType.parse("text/plain"), endsOn));
        map.put(Cons.KEY_EndsAfterOccurrences, RequestBody.create(MediaType.parse("text/plain"), endsAfter));
        map.put(Cons.KEY_StartTime, RequestBody.create(MediaType.parse("text/plain"), recurrenceStartTime));
        map.put(Cons.KEY_EndTime, RequestBody.create(MediaType.parse("text/plain"), recurrenceEndTime));
        Call<ResponseBody> response;
        response = apiInterface.createTaskDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Task_Create" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            isCloneMode = false;
                            String strTaskID = object.getString("TaskID");
                            taskId = strTaskID;
                            Utl.showToast(getActivity(), strMessage);
                            getTaskDetailed(strTaskID);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_21", "" + e.getMessage());
                    } catch (IOException e) {
                        Log.d("TAG_err_22", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_23", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_24", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void editTask(final String taskId,
                          String subject,
                          String assignedTo,
                          String relatedTo,
                          String what,
                          String who,
                          String dueDate,
                          String description,
                          String email,
                          String phone,
                          String type,
                          String status,
                          String priority) {

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_TaskID, RequestBody.create(MediaType.parse("text/plain"), taskId));
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_Subject, RequestBody.create(MediaType.parse("text/plain"), subject));
        map.put(Cons.KEY_AssignedTo, RequestBody.create(MediaType.parse("text/plain"), assignedTo));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), relatedTo));
        map.put(Cons.KEY_What, RequestBody.create(MediaType.parse("text/plain"), what));
        map.put(Cons.KEY_Who, RequestBody.create(MediaType.parse("text/plain"), who));
        map.put(Cons.KEY_DueDate, RequestBody.create(MediaType.parse("text/plain"), dueDate));
        if (!TextUtils.isEmpty(description)) {
            map.put(Cons.KEY_Description, RequestBody.create(MediaType.parse("text/plain"), description));
        }
        if (!TextUtils.isEmpty(email)) {
            map.put(Cons.KEY_Email, RequestBody.create(MediaType.parse("text/plain"), email));
        }
        if (!TextUtils.isEmpty(phone)) {
            map.put(Cons.KEY_Phone, RequestBody.create(MediaType.parse("text/plain"), phone));
        }
        map.put(Cons.KEY_TaskType, RequestBody.create(MediaType.parse("text/plain"), type));
        map.put(Cons.KEY_TaskStatus, RequestBody.create(MediaType.parse("text/plain"), status));
        map.put(Cons.KEY_TaskPriority, RequestBody.create(MediaType.parse("text/plain"), priority));
        Call<ResponseBody> response;
        response = apiInterface.editTaskDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Task_Edit" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            getTaskDetailed(taskId);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_21", "" + e.getMessage());
                    } catch (IOException e) {
                        Log.d("TAG_err_22", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_23", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_24", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void deleteObject() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), "Task"));
        map.put(Cons.KEY_What, RequestBody.create(MediaType.parse("text/plain"), taskId));
        Call<ResponseBody> response = apiInterface.deleteObject(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_File_Create" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            clickGoToRecent.callbackGoToRecentTask(getString(R.string.task));
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void setMarkCompleted() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_TaskID, RequestBody.create(MediaType.parse("text/plain"), taskId));
        Call<ResponseBody> response = apiInterface.setMarkCompleted(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_MarkComplete" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        Utl.showToast(getActivity(), strMessage);
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    @Override
    public void callbackAddAssignedToListen(int pos, ModelSpinner model) {
        phvAssignedToAdd.removeAllViews();
        layoutAssignedToAdd.setVisibility(View.GONE);
        strAssignedToNew = model.getId();
        strAssignedToNameNew = model.getName();
        edittextAssignedTo.setText(strAssignedToNameNew);
    }

    public boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), CAMERA);
        int result2 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION);
        int result3 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_COARSE_LOCATION);
        int result4 = ContextCompat.checkSelfPermission(getActivity(), CALL_PHONE);
        int result5 = ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE);
        int result6 = ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE);
        return
                result1 == PackageManager.PERMISSION_GRANTED
                        && result2 == PackageManager.PERMISSION_GRANTED
                        && result3 == PackageManager.PERMISSION_GRANTED
                        && result4 == PackageManager.PERMISSION_GRANTED
                        && result5 == PackageManager.PERMISSION_GRANTED
                        && result6 == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{
                        CAMERA,
                        ACCESS_FINE_LOCATION,
                        ACCESS_COARSE_LOCATION,
                        CALL_PHONE,
                        READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean locationFineAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean locationCoarseAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean callAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean readAccepted = grantResults[4] == PackageManager.PERMISSION_GRANTED;
                    boolean writeAccepted = grantResults[5] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted &&
                            locationFineAccepted &&
                            locationCoarseAccepted &&
                            callAccepted &&
                            readAccepted &&
                            writeAccepted)
                        Utl.showToast(getActivity(), "Permission Granted.");
                    else {
                        Utl.showToast(getActivity(), "Permission Denied.");
                    }
                }
                break;
        }
    }

}