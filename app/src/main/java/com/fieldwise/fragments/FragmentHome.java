package com.fieldwise.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.activities.ActivityLogin;
import com.fieldwise.activities.ActivityMain;
import com.fieldwise.adapters.AdapterHomeTask;
import com.fieldwise.adapters.AdapterRecent;
import com.fieldwise.adapters.AdapterSchedule;
import com.fieldwise.models.ModelHomeRecent;
import com.fieldwise.models.ModelHomeSchedule;
import com.fieldwise.models.ModelHomeTask;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;
import com.mindorks.placeholderview.PlaceHolderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentHome extends Fragment implements AdapterRecent.ClickListenHomeItem {

    private String[] strItemsTasks = {"Today", "Today + Overdue", "Overdue"};
    private ArrayAdapter adapterTasks;
    private Spinner spinnerTasks;

    private RelativeLayout rvSchedule;
    private List<ModelHomeSchedule> listHomeSchedule;
    private List<ModelHomeTask> listHomeTask;
    private List<ModelHomeRecent> listHomeRecent;
    private RelativeLayout laySchedule, layTask, layRecent;
    private PlaceHolderView phvSchedule, phvTask, phvRecent;
    private TextView tvNoRecordSchedule, tvNoRecordTask, tvNoRecordRecent;

    private ImageView imageDemo;

    public APIInterface apiInterface;

    public ClickListenHomeItem clickHomeItem;

    public interface ClickListenHomeItem {
        void callbackHomeItem(String object, String id, String name);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        rvSchedule = (RelativeLayout) view.findViewById(R.id.line1);
        laySchedule = (RelativeLayout) view.findViewById(R.id.lay_schedule);
        layTask = (RelativeLayout) view.findViewById(R.id.lay_task);
        layRecent = (RelativeLayout) view.findViewById(R.id.lay_recent);
        phvSchedule = (PlaceHolderView) view.findViewById(R.id.phv_schedule);
        phvTask = (PlaceHolderView) view.findViewById(R.id.phv_task);
        phvRecent = (PlaceHolderView) view.findViewById(R.id.phv_recent);
        tvNoRecordSchedule = (TextView) view.findViewById(R.id.tv_no_record_schedule);
        tvNoRecordTask = (TextView) view.findViewById(R.id.tv_no_record_task);
        tvNoRecordRecent = (TextView) view.findViewById(R.id.tv_no_record_recent);

        spinnerTasks = (Spinner) view.findViewById(R.id.spinner_tasks);

        clickHomeItem = (ClickListenHomeItem) getActivity();

        listHomeSchedule = new ArrayList<>();
        listHomeTask = new ArrayList<>();
        listHomeRecent = new ArrayList<>();
        adapterTasks = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, strItemsTasks);
        adapterTasks.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTasks.setAdapter(adapterTasks);

        spinnerTasks.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 0) {
                    getHomeTask("Today");
                } else if (position == 1) {
                    getHomeTask("TodayOverdue");
                } else if (position == 2) {
                    getHomeTask("Overdue");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        getHomeSchedule();
        getHomeTask("Today");
        getHomeRecent();

        return view;

    }

    private void getHomeSchedule() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getHomeSchedule(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_HomeSchedule" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.clearSP(getActivity());
                            ActivityMain.activity.finish();
                            startActivity(new Intent(getActivity(), ActivityLogin.class));
                        } else if (responseCode == 1) {
                            listHomeSchedule.clear();
                            phvSchedule.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelHomeSchedule modelSchedule = new ModelHomeSchedule();
                                modelSchedule.setEventID(dataObject.getString(Cons.KEY_EventID));
                                modelSchedule.setSubject(dataObject.getString(Cons.KEY_Subject));
                                modelSchedule.setAddress(dataObject.getString(Cons.KEY_Address));
                                modelSchedule.setTime(dataObject.getString(Cons.KEY_Time));
                                modelSchedule.setRelatedTo(dataObject.getString(Cons.KEY_RelatedTo));
                                modelSchedule.setRelatedObjID(dataObject.getString(Cons.KEY_RelatedObjID));
                                modelSchedule.setRelatedObjNo(dataObject.getString(Cons.KEY_RelatedObjNo));
                                listHomeSchedule.add(modelSchedule);
                            }
                            if (listHomeSchedule.size() > 5) {
                                ViewGroup.LayoutParams params = laySchedule.getLayoutParams();
                                params.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 350, getResources().getDisplayMetrics());
                                ;
                                laySchedule.setLayoutParams(params);
                            }
                            for (int i = 0; i < listHomeSchedule.size(); i++) {
                                phvSchedule.addView(new AdapterSchedule(getActivity(), listHomeSchedule.get(i)));
                            }
                            if (listHomeSchedule.isEmpty()) {
                                tvNoRecordSchedule.setVisibility(View.VISIBLE);
                            } else {
                                tvNoRecordSchedule.setVisibility(View.GONE);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getHomeTask(String filter) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_Filter, RequestBody.create(MediaType.parse("text/plain"), filter));
        Call<ResponseBody> response = apiInterface.getHomeTask(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_HomeTask" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            //Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listHomeTask.clear();
                            phvTask.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelHomeTask model = new ModelHomeTask();
                                model.setTaskID(dataObject.getString(Cons.KEY_TaskID));
                                model.setSubject(dataObject.getString(Cons.KEY_Subject));
                                model.setDate(dataObject.getString(Cons.KEY_Date));
                                model.setRelatedTo(dataObject.getString(Cons.KEY_RelatedTo));
                                model.setRelatedObjID(dataObject.getString(Cons.KEY_RelatedObjID));
                                model.setRelatedObjNo(dataObject.getString(Cons.KEY_RelatedObjNo));
                                listHomeTask.add(model);
                            }
                            if (listHomeTask.size() > 5) {
                                ViewGroup.LayoutParams params = layTask.getLayoutParams();
                                params.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 350, getResources().getDisplayMetrics());
                                ;
                                layTask.setLayoutParams(params);
                            }
                            for (int i = 0; i < listHomeTask.size(); i++) {
                                phvTask.addView(new AdapterHomeTask(getActivity(), listHomeTask.get(i)));
                            }
                            if (listHomeTask.isEmpty()) {
                                tvNoRecordTask.setVisibility(View.VISIBLE);
                            } else {
                                tvNoRecordTask.setVisibility(View.GONE);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getHomeRecent() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getHomeRecent(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_HomeRecent" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            //Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listHomeRecent.clear();
                            phvRecent.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelHomeRecent model = new ModelHomeRecent();
                                model.setID(dataObject.getString(Cons.KEY_ID));
                                model.setObject(dataObject.getString(Cons.KEY_Object));
                                model.setName(dataObject.getString(Cons.KEY_Name));
                                model.setDate(dataObject.getString(Cons.KEY_Date));
                                listHomeRecent.add(model);
                            }
                            if (listHomeRecent.size() > 5) {
                                ViewGroup.LayoutParams params = layRecent.getLayoutParams();
                                params.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 350, getResources().getDisplayMetrics());
                                ;
                                layRecent.setLayoutParams(params);
                            }
                            for (int i = 0; i < listHomeRecent.size(); i++) {
                                phvRecent.addView(new AdapterRecent(getActivity(), FragmentHome.this, listHomeRecent.get(i)));
                            }
                            if (listHomeRecent.isEmpty()) {
                                tvNoRecordRecent.setVisibility(View.VISIBLE);
                            } else {
                                tvNoRecordRecent.setVisibility(View.GONE);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    @Override
    public void callbackHomeItem(String object, String id, String name) {
        if (clickHomeItem != null) {
            clickHomeItem.callbackHomeItem(object, id, name);
        }
    }

}