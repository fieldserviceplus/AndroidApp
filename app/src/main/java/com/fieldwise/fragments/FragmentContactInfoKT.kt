package com.fieldwise.fragments

import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Criteria
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.CheckBox
import android.widget.DatePicker
import android.widget.EditText
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.RelativeLayout
import android.widget.ScrollView
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast

import com.fieldwise.R
import com.fieldwise.adapters.AdapterAccount
import com.fieldwise.adapters.AdapterAssignedTo
import com.fieldwise.adapters.AdapterContactAddItem
import com.fieldwise.adapters.AdapterContactRelatedChild
import com.fieldwise.adapters.AdapterContactRelatedParent
import com.fieldwise.adapters.AdapterContactViewAll
import com.fieldwise.models.ModelEstimate
import com.fieldwise.models.ModelEvent
import com.fieldwise.models.ModelFile
import com.fieldwise.models.ModelInvoice
import com.fieldwise.models.ModelNote
import com.fieldwise.models.ModelSpinner
import com.fieldwise.models.ModelTask
import com.fieldwise.models.ModelWorkOrder
import com.fieldwise.utils.APIClient
import com.fieldwise.utils.APIInterface
import com.fieldwise.utils.Cons
import com.fieldwise.utils.Utl
import com.mindorks.placeholderview.ExpandablePlaceHolderView
import com.mindorks.placeholderview.PlaceHolderView
import com.schibstedspain.leku.LocationPickerActivity

import org.json.JSONException
import org.json.JSONObject

import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Date
import java.util.HashMap
import java.util.Locale

import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.Manifest.permission.CALL_PHONE
import android.app.Dialog
import android.net.Uri
import android.view.*

class FragmentContactInfoKT : Fragment(), LocationListener,
        AdapterContactViewAll.ViewAllListen,
        AdapterContactAddItem.AddItemListen,
        AdapterContactRelatedChild.ClickListen,
        AdapterAssignedTo.AddAssignedToClickListen,
        AdapterAccount.AddAccountClickListen,
        AdapterContactRelatedChild.ClickListenNote {

    var apiInterface = APIClient.getClient().create(APIInterface::class.java)

    private var strLat = "40.730610"
    private var strLng = "-73.935242"
    private var provider: String? = null
    private var locationManager: LocationManager? = null

    private var isAssignedToClicked = false
    private var isAccountClicked = false
    private var isBirthDayClicked = false
    private var isMilClicked = false

    private var radioGroupMain: RadioGroup? = null
    private var radioButtonDetails: RadioButton? = null
    private var radioButtonRelated: RadioButton? = null

    private var layoutRecentContactDetails: ScrollView? = null
    private var phvRecentContactsRelated: ExpandablePlaceHolderView? = null

    private var layoutAssignedTo: RelativeLayout? = null
    private var layoutAccount: RelativeLayout? = null
    private var layoutTitle: RelativeLayout? = null
    private var layoutLeadSource: RelativeLayout? = null
    private var layoutSalutation: LinearLayout? = null
    private var layoutSystemInfo: LinearLayout? = null

    private var spinnerTitle: Spinner? = null
    private var spinnerLeadSource: Spinner? = null
    private var spinnerSalutation: Spinner? = null

    private var listAssignedTo: MutableList<ModelSpinner>? = null
    private var listAccount: MutableList<ModelSpinner>? = null
    private var listTitle: MutableList<ModelSpinner>? = null
    private var listLeadSource: MutableList<ModelSpinner>? = null
    private var listSalutation: MutableList<ModelSpinner>? = null

    private var adapterTitle: ArrayAdapter<ModelSpinner>? = null
    private var adapterLeadSource: ArrayAdapter<ModelSpinner>? = null
    private var adapterSalutation: ArrayAdapter<ModelSpinner>? = null

    private var contactId: String? = null
    private var strContactID: String? = null
    private var strContactName: String? = null
    private var strContactNo: String? = null
    private var strAssignedToId: String? = null
    private var strAssignedToName: String? = null
    private var strAssignedToIdNew: String? = null
    private var strAssignedToNameNew: String? = null
    private var strAccountId: String? = null
    private var strAccountIdNew: String? = null
    private var strAccountName: String? = null
    private var strAccountNameNew: String? = null
    private var strName: String? = null
    private var strSalutation: String? = null
    private var strFirstName: String? = null
    private var strLastName: String? = null
    private var strTitle: String? = null
    private var strLeadSource: String? = null
    private var strLeadSourceName: String? = null
    private var strBirthDate: String? = null
    private var strBirthDateNew: String? = null
    private var strEmail: String? = null
    private var strPhoneNo: String? = null
    private var strMobileNo: String? = null
    private var strNotes: String? = null
    private var strIsEmailOptOut: String? = null
    private var strIsDeleted: String? = null
    private var strIsDoNotCall: String? = null
    private var strIsActive: String? = null
    private var strMilLat: String? = null
    private var strMilLng: String? = null
    private var strMilLatNew: String? = null
    private var strMilLngNew: String? = null
    private var strMilAddress: String? = null
    private var strMilCity: String? = null
    private var strMilState: String? = null
    private var strMilCountry: String? = null
    private var strMilPostal: String? = null
    private var strMilAddressNew: String? = null
    private var strMilCityNew: String? = null
    private var strMilStateNew: String? = null
    private var strMilCountryNew: String? = null
    private var strMilPostalNew: String? = null
    private var strCreatedDate: String? = null
    private var strCreatedBy: String? = null
    private var strLastModifiedDate: String? = null
    private var strLastModifiedBy: String? = null

    private var edittextAssignedToName: EditText? = null
    private var edittextAccountName: EditText? = null
    private var edittextFirstname: EditText? = null
    private var edittextLastname: EditText? = null
    private var edittextEmail: EditText? = null
    private var edittextPhone: EditText? = null
    private var edittextMobile: EditText? = null
    private var edittextNotes: EditText? = null
    private var edittextMilAddress: EditText? = null
    private var edittextMilCity: EditText? = null
    private var edittextMilState: EditText? = null
    private var edittextMilCountry: EditText? = null
    private var edittextMilPostal: EditText? = null

    private var textviewHeaderLeadSource: TextView? = null
    private var textviewHeaderPhone: TextView? = null
    private var textviewAssignedToName: TextView? = null
    private var textviewAccountName: TextView? = null
    private var textViewName: TextView? = null
    private var textviewTitle: TextView? = null
    private var textviewLeadSource: TextView? = null
    private var textviewEmail: TextView? = null
    private var textviewPhone: TextView? = null
    private var textviewMobile: TextView? = null
    private var textviewNotes: TextView? = null
    private var textviewBirthDate: TextView? = null
    private var edittextBirthDate: TextView? = null
    private var textviewMilAddress: TextView? = null
    private var textviewMilCity: TextView? = null
    private var textviewMilState: TextView? = null
    private var textviewMilCountry: TextView? = null
    private var textviewMilPostal: TextView? = null
    private var textviewCreatedDate: TextView? = null
    private var textviewCreatedBy: TextView? = null
    private var textviewLastModifiedDate: TextView? = null
    private var textviewLastModifiedBy: TextView? = null

    private var checkboxIsDoNotCall: CheckBox? = null
    private var checkboxIsActive: CheckBox? = null
    private var buttonMilAddress: ImageButton? = null

    private var buttonAssignedTo: ImageButton? = null
    private var layoutAssignedToAdd: LinearLayout? = null
    private var edittextAssignedToAddSearch: EditText? = null
    private var phvAssignedToAdd: PlaceHolderView? = null

    private var buttonAccount: ImageButton? = null
    private var layoutAccountAdd: LinearLayout? = null
    private var edittextAccountAddSearch: EditText? = null
    private var phvAccountAdd: PlaceHolderView? = null

    private var layoutNavigation: LinearLayout? = null
    private var layoutSaveCancel: LinearLayout? = null
    private var buttonCall: ImageButton? = null
    private var buttonComment: ImageButton? = null
    private var buttonDate: ImageButton? = null
    private var buttonEdit: ImageButton? = null
    private var buttonMore: ImageButton? = null
    private var buttonSave: Button? = null
    private var buttonCancel: Button? = null

    private var listRelatedContactList: MutableList<String>? = null
    private var listRelatedContactWorkOrders: MutableList<ModelWorkOrder>? = null
    private var listRelatedContactEvents: MutableList<ModelEvent>? = null
    private var listRelatedContactEstimates: MutableList<ModelEstimate>? = null
    private var listRelatedContactInvoices: MutableList<ModelInvoice>? = null
    private var listRelatedContactFiles: MutableList<ModelFile>? = null
    private var listRelatedContactNotes: MutableList<ModelNote>? = null
    private var listRelatedContactTasks: MutableList<ModelTask>? = null

    var click: ContactViewAllListen? = null

    interface ContactViewAllListen {
        fun callbackContactViewAllListen(str: String, accountId: String?)
    }

    var clickContactRelatedItemWorkOrderOpen: ClickListenContactRelatedItemWorkOrderOpen? = null

    interface ClickListenContactRelatedItemWorkOrderOpen {
        fun callbackContactRelatedWorkOrderOpen(contactID: String, contactNo: String)
    }

    var clickCrateFile: CreateFileListener? = null

    interface CreateFileListener {
        fun callbackContactCreateFile(strObject: String)
    }

    var clickCrateEvent: CreateEventListener? = null

    interface CreateEventListener {
        fun callbackContactCreateEvent(strWhoName: String, strWhatID: String, strWhatName: String)
    }

    var clickSendEmail: SendEmailListener? = null

    interface SendEmailListener {
        fun callbackContactSendEmail(what: String, header: String)
    }

    var clickGoToRecent: GoToRecent? = null

    interface GoToRecent {
        fun callbackGoToRecentContact(strObject: String)
    }

    //Create Interface

    var clickWorkOrderCreate: ClickListenWorkOrderCreate? = null

    interface ClickListenWorkOrderCreate {
        fun callbackContactWorkOrderCreate()
    }

    var clickEstimateCreate: ClickListenEstimateCreate? = null

    interface ClickListenEstimateCreate {
        fun callbackContactEstimateCreate()
    }

    var clickInvoiceCreate: ClickListenInvoiceCreate? = null

    interface ClickListenInvoiceCreate {
        fun callbackContactInvoiceCreate()
    }

    var clickTaskCreate: ClickListenTaskCreate? = null

    interface ClickListenTaskCreate {
        fun callbackContactTaskCreate()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_contact_detailed, container, false)

        apiInterface = APIClient.getClient().create(APIInterface::class.java)

        click = activity as ContactViewAllListen?
        clickContactRelatedItemWorkOrderOpen = activity as ClickListenContactRelatedItemWorkOrderOpen?
        clickCrateFile = activity as CreateFileListener?
        clickCrateEvent = activity as CreateEventListener?
        clickSendEmail = activity as SendEmailListener?
        clickGoToRecent = activity as GoToRecent?

        clickWorkOrderCreate = activity as ClickListenWorkOrderCreate?
        clickEstimateCreate = activity as ClickListenEstimateCreate?
        clickInvoiceCreate = activity as ClickListenInvoiceCreate?
        clickTaskCreate = activity as ClickListenTaskCreate?

        radioGroupMain = view.findViewById<View>(R.id.radio_group_main) as RadioGroup
        radioButtonDetails = view.findViewById<View>(R.id.radio_button_details) as RadioButton
        radioButtonRelated = view.findViewById<View>(R.id.radio_button_related) as RadioButton

        layoutRecentContactDetails = view.findViewById<View>(R.id.layout_recent_contact_details) as ScrollView
        phvRecentContactsRelated = view.findViewById<View>(R.id.phv_recent_contact_related) as ExpandablePlaceHolderView

        layoutAssignedTo = view.findViewById<View>(R.id.layout_assigned_to) as RelativeLayout
        layoutAccount = view.findViewById<View>(R.id.layout_account) as RelativeLayout
        layoutTitle = view.findViewById<View>(R.id.layout_title) as RelativeLayout
        layoutLeadSource = view.findViewById<View>(R.id.layout_lead_source) as RelativeLayout
        layoutSalutation = view.findViewById<View>(R.id.layout_salutation) as LinearLayout

        spinnerTitle = view.findViewById<View>(R.id.spinner_title) as Spinner
        spinnerLeadSource = view.findViewById<View>(R.id.spinner_lead_source) as Spinner
        spinnerSalutation = view.findViewById<View>(R.id.spinner_salutation) as Spinner

        edittextAssignedToName = view.findViewById<View>(R.id.edittext_assigned_to_name) as EditText
        edittextAccountName = view.findViewById<View>(R.id.edittext_account_name) as EditText
        edittextFirstname = view.findViewById<View>(R.id.edittext_firstname) as EditText
        edittextLastname = view.findViewById<View>(R.id.edittext_lastname) as EditText
        edittextEmail = view.findViewById<View>(R.id.edittext_email) as EditText
        edittextPhone = view.findViewById<View>(R.id.edittext_phone) as EditText
        edittextMobile = view.findViewById<View>(R.id.edittext_mobile) as EditText
        edittextNotes = view.findViewById<View>(R.id.edittext_notes) as EditText
        edittextMilAddress = view.findViewById<View>(R.id.edittext_mil_address) as EditText
        edittextMilCity = view.findViewById<View>(R.id.edittext_mil_city) as EditText
        edittextMilState = view.findViewById<View>(R.id.edittext_mil_state) as EditText
        edittextMilCountry = view.findViewById<View>(R.id.edittext_mil_country) as EditText
        edittextMilPostal = view.findViewById<View>(R.id.edittext_mil_postal) as EditText

        textviewHeaderLeadSource = view.findViewById<View>(R.id.textview_header_lead_source) as TextView
        textviewHeaderPhone = view.findViewById<View>(R.id.textview_header_phone) as TextView
        textviewAssignedToName = view.findViewById<View>(R.id.textview_assigned_to_name) as TextView
        textviewAccountName = view.findViewById<View>(R.id.textview_account_name) as TextView
        textViewName = view.findViewById<View>(R.id.textview_name) as TextView
        textviewTitle = view.findViewById<View>(R.id.textview_title) as TextView
        textviewLeadSource = view.findViewById<View>(R.id.textview_lead_source) as TextView
        textviewEmail = view.findViewById<View>(R.id.textview_email) as TextView
        textviewPhone = view.findViewById<View>(R.id.textview_phone) as TextView
        textviewMobile = view.findViewById<View>(R.id.textview_mobile) as TextView
        textviewNotes = view.findViewById<View>(R.id.textview_notes) as TextView
        textviewBirthDate = view.findViewById<View>(R.id.textview_birth_date) as TextView
        edittextBirthDate = view.findViewById<View>(R.id.edittext_birth_date) as TextView
        textviewMilAddress = view.findViewById<View>(R.id.textview_mil_address) as TextView
        textviewMilCity = view.findViewById<View>(R.id.textview_mil_city) as TextView
        textviewMilState = view.findViewById<View>(R.id.textview_mil_state) as TextView
        textviewMilCountry = view.findViewById<View>(R.id.textview_mil_country) as TextView
        textviewMilPostal = view.findViewById<View>(R.id.textview_mil_postal) as TextView
        textviewCreatedDate = view.findViewById<View>(R.id.textview_created_date) as TextView
        textviewCreatedBy = view.findViewById<View>(R.id.textview_created_by) as TextView
        textviewLastModifiedDate = view.findViewById<View>(R.id.textview_last_modified_date) as TextView
        textviewLastModifiedBy = view.findViewById<View>(R.id.textview_last_modified_by) as TextView

        checkboxIsDoNotCall = view.findViewById<View>(R.id.checkbox_is_do_not_call) as CheckBox
        checkboxIsActive = view.findViewById<View>(R.id.checkbox_is_active) as CheckBox
        buttonMilAddress = view.findViewById<View>(R.id.button_mil_address) as ImageButton

        layoutSystemInfo = view.findViewById<View>(R.id.layout_system_info) as LinearLayout
        textviewCreatedDate = view.findViewById<View>(R.id.textview_created_date) as TextView
        textviewCreatedBy = view.findViewById<View>(R.id.textview_created_by) as TextView
        textviewLastModifiedDate = view.findViewById<View>(R.id.textview_last_modified_date) as TextView
        textviewLastModifiedBy = view.findViewById<View>(R.id.textview_last_modified_by) as TextView

        layoutNavigation = view.findViewById<View>(R.id.layout_navigation) as LinearLayout
        buttonCall = view.findViewById<View>(R.id.button_call) as ImageButton
        buttonComment = view.findViewById<View>(R.id.button_comment) as ImageButton
        buttonDate = view.findViewById<View>(R.id.button_date) as ImageButton
        buttonEdit = view.findViewById<View>(R.id.button_edit) as ImageButton
        buttonMore = view.findViewById<View>(R.id.button_more) as ImageButton

        layoutSaveCancel = view.findViewById<View>(R.id.layout_save_cancel) as LinearLayout
        buttonSave = view.findViewById<View>(R.id.button_save) as Button
        buttonCancel = view.findViewById<View>(R.id.button_cancel) as Button

        buttonAssignedTo = view.findViewById<View>(R.id.button_assigned_to) as ImageButton
        layoutAssignedToAdd = view.findViewById<View>(R.id.layout_assigned_to_add) as LinearLayout
        edittextAssignedToAddSearch = view.findViewById<View>(R.id.edittext_assigned_to_add_search) as EditText
        phvAssignedToAdd = view.findViewById<View>(R.id.phv_assigned_to_add) as PlaceHolderView

        buttonAccount = view.findViewById<View>(R.id.button_account) as ImageButton
        layoutAccountAdd = view.findViewById<View>(R.id.layout_account_add) as LinearLayout
        edittextAccountAddSearch = view.findViewById<View>(R.id.edittext_account_add_search) as EditText
        phvAccountAdd = view.findViewById<View>(R.id.phv_account_add) as PlaceHolderView

        strMilLatNew = "0"
        strMilLngNew = "0"
        strMilAddressNew = ""
        strMilCityNew = ""
        strMilStateNew = ""
        strMilCountryNew = ""
        strMilPostalNew = ""

        listAssignedTo = ArrayList()
        listAccount = ArrayList()
        listTitle = ArrayList()
        listLeadSource = ArrayList()
        listSalutation = ArrayList()
        adapterTitle = ArrayAdapter(activity!!, android.R.layout.simple_spinner_dropdown_item, listTitle!!)
        adapterTitle!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        adapterLeadSource = ArrayAdapter(activity!!, android.R.layout.simple_spinner_dropdown_item, listLeadSource!!)
        adapterLeadSource!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        adapterSalutation = ArrayAdapter(activity!!, android.R.layout.simple_spinner_dropdown_item, listSalutation!!)
        adapterSalutation!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        radioButtonDetails!!.isChecked = true
        radioButtonRelated!!.isChecked = false
        layoutRecentContactDetails!!.visibility = View.VISIBLE
        phvRecentContactsRelated!!.visibility = View.GONE

        editMode(false, false)

        contactId = arguments!!.getString(Cons.KEY_ContactID)
        getContactDetailed(contactId)

        getAssignedTo()
        getAccount()
        getTitle()
        getLeadSource()
        getSalutation()

        listRelatedContactList = ArrayList()
        listRelatedContactWorkOrders = ArrayList()
        listRelatedContactEvents = ArrayList()
        listRelatedContactEstimates = ArrayList()
        listRelatedContactInvoices = ArrayList()
        listRelatedContactFiles = ArrayList()
        listRelatedContactNotes = ArrayList()
        listRelatedContactTasks = ArrayList()
        getRelatedContactList(contactId)
        getRelatedContactWorkOrders(contactId)
        getRelatedContactEvents(contactId)
        getRelatedContactEstimates(contactId)
        getRelatedContactInvoices(contactId)
        getRelatedContactFiles(contactId)
        getRelatedContactNotes(contactId)
        getRelatedContactTasks(contactId)

        radioGroupMain!!.setOnCheckedChangeListener { group, checkedId ->
            if (checkedId == R.id.radio_button_details) {
                buttonCall!!.isEnabled = true
                buttonComment!!.isEnabled = true
                buttonDate!!.isEnabled = true
                buttonEdit!!.isEnabled = true
                layoutRecentContactDetails!!.visibility = View.VISIBLE
                phvRecentContactsRelated!!.visibility = View.GONE
            } else if (checkedId == R.id.radio_button_related) {
                buttonCall!!.isEnabled = false
                buttonComment!!.isEnabled = false
                buttonDate!!.isEnabled = false
                buttonEdit!!.isEnabled = false
                layoutRecentContactDetails!!.visibility = View.GONE
                phvRecentContactsRelated!!.visibility = View.VISIBLE
                phvRecentContactsRelated!!.removeAllViews()

                for (i in listRelatedContactList!!.indices) {
                    phvRecentContactsRelated!!
                            .addView(AdapterContactRelatedParent(activity, listRelatedContactList!![i]))
                }

                val lengthWorkOrder: Int
                if (listRelatedContactWorkOrders!!.size > 3) {
                    lengthWorkOrder = 3
                } else {
                    lengthWorkOrder = listRelatedContactWorkOrders!!.size
                }
                for (i in 0 until lengthWorkOrder) {
                    phvRecentContactsRelated!!
                            .addChildView(0, AdapterContactRelatedChild(activity, this@FragmentContactInfoKT,
                                    listRelatedContactWorkOrders!![i].workOrderNo, listRelatedContactWorkOrders!![i].workOrderID,
                                    "Order: " +
                                            listRelatedContactWorkOrders!![i].workOrderNo + "\n" +
                                            "Subject: " +
                                            listRelatedContactWorkOrders!![i].subject + "\n" +
                                            "Category: " +
                                            listRelatedContactWorkOrders!![i].categoryName + "\n" +
                                            "Priority: " +
                                            listRelatedContactWorkOrders!![i].priority + "\n" +
                                            "Status: " +
                                            listRelatedContactWorkOrders!![i].status))
                }
                if (listRelatedContactWorkOrders!!.size > 3) {
                    phvRecentContactsRelated!!.addChildView(0, AdapterContactViewAll(activity, this@FragmentContactInfoKT, getString(R.string.work_order)))
                } else if (listRelatedContactWorkOrders!!.size == 0) {
                    phvRecentContactsRelated!!.addChildView(0, AdapterContactAddItem(activity, this@FragmentContactInfoKT, getString(R.string.work_order)))
                }

                val lengthEvents: Int
                if (listRelatedContactEvents!!.size > 3) {
                    lengthEvents = 3
                } else {
                    lengthEvents = listRelatedContactEvents!!.size
                }
                for (i in 0 until lengthEvents) {
                    phvRecentContactsRelated!!
                            .addChildView(1, AdapterContactRelatedChild(activity, this@FragmentContactInfoKT, getString(R.string.event), listRelatedContactEvents!![i].eventID,
                                    "Name: " +
                                            listRelatedContactEvents!![i].name + "\n" +
                                            "Assigned To: " +
                                            listRelatedContactEvents!![i].assignedTo + "\n" +
                                            "Subject: " +
                                            listRelatedContactEvents!![i].subject + "\n" +
                                            "Event Start Date: " +
                                            listRelatedContactEvents!![i].eventStartDate + "\n" +
                                            "Event End Date: " +
                                            listRelatedContactEvents!![i].eventEndDate + "\n" +
                                            "Created By: " +
                                            listRelatedContactEvents!![i].createdBy + "\n" +
                                            "Created Date: " +
                                            listRelatedContactEvents!![i].createdDate))
                }
                if (listRelatedContactEvents!!.size > 3) {
                    phvRecentContactsRelated!!.addChildView(1, AdapterContactViewAll(activity, this@FragmentContactInfoKT, getString(R.string.event)))
                } else if (listRelatedContactEvents!!.size == 0) {
                    phvRecentContactsRelated!!.addChildView(1, AdapterContactAddItem(activity, this@FragmentContactInfoKT, getString(R.string.event)))
                }

                val lengthEstimates: Int
                if (listRelatedContactEstimates!!.size > 3) {
                    lengthEstimates = 3
                } else {
                    lengthEstimates = listRelatedContactEstimates!!.size
                }
                for (i in 0 until lengthEstimates) {
                    phvRecentContactsRelated!!
                            .addChildView(2, AdapterContactRelatedChild(activity,
                                    "Owner Name: " +
                                            listRelatedContactEstimates!![i].ownerName + "\n" +
                                            "ET#: " +
                                            listRelatedContactEstimates!![i].estimateNo + "\n" +
                                            "Estimate Name: " +
                                            listRelatedContactEstimates!![i].estimateName + "\n" +
                                            "Status: " +
                                            listRelatedContactEstimates!![i].status + "\n" +
                                            "Grand Total: " +
                                            listRelatedContactEstimates!![i].grandTotal + "\n" +
                                            "Created Date: " +
                                            listRelatedContactEstimates!![i].createdDate + "\n" +
                                            "Expiration Date: " +
                                            listRelatedContactEstimates!![i].expirationDate))
                }
                if (listRelatedContactEstimates!!.size > 3) {
                    phvRecentContactsRelated!!.addChildView(2, AdapterContactViewAll(activity, this@FragmentContactInfoKT, getString(R.string.estimate)))
                } else if (listRelatedContactEstimates!!.size == 0) {
                    phvRecentContactsRelated!!.addChildView(2, AdapterContactAddItem(activity, this@FragmentContactInfoKT, getString(R.string.estimate)))
                }

                val lengthInvoices: Int
                if (listRelatedContactInvoices!!.size > 3) {
                    lengthInvoices = 3
                } else {
                    lengthInvoices = listRelatedContactInvoices!!.size
                }
                for (i in 0 until lengthInvoices) {
                    phvRecentContactsRelated!!
                            .addChildView(3, AdapterContactRelatedChild(activity,
                                    "Invoice Number: " +
                                            listRelatedContactInvoices!![i].invoiceNumber + "\n" +
                                            "Invoice Status: " +
                                            listRelatedContactInvoices!![i].invoiceStatus + "\n" +
                                            "Subject: " +
                                            listRelatedContactInvoices!![i].subject + "\n" +
                                            "Sub Total: " +
                                            listRelatedContactInvoices!![i].subTotal + "\n" +
                                            "Total Price: " +
                                            listRelatedContactInvoices!![i].totalPrice + "\n" +
                                            "Due Date: " +
                                            listRelatedContactInvoices!![i].dueDate
                            ))
                }
                if (listRelatedContactInvoices!!.size > 3) {
                    phvRecentContactsRelated!!.addChildView(3, AdapterContactViewAll(activity, this@FragmentContactInfoKT, getString(R.string.invoice)))
                } else if (listRelatedContactInvoices!!.size == 0) {
                    phvRecentContactsRelated!!.addChildView(3, AdapterContactAddItem(activity, this@FragmentContactInfoKT, getString(R.string.invoice)))
                }

                val lengthFiles: Int
                if (listRelatedContactFiles!!.size > 3) {
                    lengthFiles = 3
                } else {
                    lengthFiles = listRelatedContactFiles!!.size
                }
                for (i in 0 until lengthFiles) {
                    phvRecentContactsRelated!!
                            .addChildView(4, AdapterContactRelatedChild(activity,
                                    "File Name: " +
                                            listRelatedContactFiles!![i].fileName + "\n" +
                                            "Subject: " +
                                            listRelatedContactFiles!![i].subject + "\n" +
                                            "Content Type: " +
                                            listRelatedContactFiles!![i].contentType
                            ))
                }
                if (listRelatedContactFiles!!.size > 3) {
                    phvRecentContactsRelated!!.addChildView(4, AdapterContactViewAll(activity, this@FragmentContactInfoKT, getString(R.string.file)))
                } else if (listRelatedContactFiles!!.size == 0) {
                    phvRecentContactsRelated!!.addChildView(4, AdapterContactAddItem(activity, this@FragmentContactInfoKT, getString(R.string.file)))
                }

                val lengthNotes: Int
                if (listRelatedContactNotes!!.size > 3) {
                    lengthNotes = 3
                } else {
                    lengthNotes = listRelatedContactNotes!!.size
                }
                for (i in 0 until lengthNotes) {
                    phvRecentContactsRelated!!
                            .addChildView(5, AdapterContactRelatedChild(activity, this@FragmentContactInfoKT, getString(R.string.note), listRelatedContactNotes!![i].noteID,
                                    "Subject: " +
                                            listRelatedContactNotes!![i].subject + "\n" +
                                            "Created Date: " +
                                            listRelatedContactNotes!![i].createdDate + "\n" +
                                            "Owner: " +
                                            listRelatedContactNotes!![i].ownerName
                            ))
                }
                if (listRelatedContactNotes!!.size > 3) {
                    phvRecentContactsRelated!!.addChildView(5, AdapterContactViewAll(activity, this@FragmentContactInfoKT, getString(R.string.note)))
                } else if (listRelatedContactNotes!!.size == 0) {
                    phvRecentContactsRelated!!.addChildView(5, AdapterContactAddItem(activity, this@FragmentContactInfoKT, getString(R.string.note)))
                }

                val lengthTasks: Int
                if (listRelatedContactTasks!!.size > 3) {
                    lengthTasks = 3
                } else {
                    lengthTasks = listRelatedContactTasks!!.size
                }
                for (i in 0 until lengthTasks) {
                    phvRecentContactsRelated!!
                            .addChildView(6, AdapterContactRelatedChild(activity,
                                    "Name: " +
                                            listRelatedContactTasks!![i].name + "\n" +
                                            "Assigned To: " +
                                            listRelatedContactTasks!![i].assignedTo + "\n" +
                                            "Subject: " +
                                            listRelatedContactTasks!![i].subject + "\n" +
                                            "Task Type: " +
                                            listRelatedContactTasks!![i].taskType + "\n" +
                                            "Task Status: " +
                                            listRelatedContactTasks!![i].taskStatus + "\n" +
                                            "Priority: " +
                                            listRelatedContactTasks!![i].priority + "\n" +
                                            "Date: " +
                                            listRelatedContactTasks!![i].date
                            ))
                }
                if (listRelatedContactTasks!!.size > 3) {
                    phvRecentContactsRelated!!.addChildView(6, AdapterContactViewAll(activity, this@FragmentContactInfoKT, getString(R.string.task)))
                } else if (listRelatedContactTasks!!.size == 0) {
                    phvRecentContactsRelated!!.addChildView(6, AdapterContactAddItem(activity, this@FragmentContactInfoKT, getString(R.string.task)))
                }
            }
        }

        edittextBirthDate!!.setOnClickListener {
            val strSplit = strBirthDate!!.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val day = Integer.valueOf(strSplit[1])
            val month = Integer.valueOf(strSplit[0]) - 1
            val year = Integer.valueOf(strSplit[2])
            val datePickerDialog = DatePickerDialog(activity!!, DatePickerDialog.OnDateSetListener { datePicker, y, m, d ->
                isBirthDayClicked = true
                edittextBirthDate!!.text = selectDate(d, m, y)
                strBirthDateNew = selectDateEdit(d, m, y)
                Log.d("TAG_BirthDate", strBirthDateNew)
            }, year, month, day)
            datePickerDialog.show()
        }

        buttonMilAddress!!.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!checkPermission()) {
                    requestPermission()
                } else {
                    isMilClicked = true
                    val lat = java.lang.Double.valueOf(strLat)!!
                    val lng = java.lang.Double.valueOf(strLng)!!
                    val locationPickerIntent = LocationPickerActivity.Builder()
                            .withLocation(lat, lng)
                            .withGeolocApiKey(getString(R.string.google_maps_key))
                            .withGooglePlacesEnabled()
                            .withSatelliteViewHidden()
                            .withVoiceSearchHidden()
                            .shouldReturnOkOnBackPressed()
                            .build(activity!!)
                    startActivityForResult(locationPickerIntent, 99)
                }
            } else {
                isMilClicked = true
                val lat = java.lang.Double.valueOf(strLat)!!
                val lng = java.lang.Double.valueOf(strLng)!!
                val locationPickerIntent = LocationPickerActivity.Builder()
                        .withLocation(lat, lng)
                        .withGeolocApiKey(getString(R.string.google_maps_key))
                        .withGooglePlacesEnabled()
                        .withSatelliteViewHidden()
                        .withVoiceSearchHidden()
                        .shouldReturnOkOnBackPressed()
                        .build(activity!!)
                startActivityForResult(locationPickerIntent, 99)
            }
        }

        buttonAssignedTo!!.setOnClickListener {
            layoutAssignedToAdd!!.visibility = View.VISIBLE
            listAssignedTo!!.clear()
            getAssignedTo()
        }

        edittextAssignedToAddSearch!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i1: Int, i2: Int, i3: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i1: Int, i2: Int, i3: Int) {
                val search = charSequence.toString()
                if (!TextUtils.isEmpty(search)) {
                    phvAssignedToAdd!!.removeAllViews()
                    val modle = ArrayList<ModelSpinner>()
                    for (m in listAssignedTo!!) {
                        if (m.name.toLowerCase().contains(search)) {
                            modle.add(m)
                        }
                    }
                    for (i in modle.indices) {
                        phvAssignedToAdd!!
                                .addView(AdapterAssignedTo(activity, this@FragmentContactInfoKT, i, modle[i]))
                    }
                } else {
                    phvAssignedToAdd!!.removeAllViews()
                    for (i in listAssignedTo!!.indices) {
                        phvAssignedToAdd!!
                                .addView(AdapterAssignedTo(activity, this@FragmentContactInfoKT, i, listAssignedTo!![i]))
                    }
                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })

        buttonAccount!!.setOnClickListener {
            layoutAccountAdd!!.visibility = View.VISIBLE
            listAccount!!.clear()
            getAccount()
        }

        edittextAccountAddSearch!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i1: Int, i2: Int, i3: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i1: Int, i2: Int, i3: Int) {
                val search = charSequence.toString()
                if (!TextUtils.isEmpty(search)) {
                    phvAccountAdd!!.removeAllViews()
                    val modle = ArrayList<ModelSpinner>()
                    for (m in listAccount!!) {
                        if (m.name.toLowerCase().contains(search)) {
                            modle.add(m)
                        }
                    }
                    for (i in modle.indices) {
                        phvAccountAdd!!
                                .addView(AdapterAccount(activity, this@FragmentContactInfoKT, i, modle[i]))
                    }
                } else {
                    phvAccountAdd!!.removeAllViews()
                    for (i in listAccount!!.indices) {
                        phvAccountAdd!!
                                .addView(AdapterAccount(activity, this@FragmentContactInfoKT, i, listAccount!![i]))
                    }
                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })

        buttonEdit!!.setOnClickListener {
            radioButtonDetails!!.isEnabled = false
            radioButtonRelated!!.isEnabled = false
            editMode(true, true)
        }

        buttonMore!!.setOnClickListener {
            dialogMore()
        }

        buttonSave!!.setOnClickListener {
            var assignedTo = strAssignedToId
            if (isAssignedToClicked) {
                assignedTo = strAssignedToIdNew
            }
            var account = strAccountId
            if (isAccountClicked) {
                account = strAccountIdNew
            }
            val salutation = listSalutation!![spinnerSalutation!!.selectedItemPosition].name
            val firstName = edittextFirstname!!.text.toString().trim { it <= ' ' }
            val lastName = edittextLastname!!.text.toString().trim { it <= ' ' }
            val title = listTitle!![spinnerTitle!!.selectedItemPosition].name
            val leadSource = listLeadSource!![spinnerLeadSource!!.selectedItemPosition].id
            var birthDate = strBirthDate
            if (isBirthDayClicked) {
                birthDate = strBirthDateNew
            }
            val email = edittextEmail!!.text.toString().trim { it <= ' ' }
            val phone = edittextPhone!!.text.toString().trim { it <= ' ' }
            val mobile = edittextMobile!!.text.toString().trim { it <= ' ' }
            val notes = edittextNotes!!.text.toString().trim { it <= ' ' }
            var isDoNotCall: String? = null
            if (checkboxIsDoNotCall!!.isChecked) {
                isDoNotCall = "1"
            } else {
                isDoNotCall = "0"
            }
            var isActive: String? = null
            if (checkboxIsActive!!.isChecked) {
                isActive = "1"
            } else {
                isActive = "0"
            }
            var milLat = strMilLat
            var milLng = strMilLng
            if (isMilClicked) {
                milLat = strMilLatNew
                milLng = strMilLngNew
            }
            val milAddress = edittextMilAddress!!.text.toString().trim { it <= ' ' }
            val milCity = edittextMilCity!!.text.toString().trim { it <= ' ' }
            val milState = edittextMilState!!.text.toString().trim { it <= ' ' }
            val milCountry = edittextMilCountry!!.text.toString().trim { it <= ' ' }
            val milPostal = edittextMilPostal!!.text.toString().trim { it <= ' ' }

            if (TextUtils.isEmpty(assignedTo)) {
                Utl.showToast(activity, "Select AssignedTo")
            } else if (TextUtils.isEmpty(account)) {
                Utl.showToast(activity, "Select Account")
            } else if (TextUtils.isEmpty(firstName)) {
                Utl.showToast(activity, "Enter First Name")
            } else if (TextUtils.isEmpty(lastName)) {
                Utl.showToast(activity, "Enter Last Name")
            } else if (TextUtils.isEmpty(milAddress)) {
                Utl.showToast(activity, "Enter Mailing Address")
            } else if (TextUtils.isEmpty(milCity)) {
                Utl.showToast(activity, "Enter Mailing City")
            } else if (TextUtils.isEmpty(milState)) {
                Utl.showToast(activity, "Enter Mailing State")
            } else if (TextUtils.isEmpty(milCountry)) {
                Utl.showToast(activity, "Enter Mailing Country")
            } else if (TextUtils.isEmpty(milPostal)) {
                Utl.showToast(activity, "Enter Mailing Postal Code")
            } else {
                editContact(contactId,
                        assignedTo,
                        account,
                        salutation,
                        firstName,
                        lastName,
                        title,
                        leadSource,
                        birthDate,
                        email,
                        phone,
                        mobile,
                        notes,
                        isDoNotCall,
                        isActive,
                        milLat, milLng,
                        milAddress, milCity, milState, milCountry, milPostal)
            }
        }

        buttonCancel!!.setOnClickListener {
            editMode(false, true)
            radioButtonDetails!!.isEnabled = true
            radioButtonRelated!!.isEnabled = true
        }

        locationManager = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val criteria = Criteria()
        provider = locationManager!!.getBestProvider(criteria, false)
        val location = locationManager!!.getLastKnownLocation(provider)
        if (location != null) {
            Log.d("Provider: ", provider!! + " has been selected.")
            onLocationChanged(location)
        }

        buttonCall!!.setOnClickListener(View.OnClickListener {
            if (!checkPermission()) {
                requestPermission()
            } else {
                if (!TextUtils.isEmpty(strPhoneNo)) {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:$strPhoneNo")
                    startActivity(callIntent)
                } else {
                    Utl.showToast(activity, "No Phone Number Available.")
                }
            }
        })

        buttonComment!!.setOnClickListener(View.OnClickListener {
            if (!checkPermission()) {
                requestPermission()
            } else {
                if (!TextUtils.isEmpty(strPhoneNo)) {
                    val smsIntent = Intent(Intent.ACTION_VIEW)
                    smsIntent.data = Uri.parse("sms:$strPhoneNo")
                    startActivity(smsIntent)
                } else {
                    Utl.showToast(activity, "No Phone Number Available.")
                }
            }
        })

        buttonDate!!.setOnClickListener {
            if (clickSendEmail != null) {
                clickSendEmail!!.callbackContactSendEmail(contactId!!, strContactName!!)
            }
        }

        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 99) {
            if (data != null) {
                val latitude = data.getDoubleExtra("latitude", 0.0)
                strMilLat = latitude.toString()
                val longitude = data.getDoubleExtra("longitude", 0.0)
                strMilLng = longitude.toString()
                val geocoder = Geocoder(activity, Locale.getDefault())
                var addresses: List<Address> = ArrayList()
                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1)
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                strMilAddress = addresses[0].subThoroughfare + ", " + addresses[0].thoroughfare
                if (addresses[0].locality != null) {
                    strMilCity = addresses[0].locality
                } else {
                    strMilCity = addresses[0].subLocality
                }
                if (addresses[0].adminArea != null) {
                    strMilState = addresses[0].adminArea
                } else {
                    strMilState = addresses[0].subAdminArea
                }
                strMilCountry = addresses[0].countryName
                strMilPostal = addresses[0].postalCode
                Log.d("TAG_LATITUDE****", strMilLat)
                Log.d("TAG_LONGITUDE****", strMilLng)
                Log.d("TAG_ADDRESS****", strMilAddress)
                Log.d("TAG_CITY****", strMilCity)
                Log.d("TAG_STATE****", strMilState)
                Log.d("TAG_COUNTRY****", strMilCountry)
                Log.d("TAG_POSTAL****", strMilPostal)
                edittextMilAddress!!.setText(strMilAddress)
                edittextMilCity!!.setText(strMilCity)
                edittextMilState!!.setText(strMilState)
                edittextMilCountry!!.setText(strMilCountry)
                edittextMilPostal!!.setText(strMilPostal)
            } else {
                Log.d("RESULT", "CANCELLED")
            }
        }
    }

    override fun onLocationChanged(location: Location) {
        strLat = location.latitude.toString()
        strLng = location.longitude.toString()
        Log.d("TAG_LAT*****", strLat)
        Log.d("TAG_LNG*****", strLng)
    }

    override fun onStatusChanged(s: String, i: Int, bundle: Bundle) {}

    override fun onProviderEnabled(s: String) {
        Toast.makeText(activity, "Enabled New Provider " + provider!!, Toast.LENGTH_SHORT).show()
    }

    override fun onProviderDisabled(s: String) {
        Toast.makeText(activity, "Disabled New Provider " + provider!!, Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()
        locationManager!!.requestLocationUpdates(provider, 400, 1f, this)
    }

    override fun onPause() {
        super.onPause()
        locationManager!!.removeUpdates(this)
    }

    private fun editMode(bolEdit: Boolean, bolResponse: Boolean) {
        if (!bolEdit) {
            textviewHeaderLeadSource!!.visibility = View.VISIBLE
            textviewHeaderPhone!!.visibility = View.VISIBLE

            textviewAssignedToName!!.visibility = View.VISIBLE
            layoutAssignedTo!!.visibility = View.GONE
            textviewAccountName!!.visibility = View.VISIBLE
            layoutAccount!!.visibility = View.GONE
            textViewName!!.visibility = View.VISIBLE
            layoutSalutation!!.visibility = View.GONE
            textviewTitle!!.visibility = View.VISIBLE
            layoutTitle!!.visibility = View.GONE
            spinnerTitle!!.adapter = null
            textviewLeadSource!!.visibility = View.VISIBLE
            layoutLeadSource!!.visibility = View.GONE
            spinnerLeadSource!!.adapter = null
            textviewEmail!!.visibility = View.VISIBLE
            edittextEmail!!.visibility = View.GONE
            textviewPhone!!.visibility = View.VISIBLE
            edittextPhone!!.visibility = View.GONE
            textviewMobile!!.visibility = View.VISIBLE
            edittextMobile!!.visibility = View.GONE
            textviewBirthDate!!.visibility = View.VISIBLE
            edittextBirthDate!!.visibility = View.GONE
            textviewNotes!!.visibility = View.VISIBLE
            edittextNotes!!.visibility = View.GONE

            checkboxIsDoNotCall!!.isEnabled = false
            checkboxIsActive!!.isEnabled = false

            textviewMilAddress!!.visibility = View.VISIBLE
            edittextMilAddress!!.visibility = View.GONE
            textviewMilCity!!.visibility = View.GONE
            edittextMilCity!!.visibility = View.GONE
            textviewMilState!!.visibility = View.GONE
            edittextMilState!!.visibility = View.GONE
            textviewMilCountry!!.visibility = View.GONE
            edittextMilCountry!!.visibility = View.GONE
            textviewMilPostal!!.visibility = View.GONE
            edittextMilPostal!!.visibility = View.GONE
            buttonMilAddress!!.visibility = View.GONE

            layoutSaveCancel!!.visibility = View.GONE
            layoutNavigation!!.visibility = View.VISIBLE
            buttonCall!!.isEnabled = true
            buttonComment!!.isEnabled = true
            buttonDate!!.isEnabled = true
            buttonEdit!!.isEnabled = true

            layoutSystemInfo!!.visibility = View.VISIBLE
            if (bolResponse) {
                setViewInfo()
            }
        } else {
            textviewHeaderLeadSource!!.visibility = View.GONE
            textviewHeaderPhone!!.visibility = View.GONE

            textviewAssignedToName!!.visibility = View.GONE
            layoutAssignedTo!!.visibility = View.VISIBLE
            textviewAccountName!!.visibility = View.GONE
            layoutAccount!!.visibility = View.VISIBLE
            textViewName!!.visibility = View.GONE
            layoutSalutation!!.visibility = View.VISIBLE
            spinnerSalutation!!.adapter = adapterSalutation
            textviewTitle!!.visibility = View.GONE
            layoutTitle!!.visibility = View.VISIBLE
            spinnerTitle!!.adapter = adapterTitle
            textviewLeadSource!!.visibility = View.GONE
            layoutLeadSource!!.visibility = View.VISIBLE
            spinnerLeadSource!!.adapter = adapterLeadSource
            textviewEmail!!.visibility = View.GONE
            edittextEmail!!.visibility = View.VISIBLE
            textviewPhone!!.visibility = View.GONE
            edittextPhone!!.visibility = View.VISIBLE
            textviewMobile!!.visibility = View.GONE
            edittextMobile!!.visibility = View.VISIBLE
            textviewBirthDate!!.visibility = View.GONE
            edittextBirthDate!!.visibility = View.VISIBLE
            textviewNotes!!.visibility = View.GONE
            edittextNotes!!.visibility = View.VISIBLE

            checkboxIsDoNotCall!!.isEnabled = true
            checkboxIsActive!!.isEnabled = true

            textviewMilAddress!!.visibility = View.GONE
            edittextMilAddress!!.visibility = View.VISIBLE
            textviewMilCity!!.visibility = View.VISIBLE
            edittextMilCity!!.visibility = View.VISIBLE
            textviewMilState!!.visibility = View.VISIBLE
            edittextMilState!!.visibility = View.VISIBLE
            textviewMilCountry!!.visibility = View.VISIBLE
            edittextMilCountry!!.visibility = View.VISIBLE
            textviewMilPostal!!.visibility = View.VISIBLE
            edittextMilPostal!!.visibility = View.VISIBLE
            buttonMilAddress!!.visibility = View.VISIBLE

            layoutSaveCancel!!.visibility = View.VISIBLE
            layoutNavigation!!.visibility = View.GONE
            buttonCall!!.isEnabled = false
            buttonComment!!.isEnabled = false
            buttonDate!!.isEnabled = false
            buttonEdit!!.isEnabled = false

            layoutSystemInfo!!.visibility = View.GONE
            if (bolResponse) {
                setEditInfo()
            }
        }
    }

    private fun setViewInfo() {
        textviewHeaderLeadSource!!.text = "Lead Source: " + strLeadSourceName!!
        textviewHeaderPhone!!.text = "Phone: " + strPhoneNo!!

        textviewAssignedToName!!.text = strAssignedToName
        textviewAccountName!!.text = strAccountName
        textViewName!!.text = "$strSalutation $strFirstName $strLastName"
        textviewTitle!!.text = strTitle
        textviewLeadSource!!.text = strLeadSourceName
        textviewBirthDate!!.text = formatDate(strBirthDate)
        textviewEmail!!.text = strEmail
        textviewPhone!!.text = strPhoneNo
        textviewMobile!!.text = strMobileNo
        textviewNotes!!.text = strNotes
        if (strIsDoNotCall == "1") {
            checkboxIsDoNotCall!!.isChecked = true
        } else {
            checkboxIsDoNotCall!!.isChecked = false
        }
        if (strIsActive == "1") {
            checkboxIsActive!!.isChecked = true
        } else {
            checkboxIsActive!!.isChecked = false
        }
        textviewMilAddress!!.text = strMilAddress + "\n" +
                strMilCity + ", " +
                strMilState + ", " +
                strMilPostal + "\n" +
                strMilCountry
        textviewCreatedDate!!.text = strCreatedDate
        textviewCreatedBy!!.text = strCreatedBy
        textviewLastModifiedDate!!.text = strLastModifiedDate
        textviewLastModifiedBy!!.text = strLastModifiedBy
    }

    private fun setEditInfo() {
        edittextAssignedToName!!.setText(strAssignedToName)
        edittextAccountName!!.setText(strAccountName)
        setSpinnerDropDownHeight(listSalutation!!, spinnerSalutation!!)
        val nodelistSalutation = listSalutation
        if (nodelistSalutation != null) {
            spinnerSalutation!!.setSelection(getListIndex(nodelistSalutation, strSalutation))
        }
        setSpinnerDropDownHeight(listTitle!!, spinnerTitle!!)
        edittextFirstname!!.setText(strFirstName)
        edittextLastname!!.setText(strLastName)
        val nodelistTitle = listTitle
        if (nodelistTitle != null) {
            spinnerTitle!!.setSelection(getListIndex(nodelistTitle, strTitle))
        }
        setSpinnerDropDownHeight(listLeadSource!!, spinnerLeadSource!!)
        val nodelistLeadSource = listLeadSource
        if (nodelistLeadSource != null) {
            spinnerLeadSource!!.setSelection(getListIndex(nodelistLeadSource, strLeadSource))
        }
        edittextBirthDate!!.text = formatDate(strBirthDate)
        edittextEmail!!.setText(strEmail)
        edittextPhone!!.setText(strPhoneNo)
        edittextMobile!!.setText(strMobileNo)
        edittextNotes!!.setText(strNotes)
        if (strIsDoNotCall == "1") {
            checkboxIsDoNotCall!!.isChecked = true
        } else {
            checkboxIsDoNotCall!!.isChecked = false
        }
        if (strIsActive == "1") {
            checkboxIsActive!!.isChecked = true
        } else {
            checkboxIsActive!!.isChecked = false
        }
        edittextMilAddress!!.setText(strMilAddress)
        edittextMilCity!!.setText(strMilCity)
        edittextMilState!!.setText(strMilState)
        edittextMilCountry!!.setText(strMilCountry)
        edittextMilPostal!!.setText(strMilPostal)
    }

    private fun setSpinnerDropDownHeight(list: List<ModelSpinner>, spinner: Spinner) {
        val displayMetrics = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        spinner.dropDownWidth = width - 70
        if (list.size > 4) {
            try {
                val popup = Spinner::class.java.getDeclaredField("mPopup")
                popup.isAccessible = true
                val popupWindow = popup.get(spinner) as android.widget.ListPopupWindow
                popupWindow.height = 800
            } catch (e: NoClassDefFoundError) {
            } catch (e: ClassCastException) {
            } catch (e: NoSuchFieldException) {
            } catch (e: IllegalAccessException) {
            }

        }
    }

    fun getListIndex(list: List<ModelSpinner>, name: String?): Int {
        for (i in list.indices) {
            val model = list[i]
            if (name == model.name) {
                return i
            }
        }
        return -1
    }

    fun formatDate(time: String?): String? {
        val inputPattern = "MM/dd/yyyy"
        val outputPattern = "dd MMM yyyy"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return str
    }

    fun selectDate(d: Int, m: Int, y: Int): String? {
        val time = "" + (m + 1) + "/" + d + "/" + y
        val inputPattern = "MM/dd/yyyy"
        val outputPattern = "dd MMM yyyy"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return str
    }

    fun selectDateEdit(d: Int, m: Int, y: Int): String? {
        val time = "" + (m + 1) + "/" + d + "/" + y
        val inputPattern = "MM/dd/yyyy"
        val outputPattern = "dd MMM yyyy"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return str
    }

    private fun dialogMore() {
        val dialogMore = Dialog(activity!!, R.style.DialogFullScreen)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialogMore.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.BOTTOM
        dialogMore.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogMore.setCancelable(true)
        dialogMore.setCanceledOnTouchOutside(true)
        dialogMore.setContentView(R.layout.dialog_more)

        val buttonMoreCall = dialogMore.findViewById<View>(R.id.button_more_call) as TextView
        val buttonMoreText = dialogMore.findViewById<View>(R.id.button_more_text) as TextView
        val buttonMoreEmail = dialogMore.findViewById<View>(R.id.button_more_email) as TextView
        val buttonMoreEdit = dialogMore.findViewById<View>(R.id.button_more_edit) as TextView
        val buttonMoreSign = dialogMore.findViewById<View>(R.id.button_more_sign) as TextView
        val buttonMoreDoc = dialogMore.findViewById<View>(R.id.button_more_doc) as TextView
        val buttonMoreNewLine = dialogMore.findViewById<View>(R.id.button_more_new_line) as TextView
        val buttonMoreChemical = dialogMore.findViewById<View>(R.id.button_more_new_chemical) as TextView
        val buttonMoreEvent = dialogMore.findViewById<View>(R.id.button_more_new_event) as TextView
        val buttonMoreInvoice = dialogMore.findViewById<View>(R.id.button_more_new_invoice) as TextView
        val buttonMoreFile = dialogMore.findViewById<View>(R.id.button_more_new_file) as TextView
        val buttonMoreNote = dialogMore.findViewById<View>(R.id.button_more_new_note) as TextView
        val buttonMoreTask = dialogMore.findViewById<View>(R.id.button_more_new_task) as TextView
        val buttonMoreConvert = dialogMore.findViewById<View>(R.id.button_more_convert) as TextView
        val buttonMoreClone = dialogMore.findViewById<View>(R.id.button_more_clone) as TextView
        val buttonMoreDelete = dialogMore.findViewById<View>(R.id.button_more_delete) as TextView
        val buttomViewCall = dialogMore.findViewById(R.id.view_more_call) as View
        val buttomViewText = dialogMore.findViewById(R.id.view_more_text) as View
        val buttomViewEmail = dialogMore.findViewById(R.id.view_more_email) as View
        val buttomViewEdit = dialogMore.findViewById(R.id.view_more_edit) as View
        val buttomViewSign = dialogMore.findViewById(R.id.view_more_sign) as View
        val buttomViewDoc = dialogMore.findViewById(R.id.view_more_doc) as View
        val buttomViewNewLine = dialogMore.findViewById(R.id.view_more_new_line) as View
        val buttomViewChemical = dialogMore.findViewById(R.id.view_more_new_chemical) as View
        val buttomViewEvent = dialogMore.findViewById(R.id.view_more_new_event) as View
        val buttomViewInvoice = dialogMore.findViewById(R.id.view_more_new_invoice) as View
        val buttomViewFile = dialogMore.findViewById(R.id.view_more_new_file) as View
        val buttomViewNote = dialogMore.findViewById(R.id.view_more_new_note) as View
        val buttomViewTask = dialogMore.findViewById(R.id.view_more_new_task) as View
        val buttomViewConvert = dialogMore.findViewById(R.id.view_more_convert) as View
        val buttomViewClone = dialogMore.findViewById(R.id.view_more_clone) as View
        val buttonMoreClose = dialogMore.findViewById<View>(R.id.button_more_close) as Button

        buttonMoreSign.visibility = View.GONE
        buttomViewSign.visibility = View.GONE
        buttonMoreDoc.visibility = View.GONE
        buttomViewDoc.visibility = View.GONE
        buttonMoreNote.visibility = View.GONE
        buttomViewNote.visibility = View.GONE
        buttonMoreConvert.visibility = View.GONE
        buttomViewConvert.visibility = View.GONE
        buttonMoreNewLine.text = getString(R.string.new_work_order)
        buttonMoreChemical.text = getString(R.string.new_event)
        buttonMoreEvent.text = getString(R.string.new_estimate)
        buttonMoreClone.text = getString(R.string.clone_contact)
        buttonMoreDelete.text = getString(R.string.delete_contact)

        buttonMoreCall.setOnClickListener {
            dialogMore.dismiss()
            if (!checkPermission()) {
                requestPermission()
            } else {
                if (!TextUtils.isEmpty(strPhoneNo)) {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:$strPhoneNo")
                    startActivity(callIntent)
                } else {
                    Utl.showToast(activity, "No Phone Number Available.")
                }
            }
        }

        buttonMoreText.setOnClickListener {
            dialogMore.dismiss()
            if (!checkPermission()) {
                requestPermission()
            } else {
                if (!TextUtils.isEmpty(strPhoneNo)) {
                    val smsIntent = Intent(Intent.ACTION_VIEW)
                    smsIntent.data = Uri.parse("sms:$strPhoneNo")
                    startActivity(smsIntent)
                } else {
                    Utl.showToast(activity, "No Phone Number Available.")
                }
            }
        }

        buttonMoreEmail.setOnClickListener {
            dialogMore.dismiss()
            if (clickSendEmail != null) {
                clickSendEmail!!.callbackContactSendEmail(contactId!!, strContactName!!)
            }
        }

        buttonMoreEdit.setOnClickListener {
            dialogMore.dismiss()
            radioButtonDetails!!.setEnabled(false)
            radioButtonRelated!!.setEnabled(false)
            editMode(true, true)
        }

        buttonMoreNewLine.setOnClickListener {
            dialogMore.dismiss()
            if (clickWorkOrderCreate != null) {
                clickWorkOrderCreate!!.callbackContactWorkOrderCreate()
            }
        }

        buttonMoreChemical.setOnClickListener {
            dialogMore.dismiss()
            if (clickCrateEvent != null) {
                clickCrateEvent!!.callbackContactCreateEvent(getString(R.string.contact), contactId!!, strContactName!!)
            }
        }

        buttonMoreEvent.setOnClickListener {
            dialogMore.dismiss()
            if (clickEstimateCreate != null) {
                clickEstimateCreate!!.callbackContactEstimateCreate()
            }
        }

        buttonMoreInvoice.setOnClickListener {
            dialogMore.dismiss()
            if (clickInvoiceCreate != null) {
                clickInvoiceCreate!!.callbackContactInvoiceCreate()
            }
        }

        buttonMoreFile.setOnClickListener {
            dialogMore.dismiss()
            if (clickCrateFile != null) {
                clickCrateFile!!.callbackContactCreateFile(getString(R.string.contact))
            }
        }

        buttonMoreTask.setOnClickListener {
            dialogMore.dismiss()
            if (clickTaskCreate != null) {
                clickTaskCreate!!.callbackContactTaskCreate()
            }
        }

        buttonMoreDelete.setOnClickListener {
            dialogMore.dismiss()
            deleteObject()
        }

        buttonMoreClose.setOnClickListener {
            dialogMore.dismiss()
        }

        dialogMore.show()
        dialogMore.window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        dialogMore.window!!.setDimAmount(0.5f)
        dialogMore.window!!.attributes = lp
    }

    private fun deleteObject() {
        val dialog = ProgressDialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setMessage(getString(R.string.please_wait))
        dialog.show()
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_RelatedTo] = RequestBody.create(MediaType.parse("text/plain"), "Contact")
        map[Cons.KEY_What] = RequestBody.create(MediaType.parse("text/plain"), contactId)
        val response = apiInterface.deleteObject(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (dialog.isShowing) {
                    dialog.dismiss()
                }
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_File_Create" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            Utl.showToast(activity, strMessage)
                            clickGoToRecent!!.callbackGoToRecentContact(getString(R.string.contact))
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (dialog.isShowing) {
                    dialog.dismiss()
                }
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })

    }

    private fun getContactDetailed(contactId: String?) {
        val dialog = ProgressDialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setMessage(getString(R.string.please_wait))
        dialog.show()
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactID] = RequestBody.create(MediaType.parse("text/plain"), contactId!!)
        val response: Call<ResponseBody>
        response = apiInterface.getContactDetailed(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (dialog.isShowing) {
                    dialog.dismiss()
                }
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_ContactInfo" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            val dataObject = `object`.getJSONObject("data")
                            strContactID = dataObject.getString(Cons.KEY_ContactID)
                            strContactName = dataObject.getString(Cons.KEY_ContactName)
                            strContactNo = dataObject.getString(Cons.KEY_ContactNo)
                            strAssignedToId = dataObject.getString(Cons.KEY_AssignedTo)
                            strAssignedToName = dataObject.getString(Cons.KEY_AssignedToName)
                            strAccountId = dataObject.getString(Cons.KEY_Account)
                            strAccountName = dataObject.getString(Cons.KEY_AccountName)
                            strSalutation = dataObject.getString(Cons.KEY_Salutation)
                            strName = dataObject.getString(Cons.KEY_Name)
                            strFirstName = dataObject.getString(Cons.KEY_FirstName)
                            strLastName = dataObject.getString(Cons.KEY_LastName)
                            strTitle = dataObject.getString(Cons.KEY_Title)
                            strLeadSource = dataObject.getString(Cons.KEY_LeadSource)
                            strLeadSourceName = dataObject.getString(Cons.KEY_LeadSourceName)
                            strBirthDate = dataObject.getString(Cons.KEY_BirthDate)
                            strEmail = dataObject.getString(Cons.KEY_Email)
                            strIsEmailOptOut = dataObject.getString(Cons.KEY_EmailOptOut)
                            strPhoneNo = dataObject.getString(Cons.KEY_PhoneNo)
                            strMobileNo = dataObject.getString(Cons.KEY_MobileNo)
                            strNotes = dataObject.getString(Cons.KEY_Notes)
                            strIsDeleted = dataObject.getString(Cons.KEY_IsDeleted)
                            strIsDoNotCall = dataObject.getString(Cons.KEY_DoNotCall)
                            strIsActive = dataObject.getString(Cons.KEY_IsActive)
                            strMilAddress = dataObject.getString(Cons.KEY_MailingAddress)
                            strMilCity = dataObject.getString(Cons.KEY_MailingCity)
                            strMilState = dataObject.getString(Cons.KEY_MailingState)
                            strMilCountry = dataObject.getString(Cons.KEY_MailingCountry)
                            strMilPostal = dataObject.getString(Cons.KEY_MailingPostalCode)
                            strMilLat = dataObject.getString(Cons.KEY_MailingLatitude)
                            strMilLng = dataObject.getString(Cons.KEY_MailingLongitude)
                            strCreatedDate = dataObject.getString(Cons.KEY_CreatedDate)
                            strCreatedBy = dataObject.getString(Cons.KEY_CreatedBy)
                            strLastModifiedDate = dataObject.getString(Cons.KEY_LastModifiedDate)
                            strLastModifiedBy = dataObject.getString(Cons.KEY_LastModifiedBy)
                            editMode(false, true)
                            radioButtonDetails!!.isEnabled = true
                            radioButtonRelated!!.isEnabled = true
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (dialog.isShowing) {
                    dialog.dismiss()
                }
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun editContact(contactId: String?,
                            assignedTo: String?,
                            account: String?,
                            salutation: String,
                            firstName: String,
                            lastName: String,
                            title: String,
                            leadSource: String,
                            birthDate: String?,
                            email: String,
                            phone: String,
                            mobile: String,
                            notes: String,
                            isDoNotCall: String?,
                            isActive: String?,
                            milLat: String?,
                            milLng: String?,
                            milAddress: String,
                            milCity: String,
                            milState: String,
                            milCountry: String,
                            milPostal: String) {

        val dialog = ProgressDialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setMessage(getString(R.string.please_wait))
        dialog.show()
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactID] = RequestBody.create(MediaType.parse("text/plain"), contactId!!)
        map[Cons.KEY_AssignedTo] = RequestBody.create(MediaType.parse("text/plain"), assignedTo!!)
        map[Cons.KEY_Account] = RequestBody.create(MediaType.parse("text/plain"), account!!)
        map[Cons.KEY_Salutation] = RequestBody.create(MediaType.parse("text/plain"), salutation)
        map[Cons.KEY_FirstName] = RequestBody.create(MediaType.parse("text/plain"), firstName)
        map[Cons.KEY_LastName] = RequestBody.create(MediaType.parse("text/plain"), lastName)
        map[Cons.KEY_Title] = RequestBody.create(MediaType.parse("text/plain"), title)
        map[Cons.KEY_LeadSource] = RequestBody.create(MediaType.parse("text/plain"), leadSource)
        if (!TextUtils.isEmpty(birthDate)) {
            map[Cons.KEY_BirthDate] = RequestBody.create(MediaType.parse("text/plain"), birthDate!!)
        }
        if (!TextUtils.isEmpty(email)) {
            map[Cons.KEY_Email] = RequestBody.create(MediaType.parse("text/plain"), email)
        }
        if (!TextUtils.isEmpty(phone)) {
            map[Cons.KEY_PhoneNo] = RequestBody.create(MediaType.parse("text/plain"), phone)
        }
        if (!TextUtils.isEmpty(mobile)) {
            map[Cons.KEY_MobileNo] = RequestBody.create(MediaType.parse("text/plain"), mobile)
        }
        if (!TextUtils.isEmpty(notes)) {
            map[Cons.KEY_Notes] = RequestBody.create(MediaType.parse("text/plain"), notes)
        }
        map[Cons.KEY_DoNotCall] = RequestBody.create(MediaType.parse("text/plain"), isDoNotCall!!)
        map[Cons.KEY_IsActive] = RequestBody.create(MediaType.parse("text/plain"), isActive!!)
        map[Cons.KEY_MailingLatitude] = RequestBody.create(MediaType.parse("text/plain"), milLat!!)
        map[Cons.KEY_MailingLongitude] = RequestBody.create(MediaType.parse("text/plain"), milLng!!)
        map[Cons.KEY_MailingAddress] = RequestBody.create(MediaType.parse("text/plain"), milAddress)
        map[Cons.KEY_MailingCity] = RequestBody.create(MediaType.parse("text/plain"), milCity)
        map[Cons.KEY_MailingState] = RequestBody.create(MediaType.parse("text/plain"), milState)
        map[Cons.KEY_MailingCountry] = RequestBody.create(MediaType.parse("text/plain"), milCountry)
        map[Cons.KEY_MailingPostalCode] = RequestBody.create(MediaType.parse("text/plain"), milPostal)
        val response: Call<ResponseBody>
        response = apiInterface.editContactDetailed(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (dialog.isShowing) {
                    dialog.dismiss()
                }
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_Contact_Create" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            Utl.showToast(activity, strMessage)
                            getContactDetailed(contactId)
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (dialog.isShowing) {
                    dialog.dismiss()
                }
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getAssignedTo() {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        val response = apiInterface.getContactAssignedTo(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_AllUser" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            listAssignedTo!!.clear()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val model = ModelSpinner()
                                model.id = dataObject.getString(Cons.KEY_UserID)
                                model.name = dataObject.getString(Cons.KEY_FullName)
                                listAssignedTo!!.add(model)
                            }
                            for (i in listAssignedTo!!.indices) {
                                phvAssignedToAdd!!
                                        .addView(AdapterAssignedTo(activity, this@FragmentContactInfoKT, i, listAssignedTo!![i]))
                            }
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getAccount() {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        val response = apiInterface.getContactAccount(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_Account" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            listAccount!!.clear()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val model = ModelSpinner()
                                model.id = dataObject.getString(Cons.KEY_AccountID)
                                model.name = dataObject.getString(Cons.KEY_AccountName)
                                listAccount!!.add(model)
                            }
                            for (i in listAccount!!.indices) {
                                phvAccountAdd!!
                                        .addView(AdapterAccount(activity, this@FragmentContactInfoKT, i, listAccount!![i]))
                            }
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getTitle() {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        val response = apiInterface.getContactTitle(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_ContactTitle" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            listTitle!!.clear()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val model = ModelSpinner()
                                model.id = dataObject.getString(Cons.KEY_TitleOfPeopleID)
                                model.name = dataObject.getString(Cons.KEY_Title)
                                listTitle!!.add(model)
                            }
                            setSpinnerDropDownHeight(listTitle!!, spinnerTitle!!)
                            spinnerTitle!!.adapter = adapterTitle
                            spinnerTitle!!.setSelection(0)
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getLeadSource() {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        val response = apiInterface.getContactLeadSource(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_ContactLeadSour" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            listLeadSource!!.clear()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val model = ModelSpinner()
                                model.id = dataObject.getString(Cons.KEY_LeadSourceID)
                                model.name = dataObject.getString(Cons.KEY_LeadSource)
                                listLeadSource!!.add(model)
                            }
                            setSpinnerDropDownHeight(listLeadSource!!, spinnerLeadSource!!)
                            spinnerLeadSource!!.adapter = adapterLeadSource
                            spinnerLeadSource!!.setSelection(0)
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getSalutation() {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        val response = apiInterface.getContactSalutation(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_ContactSalutati" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            listAssignedTo!!.clear()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val model = ModelSpinner()
                                model.id = dataObject.getString(Cons.KEY_SalutationID)
                                model.name = dataObject.getString(Cons.KEY_Salutation)
                                listSalutation!!.add(model)
                            }
                            setSpinnerDropDownHeight(listSalutation!!, spinnerSalutation!!)
                            spinnerSalutation!!.adapter = adapterSalutation
                            spinnerSalutation!!.setSelection(0)
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getRelatedContactList(contactId: String?) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactID] = RequestBody.create(MediaType.parse("text/plain"), contactId!!)
        val response = apiInterface.getRelatedContactList(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val dataObject = `object`.getJSONObject("data")
                        val WorkOrderObject = dataObject.getJSONObject("WorkOrder")
                        val EventsObject = dataObject.getJSONObject("Event")
                        val EstimatesObject = dataObject.getJSONObject("Estimate")
                        val InvoicesObject = dataObject.getJSONObject("Invoice")
                        val FilesObject = dataObject.getJSONObject("File")
                        val NoteObject = dataObject.getJSONObject("Note")
                        val TasksObject = dataObject.getJSONObject("Task")
                        listRelatedContactList!!.add(WorkOrderObject.getString("title"))
                        listRelatedContactList!!.add(EventsObject.getString("title"))
                        listRelatedContactList!!.add(EstimatesObject.getString("title"))
                        listRelatedContactList!!.add(InvoicesObject.getString("title"))
                        listRelatedContactList!!.add(FilesObject.getString("title"))
                        listRelatedContactList!!.add(NoteObject.getString("title"))
                        listRelatedContactList!!.add(TasksObject.getString("title"))
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getRelatedContactWorkOrders(ContactId: String?) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactID] = RequestBody.create(MediaType.parse("text/plain"), ContactId!!)
        val response = apiInterface.getRelatedContactWorkOrders(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val dataArray = `object`.getJSONArray("data")
                        for (i in 0 until dataArray.length()) {
                            val dataObject = dataArray.getJSONObject(i)
                            val model = ModelWorkOrder()
                            model.workOrderID = dataObject.getString(Cons.KEY_WorkOrderID)
                            model.workOrderNo = dataObject.getString(Cons.KEY_WorkOrderNo)
                            model.subject = dataObject.getString(Cons.KEY_Subject)
                            model.priority = dataObject.getString(Cons.KEY_Priority)
                            model.status = dataObject.getString(Cons.KEY_Status)
                            model.categoryName = dataObject.getString(Cons.KEY_CategoryName)
                            listRelatedContactWorkOrders!!.add(model)
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getRelatedContactEvents(ContactId: String?) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactID] = RequestBody.create(MediaType.parse("text/plain"), ContactId!!)
        val response = apiInterface.getRelatedContactEvents(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val dataArray = `object`.getJSONArray("data")
                        for (i in 0 until dataArray.length()) {
                            val dataObject = dataArray.getJSONObject(i)
                            val model = ModelEvent()
                            model.assignedTo = dataObject.getString(Cons.KEY_AssignedTo)
                            model.createdBy = dataObject.getString(Cons.KEY_CreatedBy)
                            model.createdDate = dataObject.getString(Cons.KEY_CreatedDate)
                            model.eventEndDate = dataObject.getString(Cons.KEY_EventEndDate)
                            model.eventStartDate = dataObject.getString(Cons.KEY_EventStartDate)
                            model.name = dataObject.getString(Cons.KEY_Name)
                            model.subject = dataObject.getString(Cons.KEY_Subject)
                            listRelatedContactEvents!!.add(model)
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getRelatedContactEstimates(ContactId: String?) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactID] = RequestBody.create(MediaType.parse("text/plain"), ContactId!!)
        val response = apiInterface.getRelatedContactEstimates(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val dataArray = `object`.getJSONArray("data")
                        for (i in 0 until dataArray.length()) {
                            val dataObject = dataArray.getJSONObject(i)
                            val model = ModelEstimate()
                            model.createdDate = dataObject.getString(Cons.KEY_CreatedDate)
                            //model.setEstimateID(dataObject.getString(Cons.KEY_EstimateID));
                            model.estimateNo = dataObject.getString(Cons.KEY_EstimateNo)
                            model.estimateName = dataObject.getString(Cons.KEY_EstimateName)
                            model.expirationDate = dataObject.getString(Cons.KEY_ExpirationDate)
                            model.grandTotal = dataObject.getString(Cons.KEY_GrandTotal)
                            model.ownerName = dataObject.getString(Cons.KEY_OwnerName)
                            model.status = dataObject.getString(Cons.KEY_Status)
                            listRelatedContactEstimates!!.add(model)
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getRelatedContactInvoices(ContactId: String?) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactID] = RequestBody.create(MediaType.parse("text/plain"), ContactId!!)
        val response = apiInterface.getRelatedContactInvoices(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val dataArray = `object`.getJSONArray("data")
                        for (i in 0 until dataArray.length()) {
                            val dataObject = dataArray.getJSONObject(i)
                            val model = ModelInvoice()
                            model.dueDate = dataObject.getString(Cons.KEY_DueDate)
                            model.invoiceNumber = dataObject.getString(Cons.KEY_InvoiceNumber)
                            model.invoiceStatus = dataObject.getString(Cons.KEY_InvoiceStatus)
                            model.subject = dataObject.getString(Cons.KEY_Subject)
                            model.subTotal = dataObject.getString(Cons.KEY_SubTotal)
                            model.totalPrice = dataObject.getString(Cons.KEY_TotalPrice)
                            listRelatedContactInvoices!!.add(model)
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getRelatedContactFiles(ContactId: String?) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactID] = RequestBody.create(MediaType.parse("text/plain"), ContactId!!)
        val response = apiInterface.getRelatedContactFiles(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val dataArray = `object`.getJSONArray("data")
                        for (i in 0 until dataArray.length()) {
                            val dataObject = dataArray.getJSONObject(i)
                            val model = ModelFile()
                            model.contentType = dataObject.getString(Cons.KEY_ContentType)
                            model.fileID = dataObject.getString(Cons.KEY_FileID)
                            model.fileName = dataObject.getString(Cons.KEY_FileName)
                            model.subject = dataObject.getString(Cons.KEY_Subject)
                            listRelatedContactFiles!!.add(model)
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getRelatedContactNotes(ContactId: String?) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactID] = RequestBody.create(MediaType.parse("text/plain"), ContactId!!)
        val response = apiInterface.getRelatedContactNote(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val dataArray = `object`.getJSONArray("data")
                        for (i in 0 until dataArray.length()) {
                            val dataObject = dataArray.getJSONObject(i)
                            val model = ModelNote()
                            model.noteID = dataObject.getString(Cons.KEY_NoteID)
                            model.subject = dataObject.getString(Cons.KEY_Subject)
                            model.createdDate = dataObject.getString(Cons.KEY_CreatedDate)
                            model.ownerName = dataObject.getString(Cons.KEY_OwnerName)
                            listRelatedContactNotes!!.add(model)
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getRelatedContactTasks(ContactId: String?) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactID] = RequestBody.create(MediaType.parse("text/plain"), ContactId!!)
        val response = apiInterface.getRelatedContactTasks(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val dataArray = `object`.getJSONArray("data")
                        for (i in 0 until dataArray.length()) {
                            val dataObject = dataArray.getJSONObject(i)
                            val model = ModelTask()
                            model.assignedTo = dataObject.getString(Cons.KEY_AssignedTo)
                            model.date = dataObject.getString(Cons.KEY_Date)
                            model.name = dataObject.getString(Cons.KEY_Name)
                            model.priority = dataObject.getString(Cons.KEY_Priority)
                            model.subject = dataObject.getString(Cons.KEY_Subject)
                            model.taskType = dataObject.getString(Cons.KEY_TaskType)
                            model.taskStatus = dataObject.getString(Cons.KEY_TaskStatus)
                            listRelatedContactTasks!!.add(model)
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(activity!!, ACCESS_FINE_LOCATION)
        val result1 = ContextCompat.checkSelfPermission(activity!!, ACCESS_COARSE_LOCATION)
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED
    }

    fun requestPermission() {
        ActivityCompat.requestPermissions(activity!!, arrayOf(ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION), PERMISSION_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0) {
                val locationFineAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED
                val locationCoarseAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED
                if (locationFineAccepted && locationCoarseAccepted)
                    Utl.showToast(activity, "Permission Granted.");
                else {
                    Utl.showToast(activity, "Permission Denied.");
                }
            }
        }
    }

    override fun callbackAddAssignedToListen(pos: Int, model: ModelSpinner) {
        isAssignedToClicked = true
        phvAssignedToAdd!!.removeAllViews()
        layoutAssignedToAdd!!.visibility = View.GONE
        strAssignedToIdNew = model.id
        strAssignedToNameNew = model.name
        edittextAssignedToName!!.setText(strAssignedToNameNew)
    }

    override fun callbackAddAccountListen(pos: Int, model: ModelSpinner) {
        isAccountClicked = true
        phvAccountAdd!!.removeAllViews()
        layoutAccountAdd!!.visibility = View.GONE
        strAccountIdNew = model.id
        strAccountNameNew = model.name
        edittextAccountName!!.setText(strAccountNameNew)
    }

    override fun callbackContactRelatedWorkOrderOpen(workOrderNo: String, workOrderID: String) {
        if (clickContactRelatedItemWorkOrderOpen != null) {
            clickContactRelatedItemWorkOrderOpen!!.callbackContactRelatedWorkOrderOpen(workOrderID, workOrderNo)
        }
    }

    override fun callbackViewAllListen(str: String) {
        if (click != null) {
            click!!.callbackContactViewAllListen(str, contactId)
        }
    }

    override fun callbackAddItemListen(str: String) {

    }

    companion object {

        private val PERMISSION_REQUEST_CODE = 200
    }

    override fun callbackContactRelatedDetails(whoName: String, whatID: String) {
        if (whoName == getString(R.string.event)) {
            getEventDetails(whatID)
        } else if (whoName == getString(R.string.note)) {
            getNoteDetails(whatID)
        }
    }

    private fun getEventDetails(whatID: String) {

    }

    private fun getNoteDetails(whatID: String) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_NoteID] = RequestBody.create(MediaType.parse("text/plain"), whatID)
        val response = apiInterface.getNoteDetails(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val dataObject = `object`.getJSONObject("data")
                        val owner = dataObject.getString(Cons.KEY_OwnerName)
                        val relatedTo = dataObject.getString(Cons.KEY_RelatedTo)
                        val subject = dataObject.getString(Cons.KEY_Subject)
                        val body = dataObject.getString(Cons.KEY_Body)
                        val createdDate = dataObject.getString(Cons.KEY_CreatedByName)
                        val createdBy = dataObject.getString(Cons.KEY_CreatedDate)
                        val lastModifiedBy = dataObject.getString(Cons.KEY_LastModifiedByName)
                        val lateModifiedDate = dataObject.getString(Cons.KEY_LastModifiedDate)
                        val dialogDetails = Dialog(activity!!, R.style.DialogFullScreen)
                        val lp = WindowManager.LayoutParams()
                        lp.copyFrom(dialogDetails.window!!.attributes)
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT
                        lp.height = WindowManager.LayoutParams.MATCH_PARENT
                        lp.gravity = Gravity.BOTTOM
                        dialogDetails.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        dialogDetails.setCancelable(true)
                        dialogDetails.setCanceledOnTouchOutside(true)
                        dialogDetails.setContentView(R.layout.dialog_note_details)
                        val textviewOwner = dialogDetails.findViewById<View>(R.id.textview_owner) as TextView
                        val textviewRelatedTo = dialogDetails.findViewById<View>(R.id.textview_related_to) as TextView
                        val textviewSubject = dialogDetails.findViewById<View>(R.id.textview_subject) as TextView
                        val textviewBody = dialogDetails.findViewById<View>(R.id.textview_body) as TextView
                        val textviewCreatedDate = dialogDetails.findViewById<View>(R.id.textview_created_date) as TextView
                        val textviewCreatedBy = dialogDetails.findViewById<View>(R.id.textview_created_by) as TextView
                        val textviewLastModifiedDate = dialogDetails.findViewById<View>(R.id.textview_last_modified_date) as TextView
                        val textviewLastModifiedBy = dialogDetails.findViewById<View>(R.id.textview_last_modified_by) as TextView
                        val buttonClose = dialogDetails.findViewById<View>(R.id.button_details_close) as Button
                        textviewOwner.text = owner
                        textviewRelatedTo.text = relatedTo
                        textviewSubject.text = subject
                        textviewBody.text = body
                        textviewCreatedBy.text = createdBy
                        textviewCreatedDate.text = createdDate
                        textviewLastModifiedBy.text = lastModifiedBy
                        textviewLastModifiedDate.text = lateModifiedDate
                        buttonClose.setOnClickListener { dialogDetails.dismiss() }

                        dialogDetails.show()
                        dialogDetails.window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
                        dialogDetails.window!!.setDimAmount(0.5f)
                        dialogDetails.window!!.attributes = lp
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

}