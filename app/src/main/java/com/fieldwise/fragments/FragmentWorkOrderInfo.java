package com.fieldwise.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.ListPopupWindow;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import com.fieldwise.R;
import com.fieldwise.activities.ActivityMain;
import com.fieldwise.adapters.AdapterAccount;
import com.fieldwise.adapters.AdapterChemical;
import com.fieldwise.adapters.AdapterProductChild;
import com.fieldwise.adapters.AdapterProductParent;
import com.fieldwise.adapters.AdapterWorkOrderAddItem;
import com.fieldwise.adapters.AdapterWorkOrderRelatedChild;
import com.fieldwise.adapters.AdapterWorkOrderRelatedParent;
import com.fieldwise.adapters.AdapterWorkOrderViewAll;
import com.fieldwise.models.ModelChemical;
import com.fieldwise.models.ModelProduct;
import com.fieldwise.models.ModelSpinner;
import com.fieldwise.models.ModelWorkOrderChemical;
import com.fieldwise.models.ModelWorkOrderEvent;
import com.fieldwise.models.ModelWorkOrderFile;
import com.fieldwise.models.ModelWorkOrderInvoice;
import com.fieldwise.models.ModelWorkOrderLineItem;
import com.fieldwise.models.ModelWorkOrderNote;
import com.fieldwise.models.ModelWorkOrderTask;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Singleton;
import com.fieldwise.utils.Utl;
import com.mindorks.placeholderview.ExpandablePlaceHolderView;
import com.mindorks.placeholderview.PlaceHolderView;
import com.schibstedspain.leku.LocationPickerActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class FragmentWorkOrderInfo extends Fragment implements
        AdapterAccount.AddAccountClickListen,
        AdapterWorkOrderViewAll.ViewAllListen,
        AdapterWorkOrderAddItem.AddItemListen,
        AdapterProductParent.AddProductCheckBoxListen,
        AdapterChemical.AddChemicalClickListen,
        AdapterWorkOrderRelatedChild.ClickListen {

    private static final int PERMISSION_REQUEST_CODE = 200;

    private RadioGroup radioGroupMain;
    private RadioButton radioButtonDetails, radioButtonRelated;

    private ScrollView layoutRecentWorkOrdersDetails;
    private ExpandablePlaceHolderView phvRecentWorkOrdersRelated;

    public APIInterface apiInterface;

    private String workOrderId;

    private String strWorkOrderID, strWorkOrderNo,
            strAssignedTo, strAssignedToName,
            strAccount, strAccountNew, strAccountName, strAccountNameNew,
            strPrimaryContact, strPrimaryContactNew, strPrimaryContactName, strPrimaryContactNameNew,
            strParentWorkOrder, strParentWorkOrderNew, strParentWorkOrderName, strParentWorkOrderNameNew,
            strSubject,
            strDescription,
            strPopupReminder,
            strType, strTypeName,
            strStatus, strStatusName,
            strPriority, strPriorityName,
            strCategory, strCategoryName,
            strStartDate, strEndDate,
            strStartDateTimeDate, strNewStartDateTimeDate, strStartDateTimeTime, strNewStartDateTimeTime,
            strEndDateTimeDate, strNewEndDateTimeDate, strEndDateTimeTime, strNewEndDateTimeTime,
            strRecurring, strStartOn, strStartOnNew,
            strBilLat, strBilLng, strBilAddress, strBilCity, strBilState, strBilCountry, strBilPostal,
            strNewBilLat, strNewBilLng,
            strNewLat, strNewLng, strNewAddress, strNewCity, strNewState, strNewCountry, strNewPostal,
            strSubTotal,
            strDiscount,
            strTax,
            strTotalPrice,
            strGrandTotal,
            strSignature,
            strLineItemCount,
            strCreatedDate, strLastModifiedDate, strCreatedBy, strLastModifiedBy,
            strRepeatEvery, strNewRepeatEvery,
            strIntervalEvery, strNewIntervalEvery,
            strRepeatOn,
            strSun, strMon, strTue, strWed, strThu, strFri, strSat,
            strEnds, strEndsOn, strEndsOnNew, strEndsAfter,
            strRecurrenceStartTime, strRecurrenceNewStartTime, strRecurrenceEndTime, strRecurrenceNewEndTime,
            strMobileNo;

    private boolean isAccountClicked = false;
    private boolean isContactClicked = false;
    private boolean isWorkOrderClicked = false;
    private boolean isCloneMode = false;
    private boolean isStartDateClicked = false;
    private boolean isEndDateClicked = false;
    private boolean isBilClicked = false;
    private boolean isStartDateOnClicked = false;
    private boolean isRecurrenceStartDateClicked = false;
    private boolean isRecurrenceEndDateClicked = false;
    private boolean isRepeatEveryClicked = false;
    private boolean isIntervalClicked = false;
    private boolean isEndDateOnClicked = false;

    private RelativeLayout layoutAssignedTo,
            layoutAccount,
            layoutPrimaryContact,
            layoutParentWorkOrder,
            layoutType,
            layoutStatus,
            layoutPriority,
            layoutCategory;

    private LinearLayout layoutHeader, layoutWorkOrderNo, layoutDateInfo, layoutFinancialInfo, layoutSystemInfo;

    private Spinner spinnerAssignedTo,
            spinnerType,
            spinnerStatus,
            spinnerPriority,
            spinnerCategory;

    private List<ModelSpinner> listAssignedTo,
            listAccounts,
            listPrimaryContacts,
            listParentWorkOrders,
            listTypes,
            listStatus,
            listPriorities,
            listCategories;

    private ArrayAdapter<ModelSpinner> adapterAssignedTo,
            adapterPrimaryContact,
            adapterParentWorkOrder,
            adapterType,
            adapterStatus,
            adapterPriority,
            adapterCategory;

    private EditText edittextAccount,
            edittextPrimaryContact,
            edittextParentWorkOrder,
            edittextSubject,
            edittextDescription,
            edittextPopupReminder,
            edittextBilAddress,
            edittextBilCity,
            edittextBilState,
            edittextBilCountry,
            edittextBilPostal;

    private TextView textviewHeaderStatus, textviewHeaderType,
            textviewWorkOrderNo,
            textviewAssignedTo,
            textviewAccount,
            textviewPrimaryContact,
            textviewParentWorkOrder,
            textviewSubject,
            textviewDescription,
            textviewPopupReminder,
            textviewType,
            textviewStatus,
            textviewPriority,
            textviewCategory,
            textviewStartDate, edittextStartDateTime,
            textviewEndDate, edittextEndDateTime,
            textviewBilAddress,
            textviewBilCity,
            textviewBilState,
            textviewBilCountry,
            textviewBilPostal,
            textviewSubTotal,
            textviewDiscount,
            textviewTax,
            textviewTotalPrice,
            textviewGrandTotal,
            textviewLineItemCount,
            textviewCreatedDate,
            textviewCreatedBy,
            textviewLastModifiedDate,
            textviewLastModifiedBy;

    private ImageButton buttonAccount;
    private LinearLayout layoutAccountAdd;
    private EditText edittextAccountAddSearch;
    private PlaceHolderView phvAccountAdd;

    private ImageButton buttonContact;
    private LinearLayout layoutContactAdd;
    private EditText edittextContactAddSearch;
    private PlaceHolderView phvContactAdd;

    private ImageButton buttonWorkOrder;
    private LinearLayout layoutWorkOrderAdd;
    private EditText edittextWorkOrderAddSearch;
    private PlaceHolderView phvWorkOrderAdd;

    private ImageView imageviewSignature;

    private CheckBox checkboxIsRecurring;
    private ImageButton buttonBilAddress;

    private LinearLayout layoutNavigation, layoutSaveCancel;
    private ImageButton buttonCall, buttonComment, buttonDate, buttonEdit, buttonMore;
    private Button buttonSave, buttonCancel;

    private RelativeLayout layoutRecurrence, layoutEndAfter;
    private LinearLayout layoutRecurrenceRepeatOn;
    private CheckBox checkboxSun, checkboxMon, checkboxTue, checkboxWed, checkboxThu, checkboxFri, checkboxSat;
    private Spinner spinnerRepeatsEvery, spinnerIntervalEvery, spinnerEndsAfter;
    private TextView textviewStartOnDate, textviewRepeatsEveryPostfix, textviewEndsOnDate, textviewEndsAfterPostfix,
            textviewRecurrenceStartTime, textviewRecurrenceEndTime;
    private RadioGroup radioGroupRecurrence;
    private RadioButton radioButtonRecurrenceNever, radioButtonRecurrenceOn, radioButtonRecurrenceAfter;
    private Button buttonRecurrenceSave, buttonRecurrenceClose;

    private LinearLayout layoutProductAdd, layoutProductEdit, layoutProductEditChange;
    private RelativeLayout layoutProductFilter;
    private EditText edittextProductAddSearch, edittextProductFilterCode;
    private Button buttonProductAddMenu, buttonProductEditMenu, buttonProductFilterAdd, buttonProductFilterCancel, buttonProductFilterApply, buttonProductFilterClear;

    private ExpandablePlaceHolderView phvProductAdd;
    private List<ModelProduct> listProduct;
    private List<String> listProductEditChange;

    private List<ModelSpinner> listProductFilterFamily;
    private ArrayAdapter<ModelSpinner> adapterProductFilterFamily;
    private Spinner spinnerProductFilterFamily;

    private TextView textviewProductEditGrandTotal;

    private LinearLayout layoutChemicalAdd, layoutChemicalEdit, layoutChemicalEditChange;
    private EditText edittextChemicalAddSearch;
    private Button buttonChemicalEditMenu;

    private PlaceHolderView phvChemicalAdd;
    private List<ModelChemical> listChemical;
    private List<String> listChemicalEdit;
    private List<String> listChemicalEditChange;

    private RelativeLayout layoutNewNote;
    private EditText edittextNewNoteSubject, edittextNewNoteBody;
    private Button buttonNewNoteSave, buttonNewNoteCancel;

    private List<ModelSpinner> listUnitSpinner;
    private ArrayAdapter<ModelSpinner> adapterUnitSpinner;

    private TextView textviewChemicalAddAnother;

    private List<String> listRecurrenceRepeat, listRecurrenceInterval, listRecurrenceEndAfter;
    private ArrayAdapter<String> adapterRecurrenceRepeat, adapterRecurrenceInterval, adapterRecurrenceEndAfter;

    private List<String> listRelatedWorkOrderList;
    private List<ModelWorkOrderLineItem> listRelatedWorkOrderLineItems;
    private List<ModelWorkOrderChemical> listRelatedWorkOrderChemicals;
    private List<ModelWorkOrderEvent> listRelatedWorkOrderEvents;
    private List<ModelWorkOrderFile> listRelatedWorkOrderFiles;
    private List<ModelWorkOrderTask> listRelatedWorkOrderTasks;
    private List<ModelWorkOrderInvoice> listRelatedWorkOrderInvoices;
    private List<ModelWorkOrderNote> listRelatedWorkOrderNotes;

    private RelativeLayout layoutWebView;
    private WebView webView;
    private Button buttonWebViewClose;

    public WorkOrderViewAllListen click;

    public interface WorkOrderViewAllListen {
        void callbackWorkOrderViewAllListen(String str, String workOrderId);
    }

    public WorkOrderSignatureListener clickSignature;

    public interface WorkOrderSignatureListener {
        void callbackWorkOrderSignatureListener(String workOrderId, String workOrderNo);
    }

    public CreateFileListener clickCrateFile;

    public interface CreateFileListener {
        void callbackWorkOrderCreateFile(String strObject);
    }

    public CreateEventListener clickCrateEvent;

    public interface CreateEventListener {
        void callbackWorkOrderCreateEvent(String strWhoName, String strWhatID, String strWhatName);
    }

    public SendEmailListener clickSendEmail;

    public interface SendEmailListener {
        void callbackWorkOrderSendEmail(String what, String header);
    }

    public GoToRecent clickGoToRecent;

    public interface GoToRecent {
        void callbackGoToRecentWorkOrder(String strObject);
    }

    //Create Interface
    public ClickListenInvoiceCreate clickInvoiceCreate;

    public interface ClickListenInvoiceCreate {
        void callbackWorkOrderInvoiceCreate();
    }

    public ClickListenTaskCreate clickTaskCreate;

    public interface ClickListenTaskCreate {
        void callbackWorkOrderTaskCreate();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_work_order_detailed, container, false);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        click = (WorkOrderViewAllListen) getActivity();
        clickSignature = (WorkOrderSignatureListener) getActivity();
        clickCrateFile = (CreateFileListener) getActivity();
        clickCrateEvent = (CreateEventListener) getActivity();
        clickSendEmail = (SendEmailListener) getActivity();
        clickGoToRecent = (GoToRecent) getActivity();

        clickInvoiceCreate = (ClickListenInvoiceCreate) getActivity();
        clickTaskCreate = (ClickListenTaskCreate) getActivity();

        textviewHeaderStatus = (TextView) view.findViewById(R.id.textview_header_status);
        textviewHeaderType = (TextView) view.findViewById(R.id.textview_header_type);

        radioGroupMain = (RadioGroup) view.findViewById(R.id.radio_group_main);
        radioButtonDetails = (RadioButton) view.findViewById(R.id.radio_button_details);
        radioButtonRelated = (RadioButton) view.findViewById(R.id.radio_button_related);

        layoutRecentWorkOrdersDetails = (ScrollView) view.findViewById(R.id.layout_recent_work_order_details);
        phvRecentWorkOrdersRelated = (ExpandablePlaceHolderView) view.findViewById(R.id.phv_recent_work_order_related);

        layoutHeader = (LinearLayout) view.findViewById(R.id.layout_header);
        layoutWorkOrderNo = (LinearLayout) view.findViewById(R.id.layout_work_order_no);
        layoutAssignedTo = (RelativeLayout) view.findViewById(R.id.layout_assigned_to);
        layoutAccount = (RelativeLayout) view.findViewById(R.id.layout_account);
        layoutPrimaryContact = (RelativeLayout) view.findViewById(R.id.layout_primary_contact);
        layoutParentWorkOrder = (RelativeLayout) view.findViewById(R.id.layout_parent_work_order);
        layoutType = (RelativeLayout) view.findViewById(R.id.layout_type);
        layoutStatus = (RelativeLayout) view.findViewById(R.id.layout_status);
        layoutPriority = (RelativeLayout) view.findViewById(R.id.layout_priority);
        layoutCategory = (RelativeLayout) view.findViewById(R.id.layout_category);
        layoutDateInfo = (LinearLayout) view.findViewById(R.id.layout_date_info);
        layoutFinancialInfo = (LinearLayout) view.findViewById(R.id.layout_financial_info);
        layoutSystemInfo = (LinearLayout) view.findViewById(R.id.layout_system_info);

        spinnerAssignedTo = (Spinner) view.findViewById(R.id.spinner_assigned_to);
        spinnerType = (Spinner) view.findViewById(R.id.spinner_type);
        spinnerStatus = (Spinner) view.findViewById(R.id.spinner_status);
        spinnerPriority = (Spinner) view.findViewById(R.id.spinner_priority);
        spinnerCategory = (Spinner) view.findViewById(R.id.spinner_category);

        edittextAccount = (EditText) view.findViewById(R.id.edittext_account);
        edittextPrimaryContact = (EditText) view.findViewById(R.id.edittext_contact);
        edittextParentWorkOrder = (EditText) view.findViewById(R.id.edittext_work_order);
        edittextSubject = (EditText) view.findViewById(R.id.edittext_subject);
        edittextDescription = (EditText) view.findViewById(R.id.edittext_description);
        edittextPopupReminder = (EditText) view.findViewById(R.id.edittext_popup_reminder);
        edittextBilAddress = (EditText) view.findViewById(R.id.edittext_bil_address);
        edittextBilCity = (EditText) view.findViewById(R.id.edittext_bil_city);
        edittextBilState = (EditText) view.findViewById(R.id.edittext_bil_state);
        edittextBilCountry = (EditText) view.findViewById(R.id.edittext_bil_country);
        edittextBilPostal = (EditText) view.findViewById(R.id.edittext_bil_postal);

        textviewWorkOrderNo = (TextView) view.findViewById(R.id.textview_work_order_no);
        textviewAssignedTo = (TextView) view.findViewById(R.id.textview_assigned_to);
        textviewAccount = (TextView) view.findViewById(R.id.textview_account);
        textviewPrimaryContact = (TextView) view.findViewById(R.id.textview_primary_contact);
        textviewParentWorkOrder = (TextView) view.findViewById(R.id.textview_parent_work_order);
        textviewSubject = (TextView) view.findViewById(R.id.textview_subject);
        textviewDescription = (TextView) view.findViewById(R.id.textview_description);
        textviewPopupReminder = (TextView) view.findViewById(R.id.textview_popup_reminder);
        textviewType = (TextView) view.findViewById(R.id.textview_type);
        textviewStatus = (TextView) view.findViewById(R.id.textview_status);
        textviewPriority = (TextView) view.findViewById(R.id.textview_priority);
        textviewCategory = (TextView) view.findViewById(R.id.textview_category);
        textviewStartDate = (TextView) view.findViewById(R.id.textview_start_date);
        edittextStartDateTime = (TextView) view.findViewById(R.id.edittext_start_date_time);
        textviewEndDate = (TextView) view.findViewById(R.id.textview_end_date);
        edittextEndDateTime = (TextView) view.findViewById(R.id.edittext_end_date_time);
        textviewBilAddress = (TextView) view.findViewById(R.id.textview_bil_address);
        textviewBilCity = (TextView) view.findViewById(R.id.textview_bil_city);
        textviewBilState = (TextView) view.findViewById(R.id.textview_bil_state);
        textviewBilCountry = (TextView) view.findViewById(R.id.textview_bil_country);
        textviewBilPostal = (TextView) view.findViewById(R.id.textview_bil_postal);
        textviewSubTotal = (TextView) view.findViewById(R.id.textview_sub_total);
        textviewDiscount = (TextView) view.findViewById(R.id.textview_discount);
        textviewTax = (TextView) view.findViewById(R.id.textview_tax);
        textviewTotalPrice = (TextView) view.findViewById(R.id.textview_total_price);
        textviewGrandTotal = (TextView) view.findViewById(R.id.textview_grand_total);
        imageviewSignature = (ImageView) view.findViewById(R.id.imageview_signature);
        textviewLineItemCount = (TextView) view.findViewById(R.id.textview_line_item_count);
        textviewCreatedDate = (TextView) view.findViewById(R.id.textview_created_date);
        textviewCreatedBy = (TextView) view.findViewById(R.id.textview_created_by);
        textviewLastModifiedDate = (TextView) view.findViewById(R.id.textview_last_modified_date);
        textviewLastModifiedBy = (TextView) view.findViewById(R.id.textview_last_modified_by);

        checkboxIsRecurring = (CheckBox) view.findViewById(R.id.checkbox_is_recurring);
        buttonBilAddress = (ImageButton) view.findViewById(R.id.button_bil_address);

        layoutNavigation = (LinearLayout) view.findViewById(R.id.layout_navigation);
        buttonCall = (ImageButton) view.findViewById(R.id.button_call);
        buttonComment = (ImageButton) view.findViewById(R.id.button_comment);
        buttonDate = (ImageButton) view.findViewById(R.id.button_date);
        buttonEdit = (ImageButton) view.findViewById(R.id.button_edit);
        buttonMore = (ImageButton) view.findViewById(R.id.button_more);
        layoutSaveCancel = (LinearLayout) view.findViewById(R.id.layout_save_cancel);
        buttonSave = (Button) view.findViewById(R.id.button_save);
        buttonCancel = (Button) view.findViewById(R.id.button_cancel);

        buttonAccount = (ImageButton) view.findViewById(R.id.button_account);
        layoutAccountAdd = (LinearLayout) view.findViewById(R.id.layout_account_add);
        edittextAccountAddSearch = (EditText) view.findViewById(R.id.edittext_account_add_search);
        phvAccountAdd = (PlaceHolderView) view.findViewById(R.id.phv_account_add);

        buttonContact = (ImageButton) view.findViewById(R.id.button_contact);
        layoutContactAdd = (LinearLayout) view.findViewById(R.id.layout_contact_add);
        edittextContactAddSearch = (EditText) view.findViewById(R.id.edittext_contact_add_search);
        phvContactAdd = (PlaceHolderView) view.findViewById(R.id.phv_contact_add);

        buttonWorkOrder = (ImageButton) view.findViewById(R.id.button_work_order);
        layoutWorkOrderAdd = (LinearLayout) view.findViewById(R.id.layout_work_order_add);
        edittextWorkOrderAddSearch = (EditText) view.findViewById(R.id.edittext_work_order_add_search);
        phvWorkOrderAdd = (PlaceHolderView) view.findViewById(R.id.phv_work_order_add);

        layoutRecurrence = (RelativeLayout) view.findViewById(R.id.layout_recurrence);
        spinnerRepeatsEvery = (Spinner) view.findViewById(R.id.spinner_repeats_every);
        spinnerIntervalEvery = (Spinner) view.findViewById(R.id.spinner_interval_every);
        textviewRepeatsEveryPostfix = (TextView) view.findViewById(R.id.textview_repeats_every_postfix);
        layoutRecurrenceRepeatOn = (LinearLayout) view.findViewById(R.id.layout_recurrence_repeat_on);
        checkboxSun = (CheckBox) view.findViewById(R.id.checkbox_sun);
        checkboxMon = (CheckBox) view.findViewById(R.id.checkbox_mon);
        checkboxTue = (CheckBox) view.findViewById(R.id.checkbox_tue);
        checkboxWed = (CheckBox) view.findViewById(R.id.checkbox_wed);
        checkboxThu = (CheckBox) view.findViewById(R.id.checkbox_thu);
        checkboxFri = (CheckBox) view.findViewById(R.id.checkbox_fri);
        checkboxSat = (CheckBox) view.findViewById(R.id.checkbox_sat);
        textviewStartOnDate = (TextView) view.findViewById(R.id.textview_start_on_date);
        textviewEndsOnDate = (TextView) view.findViewById(R.id.textview_ends_on_date);
        textviewRecurrenceStartTime = (TextView) view.findViewById(R.id.textview_recurrence_start_time);
        textviewRecurrenceEndTime = (TextView) view.findViewById(R.id.textview_recurrence_end_time);
        radioGroupRecurrence = (RadioGroup) view.findViewById(R.id.radio_group_recurrence);
        radioButtonRecurrenceNever = (RadioButton) view.findViewById(R.id.radio_button_recurrence_never);
        radioButtonRecurrenceOn = (RadioButton) view.findViewById(R.id.radio_button_recurrence_on);
        radioButtonRecurrenceAfter = (RadioButton) view.findViewById(R.id.radio_button_recurrence_after);
        layoutEndAfter = (RelativeLayout) view.findViewById(R.id.layout_end_after);
        spinnerEndsAfter = (Spinner) view.findViewById(R.id.spinner_end_after);
        textviewEndsAfterPostfix = (TextView) view.findViewById(R.id.textview_ends_after_postfix);
        buttonRecurrenceSave = (Button) view.findViewById(R.id.button_recurrence_save);
        buttonRecurrenceClose = (Button) view.findViewById(R.id.button_recurrence_close);

        layoutProductAdd = (LinearLayout) view.findViewById(R.id.layout_product_add);
        edittextProductAddSearch = (EditText) view.findViewById(R.id.edittext_product_add_search);
        buttonProductFilterAdd = (Button) view.findViewById(R.id.button_product_add_filter);
        buttonProductAddMenu = (Button) view.findViewById(R.id.button_product_add_menu);
        phvProductAdd = (ExpandablePlaceHolderView) view.findViewById(R.id.phv_product_add);

        layoutProductEdit = (LinearLayout) view.findViewById(R.id.layout_product_edit);
        buttonProductEditMenu = (Button) view.findViewById(R.id.button_product_edit_menu);
        layoutProductEditChange = (LinearLayout) view.findViewById(R.id.layout_product_edit_change);

        layoutProductFilter = (RelativeLayout) view.findViewById(R.id.layout_product_filter);
        spinnerProductFilterFamily = (Spinner) view.findViewById(R.id.spinner_product_filter_family);
        edittextProductFilterCode = (EditText) view.findViewById(R.id.edittext_product_filter_code);
        buttonProductFilterCancel = (Button) view.findViewById(R.id.button_product_filter_cancel);
        buttonProductFilterApply = (Button) view.findViewById(R.id.button_product_filter_apply);
        buttonProductFilterClear = (Button) view.findViewById(R.id.button_product_filter_clear);

        textviewProductEditGrandTotal = (TextView) view.findViewById(R.id.textview_product_edit_grand_total);

        layoutChemicalAdd = (LinearLayout) view.findViewById(R.id.layout_chemical_add);
        edittextChemicalAddSearch = (EditText) view.findViewById(R.id.edittext_chemical_add_search);

        phvChemicalAdd = (PlaceHolderView) view.findViewById(R.id.phv_chemical_add);

        layoutChemicalEdit = (LinearLayout) view.findViewById(R.id.layout_chemical_edit);
        buttonChemicalEditMenu = (Button) view.findViewById(R.id.button_chemical_edit_menu);
        layoutChemicalEditChange = (LinearLayout) view.findViewById(R.id.layout_chemical_edit_change);

        layoutNewNote = (RelativeLayout) view.findViewById(R.id.layout_new_note);
        edittextNewNoteSubject = (EditText) view.findViewById(R.id.edittext_new_note_subject);
        edittextNewNoteBody = (EditText) view.findViewById(R.id.edittext_new_note_body);
        buttonNewNoteSave = (Button) view.findViewById(R.id.button_new_note_save);
        buttonNewNoteCancel = (Button) view.findViewById(R.id.button_new_note_cancel);

        textviewChemicalAddAnother = (TextView) view.findViewById(R.id.textview_chemical_add_another);

        layoutWebView = (RelativeLayout) view.findViewById(R.id.layout_webview);
        webView = (WebView) view.findViewById(R.id.webview);
        buttonWebViewClose = (Button) view.findViewById(R.id.button_webview_close);
        webView.getSettings().setJavaScriptEnabled(true);
        buttonWebViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutWebView.setVisibility(View.GONE);
            }
        });

        strNewBilLat = "0";
        strNewBilLng = "0";
        strNewLat = "0";
        strNewLng = "0";
        strNewAddress = "0";
        strNewCity = "0";
        strNewState = "0";
        strNewCountry = "0";
        strNewPostal = "0";

        listAssignedTo = new ArrayList<>();
        listAccounts = new ArrayList<>();
        listPrimaryContacts = new ArrayList<>();
        listParentWorkOrders = new ArrayList<>();
        listTypes = new ArrayList<>();
        listStatus = new ArrayList<>();
        listPriorities = new ArrayList<>();
        listCategories = new ArrayList<>();
        adapterAssignedTo = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listAssignedTo);
        adapterAssignedTo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterPrimaryContact = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listPrimaryContacts);
        adapterPrimaryContact.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterParentWorkOrder = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listParentWorkOrders);
        adapterParentWorkOrder.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterType = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listTypes);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterStatus = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listStatus);
        adapterStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterPriority = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listPriorities);
        adapterPriority.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterCategory = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listCategories);
        adapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Singleton.getInstance().listWorkOrderProduct.clear();
        listProduct = new ArrayList<>();
        listProductEditChange = new ArrayList<>();

        Singleton.getInstance().listWorkOrderChemical.clear();
        listChemical = new ArrayList<>();
        listChemicalEdit = new ArrayList<>();
        listChemicalEditChange = new ArrayList<>();
        listUnitSpinner = new ArrayList<>();
        adapterUnitSpinner = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listUnitSpinner);
        listUnitSpinner.add(new ModelSpinner("1", "Unit"));
        listUnitSpinner.add(new ModelSpinner("2", "PPM"));
        listUnitSpinner.add(new ModelSpinner("3", "Gallon"));
        listUnitSpinner.add(new ModelSpinner("4", "Quarts"));

        radioButtonDetails.setChecked(true);
        radioButtonRelated.setChecked(false);
        layoutRecentWorkOrdersDetails.setVisibility(View.VISIBLE);
        phvRecentWorkOrdersRelated.setVisibility(View.GONE);

        editMode(false, false);

        workOrderId = getArguments().getString(Cons.KEY_WorkOrderID);

        getWorkOrderDetailed(workOrderId);

        getAllUsers();
        //getAccounts();
        //getContacts();
        //getWorkOrders();
        getTypes();
        getStatus();
        getPriorities();
        getCategories();

        listRelatedWorkOrderList = new ArrayList<>();
        listRelatedWorkOrderLineItems = new ArrayList<>();
        listRelatedWorkOrderChemicals = new ArrayList<>();
        listRelatedWorkOrderEvents = new ArrayList<>();
        listRelatedWorkOrderFiles = new ArrayList<>();
        listRelatedWorkOrderTasks = new ArrayList<>();
        listRelatedWorkOrderInvoices = new ArrayList<>();
        listRelatedWorkOrderNotes = new ArrayList<>();

        getRelatedWorkOrderList(workOrderId);
        getRelatedWorkOrderLineItems(workOrderId);
        getRelatedWorkOrderChemicals(workOrderId);
        getRelatedWorkOrderEvents(workOrderId);
        getRelatedWorkOrderFiles(workOrderId);
        getRelatedWorkOrderTasks(workOrderId);
        getRelatedWorkOrderInvoices(workOrderId);
        getRelatedWorkOrderNotes(workOrderId);

        radioGroupMain.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radio_button_details) {
                    buttonCall.setEnabled(true);
                    buttonComment.setEnabled(true);
                    buttonDate.setEnabled(true);
                    buttonEdit.setEnabled(true);
                    layoutRecentWorkOrdersDetails.setVisibility(View.VISIBLE);
                    phvRecentWorkOrdersRelated.setVisibility(View.GONE);
                } else if (checkedId == R.id.radio_button_related) {
                    buttonCall.setEnabled(false);
                    buttonComment.setEnabled(false);
                    buttonDate.setEnabled(false);
                    buttonEdit.setEnabled(false);
                    layoutRecentWorkOrdersDetails.setVisibility(View.GONE);
                    phvRecentWorkOrdersRelated.setVisibility(View.VISIBLE);
                    phvRecentWorkOrdersRelated.removeAllViews();

                    for (int i = 0; i < listRelatedWorkOrderList.size(); i++) {
                        phvRecentWorkOrdersRelated
                                .addView(new AdapterWorkOrderRelatedParent(getActivity(), listRelatedWorkOrderList.get(i)));
                    }

                    int lengthLineItem;
                    if (listRelatedWorkOrderLineItems.size() > 3) {
                        lengthLineItem = 3;
                    } else {
                        lengthLineItem = listRelatedWorkOrderLineItems.size();
                    }
                    for (int i = 0; i < lengthLineItem; i++) {
                        phvRecentWorkOrdersRelated
                                .addChildView(0, new AdapterWorkOrderRelatedChild(getActivity(), FragmentWorkOrderInfo.this, getString(R.string.line_item), listRelatedWorkOrderLineItems.get(i).getProductID(),
                                        "WO Line#: " + listRelatedWorkOrderLineItems.get(i).getLineItemNo() + "\n" +
                                                "Product: " +
                                                listRelatedWorkOrderLineItems.get(i).getProductName() + "\n" +
                                                "Unit Price: $" +
                                                listRelatedWorkOrderLineItems.get(i).getUnitPrice() + "\n" +
                                                "Quantity: " +
                                                listRelatedWorkOrderLineItems.get(i).getQuantity() + "\n" +
                                                "Net Total: $" +
                                                listRelatedWorkOrderLineItems.get(i).getNetTotal()
                                ));
                    }
                    if (listRelatedWorkOrderLineItems.size() > 3) {
                        phvRecentWorkOrdersRelated.
                                addChildView(0, new AdapterWorkOrderViewAll(getActivity(), FragmentWorkOrderInfo.this, getString(R.string.line_item)));
                    } else if (listRelatedWorkOrderLineItems.size() == 0) {
                        phvRecentWorkOrdersRelated.
                                addChildView(0, new AdapterWorkOrderAddItem(getActivity(), FragmentWorkOrderInfo.this, getString(R.string.line_item)));
                    }

                    int lengthChemical;
                    if (listRelatedWorkOrderChemicals.size() > 3) {
                        lengthChemical = 3;
                    } else {
                        lengthChemical = listRelatedWorkOrderChemicals.size();
                    }
                    for (int i = 0; i < lengthChemical; i++) {
                        phvRecentWorkOrdersRelated
                                .addChildView(1, new AdapterWorkOrderRelatedChild(getActivity(), FragmentWorkOrderInfo.this, getString(R.string.chemical), listRelatedWorkOrderChemicals.get(i).getProductID(),
                                        "CH#: " +
                                                listRelatedWorkOrderChemicals.get(i).getChemicalNo() + "\n" +
                                                "Chemical: " +
                                                listRelatedWorkOrderChemicals.get(i).getProductName() + "\n" +
                                                "Tested Concentration: " +
                                                listRelatedWorkOrderChemicals.get(i).getTestConcentration() + " " + listRelatedWorkOrderChemicals.get(i).getTestedUnitOfMeasure() + "\n" +
                                                "Application: " +
                                                listRelatedWorkOrderChemicals.get(i).getApplicationAmount() + " " + listRelatedWorkOrderChemicals.get(i).getApplicationUnitOfMeasure()
                                ));
                    }
                    if (listRelatedWorkOrderChemicals.size() > 3) {
                        phvRecentWorkOrdersRelated.
                                addChildView(1, new AdapterWorkOrderViewAll(getActivity(), FragmentWorkOrderInfo.this, getString(R.string.chemical)));
                    } else if (listRelatedWorkOrderChemicals.size() == 0) {
                        phvRecentWorkOrdersRelated.
                                addChildView(1, new AdapterWorkOrderAddItem(getActivity(), FragmentWorkOrderInfo.this, getString(R.string.chemical)));
                    }
                    int lengthEvent;
                    if (listRelatedWorkOrderEvents.size() > 3) {
                        lengthEvent = 3;
                    } else {
                        lengthEvent = listRelatedWorkOrderEvents.size();
                    }
                    for (int i = 0; i < lengthEvent; i++) {
                        phvRecentWorkOrdersRelated
                                .addChildView(2, new AdapterWorkOrderRelatedChild(getActivity(), FragmentWorkOrderInfo.this,
                                        getString(R.string.event), listRelatedWorkOrderEvents.get(i).getEventID(),
                                        "Subject: " +
                                                listRelatedWorkOrderEvents.get(i).getSubject() + "\n" +
                                                "Status: " +
                                                listRelatedWorkOrderEvents.get(i).getEventStatus() + "\n" +
                                                "Type: " +
                                                listRelatedWorkOrderEvents.get(i).getEventTypeName() + "\n" +
                                                "Start: " +
                                                listRelatedWorkOrderEvents.get(i).getEventStartDate() + " " + listRelatedWorkOrderEvents.get(i).getEventStartTime() + "\n" +
                                                "End: " +
                                                listRelatedWorkOrderEvents.get(i).getEventEndDate() + " " + listRelatedWorkOrderEvents.get(i).getEventEndTime()
                                ));
                    }
                    if (listRelatedWorkOrderEvents.size() > 3) {
                        phvRecentWorkOrdersRelated.
                                addChildView(2, new AdapterWorkOrderViewAll(getActivity(), FragmentWorkOrderInfo.this, getString(R.string.event)));
                    } else if (listRelatedWorkOrderEvents.size() == 0) {
                        phvRecentWorkOrdersRelated.
                                addChildView(2, new AdapterWorkOrderAddItem(getActivity(), FragmentWorkOrderInfo.this, getString(R.string.event)));
                    }
                    int lengthFile;
                    if (listRelatedWorkOrderFiles.size() > 3) {
                        lengthFile = 3;
                    } else {
                        lengthFile = listRelatedWorkOrderFiles.size();
                    }
                    for (int i = 0; i < lengthFile; i++) {
                        phvRecentWorkOrdersRelated
                                .addChildView(3, new AdapterWorkOrderRelatedChild(getActivity(),
                                        "Name: " +
                                                listRelatedWorkOrderFiles.get(i).getFileName() + "\n" +
                                                "Subject: " +
                                                listRelatedWorkOrderFiles.get(i).getSubject()
                                ));
                    }
                    if (listRelatedWorkOrderFiles.size() > 3) {
                        phvRecentWorkOrdersRelated.
                                addChildView(3, new AdapterWorkOrderViewAll(getActivity(), FragmentWorkOrderInfo.this, getString(R.string.file)));
                    } else if (listRelatedWorkOrderFiles.size() == 0) {
                        phvRecentWorkOrdersRelated.
                                addChildView(3, new AdapterWorkOrderAddItem(getActivity(), FragmentWorkOrderInfo.this, getString(R.string.file)));
                    }
                    int lengthTask;
                    if (listRelatedWorkOrderTasks.size() > 3) {
                        lengthTask = 3;
                    } else {
                        lengthTask = listRelatedWorkOrderTasks.size();
                    }
                    for (int i = 0; i < lengthTask; i++) {
                        phvRecentWorkOrdersRelated
                                .addChildView(4, new AdapterWorkOrderRelatedChild(getActivity(),
                                        "Subject: " +
                                                listRelatedWorkOrderTasks.get(i).getSubject() + "\n" +
                                                "Type: " +
                                                listRelatedWorkOrderTasks.get(i).getTaskType() + "\n" +
                                                "Status: " +
                                                listRelatedWorkOrderTasks.get(i).getTaskStatus() + "\n" +
                                                "Priority: " +
                                                listRelatedWorkOrderTasks.get(i).getPriority() + "\n" /*+
                                                "CallDisposition: " +
                                                listRelatedWorkOrderTasks.get(i).getCallDisposition()*/
                                ));
                    }
                    if (listRelatedWorkOrderTasks.size() > 3) {
                        phvRecentWorkOrdersRelated.
                                addChildView(4, new AdapterWorkOrderViewAll(getActivity(), FragmentWorkOrderInfo.this, getString(R.string.task)));
                    } else if (listRelatedWorkOrderTasks.size() == 0) {
                        phvRecentWorkOrdersRelated.
                                addChildView(4, new AdapterWorkOrderAddItem(getActivity(), FragmentWorkOrderInfo.this, getString(R.string.task)));
                    }
                    int lengthInvoice;
                    if (listRelatedWorkOrderInvoices.size() > 3) {
                        lengthInvoice = 3;
                    } else {
                        lengthInvoice = listRelatedWorkOrderInvoices.size();
                    }
                    for (int i = 0; i < lengthInvoice; i++) {
                        phvRecentWorkOrdersRelated
                                .addChildView(5, new AdapterWorkOrderRelatedChild(getActivity(),
                                        "Invoice: " +
                                                listRelatedWorkOrderInvoices.get(i).getInvoiceNumber() + "\n" +
                                                "Work Order: " +
                                                listRelatedWorkOrderInvoices.get(i).getWorkOrderName() + "\n" +
                                                "Status: " +
                                                listRelatedWorkOrderInvoices.get(i).getInvoiceStatus() + "\n" +
                                                "Invoice Date: " +
                                                listRelatedWorkOrderInvoices.get(i).getInvoiceDate() + "\n" +
                                                "Due Date: " +
                                                listRelatedWorkOrderInvoices.get(i).getDueDate() + "\n" +
                                                "Sub Total: " +
                                                listRelatedWorkOrderInvoices.get(i).getSubTotal() + "\n" +
                                                "Total: " +
                                                listRelatedWorkOrderInvoices.get(i).getTotalPrice()
                                ));
                    }
                    if (listRelatedWorkOrderInvoices.size() > 3) {
                        phvRecentWorkOrdersRelated.
                                addChildView(5, new AdapterWorkOrderViewAll(getActivity(), FragmentWorkOrderInfo.this, getString(R.string.invoice)));
                    } else if (listRelatedWorkOrderInvoices.size() == 0) {
                        phvRecentWorkOrdersRelated.
                                addChildView(5, new AdapterWorkOrderAddItem(getActivity(), FragmentWorkOrderInfo.this, getString(R.string.invoice)));
                    }
                    int lengthNote;
                    if (listRelatedWorkOrderNotes.size() > 3) {
                        lengthNote = 3;
                    } else {
                        lengthNote = listRelatedWorkOrderNotes.size();
                    }
                    for (int i = 0; i < lengthNote; i++) {
                        phvRecentWorkOrdersRelated
                                .addChildView(6, new AdapterWorkOrderRelatedChild(getActivity(), FragmentWorkOrderInfo.this, getString(R.string.note), listRelatedWorkOrderNotes.get(i).getNoteID(),
                                        "Subject: " +
                                                listRelatedWorkOrderNotes.get(i).getSubject() + "\n" +
                                                "Created Date: " +
                                                listRelatedWorkOrderNotes.get(i).getCreatedDate() + "\n" +
                                                "Owner: " +
                                                listRelatedWorkOrderNotes.get(i).getOwnerName()
                                ));
                    }
                    if (listRelatedWorkOrderNotes.size() > 3) {
                        phvRecentWorkOrdersRelated.
                                addChildView(6, new AdapterWorkOrderViewAll(getActivity(), FragmentWorkOrderInfo.this, getString(R.string.note)));
                    } else if (listRelatedWorkOrderNotes.size() == 0) {
                        phvRecentWorkOrdersRelated.
                                addChildView(6, new AdapterWorkOrderAddItem(getActivity(), FragmentWorkOrderInfo.this, getString(R.string.note)));
                    }
                }
            }
        });

        buttonAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isAccountClicked = true;
                layoutAccountAdd.setVisibility(View.VISIBLE);
                listAccounts.clear();
                getAccounts();
            }
        });

        edittextAccountAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvAccountAdd.removeAllViews();
                    ArrayList<ModelSpinner> modle = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listAccounts) {
                        if (m.getName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvAccountAdd
                                .addView(new AdapterAccount(getActivity(), FragmentWorkOrderInfo.this, i, modle.get(i)));
                    }
                } else {
                    phvAccountAdd.removeAllViews();
                    for (int i = 0; i < listAccounts.size(); i++) {
                        phvAccountAdd
                                .addView(new AdapterAccount(getActivity(), FragmentWorkOrderInfo.this, i, listAccounts.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        buttonContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isAccountClicked = false;
                isContactClicked = true;
                isWorkOrderClicked = false;
                layoutContactAdd.setVisibility(View.VISIBLE);
                listPrimaryContacts.clear();
                getContacts();
            }
        });

        edittextContactAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvContactAdd.removeAllViews();
                    ArrayList<ModelSpinner> modle = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listPrimaryContacts) {
                        if (m.getName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvContactAdd
                                .addView(new AdapterAccount(getActivity(), FragmentWorkOrderInfo.this, i, modle.get(i)));
                    }
                } else {
                    phvContactAdd.removeAllViews();
                    for (int i = 0; i < listPrimaryContacts.size(); i++) {
                        phvContactAdd
                                .addView(new AdapterAccount(getActivity(), FragmentWorkOrderInfo.this, i, listPrimaryContacts.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        buttonWorkOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isAccountClicked = false;
                isContactClicked = false;
                isWorkOrderClicked = true;
                layoutWorkOrderAdd.setVisibility(View.VISIBLE);
                listParentWorkOrders.clear();
                getWorkOrders();
            }
        });

        edittextWorkOrderAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvWorkOrderAdd.removeAllViews();
                    ArrayList<ModelSpinner> modle = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listParentWorkOrders) {
                        if (m.getName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvWorkOrderAdd
                                .addView(new AdapterAccount(getActivity(), FragmentWorkOrderInfo.this, i, modle.get(i)));
                    }
                } else {
                    phvWorkOrderAdd.removeAllViews();
                    for (int i = 0; i < listParentWorkOrders.size(); i++) {
                        phvWorkOrderAdd
                                .addView(new AdapterAccount(getActivity(), FragmentWorkOrderInfo.this, i, listParentWorkOrders.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        edittextStartDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                final int hour = calendar.get(Calendar.HOUR);
                final int minute = calendar.get(Calendar.MINUTE);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hr, int min) {
                                isStartDateClicked = true;
                                Calendar c = Calendar.getInstance();
                                c.set(0, 0, 0, hr, min);
                                String AM_PM = (String) android.text.format.DateFormat.format("aaa", c);
                                if (hr > 12) {
                                    hr = hr - 12;
                                }
                                strNewStartDateTimeDate = setDate(min, hr, d, m, y, AM_PM);
                                strNewStartDateTimeTime = setTime(min, hr, AM_PM, 0);
                                edittextStartDateTime.setText(formatDate(strNewStartDateTimeDate));
                            }
                        }, hour, minute, false);
                        timePickerDialog.show();
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        edittextEndDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                final int hour = calendar.get(Calendar.HOUR);
                final int minute = calendar.get(Calendar.MINUTE);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hr, int min) {
                                isEndDateClicked = true;
                                Calendar c = Calendar.getInstance();
                                c.set(0, 0, 0, hr, min);
                                String AM_PM = (String) android.text.format.DateFormat.format("aaa", c);
                                if (hr > 12) {
                                    hr = hr - 12;
                                }
                                strNewEndDateTimeDate = setDate(min, hr, d, m, y, AM_PM);
                                strNewEndDateTimeTime = setTime(min, hr, AM_PM, 0);
                                edittextEndDateTime.setText(formatDate(strNewEndDateTimeDate));
                            }
                        }, hour, minute, false);
                        timePickerDialog.show();
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        buttonBilAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!checkPermission()) {
                        requestPermission();
                    } else {
                        isBilClicked = true;
                        double lat = Double.valueOf(strBilLat);
                        double lng = Double.valueOf(strBilLng);
                        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                                .withLocation(lat, lng)
                                .withGeolocApiKey(getString(R.string.google_maps_key))
                                .withGooglePlacesEnabled()
                                .withSatelliteViewHidden()
                                .withVoiceSearchHidden()
                                .shouldReturnOkOnBackPressed()
                                .build(getActivity());
                        startActivityForResult(locationPickerIntent, 99);
                    }
                } else {
                    isBilClicked = true;
                    double lat = Double.valueOf(strBilLat);
                    double lng = Double.valueOf(strBilLng);
                    Intent locationPickerIntent = new LocationPickerActivity.Builder()
                            .withLocation(lat, lng)
                            .withGeolocApiKey(getString(R.string.google_maps_key))
                            .withGooglePlacesEnabled()
                            .withSatelliteViewHidden()
                            .withVoiceSearchHidden()
                            .shouldReturnOkOnBackPressed()
                            .build(getActivity());
                    startActivityForResult(locationPickerIntent, 99);
                }
            }
        });

        listRecurrenceRepeat = new ArrayList<>();
        listRecurrenceRepeat.add("Daily");
        listRecurrenceRepeat.add("Weekly");
        listRecurrenceRepeat.add("Monthly");
        listRecurrenceRepeat.add("Yearly");
        listRecurrenceRepeat.add("Periodically");
        listRecurrenceInterval = new ArrayList<>();
        listRecurrenceInterval.add("1");
        listRecurrenceInterval.add("2");
        listRecurrenceInterval.add("3");
        listRecurrenceInterval.add("4");
        listRecurrenceInterval.add("5");
        listRecurrenceEndAfter = new ArrayList<>();
        listRecurrenceEndAfter.add("1");
        listRecurrenceEndAfter.add("2");
        listRecurrenceEndAfter.add("3");
        listRecurrenceEndAfter.add("4");
        listRecurrenceEndAfter.add("5");
        adapterRecurrenceRepeat = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listRecurrenceRepeat);
        adapterRecurrenceRepeat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterRecurrenceInterval = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listRecurrenceInterval);
        adapterRecurrenceInterval.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterRecurrenceEndAfter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listRecurrenceEndAfter);
        adapterRecurrenceEndAfter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        textviewStartOnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                final int day = calendar.get(Calendar.DATE);
                final int month = calendar.get(Calendar.MONTH);
                final int year = calendar.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        isStartDateOnClicked = true;
                        strStartOnNew = selectSimpleDate(d, m, y);
                        textviewStartOnDate.setText(formatSimpleDate(strStartOnNew));
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        spinnerRepeatsEvery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 1) {
                    layoutRecurrenceRepeatOn.setVisibility(View.VISIBLE);
                } else {
                    layoutRecurrenceRepeatOn.setVisibility(View.GONE);
                }
                textviewRepeatsEveryPostfix.setText(listRecurrenceRepeat.get(position) + " On");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        spinnerIntervalEvery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                isIntervalClicked = true;
                strNewIntervalEvery = listRecurrenceInterval.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        checkboxSun.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strSun = "Sun";
                } else {
                    strSun = "";
                }
            }
        });

        checkboxMon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strMon = "Mon";
                } else {
                    strMon = "";
                }
            }
        });

        checkboxTue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strTue = "Tue";
                } else {
                    strTue = "";
                }
            }
        });

        checkboxWed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strWed = "Wed";
                } else {
                    strWed = "";
                }
            }
        });

        checkboxThu.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strThu = "Thu";
                } else {
                    strThu = "";
                }
            }
        });

        checkboxFri.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strFri = "Fri";
                } else {
                    strFri = "";
                }
            }
        });

        checkboxSat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strSat = "Sat";
                } else {
                    strSat = "";
                }
            }
        });

        radioButtonRecurrenceNever.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strEnds = "Never";
                    strEndsOnNew = "";
                    isEndDateOnClicked = false;
                    textviewEndsOnDate.setVisibility(View.GONE);
                    spinnerEndsAfter.setEnabled(false);
                    layoutEndAfter.setVisibility(View.GONE);
                    textviewEndsAfterPostfix.setVisibility(View.GONE);
                }
            }
        });

        radioButtonRecurrenceOn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strEnds = "On";
                    strEndsOnNew = "";
                    isEndDateOnClicked = false;
                    textviewEndsOnDate.setVisibility(View.VISIBLE);
                    if (isEndDateOnClicked) {
                        textviewEndsOnDate.setText(formatSimpleDate(strEndsOnNew));
                    } else {
                        Calendar calendar = Calendar.getInstance();
                        int day = calendar.get(Calendar.DATE);
                        int month = calendar.get(Calendar.MONTH);
                        int year = calendar.get(Calendar.YEAR);
                        strEndsOnNew = selectSimpleDate(day, month, year);
                        textviewEndsOnDate.setText(formatSimpleDate(strEndsOnNew));
                    }
                    spinnerEndsAfter.setEnabled(false);
                    layoutEndAfter.setVisibility(View.GONE);
                    textviewEndsAfterPostfix.setVisibility(View.GONE);
                }
            }
        });

        radioButtonRecurrenceAfter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strEnds = "After";
                    strEndsOn = "";
                    strEndsOnNew = "";
                    isEndDateOnClicked = false;

                    spinnerEndsAfter.setAdapter(adapterRecurrenceEndAfter);
                    spinnerEndsAfter.setSelection(0);
                    setStrSpinnerDropDownHeight(spinnerEndsAfter);
                    strEndsAfter = listRecurrenceEndAfter.get(0);

                    textviewEndsOnDate.setVisibility(View.GONE);
                    spinnerEndsAfter.setEnabled(true);
                    layoutEndAfter.setVisibility(View.VISIBLE);
                    textviewEndsAfterPostfix.setVisibility(View.VISIBLE);
                }
            }
        });

        textviewEndsOnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                final int day = calendar.get(Calendar.DATE);
                final int month = calendar.get(Calendar.MONTH);
                final int year = calendar.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        isEndDateOnClicked = true;
                        strEndsOnNew = selectSimpleDate(d, m, y);
                        textviewEndsOnDate.setText(formatSimpleDate(strEndsOnNew));
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        checkboxIsRecurring.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (layoutSaveCancel.getVisibility() == View.VISIBLE) {
                    if (b) {
                        ActivityMain.isRecurringOn = true;
                        layoutRecurrence.setVisibility(View.VISIBLE);
                        showView(layoutRecurrence);
                        textviewEndsOnDate.setText(formatSimpleDate(strStartOn));
                        spinnerRepeatsEvery.setAdapter(adapterRecurrenceRepeat);
                        spinnerRepeatsEvery.setSelection(listRecurrenceRepeat.indexOf(strRepeatEvery));
                        setStrSpinnerDropDownHeight(spinnerRepeatsEvery);
                        spinnerIntervalEvery.setAdapter(adapterRecurrenceInterval);
                        spinnerIntervalEvery.setSelection(listRecurrenceInterval.indexOf(strIntervalEvery));
                        setStrSpinnerDropDownHeight(spinnerIntervalEvery);
                        spinnerEndsAfter.setAdapter(adapterRecurrenceEndAfter);
                        spinnerEndsAfter.setSelection(listRecurrenceEndAfter.indexOf(strEndsAfter));
                        setStrSpinnerDropDownHeight(spinnerEndsAfter);
                        if (!strRepeatEvery.equals("")) {
                            int position = listRecurrenceRepeat.indexOf(strRepeatEvery);
                            layoutRecurrenceRepeatOn.setVisibility(View.VISIBLE);
                            if (position == 1) {
                                textviewRepeatsEveryPostfix.setText(listRecurrenceRepeat.get(position) + " On");
                                if (!strRepeatOn.equals("")) {
                                    if (strRepeatOn.contains("Sun")) {
                                        checkboxSun.setChecked(true);
                                    }
                                    if (strRepeatOn.contains("Mon")) {
                                        checkboxMon.setChecked(true);
                                    }
                                    if (strRepeatOn.contains("Tue")) {
                                        checkboxTue.setChecked(true);
                                    }
                                    if (strRepeatOn.contains("Wed")) {
                                        checkboxWed.setChecked(true);
                                    }
                                    if (strRepeatOn.contains("Thu")) {
                                        checkboxTue.setChecked(true);
                                    }
                                    if (strRepeatOn.contains("Fri")) {
                                        checkboxFri.setChecked(true);
                                    }
                                    if (strRepeatOn.contains("Sat")) {
                                        checkboxSat.setChecked(true);
                                    }
                                }
                            } else if (strRepeatEvery.equals(listRecurrenceRepeat.get(position))) {
                                textviewRepeatsEveryPostfix.setText(listRecurrenceRepeat.get(position) + " On");
                            }
                        }

                        if (strEnds.equals("On")) {
                            radioButtonRecurrenceOn.setChecked(true);
                            textviewEndsOnDate.setText(formatSimpleDate(strEndsOn));
                            spinnerEndsAfter.setEnabled(false);
                        } else if (strEnds.equals("After")) {
                            radioButtonRecurrenceAfter.setChecked(true);
                            textviewEndsOnDate.setEnabled(false);
                            textviewEndsOnDate.setText("");
                        } else {
                            radioButtonRecurrenceNever.setChecked(true);
                        }
                        textviewRecurrenceStartTime.setText(formatTime(strRecurrenceStartTime));
                        textviewRecurrenceEndTime.setText(formatTime(strRecurrenceEndTime));
                    } else {

                    }
                }
            }
        });

        textviewRecurrenceStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hr, int min) {
                        isRecurrenceStartDateClicked = true;
                        Calendar c = Calendar.getInstance();
                        c.set(0, 0, 0, hr, min);
                        String AM_PM = (String) android.text.format.DateFormat.format("aaa", c);
                        if (hr > 12) {
                            hr = hr - 12;
                        }
                        strRecurrenceNewStartTime = setTime(min, hr, AM_PM, 1);
                        textviewRecurrenceStartTime.setText(strRecurrenceNewStartTime);
                    }
                }, 0, 0, false);
                timePickerDialog.show();
            }
        });

        textviewRecurrenceEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hr, int min) {
                        isRecurrenceEndDateClicked = true;
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(0, 0, 0, hr, min);
                        String AM_PM = (String) android.text.format.DateFormat.format("aaa", calendar);
                        if (hr > 12) {
                            hr = hr - 12;
                        }
                        strRecurrenceNewEndTime = setTime(min, hr, AM_PM, 1);
                        textviewRecurrenceEndTime.setText(strRecurrenceNewEndTime);
                    }
                }, 0, 0, false);
                timePickerDialog.show();
            }
        });

        buttonRecurrenceSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityMain.isRecurringOn = false;
                layoutRecurrence.setVisibility(View.GONE);
            }
        });

        buttonRecurrenceClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityMain.isRecurringOn = false;
                layoutRecurrence.setVisibility(View.GONE);
            }
        });

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonDetails.setEnabled(false);
                radioButtonRelated.setEnabled(false);
                editMode(true, true);
            }
        });

        buttonMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String assignedTo = listAssignedTo.get(spinnerAssignedTo.getSelectedItemPosition()).getId();
                String account = "";
                if (isAccountClicked) {
                    account = strAccountNew;
                } else {
                    account = strAccount;
                }
                String primaryContact = "";
                if (isContactClicked) {
                    primaryContact = strPrimaryContactNew;
                } else {
                    primaryContact = strPrimaryContact;
                }
                String parentWorkOrder = "";
                if (isWorkOrderClicked) {
                    parentWorkOrder = strParentWorkOrderNew;
                } else {
                    parentWorkOrder = strParentWorkOrder;
                }
                String subject = edittextSubject.getText().toString().trim();
                String description = edittextDescription.getText().toString().trim();
                String popUpReminder = edittextPopupReminder.getText().toString().trim();
                String types = listTypes.get(spinnerType.getSelectedItemPosition()).getId();
                String status = listStatus.get(spinnerStatus.getSelectedItemPosition()).getId();
                String priority = listPriorities.get(spinnerPriority.getSelectedItemPosition()).getId();
                String category = listCategories.get(spinnerCategory.getSelectedItemPosition()).getId();
                if (checkboxIsRecurring.isChecked()) {
                    strRecurring = "1";
                } else {
                    strRecurring = "0";

                }
                String startOn = "";
                String repeatEvery = "";
                String intervalEvery = "";
                StringBuilder repeatOn = new StringBuilder();
                String ends = strEnds;
                String endsOn = "";
                String endsAfter = "";
                String recurrenceStartTime = "";
                String recurrenceEndTime = "";
                if (isStartDateOnClicked) {
                    startOn = strStartOnNew;
                } else {
                    startOn = strStartOn;
                }
                if (isRepeatEveryClicked) {
                    repeatEvery = strNewRepeatEvery;
                } else {
                    repeatEvery = strRepeatEvery;
                }
                if (isIntervalClicked) {
                    intervalEvery = strNewIntervalEvery;
                } else {
                    intervalEvery = strIntervalEvery;
                }
                repeatOn.append("");
                if (!strSun.equals("")) {
                    repeatOn.append(strSun).append(",");
                }
                if (!strMon.equals("")) {
                    repeatOn.append(strMon).append(",");
                }
                if (!strTue.equals("")) {
                    repeatOn.append(strTue).append(",");
                }
                if (!strWed.equals("")) {
                    repeatOn.append(strWed).append(",");
                }
                if (!strThu.equals("")) {
                    repeatOn.append(strThu).append(",");
                }
                if (!strFri.equals("")) {
                    repeatOn.append(strFri).append(",");
                }
                if (!strSat.equals("")) {
                    repeatOn.append(strSat).append(",");
                }
                if (repeatOn.toString().endsWith(",")) {
                    String str = repeatOn.substring(0, repeatOn.length() - 1);
                    repeatOn = new StringBuilder(str);
                }
                if (!strEnds.equals("Never")) {
                    if (ends.equals("On")) {
                        if (isEndDateOnClicked) {
                            endsOn = strEndsOnNew;
                        } else {
                            endsOn = strEndsOn;
                        }
                    } else if (ends.equals("After")) {
                        endsAfter = strEndsAfter;
                    }
                } else {
                    endsOn = "";
                    endsAfter = "";
                }
                if (isRecurrenceStartDateClicked) {
                    recurrenceStartTime = strRecurrenceNewStartTime;
                } else {
                    recurrenceStartTime = strRecurrenceStartTime;
                }
                if (isRecurrenceEndDateClicked) {
                    recurrenceEndTime = strRecurrenceNewEndTime;
                } else {
                    recurrenceEndTime = strRecurrenceEndTime;
                }
                String bilLat = "";
                String bilLng = "";
                if (isBilClicked) {
                    bilLat = strNewBilLat;
                    bilLng = strNewBilLng;
                } else {
                    bilLat = strBilLat;
                    bilLng = strBilLng;
                }
                String bilAddress = edittextBilAddress.getText().toString().trim();
                String bilCity = edittextBilCity.getText().toString().trim();
                String bilState = edittextBilState.getText().toString().trim();
                String bilCountry = edittextBilCountry.getText().toString().trim();
                String bilPostal = edittextBilPostal.getText().toString().trim();
                if (TextUtils.isEmpty(subject)) {
                    Utl.showToast(getActivity(), "Enter Subject");
                } else if (TextUtils.isEmpty(description)) {
                    Utl.showToast(getActivity(), "Enter Description");
                } else if (TextUtils.isEmpty(bilAddress)) {
                    Utl.showToast(getActivity(), "Enter Address");
                } else if (TextUtils.isEmpty(bilCity)) {
                    Utl.showToast(getActivity(), "Enter City");
                } else if (TextUtils.isEmpty(bilState)) {
                    Utl.showToast(getActivity(), "Enter State");
                } else if (TextUtils.isEmpty(bilCountry)) {
                    Utl.showToast(getActivity(), "Enter Country");
                } else if (TextUtils.isEmpty(bilPostal)) {
                    Utl.showToast(getActivity(), "Enter Postal Code");
                } else {
                    if (isCloneMode) {
                        String startDate = "";
                        String startTime = "";
                        String endDate = "";
                        String endTime = "";
                        if (isStartDateClicked) {
                            startDate = strNewStartDateTimeDate;
                            startTime = strNewStartDateTimeTime;
                        } else {
                            startDate = strStartDateTimeDate;
                            startTime = strStartDateTimeTime;
                        }
                        if (isEndDateClicked) {
                            endDate = strNewEndDateTimeDate;
                            endTime = strNewEndDateTimeTime;
                        } else {
                            endDate = strEndDateTimeDate;
                            endTime = strEndDateTimeTime;
                        }
                        createWorkOrder(assignedTo,
                                account,
                                primaryContact,
                                parentWorkOrder,
                                subject,
                                description,
                                popUpReminder,
                                types,
                                status,
                                priority,
                                category,
                                startDate,
                                startTime,
                                endDate,
                                endTime,
                                strRecurring, startOn,
                                bilLat, bilLng,
                                bilAddress, bilCity, bilState, bilCountry, bilPostal,
                                repeatEvery, intervalEvery,
                                repeatOn.toString(), ends, endsOn, endsAfter, recurrenceStartTime, recurrenceEndTime);
                    } else {
                        editWorkOrder(workOrderId,
                                assignedTo,
                                account,
                                primaryContact,
                                parentWorkOrder,
                                subject,
                                description,
                                popUpReminder,
                                types,
                                status,
                                priority,
                                category,
                                strRecurring, startOn,
                                bilLat, bilLng,
                                bilAddress, bilCity, bilState, bilCountry, bilPostal,
                                repeatEvery, intervalEvery,
                                repeatOn.toString(), ends, endsOn, endsAfter, recurrenceStartTime, recurrenceEndTime);
                    }
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCloneMode = false;
                editMode(false, true);
                radioButtonDetails.setEnabled(true);
                radioButtonRelated.setEnabled(true);
            }
        });

        buttonProductAddMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenuPopupProductAdd(view);
            }
        });

        buttonProductEditMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenuPopupProductEdit(view);
            }
        });

        layoutProductFilter.setOnClickListener(null);

        buttonProductFilterAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutProductFilter.setVisibility(View.VISIBLE);
                listProductFilterFamily = new ArrayList<>();
                adapterProductFilterFamily = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, listProductFilterFamily);
                adapterProductFilterFamily.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                getProductFamilySpinner();
            }
        });

        buttonProductFilterApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String productFamily = "";
                if (spinnerProductFilterFamily.getSelectedItemPosition() != 0) {
                    productFamily = listProductFilterFamily.get(spinnerProductFilterFamily.getSelectedItemPosition()).getId();
                }
                String productCode = edittextProductFilterCode.getText().toString().trim();
                listProduct.clear();
                phvProductAdd.removeAllViews();
                getRelatedWorkOrderGetProduct(workOrderId, productFamily, productCode);
                spinnerProductFilterFamily.setAdapter(null);
                edittextProductFilterCode.setText("");
                layoutProductFilter.setVisibility(View.GONE);
            }
        });

        buttonProductFilterCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinnerProductFilterFamily.setSelection(0);
                edittextProductFilterCode.setText("");
                layoutProductFilter.setVisibility(View.GONE);
            }
        });

        buttonProductFilterClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinnerProductFilterFamily.setSelection(0);
                edittextProductFilterCode.setText("");
            }
        });

        buttonChemicalEditMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenuPopupChemicalEdit(view);
            }
        });

        edittextProductAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvProductAdd.removeAllViews();
                    ArrayList<ModelProduct> modle = new ArrayList<ModelProduct>();
                    for (ModelProduct m : listProduct) {
                        if (m.getProductName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvProductAdd
                                .addView(new AdapterProductParent(getActivity(), FragmentWorkOrderInfo.this, modle.get(i)));
                        phvProductAdd
                                .addChildView(i, new AdapterProductChild(getActivity(), modle.get(i)));
                    }
                } else {
                    phvProductAdd.removeAllViews();
                    for (int i = 0; i < listProduct.size(); i++) {
                        phvProductAdd
                                .addView(new AdapterProductParent(getActivity(), FragmentWorkOrderInfo.this, listProduct.get(i)));
                        phvProductAdd
                                .addChildView(i, new AdapterProductChild(getActivity(), listProduct.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        edittextChemicalAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvChemicalAdd.removeAllViews();
                    ArrayList<ModelChemical> modle = new ArrayList<ModelChemical>();
                    for (ModelChemical m : listChemical) {
                        if (m.getProductName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvChemicalAdd
                                .addView(new AdapterChemical(getActivity(), FragmentWorkOrderInfo.this, "", modle.get(i)));
                    }
                } else {
                    phvChemicalAdd.removeAllViews();
                    for (int i = 0; i < listChemical.size(); i++) {
                        phvChemicalAdd
                                .addView(new AdapterChemical(getActivity(), FragmentWorkOrderInfo.this, "", listChemical.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        textviewChemicalAddAnother.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutChemicalAdd.setVisibility(View.VISIBLE);
                listChemical.clear();
                getRelatedWorkOrderGetChemical("", workOrderId);
            }
        });

        buttonNewNoteCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutNewNote.setVisibility(View.GONE);
            }
        });

        buttonNewNoteSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String subject = edittextNewNoteSubject.getText().toString().trim();
                String body = edittextNewNoteBody.getText().toString().trim();
                if (TextUtils.isEmpty(subject)) {
                    Utl.showToast(getActivity(), "Enter Subject");
                } else if (TextUtils.isEmpty(body)) {
                    Utl.showToast(getActivity(), "Enter Body");
                } else {
                    addNewNote(subject, body);
                }
            }
        });


        buttonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    if (!TextUtils.isEmpty(strMobileNo)) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + strMobileNo));
                        startActivity(callIntent);
                    } else {
                        Utl.showToast(getActivity(), "No Phone Number Available.");
                    }
                }
            }
        });

        buttonComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    if (!TextUtils.isEmpty(strMobileNo)) {
                        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                        smsIntent.setData(Uri.parse("sms:" + strMobileNo));
                        startActivity(smsIntent);
                    } else {
                        Utl.showToast(getActivity(), "No Phone Number Available.");
                    }
                }
            }
        });

        buttonDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickCrateEvent != null) {
                    clickCrateEvent.callbackWorkOrderCreateEvent(getString(R.string.workorder), workOrderId, strWorkOrderNo);
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99) {
            if (data != null) {
                double latitude = data.getDoubleExtra("latitude", 0.0);
                strNewLat = String.valueOf(latitude);
                double longitude = data.getDoubleExtra("longitude", 0.0);
                strNewLng = String.valueOf(longitude);
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> addresses = new ArrayList<>();
                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                strNewAddress = addresses.get(0).getSubThoroughfare() + ", " + addresses.get(0).getThoroughfare();
                if (addresses.get(0).getLocality() != null) {
                    strNewCity = addresses.get(0).getLocality();
                } else {
                    strNewCity = addresses.get(0).getSubLocality();
                }
                if (addresses.get(0).getAdminArea() != null) {
                    strNewState = addresses.get(0).getAdminArea();
                } else {
                    strNewState = addresses.get(0).getSubAdminArea();
                }
                strNewCountry = addresses.get(0).getCountryName();
                strNewPostal = addresses.get(0).getPostalCode();
                Log.d("TAG_LATITUDE****", strNewLat);
                Log.d("TAG_LONGITUDE****", strNewLng);
                Log.d("TAG_ADDRESS****", strNewAddress);
                Log.d("TAG_CITY****", strNewCity);
                Log.d("TAG_STATE****", strNewState);
                Log.d("TAG_COUNTRY****", strNewCountry);
                Log.d("TAG_POSTAL****", strNewPostal);
                strNewBilLat = strNewLat;
                strNewBilLng = strNewLng;
                edittextBilAddress.setText(strNewAddress);
                edittextBilCity.setText(strNewCity);
                edittextBilState.setText(strNewState);
                edittextBilCountry.setText(strNewCountry);
                edittextBilPostal.setText(strNewPostal);
            } else {
                Log.d("RESULT", "CANCELLED");
            }
        }
    }

    private void editMode(boolean bolEdit, boolean bolResponse) {
        if (!bolEdit) {
            layoutHeader.setVisibility(View.VISIBLE);
            radioGroupMain.setVisibility(View.VISIBLE);
            layoutAssignedTo.setVisibility(View.GONE);
            layoutAccount.setVisibility(View.GONE);
            layoutPrimaryContact.setVisibility(View.GONE);
            layoutParentWorkOrder.setVisibility(View.GONE);
            layoutType.setVisibility(View.GONE);
            layoutStatus.setVisibility(View.GONE);
            layoutPriority.setVisibility(View.GONE);
            layoutCategory.setVisibility(View.GONE);
            spinnerAssignedTo.setVisibility(View.GONE);
            spinnerType.setVisibility(View.GONE);
            spinnerStatus.setVisibility(View.GONE);
            spinnerPriority.setVisibility(View.GONE);
            spinnerCategory.setVisibility(View.GONE);
            spinnerAssignedTo.setAdapter(null);
            spinnerType.setAdapter(null);
            spinnerStatus.setAdapter(null);
            spinnerPriority.setAdapter(null);
            spinnerCategory.setAdapter(null);
            edittextSubject.setVisibility(View.GONE);
            edittextDescription.setVisibility(View.GONE);
            edittextPopupReminder.setVisibility(View.GONE);
            edittextBilAddress.setVisibility(View.GONE);
            edittextBilCity.setVisibility(View.GONE);
            edittextBilState.setVisibility(View.GONE);
            edittextBilCountry.setVisibility(View.GONE);
            edittextBilPostal.setVisibility(View.GONE);
            buttonBilAddress.setVisibility(View.GONE);
            layoutWorkOrderNo.setVisibility(View.VISIBLE);
            textviewStartDate.setVisibility(View.VISIBLE);
            edittextStartDateTime.setVisibility(View.GONE);
            textviewEndDate.setVisibility(View.VISIBLE);
            edittextEndDateTime.setVisibility(View.GONE);
            layoutFinancialInfo.setVisibility(View.VISIBLE);
            layoutSystemInfo.setVisibility(View.VISIBLE);
            layoutSaveCancel.setVisibility(View.GONE);
            layoutNavigation.setVisibility(View.VISIBLE);
            buttonCall.setEnabled(true);
            buttonComment.setEnabled(true);
            buttonDate.setEnabled(true);
            buttonEdit.setEnabled(true);
            textviewHeaderStatus.setVisibility(View.VISIBLE);
            textviewHeaderType.setVisibility(View.VISIBLE);
            textviewWorkOrderNo.setVisibility(View.VISIBLE);
            textviewAssignedTo.setVisibility(View.VISIBLE);
            textviewAccount.setVisibility(View.VISIBLE);
            textviewPrimaryContact.setVisibility(View.VISIBLE);
            textviewParentWorkOrder.setVisibility(View.VISIBLE);
            textviewSubject.setVisibility(View.VISIBLE);
            textviewDescription.setVisibility(View.VISIBLE);
            textviewPopupReminder.setVisibility(View.VISIBLE);
            textviewType.setVisibility(View.VISIBLE);
            textviewStatus.setVisibility(View.VISIBLE);
            textviewPriority.setVisibility(View.VISIBLE);
            textviewCategory.setVisibility(View.VISIBLE);
            textviewBilAddress.setVisibility(View.VISIBLE);
            textviewBilCity.setVisibility(View.GONE);
            textviewBilState.setVisibility(View.GONE);
            textviewBilCountry.setVisibility(View.GONE);
            textviewBilPostal.setVisibility(View.GONE);
            textviewSubTotal.setVisibility(View.VISIBLE);
            textviewDiscount.setVisibility(View.VISIBLE);
            textviewTax.setVisibility(View.VISIBLE);
            textviewTotalPrice.setVisibility(View.VISIBLE);
            textviewGrandTotal.setVisibility(View.VISIBLE);
            imageviewSignature.setVisibility(View.VISIBLE);
            textviewLineItemCount.setVisibility(View.VISIBLE);
            checkboxIsRecurring.setEnabled(false);
            if (bolResponse) {
                setViewInfo();
            }
        } else {
            layoutHeader.setVisibility(View.VISIBLE);
            radioGroupMain.setVisibility(View.VISIBLE);
            layoutAssignedTo.setVisibility(View.VISIBLE);
            layoutAccount.setVisibility(View.VISIBLE);
            layoutPrimaryContact.setVisibility(View.VISIBLE);
            layoutParentWorkOrder.setVisibility(View.VISIBLE);
            layoutType.setVisibility(View.VISIBLE);
            layoutStatus.setVisibility(View.VISIBLE);
            layoutPriority.setVisibility(View.VISIBLE);
            layoutCategory.setVisibility(View.VISIBLE);
            spinnerAssignedTo.setVisibility(View.VISIBLE);
            spinnerType.setVisibility(View.VISIBLE);
            spinnerStatus.setVisibility(View.VISIBLE);
            spinnerPriority.setVisibility(View.VISIBLE);
            spinnerCategory.setVisibility(View.VISIBLE);
            spinnerAssignedTo.setAdapter(adapterAssignedTo);
            spinnerType.setAdapter(adapterType);
            spinnerStatus.setAdapter(adapterStatus);
            spinnerPriority.setAdapter(adapterPriority);
            spinnerCategory.setAdapter(adapterCategory);
            edittextSubject.setVisibility(View.VISIBLE);
            edittextDescription.setVisibility(View.VISIBLE);
            edittextPopupReminder.setVisibility(View.VISIBLE);
            edittextBilAddress.setVisibility(View.VISIBLE);
            edittextBilCity.setVisibility(View.VISIBLE);
            edittextBilState.setVisibility(View.VISIBLE);
            edittextBilCountry.setVisibility(View.VISIBLE);
            edittextBilPostal.setVisibility(View.VISIBLE);
            buttonBilAddress.setVisibility(View.VISIBLE);
            layoutWorkOrderNo.setVisibility(View.GONE);
            layoutDateInfo.setVisibility(View.GONE);
            layoutFinancialInfo.setVisibility(View.GONE);
            layoutSystemInfo.setVisibility(View.GONE);
            layoutSaveCancel.setVisibility(View.VISIBLE);
            layoutNavigation.setVisibility(View.GONE);
            buttonCall.setEnabled(false);
            buttonComment.setEnabled(false);
            buttonDate.setEnabled(false);
            buttonEdit.setEnabled(false);
            textviewHeaderStatus.setVisibility(View.GONE);
            textviewHeaderType.setVisibility(View.GONE);
            textviewWorkOrderNo.setVisibility(View.GONE);
            textviewAssignedTo.setVisibility(View.GONE);
            textviewAccount.setVisibility(View.GONE);
            textviewPrimaryContact.setVisibility(View.GONE);
            textviewParentWorkOrder.setVisibility(View.GONE);
            textviewSubject.setVisibility(View.GONE);
            textviewDescription.setVisibility(View.GONE);
            textviewPopupReminder.setVisibility(View.GONE);
            textviewType.setVisibility(View.GONE);
            textviewStatus.setVisibility(View.GONE);
            textviewPriority.setVisibility(View.GONE);
            textviewCategory.setVisibility(View.GONE);
            textviewBilAddress.setVisibility(View.GONE);
            textviewBilCity.setVisibility(View.VISIBLE);
            textviewBilState.setVisibility(View.VISIBLE);
            textviewBilCountry.setVisibility(View.VISIBLE);
            textviewBilPostal.setVisibility(View.VISIBLE);
            textviewSubTotal.setVisibility(View.GONE);
            textviewDiscount.setVisibility(View.GONE);
            textviewTax.setVisibility(View.GONE);
            textviewTotalPrice.setVisibility(View.GONE);
            textviewGrandTotal.setVisibility(View.GONE);
            imageviewSignature.setVisibility(View.GONE);
            textviewLineItemCount.setVisibility(View.GONE);
            checkboxIsRecurring.setEnabled(true);
            if (bolResponse) {
                setEditInfo();
            }
        }
    }

    private void cloneMode() {
        layoutHeader.setVisibility(View.GONE);
        radioGroupMain.setVisibility(View.GONE);
        layoutAssignedTo.setVisibility(View.VISIBLE);
        layoutAccount.setVisibility(View.VISIBLE);
        layoutPrimaryContact.setVisibility(View.VISIBLE);
        layoutParentWorkOrder.setVisibility(View.VISIBLE);
        layoutType.setVisibility(View.VISIBLE);
        layoutStatus.setVisibility(View.VISIBLE);
        layoutPriority.setVisibility(View.VISIBLE);
        layoutCategory.setVisibility(View.VISIBLE);
        spinnerAssignedTo.setVisibility(View.VISIBLE);
        spinnerType.setVisibility(View.VISIBLE);
        spinnerStatus.setVisibility(View.VISIBLE);
        spinnerPriority.setVisibility(View.VISIBLE);
        spinnerCategory.setVisibility(View.VISIBLE);
        spinnerAssignedTo.setAdapter(adapterAssignedTo);
        spinnerType.setAdapter(adapterType);
        spinnerStatus.setAdapter(adapterStatus);
        spinnerPriority.setAdapter(adapterPriority);
        spinnerCategory.setAdapter(adapterCategory);
        edittextSubject.setVisibility(View.VISIBLE);
        edittextDescription.setVisibility(View.VISIBLE);
        edittextPopupReminder.setVisibility(View.VISIBLE);
        edittextBilAddress.setVisibility(View.VISIBLE);
        edittextBilCity.setVisibility(View.VISIBLE);
        edittextBilState.setVisibility(View.VISIBLE);
        edittextBilCountry.setVisibility(View.VISIBLE);
        edittextBilPostal.setVisibility(View.VISIBLE);
        buttonBilAddress.setVisibility(View.VISIBLE);
        layoutWorkOrderNo.setVisibility(View.GONE);
        layoutDateInfo.setVisibility(View.VISIBLE);
        textviewStartDate.setVisibility(View.GONE);
        edittextStartDateTime.setVisibility(View.VISIBLE);
        textviewEndDate.setVisibility(View.GONE);
        edittextEndDateTime.setVisibility(View.VISIBLE);
        layoutFinancialInfo.setVisibility(View.GONE);
        layoutSystemInfo.setVisibility(View.GONE);
        layoutSaveCancel.setVisibility(View.VISIBLE);
        layoutNavigation.setVisibility(View.GONE);
        buttonCall.setEnabled(false);
        buttonComment.setEnabled(false);
        buttonDate.setEnabled(false);
        buttonEdit.setEnabled(false);
        textviewHeaderStatus.setVisibility(View.GONE);
        textviewHeaderType.setVisibility(View.GONE);
        textviewWorkOrderNo.setVisibility(View.GONE);
        textviewAssignedTo.setVisibility(View.GONE);
        textviewAccount.setVisibility(View.GONE);
        textviewPrimaryContact.setVisibility(View.GONE);
        textviewParentWorkOrder.setVisibility(View.GONE);
        textviewSubject.setVisibility(View.GONE);
        textviewDescription.setVisibility(View.GONE);
        textviewPopupReminder.setVisibility(View.GONE);
        textviewType.setVisibility(View.GONE);
        textviewStatus.setVisibility(View.GONE);
        textviewPriority.setVisibility(View.GONE);
        textviewCategory.setVisibility(View.GONE);
        textviewBilAddress.setVisibility(View.GONE);
        textviewBilCity.setVisibility(View.VISIBLE);
        textviewBilState.setVisibility(View.VISIBLE);
        textviewBilCountry.setVisibility(View.VISIBLE);
        textviewBilPostal.setVisibility(View.VISIBLE);
        textviewSubTotal.setVisibility(View.GONE);
        textviewDiscount.setVisibility(View.GONE);
        textviewTax.setVisibility(View.GONE);
        textviewTotalPrice.setVisibility(View.GONE);
        textviewGrandTotal.setVisibility(View.GONE);
        imageviewSignature.setVisibility(View.GONE);
        textviewLineItemCount.setVisibility(View.GONE);
        checkboxIsRecurring.setEnabled(true);
        setCloneInfo();
    }

    private void setViewInfo() {
        ActivityMain.textviewCenter.setText(strWorkOrderNo);
        textviewHeaderStatus.setText("Status: " + strStatusName);
        textviewHeaderType.setText("Type: " + strTypeName);
        textviewWorkOrderNo.setText(strWorkOrderNo);
        textviewAssignedTo.setText(strAssignedToName);
        textviewAccount.setText(strAccountName);
        textviewPrimaryContact.setText(strPrimaryContactName);
        textviewParentWorkOrder.setText(strParentWorkOrderName);
        textviewSubject.setText(strSubject);
        textviewDescription.setText(strDescription);
        textviewPopupReminder.setText(strPopupReminder);
        textviewType.setText(strTypeName);
        textviewStatus.setText(strStatusName);
        textviewPriority.setText(strPriorityName);
        textviewCategory.setText(strCategoryName);
        textviewStartDate.setText(formatDate(strStartDate));
        textviewEndDate.setText(formatDate(strEndDate));
        if (strRecurring.equals("1")) {
            checkboxIsRecurring.setChecked(true);
        } else {
            checkboxIsRecurring.setChecked(false);
        }
        textviewBilAddress.setText(strBilAddress + "\n" +
                strBilCity + ", " +
                strBilState + ", " +
                strBilPostal + "\n" +
                strBilCountry);
        textviewSubTotal.setText("$ " + strSubTotal);
        textviewDiscount.setText(strDiscount + " %");
        textviewTax.setText("$ " + strTax);
        textviewTotalPrice.setText("$ " + strTotalPrice);
        textviewGrandTotal.setText("$ " + strGrandTotal);
        if (!TextUtils.isEmpty(strSignature)) {
            try {
                byte[] decodedString = Base64.decode(strSignature.split(",")[1], Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                imageviewSignature.setImageBitmap(decodedByte);
            } catch (IllegalArgumentException e) {
            }
        }
        textviewLineItemCount.setText(strLineItemCount);
        textviewCreatedDate.setText(strCreatedDate);
        textviewCreatedBy.setText(strCreatedBy);
        textviewLastModifiedDate.setText(strLastModifiedDate);
        textviewLastModifiedBy.setText(strLastModifiedBy);
    }

    private void setEditInfo() {
        setSpinnerDropDownHeight(listAssignedTo, spinnerAssignedTo);
        setSpinnerDropDownHeight(listTypes, spinnerType);
        setSpinnerDropDownHeight(listStatus, spinnerStatus);
        setSpinnerDropDownHeight(listPriorities, spinnerPriority);
        setSpinnerDropDownHeight(listCategories, spinnerCategory);
        spinnerAssignedTo.setSelection(getListIndex(listAssignedTo, strAssignedToName));
        if (isAccountClicked) {
            edittextAccount.setText(strAccountNameNew);
        } else {
            edittextAccount.setText(strAccountName);
        }
        if (isContactClicked) {
            edittextPrimaryContact.setText(strPrimaryContactNameNew);
        } else {
            edittextPrimaryContact.setText(strPrimaryContactName);
        }
        if (isWorkOrderClicked) {
            edittextParentWorkOrder.setText(strParentWorkOrderNameNew);
        } else {
            edittextParentWorkOrder.setText(strParentWorkOrderName);
        }
        spinnerType.setSelection(getListIndex(listTypes, strTypeName));
        spinnerStatus.setSelection(getListIndex(listStatus, strStatusName));
        spinnerPriority.setSelection(getListIndex(listPriorities, strPriorityName));
        spinnerCategory.setSelection(getListIndex(listCategories, strCategoryName));
        edittextSubject.setText(strSubject);
        edittextDescription.setText(strDescription);
        edittextPopupReminder.setText(strPopupReminder);
        if (strRecurring.equals("1")) {
            checkboxIsRecurring.setChecked(true);
        } else {
            checkboxIsRecurring.setChecked(false);
        }
        edittextBilAddress.setText(strBilAddress);
        edittextBilCity.setText(strBilCity);
        edittextBilState.setText(strBilState);
        edittextBilCountry.setText(strBilCountry);
        edittextBilPostal.setText(strBilPostal);
    }

    private void setCloneInfo() {
        isCloneMode = true;
        ActivityMain.textviewCenter.setText(getString(R.string.work_order_create));
        setSpinnerDropDownHeight(listAssignedTo, spinnerAssignedTo);
        setSpinnerDropDownHeight(listTypes, spinnerType);
        setSpinnerDropDownHeight(listStatus, spinnerStatus);
        setSpinnerDropDownHeight(listPriorities, spinnerPriority);
        setSpinnerDropDownHeight(listCategories, spinnerCategory);
        spinnerAssignedTo.setSelection(getListIndex(listAssignedTo, strAssignedToName));
        if (isAccountClicked) {
            edittextAccount.setText(strAccountNameNew);
        } else {
            edittextAccount.setText(strAccountName);
        }
        if (isContactClicked) {
            edittextPrimaryContact.setText(strPrimaryContactNameNew);
        } else {
            edittextPrimaryContact.setText(strPrimaryContactName);
        }
        if (isWorkOrderClicked) {
            edittextParentWorkOrder.setText(strParentWorkOrderNameNew);
        } else {
            edittextParentWorkOrder.setText(strParentWorkOrderName);
        }
        spinnerType.setSelection(getListIndex(listTypes, strTypeName));
        spinnerStatus.setSelection(getListIndex(listStatus, strStatusName));
        spinnerPriority.setSelection(getListIndex(listPriorities, strPriorityName));
        spinnerCategory.setSelection(getListIndex(listCategories, strCategoryName));
        edittextSubject.setText(strSubject);
        edittextDescription.setText(strDescription);
        edittextPopupReminder.setText(strPopupReminder);
        edittextStartDateTime.setText(formatDate(strStartDate));
        edittextEndDateTime.setText(formatDate(strEndDate));
        if (strRecurring.equals("1")) {
            checkboxIsRecurring.setChecked(true);
        } else {
            checkboxIsRecurring.setChecked(false);
        }
        edittextBilAddress.setText(strBilAddress);
        edittextBilCity.setText(strBilCity);
        edittextBilState.setText(strBilState);
        edittextBilCountry.setText(strBilCountry);
        edittextBilPostal.setText(strBilPostal);
    }

    private void setStrSpinnerDropDownHeight(Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 70);
    }

    private void setSpinnerDropDownHeight(List<ModelSpinner> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 70);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    private void setFilterSpinnerDropDownHeight(List<ModelSpinner> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    public int getListIndex(List<ModelSpinner> list, String name) {
        for (int i = 0; i < list.size(); i++) {
            ModelSpinner model = list.get(i);
            if (name.equals(model.getName())) {
                return i;
            }
        }
        return -1;
    }

    public String selectSimpleDate(int d, int m, int y) {
        String time = "" + y + "-" + (m + 1) + "-" + d;
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String formatSimpleDate(String strDate) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(strDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String formatTime(String time) {
        String inputPattern = "kk:mm:ss";
        String outputPattern = "hh:mm aaa";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String setDate(int min, int hr, int d, int m, int y, String AM_PM) {
        String time = "" + (m + 1) + "/" + d + "/" + y + " " + hr + ":" + min + " " + AM_PM;
        String inputPattern = "MM/dd/yyyy hh:mm aaa";
        String outputPattern = "MM/dd/yyyy hh:mm aaa";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String formatDate(String time) {
        String inputPattern = "MM/dd/yyyy hh:mm aaa";
        String outputPattern = "dd MMM yyyy hh:mm aaa";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String getTime(String dt) {
        String inputPattern = "MM/dd/yyyy hh:mm aaa";
        String outputPattern = "hh:mm aaa";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(dt);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String setTime(int min, int hr, String AM_PM, int format) {
        String time = "" + hr + ":" + min + " " + AM_PM;
        String inputPattern;
        String outputPattern;
        if (format == 0) {
            inputPattern = "hh:mm aaa";
            outputPattern = "hh:mm aaa";
        } else {
            inputPattern = "hh:mm aaa";
            outputPattern = "kk:mm:ss";
        }
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void showMenuPopupProductAdd(View view) {
        final List<HashMap<String, Object>> data = new ArrayList<>();
        HashMap<String, Object> map = new HashMap<>();
        map.put("title", getString(R.string.select));
        map.put("icon", R.drawable.bg_home);
        data.add(map);
        map = new HashMap<>();
        map.put("title", getString(R.string.select_n_add_more));
        map.put("icon", R.drawable.bg_home);
        data.add(map);
        map = new HashMap<>();
        map.put("title", getString(R.string.cancel));
        map.put("icon", R.drawable.bg_home);
        data.add(map);
        final ListPopupWindow popupWindow = new ListPopupWindow(getActivity());
        ListAdapter adapter = new SimpleAdapter(
                getActivity(),
                data,
                R.layout.menu_popup,
                new String[]{"title"},
                new int[]{R.id.title});
        popupWindow.setAnchorView(view);
        popupWindow.setAdapter(adapter);
        popupWindow.setContentWidth(dialogWidth(getActivity(), adapter));
        popupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                switch (position) {
                    case 0:
                        phvProductAdd.removeAllViews();
                        layoutProductAdd.setVisibility(View.GONE);
                        layoutProductEdit.setVisibility(View.VISIBLE);
                        for (int i = 0; i < Singleton.getInstance().listWorkOrderProduct.size(); i++) {
                            LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View v = layoutInflater.inflate(R.layout.layout_product_edit_change, null);
                            layoutProductEditChange.addView(v);
                            int size = layoutProductEditChange.getChildCount();
                            View viw = layoutProductEditChange.getChildAt(size - 1);
                            final CheckBox checkboxProductId = (CheckBox) viw.findViewById(R.id.checkbox_product_id);
                            final ToggleButton toggleIcon = (ToggleButton) viw.findViewById(R.id.toggle_icon);
                            final TextView textviewName = (TextView) viw.findViewById(R.id.textview_name);
                            final TextView textviewQuantity = (TextView) viw.findViewById(R.id.textview_quantity);
                            final TextView textviewNetTotal = (TextView) viw.findViewById(R.id.textview_net_total);
                            final LinearLayout layoutToggle = (LinearLayout) viw.findViewById(R.id.layout_toggle);
                            final EditText edittextListPrice = (EditText) viw.findViewById(R.id.edittext_list_price);
                            final EditText edittextDiscount = (EditText) viw.findViewById(R.id.edittext_discount);
                            final EditText edittextUnitPrice = (EditText) viw.findViewById(R.id.edittext_unit_price);
                            final EditText edittextQuantity = (EditText) viw.findViewById(R.id.edittext_quantity);
                            final EditText edittextSubTotal = (EditText) viw.findViewById(R.id.edittext_sub_total);
                            final CheckBox checkboxTexable = (CheckBox) viw.findViewById(R.id.checkbox_texable);
                            final EditText edittextNetTotal = (EditText) viw.findViewById(R.id.edittext_net_total);
                            checkboxProductId.setChecked(false);
                            checkboxProductId.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                                    if (isChecked) {
                                        listProductEditChange.add(Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getProductID());
                                    } else {
                                        int idx = listProductEditChange.indexOf(Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getProductID());
                                        listProductEditChange.remove(idx);
                                    }
                                }
                            });
                            layoutToggle.setVisibility(View.GONE);
                            toggleIcon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                                    if (isChecked) {
                                        layoutToggle.setVisibility(View.VISIBLE);
                                        textviewQuantity.setVisibility(View.GONE);
                                        textviewNetTotal.setVisibility(View.GONE);
                                    } else {
                                        layoutToggle.setVisibility(View.GONE);
                                        textviewQuantity.setVisibility(View.VISIBLE);
                                        textviewNetTotal.setVisibility(View.VISIBLE);
                                    }
                                }
                            });
                            double listPrice = Double.valueOf(Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getListPrice());
                            double discount = Double.valueOf(Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getDiscount());
                            double unitPrice = getUnitPrice(Double.valueOf(Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getUnitPrice()), discount);
                            int quantity = Integer.valueOf(Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getDefaultQuantity());
                            double subtotal = getSubTotal(listPrice, discount, quantity);
                            int isTaxable = Integer.valueOf(Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getTaxable());
                            final double tax = Double.valueOf(Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getTax());
                            double netTotal = getNetTotal(listPrice, discount, quantity, isTaxable, tax);
                            double grandTotal = 0;
                            for (int k = 0; k < Singleton.getInstance().listWorkOrderProduct.size(); k++) {
                                double net = Double.valueOf(Singleton.getInstance().listWorkOrderProduct.get(k).getNetTotal());
                                grandTotal = grandTotal + net;
                            }
                            textviewProductEditGrandTotal.setText("Grand Total: ($) " + String.format("%.2f", grandTotal));
                            textviewName.setText("Product: " + Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getProductName());
                            textviewQuantity.setText("Quantity: " + String.valueOf(quantity));
                            textviewNetTotal.setText("Net Total: $" + String.format("%.2f", netTotal));
                            edittextListPrice.setText(String.format("%.2f", listPrice));
                            edittextDiscount.setText(String.format("%.2f", discount));
                            edittextUnitPrice.setText(String.format("%.2f", unitPrice));
                            edittextQuantity.setText(String.valueOf(quantity));
                            edittextSubTotal.setText(String.format("%.2f", subtotal));
                            if (isTaxable == 1) {
                                checkboxTexable.setChecked(true);
                            } else {
                                checkboxTexable.setChecked(false);
                            }
                            edittextNetTotal.setText(String.format("%.2f", netTotal));

                            edittextListPrice.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                                }

                                @Override
                                public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                                    String strListPrice = edittextListPrice.getText().toString();
                                    if (TextUtils.isEmpty(strListPrice)) {
                                        strListPrice = "0";
                                        edittextListPrice.setText("0.00");
                                    }
                                    String strDiscount = edittextDiscount.getText().toString();
                                    if (TextUtils.isEmpty(strDiscount)) {
                                        strDiscount = "0";
                                        edittextDiscount.setText("0.00");
                                    }
                                    String strQuantity = edittextQuantity.getText().toString();
                                    if (TextUtils.isEmpty(strQuantity)) {
                                        strQuantity = "0";
                                        edittextQuantity.setText("0");
                                    }
                                    double listPrice = Double.valueOf(strListPrice);
                                    double discount = Double.valueOf(strDiscount);
                                    double unitPrice = getUnitPrice(listPrice, discount);
                                    int quantity = Integer.valueOf(strQuantity);
                                    double subtotal = getSubTotal(listPrice, discount, quantity);
                                    int isTaxable;
                                    if (checkboxTexable.isChecked()) {
                                        isTaxable = 1;
                                    } else {
                                        isTaxable = 0;
                                    }
                                    double netTotal = getNetTotal(listPrice, discount, quantity, isTaxable, tax);
                                    textviewNetTotal.setText("Net Total: $" + String.format("%.2f", netTotal));
                                    edittextUnitPrice.setText(String.format("%.2f", unitPrice));
                                    edittextSubTotal.setText(String.format("%.2f", subtotal));
                                    edittextNetTotal.setText(String.format("%.2f", netTotal));
                                    double grandTotal = 0;
                                    for (int k = 0; k < Singleton.getInstance().listWorkOrderProduct.size(); k++) {
                                        if (k != layoutProductEditChange.indexOfChild(v)) {
                                            double net = Double.valueOf(Singleton.getInstance().listWorkOrderProduct.get(k).getNetTotal());
                                            grandTotal = grandTotal + net;
                                        }
                                    }
                                    textviewProductEditGrandTotal.setText("Grand Total: ($) " + String.format("%.2f", grandTotal + netTotal));
                                    layoutProductEditChange.invalidate();
                                    updateModelProduct(layoutProductEditChange.indexOfChild(v),
                                            Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getProductID(),
                                            Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getProductName(),
                                            String.format("%.2f", listPrice),
                                            String.format("%.2f", discount),
                                            String.format("%.2f", unitPrice),
                                            String.valueOf(quantity),
                                            String.format("%.2f", subtotal),
                                            String.valueOf(isTaxable),
                                            Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getTax(),
                                            String.format("%.2f", netTotal));
                                }

                                @Override
                                public void afterTextChanged(Editable editable) {
                                }
                            });

                            edittextDiscount.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                                }

                                @Override
                                public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                                    String strListPrice = edittextListPrice.getText().toString();
                                    if (TextUtils.isEmpty(strListPrice)) {
                                        strListPrice = "0";
                                        edittextListPrice.setText("0.00");
                                    }
                                    String strDiscount = edittextDiscount.getText().toString();
                                    if (TextUtils.isEmpty(strDiscount)) {
                                        strDiscount = "0";
                                        edittextDiscount.setText("0.00");
                                    }
                                    String strQuantity = edittextQuantity.getText().toString();
                                    if (TextUtils.isEmpty(strQuantity)) {
                                        strQuantity = "0";
                                        edittextQuantity.setText("0");
                                    }
                                    double listPrice = Double.valueOf(strListPrice);
                                    double discount = Double.valueOf(strDiscount);
                                    double unitPrice = getUnitPrice(listPrice, discount);
                                    int quantity = Integer.valueOf(strQuantity);
                                    double subtotal = getSubTotal(listPrice, discount, quantity);
                                    int isTaxable;
                                    if (checkboxTexable.isChecked()) {
                                        isTaxable = 1;
                                    } else {
                                        isTaxable = 0;
                                    }
                                    double netTotal = getNetTotal(listPrice, discount, quantity, isTaxable, tax);
                                    textviewNetTotal.setText("Net Total: $" + String.format("%.2f", netTotal));
                                    edittextUnitPrice.setText(String.format("%.2f", unitPrice));
                                    edittextSubTotal.setText(String.format("%.2f", subtotal));
                                    edittextNetTotal.setText(String.format("%.2f", netTotal));
                                    double grandTotal = 0;
                                    for (int k = 0; k < Singleton.getInstance().listWorkOrderProduct.size(); k++) {
                                        if (k != layoutProductEditChange.indexOfChild(v)) {
                                            double net = Double.valueOf(Singleton.getInstance().listWorkOrderProduct.get(k).getNetTotal());
                                            grandTotal = grandTotal + net;
                                        }
                                    }
                                    textviewProductEditGrandTotal.setText("Grand Total: ($) " + String.format("%.2f", grandTotal + netTotal));
                                    layoutProductEditChange.invalidate();
                                    updateModelProduct(layoutProductEditChange.indexOfChild(v),
                                            Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getProductID(),
                                            Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getProductName(),
                                            String.format("%.2f", listPrice),
                                            String.format("%.2f", discount),
                                            String.format("%.2f", unitPrice),
                                            String.valueOf(quantity),
                                            String.format("%.2f", subtotal),
                                            String.valueOf(isTaxable),
                                            Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getTax(),
                                            String.format("%.2f", netTotal));
                                }

                                @Override
                                public void afterTextChanged(Editable editable) {
                                }
                            });

                            edittextQuantity.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                                }

                                @Override
                                public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                                    String strListPrice = edittextListPrice.getText().toString();
                                    if (TextUtils.isEmpty(strListPrice)) {
                                        strListPrice = "0";
                                        edittextListPrice.setText("0.00");
                                    }
                                    String strDiscount = edittextDiscount.getText().toString();
                                    if (TextUtils.isEmpty(strDiscount)) {
                                        strDiscount = "0";
                                        edittextDiscount.setText("0.00");
                                    }
                                    String strQuantity = edittextQuantity.getText().toString();
                                    if (TextUtils.isEmpty(strQuantity)) {
                                        strQuantity = "0";
                                        edittextQuantity.setText("0");
                                    }
                                    double listPrice = Double.valueOf(strListPrice);
                                    double discount = Double.valueOf(strDiscount);
                                    double unitPrice = getUnitPrice(listPrice, discount);
                                    int quantity = Integer.valueOf(strQuantity);
                                    double subtotal = getSubTotal(listPrice, discount, quantity);
                                    int isTaxable;
                                    if (checkboxTexable.isChecked()) {
                                        isTaxable = 1;
                                    } else {
                                        isTaxable = 0;
                                    }
                                    double netTotal = getNetTotal(listPrice, discount, quantity, isTaxable, tax);
                                    textviewQuantity.setText("Quantity: " + String.valueOf(quantity));
                                    textviewNetTotal.setText("Net Total: $" + String.format("%.2f", netTotal));
                                    edittextSubTotal.setText(String.format("%.2f", subtotal));
                                    edittextNetTotal.setText(String.format("%.2f", netTotal));
                                    double grandTotal = 0;
                                    for (int k = 0; k < Singleton.getInstance().listWorkOrderProduct.size(); k++) {
                                        if (k != layoutProductEditChange.indexOfChild(v)) {
                                            double net = Double.valueOf(Singleton.getInstance().listWorkOrderProduct.get(k).getNetTotal());
                                            grandTotal = grandTotal + net;
                                        }
                                    }
                                    textviewProductEditGrandTotal.setText("Grand Total: ($) " + String.format("%.2f", grandTotal + netTotal));
                                    layoutProductEditChange.invalidate();
                                    updateModelProduct(layoutProductEditChange.indexOfChild(v),
                                            Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getProductID(),
                                            Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getProductName(),
                                            String.format("%.2f", listPrice),
                                            String.format("%.2f", discount),
                                            String.format("%.2f", unitPrice),
                                            String.valueOf(quantity),
                                            String.format("%.2f", subtotal),
                                            String.valueOf(isTaxable),
                                            Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getTax(),
                                            String.format("%.2f", netTotal));
                                }

                                @Override
                                public void afterTextChanged(Editable editable) {

                                }
                            });

                            checkboxTexable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                                    int isTaxable;
                                    if (isChecked) {
                                        isTaxable = 1;
                                    } else {
                                        isTaxable = 0;
                                    }
                                    String strListPrice = edittextListPrice.getText().toString();
                                    if (TextUtils.isEmpty(strListPrice)) {
                                        strListPrice = "0";
                                        edittextListPrice.setText("0.00");
                                    }
                                    String strDiscount = edittextDiscount.getText().toString();
                                    if (TextUtils.isEmpty(strDiscount)) {
                                        strDiscount = "0";
                                        edittextDiscount.setText("0.00");
                                    }
                                    String strQuantity = edittextQuantity.getText().toString();
                                    if (TextUtils.isEmpty(strQuantity)) {
                                        strQuantity = "0";
                                        edittextQuantity.setText("0");
                                    }
                                    double listPrice = Double.valueOf(strListPrice);
                                    double discount = Double.valueOf(strDiscount);
                                    double unitPrice = getUnitPrice(listPrice, discount);
                                    int quantity = Integer.valueOf(strQuantity);
                                    double subtotal = getSubTotal(listPrice, discount, quantity);
                                    double netTotal = getNetTotal(listPrice, discount, quantity, isTaxable, tax);
                                    textviewNetTotal.setText("Net Total: $" + String.format("%.2f", netTotal));
                                    edittextNetTotal.setText(String.format("%.2f", netTotal));
                                    double grandTotal = 0;
                                    for (int k = 0; k < Singleton.getInstance().listWorkOrderProduct.size(); k++) {
                                        if (k != layoutProductEditChange.indexOfChild(v)) {
                                            double net = Double.valueOf(Singleton.getInstance().listWorkOrderProduct.get(k).getNetTotal());
                                            grandTotal = grandTotal + net;
                                        }
                                    }
                                    textviewProductEditGrandTotal.setText("Grand Total: ($) " + String.format("%.2f", grandTotal + netTotal));
                                    layoutProductEditChange.invalidate();
                                    updateModelProduct(layoutProductEditChange.indexOfChild(v),
                                            Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getProductID(),
                                            Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getProductName(),
                                            String.format("%.2f", listPrice),
                                            String.format("%.2f", discount),
                                            String.format("%.2f", unitPrice),
                                            String.valueOf(quantity),
                                            String.format("%.2f", subtotal),
                                            String.valueOf(isTaxable),
                                            Singleton.getInstance().listWorkOrderProduct.get(layoutProductEditChange.indexOfChild(v)).getTax(),
                                            String.format("%.2f", netTotal));
                                }
                            });
                        }
                        popupWindow.dismiss();
                        break;
                    case 1:
                        addWorkOrderLineItem(0, false, Singleton.getInstance().listWorkOrderProduct);
                        popupWindow.dismiss();
                        break;
                    case 2:
                        listProduct.clear();
                        Singleton.getInstance().listWorkOrderProduct.clear();
                        phvProductAdd.removeAllViews();
                        layoutProductAdd.setVisibility(View.GONE);
                        popupWindow.dismiss();
                        break;
                }
            }
        });
        popupWindow.show();
    }

    private void showMenuPopupProductEdit(View view) {
        final List<HashMap<String, Object>> data = new ArrayList<>();
        HashMap<String, Object> map = new HashMap<>();
        map.put("title", getString(R.string.save));
        map.put("icon", R.drawable.bg_home);
        data.add(map);
        map = new HashMap<>();
        map.put("title", getString(R.string.quick_select));
        map.put("icon", R.drawable.bg_home);
        data.add(map);
        map = new HashMap<>();
        map.put("title", getString(R.string.add_products));
        map.put("icon", R.drawable.bg_home);
        data.add(map);
        map = new HashMap<>();
        map.put("title", getString(R.string.delete_lines));
        map.put("icon", R.drawable.bg_home);
        data.add(map);
        map = new HashMap<>();
        map.put("title", getString(R.string.cancel));
        map.put("icon", R.drawable.bg_home);
        data.add(map);
        final ListPopupWindow popupWindow = new ListPopupWindow(getActivity());
        ListAdapter adapter = new SimpleAdapter(
                getActivity(),
                data,
                R.layout.menu_popup,
                new String[]{"title"},
                new int[]{R.id.title});
        popupWindow.setAnchorView(view);
        popupWindow.setAdapter(adapter);
        popupWindow.setContentWidth(dialogWidth(getActivity(), adapter));
        popupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        addWorkOrderLineItem(1, false, Singleton.getInstance().listWorkOrderProduct);
                        popupWindow.dismiss();
                        break;
                    case 1:
                        addWorkOrderLineItem(1, true, Singleton.getInstance().listWorkOrderProduct);
                        popupWindow.dismiss();
                        break;
                    case 2:
                        layoutProductEditChange.removeAllViews();
                        layoutProductEdit.setVisibility(View.GONE);
                        layoutProductAdd.setVisibility(View.VISIBLE);
                        for (int i = 0; i < listProduct.size(); i++) {
                            phvProductAdd
                                    .addView(new AdapterProductParent(getActivity(), FragmentWorkOrderInfo.this, listProduct.get(i)));
                            phvProductAdd
                                    .addChildView(i, new AdapterProductChild(getActivity(), listProduct.get(i)));
                        }
                        popupWindow.dismiss();
                        break;
                    case 3:
                        for (int i = 0; i < Singleton.getInstance().listWorkOrderProduct.size(); i++) {
                            String strId = Singleton.getInstance().listWorkOrderProduct.get(i).getProductID();
                            if (listProductEditChange.contains(strId)) {
                                Singleton.getInstance().listWorkOrderProduct.remove(i);
                                layoutProductEditChange.removeViewAt(i);
                            }
                        }
                        layoutProductEditChange.invalidate();
                        double grandTotal = 0;
                        for (int k = 0; k < Singleton.getInstance().listWorkOrderProduct.size(); k++) {
                            double net = Double.valueOf(Singleton.getInstance().listWorkOrderProduct.get(k).getNetTotal());
                            grandTotal = grandTotal + net;
                        }
                        textviewProductEditGrandTotal.setText("Grand Total: ($) " + String.format("%.2f", grandTotal));
                        popupWindow.dismiss();
                        break;
                    case 4:
                        listProduct.clear();
                        listProductEditChange.clear();
                        Singleton.getInstance().listWorkOrderProduct.clear();
                        layoutProductEditChange.removeAllViews();
                        layoutProductEdit.setVisibility(View.GONE);
                        popupWindow.dismiss();
                        break;
                }
            }
        });
        popupWindow.show();
    }

    private void showMenuPopupChemicalEdit(View view) {
        final List<HashMap<String, Object>> data = new ArrayList<>();
        HashMap<String, Object> map = new HashMap<>();
        map.put("title", getString(R.string.save));
        map.put("icon", R.drawable.bg_home);
        data.add(map);

        map = new HashMap<>();
        map.put("title", getString(R.string.quick_save));
        map.put("icon", R.drawable.bg_home);
        data.add(map);

        map = new HashMap<>();
        map.put("title", getString(R.string.delete_lines));
        map.put("icon", R.drawable.bg_home);
        data.add(map);

        map = new HashMap<>();
        map.put("title", getString(R.string.cancel));
        map.put("icon", R.drawable.bg_home);
        data.add(map);

        final ListPopupWindow popupWindow = new ListPopupWindow(getActivity());
        ListAdapter adapter = new SimpleAdapter(
                getActivity(),
                data,
                R.layout.menu_popup,
                new String[]{"title"},
                new int[]{R.id.title});

        popupWindow.setAnchorView(view);
        popupWindow.setAdapter(adapter);
        popupWindow.setContentWidth(dialogWidth(getActivity(), adapter));
        popupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        addWorkOrderChemical(false, Singleton.getInstance().listWorkOrderChemical);
                        popupWindow.dismiss();
                        break;
                    case 1:
                        addWorkOrderChemical(true, Singleton.getInstance().listWorkOrderChemical);
                        popupWindow.dismiss();
                        break;
                    case 2:
                        for (int i = 0; i < Singleton.getInstance().listWorkOrderChemical.size(); i++) {
                            String strId = Singleton.getInstance().listWorkOrderChemical.get(i).getProductID();
                            if (listChemicalEditChange.contains(strId)) {
                                Singleton.getInstance().listWorkOrderChemical.remove(i);
                                layoutChemicalEditChange.removeViewAt(i);
                            }
                        }
                        layoutChemicalEditChange.invalidate();
                        popupWindow.dismiss();
                        break;
                    case 3:
                        listChemical.clear();
                        listChemicalEdit.clear();
                        listChemicalEditChange.clear();
                        Singleton.getInstance().listWorkOrderChemical.clear();
                        layoutChemicalEditChange.removeAllViews();
                        layoutChemicalEdit.setVisibility(View.GONE);
                        popupWindow.dismiss();
                        break;
                }
            }
        });
        popupWindow.show();
    }

    private int dialogWidth(Context c, ListAdapter la) {
        ViewGroup viewGroup = null;
        int maxWidth = 0;
        View itemView = null;
        int itemType = 0;
        final ListAdapter adapter = la;
        final int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int count = adapter.getCount();
        for (int i = 0; i < count; i++) {
            final int positionType = adapter.getItemViewType(i);
            if (positionType != itemType) {
                itemType = positionType;
                itemView = null;
            }
            if (viewGroup == null) {
                viewGroup = new FrameLayout(c);
            }
            itemView = adapter.getView(i, itemView, viewGroup);
            itemView.measure(widthMeasureSpec, heightMeasureSpec);
            final int itemWidth = itemView.getMeasuredWidth();
            if (itemWidth > maxWidth) {
                maxWidth = itemWidth;
            }
        }
        return maxWidth;
    }

    private double getUnitPrice(double listPrice, double discount) {
        double subPrice, totalPrice;
        subPrice = listPrice;
        totalPrice = subPrice - ((subPrice / 100.0f) * discount);
        return totalPrice;
    }

    private double getSubTotal(double listPrice, double discount, int quantity) {
        double subTotal, total;
        subTotal = listPrice * quantity;
        total = subTotal - ((subTotal / 100.0f) * discount);
        return total;
    }

    private double getNetTotal(double listPrice, double discount, int quantity, int isTaxable, double tax) {
        double subTotal, netTotal, total = 0;
        subTotal = listPrice * quantity;
        netTotal = subTotal - ((subTotal / 100.0f) * discount);
        if (isTaxable == 1) {
            total = netTotal + ((netTotal / 100.0f) * tax);
        } else {
            total = netTotal;
        }
        return total;
    }

    private void updateModelProduct(int position,
                                    String productId,
                                    String productName,
                                    String listPrice,
                                    String discount,
                                    String unitPrice,
                                    String quantity,
                                    String subTotal,
                                    String taxable,
                                    String tax,
                                    String netTotal) {
        ModelProduct mdl = new ModelProduct();
        mdl.setProductID(productId);
        mdl.setProductName(productName);
        mdl.setListPrice(listPrice);
        mdl.setDiscount(discount);
        mdl.setUnitPrice(unitPrice);
        mdl.setDefaultQuantity(quantity);
        mdl.setSubTotal(subTotal);
        mdl.setTaxable(taxable);
        mdl.setTax(tax);
        mdl.setNetTotal(netTotal);
        Singleton.getInstance().listWorkOrderProduct.set(position, mdl);
    }

    private void updateModelChemical(int position,
                                     String productId,
                                     String testConcentration,
                                     String testedUnitOfMeasure,
                                     String applicationAmount,
                                     String applicationUnitOfMeasure,
                                     String applicationArea,
                                     String additionalNotes) {
        ModelChemical mdl = new ModelChemical();
        mdl.setProductID(productId);
        mdl.setTestConcentration(testConcentration);
        mdl.setTestedUnitOfMeasure(testedUnitOfMeasure);
        mdl.setApplicationAmount(applicationAmount);
        mdl.setApplicationUnitOfMeasure(applicationUnitOfMeasure);
        mdl.setApplicationArea(applicationArea);
        mdl.setAdditionalNotes(additionalNotes);
        Singleton.getInstance().listWorkOrderChemical.set(position, mdl);
    }

    private void dialogMore() {
        final Dialog dialogMore = new Dialog(getActivity(), R.style.DialogFullScreen);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMore.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        dialogMore.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMore.setCancelable(true);
        dialogMore.setCanceledOnTouchOutside(true);
        dialogMore.setContentView(R.layout.dialog_more);

        TextView buttonMoreCall = (TextView) dialogMore.findViewById(R.id.button_more_call);
        TextView buttonMoreText = (TextView) dialogMore.findViewById(R.id.button_more_text);
        TextView buttonMoreEmail = (TextView) dialogMore.findViewById(R.id.button_more_edit);
        TextView buttonMoreEdit = (TextView) dialogMore.findViewById(R.id.button_more_email);
        TextView buttonMoreSign = (TextView) dialogMore.findViewById(R.id.button_more_sign);
        TextView buttonMoreDoc = (TextView) dialogMore.findViewById(R.id.button_more_doc);
        TextView buttonMoreNewLine = (TextView) dialogMore.findViewById(R.id.button_more_new_line);
        TextView buttonMoreChemical = (TextView) dialogMore.findViewById(R.id.button_more_new_chemical);
        TextView buttonMoreEvent = (TextView) dialogMore.findViewById(R.id.button_more_new_event);
        TextView buttonMoreInvoice = (TextView) dialogMore.findViewById(R.id.button_more_new_invoice);
        TextView buttonMoreFile = (TextView) dialogMore.findViewById(R.id.button_more_new_file);
        TextView buttonMoreNote = (TextView) dialogMore.findViewById(R.id.button_more_new_note);
        TextView buttonMoreTask = (TextView) dialogMore.findViewById(R.id.button_more_new_task);
        TextView buttonMoreConvert = (TextView) dialogMore.findViewById(R.id.button_more_convert);
        TextView buttonMoreClone = (TextView) dialogMore.findViewById(R.id.button_more_clone);
        TextView buttonMoreDelete = (TextView) dialogMore.findViewById(R.id.button_more_delete);
        View buttomViewCall = (View) dialogMore.findViewById(R.id.view_more_call);
        View buttomViewText = (View) dialogMore.findViewById(R.id.view_more_text);
        View buttomViewEmail = (View) dialogMore.findViewById(R.id.view_more_email);
        View buttomViewEdit = (View) dialogMore.findViewById(R.id.view_more_edit);
        View buttomViewSign = (View) dialogMore.findViewById(R.id.view_more_sign);
        View buttomViewDoc = (View) dialogMore.findViewById(R.id.view_more_doc);
        View buttomViewNewLine = (View) dialogMore.findViewById(R.id.view_more_new_line);
        View buttomViewChemical = (View) dialogMore.findViewById(R.id.view_more_new_chemical);
        View buttomViewEvent = (View) dialogMore.findViewById(R.id.view_more_new_event);
        View buttomViewInvoice = (View) dialogMore.findViewById(R.id.view_more_new_invoice);
        View buttomViewFile = (View) dialogMore.findViewById(R.id.view_more_new_file);
        View buttomViewNote = (View) dialogMore.findViewById(R.id.view_more_new_note);
        View buttomViewTask = (View) dialogMore.findViewById(R.id.view_more_new_task);
        View buttomViewConvert = (View) dialogMore.findViewById(R.id.view_more_convert);
        View buttomViewClone = (View) dialogMore.findViewById(R.id.view_more_clone);
        Button buttonMoreClose = (Button) dialogMore.findViewById(R.id.button_more_close);

        buttonMoreClone.setText(getString(R.string.clone_work_order));
        buttonMoreDelete.setText(getString(R.string.delete_work_order));

        buttonMoreCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    if (!TextUtils.isEmpty(strMobileNo)) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + strMobileNo));
                        startActivity(callIntent);
                    } else {
                        Utl.showToast(getActivity(), "No Phone Number Available.");
                    }
                }
            }
        });

        buttonMoreText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    if (!TextUtils.isEmpty(strMobileNo)) {
                        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                        smsIntent.setData(Uri.parse("sms:" + strMobileNo));
                        startActivity(smsIntent);
                    } else {
                        Utl.showToast(getActivity(), "No Phone Number Available.");
                    }
                }
            }
        });

        buttonMoreEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickSendEmail != null) {
                    clickSendEmail.callbackWorkOrderSendEmail(workOrderId, strWorkOrderNo);
                }
                dialogMore.dismiss();
            }
        });

        buttonMoreEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonDetails.setEnabled(false);
                radioButtonRelated.setEnabled(false);
                editMode(true, true);
                dialogMore.dismiss();
            }
        });

        buttonMoreSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickSignature != null) {
                    clickSignature.callbackWorkOrderSignatureListener(workOrderId, strWorkOrderNo);
                }
                dialogMore.dismiss();
            }
        });

        buttonMoreDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogDocument();
                dialogMore.dismiss();
            }
        });

        buttonMoreEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickCrateEvent != null) {
                    clickCrateEvent.callbackWorkOrderCreateEvent(getString(R.string.workorder), workOrderId, strWorkOrderNo);
                }
                dialogMore.dismiss();
            }
        });

        buttonMoreInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                if (clickInvoiceCreate != null) {
                    clickInvoiceCreate.callbackWorkOrderInvoiceCreate();
                }
            }
        });

        buttonMoreFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickCrateFile != null) {
                    clickCrateFile.callbackWorkOrderCreateFile(getString(R.string.work_order));
                }
                dialogMore.dismiss();
            }
        });

        buttonMoreNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutNewNote.setVisibility(View.VISIBLE);
                dialogMore.dismiss();
            }
        });

        buttonMoreTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                if (clickTaskCreate != null) {
                    clickTaskCreate.callbackWorkOrderTaskCreate();
                }
            }
        });

        buttonMoreConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addConvertToInvoice();
                dialogMore.dismiss();
            }
        });

        buttonMoreClone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cloneMode();
                dialogMore.dismiss();
            }
        });

        buttonMoreDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteObject();
                dialogMore.dismiss();
            }
        });

        buttonMoreClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
            }
        });

        dialogMore.show();
        dialogMore.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialogMore.getWindow().setDimAmount(0.5f);
        dialogMore.getWindow().setAttributes(lp);
    }

    private void dialogDocument() {
        final Dialog dialogDocument = new Dialog(getActivity(), R.style.DialogFullScreen);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogDocument.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialogDocument.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogDocument.setCancelable(true);
        dialogDocument.setCanceledOnTouchOutside(true);
        dialogDocument.setContentView(R.layout.dialog_document_more);

        Spinner spinnerTemplate = (Spinner) dialogDocument.findViewById(R.id.spinner_template);
        Spinner spinnerDocFormat = (Spinner) dialogDocument.findViewById(R.id.spinner_document_format);
        List<ModelSpinner> listTemplate = new ArrayList<>();
        List<ModelSpinner> listDocFormat = new ArrayList<>();
        listDocFormat.add(new ModelSpinner("0", "PDF"));
        listDocFormat.add(new ModelSpinner("1", "Word"));
        ArrayAdapter<ModelSpinner> adapterTemplate = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listTemplate);
        adapterTemplate.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<ModelSpinner> adapterDocFormat = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listDocFormat);
        adapterDocFormat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDocFormat.setAdapter(adapterDocFormat);
        getTemplateList(spinnerTemplate, adapterTemplate, listTemplate);
        CheckBox checkboxIsSaveToObject = (CheckBox) dialogDocument.findViewById(R.id.checkbox_is_save_to_object);
        Button buttonSave = (Button) dialogDocument.findViewById(R.id.button_save);
        Button buttonCancel = (Button) dialogDocument.findViewById(R.id.button_cancel);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String templateID = listTemplate.get(spinnerTemplate.getSelectedItemPosition()).getId();
                ;
                String isSaveToObject = "";
                if (checkboxIsSaveToObject.isChecked()) {
                    isSaveToObject = "1";
                } else {
                    isSaveToObject = "0";
                }
                String outputFormat = listDocFormat.get(spinnerDocFormat.getSelectedItemPosition()).getId();
                createDocument(dialogDocument, templateID, isSaveToObject, outputFormat);
            }
        });
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogDocument.dismiss();
            }
        });

        dialogDocument.show();
        dialogDocument.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialogDocument.getWindow().setDimAmount(0.5f);
        dialogDocument.getWindow().setAttributes(lp);
    }

    private void getTemplateList(Spinner spinnerTemplate, ArrayAdapter<ModelSpinner> adapterTemplate, List<ModelSpinner> listTemplate) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), getString(R.string.workorder)));
        Call<ResponseBody> response = apiInterface.getTemplateList(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listTemplate.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_GenDocTemplateID));
                                model.setName(dataObject.getString(Cons.KEY_TemplateName));
                                listTemplate.add(model);
                            }
                            spinnerTemplate.setAdapter(adapterTemplate);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void createDocument(Dialog dialogDocument, String templateID, String isSaveToObject, String outputFormat) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), getString(R.string.workorder)));
        map.put(Cons.KEY_ObjectID, RequestBody.create(MediaType.parse("text/plain"), workOrderId));
        map.put(Cons.KEY_GenDocTemplateID, RequestBody.create(MediaType.parse("text/plain"), templateID));
        map.put(Cons.KEY_SaveToObject, RequestBody.create(MediaType.parse("text/plain"), isSaveToObject));
        map.put(Cons.KEY_OutputFormat, RequestBody.create(MediaType.parse("text/plain"), outputFormat));
        Call<ResponseBody> response = apiInterface.createDocument(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            JSONObject dataObject = object.getJSONObject("data");
                            String generatedDocumentURL = dataObject.getString(Cons.KEY_GenerateDocumentURL);
                            dialogDocument.dismiss();
                            webView.setWebViewClient(new WebViewClient() {
                                @Override
                                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                                    super.onPageStarted(view, url, favicon);
                                }

                                @Override
                                public void onPageFinished(WebView view, String url) {
                                    super.onPageFinished(view, url);
                                    layoutWebView.setVisibility(View.VISIBLE);
                                }
                            });
                            webView.loadUrl("https://docs.google.com/viewer?url=" + generatedDocumentURL);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void deleteObject() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), "WorkOrder"));
        map.put(Cons.KEY_What, RequestBody.create(MediaType.parse("text/plain"), workOrderId));
        Call<ResponseBody> response = apiInterface.deleteObject(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_File_Create" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            clickGoToRecent.callbackGoToRecentWorkOrder(getString(R.string.work_order));
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });

    }

    private void getAllUsers() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getWorkOrderAllUsers(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_AllUsers" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAssignedTo.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_UserID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listAssignedTo.add(model);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getAccounts() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getWorkOrderAccounts(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Accounts" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAccounts.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_AccountID));
                                model.setName(dataObject.getString(Cons.KEY_AccountName));
                                listAccounts.add(model);
                            }
                            for (int i = 0; i < listAccounts.size(); i++) {
                                phvAccountAdd
                                        .addView(new AdapterAccount(getActivity(), FragmentWorkOrderInfo.this, i, listAccounts.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getContacts() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getWorkOrderContacts(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Contacts" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listPrimaryContacts.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_ContactID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listPrimaryContacts.add(model);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getWorkOrders() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getWorkOrderParentWorkOrders(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_ParentWO" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listParentWorkOrders.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_WorkOrderID));
                                model.setName(dataObject.getString(Cons.KEY_Subject));
                                listParentWorkOrders.add(model);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getTypes() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getWorkOrderTypes(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Types" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listTypes.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_WorkOrderTypeID));
                                model.setName(dataObject.getString(Cons.KEY_WorkOrderType));
                                listTypes.add(model);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getStatus() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getWorkOrderStatus(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Status" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listStatus.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_WorkOrderStatusID));
                                model.setName(dataObject.getString(Cons.KEY_Status));
                                listStatus.add(model);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getPriorities() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getWorkOrderPriorities(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Priorities" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listPriorities.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_WorkOrderPriorityID));
                                model.setName(dataObject.getString(Cons.KEY_Priority));
                                listPriorities.add(model);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getCategories() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getWorkOrderCategories(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Categories" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listCategories.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_WorkOrderCategoryID));
                                model.setName(dataObject.getString(Cons.KEY_CategoryName));
                                listCategories.add(model);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getWorkOrderDetailed(final String workOrderId) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_WorkOrderID, RequestBody.create(MediaType.parse("text/plain"), workOrderId));
        Call<ResponseBody> response;
        response = apiInterface.getWorkOrderDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_AccInfo" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            JSONObject dataObject = object.getJSONObject("data");
                            strWorkOrderNo = dataObject.getString(Cons.KEY_WorkOrderNo);
                            strAssignedTo = dataObject.getString(Cons.KEY_AssignedTo);
                            strAssignedToName = dataObject.getString(Cons.KEY_AssignedToName);
                            strAccount = dataObject.getString(Cons.KEY_Account);
                            strAccountName = dataObject.getString(Cons.KEY_AccountName);
                            strPrimaryContact = dataObject.getString(Cons.KEY_PrimaryContact);
                            strPrimaryContactName = dataObject.getString(Cons.KEY_PrimaryContactName);
                            strParentWorkOrder = dataObject.getString(Cons.KEY_ParentWorkOrder);
                            strParentWorkOrderName = dataObject.getString(Cons.KEY_ParentWorkOrderName);
                            strSubject = dataObject.getString(Cons.KEY_Subject);
                            strDescription = dataObject.getString(Cons.KEY_Description);
                            strPopupReminder = dataObject.getString(Cons.KEY_PopupReminder);
                            strType = dataObject.getString(Cons.KEY_WorkOrderType);
                            strTypeName = dataObject.getString(Cons.KEY_WorkOrderTypeName);
                            strStatus = dataObject.getString(Cons.KEY_WorkOrderStatus);
                            strStatusName = dataObject.getString(Cons.KEY_Status);
                            strPriority = dataObject.getString(Cons.KEY_WorkOrderPriority);
                            strPriorityName = dataObject.getString(Cons.KEY_Priority);
                            strCategory = dataObject.getString(Cons.KEY_WorkOrderCategory);
                            strCategoryName = dataObject.getString(Cons.KEY_CategoryName);
                            strStartDate = dataObject.getString(Cons.KEY_StartDate);
                            strEndDate = dataObject.getString(Cons.KEY_EndDate);
                            strRecurring = dataObject.getString(Cons.KEY_IsRecurring);
                            strBilAddress = dataObject.getString(Cons.KEY_Address);
                            strBilCity = dataObject.getString(Cons.KEY_City);
                            strBilState = dataObject.getString(Cons.KEY_State);
                            strBilCountry = dataObject.getString(Cons.KEY_Country);
                            strBilPostal = dataObject.getString(Cons.KEY_PostalCode);
                            strBilLat = dataObject.getString(Cons.KEY_Latitude);
                            strBilLng = dataObject.getString(Cons.KEY_Longitude);
                            double subTotal = Double.valueOf(dataObject.getString(Cons.KEY_SubTotal));
                            strSubTotal = String.format("%.2f", subTotal);
                            double discount = Double.valueOf(dataObject.getString(Cons.KEY_Discount));
                            strDiscount = String.format("%.2f", discount);
                            double tax = Double.valueOf(dataObject.getString(Cons.KEY_Tax));
                            strTax = String.format("%.2f", tax);
                            double totalPrice = Double.valueOf(dataObject.getString(Cons.KEY_TotalPrice));
                            strTotalPrice = String.format("%.2f", totalPrice);
                            double grandTotal = Double.valueOf(dataObject.getString(Cons.KEY_GrandTotal));
                            strGrandTotal = String.format("%.2f", grandTotal);
                            strSignature = dataObject.getString(Cons.KEY_Signature);
                            strLineItemCount = dataObject.getString(Cons.KEY_LineItemCount);
                            strCreatedDate = dataObject.getString(Cons.KEY_CreatedDate);
                            strCreatedBy = dataObject.getString(Cons.KEY_CreatedBy);
                            strLastModifiedDate = dataObject.getString(Cons.KEY_LastModifiedDate);
                            strLastModifiedBy = dataObject.getString(Cons.KEY_LastModifiedBy);
                            editMode(false, true);
                            radioButtonDetails.setEnabled(true);
                            radioButtonRelated.setEnabled(true);
                            strStartOn = dataObject.getString(Cons.KEY_StartOn);
                            strRepeatEvery = dataObject.getString(Cons.KEY_RepeatEvery);
                            strIntervalEvery = dataObject.getString(Cons.KEY_IntervalEvery);
                            strRepeatOn = dataObject.getString(Cons.KEY_RepeatOn);
                            strMobileNo = dataObject.getString(Cons.KEY_MobileNo);
                            strStartDateTimeDate = dataObject.getString(Cons.KEY_StartDate);
                            strStartDateTimeTime = getTime(strStartDateTimeDate);
                            strEndDateTimeDate = dataObject.getString(Cons.KEY_EndDate);
                            strEndDateTimeTime = getTime(strEndDateTimeDate);
                            if (!strRepeatOn.equals("")) {
                                if (strRepeatOn.contains("Sun")) {
                                    strSun = "Sun";
                                } else {
                                    strSun = "";
                                }
                                if (strRepeatOn.contains("Mon")) {
                                    strMon = "Mon";
                                } else {
                                    strMon = "";
                                }
                                if (strRepeatOn.contains("Tue")) {
                                    strTue = "Tue";
                                } else {
                                    strTue = "";
                                }
                                if (strRepeatOn.contains("Wed")) {
                                    strWed = "Wed";
                                } else {
                                    strWed = "";
                                }
                                if (strRepeatOn.contains("Thu")) {
                                    strThu = "Thu";
                                } else {
                                    strThu = "";
                                }
                                if (strRepeatOn.contains("Fri")) {
                                    strFri = "Fri";
                                } else {
                                    strFri = "";
                                }
                                if (strRepeatOn.contains("Sat")) {
                                    strSat = "Sat";
                                } else {
                                    strSat = "";
                                }
                            } else {
                                strSun = "";
                                strMon = "";
                                strTue = "";
                                strWed = "";
                                strThu = "";
                                strFri = "";
                                strSat = "";
                            }
                            strEnds = dataObject.getString(Cons.KEY_Ends);
                            strEndsOn = dataObject.getString(Cons.KEY_EndsOnDate);
                            strEndsAfter = dataObject.getString(Cons.KEY_EndsAfterOccurrences);
                            strRecurrenceStartTime = dataObject.getString(Cons.KEY_StartTime);
                            strRecurrenceEndTime = dataObject.getString(Cons.KEY_EndTime);
                        }
                        if (!TextUtils.isEmpty(strPopupReminder)) {
                            showPopup(strPopupReminder);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void createWorkOrder(String assignedTo,
                                 String account,
                                 String primaryContact,
                                 String parentWorkOrder,
                                 String subject,
                                 String description,
                                 String popUpReminder,
                                 String types,
                                 String status,
                                 String priority,
                                 String category,
                                 String startDate,
                                 String startTime,
                                 String endDate,
                                 String endTime,
                                 String strRecurring, String startOn,
                                 String bilLat, String bilLng,
                                 String bilAddress, String bilCity, String bilState, String bilCountry, String bilPostal,
                                 String repeatEvery, String intervalEvery, String repeatOn,
                                 String ends, String endsOn, String endsAfter,
                                 String recurrenceStartTime, String recurrenceEndTime) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AssignedTo, RequestBody.create(MediaType.parse("text/plain"), assignedTo));
        map.put(Cons.KEY_Account, RequestBody.create(MediaType.parse("text/plain"), account));
        map.put(Cons.KEY_PrimaryContact, RequestBody.create(MediaType.parse("text/plain"), primaryContact));
        map.put(Cons.KEY_ParentWorkOrder, RequestBody.create(MediaType.parse("text/plain"), parentWorkOrder));
        map.put(Cons.KEY_Subject, RequestBody.create(MediaType.parse("text/plain"), subject));
        map.put(Cons.KEY_Description, RequestBody.create(MediaType.parse("text/plain"), description));
        map.put(Cons.KEY_PopUpReminder, RequestBody.create(MediaType.parse("text/plain"), popUpReminder));
        map.put(Cons.KEY_WorkOrderType, RequestBody.create(MediaType.parse("text/plain"), types));
        map.put(Cons.KEY_WOStatus, RequestBody.create(MediaType.parse("text/plain"), status));
        map.put(Cons.KEY_WOPriority, RequestBody.create(MediaType.parse("text/plain"), priority));
        map.put(Cons.KEY_WOCategory, RequestBody.create(MediaType.parse("text/plain"), category));
        map.put(Cons.KEY_StartDate, RequestBody.create(MediaType.parse("text/plain"), startDate));
        map.put(Cons.KEY_WOStartTime, RequestBody.create(MediaType.parse("text/plain"), startTime));
        map.put(Cons.KEY_EndDate, RequestBody.create(MediaType.parse("text/plain"), endDate));
        map.put(Cons.KEY_WOEndTime, RequestBody.create(MediaType.parse("text/plain"), endTime));
        map.put(Cons.KEY_IsRecurring, RequestBody.create(MediaType.parse("text/plain"), strRecurring));
        map.put(Cons.KEY_StartOn, RequestBody.create(MediaType.parse("text/plain"), startOn));
        map.put(Cons.KEY_RepeatEvery, RequestBody.create(MediaType.parse("text/plain"), repeatEvery));
        map.put(Cons.KEY_RepeatOn, RequestBody.create(MediaType.parse("text/plain"), repeatOn));
        map.put(Cons.KEY_IntervalEvery, RequestBody.create(MediaType.parse("text/plain"), intervalEvery));
        map.put(Cons.KEY_Ends, RequestBody.create(MediaType.parse("text/plain"), ends));
        map.put(Cons.KEY_EndsOnDate, RequestBody.create(MediaType.parse("text/plain"), endsOn));
        map.put(Cons.KEY_EndsAfterOccurrences, RequestBody.create(MediaType.parse("text/plain"), endsAfter));
        map.put(Cons.KEY_StartTime, RequestBody.create(MediaType.parse("text/plain"), recurrenceStartTime));
        map.put(Cons.KEY_EndTime, RequestBody.create(MediaType.parse("text/plain"), recurrenceEndTime));
        map.put(Cons.KEY_Address, RequestBody.create(MediaType.parse("text/plain"), bilAddress));
        map.put(Cons.KEY_City, RequestBody.create(MediaType.parse("text/plain"), bilCity));
        map.put(Cons.KEY_State, RequestBody.create(MediaType.parse("text/plain"), bilState));
        map.put(Cons.KEY_Country, RequestBody.create(MediaType.parse("text/plain"), bilCountry));
        map.put(Cons.KEY_PostalCode, RequestBody.create(MediaType.parse("text/plain"), bilPostal));
        map.put(Cons.KEY_Latitude, RequestBody.create(MediaType.parse("text/plain"), bilLat));
        map.put(Cons.KEY_Longitude, RequestBody.create(MediaType.parse("text/plain"), bilLng));
        Call<ResponseBody> response;
        response = apiInterface.createWorkOrderDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Create" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            isCloneMode = false;
                            String strWorkOrderID = object.getString("WorkOrderID");
                            workOrderId = strWorkOrderID;
                            Utl.showToast(getActivity(), strMessage);
                            getWorkOrderDetailed(strWorkOrderID);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void editWorkOrder(final String workOrderId,
                               String assignedTo,
                               String account,
                               String primaryContact,
                               String parentWorkOrder,
                               String subject,
                               String description,
                               String popUpReminder,
                               String types,
                               String status,
                               String priority,
                               String category,
                               String strRecurring, String startOn,
                               String bilLat, String bilLng,
                               String bilAddress, String bilCity, String bilState, String bilCountry, String bilPostal,
                               String repeatEvery, String intervalEvery, String repeatOn,
                               String ends, String endsOn, String endsAfter,
                               String recurrenceStartTime, String recurrenceEndTime) {

        Log.d("TAG_assignedTo", assignedTo);
        Log.d("TAG_account", account);
        Log.d("TAG_primaryContact", primaryContact);
        Log.d("TAG_parentWorkOrder", parentWorkOrder);
        Log.d("TAG_subject", subject);
        Log.d("TAG_description", description);
        Log.d("TAG_popUpReminder", popUpReminder);
        Log.d("TAG_types", types);
        Log.d("TAG_status", status);
        Log.d("TAG_priority", priority);
        Log.d("TAG_category", category);
        Log.d("TAG_strRecurring", strRecurring);
        Log.d("TAG_repeatEvery", repeatEvery);
        Log.d("TAG_intervalEvery", intervalEvery);
        Log.d("TAG_repeatOn", repeatOn);
        Log.d("TAG_ends", ends);
        Log.d("TAG_endsOn", endsOn);
        Log.d("TAG_endsAfter", endsAfter);
        Log.d("TAG_recurrenceStartTime", recurrenceStartTime);
        Log.d("TAG_recurrenceEndTime", recurrenceEndTime);
        Log.d("TAG_bilLat", bilLat);
        Log.d("TAG_bilLng", bilLng);
        Log.d("TAG_bilAddress", bilAddress);
        Log.d("TAG_bilCity", bilCity);
        Log.d("TAG_bilState", bilState);
        Log.d("TAG_bilCountry", bilCountry);
        Log.d("TAG_bilPostal", bilPostal);

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_WorkOrderID, RequestBody.create(MediaType.parse("text/plain"), workOrderId));
        map.put(Cons.KEY_AssignedTo, RequestBody.create(MediaType.parse("text/plain"), assignedTo));
        map.put(Cons.KEY_Account, RequestBody.create(MediaType.parse("text/plain"), account));
        map.put(Cons.KEY_PrimaryContact, RequestBody.create(MediaType.parse("text/plain"), primaryContact));
        map.put(Cons.KEY_ParentWorkOrder, RequestBody.create(MediaType.parse("text/plain"), parentWorkOrder));
        map.put(Cons.KEY_Subject, RequestBody.create(MediaType.parse("text/plain"), subject));
        map.put(Cons.KEY_Description, RequestBody.create(MediaType.parse("text/plain"), description));
        map.put(Cons.KEY_PopUpReminder, RequestBody.create(MediaType.parse("text/plain"), popUpReminder));
        map.put(Cons.KEY_WorkOrderType, RequestBody.create(MediaType.parse("text/plain"), types));
        map.put(Cons.KEY_WOStatus, RequestBody.create(MediaType.parse("text/plain"), status));
        map.put(Cons.KEY_WOPriority, RequestBody.create(MediaType.parse("text/plain"), priority));
        map.put(Cons.KEY_WOCategory, RequestBody.create(MediaType.parse("text/plain"), category));
        map.put(Cons.KEY_IsRecurring, RequestBody.create(MediaType.parse("text/plain"), strRecurring));
        map.put(Cons.KEY_Latitude, RequestBody.create(MediaType.parse("text/plain"), bilLat));
        map.put(Cons.KEY_Longitude, RequestBody.create(MediaType.parse("text/plain"), bilLng));
        map.put(Cons.KEY_Address, RequestBody.create(MediaType.parse("text/plain"), bilAddress));
        map.put(Cons.KEY_Street, RequestBody.create(MediaType.parse("text/plain"), bilAddress));
        map.put(Cons.KEY_City, RequestBody.create(MediaType.parse("text/plain"), bilCity));
        map.put(Cons.KEY_State, RequestBody.create(MediaType.parse("text/plain"), bilState));
        map.put(Cons.KEY_Country, RequestBody.create(MediaType.parse("text/plain"), bilCountry));
        map.put(Cons.KEY_PostalCode, RequestBody.create(MediaType.parse("text/plain"), bilPostal));
        map.put(Cons.KEY_StartOn, RequestBody.create(MediaType.parse("text/plain"), startOn));
        map.put(Cons.KEY_RepeatEvery, RequestBody.create(MediaType.parse("text/plain"), repeatEvery));
        map.put(Cons.KEY_IntervalEvery, RequestBody.create(MediaType.parse("text/plain"), intervalEvery));
        map.put(Cons.KEY_RepeatOn, RequestBody.create(MediaType.parse("text/plain"), repeatOn));
        map.put(Cons.KEY_Ends, RequestBody.create(MediaType.parse("text/plain"), ends));
        map.put(Cons.KEY_EndsOnDate, RequestBody.create(MediaType.parse("text/plain"), endsOn));
        map.put(Cons.KEY_EndsAfterOccurrences, RequestBody.create(MediaType.parse("text/plain"), endsAfter));
        map.put(Cons.KEY_StartTime, RequestBody.create(MediaType.parse("text/plain"), recurrenceStartTime));
        map.put(Cons.KEY_EndTime, RequestBody.create(MediaType.parse("text/plain"), recurrenceEndTime));
        Call<ResponseBody> response;
        response = apiInterface.editWorkOrderDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Edit" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            getWorkOrderDetailed(workOrderId);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        getWorkOrderDetailed(workOrderId);
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedWorkOrderList(String workOrderId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_WorkOrderID, RequestBody.create(MediaType.parse("text/plain"), workOrderId));
        Call<ResponseBody> response = apiInterface.getRelatedWorkOrderList(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONObject dataObject = object.getJSONObject("data");
                        JSONObject WOLineItemObject = dataObject.getJSONObject("WOLineItem");
                        JSONObject ChemicalObject = dataObject.getJSONObject("Chemical");
                        JSONObject EventObject = dataObject.getJSONObject("Event");
                        JSONObject FileObject = dataObject.getJSONObject("File");
                        JSONObject TaskObject = dataObject.getJSONObject("Task");
                        JSONObject InvoiceObject = dataObject.getJSONObject("Invoice");
                        JSONObject NoteObject = dataObject.getJSONObject("Note");
                        listRelatedWorkOrderList.add(WOLineItemObject.getString("title"));
                        listRelatedWorkOrderList.add(ChemicalObject.getString("title"));
                        listRelatedWorkOrderList.add(EventObject.getString("title"));
                        listRelatedWorkOrderList.add(FileObject.getString("title"));
                        listRelatedWorkOrderList.add(TaskObject.getString("title"));
                        listRelatedWorkOrderList.add(InvoiceObject.getString("title"));
                        listRelatedWorkOrderList.add(NoteObject.getString("title"));
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedWorkOrderLineItems(String workOrderId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_WorkOrderID, RequestBody.create(MediaType.parse("text/plain"), workOrderId));
        Call<ResponseBody> response = apiInterface.getRelatedWorkOrderLineItem(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelWorkOrderLineItem model = new ModelWorkOrderLineItem();
                            model.setLineItemNo(dataObject.getString(Cons.KEY_LineItemNo));
                            model.setProductID(dataObject.getString(Cons.KEY_ProductID));
                            model.setProductName(dataObject.getString(Cons.KEY_ProductName));
                            model.setQuantity(dataObject.getString(Cons.KEY_Quantity));
                            model.setUnitPrice(dataObject.getString(Cons.KEY_UnitPrice));
                            model.setNetTotal(dataObject.getString(Cons.KEY_NetTotal));
                            listRelatedWorkOrderLineItems.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedWorkOrderChemicals(String workOrderId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_WorkOrderID, RequestBody.create(MediaType.parse("text/plain"), workOrderId));
        Call<ResponseBody> response = apiInterface.getRelatedWorkOrderChemical(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelWorkOrderChemical model = new ModelWorkOrderChemical();
                            model.setChemicalNo(dataObject.getString(Cons.KEY_ChemicalNo));
                            model.setProductID(dataObject.getString(Cons.KEY_ProductID));
                            model.setProductName(dataObject.getString(Cons.KEY_ProductName));
                            model.setTestConcentration(dataObject.getString(Cons.KEY_TestConcentration));
                            model.setTestedUnitOfMeasure(dataObject.getString(Cons.KEY_TestedUnitOfMeasure));
                            model.setApplicationAmount(dataObject.getString(Cons.KEY_ApplicationAmount));
                            model.setApplicationUnitOfMeasure(dataObject.getString(Cons.KEY_ApplicationUnitOfMeasure));
                            listRelatedWorkOrderChemicals.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedWorkOrderEvents(String workOrderId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_WorkOrderID, RequestBody.create(MediaType.parse("text/plain"), workOrderId));
        Call<ResponseBody> response = apiInterface.getRelatedWorkOrderEvent(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelWorkOrderEvent model = new ModelWorkOrderEvent();
                            model.setEventEndDate(dataObject.getString(Cons.KEY_EventEndDate));
                            model.setEventEndTime(dataObject.getString(Cons.KEY_EventEndTime));
                            model.setEventID(dataObject.getString(Cons.KEY_EventID));
                            model.setEventStartDate(dataObject.getString(Cons.KEY_EventStartDate));
                            model.setEventStartTime(dataObject.getString(Cons.KEY_EventStartTime));
                            model.setEventStatus(dataObject.getString(Cons.KEY_EventStatus));
                            model.setEventTypeName(dataObject.getString(Cons.KEY_EventTypeName));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            listRelatedWorkOrderEvents.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedWorkOrderFiles(String workOrderId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_WorkOrderID, RequestBody.create(MediaType.parse("text/plain"), workOrderId));
        Call<ResponseBody> response = apiInterface.getRelatedWorkOrderFile(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelWorkOrderFile model = new ModelWorkOrderFile();
                            model.setContentType(dataObject.getString(Cons.KEY_ContentType));
                            model.setFileID(dataObject.getString(Cons.KEY_FileID));
                            model.setFileName(dataObject.getString(Cons.KEY_FileName));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            listRelatedWorkOrderFiles.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedWorkOrderTasks(String workOrderId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_WorkOrderID, RequestBody.create(MediaType.parse("text/plain"), workOrderId));
        Call<ResponseBody> response = apiInterface.getRelatedWorkOrderTask(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelWorkOrderTask model = new ModelWorkOrderTask();
                            //model.setCallDisposition(dataObject.getString(Cons.KEY_CallDisposition));
                            model.setPriority(dataObject.getString(Cons.KEY_Priority));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            model.setTaskID(dataObject.getString(Cons.KEY_TaskID));
                            model.setTaskStatus(dataObject.getString(Cons.KEY_TaskStatus));
                            model.setTaskType(dataObject.getString(Cons.KEY_TaskType));
                            listRelatedWorkOrderTasks.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedWorkOrderInvoices(String workOrderId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_WorkOrderID, RequestBody.create(MediaType.parse("text/plain"), workOrderId));
        Call<ResponseBody> response = apiInterface.getRelatedWorkOrderInvoice(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelWorkOrderInvoice model = new ModelWorkOrderInvoice();
                            model.setInvoiceNumber(dataObject.getString(Cons.KEY_InvoiceNumber));
                            model.setWorkOrderName(dataObject.getString(Cons.KEY_WorkOrderName));
                            model.setInvoiceStatus(dataObject.getString(Cons.KEY_InvoiceStatus));
                            model.setInvoiceDate(dataObject.getString(Cons.KEY_InvoiceDate));
                            model.setDueDate(dataObject.getString(Cons.KEY_DueDate));
                            model.setSubTotal(dataObject.getString(Cons.KEY_SubTotal));
                            model.setTotalPrice(dataObject.getString(Cons.KEY_TotalPrice));
                            listRelatedWorkOrderInvoices.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_3", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedWorkOrderNotes(String workOrderId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_WorkOrderID, RequestBody.create(MediaType.parse("text/plain"), workOrderId));
        Call<ResponseBody> response = apiInterface.getRelatedWorkOrderNote(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelWorkOrderNote model = new ModelWorkOrderNote();
                            model.setNoteID(dataObject.getString(Cons.KEY_NoteID));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            model.setCreatedDate(dataObject.getString(Cons.KEY_CreatedDate));
                            model.setOwnerName(dataObject.getString(Cons.KEY_OwnerName));
                            listRelatedWorkOrderNotes.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedWorkOrderGetProduct(String workOrderId,
                                               String productFamily, String productCode) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_WorkOrderID, RequestBody.create(MediaType.parse("text/plain"), workOrderId));
        map.put(Cons.KEY_ProductFamily, RequestBody.create(MediaType.parse("text/plain"), productFamily));
        map.put(Cons.KEY_ProductCode, RequestBody.create(MediaType.parse("text/plain"), productCode));
        Call<ResponseBody> response = apiInterface.getRelatedWorkOrderGetProduct(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        //String strMessage = object.getString("ResponseMsg");
                        //Utl.showToast(getActivity(), strMessage);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelProduct model = new ModelProduct();
                            model.setProductID(dataObject.getString(Cons.KEY_ProductID));
                            model.setProductCode(dataObject.getString(Cons.KEY_ProductCode));
                            model.setProductName(dataObject.getString(Cons.KEY_ProductName));
                            model.setDescription(dataObject.getString(Cons.KEY_Description));
                            model.setListPrice(dataObject.getString(Cons.KEY_ListPrice));
                            model.setDefaultQuantity(dataObject.getString(Cons.KEY_DefaultQuantity));
                            model.setTaxable(dataObject.getString(Cons.KEY_Taxable));
                            model.setIsListPriceEditable(dataObject.getString(Cons.KEY_IsListPriceEditable));
                            model.setIsQuantityEditable(dataObject.getString(Cons.KEY_IsQuantityEditable));
                            model.setTax(dataObject.getString(Cons.KEY_Tax));

                            double listPrice = Double.valueOf(dataObject.getString(Cons.KEY_ListPrice));
                            double discount = 0;
                            double unitPrice = getUnitPrice(Double.valueOf(dataObject.getString(Cons.KEY_ListPrice)), discount);
                            int quantity = Integer.valueOf(dataObject.getString(Cons.KEY_DefaultQuantity));
                            double subtotal = getSubTotal(listPrice, discount, quantity);
                            int isTaxable = Integer.valueOf(dataObject.getString(Cons.KEY_Taxable));
                            double tax = Double.valueOf(dataObject.getString(Cons.KEY_Tax));
                            double netTotal = getNetTotal(listPrice, discount, quantity, isTaxable, tax);
                            model.setDiscount("0");
                            model.setUnitPrice(String.format("%.2f", unitPrice));
                            model.setSubTotal(String.format("%.2f", subtotal));
                            model.setNetTotal(String.format("%.2f", netTotal));

                            listProduct.add(model);
                        }
                        for (int i = 0; i < listProduct.size(); i++) {
                            phvProductAdd
                                    .addView(new AdapterProductParent(getActivity(), FragmentWorkOrderInfo.this, listProduct.get(i)));
                            phvProductAdd
                                    .addChildView(i, new AdapterProductChild(getActivity(), listProduct.get(i)));
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedWorkOrderGetChemical(final String pos, String workOrderId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_WorkOrderID, RequestBody.create(MediaType.parse("text/plain"), workOrderId));
        Call<ResponseBody> response = apiInterface.getRelatedWorkOrderGetChemical(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelChemical model = new ModelChemical();
                            model.setProductID(dataObject.getString(Cons.KEY_ProductID));
                            model.setProductCode(dataObject.getString(Cons.KEY_ProductCode));
                            model.setProductName(dataObject.getString(Cons.KEY_ProductName));
                            model.setDefaultQuantity(dataObject.getString(Cons.KEY_DefaultQuantity));
                            model.setListPrice(dataObject.getString(Cons.KEY_ListPrice));
                            model.setDescription(dataObject.getString(Cons.KEY_Description));
                            model.setTaxable(dataObject.getString(Cons.KEY_Taxable));
                            listChemical.add(model);
                        }
                        for (int i = 0; i < listChemical.size(); i++) {
                            phvChemicalAdd
                                    .addView(new AdapterChemical(getActivity(), FragmentWorkOrderInfo.this, pos, listChemical.get(i)));
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getProductFamilySpinner() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getWorkOrderProductFamily(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_ProductFamily" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listProductFilterFamily.clear();
                            ModelSpinner m = new ModelSpinner();
                            m.setId("");
                            m.setName("All");
                            listProductFilterFamily.add(m);
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_ProductFamilyID));
                                model.setName(dataObject.getString(Cons.KEY_ProductFamily));
                                listProductFilterFamily.add(model);
                            }
                            spinnerProductFilterFamily.setAdapter(adapterProductFilterFamily);
                            setFilterSpinnerDropDownHeight(listProductFilterFamily, spinnerProductFilterFamily);
                            spinnerProductFilterFamily.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void addWorkOrderLineItem(final int isAddEdit, final boolean isQuickEdit,
                                      final ArrayList<ModelProduct> listProduct) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new LinkedHashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_WorkOrderID, RequestBody.create(MediaType.parse("text/plain"), workOrderId));
        for (int i = 0; i < listProduct.size(); i++) {
            map.put("Product[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listProduct.get(i).getProductID()));
            map.put("ListPrice[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listProduct.get(i).getListPrice()));
            map.put("Discount[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listProduct.get(i).getDiscount()));
            map.put("UnitPrice[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listProduct.get(i).getUnitPrice()));
            map.put("Quantity[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listProduct.get(i).getDefaultQuantity()));
            map.put("SubTotal[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listProduct.get(i).getSubTotal()));
            map.put("Taxable[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listProduct.get(i).getTaxable()));
            map.put("NetTotal[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listProduct.get(i).getNetTotal()));
        }
        Call<ResponseBody> response = apiInterface.addWorkOrderLineItem(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Filter" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            if (isAddEdit == 0) {
                                listProduct.clear();
                                listProductEditChange.clear();
                                Singleton.getInstance().listWorkOrderProduct.clear();
                                phvProductAdd.removeAllViews();
                                edittextProductAddSearch.setText("");
                                getRelatedWorkOrderGetProduct(workOrderId, "", "");
                            } else {
                                if (!isQuickEdit) {
                                    listProduct.clear();
                                    listProductEditChange.clear();
                                    Singleton.getInstance().listWorkOrderProduct.clear();
                                    layoutProductEditChange.removeAllViews();
                                    layoutProductEdit.setVisibility(View.GONE);
                                }
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void addWorkOrderChemical(final boolean isQuickEdit,
                                      final ArrayList<ModelChemical> listChemical) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));

        final Map<String, RequestBody> map = new LinkedHashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_WorkOrderID, RequestBody.create(MediaType.parse("text/plain"), workOrderId));
        map.put(Cons.KEY_Account, RequestBody.create(MediaType.parse("text/plain"), "11"));
        for (int i = 0; i < listChemical.size(); i++) {
            map.put("Product[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listChemical.get(i).getProductID()));
            map.put("TestConcentration[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listChemical.get(i).getTestConcentration()));
            map.put("TestedUnitOfMeasure[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listChemical.get(i).getApplicationUnitOfMeasure()));
            map.put("ApplicationAmount[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listChemical.get(i).getApplicationAmount()));
            map.put("ApplicationUnitOfMeasure[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listChemical.get(i).getApplicationUnitOfMeasure()));
            map.put("ApplicationArea[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listChemical.get(i).getApplicationArea()));
            map.put("AdditionalNotes[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listChemical.get(i).getAdditionalNotes()));
        }
        Call<ResponseBody> response = apiInterface.addWorkOrderChemical(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Filter" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            if (!isQuickEdit) {
                                listChemical.clear();
                                listChemicalEdit.clear();
                                listChemicalEditChange.clear();
                                Singleton.getInstance().listWorkOrderChemical.clear();
                                layoutChemicalEditChange.removeAllViews();
                                layoutChemicalEdit.setVisibility(View.GONE);
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void addNewNote(final String subject, final String body) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new LinkedHashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), getString(R.string.workorder)));
        map.put(Cons.KEY_What, RequestBody.create(MediaType.parse("text/plain"), workOrderId));
        map.put(Cons.KEY_Subject, RequestBody.create(MediaType.parse("text/plain"), subject));
        map.put(Cons.KEY_Body, RequestBody.create(MediaType.parse("text/plain"), body));
        Call<ResponseBody> response = apiInterface.addNewNote(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_NewNote" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        Utl.showToast(getActivity(), strMessage);
                        if (responseCode == 200) {
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            layoutNewNote.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void addConvertToInvoice() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new LinkedHashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_WorkOrderID, RequestBody.create(MediaType.parse("text/plain"), workOrderId));
        Call<ResponseBody> response = apiInterface.addConvertToInvoice(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_ConvertToInv" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        Utl.showToast(getActivity(), strMessage);
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    public boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), CAMERA);
        int result2 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION);
        int result3 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_COARSE_LOCATION);
        int result4 = ContextCompat.checkSelfPermission(getActivity(), CALL_PHONE);
        int result5 = ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE);
        int result6 = ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE);
        return
                result1 == PackageManager.PERMISSION_GRANTED
                        && result2 == PackageManager.PERMISSION_GRANTED
                        && result3 == PackageManager.PERMISSION_GRANTED
                        && result4 == PackageManager.PERMISSION_GRANTED
                        && result5 == PackageManager.PERMISSION_GRANTED
                        && result6 == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{
                        CAMERA,
                        ACCESS_FINE_LOCATION,
                        ACCESS_COARSE_LOCATION,
                        CALL_PHONE,
                        READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean locationFineAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean locationCoarseAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean callAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean readAccepted = grantResults[4] == PackageManager.PERMISSION_GRANTED;
                    boolean writeAccepted = grantResults[5] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted &&
                            locationFineAccepted &&
                            locationCoarseAccepted &&
                            callAccepted &&
                            readAccepted &&
                            writeAccepted)
                        Utl.showToast(getActivity(), "Permission Granted.");
                    else {
                        Utl.showToast(getActivity(), "Permission Denied.");
                    }
                }
                break;
        }
    }

    private void showView(final View view) {
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_right);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }
        });
        view.startAnimation(animation);
    }

    @Override
    public void callbackAddAccountListen(int pos, ModelSpinner model) {
        if (isAccountClicked) {
            phvAccountAdd.removeAllViews();
            layoutAccountAdd.setVisibility(View.GONE);
            strAccount = model.getId();
            strAccountName = model.getName();
            edittextAccount.setText(strAccountName);
        } else if (isContactClicked) {
            phvContactAdd.removeAllViews();
            layoutContactAdd.setVisibility(View.GONE);
            strPrimaryContact = model.getId();
            strPrimaryContactName = model.getName();
            edittextPrimaryContact.setText(strPrimaryContactName);
        } else if (isWorkOrderClicked) {
            phvWorkOrderAdd.removeAllViews();
            layoutWorkOrderAdd.setVisibility(View.GONE);
            strParentWorkOrder = model.getId();
            strParentWorkOrderName = model.getName();
            edittextParentWorkOrder.setText(strParentWorkOrderName);
        }
    }

    @Override
    public void callbackViewAllListen(String str) {
        if (click != null) {
            click.callbackWorkOrderViewAllListen(str, workOrderId);
        }
    }

    @Override
    public void callbackAddItemListen(String str) {
        if (str.equals(getString(R.string.line_item))) {
            layoutProductAdd.setVisibility(View.VISIBLE);
            getRelatedWorkOrderGetProduct(workOrderId, "", "");
        } else if (str.equals(getString(R.string.chemical))) {
            layoutChemicalAdd.setVisibility(View.VISIBLE);
            getRelatedWorkOrderGetChemical("", workOrderId);
        }
    }

    @Override
    public void callbackAddProductCheckBoxListen(String productId, boolean b) {
        if (b) {
            for (int i = 0; i < listProduct.size(); i++) {
                String strId = listProduct.get(i).getProductID();
                if (strId.equals(productId)) {
                    Singleton.getInstance().listWorkOrderProduct.add(listProduct.get(i));
                }
            }
            for (int j = 0; j < Singleton.getInstance().listWorkOrderProduct.size(); j++) {
                Log.d("TAG_", Singleton.getInstance().listWorkOrderProduct.get(j).getProductID());
            }
        } else {
            for (int i = 0; i < Singleton.getInstance().listWorkOrderProduct.size(); i++) {
                String strId = Singleton.getInstance().listWorkOrderProduct.get(i).getProductID();
                if (strId.equals(productId)) {
                    Singleton.getInstance().listWorkOrderProduct.remove(Singleton.getInstance().listWorkOrderProduct.get(i));
                }
            }
            for (int j = 0; j < Singleton.getInstance().listWorkOrderProduct.size(); j++) {
                Log.d("TAG_", Singleton.getInstance().listWorkOrderProduct.get(j).getProductID());
            }
        }

    }

    @Override
    public void callbackAddChemicalListen(String pos, final ModelChemical model) {

        if (TextUtils.isEmpty(pos)) {

            if (!listChemicalEdit.contains(model.getProductID())) {

                phvChemicalAdd.removeAllViews();
                layoutChemicalAdd.setVisibility(View.GONE);
                layoutChemicalEdit.setVisibility(View.VISIBLE);

                LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View v = layoutInflater.inflate(R.layout.layout_chemical_edit_change, null);

                layoutChemicalEditChange.addView(v);

                int size = layoutChemicalEditChange.getChildCount();
                View viw = layoutChemicalEditChange.getChildAt(size - 1);

                final CheckBox checkboxChemicalId = (CheckBox) viw.findViewById(R.id.checkbox_chemical_id);
                final ToggleButton toggleIcon = (ToggleButton) viw.findViewById(R.id.toggle_icon);

                final TextView textviewName = (TextView) viw.findViewById(R.id.textview_name);
                final LinearLayout layoutToggle = (LinearLayout) viw.findViewById(R.id.layout_toggle);

                final EditText edittextName = (EditText) viw.findViewById(R.id.edittext_name);
                final ImageButton buttonName = (ImageButton) viw.findViewById(R.id.button_name);
                final EditText edittextTestedConcentration = (EditText) viw.findViewById(R.id.edittext_tested_concentration);
                final Spinner spinnerTestedConcentration = (Spinner) viw.findViewById(R.id.spinner_tested_concentration);
                final EditText edittextApplication = (EditText) viw.findViewById(R.id.edittext_application);
                final Spinner spinnerApplication = (Spinner) viw.findViewById(R.id.spinner_application);
                final EditText edittextApplicationArea = (EditText) viw.findViewById(R.id.edittext_application_area);
                final EditText edittextApplicationNotes = (EditText) viw.findViewById(R.id.edittext_application_notes);

                checkboxChemicalId.setChecked(false);
                checkboxChemicalId.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                        if (isChecked) {
                            listChemicalEditChange.add(model.getProductID());
                        } else {
                            int idx = listChemicalEditChange.indexOf(model.getProductID());
                            listChemicalEditChange.remove(idx);
                        }
                    }
                });

                layoutToggle.setVisibility(View.GONE);
                toggleIcon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                        if (isChecked) {
                            layoutToggle.setVisibility(View.VISIBLE);
                            textviewName.setVisibility(View.GONE);
                        } else {
                            layoutToggle.setVisibility(View.GONE);
                            textviewName.setVisibility(View.VISIBLE);
                        }
                    }
                });

                listChemicalEdit.add(model.getProductID());

                String strTestedConcentration = "0.00";
                double testedConcentration = Double.valueOf(strTestedConcentration);
                int testedConcentrationUnit = 0;
                String strApplication = "0.00";
                double application = Double.valueOf(strApplication);
                int applicationUnit = 0;
                String strApplicationArea = "";
                String strApplicationNotes = "";

                ModelChemical mdl = new ModelChemical();
                mdl.setProductID(model.getProductID());
                mdl.setTestConcentration(strTestedConcentration);
                mdl.setTestedUnitOfMeasure(String.valueOf(testedConcentrationUnit));
                mdl.setApplicationAmount(strApplication);
                mdl.setApplicationUnitOfMeasure(String.valueOf(applicationUnit));
                mdl.setApplicationArea(strApplicationArea);
                mdl.setAdditionalNotes(strApplicationNotes);
                Singleton.getInstance().listWorkOrderChemical.add(mdl);

                textviewName.setText(model.getProductName());
                edittextName.setText(model.getProductName());
                edittextTestedConcentration.setText(String.format("%.2f", testedConcentration));
                spinnerTestedConcentration.setAdapter(adapterUnitSpinner);
                edittextApplication.setText(String.format("%.2f", application));
                spinnerApplication.setAdapter(adapterUnitSpinner);
                edittextApplicationArea.setText("");
                edittextApplicationNotes.setText("");

                buttonName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listChemical.clear();
                        layoutChemicalAdd.setVisibility(View.VISIBLE);
                        getRelatedWorkOrderGetChemical("" + layoutChemicalEditChange.indexOfChild(v), workOrderId);
                    }
                });

                edittextTestedConcentration.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {

                        String strTestedConcentration = edittextTestedConcentration.getText().toString();
                        if (TextUtils.isEmpty(strTestedConcentration)) {
                            strTestedConcentration = "0.00";
                            edittextTestedConcentration.setText("0.00");
                        }
                        double testedConcentration = Double.valueOf(strTestedConcentration);
                        int testedConcentrationUnit = spinnerTestedConcentration.getSelectedItemPosition() + 1;
                        String strApplication = edittextApplication.getText().toString();
                        if (TextUtils.isEmpty(strApplication)) {
                            strApplication = "0.00";
                            edittextApplication.setText("0.00");
                        }
                        double application = Double.valueOf(strApplication);
                        int applicationUnit = spinnerApplication.getSelectedItemPosition() + 1;
                        String strApplicationArea = edittextApplicationArea.getText().toString().trim();
                        String strApplicationNotes = edittextApplicationNotes.getText().toString().trim();

                        layoutChemicalEditChange.invalidate();
                        updateModelChemical(layoutChemicalEditChange.indexOfChild(v),
                                Singleton.getInstance().listWorkOrderChemical.get(layoutChemicalEditChange.indexOfChild(v)).getProductID(),
                                String.format("%.2f", testedConcentration),
                                String.valueOf(testedConcentrationUnit),
                                String.format("%.2f", application),
                                String.valueOf(applicationUnit),
                                strApplicationArea,
                                strApplicationNotes);
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });

                spinnerTestedConcentration.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        String strTestedConcentration = edittextTestedConcentration.getText().toString();
                        if (TextUtils.isEmpty(strTestedConcentration)) {
                            strTestedConcentration = "0.00";
                            edittextTestedConcentration.setText("0.00");
                        }
                        double testedConcentration = Double.valueOf(strTestedConcentration);
                        int testedConcentrationUnit = position + 1;
                        String strApplication = edittextApplication.getText().toString();
                        if (TextUtils.isEmpty(strApplication)) {
                            strApplication = "0.00";
                            edittextApplication.setText("0.00");
                        }
                        double application = Double.valueOf(strApplication);
                        int applicationUnit = spinnerApplication.getSelectedItemPosition() + 1;
                        String strApplicationArea = edittextApplicationArea.getText().toString().trim();
                        String strApplicationNotes = edittextApplicationNotes.getText().toString().trim();

                        layoutChemicalEditChange.invalidate();
                        updateModelChemical(layoutChemicalEditChange.indexOfChild(v),
                                Singleton.getInstance().listWorkOrderChemical.get(layoutChemicalEditChange.indexOfChild(v)).getProductID(),
                                String.format("%.2f", testedConcentration),
                                String.valueOf(testedConcentrationUnit),
                                String.format("%.2f", application),
                                String.valueOf(applicationUnit),
                                strApplicationArea,
                                strApplicationNotes);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                    }
                });

                edittextApplication.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                        String strTestedConcentration = edittextTestedConcentration.getText().toString();
                        if (TextUtils.isEmpty(strTestedConcentration)) {
                            strTestedConcentration = "0.00";
                            edittextTestedConcentration.setText("0.00");
                        }
                        double testedConcentration = Double.valueOf(strTestedConcentration);
                        int testedConcentrationUnit = spinnerTestedConcentration.getSelectedItemPosition() + 1;
                        String strApplication = edittextApplication.getText().toString();
                        if (TextUtils.isEmpty(strApplication)) {
                            strApplication = "0.00";
                            edittextApplication.setText("0.00");
                        }
                        double application = Double.valueOf(strApplication);
                        int applicationUnit = spinnerApplication.getSelectedItemPosition() + 1;
                        String strApplicationArea = edittextApplicationArea.getText().toString().trim();
                        String strApplicationNotes = edittextApplicationNotes.getText().toString().trim();

                        layoutChemicalEditChange.invalidate();
                        updateModelChemical(layoutChemicalEditChange.indexOfChild(v),
                                Singleton.getInstance().listWorkOrderChemical.get(layoutChemicalEditChange.indexOfChild(v)).getProductID(),
                                String.format("%.2f", testedConcentration),
                                String.valueOf(testedConcentrationUnit),
                                String.format("%.2f", application),
                                String.valueOf(applicationUnit),
                                strApplicationArea,
                                strApplicationNotes);
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });

                spinnerApplication.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        String strTestedConcentration = edittextTestedConcentration.getText().toString();
                        if (TextUtils.isEmpty(strTestedConcentration)) {
                            strTestedConcentration = "0.00";
                            edittextTestedConcentration.setText("0.00");
                        }
                        double testedConcentration = Double.valueOf(strTestedConcentration);
                        int testedConcentrationUnit = spinnerTestedConcentration.getSelectedItemPosition() + 1;
                        String strApplication = edittextApplication.getText().toString();
                        if (TextUtils.isEmpty(strApplication)) {
                            strApplication = "0.00";
                            edittextApplication.setText("0.00");
                        }
                        double application = Double.valueOf(strApplication);
                        int applicationUnit = position + 1;
                        String strApplicationArea = edittextApplicationArea.getText().toString().trim();
                        String strApplicationNotes = edittextApplicationNotes.getText().toString().trim();

                        layoutChemicalEditChange.invalidate();
                        updateModelChemical(layoutChemicalEditChange.indexOfChild(v),
                                Singleton.getInstance().listWorkOrderChemical.get(layoutChemicalEditChange.indexOfChild(v)).getProductID(),
                                String.format("%.2f", testedConcentration),
                                String.valueOf(testedConcentrationUnit),
                                String.format("%.2f", application),
                                String.valueOf(applicationUnit),
                                strApplicationArea,
                                strApplicationNotes);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                    }
                });

                edittextApplicationArea.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                        String strTestedConcentration = edittextTestedConcentration.getText().toString();
                        if (TextUtils.isEmpty(strTestedConcentration)) {
                            strTestedConcentration = "0.00";
                            edittextTestedConcentration.setText("0.00");
                        }
                        double testedConcentration = Double.valueOf(strTestedConcentration);
                        int testedConcentrationUnit = spinnerTestedConcentration.getSelectedItemPosition() + 1;
                        String strApplication = edittextApplication.getText().toString();
                        if (TextUtils.isEmpty(strApplication)) {
                            strApplication = "0.00";
                            edittextApplication.setText("0.00");
                        }
                        double application = Double.valueOf(strApplication);
                        int applicationUnit = spinnerApplication.getSelectedItemPosition() + 1;
                        String strApplicationArea = edittextApplicationArea.getText().toString().trim();
                        String strApplicationNotes = edittextApplicationNotes.getText().toString().trim();

                        layoutChemicalEditChange.invalidate();
                        updateModelChemical(layoutChemicalEditChange.indexOfChild(v),
                                Singleton.getInstance().listWorkOrderChemical.get(layoutChemicalEditChange.indexOfChild(v)).getProductID(),
                                String.format("%.2f", testedConcentration),
                                String.valueOf(testedConcentrationUnit),
                                String.format("%.2f", application),
                                String.valueOf(applicationUnit),
                                strApplicationArea,
                                strApplicationNotes);

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });

                edittextApplicationNotes.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                        String strTestedConcentration = edittextTestedConcentration.getText().toString();
                        if (TextUtils.isEmpty(strTestedConcentration)) {
                            strTestedConcentration = "0.00";
                            edittextTestedConcentration.setText("0.00");
                        }
                        double testedConcentration = Double.valueOf(strTestedConcentration);
                        int testedConcentrationUnit = spinnerTestedConcentration.getSelectedItemPosition() + 1;
                        String strApplication = edittextApplication.getText().toString();
                        if (TextUtils.isEmpty(strApplication)) {
                            strApplication = "0.00";
                            edittextApplication.setText("0.00");
                        }
                        double application = Double.valueOf(strApplication);
                        int applicationUnit = spinnerApplication.getSelectedItemPosition() + 1;
                        String strApplicationArea = edittextApplicationArea.getText().toString().trim();
                        String strApplicationNotes = edittextApplicationNotes.getText().toString().trim();

                        layoutChemicalEditChange.invalidate();
                        updateModelChemical(layoutChemicalEditChange.indexOfChild(v),
                                Singleton.getInstance().listWorkOrderChemical.get(layoutChemicalEditChange.indexOfChild(v)).getProductID(),
                                String.format("%.2f", testedConcentration),
                                String.valueOf(testedConcentrationUnit),
                                String.format("%.2f", application),
                                String.valueOf(applicationUnit),
                                strApplicationArea,
                                strApplicationNotes);

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });

            } else {
                Utl.showToast(getActivity(), "Chemical is already added.");
            }
        } else {

            phvChemicalAdd.removeAllViews();
            layoutChemicalAdd.setVisibility(View.GONE);

            if (!listChemicalEdit.contains(model.getProductID())) {

                final int i = Integer.valueOf(pos);

                final CheckBox checkboxChemicalId = (CheckBox) layoutChemicalEditChange.getChildAt(i).findViewById(R.id.checkbox_chemical_id);
                final ToggleButton toggleIcon = (ToggleButton) layoutChemicalEditChange.getChildAt(i).findViewById(R.id.toggle_icon);

                final TextView textviewName = (TextView) layoutChemicalEditChange.getChildAt(i).findViewById(R.id.textview_name);
                final LinearLayout layoutToggle = (LinearLayout) layoutChemicalEditChange.getChildAt(i).findViewById(R.id.layout_toggle);

                final EditText edittextName = layoutChemicalEditChange.getChildAt(i).findViewById(R.id.edittext_name);
                final EditText edittextTestedConcentration = (EditText) layoutChemicalEditChange.getChildAt(i).findViewById(R.id.edittext_tested_concentration);
                final Spinner spinnerTestedConcentration = (Spinner) layoutChemicalEditChange.getChildAt(i).findViewById(R.id.spinner_tested_concentration);
                final EditText edittextApplication = (EditText) layoutChemicalEditChange.getChildAt(i).findViewById(R.id.edittext_application);
                final Spinner spinnerApplication = (Spinner) layoutChemicalEditChange.getChildAt(i).findViewById(R.id.spinner_application);
                final EditText edittextApplicationArea = (EditText) layoutChemicalEditChange.getChildAt(i).findViewById(R.id.edittext_application_area);
                final EditText edittextApplicationNotes = (EditText) layoutChemicalEditChange.getChildAt(i).findViewById(R.id.edittext_application_notes);

                textviewName.setText(model.getProductName());
                edittextName.setText(model.getProductName());

                String strTestedConcentration = edittextTestedConcentration.getText().toString();
                if (TextUtils.isEmpty(strTestedConcentration)) {
                    strTestedConcentration = "0.00";
                    edittextTestedConcentration.setText("0.00");
                }
                double testedConcentration = Double.valueOf(strTestedConcentration);
                int testedConcentrationUnit = spinnerTestedConcentration.getSelectedItemPosition() + 1;
                String strApplication = edittextApplication.getText().toString();
                if (TextUtils.isEmpty(strApplication)) {
                    strApplication = "0.00";
                    edittextApplication.setText("0.00");
                }
                double application = Double.valueOf(strApplication);
                int applicationUnit = spinnerApplication.getSelectedItemPosition() + 1;
                String strApplicationArea = edittextApplicationArea.getText().toString().trim();
                String strApplicationNotes = edittextApplicationNotes.getText().toString().trim();

                layoutChemicalEditChange.invalidate();
                updateModelChemical(layoutChemicalEditChange.indexOfChild(layoutChemicalEditChange.getChildAt(i)),
                        Singleton.getInstance().listWorkOrderChemical.get(layoutChemicalEditChange.indexOfChild(layoutChemicalEditChange.getChildAt(i))).getProductID(),
                        String.format("%.2f", testedConcentration),
                        String.valueOf(testedConcentrationUnit),
                        String.format("%.2f", application),
                        String.valueOf(applicationUnit),
                        strApplicationArea,
                        strApplicationNotes);

            } else {
                Utl.showToast(getActivity(), "Chemical is already added.");
            }
        }

    }

    private void showPopup(String strMessage) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(getString(R.string.popup_reminder));
        alertDialogBuilder.setMessage(strMessage);
        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        alertDialogBuilder.create().dismiss();
                    }
                });
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    @Override
    public void callbackWorkOrderRelatedDetails(String whoName, String whatID) {
        if (whoName.equals(getString(R.string.line_item))) {
            getLineItemDetails(whatID);
        } else if (whoName.equals(getString(R.string.chemical))) {
            getLineItemDetails(whatID);
        } else if (whoName.equals(getString(R.string.event))) {
            getEventDetails(whatID);
        } else if (whoName.equals(getString(R.string.note))) {
            getNoteDetails(whatID);
        }
    }

    private void getLineItemDetails(String whatID) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_ProductID, RequestBody.create(MediaType.parse("text/plain"), whatID));
        Call<ResponseBody> response = apiInterface.getProductDetails(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONObject dataObject = object.getJSONObject("data");

                        String productName = dataObject.getString(Cons.KEY_ProductName);
                        String productCode = dataObject.getString(Cons.KEY_ProductCode);
                        String quantity = dataObject.getString(Cons.KEY_DefaultQuantity);
                        String productCost = dataObject.getString(Cons.KEY_ProductCost);
                        String price = dataObject.getString(Cons.KEY_ListPrice);
                        String productFamily = dataObject.getString(Cons.KEY_ProductFamilyName);
                        String datePurchased = dataObject.getString(Cons.KEY_DatePurchased);
                        String taxable = dataObject.getString(Cons.KEY_Taxable);
                        String description = dataObject.getString(Cons.KEY_Description);
                        String account = dataObject.getString(Cons.KEY_AccountName);
                        String createdBy = dataObject.getString(Cons.KEY_CreatedByName);
                        String createdDate = dataObject.getString(Cons.KEY_CreatedDate);
                        String lastModifiedBy = dataObject.getString(Cons.KEY_LastModifiedByName);
                        String lateModifiedDate = dataObject.getString(Cons.KEY_LastModifiedDate);

                        final Dialog dialogDetails = new Dialog(getActivity(), R.style.DialogFullScreen);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        lp.copyFrom(dialogDetails.getWindow().getAttributes());
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.gravity = Gravity.BOTTOM;
                        dialogDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialogDetails.setCancelable(true);
                        dialogDetails.setCanceledOnTouchOutside(true);
                        dialogDetails.setContentView(R.layout.dialog_product_details);

                        TextView textviewProductName = (TextView) dialogDetails.findViewById(R.id.textview_product);
                        TextView textviewProductCode = (TextView) dialogDetails.findViewById(R.id.textview_code);
                        TextView textviewQuantity = (TextView) dialogDetails.findViewById(R.id.textview_quantity);
                        TextView textviewProductCost = (TextView) dialogDetails.findViewById(R.id.textview_cost);
                        TextView textviewPrice = (TextView) dialogDetails.findViewById(R.id.textview_price);
                        TextView textviewProductFamily = (TextView) dialogDetails.findViewById(R.id.textview_product_family);
                        TextView textviewDatePurchased = (TextView) dialogDetails.findViewById(R.id.textview_purchase_date);
                        CheckBox checkBoxTaxable = (CheckBox) dialogDetails.findViewById(R.id.checkbox_taxable);
                        TextView textviewDescription = (TextView) dialogDetails.findViewById(R.id.textview_description);
                        TextView textviewAccount = (TextView) dialogDetails.findViewById(R.id.textview_account);
                        TextView textviewCreatedDate = (TextView) dialogDetails.findViewById(R.id.textview_created_date);
                        TextView textviewCreatedBy = (TextView) dialogDetails.findViewById(R.id.textview_created_by);
                        TextView textviewLastModifiedDate = (TextView) dialogDetails.findViewById(R.id.textview_last_modified_date);
                        TextView textviewLastModifiedBy = (TextView) dialogDetails.findViewById(R.id.textview_last_modified_by);

                        textviewProductName.setText(productName);
                        textviewProductCode.setText(productCode);
                        textviewQuantity.setText(quantity);
                        textviewProductCost.setText("$ " + productCost);
                        textviewPrice.setText("$ " + price);
                        textviewProductFamily.setText(productFamily);
                        textviewDatePurchased.setText(datePurchased);
                        textviewDescription.setText(description);
                        if (taxable.equals("1")) {
                            checkBoxTaxable.setChecked(true);
                        } else {
                            checkBoxTaxable.setChecked(false);
                        }
                        textviewAccount.setText(account);
                        textviewCreatedDate.setText(createdDate);
                        textviewCreatedBy.setText(createdBy);
                        textviewLastModifiedBy.setText(lastModifiedBy);
                        textviewLastModifiedDate.setText(lateModifiedDate);

                        Button buttonClose = (Button) dialogDetails.findViewById(R.id.button_details_close);
                        buttonClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogDetails.dismiss();
                            }
                        });
                        dialogDetails.show();
                        dialogDetails.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                        dialogDetails.getWindow().setDimAmount(0.5f);
                        dialogDetails.getWindow().setAttributes(lp);
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getEventDetails(String whatID) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_EventID, RequestBody.create(MediaType.parse("text/plain"), whatID));
        Call<ResponseBody> response = apiInterface.getEventDetails(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONObject dataObject = object.getJSONObject("data");

                        String subject = dataObject.getString(Cons.KEY_Subject);
                        String assignedTo = dataObject.getString(Cons.KEY_AssignedToName);
                        String start = dataObject.getString(Cons.KEY_EventStartDate);
                        String end = dataObject.getString(Cons.KEY_EventEndDate);
                        String isAllDay = dataObject.getString(Cons.KEY_IsAllDayEvent);
                        String description = dataObject.getString(Cons.KEY_Description);
                        String type = dataObject.getString(Cons.KEY_EventTypeName);
                        String contact = dataObject.getString(Cons.KEY_ContactName);
                        String relatedTo = dataObject.getString(Cons.KEY_RelatedToName);
                        String email = dataObject.getString(Cons.KEY_Email);
                        String phone = dataObject.getString(Cons.KEY_PhoneNo);
                        String priority = dataObject.getString(Cons.KEY_EventPriority);
                        String createdBy = dataObject.getString(Cons.KEY_CreatedBy);
                        String createdDate = dataObject.getString(Cons.KEY_CreatedDate);
                        String lastModifiedBy = dataObject.getString(Cons.KEY_LastModifiedBy);
                        String lateModifiedDate = dataObject.getString(Cons.KEY_LastModifiedDate);

                        final Dialog dialogDetails = new Dialog(getActivity(), R.style.DialogFullScreen);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        lp.copyFrom(dialogDetails.getWindow().getAttributes());
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.gravity = Gravity.BOTTOM;
                        dialogDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialogDetails.setCancelable(true);
                        dialogDetails.setCanceledOnTouchOutside(true);
                        dialogDetails.setContentView(R.layout.dialog_event_details);

                        TextView textviewSubject = (TextView) dialogDetails.findViewById(R.id.textview_subject);
                        TextView textviewAssignedTo = (TextView) dialogDetails.findViewById(R.id.textview_assigned_to);
                        TextView textviewStart = (TextView) dialogDetails.findViewById(R.id.textview_start);
                        TextView textviewEnd = (TextView) dialogDetails.findViewById(R.id.textview_end);
                        CheckBox checkBoxIsAllDayEvent = (CheckBox) dialogDetails.findViewById(R.id.checkbox_is_all_day_event);
                        TextView textviewDescription = (TextView) dialogDetails.findViewById(R.id.textview_description);
                        TextView textviewType = (TextView) dialogDetails.findViewById(R.id.textview_type);
                        TextView textviewContact = (TextView) dialogDetails.findViewById(R.id.textview_contact);
                        TextView textviewRelatedTo = (TextView) dialogDetails.findViewById(R.id.textview_related_to);
                        TextView textviewEmail = (TextView) dialogDetails.findViewById(R.id.textview_email);
                        TextView textviewPhone = (TextView) dialogDetails.findViewById(R.id.textview_phone);
                        TextView textviewPriority = (TextView) dialogDetails.findViewById(R.id.textview_priority);
                        TextView textviewCreatedDate = (TextView) dialogDetails.findViewById(R.id.textview_created_date);
                        TextView textviewCreatedBy = (TextView) dialogDetails.findViewById(R.id.textview_created_by);
                        TextView textviewLastModifiedDate = (TextView) dialogDetails.findViewById(R.id.textview_last_modified_date);
                        TextView textviewLastModifiedBy = (TextView) dialogDetails.findViewById(R.id.textview_last_modified_by);

                        textviewSubject.setText(subject);
                        textviewAssignedTo.setText(assignedTo);
                        textviewStart.setText(start);
                        textviewEnd.setText(end);
                        if (isAllDay.equals("1")) {
                            checkBoxIsAllDayEvent.setChecked(true);
                        } else {
                            checkBoxIsAllDayEvent.setChecked(false);
                        }
                        textviewDescription.setText(description);
                        textviewType.setText(type);
                        textviewContact.setText(contact);
                        textviewRelatedTo.setText(relatedTo);
                        textviewEmail.setText(email);
                        textviewPhone.setText(phone);
                        textviewPriority.setText(priority);
                        textviewCreatedDate.setText(createdDate);
                        textviewCreatedBy.setText(createdBy);
                        textviewLastModifiedBy.setText(lastModifiedBy);
                        textviewLastModifiedDate.setText(lateModifiedDate);

                        Button buttonClose = (Button) dialogDetails.findViewById(R.id.button_details_close);
                        buttonClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogDetails.dismiss();
                            }
                        });
                        dialogDetails.show();
                        dialogDetails.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                        dialogDetails.getWindow().setDimAmount(0.5f);
                        dialogDetails.getWindow().setAttributes(lp);
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getNoteDetails(String whatID) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_NoteID, RequestBody.create(MediaType.parse("text/plain"), whatID));
        Call<ResponseBody> response = apiInterface.getNoteDetails(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONObject dataObject = object.getJSONObject("data");
                        String owner = dataObject.getString(Cons.KEY_OwnerName);
                        String relatedTo = dataObject.getString(Cons.KEY_RelatedTo);
                        String subject = dataObject.getString(Cons.KEY_Subject);
                        String body = dataObject.getString(Cons.KEY_Body);
                        String createdDate = dataObject.getString(Cons.KEY_CreatedByName);
                        String createdBy = dataObject.getString(Cons.KEY_CreatedDate);
                        String lastModifiedBy = dataObject.getString(Cons.KEY_LastModifiedByName);
                        String lateModifiedDate = dataObject.getString(Cons.KEY_LastModifiedDate);
                        final Dialog dialogDetails = new Dialog(getActivity(), R.style.DialogFullScreen);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        lp.copyFrom(dialogDetails.getWindow().getAttributes());
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.gravity = Gravity.BOTTOM;
                        dialogDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialogDetails.setCancelable(true);
                        dialogDetails.setCanceledOnTouchOutside(true);
                        dialogDetails.setContentView(R.layout.dialog_note_details);
                        TextView textviewOwner = (TextView) dialogDetails.findViewById(R.id.textview_owner);
                        TextView textviewRelatedTo = (TextView) dialogDetails.findViewById(R.id.textview_related_to);
                        TextView textviewSubject = (TextView) dialogDetails.findViewById(R.id.textview_subject);
                        TextView textviewBody = (TextView) dialogDetails.findViewById(R.id.textview_body);
                        TextView textviewCreatedDate = (TextView) dialogDetails.findViewById(R.id.textview_created_date);
                        TextView textviewCreatedBy = (TextView) dialogDetails.findViewById(R.id.textview_created_by);
                        TextView textviewLastModifiedDate = (TextView) dialogDetails.findViewById(R.id.textview_last_modified_date);
                        TextView textviewLastModifiedBy = (TextView) dialogDetails.findViewById(R.id.textview_last_modified_by);
                        textviewOwner.setText(owner);
                        textviewRelatedTo.setText(relatedTo);
                        textviewSubject.setText(subject);
                        textviewBody.setText(body);
                        textviewCreatedBy.setText(createdBy);
                        textviewCreatedDate.setText(createdDate);
                        textviewLastModifiedBy.setText(lastModifiedBy);
                        textviewLastModifiedDate.setText(lateModifiedDate);

                        Button buttonClose = (Button) dialogDetails.findViewById(R.id.button_details_close);
                        buttonClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogDetails.dismiss();
                            }
                        });

                        dialogDetails.show();
                        dialogDetails.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                        dialogDetails.getWindow().setDimAmount(0.5f);
                        dialogDetails.getWindow().setAttributes(lp);
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

}