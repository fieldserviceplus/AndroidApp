package com.fieldwise.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.models.ModelSpinner;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;
import com.fxn.pix.Pix;
import com.jkcarino.rtexteditorview.RTextEditorToolbar;
import com.jkcarino.rtexteditorview.RTextEditorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class FragmentEmail extends Fragment {

    private static final int PERMISSION_REQUEST_CODE = 200;

    public APIInterface apiInterface;

    private boolean isFileClicked = false;

    private String strRelated = "";
    private String strWhat = "";
    private String strHeader = "";

    private LinearLayout layCC, layBCC;

    private Spinner spinnerMail, spinnerTemplate;
    private List<ModelSpinner> listMail, listTemplate;
    private ArrayAdapter<ModelSpinner> adapterMail, adapterTemplate;

    private EditText editTextFrom, editTextTo, editTextCC, editTextBCC, editTextSubject, editTextFileName;

    private TextView buttonCC, buttonBCC;

    private Button buttonFileBrowse, buttonSave, buttonCancel;

    private RTextEditorView editor;
    private RTextEditorToolbar editorToolbar;

    private String attachmentPath;

    public ClickListenSendEmailClose clickClose;

    public interface ClickListenSendEmailClose {
        void callbackSendEmailClose(String relatedTo, String what, String header);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_email, container, false);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            strRelated = bundle.getString("related", "");
            strWhat = bundle.getString("what", "");
            strHeader = bundle.getString("header", "");
        }

        apiInterface = APIClient.getClient().create(APIInterface.class);

        clickClose = (ClickListenSendEmailClose) getActivity();

        layCC = (LinearLayout) view.findViewById(R.id.lay_cc);
        layBCC = (LinearLayout) view.findViewById(R.id.lay_bcc);

        spinnerMail = (Spinner) view.findViewById(R.id.spinner_select_email);
        listMail = new ArrayList<>();
        adapterMail = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listMail);
        adapterMail.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTemplate = (Spinner) view.findViewById(R.id.spinner_select_template);
        listTemplate = new ArrayList<>();
        adapterTemplate = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listTemplate);
        adapterTemplate.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        editTextFrom = (EditText) view.findViewById(R.id.edittext_from);
        editTextTo = (EditText) view.findViewById(R.id.edittext_to);
        editTextCC = (EditText) view.findViewById(R.id.edittext_cc);
        editTextBCC = (EditText) view.findViewById(R.id.edittext_bcc);
        editTextSubject = (EditText) view.findViewById(R.id.edittext_subject);
        editTextFileName = (EditText) view.findViewById(R.id.edittext_file_name);

        buttonCC = (TextView) view.findViewById(R.id.button_cc);
        buttonBCC = (TextView) view.findViewById(R.id.button_bcc);

        buttonFileBrowse = (Button) view.findViewById(R.id.button_file_browse);
        buttonSave = (Button) view.findViewById(R.id.button_save);
        buttonCancel = (Button) view.findViewById(R.id.button_cancel);

        editor = (RTextEditorView) view.findViewById(R.id.editor_view_body);
        editorToolbar = (RTextEditorToolbar) view.findViewById(R.id.editor_toolbar);
        editorToolbar.setEditorView(editor);

        getFrom();
        getTemplate();

        buttonCC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (layCC.getVisibility() == View.GONE) {
                    layCC.setVisibility(View.VISIBLE);
                } else {
                    layCC.setVisibility(View.GONE);
                }
            }
        });

        buttonBCC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (layBCC.getVisibility() == View.GONE) {
                    layBCC.setVisibility(View.VISIBLE);
                } else {
                    layBCC.setVisibility(View.GONE);
                }
            }
        });

        buttonFileBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!checkPermission()) {
                        requestPermission();
                    } else {
                        dialogFileBrowse();
                    }
                } else {
                    dialogFileBrowse();
                }
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editTextFrom.getText().toString().trim();
                String from = listMail.get(spinnerMail.getSelectedItemPosition()).getName();
                String to = editTextTo.getText().toString().trim();
                String cc = editTextCC.getText().toString().trim();
                String bcc = editTextBCC.getText().toString().trim();
                String subject = editTextSubject.getText().toString().trim();
                String template = listTemplate.get(spinnerTemplate.getSelectedItemPosition()).getId();
                String body = editor.getHtml().toString();

                if (TextUtils.isEmpty(from)) {
                    Utl.showToast(getActivity(), "Enter From Name");
                } else if (TextUtils.isEmpty(to)) {
                    Utl.showToast(getActivity(), "Enter To Email");
                } else if (TextUtils.isEmpty(subject)) {
                    Utl.showToast(getActivity(), "Enter Subject");
                } else {
                    sendEmail(strRelated,
                            strWhat,
                            name,
                            from,
                            to,
                            cc,
                            bcc,
                            subject,
                            body,
                            template);
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickClose.callbackSendEmailClose(strRelated, strWhat, strHeader);
            }
        });

        return view;

    }

    private void dialogFileBrowse() {
        final Dialog dialogMore = new Dialog(getActivity(), R.style.DialogFullScreen);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMore.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        dialogMore.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMore.setCancelable(true);
        dialogMore.setCanceledOnTouchOutside(true);
        dialogMore.setContentView(R.layout.dialog_file_browse);
        TextView buttonCamera = (TextView) dialogMore.findViewById(R.id.button_camera);
        TextView buttonGallery = (TextView) dialogMore.findViewById(R.id.button_gallery);
        TextView buttonFile = (TextView) dialogMore.findViewById(R.id.button_file);
        Button buttonClose = (Button) dialogMore.findViewById(R.id.button_close);

        buttonCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Pix.start(getActivity(), 9001, 1);
                dialogMore.dismiss();
            }
        });

        buttonGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent, 9002);
                dialogMore.dismiss();
            }
        });

        buttonFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("*/*");
                startActivityForResult(intent, 9003);
                dialogMore.dismiss();
            }
        });

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
            }
        });

        dialogMore.show();
        dialogMore.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialogMore.getWindow().setDimAmount(0.5f);
        dialogMore.getWindow().setAttributes(lp);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 9001:
                if (resultCode == Pix.RESULT_OK) {
                    //isFileClicked = true;
                    //ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    attachmentPath = cursor.getString(columnIndex);
                    cursor.close();
                } else if (resultCode == Pix.RESULT_CANCELED) {
                    Utl.showToast(getActivity(), "Cancelled");
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
                break;
            case 9002:
                if (resultCode == RESULT_OK) {
                    isFileClicked = true;
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    attachmentPath = cursor.getString(columnIndex);
                    cursor.close();
                } else if (resultCode == RESULT_CANCELED) {
                    Utl.showToast(getActivity(), "Cancelled");
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
                break;
            case 9003:
                if (resultCode == RESULT_OK && null != data) {
                    isFileClicked = true;
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    attachmentPath = cursor.getString(columnIndex);
                    cursor.close();
                } else if (resultCode == RESULT_CANCELED) {
                    Utl.showToast(getActivity(), "Cancelled");
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
                break;
        }
    }

    private void getFrom() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getAccAllUsers(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_From" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listMail.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_UserID));
                                model.setName(dataObject.getString(Cons.KEY_Email));
                                listMail.add(model);
                            }
                            setSpinnerDropDownHeight(listMail, spinnerMail);
                            spinnerMail.setAdapter(adapterMail);
                            spinnerMail.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getTemplate() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getEmailTemplate(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Template" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listTemplate.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_EmailTemplateID));
                                model.setName(dataObject.getString(Cons.KEY_Title));
                                listTemplate.add(model);
                            }
                            setSpinnerDropDownHeight(listTemplate, spinnerTemplate);
                            spinnerTemplate.setAdapter(adapterTemplate);
                            spinnerTemplate.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void sendEmail(String relatedTo,
                           String what,
                           String fromName,
                           String fromEmail,
                           String toEmail,
                           String ccEmail,
                           String bccEmail,
                           String subject,
                           String body,
                           String template) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), relatedTo));
        map.put(Cons.KEY_What, RequestBody.create(MediaType.parse("text/plain"), what));
        map.put(Cons.KEY_FromName, RequestBody.create(MediaType.parse("text/plain"), fromName));
        map.put(Cons.KEY_FromEmailID, RequestBody.create(MediaType.parse("text/plain"), fromEmail));
        map.put(Cons.KEY_ToEmailID, RequestBody.create(MediaType.parse("text/plain"), toEmail));
        if (!TextUtils.isEmpty(ccEmail)) {
            map.put(Cons.KEY_CCEmailID, RequestBody.create(MediaType.parse("text/plain"), ccEmail));
        }
        if (!TextUtils.isEmpty(bccEmail)) {
            map.put(Cons.KEY_BCCEmailID, RequestBody.create(MediaType.parse("text/plain"), bccEmail));
        }
        map.put(Cons.KEY_EmailSubject, RequestBody.create(MediaType.parse("text/plain"), subject));
        if (!TextUtils.isEmpty(body)) {
            map.put(Cons.KEY_EmailBody, RequestBody.create(MediaType.parse("text/plain"), body));
        }
        if (!TextUtils.isEmpty(template)) {
            map.put(Cons.KEY_EmailTemplate, RequestBody.create(MediaType.parse("text/plain"), template));
        }

        Call<ResponseBody> response;
        if (isFileClicked) {
            File file = new File(attachmentPath);
            RequestBody reqFile = RequestBody.create(MediaType.parse("*/*"), file);
            MultipartBody.Part attachment = MultipartBody.Part.createFormData(Cons.KEY_EmailAttachment, file.getName(), reqFile);
            response = apiInterface.sendEmailWithFile(header, map, attachment);
        } else {
            response = apiInterface.sendEmail(header, map);
        }

        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Send_Email" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            String relatedTo = object.getString(Cons.KEY_RelatedTo);
                            String what = object.getString(Cons.KEY_What);
                            clickClose.callbackSendEmailClose(relatedTo, what, strHeader);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void setSpinnerDropDownHeight(List<ModelSpinner> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 70);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    public boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), CAMERA);
        int result2 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION);
        int result3 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_COARSE_LOCATION);
        int result4 = ContextCompat.checkSelfPermission(getActivity(), CALL_PHONE);
        int result5 = ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE);
        int result6 = ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE);
        return
                result1 == PackageManager.PERMISSION_GRANTED
                        && result2 == PackageManager.PERMISSION_GRANTED
                        && result3 == PackageManager.PERMISSION_GRANTED
                        && result4 == PackageManager.PERMISSION_GRANTED
                        && result5 == PackageManager.PERMISSION_GRANTED
                        && result6 == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{
                        CAMERA,
                        ACCESS_FINE_LOCATION,
                        ACCESS_COARSE_LOCATION,
                        CALL_PHONE,
                        READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean locationFineAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean locationCoarseAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean callAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean readAccepted = grantResults[4] == PackageManager.PERMISSION_GRANTED;
                    boolean writeAccepted = grantResults[5] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted &&
                            locationFineAccepted &&
                            locationCoarseAccepted &&
                            callAccepted &&
                            readAccepted &&
                            writeAccepted)
                        Utl.showToast(getActivity(), "Permission Granted.");
                    else {
                        Utl.showToast(getActivity(), "Permission Denied.");
                    }
                }
                break;
        }
    }

}