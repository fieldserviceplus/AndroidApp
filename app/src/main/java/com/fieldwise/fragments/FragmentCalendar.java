package com.fieldwise.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.desai.vatsal.mydynamiccalendar.MyDynamicCalendar;
import com.desai.vatsal.mydynamiccalendar.OnDateClickListener;
import com.fieldwise.R;
import com.fieldwise.activities.ActivityMain;
import com.fieldwise.adapters.AdapterCalendarAssignedTo;
import com.fieldwise.adapters.AdapterCalendarEvent;
import com.fieldwise.adapters.AdapterMapEvent;
import com.fieldwise.models.ModelCalendar;
import com.fieldwise.models.ModelMap;
import com.fieldwise.models.ModelSpinner;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Singleton;
import com.fieldwise.utils.Utl;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.mindorks.placeholderview.PlaceHolderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class FragmentCalendar extends Fragment implements
        AdapterCalendarEvent.CalendarEventClickListen,
        AdapterMapEvent.MapEventClickListen,
        AdapterCalendarAssignedTo.CalendarAssignedToCheckBoxListen,
        OnMapReadyCallback {

    public APIInterface apiInterface;

    private LinearLayout layoutCalendar, layoutMap;

    private MyDynamicCalendar calendarView;
    private List<ModelCalendar> listCalendar;

    private GoogleMap googleMap;
    private SupportMapFragment googleMapView;
    private List<ModelMap> listMap;

    private Date globalDate;
    private PlaceHolderView phvCalendar, phvMap;

    private ImageButton buttonCalendar, buttonFilter, buttonAccount, buttonMore;

    private CheckBox checkboxShowTask;
    private RelativeLayout layoutEditFilter, layoutAssignedTo;
    private LinearLayout layoutEditFilterChange;
    private PlaceHolderView phvAssignedTo;
    private Button buttonCalendarApply, buttonCalendarCancel, buttonAssignedApply, buttonAssignedCancel;

    private List<String> listEditFilter;
    private List<ModelSpinner> listEditFilterType;
    private List<ModelSpinner> listEditFilterStatus;
    private List<ModelSpinner> listEditFilterPriority;

    private List<ModelSpinner> listAssignedTo;

    private int setCalendarView = 2;

    public CalendarEventClickListen clickCalender;

    public interface CalendarEventClickListen {
        void callbackCalendarEvent(ModelCalendar model);
    }

    public MapEventClickListen clickMap;

    public interface MapEventClickListen {
        void callbackMapEvent(ModelMap model);
    }

    private ImageButton buttonToggleCalendar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        clickCalender = (CalendarEventClickListen) getActivity();
        clickMap = (MapEventClickListen) getActivity();

        layoutCalendar = (LinearLayout) view.findViewById(R.id.layout_calendar);
        layoutMap = (LinearLayout) view.findViewById(R.id.layout_map);

        calendarView = (MyDynamicCalendar) view.findViewById(R.id.calendar_view);

        phvCalendar = (PlaceHolderView) view.findViewById(R.id.phv_calendar);
        phvMap = (PlaceHolderView) view.findViewById(R.id.phv_map);

        buttonCalendar = (ImageButton) view.findViewById(R.id.button_calendar);
        buttonFilter = (ImageButton) view.findViewById(R.id.button_filter);
        buttonAccount = (ImageButton) view.findViewById(R.id.button_account);
        buttonMore = (ImageButton) view.findViewById(R.id.button_more);

        layoutEditFilter = (RelativeLayout) view.findViewById(R.id.layout_edit_filter);
        layoutAssignedTo = (RelativeLayout) view.findViewById(R.id.layout_assigned_to);

        checkboxShowTask = (CheckBox) view.findViewById(R.id.checkbox_show_task);

        layoutEditFilterChange = (LinearLayout) view.findViewById(R.id.layout_edit_filter_change);
        phvAssignedTo = (PlaceHolderView) view.findViewById(R.id.phv_assigned_to);

        buttonCalendarApply = (Button) view.findViewById(R.id.button_calendar_apply);
        buttonCalendarCancel = (Button) view.findViewById(R.id.button_calendar_cancel);
        buttonAssignedApply = (Button) view.findViewById(R.id.button_assigned_apply);
        buttonAssignedCancel = (Button) view.findViewById(R.id.button_assigned_cancel);

        buttonToggleCalendar = (ImageButton) view.findViewById(R.id.button_toggle_calendar);

        googleMapView = (SupportMapFragment) (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.calendar_map);

        layoutCalendar.setVisibility(View.VISIBLE);
        layoutMap.setVisibility(View.GONE);

        Singleton.getInstance().listCalendarFilterType.clear();
        Singleton.getInstance().listCalendarFilterStatus.clear();
        Singleton.getInstance().listCalendarFilterPriority.clear();
        Singleton.getInstance().listCalendarAssignedToID.clear();
        listEditFilter = new ArrayList<>();
        listEditFilter.add("Type");
        listEditFilter.add("Status");
        listEditFilter.add("Priority");
        listEditFilterType = new ArrayList<>();
        listEditFilterStatus = new ArrayList<>();
        listEditFilterPriority = new ArrayList<>();

        listAssignedTo = new ArrayList<>();

        calendarView.showMonthView();
        calendarView.setCalendarBackgroundColor("#ffffff");
        calendarView.setHeaderBackgroundColor("#009e0f");
        calendarView.setHeaderTextColor("#ffffff");
        calendarView.setNextPreviousIndicatorColor("#ffffff");
        calendarView.setCurrentDateBackgroundColor("#009e0f");
        calendarView.setCurrentDateTextColor("#ffffff");

        calendarView.setEventCellTextColor("#ff0000");

        View viwCalendar = calendarView.getRootView().findViewById(R.id.ll_header_views);
        ImageView imageviewPrevious = (ImageView) viwCalendar.findViewById(R.id.iv_previous);
        ImageView imageviewNext = (ImageView) viwCalendar.findViewById(R.id.iv_next);

        listCalendar = new ArrayList<ModelCalendar>();
        listMap = new ArrayList<ModelMap>();

        globalDate = Calendar.getInstance().getTime();
        setCalendarView = 2;
        setCalendar(globalDate, 0, setCalendarView);

        ActivityMain.textviewCalenderDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                globalDate = Calendar.getInstance().getTime();
                setCalendar(globalDate, 0, setCalendarView);
            }
        });

        imageviewPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setCalendar(globalDate, -1, setCalendarView);
            }
        });
        imageviewNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setCalendar(globalDate, 1, setCalendarView);
            }
        });

        calendarView.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onClick(Date date) {
                setCalendar(date, 0, 0);
            }

            @Override
            public void onLongClick(Date date) {
            }
        });

        buttonCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogCalendarMore();
            }
        });

        buttonFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (layoutEditFilter.getVisibility() != View.VISIBLE) {
                    layoutEditFilter.setVisibility(View.VISIBLE);
                    checkboxShowTask.setChecked(true);
                    getCalendarFilters();
                }
            }
        });

        buttonAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (layoutAssignedTo.getVisibility() != View.VISIBLE) {
                    layoutAssignedTo.setVisibility(View.VISIBLE);
                    getAssignedTo();
                }
            }
        });

        buttonMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogCalendarAdd();
            }
        });

        buttonCalendarApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StringBuilder sbType = new StringBuilder();
                for (int i = 0; i < Singleton.getInstance().listCalendarFilterType.size(); i++) {
                    Log.d("TAG_Type", Singleton.getInstance().listCalendarFilterType.get(i));
                    sbType.append(Singleton.getInstance().listCalendarFilterType.get(i) + ",");
                }
                StringBuilder sbStatus = new StringBuilder();
                for (int i = 0; i < Singleton.getInstance().listCalendarFilterStatus.size(); i++) {
                    Log.d("TAG_Status", Singleton.getInstance().listCalendarFilterStatus.get(i));
                    sbStatus.append(Singleton.getInstance().listCalendarFilterStatus.get(i) + ",");
                }
                StringBuilder sbPriority = new StringBuilder();
                for (int i = 0; i < Singleton.getInstance().listCalendarFilterPriority.size(); i++) {
                    Log.d("TAG_Priority", Singleton.getInstance().listCalendarFilterPriority.get(i));
                    sbPriority.append(Singleton.getInstance().listCalendarFilterPriority.get(i) + ",");
                }

                String strType = sbType.toString();
                if (strType.endsWith(",")) {
                    strType = strType.substring(0, strType.length() - 1);
                }
                String strStatus = sbStatus.toString();
                if (strStatus.endsWith(",")) {
                    strStatus = strStatus.substring(0, strStatus.length() - 1);
                }
                String strPriority = sbPriority.toString();
                if (strPriority.endsWith(",")) {
                    strPriority = strPriority.substring(0, strPriority.length() - 1);
                }

                int showTask;
                if (checkboxShowTask.isChecked()) {
                    showTask = 1;
                } else {
                    showTask = 0;
                }

                StringBuilder sbAssignedTo = new StringBuilder();
                for (int i = 0; i < Singleton.getInstance().listCalendarAssignedToID.size(); i++) {
                    Log.d("TAG_Assigned_ID", Singleton.getInstance().listCalendarAssignedToID.get(i));
                    sbAssignedTo.append(Singleton.getInstance().listCalendarAssignedToID.get(i) + ",");
                }
                String strAssignedTo = sbAssignedTo.toString();
                if (strAssignedTo.endsWith(",")) {
                    strAssignedTo = strAssignedTo.substring(0, strAssignedTo.length() - 1);
                }

                Calendar c = Calendar.getInstance();
                c.setTime(globalDate);
                int day = c.get(Calendar.DATE);
                int month = c.get(Calendar.MONTH);
                int year = c.get(Calendar.YEAR);
                int lastDayOfMonth = c.getActualMaximum(Calendar.DAY_OF_MONTH);
                Log.d("TAG_lastDayOfMonth", "" + lastDayOfMonth);
                c.set(year, month - 1, day);
                Date date = c.getTime();

                int startDate = 1;
                int endDate = lastDayOfMonth;
                startDate = day;
                endDate = day;
                calendarView.setCalendarDate(day, month, year);
                String fromDate = setAPISimpleDate("" + year + "-" + month + "-" + startDate);
                String toDate = setAPISimpleDate("" + year + "-" + month + "-" + endDate);
                Log.d("TAG_", "\nCurrentMonth: " + month + "\nFrom: " + fromDate + "\n" + "To: " + toDate);

                getCalendarEvents(date,
                        fromDate, toDate,
                        true, showTask,
                        strType, strStatus, strPriority, strAssignedTo);

                getMapEvents(date,
                        fromDate, toDate,
                        true, showTask,
                        strType, strStatus, strPriority, strAssignedTo);

                listEditFilterType.clear();
                listEditFilterStatus.clear();
                listEditFilterPriority.clear();
                layoutEditFilterChange.removeAllViews();
                layoutEditFilter.setVisibility(View.GONE);
            }
        });

        buttonCalendarCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listEditFilterType.clear();
                listEditFilterStatus.clear();
                listEditFilterPriority.clear();
                layoutEditFilterChange.removeAllViews();
                layoutEditFilter.setVisibility(View.GONE);
            }
        });

        buttonAssignedApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StringBuilder sbAssignedTo = new StringBuilder();
                for (int i = 0; i < Singleton.getInstance().listCalendarAssignedToID.size(); i++) {
                    Log.d("TAG_Assigned_ID", Singleton.getInstance().listCalendarAssignedToID.get(i));
                    sbAssignedTo.append(Singleton.getInstance().listCalendarAssignedToID.get(i) + ",");
                }
                String strAssignedTo = sbAssignedTo.toString();
                if (strAssignedTo.endsWith(",")) {
                    strAssignedTo = strAssignedTo.substring(0, strAssignedTo.length() - 1);
                }
                listAssignedTo.clear();
                phvAssignedTo.removeAllViews();
                layoutAssignedTo.setVisibility(View.GONE);
            }
        });

        buttonAssignedCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listAssignedTo.clear();
                phvAssignedTo.removeAllViews();
                layoutAssignedTo.setVisibility(View.GONE);
            }
        });

        buttonToggleCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (calendarView.getVisibility() == View.VISIBLE) {
                    calendarView.setVisibility(View.GONE);
                    buttonToggleCalendar.setBackgroundResource(R.drawable.ic_toggle_down);
                } else if (calendarView.getVisibility() == View.GONE) {
                    calendarView.setVisibility(View.VISIBLE);
                    buttonToggleCalendar.setBackgroundResource(R.drawable.ic_toggle_up);
                }
            }
        });

        return view;
    }

    private void setCalendar(Date date, int isNextPrevious, int isDayWeekMonth) {

        int currentMonth = 0;

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);
        String startDate = "" + year + "-" + month + "-" + day;
        String endDate = "" + year + "-" + month + "-" + day;
        String fromDate = setAPISimpleDate(startDate);
        String toDate = setAPISimpleDate(endDate);
        Log.d("TAG_", "---\nFrom: " + fromDate + "\n" + "To: " + toDate);

        if (isNextPrevious == -1) {
            if (isDayWeekMonth == 0) {
                // Day Previous
                Calendar c1 = (Calendar) Calendar.getInstance();
                c1.setTime(date);
                c1.add(Calendar.DATE, -1);
                int day1 = c1.get(Calendar.DATE);
                int month1 = c1.get(Calendar.MONTH);
                int year1 = c1.get(Calendar.YEAR);

                currentMonth = month1;
                c1.set(year1, month1, day1);
                startDate = "" + year1 + "-" + currentMonth + "-" + day1;
                endDate = "" + year1 + "-" + currentMonth + "-" + day1;

                c1.set(year1, currentMonth, day1);
                globalDate = c1.getTime();
                calendarView.setCalendarDate(day1, currentMonth, year1);
            } else if (isDayWeekMonth == 1) {
                //Week Previous
                Calendar c1 = (Calendar) Calendar.getInstance();
                c1.setTime(date);
                c1.add(Calendar.WEEK_OF_MONTH, -1);
                int day1 = c1.get(Calendar.DATE);
                int month1 = c1.get(Calendar.MONTH);
                int year1 = c1.get(Calendar.YEAR);

                currentMonth = month1;
                c1.set(year1, month1, day1);

                startDate = "" + year1 + "-" + currentMonth + "-" + day1;
                c1.add(Calendar.DATE, +6);
                int day2 = c1.get(Calendar.DATE);
                int month2 = c1.get(Calendar.MONTH);
                int year2 = c1.get(Calendar.YEAR);
                endDate = "" + year2 + "-" + month2 + "-" + day2;

                c1.add(Calendar.DATE, -6);
                c1.set(year1, currentMonth, day1);
                globalDate = c1.getTime();
                calendarView.setCalendarDate(day1, currentMonth, year1);
            } else if (isDayWeekMonth == 2) {
                //Month Previous
                Calendar c1 = (Calendar) Calendar.getInstance();
                c1.setTime(date);
                int day1 = c1.get(Calendar.DATE);
                int month1 = c1.get(Calendar.MONTH);
                int year1 = c1.get(Calendar.YEAR);

                currentMonth = month1 - 1;
                c1.set(year1, month1 - 2, day1);
                int lastDay = c1.getActualMaximum(Calendar.DAY_OF_MONTH);
                Log.d("TAG_lastDay", "" + lastDay);
                startDate = "" + year1 + "-" + currentMonth + "-01";
                endDate = "" + year1 + "-" + currentMonth + "-" + lastDay;

                c1.set(year1, currentMonth, 1);
                globalDate = c1.getTime();
                calendarView.setCalendarDate(1, currentMonth, year1);
            }
        } else if (isNextPrevious == 0) {
            if (isDayWeekMonth == 0) {
                //Day Current
                Calendar c1 = (Calendar) Calendar.getInstance();
                c1.setTime(date);
                int day1 = c1.get(Calendar.DATE);
                int month1 = c1.get(Calendar.MONTH);
                int year1 = c1.get(Calendar.YEAR);

                currentMonth = month1 + 1;
                c1.set(year1, month1, day1);
                startDate = "" + year1 + "-" + currentMonth + "-" + day1;
                endDate = "" + year1 + "-" + currentMonth + "-" + day1;

                c1.set(year1, currentMonth, day1);
                globalDate = c1.getTime();
                calendarView.setCalendarDate(day1, currentMonth, year1);
            } else if (isDayWeekMonth == 1) {
                //Week Current
                Calendar c1 = (Calendar) Calendar.getInstance();
                c1.setTime(date);
                int day1 = c1.get(Calendar.DATE);
                int month1 = c1.get(Calendar.MONTH);
                int year1 = c1.get(Calendar.YEAR);

                currentMonth = month1 + 1;
                c1.set(year1, month1, day1);

                startDate = "" + year1 + "-" + currentMonth + "-" + day1;
                c1.add(Calendar.DATE, +6);
                int day2 = c1.get(Calendar.DATE);
                int month2 = c1.get(Calendar.MONTH);
                int year2 = c1.get(Calendar.YEAR);
                endDate = "" + year2 + "-" + (month2 + 1) + "-" + day2;

                c1.add(Calendar.DATE, -6);
                c1.set(year1, currentMonth, day1);
                globalDate = c1.getTime();
                calendarView.setCalendarDate(day1, currentMonth, year1);
            } else if (isDayWeekMonth == 2) {
                //Month Current
                Calendar c1 = (Calendar) Calendar.getInstance();
                c1.setTime(date);
                int day1 = c1.get(Calendar.DATE);
                int month1 = c1.get(Calendar.MONTH);
                int year1 = c1.get(Calendar.YEAR);

                currentMonth = month1 + 1;
                c1.set(year1, month1, day1);
                int lastDay = c1.getActualMaximum(Calendar.DAY_OF_MONTH);
                Log.d("TAG_lastDay", "" + lastDay);
                startDate = "" + year1 + "-" + currentMonth + "-01";
                endDate = "" + year1 + "-" + currentMonth + "-" + lastDay;

                c1.set(year1, currentMonth, day1);
                globalDate = c1.getTime();
                calendarView.setCalendarDate(day1, currentMonth, year1);
            }
        } else if (isNextPrevious == 1) {
            if (isDayWeekMonth == 0) {
                //Day Next
                Calendar c1 = (Calendar) Calendar.getInstance();
                c1.setTime(date);
                c1.add(Calendar.DATE, +1);
                int day1 = c1.get(Calendar.DATE);
                int month1 = c1.get(Calendar.MONTH);
                int year1 = c1.get(Calendar.YEAR);

                currentMonth = month1;
                c1.set(year1, month1, day1);
                startDate = "" + year1 + "-" + currentMonth + "-" + day1;
                endDate = "" + year1 + "-" + currentMonth + "-" + day1;

                c1.set(year1, currentMonth, day1);
                globalDate = c1.getTime();
                calendarView.setCalendarDate(day1, currentMonth, year1);
            } else if (isDayWeekMonth == 1) {
                //Week Next
                Calendar c1 = (Calendar) Calendar.getInstance();
                c1.setTime(date);
                c1.add(Calendar.WEEK_OF_MONTH, +1);
                int day1 = c1.get(Calendar.DATE);
                int month1 = c1.get(Calendar.MONTH);
                int year1 = c1.get(Calendar.YEAR);

                currentMonth = month1;
                c1.set(year1, month1, day1);

                startDate = "" + year1 + "-" + currentMonth + "-" + day1;
                c1.add(Calendar.DATE, +6);
                int day2 = c1.get(Calendar.DATE);
                int month2 = c1.get(Calendar.MONTH);
                int year2 = c1.get(Calendar.YEAR);
                endDate = "" + year2 + "-" + month2 + "-" + day2;

                c1.add(Calendar.DATE, -6);
                c1.set(year1, currentMonth, day1);
                globalDate = c1.getTime();
                calendarView.setCalendarDate(day1, currentMonth, year1);
            } else if (isDayWeekMonth == 2) {
                //Month Next
                Calendar c1 = (Calendar) Calendar.getInstance();
                c1.setTime(date);
                int day1 = c1.get(Calendar.DATE);
                int month1 = c1.get(Calendar.MONTH);
                int year1 = c1.get(Calendar.YEAR);

                currentMonth = month1 + 1;
                c1.set(year1, month1, day1);
                int lastDay = c1.getActualMaximum(Calendar.DAY_OF_MONTH);
                Log.d("TAG_lastDay", "" + lastDay);
                startDate = "" + year1 + "-" + currentMonth + "-01";
                endDate = "" + year1 + "-" + currentMonth + "-" + lastDay;

                c1.set(year1, currentMonth, 1);
                globalDate = c1.getTime();
                calendarView.setCalendarDate(1, currentMonth, year1);
            }
        }

        fromDate = setAPISimpleDate(startDate);
        toDate = setAPISimpleDate(endDate);
        Log.d("TAG_", "setCalendarMonth---\nFrom: " + fromDate + "\n" + "To: " + toDate);

        getCalendarEvents(globalDate,
                fromDate, toDate,
                false, 1,
                "", "", "", "");

        getMapEvents(globalDate,
                fromDate, toDate,
                false, 1,
                "", "", "", "");
    }

    public String setAPISimpleDate(String strDate) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "MM/dd/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(strDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String getSimpleDate(String strDate) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(strDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void getCalendarEvents(final Date currentDate,
                                   String fromDate, String toDate,
                                   boolean isFilter,
                                   int showTask,
                                   String type, String status, String priority,
                                   String assignedTo) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_FromDate, RequestBody.create(MediaType.parse("text/plain"), fromDate));
        map.put(Cons.KEY_ToDate, RequestBody.create(MediaType.parse("text/plain"), toDate));
        if (isFilter) {
            map.put(Cons.KEY_WO_Type_ARR, RequestBody.create(MediaType.parse("text/plain"), type));
            map.put(Cons.KEY_WO_Status_ARR, RequestBody.create(MediaType.parse("text/plain"), status));
            map.put(Cons.KEY_WO_Priority_ARR, RequestBody.create(MediaType.parse("text/plain"), priority));
            map.put(Cons.KEY_ShowTasks, RequestBody.create(MediaType.parse("text/plain"), String.valueOf(showTask)));
            map.put(Cons.KEY_WO_AssignedTo_ARR, RequestBody.create(MediaType.parse("text/plain"), assignedTo));
        }
        Call<ResponseBody> response = apiInterface.getCalendarEvents(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_cal_event" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listCalendar.clear();
                            phvCalendar.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);

                                String strId = dataObject.getString(Cons.KEY_ID);
                                String strType = dataObject.getString(Cons.KEY_Type);
                                String strTitle = dataObject.getString(Cons.KEY_Title);
                                String strTime = dataObject.getString(Cons.KEY_Time);

                                String strStartDate = getSimpleDate(dataObject.getString(Cons.KEY_Start));
                                String strEndDate = getSimpleDate(dataObject.getString(Cons.KEY_End));

                                String strStartTime = "00:00";
                                String strEndTime = "23:59";
                                if (!strTime.equals("All-Day")) {
                                    strStartTime = dataObject.getString(Cons.KEY_StartTime);
                                    strEndTime = dataObject.getString(Cons.KEY_EndTime);
                                }
                                String strColorCode = dataObject.getString(Cons.KEY_ColorCode);

                                String strRelatedTo = dataObject.getString(Cons.KEY_RelatedTo);
                                String strRelatedObjID = dataObject.getString(Cons.KEY_RelatedObjID);
                                String strRelatedObjNo = dataObject.getString(Cons.KEY_RelatedObjNo);

                                ModelCalendar model = new ModelCalendar();
                                model.setID(strId);
                                model.setType(strType);
                                model.setTitle(strTitle);
                                model.setTime(strTime);
                                model.setStartDate(strStartDate);
                                model.setEndDate(strEndDate);
                                model.setStartTime(strStartTime);
                                model.setEndTime(strEndTime);
                                model.setColorCode(strColorCode);
                                model.setRelatedTo(strRelatedTo);
                                model.setRelatedObjID(strRelatedObjID);
                                model.setRelatedObjNo(strRelatedObjNo);

                                listCalendar.add(model);
                                calendarView.addEvent(strStartDate, strStartTime, strEndTime, strTitle);
                            }
                            calendarView.refreshCalendar();

                            Collections.sort(listCalendar, new Comparator<ModelCalendar>() {
                                public int compare(ModelCalendar obj1, ModelCalendar obj2) {

                                    String x1 = obj1.getStartTime();
                                    String x2 = obj2.getStartTime();
                                    int comp = x1.compareTo(x2);
                                    if (comp != 0) {
                                        return comp;
                                    }

                                    String inputPattern = "dd-MM-yyyy";
                                    Date date1 = null;
                                    try {
                                        date1 = new SimpleDateFormat(inputPattern).parse(obj1.getStartDate());
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    Date date2 = null;
                                    try {
                                        date2 = new SimpleDateFormat(inputPattern).parse(obj2.getStartDate());
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    Date x3 = date1;
                                    Date x4 = date2;
                                    return x3.compareTo(x4);
                                }
                            });

                            int count = 0;
                            for (int i = 0; i < listCalendar.size(); i++) {
                                if (listCalendar.get(i).getTime().equals("All-Day")) {
                                    count++;
                                }
                            }

                            List<ModelCalendar> list = new ArrayList<ModelCalendar>();
                            for (int i = count; i < listCalendar.size(); i++) {
                                list.add(listCalendar.get(i));
                            }
                            Collections.sort(list, new Comparator<ModelCalendar>() {
                                public int compare(ModelCalendar obj1, ModelCalendar obj2) {
                                    String inputPattern = "dd-MM-yyyy";
                                    Date date1 = null;
                                    try {
                                        date1 = new SimpleDateFormat(inputPattern).parse(obj1.getStartDate());
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    Date date2 = null;
                                    try {
                                        date2 = new SimpleDateFormat(inputPattern).parse(obj2.getStartDate());
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    Date x3 = date1;
                                    Date x4 = date2;
                                    int comp = x3.compareTo(x4);
                                    if (comp != 0) {
                                        return comp;
                                    }
                                    String x1 = obj1.getStartTime();
                                    String x2 = obj2.getStartTime();
                                    return x1.compareTo(x2);
                                }
                            });

                            for (int i = 0; i < list.size(); i++) {
                                listCalendar.set(count + i, list.get(i));
                            }

                            for (int i = 0; i < listCalendar.size(); i++) {
                                phvCalendar.addView(new AdapterCalendarEvent(getActivity(), FragmentCalendar.this, currentDate, listCalendar.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_cal_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_cal_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_cal_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_cal_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getCalendarFilters() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getCalendarFilters(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_cal_filter" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            JSONObject dataObject = object.getJSONObject("data");
                            JSONArray WOType = dataObject.getJSONArray("WOType");
                            JSONArray WOStatus = dataObject.getJSONArray("WOStatus");
                            JSONArray WOPriority = dataObject.getJSONArray("WOPriority");
                            for (int i = 0; i < WOType.length(); i++) {
                                JSONObject objectType = WOType.getJSONObject(i);
                                listEditFilterType.add(
                                        new ModelSpinner(objectType.getString("WorkOrderTypeID"), objectType.getString("WorkOrderType")));
                            }
                            for (int i = 0; i < WOStatus.length(); i++) {
                                JSONObject objectStatus = WOStatus.getJSONObject(i);
                                listEditFilterStatus.add(
                                        new ModelSpinner(objectStatus.getString("WOStatusID"), objectStatus.getString("Status")));
                            }
                            for (int i = 0; i < WOPriority.length(); i++) {
                                JSONObject objectPriority = WOPriority.getJSONObject(i);
                                listEditFilterPriority.add(
                                        new ModelSpinner(objectPriority.getString("WOPriorityID"), objectPriority.getString("Priority")));
                            }

                            for (int i = 0; i < listEditFilter.size(); i++) {
                                LayoutInflater layoutInflater1 = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View vMin = layoutInflater1.inflate(R.layout.layout_calendar_filter, null);
                                TextView textViewName = (TextView) vMin.findViewById(R.id.textview_name);
                                ToggleButton toggleIcon = (ToggleButton) vMin.findViewById(R.id.toggle_icon);
                                final LinearLayout layoutCalendarSubFilter = (LinearLayout) vMin.findViewById(R.id.layout_calendar_sub_filter);

                                String strTitle = listEditFilter.get(i);
                                textViewName.setText(strTitle);
                                toggleIcon.setChecked(false);
                                layoutCalendarSubFilter.setVisibility(View.GONE);
                                toggleIcon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                    @Override
                                    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                                        if (isChecked) {
                                            layoutCalendarSubFilter.setVisibility(View.VISIBLE);
                                        } else {
                                            layoutCalendarSubFilter.setVisibility(View.GONE);
                                        }
                                    }
                                });
                                layoutEditFilterChange.addView(vMin);
                                if (strTitle.equals("Type")) {
                                    for (int j = 0; j < listEditFilterType.size(); j++) {
                                        final String strId = listEditFilterType.get(j).getId();
                                        LayoutInflater layoutInflater2 = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                        View vSub = layoutInflater2.inflate(R.layout.layout_calendar_sub_filter, null);
                                        CheckBox checkBoxId = (CheckBox) vSub.findViewById(R.id.checkbox_id);
                                        TextView textViewSubName = (TextView) vSub.findViewById(R.id.textview_name);
                                        if (Singleton.getInstance().listCalendarFilterType.contains(strId)) {
                                            checkBoxId.setChecked(true);
                                        } else {
                                            checkBoxId.setChecked(false);
                                        }
                                        textViewSubName.setText(listEditFilterType.get(j).getName());
                                        layoutCalendarSubFilter.addView(vSub);
                                        checkBoxId.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                            @Override
                                            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                                                if (isChecked) {
                                                    if (!Singleton.getInstance().listCalendarFilterType.contains(strId)) {
                                                        Singleton.getInstance().listCalendarFilterType.add(strId);
                                                    }
                                                } else {
                                                    if (Singleton.getInstance().listCalendarFilterType.contains(strId)) {
                                                        int pos = Singleton.getInstance().listCalendarFilterType.indexOf(strId);
                                                        Singleton.getInstance().listCalendarFilterType.remove(pos);
                                                    }
                                                }
                                            }
                                        });
                                    }
                                } else if (strTitle.equals("Status")) {
                                    for (int j = 0; j < listEditFilterStatus.size(); j++) {
                                        final String strId = listEditFilterStatus.get(j).getId();
                                        LayoutInflater layoutInflater2 = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                        View vSub = layoutInflater2.inflate(R.layout.layout_calendar_sub_filter, null);
                                        CheckBox checkBoxId = (CheckBox) vSub.findViewById(R.id.checkbox_id);
                                        TextView textViewSubName = (TextView) vSub.findViewById(R.id.textview_name);
                                        if (Singleton.getInstance().listCalendarFilterStatus.contains(strId)) {
                                            checkBoxId.setChecked(true);
                                        } else {
                                            checkBoxId.setChecked(false);
                                        }
                                        textViewSubName.setText(listEditFilterStatus.get(j).getName());
                                        layoutCalendarSubFilter.addView(vSub);
                                        checkBoxId.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                            @Override
                                            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                                                if (isChecked) {
                                                    if (!Singleton.getInstance().listCalendarFilterStatus.contains(strId)) {
                                                        Singleton.getInstance().listCalendarFilterStatus.add(strId);
                                                    }
                                                } else {
                                                    if (Singleton.getInstance().listCalendarFilterStatus.contains(strId)) {
                                                        int pos = Singleton.getInstance().listCalendarFilterStatus.indexOf(strId);
                                                        Singleton.getInstance().listCalendarFilterStatus.remove(pos);
                                                    }
                                                }
                                            }
                                        });
                                    }
                                } else if (strTitle.equals("Priority")) {
                                    for (int j = 0; j < listEditFilterPriority.size(); j++) {
                                        final String strId = listEditFilterPriority.get(j).getId();
                                        LayoutInflater layoutInflater2 = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                        View vSub = layoutInflater2.inflate(R.layout.layout_calendar_sub_filter, null);
                                        CheckBox checkBoxId = (CheckBox) vSub.findViewById(R.id.checkbox_id);
                                        TextView textViewSubName = (TextView) vSub.findViewById(R.id.textview_name);
                                        if (Singleton.getInstance().listCalendarFilterPriority.contains(strId)) {
                                            checkBoxId.setChecked(true);
                                        } else {
                                            checkBoxId.setChecked(false);
                                        }
                                        textViewSubName.setText(listEditFilterPriority.get(j).getName());
                                        layoutCalendarSubFilter.addView(vSub);
                                        checkBoxId.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                            @Override
                                            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                                                if (isChecked) {
                                                    if (!Singleton.getInstance().listCalendarFilterPriority.contains(strId)) {
                                                        Singleton.getInstance().listCalendarFilterPriority.add(strId);
                                                    }
                                                } else {
                                                    if (Singleton.getInstance().listCalendarFilterPriority.contains(strId)) {
                                                        int pos = Singleton.getInstance().listCalendarFilterPriority.indexOf(strId);
                                                        Singleton.getInstance().listCalendarFilterPriority.remove(pos);
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getAssignedTo() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getCalendarAssignedTo(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_cal_assigned" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAssignedTo.clear();
                            phvAssignedTo.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_UserID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listAssignedTo.add(model);
                            }
                            for (int i = 0; i < listAssignedTo.size(); i++) {
                                phvAssignedTo.addView(new AdapterCalendarAssignedTo(getActivity(), FragmentCalendar.this, listAssignedTo.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getMapEvents(final Date currentDate,
                              String fromDate, String toDate,
                              boolean isFilter,
                              int showTask,
                              String type, String status, String priority,
                              String assignedTo) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_FromDate, RequestBody.create(MediaType.parse("text/plain"), fromDate));
        map.put(Cons.KEY_ToDate, RequestBody.create(MediaType.parse("text/plain"), toDate));
        if (isFilter) {
            map.put(Cons.KEY_ShowTasks, RequestBody.create(MediaType.parse("text/plain"), String.valueOf(showTask)));
            map.put(Cons.KEY_WO_Type_ARR, RequestBody.create(MediaType.parse("text/plain"), type));
            map.put(Cons.KEY_WO_Status_ARR, RequestBody.create(MediaType.parse("text/plain"), status));
            map.put(Cons.KEY_WO_Priority_ARR, RequestBody.create(MediaType.parse("text/plain"), priority));
            map.put(Cons.KEY_WO_AssignedTo_ARR, RequestBody.create(MediaType.parse("text/plain"), assignedTo));
        }
        Call<ResponseBody> response = apiInterface.getMapEvents(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_map_event" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listMap.clear();
                            phvMap.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);

                                String strWorkOrderID = dataObject.getString(Cons.KEY_WorkOrderID);
                                String strTitle = dataObject.getString(Cons.KEY_Title);

                                String strStartDate = getSimpleDate(dataObject.getString(Cons.KEY_Start));
                                String strEndDate = getSimpleDate(dataObject.getString(Cons.KEY_End));

                                String strStartTime = dataObject.getString(Cons.KEY_StartTime);
                                String strEndTime = dataObject.getString(Cons.KEY_EndTime);
                                String strTime;
                                if (strEndTime.equals("00:00")) {
                                    strTime = "All-Day";
                                } else {
                                    strTime = strStartTime + ":" + strEndTime;
                                }

                                String strLatitude = dataObject.getString(Cons.KEY_Latitude);
                                String strLongitude = dataObject.getString(Cons.KEY_Longitude);
                                String strShapeType = dataObject.getString(Cons.KEY_ShapeType);
                                String strColorCode = dataObject.getString(Cons.KEY_ColorCode);

                                String strRelatedObjID = dataObject.getString(Cons.KEY_RelatedObjID);
                                String strRelatedObjNo = dataObject.getString(Cons.KEY_RelatedObjNo);

                                ModelMap model = new ModelMap();
                                model.setWorkOrderID(strWorkOrderID);
                                model.setTitle(strTitle);
                                model.setTime(strTime);
                                model.setStartDate(strStartDate);
                                model.setEndDate(strEndDate);
                                model.setStartTime(strStartTime);
                                model.setEndTime(strEndTime);
                                model.setLatitude(strLatitude);
                                model.setLongitude(strLongitude);
                                model.setShapeType(strShapeType);
                                model.setColorCode(strColorCode);
                                model.setRelatedObjID(strRelatedObjID);
                                model.setRelatedObjNo(strRelatedObjNo);
                                listMap.add(model);
                            }

                            Collections.sort(listMap, new Comparator<ModelMap>() {
                                public int compare(ModelMap obj1, ModelMap obj2) {

                                    String x1 = obj1.getEndTime();
                                    String x2 = obj2.getEndTime();
                                    int comp = x1.compareToIgnoreCase(x2);
                                    if (comp != 0) {
                                        return comp;
                                    }

                                    String inputPattern = "dd-MM-yyyy";
                                    Date date1 = null;
                                    try {
                                        date1 = new SimpleDateFormat(inputPattern).parse(obj1.getStartDate());
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    Date date2 = null;
                                    try {
                                        date2 = new SimpleDateFormat(inputPattern).parse(obj2.getStartDate());
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    Date x3 = date1;
                                    Date x4 = date2;
                                    return x3.compareTo(x4);
                                }
                            });

                            int count = 0;
                            for (int i = 0; i < listMap.size(); i++) {
                                if (listMap.get(i).getTime().equals("All-Day")) {
                                    count++;
                                }
                            }

                            List<ModelMap> list = new ArrayList<ModelMap>();
                            for (int i = count; i < listMap.size(); i++) {
                                list.add(listMap.get(i));
                            }
                            Collections.sort(list, new Comparator<ModelMap>() {
                                public int compare(ModelMap obj1, ModelMap obj2) {
                                    String inputPattern = "dd-MM-yyyy";
                                    Date date1 = null;
                                    try {
                                        date1 = new SimpleDateFormat(inputPattern).parse(obj1.getStartDate());
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    Date date2 = null;
                                    try {
                                        date2 = new SimpleDateFormat(inputPattern).parse(obj2.getStartDate());
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    Date x3 = date1;
                                    Date x4 = date2;
                                    int comp = x3.compareTo(x4);
                                    if (comp != 0) {
                                        return comp;
                                    }
                                    String x1 = obj1.getEndTime();
                                    String x2 = obj2.getEndTime();
                                    return x1.compareToIgnoreCase(x2);
                                }
                            });

                            for (int i = 0; i < list.size(); i++) {
                                listMap.set(count + i, list.get(i));
                            }

                            googleMapView.getMapAsync(FragmentCalendar.this);

                            for (int i = 0; i < listMap.size(); i++) {
                                phvMap.addView(new AdapterMapEvent(getActivity(), FragmentCalendar.this, currentDate, listMap.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_map_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_map_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_map_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_map_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void dialogCalendarMore() {
        final Dialog dialogMore = new Dialog(getActivity(), R.style.DialogFullScreen);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMore.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        dialogMore.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMore.setContentView(R.layout.dialog_calendar_more);
        TextView buttonCalDay = (TextView) dialogMore.findViewById(R.id.button_cal_day);
        TextView buttonCalWeek = (TextView) dialogMore.findViewById(R.id.button_cal_week);
        TextView buttonCalMonth = (TextView) dialogMore.findViewById(R.id.button_cal_month);
        TextView buttonMapDay = (TextView) dialogMore.findViewById(R.id.button_map_day);
        TextView buttonMapWeek = (TextView) dialogMore.findViewById(R.id.button_map_week);
        Button buttonClose = (Button) dialogMore.findViewById(R.id.button_close);

        buttonCalDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutCalendar.setVisibility(View.VISIBLE);
                layoutMap.setVisibility(View.GONE);
                calendarView.showDayView();
                setCalendarView = 0;

                Calendar c1 = (Calendar) Calendar.getInstance();
                c1.setTime(globalDate);
                int day1 = c1.get(Calendar.DATE);
                int month1 = c1.get(Calendar.MONTH);
                int year1 = c1.get(Calendar.YEAR);
                int currentMonth = month1 - 1;
                c1.set(year1, currentMonth, day1);
                globalDate = c1.getTime();
                setCalendar(globalDate, 0, setCalendarView);
                dialogMore.dismiss();
            }
        });

        buttonCalWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutCalendar.setVisibility(View.VISIBLE);
                layoutMap.setVisibility(View.GONE);
                calendarView.showWeekView();
                setCalendarView = 1;

                Calendar c1 = (Calendar) Calendar.getInstance();
                c1.setTime(globalDate);
                int day1 = c1.get(Calendar.DATE);
                int month1 = c1.get(Calendar.MONTH);
                int year1 = c1.get(Calendar.YEAR);
                int currentMonth = month1 - 1;
                c1.set(year1, currentMonth, day1);
                c1.set(Calendar.DAY_OF_WEEK, c1.getFirstDayOfWeek());
                c1.add(Calendar.DATE, +1);
                globalDate = c1.getTime();
                setCalendar(globalDate, 0, setCalendarView);
                dialogMore.dismiss();
            }
        });

        buttonCalMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutCalendar.setVisibility(View.VISIBLE);
                layoutMap.setVisibility(View.GONE);
                calendarView.showMonthView();
                setCalendarView = 2;

                Calendar c1 = (Calendar) Calendar.getInstance();
                c1.setTime(globalDate);
                int month1 = c1.get(Calendar.MONTH);
                int year1 = c1.get(Calendar.YEAR);
                int currentMonth = month1 - 1;
                c1.set(year1, currentMonth, 1);
                globalDate = c1.getTime();
                setCalendar(globalDate, 0, setCalendarView);
                dialogMore.dismiss();
            }
        });

        buttonMapDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutCalendar.setVisibility(View.GONE);
                layoutMap.setVisibility(View.VISIBLE);
                dialogMore.dismiss();
            }
        });

        buttonMapWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutCalendar.setVisibility(View.GONE);
                layoutMap.setVisibility(View.VISIBLE);
                dialogMore.dismiss();
            }
        });

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
            }
        });

        dialogMore.show();
        dialogMore.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialogMore.getWindow().setDimAmount(0.5f);
        dialogMore.getWindow().setAttributes(lp);
    }

    private void dialogCalendarAdd() {
        final Dialog dialogMore = new Dialog(getActivity());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMore.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        dialogMore.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMore.setContentView(R.layout.dialog_calendar_add);
        TextView buttonCalTask = (TextView) dialogMore.findViewById(R.id.button_cal_task);
        TextView buttonCalEvent = (TextView) dialogMore.findViewById(R.id.button_cal_event);
        Button buttonClose = (Button) dialogMore.findViewById(R.id.button_close);

        buttonCalTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
            }
        });

        buttonCalEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
            }
        });

        buttonCalEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
            }
        });

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
            }
        });

        dialogMore.show();
        dialogMore.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialogMore.getWindow().setDimAmount(0.5f);
        dialogMore.getWindow().setAttributes(lp);
    }

    @Override
    public void callbackCalendarEvent(ModelCalendar model) {
        if (clickCalender != null) {
            clickCalender.callbackCalendarEvent(model);
        }
    }

    @Override
    public void callbackMapEvent(ModelMap model) {
        if (clickMap != null) {
            clickMap.callbackMapEvent(model);
        }
    }

    @Override
    public void callbackCalendarAssignedToCheckBoxListen(boolean isChecked, ModelSpinner model) {
        if (isChecked) {
            if (!Singleton.getInstance().listCalendarAssignedToID.contains(model.getId())) {
                Singleton.getInstance().listCalendarAssignedToID.add(model.getId());
            }
        } else {
            if (Singleton.getInstance().listCalendarAssignedToID.contains(model.getId())) {
                int pos = Singleton.getInstance().listCalendarAssignedToID.indexOf(model.getId());
                Singleton.getInstance().listCalendarAssignedToID.remove(pos);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setScrollGesturesEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.getUiSettings().setTiltGesturesEnabled(false);

        googleMap.clear();

        for (int i = 0; i < listMap.size(); i++) {

            double lat = Double.valueOf(listMap.get(i).getLatitude());
            double lng = Double.valueOf(listMap.get(i).getLongitude());
            LatLng latLng = new LatLng(lat, lng);

            Bitmap bitmapMarker = null;

            if (listMap.get(i).getShapeType().equals("SQUARE")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.ic_map_sqr);
                    bitmapMarker = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
                    Canvas canvas1 = new Canvas(bitmapMarker);
                    drawable.setBounds(0, 0, canvas1.getWidth(), canvas1.getHeight());
                    drawable.draw(canvas1);
                } else {
                    bitmapMarker = BitmapFactory.decodeResource(getResources(), R.drawable.ic_map_sqr);
                }
                bitmapMarker = bitmapMarker.copy(Bitmap.Config.ARGB_8888, true);
                Paint paint = new Paint();
                ColorFilter filter = new PorterDuffColorFilter(Color.parseColor(listMap.get(i).getColorCode()), PorterDuff.Mode.SRC_IN);
                paint.setColorFilter(filter);
                Canvas canvas = new Canvas(bitmapMarker);
                canvas.drawBitmap(bitmapMarker, 0, 0, paint);
            }
            if (listMap.get(i).getShapeType().equals("CIRCLE")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.ic_map_cir);
                    bitmapMarker = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
                    Canvas canvas1 = new Canvas(bitmapMarker);
                    drawable.setBounds(0, 0, canvas1.getWidth(), canvas1.getHeight());
                    drawable.draw(canvas1);
                } else {
                    bitmapMarker = BitmapFactory.decodeResource(getResources(), R.drawable.ic_map_cir);
                }
                bitmapMarker = bitmapMarker.copy(Bitmap.Config.ARGB_8888, true);
                Paint paint = new Paint();
                ColorFilter filter = new PorterDuffColorFilter(Color.parseColor(listMap.get(i).getColorCode()), PorterDuff.Mode.SRC_IN);
                paint.setColorFilter(filter);
                Canvas canvas = new Canvas(bitmapMarker);
                canvas.drawBitmap(bitmapMarker, 0, 0, paint);
            }
            if (listMap.get(i).getShapeType().equals("STAR")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.ic_map_str);
                    bitmapMarker = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
                    Canvas canvas1 = new Canvas(bitmapMarker);
                    drawable.setBounds(0, 0, canvas1.getWidth(), canvas1.getHeight());
                    drawable.draw(canvas1);
                } else {
                    bitmapMarker = BitmapFactory.decodeResource(getResources(), R.drawable.ic_map_str);
                }
                bitmapMarker = bitmapMarker.copy(Bitmap.Config.ARGB_8888, true);
                Paint paint = new Paint();
                ColorFilter filter = new PorterDuffColorFilter(Color.parseColor(listMap.get(i).getColorCode()), PorterDuff.Mode.SRC_IN);
                paint.setColorFilter(filter);
                Canvas canvas = new Canvas(bitmapMarker);
                canvas.drawBitmap(bitmapMarker, 0, 0, paint);
            }

            MarkerOptions marker = new MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromBitmap(bitmapMarker))
                    .title("Title: " + listMap.get(i).getTitle() + "\n"
                            + "Start: " + listMap.get(i).getStartDate() + ", " + listMap.get(i).getStartTime() + "\n"
                            + "End: " + listMap.get(i).getEndDate() + ", " + listMap.get(i).getEndTime());
            googleMap.setInfoWindowAdapter(new MapInfoWindow(getActivity()));
            googleMap.addMarker(marker);
        }
    }

    public class MapInfoWindow implements GoogleMap.InfoWindowAdapter {
        Context context;
        LayoutInflater inflater;

        public MapInfoWindow(Context context) {
            this.context = context;
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.map_info_window, null);
            TextView textviewInfo = (TextView) v.findViewById(R.id.textview_info);
            textviewInfo.setText(marker.getTitle());
            return v;
        }
    }

}