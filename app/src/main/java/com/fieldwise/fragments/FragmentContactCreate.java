package com.fieldwise.fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fieldwise.R;
import com.fieldwise.adapters.AdapterAccount;
import com.fieldwise.adapters.AdapterAssignedTo;
import com.fieldwise.models.ModelSpinner;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;
import com.mindorks.placeholderview.PlaceHolderView;
import com.schibstedspain.leku.LocationPickerActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class FragmentContactCreate extends Fragment implements LocationListener,
        AdapterAssignedTo.AddAssignedToClickListen,
        AdapterAccount.AddAccountClickListen {

    private static final int PERMISSION_REQUEST_CODE = 200;

    int back_type;

    public APIInterface apiInterface;

    private String strLat = "40.730610";
    private String strLng = "-73.935242";
    private String provider;
    private LocationManager locationManager;

    private String strAssignedToId, strAssignedToName,
            strAccountId, strAccountName,
            strBirthDate,
            strMilLat, strMilLng,
            strMilAddress, strMilCity, strMilState, strMilCountry, strMilPostal;

    private boolean isBirthDayClicked = false;
    private boolean isBilClicked = false;

    private RelativeLayout layoutTitle,
            layoutLeadSource;
    private LinearLayout layoutSalutation;

    private Spinner spinnerTitle,
            spinnerLeadSource,
            spinnerSalutation;

    private List<ModelSpinner> listAssignedTo,
            listAccount,
            listTitle,
            listLeadSource,
            listSalutation;

    private ArrayAdapter<ModelSpinner> adapterTitle,
            adapterLeadSource,
            adapterSalutation;

    private EditText edittextAssignedToName, edittextAccountName,
            edittextFirstname, edittextLastname,
            edittextEmail, edittextPhone, edittextMobile, edittextNotes,
            edittextMilAddress,
            edittextMilCity, edittextMilState, edittextMilCountry, edittextMilPostal;

    private TextView edittextBirthDate;

    private CheckBox checkboxIsDoNotCall, checkboxIsActive;
    private ImageButton buttonMilAddress;

    private ImageButton buttonAssignedTo;
    private LinearLayout layoutAssignedToAdd;
    private EditText edittextAssignedToAddSearch;
    private PlaceHolderView phvAssignedToAdd;

    private ImageButton buttonAccount;
    private LinearLayout layoutAccountAdd;
    private EditText edittextAccountAddSearch;
    private PlaceHolderView phvAccountAdd;

    private Button buttonSave, buttonCancel;

    public ClickListenContactCreateClose clickClose;
    public interface ClickListenContactCreateClose {
        void callbackContactCreateClose(int back_type);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_contact_create, container, false);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        clickClose = (ClickListenContactCreateClose) getActivity();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            back_type = bundle.getInt("back_type", 0);
        }

        layoutTitle = (RelativeLayout) view.findViewById(R.id.layout_title);
        layoutLeadSource = (RelativeLayout) view.findViewById(R.id.layout_lead_source);
        layoutSalutation = (LinearLayout) view.findViewById(R.id.layout_salutation);

        spinnerTitle = (Spinner) view.findViewById(R.id.spinner_title);
        spinnerLeadSource = (Spinner) view.findViewById(R.id.spinner_lead_source);
        spinnerSalutation = (Spinner) view.findViewById(R.id.spinner_salutation);

        edittextAssignedToName = (EditText) view.findViewById(R.id.edittext_assigned_to_name);
        edittextAccountName = (EditText) view.findViewById(R.id.edittext_account_name);
        edittextFirstname = (EditText) view.findViewById(R.id.edittext_firstname);
        edittextLastname = (EditText) view.findViewById(R.id.edittext_lastname);
        edittextBirthDate = (TextView) view.findViewById(R.id.edittext_birth_date);
        edittextEmail = (EditText) view.findViewById(R.id.edittext_email);
        edittextPhone = (EditText) view.findViewById(R.id.edittext_phone);
        edittextMobile = (EditText) view.findViewById(R.id.edittext_mobile);
        edittextNotes = (EditText) view.findViewById(R.id.edittext_notes);
        buttonMilAddress = (ImageButton) view.findViewById(R.id.button_mil_address);
        edittextMilAddress = (EditText) view.findViewById(R.id.edittext_mil_address);
        edittextMilCity = (EditText) view.findViewById(R.id.edittext_mil_city);
        edittextMilState = (EditText) view.findViewById(R.id.edittext_mil_state);
        edittextMilCountry = (EditText) view.findViewById(R.id.edittext_mil_country);
        edittextMilPostal = (EditText) view.findViewById(R.id.edittext_mil_postal);

        checkboxIsDoNotCall = (CheckBox) view.findViewById(R.id.checkbox_is_do_not_call);
        checkboxIsActive = (CheckBox) view.findViewById(R.id.checkbox_is_active);

        buttonAssignedTo = (ImageButton) view.findViewById(R.id.button_assigned_to);
        layoutAssignedToAdd = (LinearLayout) view.findViewById(R.id.layout_assigned_to_add);
        edittextAssignedToAddSearch = (EditText) view.findViewById(R.id.edittext_assigned_to_add_search);
        phvAssignedToAdd = (PlaceHolderView) view.findViewById(R.id.phv_assigned_to_add);

        buttonAccount = (ImageButton) view.findViewById(R.id.button_account);
        layoutAccountAdd = (LinearLayout) view.findViewById(R.id.layout_account_add);
        edittextAccountAddSearch = (EditText) view.findViewById(R.id.edittext_account_add_search);
        phvAccountAdd = (PlaceHolderView) view.findViewById(R.id.phv_account_add);

        buttonSave = (Button) view.findViewById(R.id.button_save);
        buttonCancel = (Button) view.findViewById(R.id.button_cancel);

        checkboxIsDoNotCall.setChecked(false);
        checkboxIsActive.setChecked(true);

        strMilLat = strLat;
        strMilLng = strLng;
        strMilAddress = "";
        strMilCity = "";
        strMilState = "";
        strMilCountry = "";
        strMilPostal = "";
        strBirthDate = "";

        listAssignedTo = new ArrayList<>();
        listAccount = new ArrayList<>();
        listTitle = new ArrayList<>();
        listLeadSource = new ArrayList<>();
        listSalutation = new ArrayList<>();
        adapterTitle = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listTitle);
        adapterTitle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterLeadSource = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listLeadSource);
        adapterLeadSource.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterSalutation = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listSalutation);
        adapterSalutation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        getAssignedTo();
        getAccount();
        getTitle();
        getLeadSource();
        getSalutation();

        edittextBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                        isBirthDayClicked = true;
                        edittextBirthDate.setText(selectDate(d, m, y));
                        strBirthDate = selectDateEdit(d, m, y);
                        Log.d("TAG_BirthDate", strBirthDate);
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        buttonMilAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!checkPermission()) {
                        requestPermission();
                    } else {
                        isBilClicked = true;
                        double lat = Double.valueOf(strLat);
                        double lng = Double.valueOf(strLng);
                        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                                .withLocation(lat, lng)
                                .withGeolocApiKey(getString(R.string.google_maps_key))
                                .withGooglePlacesEnabled()
                                .withSatelliteViewHidden()
                                .withVoiceSearchHidden()
                                .shouldReturnOkOnBackPressed()
                                .build(getActivity());
                        startActivityForResult(locationPickerIntent, 99);
                    }
                } else {
                    isBilClicked = true;
                    double lat = Double.valueOf(strLat);
                    double lng = Double.valueOf(strLng);
                    Intent locationPickerIntent = new LocationPickerActivity.Builder()
                            .withLocation(lat, lng)
                            .withGeolocApiKey(getString(R.string.google_maps_key))
                            .withGooglePlacesEnabled()
                            .withSatelliteViewHidden()
                            .withVoiceSearchHidden()
                            .shouldReturnOkOnBackPressed()
                            .build(getActivity());
                    startActivityForResult(locationPickerIntent, 99);
                }
            }
        });

        buttonAssignedTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutAssignedToAdd.setVisibility(View.VISIBLE);
                listAssignedTo.clear();
                getAssignedTo();
            }
        });

        edittextAssignedToAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvAssignedToAdd.removeAllViews();
                    ArrayList<ModelSpinner> modle = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listAssignedTo) {
                        if (m.getName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvAssignedToAdd
                                .addView(new AdapterAssignedTo(getActivity(), FragmentContactCreate.this, i, modle.get(i)));
                    }
                } else {
                    phvAssignedToAdd.removeAllViews();
                    for (int i = 0; i < listAssignedTo.size(); i++) {
                        phvAssignedToAdd
                                .addView(new AdapterAssignedTo(getActivity(), FragmentContactCreate.this, i, listAssignedTo.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        buttonAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutAccountAdd.setVisibility(View.VISIBLE);
                listAccount.clear();
                getAccount();
            }
        });

        edittextAccountAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvAccountAdd.removeAllViews();
                    ArrayList<ModelSpinner> modle = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listAccount) {
                        if (m.getName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvAccountAdd
                                .addView(new AdapterAccount(getActivity(), FragmentContactCreate.this, i, modle.get(i)));
                    }
                } else {
                    phvAccountAdd.removeAllViews();
                    for (int i = 0; i < listAccount.size(); i++) {
                        phvAccountAdd
                                .addView(new AdapterAccount(getActivity(), FragmentContactCreate.this, i, listAccount.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String assignedTo = strAssignedToId;
                String account = strAccountId;
                String salutation = listSalutation.get(spinnerSalutation.getSelectedItemPosition()).getName();
                String firstName = edittextFirstname.getText().toString().trim();
                String lastName = edittextLastname.getText().toString().trim();
                String title = listTitle.get(spinnerTitle.getSelectedItemPosition()).getName();
                String leadSource = listLeadSource.get(spinnerLeadSource.getSelectedItemPosition()).getId();
                String birthDate = "";
                if (isBirthDayClicked) {
                    birthDate = strBirthDate;
                }
                String email = edittextEmail.getText().toString().trim();
                String phone = edittextPhone.getText().toString().trim();
                String mobile = edittextMobile.getText().toString().trim();
                String notes = edittextNotes.getText().toString().trim();
                String isDoNotCall = null;
                if (checkboxIsDoNotCall.isChecked()) {
                    isDoNotCall = "1";
                } else {
                    isDoNotCall = "0";
                }
                String isActive = null;
                if (checkboxIsActive.isChecked()) {
                    isActive = "1";
                } else {
                    isActive = "0";
                }
                String milLat = strLat;
                String milLng = strLng;
                if (isBilClicked) {
                    milLat = strMilLat;
                    milLng = strMilLng;
                }
                String milAddress = edittextMilAddress.getText().toString().trim();
                String milCity = edittextMilCity.getText().toString().trim();
                String milState = edittextMilState.getText().toString().trim();
                String milCountry = edittextMilCountry.getText().toString().trim();
                String milPostal = edittextMilPostal.getText().toString().trim();

                if (TextUtils.isEmpty(assignedTo)) {
                    Utl.showToast(getActivity(), "Select AssignedTo");
                } else if (TextUtils.isEmpty(account)) {
                    Utl.showToast(getActivity(), "Select Account");
                } else if (TextUtils.isEmpty(firstName)) {
                    Utl.showToast(getActivity(), "Enter First Name");
                } else if (TextUtils.isEmpty(lastName)) {
                    Utl.showToast(getActivity(), "Enter Last Name");
                } else if (TextUtils.isEmpty(milAddress)) {
                    Utl.showToast(getActivity(), "Enter Mailing Address");
                } else if (TextUtils.isEmpty(milCity)) {
                    Utl.showToast(getActivity(), "Enter Mailing City");
                } else if (TextUtils.isEmpty(milState)) {
                    Utl.showToast(getActivity(), "Enter Mailing State");
                } else if (TextUtils.isEmpty(milCountry)) {
                    Utl.showToast(getActivity(), "Enter Mailing Country");
                } else if (TextUtils.isEmpty(milPostal)) {
                    Utl.showToast(getActivity(), "Enter Mailing Postal Code");
                } else {
                    createContact(assignedTo,
                            account,
                            salutation,
                            firstName,
                            lastName,
                            title,
                            leadSource,
                            birthDate,
                            email,
                            phone,
                            mobile,
                            notes,
                            isDoNotCall,
                            isActive,
                            milLat, milLng,
                            milAddress, milCity, milState, milCountry, milPostal);
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickClose != null) {
                    clickClose.callbackContactCreateClose(back_type);
                }
            }
        });

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(provider);
        if (location != null) {
            Log.d("Provider: ", provider + " has been selected.");
            onLocationChanged(location);
        }

        return view;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99) {
            if (data != null) {
                double latitude = data.getDoubleExtra("latitude", 0.0);
                strMilLat = String.valueOf(latitude);
                double longitude = data.getDoubleExtra("longitude", 0.0);
                strMilLng = String.valueOf(longitude);
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> addresses = new ArrayList<>();
                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                strMilAddress = addresses.get(0).getSubThoroughfare() + ", " + addresses.get(0).getThoroughfare();
                if (addresses.get(0).getLocality() != null) {
                    strMilCity = addresses.get(0).getLocality();
                } else {
                    strMilCity = addresses.get(0).getSubLocality();
                }
                if (addresses.get(0).getAdminArea() != null) {
                    strMilState = addresses.get(0).getAdminArea();
                } else {
                    strMilState = addresses.get(0).getSubAdminArea();
                }
                strMilCountry = addresses.get(0).getCountryName();
                strMilPostal = addresses.get(0).getPostalCode();
                Log.d("TAG_LATITUDE****", strMilLat);
                Log.d("TAG_LONGITUDE****", strMilLng);
                Log.d("TAG_ADDRESS****", strMilAddress);
                Log.d("TAG_CITY****", strMilCity);
                Log.d("TAG_STATE****", strMilState);
                Log.d("TAG_COUNTRY****", strMilCountry);
                Log.d("TAG_POSTAL****", strMilPostal);
                edittextMilAddress.setText(strMilAddress);
                edittextMilCity.setText(strMilCity);
                edittextMilState.setText(strMilState);
                edittextMilCountry.setText(strMilCountry);
                edittextMilPostal.setText(strMilPostal);
            } else {
                Log.d("RESULT", "CANCELLED");
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        strLat = String.valueOf(location.getLatitude());
        strLng = String.valueOf(location.getLongitude());
        Log.d("TAG_LAT*****", strLat);
        Log.d("TAG_LNG*****", strLng);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
        Toast.makeText(getActivity(), "Enabled New Provider " + provider, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String s) {
        Toast.makeText(getActivity(), "Disabled New Provider " + provider, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    private void setSpinnerDropDownHeight(List<ModelSpinner> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 70);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    public String selectDate(int d, int m, int y) {
        String time = "" + (m + 1) + "/" + d + "/" + y;
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String selectDateEdit(int d, int m, int y) {
        String time = "" + (m + 1) + "/" + d + "/" + y;
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void getAssignedTo() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getContactAssignedTo(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_AllUser" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAssignedTo.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_UserID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listAssignedTo.add(model);
                            }
                            for (int i = 0; i < listAssignedTo.size(); i++) {
                                phvAssignedToAdd
                                        .addView(new AdapterAssignedTo(getActivity(), FragmentContactCreate.this, i, listAssignedTo.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getAccount() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getContactAccount(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Account" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAccount.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_AccountID));
                                model.setName(dataObject.getString(Cons.KEY_AccountName));
                                listAccount.add(model);
                            }
                            for (int i = 0; i < listAccount.size(); i++) {
                                phvAccountAdd
                                        .addView(new AdapterAccount(getActivity(), FragmentContactCreate.this, i, listAccount.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getTitle() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getContactTitle(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_ContactTitle" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listTitle.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_TitleOfPeopleID));
                                model.setName(dataObject.getString(Cons.KEY_Title));
                                listTitle.add(model);
                            }
                            setSpinnerDropDownHeight(listTitle, spinnerTitle);
                            spinnerTitle.setAdapter(adapterTitle);
                            spinnerTitle.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getLeadSource() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getContactLeadSource(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_ContactLeadSour" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listLeadSource.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_LeadSourceID));
                                model.setName(dataObject.getString(Cons.KEY_LeadSource));
                                listLeadSource.add(model);
                            }
                            setSpinnerDropDownHeight(listLeadSource, spinnerLeadSource);
                            spinnerLeadSource.setAdapter(adapterLeadSource);
                            spinnerLeadSource.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getSalutation() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getContactSalutation(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_ContactSalutati" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAssignedTo.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_SalutationID));
                                model.setName(dataObject.getString(Cons.KEY_Salutation));
                                listSalutation.add(model);
                            }
                            setSpinnerDropDownHeight(listSalutation, spinnerSalutation);
                            spinnerSalutation.setAdapter(adapterSalutation);
                            spinnerSalutation.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void createContact(String assignedTo,
                               String account,
                               String salutation,
                               String firstName,
                               String lastName,
                               String title,
                               String leadSource,
                               String birthDate,
                               String email,
                               String phone,
                               String mobile,
                               String notes,
                               String isDoNotCall,
                               String isActive,
                               String milLat,
                               String milLng,
                               String milAddress,
                               String milCity,
                               String milState,
                               String milCountry,
                               String milPostal) {

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AssignedTo, RequestBody.create(MediaType.parse("text/plain"), assignedTo));
        map.put(Cons.KEY_Account, RequestBody.create(MediaType.parse("text/plain"), account));
        map.put(Cons.KEY_Salutation, RequestBody.create(MediaType.parse("text/plain"), salutation));
        map.put(Cons.KEY_FirstName, RequestBody.create(MediaType.parse("text/plain"), firstName));
        map.put(Cons.KEY_LastName, RequestBody.create(MediaType.parse("text/plain"), lastName));
        map.put(Cons.KEY_Title, RequestBody.create(MediaType.parse("text/plain"), title));
        map.put(Cons.KEY_LeadSource, RequestBody.create(MediaType.parse("text/plain"), leadSource));
        if (!TextUtils.isEmpty(birthDate)) {
            map.put(Cons.KEY_BirthDate, RequestBody.create(MediaType.parse("text/plain"), birthDate));
        }
        if (!TextUtils.isEmpty(email)) {
            map.put(Cons.KEY_Email, RequestBody.create(MediaType.parse("text/plain"), email));
        }
        if (!TextUtils.isEmpty(phone)) {
            map.put(Cons.KEY_PhoneNo, RequestBody.create(MediaType.parse("text/plain"), phone));
        }
        if (!TextUtils.isEmpty(mobile)) {
            map.put(Cons.KEY_MobileNo, RequestBody.create(MediaType.parse("text/plain"), mobile));
        }
        if (!TextUtils.isEmpty(notes)) {
            map.put(Cons.KEY_Notes, RequestBody.create(MediaType.parse("text/plain"), notes));
        }
        map.put(Cons.KEY_DoNotCall, RequestBody.create(MediaType.parse("text/plain"), isDoNotCall));
        map.put(Cons.KEY_IsActive, RequestBody.create(MediaType.parse("text/plain"), isActive));
        map.put(Cons.KEY_MailingLatitude, RequestBody.create(MediaType.parse("text/plain"), milLat));
        map.put(Cons.KEY_MailingLongitude, RequestBody.create(MediaType.parse("text/plain"), milLng));
        map.put(Cons.KEY_MailingAddress, RequestBody.create(MediaType.parse("text/plain"), milAddress));
        map.put(Cons.KEY_MailingCity, RequestBody.create(MediaType.parse("text/plain"), milCity));
        map.put(Cons.KEY_MailingState, RequestBody.create(MediaType.parse("text/plain"), milState));
        map.put(Cons.KEY_MailingCountry, RequestBody.create(MediaType.parse("text/plain"), milCountry));
        map.put(Cons.KEY_MailingPostalCode, RequestBody.create(MediaType.parse("text/plain"), milPostal));
        Call<ResponseBody> response;
        response = apiInterface.createContactDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Contact_Create" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            if (clickClose != null) {
                                clickClose.callbackContactCreateClose(back_type);
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    public boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), CAMERA);
        int result2 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION);
        int result3 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_COARSE_LOCATION);
        int result4 = ContextCompat.checkSelfPermission(getActivity(), CALL_PHONE);
        int result5 = ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE);
        int result6 = ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE);
        return
                result1 == PackageManager.PERMISSION_GRANTED
                        && result2 == PackageManager.PERMISSION_GRANTED
                        && result3 == PackageManager.PERMISSION_GRANTED
                        && result4 == PackageManager.PERMISSION_GRANTED
                        && result5 == PackageManager.PERMISSION_GRANTED
                        && result6 == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{
                        CAMERA,
                        ACCESS_FINE_LOCATION,
                        ACCESS_COARSE_LOCATION,
                        CALL_PHONE,
                        READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean locationFineAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean locationCoarseAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean callAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean readAccepted = grantResults[4] == PackageManager.PERMISSION_GRANTED;
                    boolean writeAccepted = grantResults[5] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted &&
                            locationFineAccepted &&
                            locationCoarseAccepted &&
                            callAccepted &&
                            readAccepted &&
                            writeAccepted)
                        Utl.showToast(getActivity(), "Permission Granted.");
                    else {
                        Utl.showToast(getActivity(), "Permission Denied.");
                    }
                }
                break;
        }
    }

    @Override
    public void callbackAddAssignedToListen(int pos, ModelSpinner model) {
        phvAssignedToAdd.removeAllViews();
        layoutAssignedToAdd.setVisibility(View.GONE);
        strAssignedToId = model.getId();
        strAssignedToName = model.getName();
        edittextAssignedToName.setText(strAssignedToName);
    }

    @Override
    public void callbackAddAccountListen(int pos, ModelSpinner model) {
        phvAccountAdd.removeAllViews();
        layoutAccountAdd.setVisibility(View.GONE);
        strAccountId = model.getId();
        strAccountName = model.getName();
        edittextAccountName.setText(strAccountName);
    }

}