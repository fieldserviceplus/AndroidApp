package com.fieldwise.fragments

import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Criteria
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast

import com.fieldwise.R
import com.fieldwise.adapters.AdapterAccount
import com.fieldwise.adapters.AdapterAssignedTo
import com.fieldwise.models.ModelSpinner
import com.fieldwise.utils.APIClient
import com.fieldwise.utils.APIInterface
import com.fieldwise.utils.Cons
import com.fieldwise.utils.Utl
import com.mindorks.placeholderview.PlaceHolderView
import com.schibstedspain.leku.LocationPickerActivity

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

import java.io.IOException
import java.lang.reflect.Field
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Calendar
import java.util.Date
import java.util.HashMap
import java.util.Locale

import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.Manifest.permission.CALL_PHONE

class FragmentContactCreateKT : Fragment(), LocationListener, AdapterAssignedTo.AddAssignedToClickListen, AdapterAccount.AddAccountClickListen {

    var apiInterface = APIClient.getClient().create(APIInterface::class.java)

    internal var back_type: Int = 0

    private var strLat = "40.730610"
    private var strLng = "-73.935242"
    private var provider: String? = null
    private var locationManager: LocationManager? = null

    private var strAssignedToId: String? = null
    private var strAssignedToName: String? = null
    private var strAccountId: String? = null
    private var strAccountName: String? = null
    private var strBirthDate: String? = null
    private var strMilLat: String? = null
    private var strMilLng: String? = null
    private var strMilAddress: String? = null
    private var strMilCity: String? = null
    private var strMilState: String? = null
    private var strMilCountry: String? = null
    private var strMilPostal: String? = null

    private var isBirthDayClicked = false
    private var isBilClicked = false

    private var layoutTitle: RelativeLayout? = null
    private var layoutLeadSource: RelativeLayout? = null
    private var layoutSalutation: LinearLayout? = null

    private var spinnerTitle: Spinner? = null
    private var spinnerLeadSource: Spinner? = null
    private var spinnerSalutation: Spinner? = null

    private var listAssignedTo: MutableList<ModelSpinner>? = null
    private var listAccount: MutableList<ModelSpinner>? = null
    private var listTitle: MutableList<ModelSpinner>? = null
    private var listLeadSource: MutableList<ModelSpinner>? = null
    private var listSalutation: MutableList<ModelSpinner>? = null

    private var adapterTitle: ArrayAdapter<ModelSpinner>? = null
    private var adapterLeadSource: ArrayAdapter<ModelSpinner>? = null
    private var adapterSalutation: ArrayAdapter<ModelSpinner>? = null

    private var edittextAssignedToName: EditText? = null
    private var edittextAccountName: EditText? = null
    private var edittextFirstname: EditText? = null
    private var edittextLastname: EditText? = null
    private var edittextEmail: EditText? = null
    private var edittextPhone: EditText? = null
    private var edittextMobile: EditText? = null
    private var edittextNotes: EditText? = null
    private var edittextMilAddress: EditText? = null
    private var edittextMilCity: EditText? = null
    private var edittextMilState: EditText? = null
    private var edittextMilCountry: EditText? = null
    private var edittextMilPostal: EditText? = null

    private var edittextBirthDate: TextView? = null

    private var checkboxIsDoNotCall: CheckBox? = null
    private var checkboxIsActive: CheckBox? = null
    private var buttonMilAddress: ImageButton? = null

    private var buttonAssignedTo: ImageButton? = null
    private var layoutAssignedToAdd: LinearLayout? = null
    private var edittextAssignedToAddSearch: EditText? = null
    private var phvAssignedToAdd: PlaceHolderView? = null

    private var buttonAccount: ImageButton? = null
    private var layoutAccountAdd: LinearLayout? = null
    private var edittextAccountAddSearch: EditText? = null
    private var phvAccountAdd: PlaceHolderView? = null

    private var buttonSave: Button? = null
    private var buttonCancel: Button? = null

    var clickClose: ClickListenContactCreateClose? = null

    interface ClickListenContactCreateClose {
        fun callbackContactCreateClose(back_type: Int, isSave: Boolean, contactID: String, contactNo: String)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_contact_create, container, false)

        clickClose = activity as ClickListenContactCreateClose

        val bundle = this.arguments
        if (bundle != null) {
            back_type = bundle.getInt("back_type", 0)
        }

        layoutTitle = view.findViewById<View>(R.id.layout_title) as RelativeLayout
        layoutLeadSource = view.findViewById<View>(R.id.layout_lead_source) as RelativeLayout
        layoutSalutation = view.findViewById<View>(R.id.layout_salutation) as LinearLayout

        spinnerTitle = view.findViewById<View>(R.id.spinner_title) as Spinner
        spinnerLeadSource = view.findViewById<View>(R.id.spinner_lead_source) as Spinner
        spinnerSalutation = view.findViewById<View>(R.id.spinner_salutation) as Spinner

        edittextAssignedToName = view.findViewById<View>(R.id.edittext_assigned_to_name) as EditText
        edittextAccountName = view.findViewById<View>(R.id.edittext_account_name) as EditText
        edittextFirstname = view.findViewById<View>(R.id.edittext_firstname) as EditText
        edittextLastname = view.findViewById<View>(R.id.edittext_lastname) as EditText
        edittextBirthDate = view.findViewById<View>(R.id.edittext_birth_date) as TextView
        edittextEmail = view.findViewById<View>(R.id.edittext_email) as EditText
        edittextPhone = view.findViewById<View>(R.id.edittext_phone) as EditText
        edittextMobile = view.findViewById<View>(R.id.edittext_mobile) as EditText
        edittextNotes = view.findViewById<View>(R.id.edittext_notes) as EditText
        buttonMilAddress = view.findViewById<View>(R.id.button_mil_address) as ImageButton
        edittextMilAddress = view.findViewById<View>(R.id.edittext_mil_address) as EditText
        edittextMilCity = view.findViewById<View>(R.id.edittext_mil_city) as EditText
        edittextMilState = view.findViewById<View>(R.id.edittext_mil_state) as EditText
        edittextMilCountry = view.findViewById<View>(R.id.edittext_mil_country) as EditText
        edittextMilPostal = view.findViewById<View>(R.id.edittext_mil_postal) as EditText

        checkboxIsDoNotCall = view.findViewById<View>(R.id.checkbox_is_do_not_call) as CheckBox
        checkboxIsActive = view.findViewById<View>(R.id.checkbox_is_active) as CheckBox

        buttonAssignedTo = view.findViewById<View>(R.id.button_assigned_to) as ImageButton
        layoutAssignedToAdd = view.findViewById<View>(R.id.layout_assigned_to_add) as LinearLayout
        edittextAssignedToAddSearch = view.findViewById<View>(R.id.edittext_assigned_to_add_search) as EditText
        phvAssignedToAdd = view.findViewById<View>(R.id.phv_assigned_to_add) as PlaceHolderView

        buttonAccount = view.findViewById<View>(R.id.button_account) as ImageButton
        layoutAccountAdd = view.findViewById<View>(R.id.layout_account_add) as LinearLayout
        edittextAccountAddSearch = view.findViewById<View>(R.id.edittext_account_add_search) as EditText
        phvAccountAdd = view.findViewById<View>(R.id.phv_account_add) as PlaceHolderView

        buttonSave = view.findViewById<View>(R.id.button_save) as Button
        buttonCancel = view.findViewById<View>(R.id.button_cancel) as Button

        checkboxIsDoNotCall!!.isChecked = false
        checkboxIsActive!!.isChecked = true

        strMilLat = strLat
        strMilLng = strLng
        strMilAddress = ""
        strMilCity = ""
        strMilState = ""
        strMilCountry = ""
        strMilPostal = ""
        strBirthDate = ""

        listAssignedTo = ArrayList()
        listAccount = ArrayList()
        listTitle = ArrayList()
        listLeadSource = ArrayList()
        listSalutation = ArrayList()
        adapterTitle = ArrayAdapter(activity!!, android.R.layout.simple_spinner_dropdown_item, listTitle!!)
        adapterTitle!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        adapterLeadSource = ArrayAdapter(activity!!, android.R.layout.simple_spinner_dropdown_item, listLeadSource!!)
        adapterLeadSource!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        adapterSalutation = ArrayAdapter(activity!!, android.R.layout.simple_spinner_dropdown_item, listSalutation!!)
        adapterSalutation!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        getAssignedTo()
        getAccount()
        getTitle()
        getLeadSource()
        getSalutation()

        edittextBirthDate!!.setOnClickListener {
            val calendar = Calendar.getInstance()
            val day = calendar.get(Calendar.DATE)
            val month = calendar.get(Calendar.MONTH)
            val year = calendar.get(Calendar.YEAR)
            val datePickerDialog = DatePickerDialog(activity!!, DatePickerDialog.OnDateSetListener { datePicker, y, m, d ->
                isBirthDayClicked = true
                edittextBirthDate!!.text = selectDate(d, m, y)
                strBirthDate = selectDateEdit(d, m, y)
                Log.d("TAG_BirthDate", strBirthDate)
            }, year, month, day)
            datePickerDialog.show()
        }

        buttonMilAddress!!.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!checkPermission()) {
                    requestPermission()
                } else {
                    isBilClicked = true
                    val lat = java.lang.Double.valueOf(strLat)!!
                    val lng = java.lang.Double.valueOf(strLng)!!
                    val locationPickerIntent = LocationPickerActivity.Builder()
                            .withLocation(lat, lng)
                            .withGeolocApiKey(getString(R.string.google_maps_key))
                            .withGooglePlacesEnabled()
                            .withSatelliteViewHidden()
                            .withVoiceSearchHidden()
                            .shouldReturnOkOnBackPressed()
                            .build(activity!!)
                    startActivityForResult(locationPickerIntent, 99)
                }
            } else {
                isBilClicked = true
                val lat = java.lang.Double.valueOf(strLat)!!
                val lng = java.lang.Double.valueOf(strLng)!!
                val locationPickerIntent = LocationPickerActivity.Builder()
                        .withLocation(lat, lng)
                        .withGeolocApiKey(getString(R.string.google_maps_key))
                        .withGooglePlacesEnabled()
                        .withSatelliteViewHidden()
                        .withVoiceSearchHidden()
                        .shouldReturnOkOnBackPressed()
                        .build(activity!!)
                startActivityForResult(locationPickerIntent, 99)
            }
        }

        buttonAssignedTo!!.setOnClickListener {
            layoutAssignedToAdd!!.visibility = View.VISIBLE
            listAssignedTo!!.clear()
            getAssignedTo()
        }

        edittextAssignedToAddSearch!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i1: Int, i2: Int, i3: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i1: Int, i2: Int, i3: Int) {
                val search = charSequence.toString()
                if (!TextUtils.isEmpty(search)) {
                    phvAssignedToAdd!!.removeAllViews()
                    val modle = ArrayList<ModelSpinner>()
                    for (m in listAssignedTo!!) {
                        if (m.name.toLowerCase().contains(search)) {
                            modle.add(m)
                        }
                    }
                    for (i in modle.indices) {
                        phvAssignedToAdd!!
                                .addView(AdapterAssignedTo(activity, this@FragmentContactCreateKT, i, modle[i]))
                    }
                } else {
                    phvAssignedToAdd!!.removeAllViews()
                    for (i in listAssignedTo!!.indices) {
                        phvAssignedToAdd!!
                                .addView(AdapterAssignedTo(activity, this@FragmentContactCreateKT, i, listAssignedTo!![i]))
                    }
                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })

        buttonAccount!!.setOnClickListener {
            layoutAccountAdd!!.visibility = View.VISIBLE
            listAccount!!.clear()
            getAccount()
        }

        edittextAccountAddSearch!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i1: Int, i2: Int, i3: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i1: Int, i2: Int, i3: Int) {
                val search = charSequence.toString()
                if (!TextUtils.isEmpty(search)) {
                    phvAccountAdd!!.removeAllViews()
                    val modle = ArrayList<ModelSpinner>()
                    for (m in listAccount!!) {
                        if (m.name.toLowerCase().contains(search)) {
                            modle.add(m)
                        }
                    }
                    for (i in modle.indices) {
                        phvAccountAdd!!
                                .addView(AdapterAccount(activity, this@FragmentContactCreateKT, i, modle[i]))
                    }
                } else {
                    phvAccountAdd!!.removeAllViews()
                    for (i in listAccount!!.indices) {
                        phvAccountAdd!!
                                .addView(AdapterAccount(activity, this@FragmentContactCreateKT, i, listAccount!![i]))
                    }
                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })

        buttonSave!!.setOnClickListener {
            val assignedTo = strAssignedToId
            val account = strAccountId
            val salutation = listSalutation!![spinnerSalutation!!.selectedItemPosition].name
            val firstName = edittextFirstname!!.text.toString().trim { it <= ' ' }
            val lastName = edittextLastname!!.text.toString().trim { it <= ' ' }
            val title = listTitle!![spinnerTitle!!.selectedItemPosition].name
            val leadSource = listLeadSource!![spinnerLeadSource!!.selectedItemPosition].id
            var birthDate: String? = ""
            if (isBirthDayClicked) {
                birthDate = strBirthDate
            }
            val email = edittextEmail!!.text.toString().trim { it <= ' ' }
            val phone = edittextPhone!!.text.toString().trim { it <= ' ' }
            val mobile = edittextMobile!!.text.toString().trim { it <= ' ' }
            val notes = edittextNotes!!.text.toString().trim { it <= ' ' }
            var isDoNotCall: String? = null
            if (checkboxIsDoNotCall!!.isChecked) {
                isDoNotCall = "1"
            } else {
                isDoNotCall = "0"
            }
            var isActive: String? = null
            if (checkboxIsActive!!.isChecked) {
                isActive = "1"
            } else {
                isActive = "0"
            }
            var milLat: String? = strLat
            var milLng: String? = strLng
            if (isBilClicked) {
                milLat = strMilLat
                milLng = strMilLng
            }
            val milAddress = edittextMilAddress!!.text.toString().trim { it <= ' ' }
            val milCity = edittextMilCity!!.text.toString().trim { it <= ' ' }
            val milState = edittextMilState!!.text.toString().trim { it <= ' ' }
            val milCountry = edittextMilCountry!!.text.toString().trim { it <= ' ' }
            val milPostal = edittextMilPostal!!.text.toString().trim { it <= ' ' }

            if (TextUtils.isEmpty(assignedTo)) {
                Utl.showToast(activity, "Select AssignedTo")
            } else if (TextUtils.isEmpty(account)) {
                Utl.showToast(activity, "Select Account")
            } else if (TextUtils.isEmpty(firstName)) {
                Utl.showToast(activity, "Enter First Name")
            } else if (TextUtils.isEmpty(lastName)) {
                Utl.showToast(activity, "Enter Last Name")
            } else if (TextUtils.isEmpty(milAddress)) {
                Utl.showToast(activity, "Enter Mailing Address")
            } else if (TextUtils.isEmpty(milCity)) {
                Utl.showToast(activity, "Enter Mailing City")
            } else if (TextUtils.isEmpty(milState)) {
                Utl.showToast(activity, "Enter Mailing State")
            } else if (TextUtils.isEmpty(milCountry)) {
                Utl.showToast(activity, "Enter Mailing Country")
            } else if (TextUtils.isEmpty(milPostal)) {
                Utl.showToast(activity, "Enter Mailing Postal Code")
            } else {
                createContact(assignedTo,
                        account,
                        salutation,
                        firstName,
                        lastName,
                        title,
                        leadSource,
                        birthDate,
                        email,
                        phone,
                        mobile,
                        notes,
                        isDoNotCall,
                        isActive,
                        milLat, milLng,
                        milAddress, milCity, milState, milCountry, milPostal)
            }
        }

        buttonCancel!!.setOnClickListener {
            if (clickClose != null) {
                clickClose!!.callbackContactCreateClose(back_type, false, "", "")
            }
        }

        locationManager = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val criteria = Criteria()
        provider = locationManager!!.getBestProvider(criteria, false)
        val location = locationManager!!.getLastKnownLocation(provider)
        if (location != null) {
            Log.d("Provider: ", provider!! + " has been selected.")
            onLocationChanged(location)
        }

        return view

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 99) {
            if (data != null) {
                val latitude = data.getDoubleExtra("latitude", 0.0)
                strMilLat = latitude.toString()
                val longitude = data.getDoubleExtra("longitude", 0.0)
                strMilLng = longitude.toString()
                val geocoder = Geocoder(activity, Locale.getDefault())
                var addresses: List<Address> = ArrayList()
                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1)
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                strMilAddress = addresses[0].subThoroughfare + ", " + addresses[0].thoroughfare
                if (addresses[0].locality != null) {
                    strMilCity = addresses[0].locality
                } else {
                    strMilCity = addresses[0].subLocality
                }
                if (addresses[0].adminArea != null) {
                    strMilState = addresses[0].adminArea
                } else {
                    strMilState = addresses[0].subAdminArea
                }
                strMilCountry = addresses[0].countryName
                strMilPostal = addresses[0].postalCode
                Log.d("TAG_LATITUDE****", strMilLat)
                Log.d("TAG_LONGITUDE****", strMilLng)
                Log.d("TAG_ADDRESS****", strMilAddress)
                Log.d("TAG_CITY****", strMilCity)
                Log.d("TAG_STATE****", strMilState)
                Log.d("TAG_COUNTRY****", strMilCountry)
                Log.d("TAG_POSTAL****", strMilPostal)
                edittextMilAddress!!.setText(strMilAddress)
                edittextMilCity!!.setText(strMilCity)
                edittextMilState!!.setText(strMilState)
                edittextMilCountry!!.setText(strMilCountry)
                edittextMilPostal!!.setText(strMilPostal)
            } else {
                Log.d("RESULT", "CANCELLED")
            }
        }
    }

    override fun onLocationChanged(location: Location) {
        strLat = location.latitude.toString()
        strLng = location.longitude.toString()
        Log.d("TAG_LAT*****", strLat)
        Log.d("TAG_LNG*****", strLng)
    }

    override fun onStatusChanged(s: String, i: Int, bundle: Bundle) {}

    override fun onProviderEnabled(s: String) {
        Toast.makeText(activity, "Enabled New Provider " + provider!!, Toast.LENGTH_SHORT).show()
    }

    override fun onProviderDisabled(s: String) {
        Toast.makeText(activity, "Disabled New Provider " + provider!!, Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()
        locationManager!!.requestLocationUpdates(provider, 400, 1f, this)
    }

    override fun onPause() {
        super.onPause()
        locationManager!!.removeUpdates(this)
    }

    private fun setSpinnerDropDownHeight(list: List<ModelSpinner>, spinner: Spinner) {
        val displayMetrics = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        spinner.dropDownWidth = width - 70
        if (list.size > 4) {
            try {
                val popup = Spinner::class.java.getDeclaredField("mPopup")
                popup.isAccessible = true
                val popupWindow = popup.get(spinner) as android.widget.ListPopupWindow
                popupWindow.height = 800
            } catch (e: NoClassDefFoundError) {
            } catch (e: ClassCastException) {
            } catch (e: NoSuchFieldException) {
            } catch (e: IllegalAccessException) {
            }

        }
    }

    fun selectDate(d: Int, m: Int, y: Int): String? {
        val time = "" + (m + 1) + "/" + d + "/" + y
        val inputPattern = "MM/dd/yyyy"
        val outputPattern = "dd MMM yyyy"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return str
    }

    fun selectDateEdit(d: Int, m: Int, y: Int): String? {
        val time = "" + (m + 1) + "/" + d + "/" + y
        val inputPattern = "MM/dd/yyyy"
        val outputPattern = "dd MMM yyyy"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return str
    }

    private fun getAssignedTo() {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        val response = apiInterface.getContactAssignedTo(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_AllUser" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            listAssignedTo!!.clear()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val model = ModelSpinner()
                                model.id = dataObject.getString(Cons.KEY_UserID)
                                model.name = dataObject.getString(Cons.KEY_FullName)
                                listAssignedTo!!.add(model)
                            }
                            for (i in listAssignedTo!!.indices) {
                                phvAssignedToAdd!!
                                        .addView(AdapterAssignedTo(activity, this@FragmentContactCreateKT, i, listAssignedTo!![i]))
                            }
                        }
                    } catch (e: JSONException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getAccount() {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        val response = apiInterface.getContactAccount(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_Account" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            listAccount!!.clear()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val model = ModelSpinner()
                                model.id = dataObject.getString(Cons.KEY_AccountID)
                                model.name = dataObject.getString(Cons.KEY_AccountName)
                                listAccount!!.add(model)
                            }
                            for (i in listAccount!!.indices) {
                                phvAccountAdd!!
                                        .addView(AdapterAccount(activity, this@FragmentContactCreateKT, i, listAccount!![i]))
                            }
                        }
                    } catch (e: JSONException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getTitle() {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        val response = apiInterface.getContactTitle(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_ContactTitle" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            listTitle!!.clear()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val model = ModelSpinner()
                                model.id = dataObject.getString(Cons.KEY_TitleOfPeopleID)
                                model.name = dataObject.getString(Cons.KEY_Title)
                                listTitle!!.add(model)
                            }
                            setSpinnerDropDownHeight(listTitle!!, spinnerTitle!!)
                            spinnerTitle!!.adapter = adapterTitle
                            spinnerTitle!!.setSelection(0)
                        }
                    } catch (e: JSONException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getLeadSource() {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        val response = apiInterface.getContactLeadSource(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_ContactLeadSour" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            listLeadSource!!.clear()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val model = ModelSpinner()
                                model.id = dataObject.getString(Cons.KEY_LeadSourceID)
                                model.name = dataObject.getString(Cons.KEY_LeadSource)
                                listLeadSource!!.add(model)
                            }
                            setSpinnerDropDownHeight(listLeadSource!!, spinnerLeadSource!!)
                            spinnerLeadSource!!.adapter = adapterLeadSource
                            spinnerLeadSource!!.setSelection(0)
                        }
                    } catch (e: JSONException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getSalutation() {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        val response = apiInterface.getContactSalutation(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_ContactSalutati" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            listAssignedTo!!.clear()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val model = ModelSpinner()
                                model.id = dataObject.getString(Cons.KEY_SalutationID)
                                model.name = dataObject.getString(Cons.KEY_Salutation)
                                listSalutation!!.add(model)
                            }
                            setSpinnerDropDownHeight(listSalutation!!, spinnerSalutation!!)
                            spinnerSalutation!!.adapter = adapterSalutation
                            spinnerSalutation!!.setSelection(0)
                        }
                    } catch (e: JSONException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun createContact(assignedTo: String?,
                              account: String?,
                              salutation: String,
                              firstName: String,
                              lastName: String,
                              title: String,
                              leadSource: String,
                              birthDate: String?,
                              email: String,
                              phone: String,
                              mobile: String,
                              notes: String,
                              isDoNotCall: String?,
                              isActive: String?,
                              milLat: String?,
                              milLng: String?,
                              milAddress: String,
                              milCity: String,
                              milState: String,
                              milCountry: String,
                              milPostal: String) {

        val dialog = ProgressDialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setMessage(getString(R.string.please_wait))
        dialog.show()
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_AssignedTo] = RequestBody.create(MediaType.parse("text/plain"), assignedTo!!)
        map[Cons.KEY_Account] = RequestBody.create(MediaType.parse("text/plain"), account!!)
        map[Cons.KEY_Salutation] = RequestBody.create(MediaType.parse("text/plain"), salutation)
        map[Cons.KEY_FirstName] = RequestBody.create(MediaType.parse("text/plain"), firstName)
        map[Cons.KEY_LastName] = RequestBody.create(MediaType.parse("text/plain"), lastName)
        map[Cons.KEY_Title] = RequestBody.create(MediaType.parse("text/plain"), title)
        map[Cons.KEY_LeadSource] = RequestBody.create(MediaType.parse("text/plain"), leadSource)
        if (!TextUtils.isEmpty(birthDate)) {
            map[Cons.KEY_BirthDate] = RequestBody.create(MediaType.parse("text/plain"), birthDate!!)
        }
        if (!TextUtils.isEmpty(email)) {
            map[Cons.KEY_Email] = RequestBody.create(MediaType.parse("text/plain"), email)
        }
        if (!TextUtils.isEmpty(phone)) {
            map[Cons.KEY_PhoneNo] = RequestBody.create(MediaType.parse("text/plain"), phone)
        }
        if (!TextUtils.isEmpty(mobile)) {
            map[Cons.KEY_MobileNo] = RequestBody.create(MediaType.parse("text/plain"), mobile)
        }
        if (!TextUtils.isEmpty(notes)) {
            map[Cons.KEY_Notes] = RequestBody.create(MediaType.parse("text/plain"), notes)
        }
        map[Cons.KEY_DoNotCall] = RequestBody.create(MediaType.parse("text/plain"), isDoNotCall!!)
        map[Cons.KEY_IsActive] = RequestBody.create(MediaType.parse("text/plain"), isActive!!)
        map[Cons.KEY_MailingLatitude] = RequestBody.create(MediaType.parse("text/plain"), milLat!!)
        map[Cons.KEY_MailingLongitude] = RequestBody.create(MediaType.parse("text/plain"), milLng!!)
        map[Cons.KEY_MailingAddress] = RequestBody.create(MediaType.parse("text/plain"), milAddress)
        map[Cons.KEY_MailingCity] = RequestBody.create(MediaType.parse("text/plain"), milCity)
        map[Cons.KEY_MailingState] = RequestBody.create(MediaType.parse("text/plain"), milState)
        map[Cons.KEY_MailingCountry] = RequestBody.create(MediaType.parse("text/plain"), milCountry)
        map[Cons.KEY_MailingPostalCode] = RequestBody.create(MediaType.parse("text/plain"), milPostal)
        val response: Call<ResponseBody>
        response = apiInterface.createContactDetailed(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (dialog.isShowing) {
                    dialog.dismiss()
                }
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_Contact_Create" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            Utl.showToast(activity, strMessage)
                            if (clickClose != null) {
                                val contactID = `object`.getString(Cons.KEY_ContactID)
                                val contactNo = `object`.getString(Cons.KEY_ContactNo)
                                clickClose!!.callbackContactCreateClose(back_type, true, contactID, contactNo)
                            }
                        }
                    } catch (e: JSONException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (dialog.isShowing) {
                    dialog.dismiss()
                }
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(activity!!, ACCESS_FINE_LOCATION)
        val result1 = ContextCompat.checkSelfPermission(activity!!, ACCESS_COARSE_LOCATION)
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED
    }


    fun requestPermission() {
        ActivityCompat.requestPermissions(activity!!, arrayOf(ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION), PERMISSION_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0) {
                val locationFineAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED
                val locationCoarseAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED
                if (locationFineAccepted && locationCoarseAccepted)
                    Utl.showToast(activity, "Permission Granted.");
                else {
                    Utl.showToast(activity, "Permission Denied.");
                }
            }
        }
    }

    override fun callbackAddAssignedToListen(pos: Int, model: ModelSpinner) {
        phvAssignedToAdd!!.removeAllViews()
        layoutAssignedToAdd!!.visibility = View.GONE
        strAssignedToId = model.id
        strAssignedToName = model.name
        edittextAssignedToName!!.setText(strAssignedToName)
    }

    override fun callbackAddAccountListen(pos: Int, model: ModelSpinner) {
        phvAccountAdd!!.removeAllViews()
        layoutAccountAdd!!.visibility = View.GONE
        strAccountId = model.id
        strAccountName = model.name
        edittextAccountName!!.setText(strAccountName)
    }

    companion object {

        private val PERMISSION_REQUEST_CODE = 200
    }

}