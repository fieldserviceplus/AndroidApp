package com.fieldwise.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.activities.ActivityMain;
import com.fieldwise.adapters.AdapterAccountAddItem;
import com.fieldwise.adapters.AdapterAccountRelatedChild;
import com.fieldwise.adapters.AdapterAccountRelatedParent;
import com.fieldwise.adapters.AdapterAccountViewAll;
import com.fieldwise.models.ModelContact;
import com.fieldwise.models.ModelEstimate;
import com.fieldwise.models.ModelEvent;
import com.fieldwise.models.ModelFile;
import com.fieldwise.models.ModelInvoice;
import com.fieldwise.models.ModelSpinner;
import com.fieldwise.models.ModelTask;
import com.fieldwise.models.ModelWorkOrder;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;
import com.mindorks.placeholderview.ExpandablePlaceHolderView;
import com.schibstedspain.leku.LocationPickerActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class FragmentAccountInfo extends Fragment implements
        AdapterAccountViewAll.ViewAllListen,
        AdapterAccountAddItem.AddItemListen,
        AdapterAccountRelatedChild.ClickListen {

    private static final int PERMISSION_REQUEST_CODE = 200;

    private RadioGroup radioGroupMain;
    private RadioButton radioButtonDetails, radioButtonRelated;

    private ScrollView layoutRecentAccountsDetails;
    private ExpandablePlaceHolderView phvRecentAccountsRelated;

    public APIInterface apiInterface;

    private String accountId;

    private String strAssignedTo, strAssignedToName,
            strAccountId, strAccountName, strAccountNo,
            strPrimaryContact, strPrimaryContactName,
            strPhoneNo,
            strPreferredTechnician, strPreferredTechnicianName,
            strAccessNotes,
            strNotes,
            strAccountType, strAccountTypeName,
            strLastActivityDate, strNewLastActivityDate,
            strLastServiceDate, strNewLastServiceDate,
            strPopupReminder,
            strBilLat, strBilLng, strBilAddress, strBilCity, strBilState, strBilCountry, strBilPostal,
            strShpLat, strShpLng, strShpAddress, strShpCity, strShpState, strShpCountry, strShpPostal,
            strIsActive,
            strNewBilLat, strNewBilLng, strNewShpLat, strNewShpLng,
            strNewLat, strNewLng, strNewAddress, strNewCity, strNewState, strNewCountry, strNewPostal,
            strCreatedDate, strLastModifiedDate, strCreatedBy, strLastModifiedBy;

    private boolean isCloneMode = false;
    private boolean isBilClicked = false;
    private boolean isShpClicked = false;
    private boolean isActivityClicked = false;
    private boolean isServiceClicked = false;

    private RelativeLayout layoutAssignedTo,
            layoutPrimaryContact,
            layoutPreferredTechnician,
            layoutAccountType;
    private LinearLayout layoutHeader, layoutSystemInfo;

    private Spinner spinnerAssignedTo,
            spinnerPrimaryContact,
            spinnerPreferredTechnician,
            spinnerAccountType;

    private List<ModelSpinner> listAssignedTo, listPrimaryContact, listPreferredTechnician, listAccountType;
    private ArrayAdapter<ModelSpinner> adapterAssignedTo, adapterPrimaryContact, adapterPreferredTechnician, adapterAccountType;

    private EditText edittextAssignedName,
            edittextPhone,
            edittextAccessNotes,
            edittextNotes,
            edittextPopupReminder,
            edittextBilAddress,
            edittextBilCity, edittextBilState, edittextBilCountry, edittextBilPostal,
            edittextShpAddress,
            edittextShpCity, edittextShpState, edittextShpCountry, edittextShpPostal;

    private TextView textviewHeaderType, textviewHeaderPhone,
            textviewAssignedTo,
            textviewAssignedName,
            textviewPrimaryContactHeader, textviewPrimaryContact,
            textviewPhone,
            textviewPreferredTechnician,
            textviewAccesNotes,
            textviewNotes,
            textviewAccountType,
            edittextLastActivityDate, textviewLastActivityDate,
            edittextLastServiceDate, textviewLastServiceDate,
            textviewPopupReminder,
            textviewBilAddress,
            textviewBilCity, textviewBilState, textviewBilCountry, textviewBilPostal,
            textviewShpAddress,
            textviewShpCity, textviewShpState, textviewShpCountry, textviewShpPostal,
            textviewCreatedDate, textviewCreatedBy, textviewLastModifiedDate, textviewLastModifiedBy;

    private CheckBox checkboxIsActive;
    private ImageButton buttonBilAddress, buttonShpAddress;

    private LinearLayout layoutNavigation, layoutSaveCancel;
    private ImageButton buttonCall, buttonComment, buttonDate, buttonEdit, buttonMore;
    private Button buttonSave, buttonCancel;

    private List<String> listRelatedAccountList;
    private List<ModelContact> listRelatedAccountContacts;
    private List<ModelWorkOrder> listRelatedAccountWorkOrders;
    private List<ModelEstimate> listRelatedAccountEstimates;
    private List<ModelInvoice> listRelatedAccountInvoices;
    private List<ModelFile> listRelatedAccountFiles;
    private List<ModelEvent> listRelatedAccountEvents;
    private List<ModelTask> listRelatedAccountTasks;
    //private List<ModelLocation> listRelatedAccountLocations;
    //private List<ModelChemical> listRelatedAccountChemicals;
    //private List<ModelProduct> listRelatedAccountProducts;

    public AccountViewAllListen click;

    public interface AccountViewAllListen {
        void callbackAccountViewAllListen(String str, String accountId);
    }

    public ClickListenAccountRelatedItemWorkOrderOpen clickAccountRelatedItemWorkOrderOpen;

    public interface ClickListenAccountRelatedItemWorkOrderOpen {
        void callbackAccountRelatedWorkOrderOpen(String workOrderID, String workOrderNo);
    }

    public CreateFileListener clickCrateFile;

    public interface CreateFileListener {
        void callbackAccountCreateFile(String strObject);
    }

    public CreateEventListener clickCrateEvent;

    public interface CreateEventListener {
        void callbackAccountCreateEvent(String strWhoName, String strWhatID, String strWhatName);
    }

    public GoToRecent clickGoToRecent;

    public interface GoToRecent {
        void callbackGoToRecentAccount(String strObject);
    }

    //Create Interface
    public ClickListenContactCreate clickContactCreate;

    public interface ClickListenContactCreate {
        void callbackAccountContactCreate();
    }

    public ClickListenWorkOrderCreate clickWorkOrderCreate;

    public interface ClickListenWorkOrderCreate {
        void callbackAccountWorkOrderCreate();
    }

    public ClickListenEstimateCreate clickEstimateCreate;

    public interface ClickListenEstimateCreate {
        void callbackAccountEstimateCreate();
    }

    public ClickListenInvoiceCreate clickInvoiceCreate;

    public interface ClickListenInvoiceCreate {
        void callbackAccountInvoiceCreate();
    }

    public ClickListenTaskCreate clickTaskCreate;

    public interface ClickListenTaskCreate {
        void callbackAccountTaskCreate();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_account_detailed, container, false);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        click = (AccountViewAllListen) getActivity();
        clickAccountRelatedItemWorkOrderOpen = (ClickListenAccountRelatedItemWorkOrderOpen) getActivity();
        clickCrateFile = (CreateFileListener) getActivity();
        clickCrateEvent = (CreateEventListener) getActivity();
        clickGoToRecent = (GoToRecent) getActivity();

        clickContactCreate = (ClickListenContactCreate) getActivity();
        clickWorkOrderCreate = (ClickListenWorkOrderCreate) getActivity();
        clickEstimateCreate = (ClickListenEstimateCreate) getActivity();
        clickInvoiceCreate = (ClickListenInvoiceCreate) getActivity();
        clickTaskCreate = (ClickListenTaskCreate) getActivity();

        textviewHeaderType = (TextView) view.findViewById(R.id.textview_header_type);
        textviewHeaderPhone = (TextView) view.findViewById(R.id.textview_header_phone);

        radioGroupMain = (RadioGroup) view.findViewById(R.id.radio_group_main);
        radioButtonDetails = (RadioButton) view.findViewById(R.id.radio_button_details);
        radioButtonRelated = (RadioButton) view.findViewById(R.id.radio_button_related);

        layoutRecentAccountsDetails = (ScrollView) view.findViewById(R.id.layout_recent_account_details);
        phvRecentAccountsRelated = (ExpandablePlaceHolderView) view.findViewById(R.id.phv_recent_account_related);

        layoutHeader = (LinearLayout) view.findViewById(R.id.layout_header);
        layoutAssignedTo = (RelativeLayout) view.findViewById(R.id.layout_assigned_to);
        layoutPrimaryContact = (RelativeLayout) view.findViewById(R.id.layout_primary_contact);
        layoutPreferredTechnician = (RelativeLayout) view.findViewById(R.id.layout_preferred_technician);
        layoutAccountType = (RelativeLayout) view.findViewById(R.id.layout_account_type);

        spinnerAssignedTo = (Spinner) view.findViewById(R.id.spinner_assigned_to);
        spinnerPrimaryContact = (Spinner) view.findViewById(R.id.spinner_primary_contact);
        spinnerPreferredTechnician = (Spinner) view.findViewById(R.id.spinner_preferred_technician);
        spinnerAccountType = (Spinner) view.findViewById(R.id.spinner_account_type);

        edittextAssignedName = (EditText) view.findViewById(R.id.edittext_assigned_name);
        edittextPhone = (EditText) view.findViewById(R.id.edittext_phone);
        edittextAccessNotes = (EditText) view.findViewById(R.id.edittext_access_notes);
        edittextNotes = (EditText) view.findViewById(R.id.edittext_notes);
        edittextPopupReminder = (EditText) view.findViewById(R.id.edittext_popup_reminder);
        edittextBilAddress = (EditText) view.findViewById(R.id.edittext_bil_address);
        edittextBilCity = (EditText) view.findViewById(R.id.edittext_bil_city);
        edittextBilState = (EditText) view.findViewById(R.id.edittext_bil_state);
        edittextBilCountry = (EditText) view.findViewById(R.id.edittext_bil_country);
        edittextBilPostal = (EditText) view.findViewById(R.id.edittext_bil_postal);
        edittextShpAddress = (EditText) view.findViewById(R.id.edittext_shp_address);
        edittextShpCity = (EditText) view.findViewById(R.id.edittext_shp_city);
        edittextShpState = (EditText) view.findViewById(R.id.edittext_shp_state);
        edittextShpCountry = (EditText) view.findViewById(R.id.edittext_shp_country);
        edittextShpPostal = (EditText) view.findViewById(R.id.edittext_shp_postal);

        textviewAssignedTo = (TextView) view.findViewById(R.id.textview_assigned_to);
        textviewAssignedName = (TextView) view.findViewById(R.id.textview_assigned_name);
        textviewPrimaryContactHeader = (TextView) view.findViewById(R.id.textview_primary_contact_header);
        textviewPrimaryContact = (TextView) view.findViewById(R.id.textview_primary_contact);
        textviewPhone = (TextView) view.findViewById(R.id.textview_phone);
        textviewPreferredTechnician = (TextView) view.findViewById(R.id.textview_preferred_technician);
        textviewAccesNotes = (TextView) view.findViewById(R.id.textview_access_notes);
        textviewNotes = (TextView) view.findViewById(R.id.textview_notes);
        textviewAccountType = (TextView) view.findViewById(R.id.textview_account_type);
        edittextLastActivityDate = (TextView) view.findViewById(R.id.edittext_last_activity_date);
        edittextLastServiceDate = (TextView) view.findViewById(R.id.edittext_last_service_date);
        textviewLastActivityDate = (TextView) view.findViewById(R.id.textview_last_activity_date);
        textviewLastServiceDate = (TextView) view.findViewById(R.id.textview_last_service_date);
        textviewPopupReminder = (TextView) view.findViewById(R.id.textview_popup_reminder);
        textviewBilAddress = (TextView) view.findViewById(R.id.textview_bil_address);
        textviewBilCity = (TextView) view.findViewById(R.id.textview_bil_city);
        textviewBilState = (TextView) view.findViewById(R.id.textview_bil_state);
        textviewBilCountry = (TextView) view.findViewById(R.id.textview_bil_country);
        textviewBilPostal = (TextView) view.findViewById(R.id.textview_bil_postal);
        textviewShpAddress = (TextView) view.findViewById(R.id.textview_shp_address);
        textviewShpCity = (TextView) view.findViewById(R.id.textview_shp_city);
        textviewShpState = (TextView) view.findViewById(R.id.textview_shp_state);
        textviewShpCountry = (TextView) view.findViewById(R.id.textview_shp_country);
        textviewShpPostal = (TextView) view.findViewById(R.id.textview_shp_postal);

        checkboxIsActive = (CheckBox) view.findViewById(R.id.checkbox_is_active);
        buttonBilAddress = (ImageButton) view.findViewById(R.id.button_bil_address);
        buttonShpAddress = (ImageButton) view.findViewById(R.id.button_shp_address);

        layoutSystemInfo = (LinearLayout) view.findViewById(R.id.layout_system_info);
        textviewCreatedDate = (TextView) view.findViewById(R.id.textview_created_date);
        textviewCreatedBy = (TextView) view.findViewById(R.id.textview_created_by);
        textviewLastModifiedDate = (TextView) view.findViewById(R.id.textview_last_modified_date);
        textviewLastModifiedBy = (TextView) view.findViewById(R.id.textview_last_modified_by);

        layoutNavigation = (LinearLayout) view.findViewById(R.id.layout_navigation);
        buttonCall = (ImageButton) view.findViewById(R.id.button_call);
        buttonComment = (ImageButton) view.findViewById(R.id.button_comment);
        buttonDate = (ImageButton) view.findViewById(R.id.button_date);
        buttonEdit = (ImageButton) view.findViewById(R.id.button_edit);
        buttonMore = (ImageButton) view.findViewById(R.id.button_more);

        layoutSaveCancel = (LinearLayout) view.findViewById(R.id.layout_save_cancel);
        buttonSave = (Button) view.findViewById(R.id.button_save);
        buttonCancel = (Button) view.findViewById(R.id.button_cancel);

        strNewBilLat = "0";
        strNewBilLng = "0";
        strNewShpLat = "0";
        strNewShpLng = "0";
        strNewLat = "0";
        strNewLng = "0";
        strNewAddress = "0";
        strNewCity = "0";
        strNewState = "0";
        strNewCountry = "0";
        strNewPostal = "0";
        strNewLastActivityDate = "0";
        strNewLastServiceDate = "0";

        listAssignedTo = new ArrayList<>();
        listPrimaryContact = new ArrayList<>();
        listPreferredTechnician = new ArrayList<>();
        listAccountType = new ArrayList<>();
        adapterAssignedTo = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listAssignedTo);
        adapterAssignedTo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterPrimaryContact = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listPrimaryContact);
        adapterPrimaryContact.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterPreferredTechnician = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listPreferredTechnician);
        adapterPreferredTechnician.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterAccountType = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listAccountType);
        adapterAccountType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        radioButtonDetails.setChecked(true);
        radioButtonRelated.setChecked(false);
        layoutRecentAccountsDetails.setVisibility(View.VISIBLE);
        phvRecentAccountsRelated.setVisibility(View.GONE);

        editMode(false, false);

        accountId = getArguments().getString(Cons.KEY_AccountID);

        getAccountDetailed(accountId);

        getAllUser();
        getContact(accountId);
        getAccountType();

        listRelatedAccountList = new ArrayList<>();
        listRelatedAccountContacts = new ArrayList<>();
        listRelatedAccountWorkOrders = new ArrayList<>();
        listRelatedAccountEstimates = new ArrayList<>();
        listRelatedAccountInvoices = new ArrayList<>();
        listRelatedAccountFiles = new ArrayList<>();
        listRelatedAccountEvents = new ArrayList<>();
        listRelatedAccountTasks = new ArrayList<>();
        //listRelatedAccountLocations = new ArrayList<>();
        //listRelatedAccountChemicals = new ArrayList<>();
        //listRelatedAccountProducts = new ArrayList<>();
        getRelatedAccountList(accountId);
        getRelatedAccountContacts(accountId);
        getRelatedAccountWorkOrders(accountId);
        getRelatedAccountEstimates(accountId);
        getRelatedAccountInvoices(accountId);
        getRelatedAccountFiles(accountId);
        getRelatedAccountEvents(accountId);
        getRelatedAccountTasks(accountId);
        //getRelatedAccountLocations(accountId);
        //getRelatedAccountChemicals(accountId);
        //getRelatedAccountProducts(accountId);

        radioGroupMain.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radio_button_details) {
                    buttonCall.setEnabled(true);
                    buttonComment.setEnabled(true);
                    buttonDate.setEnabled(true);
                    buttonEdit.setEnabled(true);
                    layoutRecentAccountsDetails.setVisibility(View.VISIBLE);
                    phvRecentAccountsRelated.setVisibility(View.GONE);
                } else if (checkedId == R.id.radio_button_related) {
                    buttonCall.setEnabled(false);
                    buttonComment.setEnabled(false);
                    buttonDate.setEnabled(false);
                    buttonEdit.setEnabled(false);
                    layoutRecentAccountsDetails.setVisibility(View.GONE);
                    phvRecentAccountsRelated.setVisibility(View.VISIBLE);
                    phvRecentAccountsRelated.removeAllViews();

                    for (int i = 0; i < listRelatedAccountList.size(); i++) {
                        phvRecentAccountsRelated
                                .addView(new AdapterAccountRelatedParent(getActivity(), listRelatedAccountList.get(i)));
                    }

                    int lengthContacts;
                    if (listRelatedAccountContacts.size() > 3) {
                        lengthContacts = 3;
                    } else {
                        lengthContacts = listRelatedAccountContacts.size();
                    }
                    for (int i = 0; i < lengthContacts; i++) {
                        phvRecentAccountsRelated
                                .addChildView(0, new AdapterAccountRelatedChild(getActivity(),
                                        "Name: " +
                                                listRelatedAccountContacts.get(i).getFirstName() + " " +
                                                listRelatedAccountContacts.get(i).getLastName() + "\n" +
                                                "Title: " +
                                                listRelatedAccountContacts.get(i).getTitle() + "\n" +
                                                "Phone: " +
                                                listRelatedAccountContacts.get(i).getPhoneNo() + "\n" +
                                                "Email: " +
                                                listRelatedAccountContacts.get(i).getEmail()));
                    }
                    if (listRelatedAccountContacts.size() > 3) {
                        phvRecentAccountsRelated.
                                addChildView(0, new AdapterAccountViewAll(getActivity(), FragmentAccountInfo.this, getString(R.string.contact)));
                    } else if (listRelatedAccountContacts.size() == 0) {
                        phvRecentAccountsRelated.
                                addChildView(0, new AdapterAccountAddItem(getActivity(), FragmentAccountInfo.this, getString(R.string.contact)));
                    }

                    int lengthWorkOrder;
                    if (listRelatedAccountWorkOrders.size() > 3) {
                        lengthWorkOrder = 3;
                    } else {
                        lengthWorkOrder = listRelatedAccountWorkOrders.size();
                    }
                    for (int i = 0; i < lengthWorkOrder; i++) {
                        phvRecentAccountsRelated
                                .addChildView(1, new AdapterAccountRelatedChild(getActivity(), FragmentAccountInfo.this, getString(R.string.work_order),
                                        listRelatedAccountWorkOrders.get(i).getWorkOrderID(), listRelatedAccountWorkOrders.get(i).getWorkOrderNo(),
                                        "Order: " +
                                                listRelatedAccountWorkOrders.get(i).getWorkOrderNo() + "\n" +
                                                "Subject: " +
                                                listRelatedAccountWorkOrders.get(i).getSubject() + "\n" +
                                                "Category: " +
                                                listRelatedAccountWorkOrders.get(i).getCategoryName() + "\n" +
                                                "Priority: " +
                                                listRelatedAccountWorkOrders.get(i).getPriority() + "\n" +
                                                "Status: " +
                                                listRelatedAccountWorkOrders.get(i).getStatus()));
                    }
                    if (listRelatedAccountWorkOrders.size() > 3) {
                        phvRecentAccountsRelated.
                                addChildView(1, new AdapterAccountViewAll(getActivity(), FragmentAccountInfo.this, getString(R.string.work_order)));
                    } else if (listRelatedAccountWorkOrders.size() == 0) {
                        phvRecentAccountsRelated.
                                addChildView(1, new AdapterAccountAddItem(getActivity(), FragmentAccountInfo.this, getString(R.string.work_order)));
                    }

                    int lengthEstimates;
                    if (listRelatedAccountEstimates.size() > 3) {
                        lengthEstimates = 3;
                    } else {
                        lengthEstimates = listRelatedAccountEstimates.size();
                    }
                    for (int i = 0; i < lengthEstimates; i++) {
                        phvRecentAccountsRelated
                                .addChildView(2, new AdapterAccountRelatedChild(getActivity(),
                                        "Owner Name: " +
                                                listRelatedAccountEstimates.get(i).getOwnerName() + "\n" +
                                                "ET#: " +
                                                listRelatedAccountEstimates.get(i).getEstimateNo() + "\n" +
                                                "Estimate Name: " +
                                                listRelatedAccountEstimates.get(i).getEstimateName() + "\n" +
                                                "Status: " +
                                                listRelatedAccountEstimates.get(i).getStatus() + "\n" +
                                                "Grand Total: " +
                                                listRelatedAccountEstimates.get(i).getGrandTotal() + "\n" +
                                                "Created Date: " +
                                                listRelatedAccountEstimates.get(i).getCreatedDate() + "\n" +
                                                "Expiration Date: " +
                                                listRelatedAccountEstimates.get(i).getExpirationDate()));
                    }
                    if (listRelatedAccountEstimates.size() > 3) {
                        phvRecentAccountsRelated.
                                addChildView(2, new AdapterAccountViewAll(getActivity(), FragmentAccountInfo.this, getString(R.string.estimate)));
                    } else if (listRelatedAccountEstimates.size() == 0) {
                        phvRecentAccountsRelated.
                                addChildView(2, new AdapterAccountAddItem(getActivity(), FragmentAccountInfo.this, getString(R.string.estimate)));
                    }

                    int lengthInvoices;
                    if (listRelatedAccountInvoices.size() > 3) {
                        lengthInvoices = 3;
                    } else {
                        lengthInvoices = listRelatedAccountInvoices.size();
                    }
                    for (int i = 0; i < lengthInvoices; i++) {
                        phvRecentAccountsRelated
                                .addChildView(3, new AdapterAccountRelatedChild(getActivity(),
                                        "Invoice Number: " +
                                                listRelatedAccountInvoices.get(i).getInvoiceNumber() + "\n" +
                                                "Invoice Status: " +
                                                listRelatedAccountInvoices.get(i).getInvoiceStatus() + "\n" +
                                                "Subject: " +
                                                listRelatedAccountInvoices.get(i).getSubject() + "\n" +
                                                "Sub Total: " +
                                                listRelatedAccountInvoices.get(i).getSubTotal() + "\n" +
                                                "Total Price: " +
                                                listRelatedAccountInvoices.get(i).getTotalPrice() + "\n" +
                                                "Due Date: " +
                                                listRelatedAccountInvoices.get(i).getDueDate()
                                ));
                    }
                    if (listRelatedAccountInvoices.size() > 3) {
                        phvRecentAccountsRelated.
                                addChildView(3, new AdapterAccountViewAll(getActivity(), FragmentAccountInfo.this, getString(R.string.invoice)));
                    } else if (listRelatedAccountInvoices.size() == 0) {
                        phvRecentAccountsRelated.
                                addChildView(3, new AdapterAccountAddItem(getActivity(), FragmentAccountInfo.this, getString(R.string.invoice)));
                    }

                    int lengthFiles;
                    if (listRelatedAccountFiles.size() > 3) {
                        lengthFiles = 3;
                    } else {
                        lengthFiles = listRelatedAccountFiles.size();
                    }
                    for (int i = 0; i < lengthFiles; i++) {
                        phvRecentAccountsRelated
                                .addChildView(4, new AdapterAccountRelatedChild(getActivity(),
                                        "File Name: " +
                                                listRelatedAccountFiles.get(i).getFileName() + "\n" +
                                                "Subject: " +
                                                listRelatedAccountFiles.get(i).getSubject() + "\n" +
                                                "Content Type: " +
                                                listRelatedAccountFiles.get(i).getContentType() + "\n" +
                                                "Created By: " +
                                                listRelatedAccountFiles.get(i).getCreatedBy() + "\n" +
                                                "Created Date: " +
                                                listRelatedAccountFiles.get(i).getCreatedDate()
                                ));
                    }
                    if (listRelatedAccountFiles.size() > 3) {
                        phvRecentAccountsRelated.
                                addChildView(4, new AdapterAccountViewAll(getActivity(), FragmentAccountInfo.this, getString(R.string.file)));
                    } else if (listRelatedAccountFiles.size() == 0) {
                        phvRecentAccountsRelated.
                                addChildView(4, new AdapterAccountAddItem(getActivity(), FragmentAccountInfo.this, getString(R.string.file)));
                    }

                    int lengthEvents;
                    if (listRelatedAccountEvents.size() > 3) {
                        lengthEvents = 3;
                    } else {
                        lengthEvents = listRelatedAccountEvents.size();
                    }
                    for (int i = 0; i < lengthEvents; i++) {
                        phvRecentAccountsRelated
                                .addChildView(5, new AdapterAccountRelatedChild(getActivity(), FragmentAccountInfo.this, getString(R.string.event),
                                        listRelatedAccountEvents.get(i).getEventID(), listRelatedAccountEvents.get(i).getSubject(),
                                        "Subject: " +
                                                listRelatedAccountEvents.get(i).getSubject() + "\n" +
                                                "Assigned To: " +
                                                listRelatedAccountEvents.get(i).getAssignedTo() + "\n" +
                                                "Event Start Date: " +
                                                listRelatedAccountEvents.get(i).getEventStartDate() + "\n" +
                                                "Event End Date: " +
                                                listRelatedAccountEvents.get(i).getEventEndDate() + "\n" +
                                                "Created By: " +
                                                listRelatedAccountEvents.get(i).getCreatedBy() + "\n" +
                                                "Created Date: " +
                                                listRelatedAccountEvents.get(i).getCreatedDate()));
                    }
                    if (listRelatedAccountEvents.size() > 3) {
                        phvRecentAccountsRelated.
                                addChildView(5, new AdapterAccountViewAll(getActivity(), FragmentAccountInfo.this, getString(R.string.event)));
                    } else if (listRelatedAccountEvents.size() == 0) {
                        phvRecentAccountsRelated.
                                addChildView(5, new AdapterAccountAddItem(getActivity(), FragmentAccountInfo.this, getString(R.string.event)));
                    }

                    int lengthTasks;
                    if (listRelatedAccountTasks.size() > 3) {
                        lengthTasks = 3;
                    } else {
                        lengthTasks = listRelatedAccountTasks.size();
                    }
                    for (int i = 0; i < lengthTasks; i++) {
                        phvRecentAccountsRelated
                                .addChildView(6, new AdapterAccountRelatedChild(getActivity(),
                                        "Name: " +
                                                listRelatedAccountTasks.get(i).getName() + "\n" +
                                                "Assigned To: " +
                                                listRelatedAccountTasks.get(i).getAssignedTo() + "\n" +
                                                "Subject: " +
                                                listRelatedAccountTasks.get(i).getSubject() + "\n" +
                                                "Task Type: " +
                                                listRelatedAccountTasks.get(i).getTaskType() + "\n" +
                                                "Task Status: " +
                                                listRelatedAccountTasks.get(i).getTaskStatus() + "\n" +
                                                "Priority: " +
                                                listRelatedAccountTasks.get(i).getPriority() + "\n" +
                                                "Date: " +
                                                listRelatedAccountTasks.get(i).getDate()
                                ));
                    }
                    if (listRelatedAccountTasks.size() > 3) {
                        phvRecentAccountsRelated.
                                addChildView(6, new AdapterAccountViewAll(getActivity(), FragmentAccountInfo.this, getString(R.string.task)));
                    } else if (listRelatedAccountTasks.size() == 0) {
                        phvRecentAccountsRelated.
                                addChildView(6, new AdapterAccountAddItem(getActivity(), FragmentAccountInfo.this, getString(R.string.task)));
                    }

                    /*
                    int lengthLocations;
                    if (listRelatedAccountLocations.size() > 3) {
                        lengthLocations = 3;
                    } else {
                        lengthLocations = listRelatedAccountLocations.size();
                    }
                    for (int i = 0; i < lengthLocations; i++) {
                        phvRecentAccountsRelated
                                .addChildView(6, new AdapterAccountRelatedChild(getActivity(),
                                        "Name: " +
                                                listRelatedAccountLocations.get(i).getName() + "\n" +
                                                "Address: " +
                                                listRelatedAccountLocations.get(i).getAddress() + ", " +
                                                listRelatedAccountLocations.get(i).getCity() + ", " +
                                                listRelatedAccountLocations.get(i).getState()
                                ));
                    }
                    if (listRelatedAccountLocations.size() > 3) {
                        phvRecentAccountsRelated.
                                addChildView(6, new AdapterAccountViewAll(getActivity(), FragmentAccountInfo.this, getString(R.string.location)));
                    } else if (listRelatedAccountLocations.size() == 0) {
                        phvRecentAccountsRelated.
                                addChildView(6, new AdapterAccountAddItem(getActivity(), FragmentAccountInfo.this, getString(R.string.location)));
                    }

                    int lengthChemicals;
                    if (listRelatedAccountChemicals.size() > 3) {
                        lengthChemicals = 3;
                    } else {
                        lengthChemicals = listRelatedAccountChemicals.size();
                    }
                    for (int i = 0; i < lengthChemicals; i++) {
                        phvRecentAccountsRelated
                                .addChildView(8, new AdapterAccountRelatedChild(getActivity(),
                                        "Chemical No: " +
                                                listRelatedAccountChemicals.get(i).getChemicalNo() + "\n" +
                                                "Product Name: " +
                                                listRelatedAccountChemicals.get(i).getProductName() + "\n" +
                                                "Account    : " +
                                                listRelatedAccountChemicals.get(i).getAccount() + "\n" +
                                                "WorkOrder: " +
                                                listRelatedAccountChemicals.get(i).getWorkOrder() + "\n" +
                                                "Created By: " +
                                                listRelatedAccountChemicals.get(i).getCreatedBy() + "\n" +
                                                "Created Date: " +
                                                listRelatedAccountChemicals.get(i).getCreatedDate()
                                ));
                    }
                    if (listRelatedAccountChemicals.size() > 3) {
                        phvRecentAccountsRelated.
                                addChildView(8, new AdapterAccountViewAll(getActivity(), FragmentAccountInfo.this, getString(R.string.chemical)));
                    } else if (listRelatedAccountChemicals.size() == 0) {
                        phvRecentAccountsRelated.
                                addChildView(8, new AdapterAccountAddItem(getActivity(), FragmentAccountInfo.this, getString(R.string.chemical)));
                    }

                    int lengthProducts;
                    if (listRelatedAccountProducts.size() > 3) {
                        lengthProducts = 3;
                    } else {
                        lengthProducts = listRelatedAccountProducts.size();
                    }
                    for (int i = 0; i < lengthProducts; i++) {
                        phvRecentAccountsRelated
                                .addChildView(9, new AdapterAccountRelatedChild(getActivity(),
                                        "Product Code: " +
                                                listRelatedAccountProducts.get(i).getProductCode() + "\n" +
                                                "Product Name: " +
                                                listRelatedAccountProducts.get(i).getProductName() + "\n" +
                                                "Default Quantity: " +
                                                listRelatedAccountProducts.get(i).getDefaultQuantity() + "\n" +
                                                "List Price: " +
                                                listRelatedAccountProducts.get(i).getListPrice() + "\n" +
                                                "Date Purchased: " +
                                                listRelatedAccountProducts.get(i).getDatePurchased() + "\n" +
                                                "Created By: " +
                                                listRelatedAccountProducts.get(i).getCreatedBy() + "\n" +
                                                "Created Date: " +
                                                listRelatedAccountProducts.get(i).getCreatedDate()
                                ));
                    }
                    if (listRelatedAccountProducts.size() > 3) {
                        phvRecentAccountsRelated.
                                addChildView(9, new AdapterAccountViewAll(getActivity(), FragmentAccountInfo.this, getString(R.string.product)));
                    } else if (listRelatedAccountProducts.size() == 0) {
                        phvRecentAccountsRelated.
                                addChildView(9, new AdapterAccountAddItem(getActivity(), FragmentAccountInfo.this, getString(R.string.product)));
                    }
                    */
                }
            }
        });

        edittextLastActivityDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] strSplit = strLastActivityDate.split("/");
                int day = Integer.valueOf(strSplit[1]);
                int month = Integer.valueOf(strSplit[0]) - 1;
                int year = Integer.valueOf(strSplit[2]);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                        isActivityClicked = true;
                        edittextLastActivityDate.setText(selectDate(d, m, y));
                        strNewLastActivityDate = selectDateEdit(d, m, y);
                        Log.d("TAG_newActDate", strNewLastActivityDate);
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        edittextLastServiceDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] strSplit = strLastActivityDate.split("/");
                int day = Integer.valueOf(strSplit[1]);
                int month = Integer.valueOf(strSplit[0]) - 1;
                int year = Integer.valueOf(strSplit[2]);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                        isServiceClicked = true;
                        edittextLastServiceDate.setText(selectDate(d, m, y));
                        strNewLastServiceDate = selectDateEdit(d, m, y);
                        Log.d("TAG_newSerDate", strNewLastServiceDate);
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        buttonBilAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!checkPermission()) {
                        requestPermission();
                    } else {
                        isBilClicked = true;
                        isShpClicked = false;
                        double lat = Double.valueOf(strBilLat);
                        double lng = Double.valueOf(strBilLng);
                        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                                .withLocation(lat, lng)
                                .withGeolocApiKey(getString(R.string.google_maps_key))
                                .withGooglePlacesEnabled()
                                .withSatelliteViewHidden()
                                .withVoiceSearchHidden()
                                .shouldReturnOkOnBackPressed()
                                .build(getActivity());
                        startActivityForResult(locationPickerIntent, 99);
                    }
                } else {
                    isBilClicked = true;
                    isShpClicked = false;
                    double lat = Double.valueOf(strBilLat);
                    double lng = Double.valueOf(strBilLng);
                    Intent locationPickerIntent = new LocationPickerActivity.Builder()
                            .withLocation(lat, lng)
                            .withGeolocApiKey(getString(R.string.google_maps_key))
                            .withGooglePlacesEnabled()
                            .withSatelliteViewHidden()
                            .withVoiceSearchHidden()
                            .shouldReturnOkOnBackPressed()
                            .build(getActivity());
                    startActivityForResult(locationPickerIntent, 99);
                }
            }
        });

        buttonShpAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!checkPermission()) {
                        requestPermission();
                    } else {
                        isBilClicked = false;
                        isShpClicked = true;
                        double lat = Double.valueOf(strShpLat);
                        double lng = Double.valueOf(strShpLng);
                        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                                .withLocation(lat, lng)
                                .withGeolocApiKey(getString(R.string.google_maps_key))
                                .withGooglePlacesEnabled()
                                .withSatelliteViewHidden()
                                .withVoiceSearchHidden()
                                .shouldReturnOkOnBackPressed()
                                .build(getActivity());
                        startActivityForResult(locationPickerIntent, 99);
                    }
                } else {
                    isBilClicked = false;
                    isShpClicked = true;
                    double lat = Double.valueOf(strShpLat);
                    double lng = Double.valueOf(strShpLng);
                    Intent locationPickerIntent = new LocationPickerActivity.Builder()
                            .withLocation(lat, lng)
                            .withGeolocApiKey(getString(R.string.google_maps_key))
                            .withGooglePlacesEnabled()
                            .withSatelliteViewHidden()
                            .withVoiceSearchHidden()
                            .shouldReturnOkOnBackPressed()
                            .build(getActivity());
                    startActivityForResult(locationPickerIntent, 99);
                }
            }
        });

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonDetails.setEnabled(false);
                radioButtonRelated.setEnabled(false);
                editMode(true, true);
            }
        });

        buttonMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String assignedTo = listAssignedTo.get(spinnerAssignedTo.getSelectedItemPosition()).getId();
                String accountName = edittextAssignedName.getText().toString().trim();
                String primaryContact;
                if (listPrimaryContact.size() > 0) {
                    primaryContact = listPrimaryContact.get(spinnerPrimaryContact.getSelectedItemPosition()).getId();
                } else {
                    primaryContact = "";
                }
                String phone = edittextPhone.getText().toString().trim();
                String preferredTechnician = listPreferredTechnician.get(spinnerPreferredTechnician.getSelectedItemPosition()).getId();
                String accessNotes = edittextAccessNotes.getText().toString().trim();
                String notes = edittextNotes.getText().toString().trim();
                String accountType = listAccountType.get(spinnerAccountType.getSelectedItemPosition()).getId();
                String isActive = null;
                if (checkboxIsActive.isChecked()) {
                    isActive = "1";
                } else {
                    isActive = "0";
                }
                String lastActivityDate = null;
                if (isActivityClicked) {
                    if (!strNewLastActivityDate.equals("0")) {
                        lastActivityDate = strNewLastActivityDate;
                    } else {
                        lastActivityDate = strLastActivityDate;
                    }
                } else {
                    lastActivityDate = strLastActivityDate;
                }
                String lastServiceDate = null;
                if (isServiceClicked) {
                    if (!strNewLastServiceDate.equals("0")) {
                        lastServiceDate = strNewLastServiceDate;
                    } else {
                        lastServiceDate = strLastServiceDate;
                    }
                } else {
                    lastServiceDate = strLastServiceDate;
                }
                String popUpReminder = edittextPopupReminder.getText().toString().trim();
                String bilLat = null;
                String bilLng = null;
                if (isBilClicked) {
                    if (!strNewBilLat.equals("0") && !strNewBilLng.equals("0")) {
                        bilLat = strNewBilLat;
                        bilLng = strNewBilLng;
                    } else {
                        bilLat = strBilLat;
                        bilLng = strBilLng;
                    }
                } else {
                    bilLat = strBilLat;
                    bilLng = strBilLng;
                }
                String bilAddress = edittextBilAddress.getText().toString().trim();
                String bilCity = edittextBilCity.getText().toString().trim();
                String bilState = edittextBilState.getText().toString().trim();
                String bilCountry = edittextBilCountry.getText().toString().trim();
                String bilPostal = edittextBilPostal.getText().toString().trim();
                String shpLat = null;
                String shpLng = null;
                if (isShpClicked) {
                    if (!strNewShpLat.equals("0") && !strNewShpLng.equals("0")) {
                        shpLat = strNewShpLat;
                        shpLng = strNewShpLng;
                    } else {
                        shpLat = strShpLat;
                        shpLng = strShpLng;
                    }
                } else {
                    shpLat = strShpLat;
                    shpLng = strShpLng;
                }
                String shpAddress = edittextShpAddress.getText().toString().trim();
                String shpCity = edittextShpCity.getText().toString().trim();
                String shpState = edittextShpState.getText().toString().trim();
                String shpCountry = edittextShpCountry.getText().toString().trim();
                String shpPostal = edittextShpPostal.getText().toString().trim();

                if (TextUtils.isEmpty(accountName)) {
                    Utl.showToast(getActivity(), "Enter Account Name");
                } else if (TextUtils.isEmpty(phone)) {
                    Utl.showToast(getActivity(), "Enter Phone No");
                } /*else if (TextUtils.isEmpty(accessNotes)) {
                    Utl.showToast(getActivity(), "Enter Access Notes");
                } else if (TextUtils.isEmpty(notes)) {
                    Utl.showToast(getActivity(), "Enter Notes");
                } else if (TextUtils.isEmpty(lastActivityDate)) {
                    Utl.showToast(getActivity(), "Select Last Activity Date");
                } else if (TextUtils.isEmpty(lastServiceDate)) {
                    Utl.showToast(getActivity(), "Select Last Service Date");
                } else if (TextUtils.isEmpty(popUpReminder)) {
                    Utl.showToast(getActivity(), "Enter Popup Reminder");
                }*/ else if (TextUtils.isEmpty(bilAddress)) {
                    Utl.showToast(getActivity(), "Enter Billing Address");
                } else if (TextUtils.isEmpty(bilCity)) {
                    Utl.showToast(getActivity(), "Enter Billing City");
                } else if (TextUtils.isEmpty(bilState)) {
                    Utl.showToast(getActivity(), "Enter Billing State");
                } else if (TextUtils.isEmpty(bilCountry)) {
                    Utl.showToast(getActivity(), "Enter Billing Country");
                } else if (TextUtils.isEmpty(bilPostal)) {
                    Utl.showToast(getActivity(), "Enter Billing Postal Code");
                } else if (TextUtils.isEmpty(shpAddress)) {
                    Utl.showToast(getActivity(), "Enter Shipping Address");
                } else if (TextUtils.isEmpty(shpCity)) {
                    Utl.showToast(getActivity(), "Enter Shipping City");
                } else if (TextUtils.isEmpty(shpState)) {
                    Utl.showToast(getActivity(), "Enter Shipping State");
                } else if (TextUtils.isEmpty(shpCountry)) {
                    Utl.showToast(getActivity(), "Enter Shipping Country");
                } else if (TextUtils.isEmpty(shpPostal)) {
                    Utl.showToast(getActivity(), "Enter Shipping Postal Code");
                } else {
                    if (isCloneMode) {
                        createAccount(assignedTo,
                                accountName,
                                phone,
                                preferredTechnician,
                                accessNotes,
                                notes,
                                accountType,
                                isActive,
                                lastActivityDate,
                                lastServiceDate,
                                popUpReminder,
                                bilLat, bilLng,
                                bilAddress, bilCity, bilState, bilCountry, bilPostal,
                                shpLat, shpLng,
                                shpAddress, shpCity, shpState, shpCountry, shpPostal);
                    } else {
                        editAccount(accountId,
                                assignedTo,
                                accountName,
                                primaryContact,
                                phone,
                                preferredTechnician,
                                accessNotes,
                                notes,
                                accountType,
                                isActive,
                                lastActivityDate,
                                lastServiceDate,
                                popUpReminder,
                                bilLat, bilLng,
                                bilAddress, bilCity, bilState, bilCountry, bilPostal,
                                shpLat, shpLng,
                                shpAddress, shpCity, shpState, shpCountry, shpPostal);
                    }
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCloneMode = false;
                editMode(false, true);
                radioButtonDetails.setEnabled(true);
                radioButtonRelated.setEnabled(true);
            }
        });

        buttonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    if (!TextUtils.isEmpty(strPhoneNo)) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + strPhoneNo));
                        startActivity(callIntent);
                    } else {
                        Utl.showToast(getActivity(), "No Phone Number Available.");
                    }
                }
            }
        });

        buttonComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    if (!TextUtils.isEmpty(strPhoneNo)) {
                        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                        smsIntent.setData(Uri.parse("sms:" + strPhoneNo));
                        startActivity(smsIntent);
                    } else {
                        Utl.showToast(getActivity(), "No Phone Number Available.");
                    }
                }
            }
        });

        buttonDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickCrateEvent != null) {
                    clickCrateEvent.callbackAccountCreateEvent(getString(R.string.account), accountId, strAccountName);
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99) {
            if (data != null) {
                double latitude = data.getDoubleExtra("latitude", 0.0);
                strNewLat = String.valueOf(latitude);
                double longitude = data.getDoubleExtra("longitude", 0.0);
                strNewLng = String.valueOf(longitude);
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> addresses = new ArrayList<>();
                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                strNewAddress = addresses.get(0).getSubThoroughfare() + ", " + addresses.get(0).getThoroughfare();
                if (addresses.get(0).getLocality() != null) {
                    strNewCity = addresses.get(0).getLocality();
                } else {
                    strNewCity = addresses.get(0).getSubLocality();
                }
                if (addresses.get(0).getAdminArea() != null) {
                    strNewState = addresses.get(0).getAdminArea();
                } else {
                    strNewState = addresses.get(0).getSubAdminArea();
                }
                strNewCountry = addresses.get(0).getCountryName();
                strNewPostal = addresses.get(0).getPostalCode();
                Log.d("TAG_LATITUDE****", strNewLat);
                Log.d("TAG_LONGITUDE****", strNewLng);
                Log.d("TAG_ADDRESS****", strNewAddress);
                Log.d("TAG_CITY****", strNewCity);
                Log.d("TAG_STATE****", strNewState);
                Log.d("TAG_COUNTRY****", strNewCountry);
                Log.d("TAG_POSTAL****", strNewPostal);
                if (isBilClicked) {
                    strNewBilLat = strNewLat;
                    strNewBilLng = strNewLng;
                    edittextBilAddress.setText(strNewAddress);
                    edittextBilCity.setText(strNewCity);
                    edittextBilState.setText(strNewState);
                    edittextBilCountry.setText(strNewCountry);
                    edittextBilPostal.setText(strNewPostal);
                } else if (isShpClicked) {
                    strNewShpLat = strNewLat;
                    strNewShpLng = strNewLng;
                    edittextShpAddress.setText(strNewAddress);
                    edittextShpCity.setText(strNewCity);
                    edittextShpState.setText(strNewState);
                    edittextShpCountry.setText(strNewCountry);
                    edittextShpPostal.setText(strNewPostal);
                }
            } else {
                Log.d("RESULT", "CANCELLED");
            }
        }
    }

    private void editMode(boolean bolEdit, boolean bolResponse) {
        if (!bolEdit) {
            layoutHeader.setVisibility(View.VISIBLE);
            radioGroupMain.setVisibility(View.VISIBLE);
            layoutAssignedTo.setVisibility(View.GONE);
            layoutPrimaryContact.setVisibility(View.GONE);
            layoutPreferredTechnician.setVisibility(View.GONE);
            layoutAccountType.setVisibility(View.GONE);
            spinnerAssignedTo.setVisibility(View.GONE);
            spinnerPrimaryContact.setVisibility(View.GONE);
            spinnerPreferredTechnician.setVisibility(View.GONE);
            spinnerAccountType.setVisibility(View.GONE);
            spinnerAssignedTo.setAdapter(null);
            spinnerPrimaryContact.setAdapter(null);
            spinnerPreferredTechnician.setAdapter(null);
            spinnerAccountType.setAdapter(null);
            edittextAssignedName.setVisibility(View.GONE);
            edittextPhone.setVisibility(View.GONE);
            edittextAccessNotes.setVisibility(View.GONE);
            edittextNotes.setVisibility(View.GONE);
            edittextLastActivityDate.setVisibility(View.GONE);
            edittextLastServiceDate.setVisibility(View.GONE);
            edittextPopupReminder.setVisibility(View.GONE);
            edittextBilAddress.setVisibility(View.GONE);
            edittextBilCity.setVisibility(View.GONE);
            edittextBilState.setVisibility(View.GONE);
            edittextBilCountry.setVisibility(View.GONE);
            edittextBilPostal.setVisibility(View.GONE);
            edittextShpAddress.setVisibility(View.GONE);
            edittextShpCity.setVisibility(View.GONE);
            edittextShpState.setVisibility(View.GONE);
            edittextShpCountry.setVisibility(View.GONE);
            edittextShpPostal.setVisibility(View.GONE);
            buttonBilAddress.setVisibility(View.GONE);
            buttonShpAddress.setVisibility(View.GONE);
            layoutSaveCancel.setVisibility(View.GONE);
            layoutNavigation.setVisibility(View.VISIBLE);
            buttonCall.setEnabled(true);
            buttonComment.setEnabled(true);
            buttonDate.setEnabled(true);
            buttonEdit.setEnabled(true);
            textviewHeaderType.setVisibility(View.VISIBLE);
            textviewHeaderPhone.setVisibility(View.VISIBLE);
            textviewAssignedTo.setVisibility(View.VISIBLE);
            textviewAssignedName.setVisibility(View.VISIBLE);
            textviewPrimaryContactHeader.setVisibility(View.VISIBLE);
            textviewPrimaryContact.setVisibility(View.VISIBLE);
            textviewPhone.setVisibility(View.VISIBLE);
            textviewPreferredTechnician.setVisibility(View.VISIBLE);
            textviewAccesNotes.setVisibility(View.VISIBLE);
            textviewNotes.setVisibility(View.VISIBLE);
            textviewAccountType.setVisibility(View.VISIBLE);
            textviewLastActivityDate.setVisibility(View.VISIBLE);
            textviewLastServiceDate.setVisibility(View.VISIBLE);
            textviewPopupReminder.setVisibility(View.VISIBLE);
            textviewBilAddress.setVisibility(View.VISIBLE);
            textviewBilCity.setVisibility(View.GONE);
            textviewBilState.setVisibility(View.GONE);
            textviewBilCountry.setVisibility(View.GONE);
            textviewBilPostal.setVisibility(View.GONE);
            textviewShpAddress.setVisibility(View.VISIBLE);
            textviewShpCity.setVisibility(View.GONE);
            textviewShpState.setVisibility(View.GONE);
            textviewShpCountry.setVisibility(View.GONE);
            textviewShpPostal.setVisibility(View.GONE);
            layoutSystemInfo.setVisibility(View.VISIBLE);
            checkboxIsActive.setEnabled(false);
            if (bolResponse) {
                setViewInfo();
            }
        } else {
            layoutHeader.setVisibility(View.VISIBLE);
            radioGroupMain.setVisibility(View.VISIBLE);
            layoutAssignedTo.setVisibility(View.VISIBLE);
            layoutPrimaryContact.setVisibility(View.VISIBLE);
            layoutPreferredTechnician.setVisibility(View.VISIBLE);
            layoutAccountType.setVisibility(View.VISIBLE);
            spinnerAssignedTo.setVisibility(View.VISIBLE);
            spinnerPrimaryContact.setVisibility(View.VISIBLE);
            spinnerPreferredTechnician.setVisibility(View.VISIBLE);
            spinnerAccountType.setVisibility(View.VISIBLE);
            spinnerAssignedTo.setAdapter(adapterAssignedTo);
            spinnerPrimaryContact.setAdapter(adapterPrimaryContact);
            spinnerPreferredTechnician.setAdapter(adapterPreferredTechnician);
            spinnerAccountType.setAdapter(adapterAccountType);
            edittextAssignedName.setVisibility(View.VISIBLE);
            edittextPhone.setVisibility(View.VISIBLE);
            edittextAccessNotes.setVisibility(View.VISIBLE);
            edittextNotes.setVisibility(View.VISIBLE);
            edittextLastActivityDate.setVisibility(View.VISIBLE);
            edittextLastServiceDate.setVisibility(View.VISIBLE);
            edittextPopupReminder.setVisibility(View.VISIBLE);
            edittextBilAddress.setVisibility(View.VISIBLE);
            edittextBilCity.setVisibility(View.VISIBLE);
            edittextBilState.setVisibility(View.VISIBLE);
            edittextBilCountry.setVisibility(View.VISIBLE);
            edittextBilPostal.setVisibility(View.VISIBLE);
            edittextShpAddress.setVisibility(View.VISIBLE);
            edittextShpCity.setVisibility(View.VISIBLE);
            edittextShpState.setVisibility(View.VISIBLE);
            edittextShpCountry.setVisibility(View.VISIBLE);
            edittextShpPostal.setVisibility(View.VISIBLE);
            buttonBilAddress.setVisibility(View.VISIBLE);
            buttonShpAddress.setVisibility(View.VISIBLE);
            layoutSaveCancel.setVisibility(View.VISIBLE);
            layoutNavigation.setVisibility(View.GONE);
            buttonCall.setEnabled(false);
            buttonComment.setEnabled(false);
            buttonDate.setEnabled(false);
            buttonEdit.setEnabled(false);
            textviewHeaderType.setVisibility(View.GONE);
            textviewHeaderPhone.setVisibility(View.GONE);
            textviewAssignedTo.setVisibility(View.GONE);
            textviewAssignedName.setVisibility(View.GONE);
            textviewPrimaryContactHeader.setVisibility(View.VISIBLE);
            textviewPrimaryContact.setVisibility(View.GONE);
            textviewPhone.setVisibility(View.GONE);
            textviewPreferredTechnician.setVisibility(View.GONE);
            textviewAccesNotes.setVisibility(View.GONE);
            textviewNotes.setVisibility(View.GONE);
            textviewAccountType.setVisibility(View.GONE);
            textviewLastActivityDate.setVisibility(View.GONE);
            textviewLastServiceDate.setVisibility(View.GONE);
            textviewPopupReminder.setVisibility(View.GONE);
            textviewBilAddress.setVisibility(View.GONE);
            textviewBilCity.setVisibility(View.VISIBLE);
            textviewBilState.setVisibility(View.VISIBLE);
            textviewBilCountry.setVisibility(View.VISIBLE);
            textviewBilPostal.setVisibility(View.VISIBLE);
            textviewShpAddress.setVisibility(View.GONE);
            textviewShpCity.setVisibility(View.VISIBLE);
            textviewShpState.setVisibility(View.VISIBLE);
            textviewShpCountry.setVisibility(View.VISIBLE);
            textviewShpPostal.setVisibility(View.VISIBLE);
            layoutSystemInfo.setVisibility(View.GONE);
            checkboxIsActive.setEnabled(true);
            if (bolResponse) {
                setEditInfo();
            }
        }
    }

    private void cloneMode() {
        layoutHeader.setVisibility(View.GONE);
        radioGroupMain.setVisibility(View.GONE);
        layoutAssignedTo.setVisibility(View.VISIBLE);
        layoutPrimaryContact.setVisibility(View.GONE);
        layoutPreferredTechnician.setVisibility(View.VISIBLE);
        layoutAccountType.setVisibility(View.VISIBLE);
        spinnerAssignedTo.setVisibility(View.VISIBLE);
        spinnerPrimaryContact.setVisibility(View.VISIBLE);
        spinnerPreferredTechnician.setVisibility(View.VISIBLE);
        spinnerAccountType.setVisibility(View.VISIBLE);
        spinnerAssignedTo.setAdapter(adapterAssignedTo);
        spinnerPrimaryContact.setAdapter(adapterPrimaryContact);
        spinnerPreferredTechnician.setAdapter(adapterPreferredTechnician);
        spinnerAccountType.setAdapter(adapterAccountType);
        edittextAssignedName.setVisibility(View.VISIBLE);
        edittextPhone.setVisibility(View.VISIBLE);
        edittextAccessNotes.setVisibility(View.VISIBLE);
        edittextNotes.setVisibility(View.VISIBLE);
        edittextLastActivityDate.setVisibility(View.VISIBLE);
        edittextLastServiceDate.setVisibility(View.VISIBLE);
        edittextPopupReminder.setVisibility(View.VISIBLE);
        edittextBilAddress.setVisibility(View.VISIBLE);
        edittextBilCity.setVisibility(View.VISIBLE);
        edittextBilState.setVisibility(View.VISIBLE);
        edittextBilCountry.setVisibility(View.VISIBLE);
        edittextBilPostal.setVisibility(View.VISIBLE);
        edittextShpAddress.setVisibility(View.VISIBLE);
        edittextShpCity.setVisibility(View.VISIBLE);
        edittextShpState.setVisibility(View.VISIBLE);
        edittextShpCountry.setVisibility(View.VISIBLE);
        edittextShpPostal.setVisibility(View.VISIBLE);
        buttonBilAddress.setVisibility(View.VISIBLE);
        buttonShpAddress.setVisibility(View.VISIBLE);
        layoutSaveCancel.setVisibility(View.VISIBLE);
        layoutNavigation.setVisibility(View.GONE);
        buttonCall.setEnabled(false);
        buttonComment.setEnabled(false);
        buttonDate.setEnabled(false);
        buttonEdit.setEnabled(false);
        textviewHeaderType.setVisibility(View.GONE);
        textviewHeaderPhone.setVisibility(View.GONE);
        textviewAssignedTo.setVisibility(View.GONE);
        textviewAssignedName.setVisibility(View.GONE);
        textviewPrimaryContactHeader.setVisibility(View.GONE);
        textviewPrimaryContact.setVisibility(View.GONE);
        textviewPhone.setVisibility(View.GONE);
        textviewPreferredTechnician.setVisibility(View.GONE);
        textviewAccesNotes.setVisibility(View.GONE);
        textviewNotes.setVisibility(View.GONE);
        textviewAccountType.setVisibility(View.GONE);
        textviewLastActivityDate.setVisibility(View.GONE);
        textviewLastServiceDate.setVisibility(View.GONE);
        textviewPopupReminder.setVisibility(View.GONE);
        textviewBilAddress.setVisibility(View.GONE);
        textviewBilCity.setVisibility(View.VISIBLE);
        textviewBilState.setVisibility(View.VISIBLE);
        textviewBilCountry.setVisibility(View.VISIBLE);
        textviewBilPostal.setVisibility(View.VISIBLE);
        textviewShpAddress.setVisibility(View.GONE);
        textviewShpCity.setVisibility(View.VISIBLE);
        textviewShpState.setVisibility(View.VISIBLE);
        textviewShpCountry.setVisibility(View.VISIBLE);
        textviewShpPostal.setVisibility(View.VISIBLE);
        layoutSystemInfo.setVisibility(View.GONE);
        checkboxIsActive.setEnabled(true);
        setCloneInfo();
    }

    private void setViewInfo() {
        ActivityMain.textviewCenter.setText(strAccountName);
        textviewHeaderType.setText("Type: " + strAccountTypeName);
        textviewHeaderPhone.setText("Phone: " + strPhoneNo);
        textviewAssignedTo.setText(strAssignedToName);
        textviewAssignedName.setText(strAccountName);
        textviewPrimaryContact.setText(strPrimaryContactName);
        textviewPhone.setText(strPhoneNo);
        textviewPreferredTechnician.setText(strPreferredTechnicianName);
        textviewAccesNotes.setText(strAccessNotes);
        textviewNotes.setText(strNotes);
        textviewAccountType.setText(strAccountTypeName);
        textviewLastActivityDate.setText(formatDate(strLastActivityDate));
        textviewLastServiceDate.setText(formatDate(strLastServiceDate));
        textviewPopupReminder.setText(strPopupReminder);
        textviewBilAddress.setText(strBilAddress + "\n" +
                strBilCity + ", " +
                strBilState + ", " +
                strBilPostal + "\n" +
                strBilCountry);
        textviewShpAddress.setText(strShpAddress + "\n" +
                strShpCity + ", " +
                strShpState + ", " +
                strShpPostal + "\n" +
                strShpCountry);
        if (strIsActive.equals("1")) {
            checkboxIsActive.setChecked(true);
        } else {
            checkboxIsActive.setChecked(false);
        }
        textviewCreatedDate.setText(strCreatedDate);
        textviewCreatedBy.setText(strCreatedBy);
        textviewLastModifiedDate.setText(strLastModifiedDate);
        textviewLastModifiedBy.setText(strLastModifiedBy);
    }

    private void setEditInfo() {
        setSpinnerDropDownHeight(listAssignedTo, spinnerAssignedTo);
        setSpinnerDropDownHeight(listPrimaryContact, spinnerPrimaryContact);
        setSpinnerDropDownHeight(listPreferredTechnician, spinnerPreferredTechnician);
        setSpinnerDropDownHeight(listAccountType, spinnerAccountType);
        spinnerAssignedTo.setSelection(getListIndex(listAssignedTo, strAssignedToName));
        if (!strPrimaryContactName.equals("")) {
            spinnerPrimaryContact.setSelection(getListIndex(listPrimaryContact, strPrimaryContactName));
        } else {
            spinnerPrimaryContact.setSelection(0);
        }
        spinnerPreferredTechnician.setSelection(getListIndex(listPreferredTechnician, strPreferredTechnicianName));
        spinnerAccountType.setSelection(getListIndex(listAccountType, strAccountTypeName));
        if (strIsActive.equals("1")) {
            checkboxIsActive.setChecked(true);
        } else {
            checkboxIsActive.setChecked(false);
        }
        edittextAssignedName.setText(strAccountName);
        edittextPhone.setText(strPhoneNo);
        edittextAccessNotes.setText(strAccessNotes);
        edittextNotes.setText(strNotes);
        edittextLastActivityDate.setText(formatDate(strLastActivityDate));
        edittextLastServiceDate.setText(formatDate(strLastServiceDate));
        edittextPopupReminder.setText(strPopupReminder);
        edittextBilAddress.setText(strBilAddress);
        edittextBilCity.setText(strBilCity);
        edittextBilState.setText(strBilState);
        edittextBilCountry.setText(strBilCountry);
        edittextBilPostal.setText(strBilPostal);
        edittextShpAddress.setText(strShpAddress);
        edittextShpCity.setText(strShpCity);
        edittextShpState.setText(strShpState);
        edittextShpCountry.setText(strShpCountry);
        edittextShpPostal.setText(strShpPostal);
    }

    private void setCloneInfo() {
        isCloneMode = true;
        ActivityMain.textviewCenter.setText(getString(R.string.account_create));
        setSpinnerDropDownHeight(listAssignedTo, spinnerAssignedTo);
        setSpinnerDropDownHeight(listPrimaryContact, spinnerPrimaryContact);
        setSpinnerDropDownHeight(listPreferredTechnician, spinnerPreferredTechnician);
        setSpinnerDropDownHeight(listAccountType, spinnerAccountType);
        spinnerAssignedTo.setSelection(getListIndex(listAssignedTo, strAssignedToName));
        if (!strPrimaryContactName.equals("")) {
            spinnerPrimaryContact.setSelection(getListIndex(listPrimaryContact, strPrimaryContactName));
        } else {
            spinnerPrimaryContact.setSelection(0);
        }
        spinnerPreferredTechnician.setSelection(getListIndex(listPreferredTechnician, strPreferredTechnicianName));
        spinnerAccountType.setSelection(getListIndex(listAccountType, strAccountTypeName));
        if (strIsActive.equals("1")) {
            checkboxIsActive.setChecked(true);
        } else {
            checkboxIsActive.setChecked(false);
        }
        edittextAssignedName.setText(strAccountName);
        edittextPhone.setText(strPhoneNo);
        edittextAccessNotes.setText(strAccessNotes);
        edittextNotes.setText(strNotes);
        edittextLastActivityDate.setText(formatDate(strLastActivityDate));
        edittextLastServiceDate.setText(formatDate(strLastServiceDate));
        edittextPopupReminder.setText(strPopupReminder);
        edittextBilAddress.setText(strBilAddress);
        edittextBilCity.setText(strBilCity);
        edittextBilState.setText(strBilState);
        edittextBilCountry.setText(strBilCountry);
        edittextBilPostal.setText(strBilPostal);
        edittextShpAddress.setText(strShpAddress);
        edittextShpCity.setText(strShpCity);
        edittextShpState.setText(strShpState);
        edittextShpCountry.setText(strShpCountry);
        edittextShpPostal.setText(strShpPostal);
    }

    private void setSpinnerDropDownHeight(List<ModelSpinner> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 70);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    public int getListIndex(List<ModelSpinner> list, String name) {
        for (int i = 0; i < list.size(); i++) {
            ModelSpinner model = list.get(i);
            if (name.equals(model.getName())) {
                return i;
            }
        }
        return -1;
    }

    public String formatDate(String time) {
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String selectDate(int d, int m, int y) {
        String time = "" + (m + 1) + "/" + d + "/" + y;
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String selectDateEdit(int d, int m, int y) {
        String time = "" + (m + 1) + "/" + d + "/" + y;
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void dialogMore() {
        final Dialog dialogMore = new Dialog(getActivity(), R.style.DialogFullScreen);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMore.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        dialogMore.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMore.setCancelable(true);
        dialogMore.setCanceledOnTouchOutside(true);
        dialogMore.setContentView(R.layout.dialog_more);

        TextView buttonMoreCall = (TextView) dialogMore.findViewById(R.id.button_more_call);
        TextView buttonMoreText = (TextView) dialogMore.findViewById(R.id.button_more_text);
        TextView buttonMoreEmail = (TextView) dialogMore.findViewById(R.id.button_more_email);
        TextView buttonMoreEdit = (TextView) dialogMore.findViewById(R.id.button_more_edit);
        TextView buttonMoreSign = (TextView) dialogMore.findViewById(R.id.button_more_sign);
        TextView buttonMoreDoc = (TextView) dialogMore.findViewById(R.id.button_more_doc);
        TextView buttonMoreNewLine = (TextView) dialogMore.findViewById(R.id.button_more_new_line);
        TextView buttonMoreChemical = (TextView) dialogMore.findViewById(R.id.button_more_new_chemical);
        TextView buttonMoreEvent = (TextView) dialogMore.findViewById(R.id.button_more_new_event);
        TextView buttonMoreInvoice = (TextView) dialogMore.findViewById(R.id.button_more_new_invoice);
        TextView buttonMoreFile = (TextView) dialogMore.findViewById(R.id.button_more_new_file);
        TextView buttonMoreNote = (TextView) dialogMore.findViewById(R.id.button_more_new_note);
        TextView buttonMoreTask = (TextView) dialogMore.findViewById(R.id.button_more_new_task);
        TextView buttonMoreConvert = (TextView) dialogMore.findViewById(R.id.button_more_convert);
        TextView buttonMoreClone = (TextView) dialogMore.findViewById(R.id.button_more_clone);
        TextView buttonMoreDelete = (TextView) dialogMore.findViewById(R.id.button_more_delete);
        View buttomViewCall = (View) dialogMore.findViewById(R.id.view_more_call);
        View buttomViewText = (View) dialogMore.findViewById(R.id.view_more_text);
        View buttomViewEmail = (View) dialogMore.findViewById(R.id.view_more_email);
        View buttomViewEdit = (View) dialogMore.findViewById(R.id.view_more_edit);
        View buttomViewSign = (View) dialogMore.findViewById(R.id.view_more_sign);
        View buttomViewDoc = (View) dialogMore.findViewById(R.id.view_more_doc);
        View buttomViewNewLine = (View) dialogMore.findViewById(R.id.view_more_new_line);
        View buttomViewChemical = (View) dialogMore.findViewById(R.id.view_more_new_chemical);
        View buttomViewEvent = (View) dialogMore.findViewById(R.id.view_more_new_event);
        View buttomViewInvoice = (View) dialogMore.findViewById(R.id.view_more_new_invoice);
        View buttomViewFile = (View) dialogMore.findViewById(R.id.view_more_new_file);
        View buttomViewNote = (View) dialogMore.findViewById(R.id.view_more_new_note);
        View buttomViewTask = (View) dialogMore.findViewById(R.id.view_more_new_task);
        View buttomViewConvert = (View) dialogMore.findViewById(R.id.view_more_convert);
        View buttomViewClone = (View) dialogMore.findViewById(R.id.view_more_clone);
        Button buttonMoreClose = (Button) dialogMore.findViewById(R.id.button_more_close);

        buttonMoreEmail.setVisibility(View.GONE);
        buttomViewEmail.setVisibility(View.GONE);
        buttonMoreSign.setVisibility(View.GONE);
        buttomViewSign.setVisibility(View.GONE);
        buttonMoreNote.setVisibility(View.GONE);
        buttomViewNote.setVisibility(View.GONE);
        buttonMoreConvert.setVisibility(View.GONE);
        buttomViewConvert.setVisibility(View.GONE);

        buttonMoreDoc.setText(getString(R.string.new_contact));
        buttonMoreNewLine.setText(getString(R.string.new_work_order));
        buttonMoreChemical.setText(getString(R.string.new_event));
        buttonMoreEvent.setText(getString(R.string.new_estimate));
        buttonMoreClone.setText(getString(R.string.clone_account));
        buttonMoreDelete.setText(getString(R.string.delete_account));

        buttonMoreCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    if (!TextUtils.isEmpty(strPhoneNo)) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + strPhoneNo));
                        startActivity(callIntent);
                    } else {
                        Utl.showToast(getActivity(), "No Phone Number Available.");
                    }
                }
            }
        });

        buttonMoreText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    if (!TextUtils.isEmpty(strPhoneNo)) {
                        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                        smsIntent.setData(Uri.parse("sms:" + strPhoneNo));
                        startActivity(smsIntent);
                    } else {
                        Utl.showToast(getActivity(), "No Phone Number Available.");
                    }
                }
            }
        });

        buttonMoreEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonDetails.setEnabled(false);
                radioButtonRelated.setEnabled(false);
                editMode(true, true);
                dialogMore.dismiss();
            }
        });

        buttonMoreDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                if (clickContactCreate != null) {
                    clickContactCreate.callbackAccountContactCreate();
                }
            }
        });

        buttonMoreNewLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                if (clickWorkOrderCreate != null) {
                    clickWorkOrderCreate.callbackAccountWorkOrderCreate();
                }
            }
        });

        buttonMoreChemical.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                if (clickCrateEvent != null) {
                    clickCrateEvent.callbackAccountCreateEvent(getString(R.string.account), accountId, strAccountName);
                }
            }
        });

        buttonMoreEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                if (clickEstimateCreate != null) {
                    clickEstimateCreate.callbackAccountEstimateCreate();
                }
            }
        });

        buttonMoreInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                if (clickInvoiceCreate != null) {
                    clickInvoiceCreate.callbackAccountInvoiceCreate();
                }
            }
        });

        buttonMoreFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                if (clickCrateFile != null) {
                    clickCrateFile.callbackAccountCreateFile(getString(R.string.account));
                }
            }
        });

        buttonMoreTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                if (clickTaskCreate != null) {
                    clickTaskCreate.callbackAccountTaskCreate();
                }
            }
        });

        buttonMoreClone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                cloneMode();
            }
        });

        buttonMoreDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                deleteObject();
            }
        });

        buttonMoreClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
            }
        });

        dialogMore.show();
        dialogMore.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialogMore.getWindow().setDimAmount(0.5f);
        dialogMore.getWindow().setAttributes(lp);
    }

    private void getAllUser() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getAccAllUsers(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_AllUser" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAssignedTo.clear();
                            listPreferredTechnician.clear();
                            listPreferredTechnician.add(new ModelSpinner("0", "Select Technician"));
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_UserID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listAssignedTo.add(model);
                                listPreferredTechnician.add(model);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getContact(final String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getAccContacts(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Contact" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listPrimaryContact.clear();
                            listPrimaryContact.add(new ModelSpinner("0", "Select Contact"));
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_ContactID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listPrimaryContact.add(model);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getAccountType() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getAccAccountType(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_AccountType" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAccountType.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_AccountTypeID));
                                model.setName(dataObject.getString(Cons.KEY_AccountType));
                                listAccountType.add(model);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getAccountDetailed(final String accountId) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response;
        response = apiInterface.getAccountDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_AccInfo" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            JSONObject dataObject = object.getJSONObject("data");
                            strAccountId = dataObject.getString(Cons.KEY_AccountID);
                            strAssignedTo = dataObject.getString(Cons.KEY_AssignedTo);
                            strAssignedToName = dataObject.getString(Cons.KEY_AssignedToName);
                            strAccountNo = dataObject.getString(Cons.KEY_AccountNo);
                            strAccountName = dataObject.getString(Cons.KEY_AccountName);
                            strPrimaryContact = dataObject.getString(Cons.KEY_PrimaryContact);
                            strPrimaryContactName = dataObject.getString(Cons.KEY_PrimaryContactName);
                            strPhoneNo = dataObject.getString(Cons.KEY_PhoneNo);
                            strPreferredTechnician = dataObject.getString(Cons.KEY_PreferredTechnician);
                            strPreferredTechnicianName = dataObject.getString(Cons.KEY_PreferredTechnicianName);
                            strAccessNotes = dataObject.getString(Cons.KEY_AccessNotes);
                            strNotes = dataObject.getString(Cons.KEY_Notes);
                            strAccountType = dataObject.getString(Cons.KEY_AccountType);
                            strAccountTypeName = dataObject.getString(Cons.KEY_AccountTypeName);
                            strLastActivityDate = dataObject.getString(Cons.KEY_LastActivityDate);
                            strLastServiceDate = dataObject.getString(Cons.KEY_LastServiceDate);
                            strPopupReminder = dataObject.getString(Cons.KEY_PopupReminder);
                            strBilAddress = dataObject.getString(Cons.KEY_BillingAddress);
                            strBilCity = dataObject.getString(Cons.KEY_BillingCity);
                            strBilState = dataObject.getString(Cons.KEY_BillingState);
                            strBilCountry = dataObject.getString(Cons.KEY_BillingCountry);
                            strBilPostal = dataObject.getString(Cons.KEY_BillingPostalCode);
                            strShpAddress = dataObject.getString(Cons.KEY_ShippingAddress);
                            strShpCity = dataObject.getString(Cons.KEY_ShippingCity);
                            strShpState = dataObject.getString(Cons.KEY_ShippingState);
                            strShpCountry = dataObject.getString(Cons.KEY_ShippingCountry);
                            strShpPostal = dataObject.getString(Cons.KEY_ShippingPostalCode);
                            strIsActive = dataObject.getString(Cons.KEY_IsActive);
                            strBilLat = dataObject.getString(Cons.KEY_BillingLatitude);
                            strBilLng = dataObject.getString(Cons.KEY_BillingLongitude);
                            strShpLat = dataObject.getString(Cons.KEY_ShippingLatitude);
                            strShpLng = dataObject.getString(Cons.KEY_ShippingLongitude);
                            strCreatedDate = dataObject.getString(Cons.KEY_CreatedDate);
                            strCreatedBy = dataObject.getString(Cons.KEY_CreatedBy);
                            strLastModifiedDate = dataObject.getString(Cons.KEY_LastModifiedDate);
                            strLastModifiedBy = dataObject.getString(Cons.KEY_LastModifiedBy);
                            editMode(false, true);
                            radioButtonDetails.setEnabled(true);
                            radioButtonRelated.setEnabled(true);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void createAccount(String assignedTo,
                               String accountName,
                               String phone,
                               String preferredTechnician,
                               String accessNotes,
                               String notes,
                               String accountType,
                               String isActive,
                               String lastActivityDate,
                               String lastServiceDate,
                               String popUpReminder,
                               String bilLat, String bilLng,
                               String bilAddress, String bilCity, String bilState, String bilCountry, String bilPostal,
                               String shpLat, String shpLng,
                               String shpAddress, String shpCity, String shpState, String shpCountry, String shpPostal) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AssignedTo, RequestBody.create(MediaType.parse("text/plain"), assignedTo));
        map.put(Cons.KEY_AccountName, RequestBody.create(MediaType.parse("text/plain"), accountName));
        map.put(Cons.KEY_PhoneNo, RequestBody.create(MediaType.parse("text/plain"), phone));
        map.put(Cons.KEY_PreferredTechnician, RequestBody.create(MediaType.parse("text/plain"), preferredTechnician));
        if (!TextUtils.isEmpty(accessNotes)) {
            map.put(Cons.KEY_AccessNotes, RequestBody.create(MediaType.parse("text/plain"), accessNotes));
        }
        if (!TextUtils.isEmpty(notes)) {
            map.put(Cons.KEY_Notes, RequestBody.create(MediaType.parse("text/plain"), notes));
        }
        if (!TextUtils.isEmpty(accountType)) {
            map.put(Cons.KEY_AccountType, RequestBody.create(MediaType.parse("text/plain"), accountType));
        }
        if (!TextUtils.isEmpty(isActive)) {
            map.put(Cons.KEY_IsActive, RequestBody.create(MediaType.parse("text/plain"), isActive));
        }
        if (!TextUtils.isEmpty(lastActivityDate)) {
            map.put(Cons.KEY_LastActivityDate, RequestBody.create(MediaType.parse("text/plain"), lastActivityDate));
        }
        if (!TextUtils.isEmpty(lastServiceDate)) {
            map.put(Cons.KEY_LastServiceDate, RequestBody.create(MediaType.parse("text/plain"), lastServiceDate));
        }
        if (!TextUtils.isEmpty(popUpReminder)) {
            map.put(Cons.KEY_PopupReminder, RequestBody.create(MediaType.parse("text/plain"), popUpReminder));
        }
        map.put(Cons.KEY_BillingAddress, RequestBody.create(MediaType.parse("text/plain"), bilAddress));
        map.put(Cons.KEY_BillingCity, RequestBody.create(MediaType.parse("text/plain"), bilCity));
        map.put(Cons.KEY_BillingState, RequestBody.create(MediaType.parse("text/plain"), bilState));
        map.put(Cons.KEY_BillingCountry, RequestBody.create(MediaType.parse("text/plain"), bilCountry));
        map.put(Cons.KEY_BillingPostalCode, RequestBody.create(MediaType.parse("text/plain"), bilPostal));
        map.put(Cons.KEY_ShippingAddress, RequestBody.create(MediaType.parse("text/plain"), shpAddress));
        map.put(Cons.KEY_ShippingCity, RequestBody.create(MediaType.parse("text/plain"), shpCity));
        map.put(Cons.KEY_ShippingState, RequestBody.create(MediaType.parse("text/plain"), shpState));
        map.put(Cons.KEY_ShippingCountry, RequestBody.create(MediaType.parse("text/plain"), shpCountry));
        map.put(Cons.KEY_ShippingPostalCode, RequestBody.create(MediaType.parse("text/plain"), shpPostal));
        map.put(Cons.KEY_BillingLatitude, RequestBody.create(MediaType.parse("text/plain"), bilLat));
        map.put(Cons.KEY_BillingLongitude, RequestBody.create(MediaType.parse("text/plain"), bilLng));
        map.put(Cons.KEY_ShippingLatitude, RequestBody.create(MediaType.parse("text/plain"), shpLat));
        map.put(Cons.KEY_ShippingLongitude, RequestBody.create(MediaType.parse("text/plain"), shpLng));
        Call<ResponseBody> response;
        response = apiInterface.createAccountDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Acc_Create" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            isCloneMode = false;
                            String strAccountID = object.getString("AccountID");
                            accountId = strAccountID;
                            Utl.showToast(getActivity(), strMessage);
                            getAccountDetailed(strAccountID);
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void editAccount(final String accountId,
                             String assignedTo,
                             String accountName,
                             String primaryContact,
                             String phone,
                             String preferredTechnician,
                             String accessNotes,
                             String notes,
                             String accountType,
                             String isActive,
                             String lastActivityDate,
                             String lastServiceDate,
                             String popUpReminder,
                             String bilLat, String bilLng,
                             String bilAddress, String bilCity, String bilState, String bilCountry, String bilPostal,
                             String shpLat, String shpLng,
                             String shpAddress, String shpCity, String shpState, String shpCountry, String shpPostal) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        map.put(Cons.KEY_AssignedTo, RequestBody.create(MediaType.parse("text/plain"), assignedTo));
        map.put(Cons.KEY_AccountName, RequestBody.create(MediaType.parse("text/plain"), accountName));
        map.put(Cons.KEY_PrimaryContact, RequestBody.create(MediaType.parse("text/plain"), primaryContact));
        map.put(Cons.KEY_PhoneNo, RequestBody.create(MediaType.parse("text/plain"), phone));
        map.put(Cons.KEY_PreferredTechnician, RequestBody.create(MediaType.parse("text/plain"), preferredTechnician));
        if (!TextUtils.isEmpty(accessNotes)) {
            map.put(Cons.KEY_AccessNotes, RequestBody.create(MediaType.parse("text/plain"), accessNotes));
        }
        if (!TextUtils.isEmpty(notes)) {
            map.put(Cons.KEY_Notes, RequestBody.create(MediaType.parse("text/plain"), notes));
        }
        if (!TextUtils.isEmpty(accountType)) {
            map.put(Cons.KEY_AccountType, RequestBody.create(MediaType.parse("text/plain"), accountType));
        }
        if (!TextUtils.isEmpty(isActive)) {
            map.put(Cons.KEY_IsActive, RequestBody.create(MediaType.parse("text/plain"), isActive));
        }
        map.put(Cons.KEY_LastActivityDate, RequestBody.create(MediaType.parse("text/plain"), lastActivityDate));
        map.put(Cons.KEY_LastServiceDate, RequestBody.create(MediaType.parse("text/plain"), lastServiceDate));
        if (!TextUtils.isEmpty(popUpReminder)) {
            map.put(Cons.KEY_PopupReminder, RequestBody.create(MediaType.parse("text/plain"), popUpReminder));
        }
        map.put(Cons.KEY_BillingAddress, RequestBody.create(MediaType.parse("text/plain"), bilAddress));
        map.put(Cons.KEY_BillingCity, RequestBody.create(MediaType.parse("text/plain"), bilCity));
        map.put(Cons.KEY_BillingState, RequestBody.create(MediaType.parse("text/plain"), bilState));
        map.put(Cons.KEY_BillingCountry, RequestBody.create(MediaType.parse("text/plain"), bilCountry));
        map.put(Cons.KEY_BillingPostalCode, RequestBody.create(MediaType.parse("text/plain"), bilPostal));
        map.put(Cons.KEY_ShippingAddress, RequestBody.create(MediaType.parse("text/plain"), shpAddress));
        map.put(Cons.KEY_ShippingCity, RequestBody.create(MediaType.parse("text/plain"), shpCity));
        map.put(Cons.KEY_ShippingState, RequestBody.create(MediaType.parse("text/plain"), shpState));
        map.put(Cons.KEY_ShippingCountry, RequestBody.create(MediaType.parse("text/plain"), shpCountry));
        map.put(Cons.KEY_ShippingPostalCode, RequestBody.create(MediaType.parse("text/plain"), shpPostal));
        map.put(Cons.KEY_BillingLatitude, RequestBody.create(MediaType.parse("text/plain"), bilLat));
        map.put(Cons.KEY_BillingLongitude, RequestBody.create(MediaType.parse("text/plain"), bilLng));
        map.put(Cons.KEY_ShippingLatitude, RequestBody.create(MediaType.parse("text/plain"), shpLat));
        map.put(Cons.KEY_ShippingLongitude, RequestBody.create(MediaType.parse("text/plain"), shpLng));
        Call<ResponseBody> response;
        response = apiInterface.editAccountDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_AccEdit" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            getAccountDetailed(accountId);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountList(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountList(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONObject dataObject = object.getJSONObject("data");
                        JSONObject ContactObject = dataObject.getJSONObject("Contact");
                        JSONObject WorkOrderObject = dataObject.getJSONObject("WorkOrder");
                        JSONObject EstimatesObject = dataObject.getJSONObject("Estimate");
                        JSONObject InvoicesObject = dataObject.getJSONObject("Invoice");
                        JSONObject FilesObject = dataObject.getJSONObject("File");
                        JSONObject EventsObject = dataObject.getJSONObject("Event");
                        JSONObject TasksObject = dataObject.getJSONObject("Task");
                        //JSONObject LocationObject = dataObject.getJSONObject("Location");
                        //JSONObject ChemicalsObject = dataObject.getJSONObject("Chemical");
                        //JSONObject ProductsObject = dataObject.getJSONObject("Product");
                        listRelatedAccountList.add(ContactObject.getString("title"));
                        listRelatedAccountList.add(WorkOrderObject.getString("title"));
                        listRelatedAccountList.add(EstimatesObject.getString("title"));
                        listRelatedAccountList.add(InvoicesObject.getString("title"));
                        listRelatedAccountList.add(FilesObject.getString("title"));
                        listRelatedAccountList.add(EventsObject.getString("title"));
                        listRelatedAccountList.add(TasksObject.getString("title"));
                        //listRelatedAccountList.add(LocationObject.getString("title"));
                        //listRelatedAccountList.add(ChemicalsObject.getString("title"));
                        //listRelatedAccountList.add(ProductsObject.getString("title"));
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountContacts(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountContacts(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelContact model = new ModelContact();
                            model.setContactID(dataObject.getString(Cons.KEY_ContactID));
                            model.setFirstName(dataObject.getString(Cons.KEY_FirstName));
                            model.setLastName(dataObject.getString(Cons.KEY_LastName));
                            model.setTitle(dataObject.getString(Cons.KEY_Title));
                            model.setPhoneNo(dataObject.getString(Cons.KEY_PhoneNo));
                            model.setEmail(dataObject.getString(Cons.KEY_Email));
                            listRelatedAccountContacts.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountWorkOrders(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountWorkOrders(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelWorkOrder model = new ModelWorkOrder();
                            model.setWorkOrderID(dataObject.getString(Cons.KEY_WorkOrderID));
                            model.setWorkOrderNo(dataObject.getString(Cons.KEY_WorkOrderNo));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            model.setPriority(dataObject.getString(Cons.KEY_Priority));
                            model.setStatus(dataObject.getString(Cons.KEY_Status));
                            model.setCategoryName(dataObject.getString(Cons.KEY_CategoryName));
                            listRelatedAccountWorkOrders.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountEstimates(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountEstimates(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelEstimate model = new ModelEstimate();
                            model.setCreatedDate(dataObject.getString(Cons.KEY_CreatedDate));
                            model.setEstimateID(dataObject.getString(Cons.KEY_EstimateID));
                            model.setEstimateNo(dataObject.getString(Cons.KEY_EstimateNo));
                            model.setEstimateName(dataObject.getString(Cons.KEY_EstimateName));
                            model.setExpirationDate(dataObject.getString(Cons.KEY_ExpirationDate));
                            model.setGrandTotal(dataObject.getString(Cons.KEY_GrandTotal));
                            model.setOwnerName(dataObject.getString(Cons.KEY_OwnerName));
                            model.setStatus(dataObject.getString(Cons.KEY_Status));
                            listRelatedAccountEstimates.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountInvoices(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountInvoices(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelInvoice model = new ModelInvoice();
                            model.setDueDate(dataObject.getString(Cons.KEY_DueDate));
                            model.setInvoiceNumber(dataObject.getString(Cons.KEY_InvoiceNumber));
                            model.setInvoiceStatus(dataObject.getString(Cons.KEY_InvoiceStatus));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            model.setSubTotal(dataObject.getString(Cons.KEY_SubTotal));
                            model.setTotalPrice(dataObject.getString(Cons.KEY_TotalPrice));
                            listRelatedAccountInvoices.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountFiles(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountFiles(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelFile model = new ModelFile();
                            model.setContentType(dataObject.getString(Cons.KEY_ContentType));
                            model.setCreatedBy(dataObject.getString(Cons.KEY_CreatedBy));
                            model.setCreatedDate(dataObject.getString(Cons.KEY_CreatedDate));
                            model.setFileName(dataObject.getString(Cons.KEY_FileName));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            listRelatedAccountFiles.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountEvents(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountEvents(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelEvent model = new ModelEvent();
                            model.setAssignedTo(dataObject.getString(Cons.KEY_AssignedTo));
                            model.setCreatedBy(dataObject.getString(Cons.KEY_CreatedBy));
                            model.setCreatedDate(dataObject.getString(Cons.KEY_CreatedDate));
                            model.setEventID(dataObject.getString(Cons.KEY_EventID));
                            model.setEventEndDate(dataObject.getString(Cons.KEY_EventEndDate));
                            model.setEventStartDate(dataObject.getString(Cons.KEY_EventStartDate));
                            model.setName(dataObject.getString(Cons.KEY_Name));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            listRelatedAccountEvents.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountTasks(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountTasks(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelTask model = new ModelTask();
                            model.setAssignedTo(dataObject.getString(Cons.KEY_AssignedTo));
                            model.setDate(dataObject.getString(Cons.KEY_Date));
                            model.setName(dataObject.getString(Cons.KEY_Name));
                            model.setPriority(dataObject.getString(Cons.KEY_Priority));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            model.setTaskType(dataObject.getString(Cons.KEY_TaskType));
                            model.setTaskStatus(dataObject.getString(Cons.KEY_TaskStatus));
                            listRelatedAccountTasks.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    /*
    private void getRelatedAccountLocations(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountLocation(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelLocation model = new ModelLocation();
                            model.setAddress(dataObject.getString(Cons.KEY_Address));
                            model.setCity(dataObject.getString(Cons.KEY_City));
                            model.setLocationID(dataObject.getString(Cons.KEY_LocationID));
                            model.setName(dataObject.getString(Cons.KEY_Name));
                            model.setState(dataObject.getString(Cons.KEY_State));
                            listRelatedAccountLocations.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountChemicals(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountChemicals(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelChemical model = new ModelChemical();
                            model.setAccount(dataObject.getString(Cons.KEY_Account));
                            model.setCreatedBy(dataObject.getString(Cons.KEY_CreatedBy));
                            model.setCreatedDate(dataObject.getString(Cons.KEY_CreatedDate));
                            model.setProductName(dataObject.getString(Cons.KEY_ProductName));
                            model.setChemicalNo(dataObject.getString(Cons.KEY_ChemicalNo));
                            model.setWorkOrder(dataObject.getString(Cons.KEY_WorkOrder));
                            listRelatedAccountChemicals.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountProducts(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountProducts(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelProduct model = new ModelProduct();
                            model.setCreatedBy(dataObject.getString(Cons.KEY_CreatedBy));
                            model.setCreatedDate(dataObject.getString(Cons.KEY_CreatedDate));
                            model.setDatePurchased(dataObject.getString(Cons.KEY_DatePurchased));
                            model.setDefaultQuantity(dataObject.getString(Cons.KEY_DefaultQuantity));
                            model.setListPrice(dataObject.getString(Cons.KEY_ListPrice));
                            model.setProductCode(dataObject.getString(Cons.KEY_ProductCode));
                            model.setProductName(dataObject.getString(Cons.KEY_ProductName));
                            listRelatedAccountProducts.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }
    */

    private void deleteObject() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), "Account"));
        map.put(Cons.KEY_What, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.deleteObject(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_File_Create" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            clickGoToRecent.callbackGoToRecentAccount(getString(R.string.account));
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });

    }

    public boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), CAMERA);
        int result2 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION);
        int result3 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_COARSE_LOCATION);
        int result4 = ContextCompat.checkSelfPermission(getActivity(), CALL_PHONE);
        int result5 = ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE);
        int result6 = ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE);
        return
                result1 == PackageManager.PERMISSION_GRANTED
                        && result2 == PackageManager.PERMISSION_GRANTED
                        && result3 == PackageManager.PERMISSION_GRANTED
                        && result4 == PackageManager.PERMISSION_GRANTED
                        && result5 == PackageManager.PERMISSION_GRANTED
                        && result6 == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{
                        CAMERA,
                        ACCESS_FINE_LOCATION,
                        ACCESS_COARSE_LOCATION,
                        CALL_PHONE,
                        READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean locationFineAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean locationCoarseAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean callAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean readAccepted = grantResults[4] == PackageManager.PERMISSION_GRANTED;
                    boolean writeAccepted = grantResults[5] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted &&
                            locationFineAccepted &&
                            locationCoarseAccepted &&
                            callAccepted &&
                            readAccepted &&
                            writeAccepted)
                        Utl.showToast(getActivity(), "Permission Granted.");
                    else {
                        Utl.showToast(getActivity(), "Permission Denied.");
                    }
                }
                break;
        }
    }

    @Override
    public void callbackAccountRelatedWorkOrderOpen(String whoName, String workOrderID, String workOrderNo) {
        if (whoName.equals(getString(R.string.work_order))) {
            if (clickAccountRelatedItemWorkOrderOpen != null) {
                clickAccountRelatedItemWorkOrderOpen.callbackAccountRelatedWorkOrderOpen(workOrderID, workOrderNo);
            }
        } else if (whoName.equals(getString(R.string.event))) {
            getEventDetails(workOrderID);
        }
    }

    @Override
    public void callbackViewAllListen(String str) {
        if (click != null) {
            click.callbackAccountViewAllListen(str, accountId);
        }
    }

    @Override
    public void callbackAddItemListen(String str) {

    }

    private void getEventDetails(String whatID) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_EventID, RequestBody.create(MediaType.parse("text/plain"), whatID));
        Call<ResponseBody> response = apiInterface.getEventDetails(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONObject dataObject = object.getJSONObject("data");

                        String subject = dataObject.getString(Cons.KEY_Subject);
                        String assignedTo = dataObject.getString(Cons.KEY_AssignedToName);
                        String start = dataObject.getString(Cons.KEY_EventStartDate);
                        String end = dataObject.getString(Cons.KEY_EventEndDate);
                        String isAllDay = dataObject.getString(Cons.KEY_IsAllDayEvent);
                        String description = dataObject.getString(Cons.KEY_Description);
                        String type = dataObject.getString(Cons.KEY_EventTypeName);
                        String contact = dataObject.getString(Cons.KEY_ContactName);
                        String relatedTo = dataObject.getString(Cons.KEY_RelatedToName);
                        String email = dataObject.getString(Cons.KEY_Email);
                        String phone = dataObject.getString(Cons.KEY_PhoneNo);
                        String priority = dataObject.getString(Cons.KEY_EventPriority);
                        String createdBy = dataObject.getString(Cons.KEY_CreatedBy);
                        String createdDate = dataObject.getString(Cons.KEY_CreatedDate);
                        String lastModifiedBy = dataObject.getString(Cons.KEY_LastModifiedBy);
                        String lateModifiedDate = dataObject.getString(Cons.KEY_LastModifiedDate);

                        final Dialog dialogDetails = new Dialog(getActivity(), R.style.DialogFullScreen);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        lp.copyFrom(dialogDetails.getWindow().getAttributes());
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.gravity = Gravity.BOTTOM;
                        dialogDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialogDetails.setCancelable(true);
                        dialogDetails.setCanceledOnTouchOutside(true);
                        dialogDetails.setContentView(R.layout.dialog_event_details);

                        LinearLayout layoutViewEvent = (LinearLayout) dialogDetails.findViewById(R.id.layout_view_event);
                        TextView textviewSubject = (TextView) dialogDetails.findViewById(R.id.textview_subject);
                        TextView textviewAssignedTo = (TextView) dialogDetails.findViewById(R.id.textview_assigned_to);
                        TextView textviewStart = (TextView) dialogDetails.findViewById(R.id.textview_start);
                        TextView textviewEnd = (TextView) dialogDetails.findViewById(R.id.textview_end);
                        CheckBox checkBoxIsAllDayEvent = (CheckBox) dialogDetails.findViewById(R.id.checkbox_is_all_day_event);
                        TextView textviewDescription = (TextView) dialogDetails.findViewById(R.id.textview_description);
                        TextView textviewType = (TextView) dialogDetails.findViewById(R.id.textview_type);
                        TextView textviewContact = (TextView) dialogDetails.findViewById(R.id.textview_contact);
                        TextView textviewRelatedTo = (TextView) dialogDetails.findViewById(R.id.textview_related_to);
                        TextView textviewEmail = (TextView) dialogDetails.findViewById(R.id.textview_email);
                        TextView textviewPhone = (TextView) dialogDetails.findViewById(R.id.textview_phone);
                        TextView textviewPriority = (TextView) dialogDetails.findViewById(R.id.textview_priority);
                        TextView textviewCreatedDate = (TextView) dialogDetails.findViewById(R.id.textview_created_date);
                        TextView textviewCreatedBy = (TextView) dialogDetails.findViewById(R.id.textview_created_by);
                        TextView textviewLastModifiedDate = (TextView) dialogDetails.findViewById(R.id.textview_last_modified_date);
                        TextView textviewLastModifiedBy = (TextView) dialogDetails.findViewById(R.id.textview_last_modified_by);

                        LinearLayout layoutEditEvent = (LinearLayout) dialogDetails.findViewById(R.id.layout_edit_event);
                        layoutEditEvent.setVisibility(View.GONE);
                        Spinner spinnerRelatedTo = (Spinner) dialogDetails.findViewById(R.id.spinner_related_to);
                        Spinner spinnerWhat = (Spinner) dialogDetails.findViewById(R.id.spinner_what);
                        Spinner spinnerType = (Spinner) dialogDetails.findViewById(R.id.spinner_type);
                        Spinner spinnerPriority = (Spinner) dialogDetails.findViewById(R.id.spinner_priority);
                        EditText editTextSubject = (EditText) dialogDetails.findViewById(R.id.edittext_subject);
                        EditText edittextAssignedTo = (EditText) dialogDetails.findViewById(R.id.edittext_assigned_to);
                        EditText edittextContact = (EditText) dialogDetails.findViewById(R.id.edittext_contact);
                        CheckBox checkboxIsAllDay = (CheckBox) dialogDetails.findViewById(R.id.checkbox_is_all_day_event);
                        EditText edittextEmail = (EditText) dialogDetails.findViewById(R.id.edittext_email);
                        EditText edittextPhone = (EditText) dialogDetails.findViewById(R.id.edittext_phone);
                        EditText edittextDescription = (EditText) dialogDetails.findViewById(R.id.edittext_description);

                        TextView edittextStartDateTime = (TextView) dialogDetails.findViewById(R.id.edittext_start_date_time);
                        TextView edittextEndDateTime = (TextView) dialogDetails.findViewById(R.id.edittext_end_date_time);

                        textviewSubject.setText(subject);
                        textviewAssignedTo.setText(assignedTo);
                        textviewStart.setText(start);
                        textviewEnd.setText(end);
                        if (isAllDay.equals("1")) {
                            checkBoxIsAllDayEvent.setChecked(true);
                        } else {
                            checkBoxIsAllDayEvent.setChecked(false);
                        }
                        textviewDescription.setText(description);
                        textviewType.setText(type);
                        textviewContact.setText(contact);
                        textviewRelatedTo.setText(relatedTo);
                        textviewEmail.setText(email);
                        textviewPhone.setText(phone);
                        textviewPriority.setText(priority);
                        textviewCreatedDate.setText(createdDate);
                        textviewCreatedBy.setText(createdBy);
                        textviewLastModifiedBy.setText(lastModifiedBy);
                        textviewLastModifiedDate.setText(lateModifiedDate);

                        Button buttonEdit = (Button) dialogDetails.findViewById(R.id.button_details_edit);
                        LinearLayout layoutEdit = (LinearLayout) dialogDetails.findViewById(R.id.layout_save_cancel);
                        Button buttonSave = (Button) dialogDetails.findViewById(R.id.button_details_save);
                        Button buttonCancel = (Button) dialogDetails.findViewById(R.id.button_details_cancel);
                        buttonEdit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                buttonEdit.setVisibility(View.GONE);
                            }
                        });
                        buttonCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogDetails.dismiss();
                            }
                        });
                        buttonSave.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogDetails.dismiss();
                            }
                        });
                        dialogDetails.show();
                        dialogDetails.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                        dialogDetails.getWindow().setDimAmount(0.5f);
                        dialogDetails.getWindow().setAttributes(lp);
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

}