package com.fieldwise.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Spinner

import com.fieldwise.R
import com.fieldwise.utils.APIClient
import com.fieldwise.utils.APIInterface
import com.fieldwise.utils.Cons
import com.fieldwise.utils.Utl

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

import java.io.IOException
import java.lang.reflect.Field
import java.util.ArrayList
import java.util.HashMap

import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentContactFilterKT : Fragment() {

    var apiInterface: APIInterface = APIClient.getClient().create(APIInterface::class.java)

    private var contactView: String? = null

    private var buttonCancel: Button? = null
    private var buttonApply: Button? = null
    private var buttonAddFilter: Button? = null
    private var buttonClearAll: Button? = null

    private var layoutAddFilter: LinearLayout? = null

    private val strFilterConditions = arrayOf("Equals", "Contains", "StartsWith", "DoesNotContain", "NotEqualTo", "LessThan", "GreaterThan", "LessOREqualTo", "GreaterOREqualTo")
    private var adapterFilterConditions: ArrayAdapter<*>? = null

    private var listFilterFields: MutableList<String>? = null
    private var adapterFilterFields: ArrayAdapter<*>? = null

    var clickFilterCancel: ClickListenContactFilterCancel? = null

    var clickFilterAdd: ClickListenContactFilterAdd? = null

    interface ClickListenContactFilterCancel {
        fun callbackContactFilterCancel()
    }

    interface ClickListenContactFilterAdd {
        fun callbackContactFilterAdd(contactView: String?, sbFields: String, sbConditions: String, sbValues: String)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_contact_filter, container, false)

        clickFilterCancel = activity as ClickListenContactFilterCancel?
        clickFilterAdd = activity as ClickListenContactFilterAdd?

        val bundle = this.arguments
        if (bundle != null) {
            contactView = bundle.getString(Cons.KEY_ContactViewID, "")
        }

        buttonCancel = view.findViewById<View>(R.id.button_cancel) as Button
        buttonApply = view.findViewById<View>(R.id.button_apply) as Button
        buttonAddFilter = view.findViewById<View>(R.id.button_add_filter) as Button
        buttonClearAll = view.findViewById<View>(R.id.button_clear_all) as Button

        layoutAddFilter = view.findViewById<View>(R.id.layout_add_filter) as LinearLayout

        adapterFilterConditions = ArrayAdapter(activity!!, android.R.layout.simple_spinner_dropdown_item, strFilterConditions)
        adapterFilterConditions!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        listFilterFields = ArrayList()
        adapterFilterFields = ArrayAdapter(activity!!, android.R.layout.simple_spinner_dropdown_item, listFilterFields!!)
        adapterFilterFields!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        getFields()

        buttonCancel!!.setOnClickListener {
            if (clickFilterCancel != null) {
                clickFilterCancel!!.callbackContactFilterCancel()
            }
        }

        buttonApply!!.setOnClickListener {
            if (clickFilterAdd != null) {
                val sbFields = StringBuilder()
                val sbConditions = StringBuilder()
                val sbValues = StringBuilder()
                val size = layoutAddFilter!!.childCount
                for (i in 0 until size) {
                    val v = layoutAddFilter!!.getChildAt(i)
                    val spinnerFilterFields = v.findViewById<View>(R.id.spinner_filter_fields) as Spinner
                    val spinnerFilterConditions = v.findViewById<View>(R.id.spinner_filter_conditions) as Spinner
                    val edittextFilterValues = v.findViewById<View>(R.id.edittext_filter_values) as EditText
                    sbFields.append(spinnerFilterFields.selectedItem.toString() + ",")
                    sbConditions.append(spinnerFilterConditions.selectedItem.toString() + ",")
                    sbValues.append(edittextFilterValues.text.toString().trim { it <= ' ' } + ",")
                }
                var strFields = sbFields.toString()
                var strConditions = sbConditions.toString()
                var strValues = sbValues.toString()
                if (strFields.endsWith(",")) {
                    strFields = strFields.substring(0, strFields.length - 1)
                }
                if (strConditions.endsWith(",")) {
                    strConditions = strConditions.substring(0, strConditions.length - 1)
                }
                if (strValues.endsWith(",")) {
                    strValues = strValues.substring(0, strValues.length - 1)
                }
                clickFilterAdd!!.callbackContactFilterAdd(contactView, strFields, strConditions, strValues)
            }
        }

        buttonAddFilter!!.setOnClickListener {
            val layoutInflater = activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val v = layoutInflater.inflate(R.layout.layout_filter, null)

            layoutAddFilter!!.addView(v)

            val size = layoutAddFilter!!.childCount
            val viw = layoutAddFilter!!.getChildAt(size - 1)

            val spinnerFilterFields = viw.findViewById<View>(R.id.spinner_filter_fields) as Spinner
            setSpinnerDropDownHeight(listFilterFields!!, spinnerFilterFields)
            spinnerFilterFields.adapter = adapterFilterFields

            val spinnerFilterConditions = viw.findViewById<View>(R.id.spinner_filter_conditions) as Spinner
            setSimpleSpinnerDropDownHeight(strFilterConditions, spinnerFilterConditions)
            spinnerFilterConditions.adapter = adapterFilterConditions

            val edittextFilterValues = viw.findViewById<View>(R.id.edittext_filter_values) as EditText

            val btnCancel = viw.findViewById<View>(R.id.btn_cancel) as Button
            btnCancel.setOnClickListener { layoutAddFilter!!.removeView(viw) }
        }

        buttonClearAll!!.setOnClickListener { layoutAddFilter!!.removeAllViews() }

        return view
    }

    private fun setSimpleSpinnerDropDownHeight(str: Array<String>, spinner: Spinner) {
        val displayMetrics = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        spinner.dropDownWidth = width - 140
        if (str.size > 4) {
            try {
                val popup = Spinner::class.java.getDeclaredField("mPopup")
                popup.isAccessible = true
                val popupWindow = popup.get(spinner) as android.widget.ListPopupWindow
                popupWindow.height = 800
            } catch (e: NoClassDefFoundError) {
            } catch (e: ClassCastException) {
            } catch (e: NoSuchFieldException) {
            } catch (e: IllegalAccessException) {
            }

        }
    }

    private fun setSpinnerDropDownHeight(list: List<String>, spinner: Spinner) {
        val displayMetrics = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        spinner.dropDownWidth = width - 140
        if (list.size > 4) {
            try {
                val popup = Spinner::class.java.getDeclaredField("mPopup")
                popup.isAccessible = true
                val popupWindow = popup.get(spinner) as android.widget.ListPopupWindow
                popupWindow.height = 800
            } catch (e: NoClassDefFoundError) {
            } catch (e: ClassCastException) {
            } catch (e: NoSuchFieldException) {
            } catch (e: IllegalAccessException) {
            }

        }
    }

    private fun getFields() {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactViewID] = RequestBody.create(MediaType.parse("text/plain"), contactView!!)
        val response = apiInterface.getContactFilter(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_Filter" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            listFilterFields!!.clear()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                listFilterFields!!.add(dataObject.getString(Cons.KEY_FieldName))
                            }
                        }
                    } catch (e: JSONException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

}