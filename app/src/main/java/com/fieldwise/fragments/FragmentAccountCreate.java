package com.fieldwise.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.fieldwise.R;
import com.fieldwise.models.ModelSpinner;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;
import com.schibstedspain.leku.LocationPickerActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class FragmentAccountCreate extends Fragment implements LocationListener {

    private static final int PERMISSION_REQUEST_CODE = 200;

    int back_type;

    public APIInterface apiInterface;

    private String strLat = "40.730610";
    private String strLng = "-73.935242";
    private String provider;
    private LocationManager locationManager;

    private String strLastActivityDate, strNewLastActivityDate,
            strLastServiceDate, strNewLastServiceDate,
            strNewBilLat, strNewBilLng, strNewShpLat, strNewShpLng,
            strNewLat, strNewLng, strNewAddress, strNewCity, strNewState, strNewCountry, strNewPostal;

    private boolean isBilClicked = false;
    private boolean isShpClicked = false;
    private boolean isActivityClicked = false;
    private boolean isServiceClicked = false;

    private RelativeLayout layoutAssignedTo,
            layoutPrimaryContact,
            layoutPreferredTechnician,
            layoutAccountType;

    private Spinner spinnerAssignedTo,
            spinnerPrimaryContact,
            spinnerPreferredTechnician,
            spinnerAccountType;

    private List<ModelSpinner> listAssignedTo,
            listPrimaryContact,
            listPreferredTechnician,
            listAccountType;

    private ArrayAdapter<ModelSpinner> adapterAssignedTo,
            adapterPrimaryContact,
            adapterPreferredTechnician,
            adapterAccountType;

    private EditText edittextAssignedName,
            edittextPhone,
            edittextAccessNotes,
            edittextNotes,
            edittextPopupReminder,
            edittextBilAddress,
            edittextBilCity, edittextBilState, edittextBilCountry, edittextBilPostal,
            edittextShpAddress,
            edittextShpCity, edittextShpState, edittextShpCountry, edittextShpPostal;

    /*private TextView edittextLastActivityDate,
            edittextLastServiceDate;*/

    private CheckBox checkboxIsActive;
    private ImageButton buttonBilAddress, buttonShpAddress;

    private Button buttonSave, buttonCancel;

    public ClickListenAccCreateClose clickClose;

    public interface ClickListenAccCreateClose {
        void callbackAccountCreateClose(int back_type, boolean isSave, String accountID, String accountNo);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_account_create, container, false);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            back_type = bundle.getInt("back_type", 0);
        }

        apiInterface = APIClient.getClient().create(APIInterface.class);

        clickClose = (ClickListenAccCreateClose) getActivity();

        layoutAssignedTo = (RelativeLayout) view.findViewById(R.id.layout_assigned_to);
        layoutPreferredTechnician = (RelativeLayout) view.findViewById(R.id.layout_preferred_technician);
        layoutAccountType = (RelativeLayout) view.findViewById(R.id.layout_account_type);

        spinnerAssignedTo = (Spinner) view.findViewById(R.id.spinner_assigned_to);
        spinnerPreferredTechnician = (Spinner) view.findViewById(R.id.spinner_preferred_technician);
        spinnerAccountType = (Spinner) view.findViewById(R.id.spinner_account_type);

        edittextAssignedName = (EditText) view.findViewById(R.id.edittext_assigned_name);
        edittextPhone = (EditText) view.findViewById(R.id.edittext_phone);
        edittextAccessNotes = (EditText) view.findViewById(R.id.edittext_access_notes);
        edittextNotes = (EditText) view.findViewById(R.id.edittext_notes);
        edittextPopupReminder = (EditText) view.findViewById(R.id.edittext_popup_reminder);
        edittextBilAddress = (EditText) view.findViewById(R.id.edittext_bil_address);
        edittextBilCity = (EditText) view.findViewById(R.id.edittext_bil_city);
        edittextBilState = (EditText) view.findViewById(R.id.edittext_bil_state);
        edittextBilCountry = (EditText) view.findViewById(R.id.edittext_bil_country);
        edittextBilPostal = (EditText) view.findViewById(R.id.edittext_bil_postal);
        edittextShpAddress = (EditText) view.findViewById(R.id.edittext_shp_address);
        edittextShpCity = (EditText) view.findViewById(R.id.edittext_shp_city);
        edittextShpState = (EditText) view.findViewById(R.id.edittext_shp_state);
        edittextShpCountry = (EditText) view.findViewById(R.id.edittext_shp_country);
        edittextShpPostal = (EditText) view.findViewById(R.id.edittext_shp_postal);

        //edittextLastActivityDate = (TextView) view.findViewById(R.id.edittext_last_activity_date);
        //edittextLastServiceDate = (TextView) view.findViewById(R.id.edittext_last_service_date);

        checkboxIsActive = (CheckBox) view.findViewById(R.id.checkbox_is_active);
        buttonBilAddress = (ImageButton) view.findViewById(R.id.button_bil_address);
        buttonShpAddress = (ImageButton) view.findViewById(R.id.button_shp_address);

        buttonSave = (Button) view.findViewById(R.id.button_save);
        buttonCancel = (Button) view.findViewById(R.id.button_cancel);

        strNewBilLat = "";
        strNewBilLng = "";
        strNewShpLat = "";
        strNewShpLng = "";
        strNewLat = "";
        strNewLng = "";
        strNewAddress = "";
        strNewCity = "";
        strNewState = "";
        strNewCountry = "";
        strNewPostal = "";
        strLastActivityDate = "";
        strNewLastActivityDate = "";
        strLastServiceDate = "";
        strNewLastServiceDate = "";

        Calendar calendar = Calendar.getInstance();
        Date date = Calendar.getInstance().getTime();
        calendar.setTime(date);
        int day = calendar.get(Calendar.DATE);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);
        calendar.set(year, month, day, hour, minute);
        String AM_PM = (String) android.text.format.DateFormat.format("aaa", calendar);
        if (hour > 12) {
            hour = hour - 12;
        }
        strLastActivityDate = selectDate(day, month, year);
        //edittextLastActivityDate.setText(formatDate(strLastActivityDate));
        strLastServiceDate = selectDate(day, month, year);
        //edittextLastServiceDate.setText(formatDate(strLastServiceDate));

        listAssignedTo = new ArrayList<>();
        listPreferredTechnician = new ArrayList<>();
        listAccountType = new ArrayList<>();
        adapterAssignedTo = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listAssignedTo);
        adapterAssignedTo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterPreferredTechnician = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listPreferredTechnician);
        adapterPreferredTechnician.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterAccountType = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listAccountType);
        adapterAccountType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        getAllUser();
        getAccountType();

        /*
        edittextLastActivityDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                        isActivityClicked = true;
                        edittextLastActivityDate.setText(selectDate(d, m, y));
                        strNewLastActivityDate = selectDateEdit(d, m, y);
                        Log.d("TAG_newActDate", strNewLastActivityDate);
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        edittextLastServiceDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                        isServiceClicked = true;
                        edittextLastServiceDate.setText(selectDate(d, m, y));
                        strNewLastServiceDate = selectDateEdit(d, m, y);
                        Log.d("TAG_newSerDate", strNewLastServiceDate);
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });
        */

        buttonBilAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!checkPermission()) {
                        requestPermission();
                    } else {
                        isBilClicked = true;
                        isShpClicked = false;
                        double lat = Double.valueOf(strLat);
                        double lng = Double.valueOf(strLng);
                        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                                .withLocation(lat, lng)
                                .withGeolocApiKey(getString(R.string.google_maps_key))
                                .withGooglePlacesEnabled()
                                .withSatelliteViewHidden()
                                .withVoiceSearchHidden()
                                .shouldReturnOkOnBackPressed()
                                .build(getActivity());
                        startActivityForResult(locationPickerIntent, 99);
                    }
                } else {
                    isBilClicked = true;
                    isShpClicked = false;
                    double lat = Double.valueOf(strLat);
                    double lng = Double.valueOf(strLng);
                    Intent locationPickerIntent = new LocationPickerActivity.Builder()
                            .withLocation(lat, lng)
                            .withGeolocApiKey(getString(R.string.google_maps_key))
                            .withGooglePlacesEnabled()
                            .withSatelliteViewHidden()
                            .withVoiceSearchHidden()
                            .shouldReturnOkOnBackPressed()
                            .build(getActivity());
                    startActivityForResult(locationPickerIntent, 99);
                }
            }
        });

        buttonShpAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!checkPermission()) {
                        requestPermission();
                    } else {
                        isBilClicked = false;
                        isShpClicked = true;
                        double lat = Double.valueOf(strLat);
                        double lng = Double.valueOf(strLng);
                        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                                .withLocation(lat, lng)
                                .withGeolocApiKey(getString(R.string.google_maps_key))
                                .withGooglePlacesEnabled()
                                .withSatelliteViewHidden()
                                .withVoiceSearchHidden()
                                .shouldReturnOkOnBackPressed()
                                .build(getActivity());
                        startActivityForResult(locationPickerIntent, 99);
                    }
                } else {
                    isBilClicked = false;
                    isShpClicked = true;
                    double lat = Double.valueOf(strLat);
                    double lng = Double.valueOf(strLng);
                    Intent locationPickerIntent = new LocationPickerActivity.Builder()
                            .withLocation(lat, lng)
                            .withGeolocApiKey(getString(R.string.google_maps_key))
                            .withGooglePlacesEnabled()
                            .withSatelliteViewHidden()
                            .withVoiceSearchHidden()
                            .shouldReturnOkOnBackPressed()
                            .build(getActivity());
                    startActivityForResult(locationPickerIntent, 99);
                }
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String assignedTo = listAssignedTo.get(spinnerAssignedTo.getSelectedItemPosition()).getId();
                String accountName = edittextAssignedName.getText().toString().trim();
                String phone = edittextPhone.getText().toString().trim();
                String preferredTechnician = listPreferredTechnician.get(spinnerPreferredTechnician.getSelectedItemPosition()).getId();
                String accessNotes = edittextAccessNotes.getText().toString().trim();
                String notes = edittextNotes.getText().toString().trim();
                String accountType = listAccountType.get(spinnerAccountType.getSelectedItemPosition()).getId();
                String isActive = null;
                if (checkboxIsActive.isChecked()) {
                    isActive = "1";
                } else {
                    isActive = "0";
                }
                String lastActivityDate = "";
                if (isActivityClicked) {
                    lastActivityDate = strNewLastActivityDate;
                } else {
                    lastActivityDate = strLastActivityDate;
                }
                String lastServiceDate = "";
                if (isServiceClicked) {
                    lastServiceDate = strNewLastServiceDate;
                } else {
                    lastServiceDate = strLastServiceDate;
                }
                String popUpReminder = edittextPopupReminder.getText().toString().trim();
                String bilLat = strNewBilLat;
                String bilLng = strNewBilLng;
                String bilAddress = edittextBilAddress.getText().toString().trim();
                String bilCity = edittextBilCity.getText().toString().trim();
                String bilState = edittextBilState.getText().toString().trim();
                String bilCountry = edittextBilCountry.getText().toString().trim();
                String bilPostal = edittextBilPostal.getText().toString().trim();
                String shpLat = strNewShpLat;
                String shpLng = strNewShpLng;
                String shpAddress = edittextShpAddress.getText().toString().trim();
                String shpCity = edittextShpCity.getText().toString().trim();
                String shpState = edittextShpState.getText().toString().trim();
                String shpCountry = edittextShpCountry.getText().toString().trim();
                String shpPostal = edittextShpPostal.getText().toString().trim();

                if (TextUtils.isEmpty(accountName)) {
                    Utl.showToast(getActivity(), "Enter Account Name");
                } else if (TextUtils.isEmpty(phone)) {
                    Utl.showToast(getActivity(), "Enter Phone No");
                } /*else if (TextUtils.isEmpty(accessNotes)) {
                    Utl.showToast(getActivity(), "Enter Access Notes");
                } else if (TextUtils.isEmpty(notes)) {
                    Utl.showToast(getActivity(), "Enter Notes");
                } else if (TextUtils.isEmpty(lastActivityDate)) {
                    Utl.showToast(getActivity(), "Select Last Activity Date");
                } else if (TextUtils.isEmpty(lastServiceDate)) {
                    Utl.showToast(getActivity(), "Select Last Service Date");
                } else if (TextUtils.isEmpty(popUpReminder)) {
                    Utl.showToast(getActivity(), "Enter Popup Reminder");
                }*/ else if (TextUtils.isEmpty(bilAddress)) {
                    Utl.showToast(getActivity(), "Enter Billing Address");
                } else if (TextUtils.isEmpty(bilCity)) {
                    Utl.showToast(getActivity(), "Enter Billing City");
                } else if (TextUtils.isEmpty(bilState)) {
                    Utl.showToast(getActivity(), "Enter Billing State");
                } else if (TextUtils.isEmpty(bilCountry)) {
                    Utl.showToast(getActivity(), "Enter Billing Country");
                } else if (TextUtils.isEmpty(bilPostal)) {
                    Utl.showToast(getActivity(), "Enter Billing Postal Code");
                } else if (TextUtils.isEmpty(shpAddress)) {
                    Utl.showToast(getActivity(), "Enter Shipping Address");
                } else if (TextUtils.isEmpty(shpCity)) {
                    Utl.showToast(getActivity(), "Enter Shipping City");
                } else if (TextUtils.isEmpty(shpState)) {
                    Utl.showToast(getActivity(), "Enter Shipping State");
                } else if (TextUtils.isEmpty(shpCountry)) {
                    Utl.showToast(getActivity(), "Enter Shipping Country");
                } else if (TextUtils.isEmpty(shpPostal)) {
                    Utl.showToast(getActivity(), "Enter Shipping Postal Code");
                } else {
                    createAccount(assignedTo,
                            accountName,
                            phone,
                            preferredTechnician,
                            accessNotes,
                            notes,
                            accountType,
                            isActive,
                            lastActivityDate,
                            lastServiceDate,
                            popUpReminder,
                            bilLat, bilLng,
                            bilAddress, bilCity, bilState, bilCountry, bilPostal,
                            shpLat, shpLng,
                            shpAddress, shpCity, shpState, shpCountry, shpPostal);
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickClose.callbackAccountCreateClose(back_type, false, "", "");
            }
        });

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(provider);
        if (location != null) {
            Log.d("Provider: ", provider + " has been selected.");
            onLocationChanged(location);
        }

        return view;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99) {
            if (data != null) {
                double latitude = data.getDoubleExtra("latitude", 0.0);
                strNewLat = String.valueOf(latitude);
                double longitude = data.getDoubleExtra("longitude", 0.0);
                strNewLng = String.valueOf(longitude);
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> addresses = new ArrayList<>();
                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                strNewAddress = addresses.get(0).getSubThoroughfare() + ", " + addresses.get(0).getThoroughfare();
                if (addresses.get(0).getLocality() != null) {
                    strNewCity = addresses.get(0).getLocality();
                } else {
                    strNewCity = addresses.get(0).getSubLocality();
                }
                if (addresses.get(0).getAdminArea() != null) {
                    strNewState = addresses.get(0).getAdminArea();
                } else {
                    strNewState = addresses.get(0).getSubAdminArea();
                }
                strNewCountry = addresses.get(0).getCountryName();
                strNewPostal = addresses.get(0).getPostalCode();
                Log.d("TAG_LATITUDE****", strNewLat);
                Log.d("TAG_LONGITUDE****", strNewLng);
                Log.d("TAG_ADDRESS****", strNewAddress);
                Log.d("TAG_CITY****", strNewCity);
                Log.d("TAG_STATE****", strNewState);
                Log.d("TAG_COUNTRY****", strNewCountry);
                Log.d("TAG_POSTAL****", strNewPostal);
                if (isBilClicked) {
                    strNewBilLat = strNewLat;
                    strNewBilLng = strNewLng;
                    edittextBilAddress.setText(strNewAddress);
                    edittextBilCity.setText(strNewCity);
                    edittextBilState.setText(strNewState);
                    edittextBilCountry.setText(strNewCountry);
                    edittextBilPostal.setText(strNewPostal);
                } else if (isShpClicked) {
                    strNewShpLat = strNewLat;
                    strNewShpLng = strNewLng;
                    edittextShpAddress.setText(strNewAddress);
                    edittextShpCity.setText(strNewCity);
                    edittextShpState.setText(strNewState);
                    edittextShpCountry.setText(strNewCountry);
                    edittextShpPostal.setText(strNewPostal);
                }
            } else {
                Log.d("RESULT", "CANCELLED");
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        strLat = String.valueOf(location.getLatitude());
        strLng = String.valueOf(location.getLongitude());
        Log.d("TAG_LAT*****", strLat);
        Log.d("TAG_LNG*****", strLng);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
        Toast.makeText(getActivity(), "Enabled New Provider " + provider, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String s) {
        Toast.makeText(getActivity(), "Disabled New Provider " + provider, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    private void setSpinnerDropDownHeight(List<ModelSpinner> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 70);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    public int getListIndex(List<ModelSpinner> list, String name) {
        for (int i = 0; i < list.size(); i++) {
            ModelSpinner model = list.get(i);
            if (name.equals(model.getName())) {
                return i;
            }
        }
        return -1;
    }

    public String formatDate(String time) {
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String selectDate(int d, int m, int y) {
        String time = "" + (m + 1) + "/" + d + "/" + y;
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String selectDateEdit(int d, int m, int y) {
        String time = "" + (m + 1) + "/" + d + "/" + y;
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void getAllUser() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getAccAllUsers(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_AllUser" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAssignedTo.clear();
                            listPreferredTechnician.clear();
                            listPreferredTechnician.add(new ModelSpinner("0", "Select Technician"));
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_UserID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listAssignedTo.add(model);
                                listPreferredTechnician.add(model);
                            }
                            setSpinnerDropDownHeight(listAssignedTo, spinnerAssignedTo);
                            setSpinnerDropDownHeight(listPreferredTechnician, spinnerPreferredTechnician);
                            spinnerAssignedTo.setAdapter(adapterAssignedTo);
                            spinnerAssignedTo.setSelection(0);
                            spinnerPreferredTechnician.setAdapter(adapterPreferredTechnician);
                            spinnerPreferredTechnician.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getAccountType() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getAccAccountType(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_AccountType" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAccountType.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_AccountTypeID));
                                model.setName(dataObject.getString(Cons.KEY_AccountType));
                                listAccountType.add(model);
                            }
                            setSpinnerDropDownHeight(listAccountType, spinnerAccountType);
                            spinnerAccountType.setAdapter(adapterAccountType);
                            spinnerAccountType.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void createAccount(String assignedTo,
                               String accountName,
                               String phone,
                               String preferredTechnician,
                               String accessNotes,
                               String notes,
                               String accountType,
                               String isActive,
                               String lastActivityDate,
                               String lastServiceDate,
                               String popUpReminder,
                               String bilLat, String bilLng,
                               String bilAddress, String bilCity, String bilState, String bilCountry, String bilPostal,
                               String shpLat, String shpLng,
                               String shpAddress, String shpCity, String shpState, String shpCountry, String shpPostal) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AssignedTo, RequestBody.create(MediaType.parse("text/plain"), assignedTo));
        map.put(Cons.KEY_AccountName, RequestBody.create(MediaType.parse("text/plain"), accountName));
        map.put(Cons.KEY_PhoneNo, RequestBody.create(MediaType.parse("text/plain"), phone));
        if (!preferredTechnician.equals("0")) {
            map.put(Cons.KEY_PreferredTechnician, RequestBody.create(MediaType.parse("text/plain"), preferredTechnician));
        }
        if (!TextUtils.isEmpty(accessNotes)) {
            map.put(Cons.KEY_AccessNotes, RequestBody.create(MediaType.parse("text/plain"), accessNotes));
        }
        if (!TextUtils.isEmpty(notes)) {
            map.put(Cons.KEY_Notes, RequestBody.create(MediaType.parse("text/plain"), notes));
        }
        if (!TextUtils.isEmpty(accountType)) {
            map.put(Cons.KEY_AccountType, RequestBody.create(MediaType.parse("text/plain"), accountType));
        }
        if (!TextUtils.isEmpty(isActive)) {
            map.put(Cons.KEY_IsActive, RequestBody.create(MediaType.parse("text/plain"), isActive));
        }
        //map.put(Cons.KEY_LastActivityDate, RequestBody.create(MediaType.parse("text/plain"), lastActivityDate));
        //map.put(Cons.KEY_LastServiceDate, RequestBody.create(MediaType.parse("text/plain"), lastServiceDate));
        if (!TextUtils.isEmpty(popUpReminder)) {
            map.put(Cons.KEY_PopupReminder, RequestBody.create(MediaType.parse("text/plain"), popUpReminder));
        }
        map.put(Cons.KEY_BillingAddress, RequestBody.create(MediaType.parse("text/plain"), bilAddress));
        map.put(Cons.KEY_BillingCity, RequestBody.create(MediaType.parse("text/plain"), bilCity));
        map.put(Cons.KEY_BillingState, RequestBody.create(MediaType.parse("text/plain"), bilState));
        map.put(Cons.KEY_BillingCountry, RequestBody.create(MediaType.parse("text/plain"), bilCountry));
        map.put(Cons.KEY_BillingPostalCode, RequestBody.create(MediaType.parse("text/plain"), bilPostal));
        map.put(Cons.KEY_ShippingAddress, RequestBody.create(MediaType.parse("text/plain"), shpAddress));
        map.put(Cons.KEY_ShippingCity, RequestBody.create(MediaType.parse("text/plain"), shpCity));
        map.put(Cons.KEY_ShippingState, RequestBody.create(MediaType.parse("text/plain"), shpState));
        map.put(Cons.KEY_ShippingCountry, RequestBody.create(MediaType.parse("text/plain"), shpCountry));
        map.put(Cons.KEY_ShippingPostalCode, RequestBody.create(MediaType.parse("text/plain"), shpPostal));
        map.put(Cons.KEY_BillingLatitude, RequestBody.create(MediaType.parse("text/plain"), bilLat));
        map.put(Cons.KEY_BillingLongitude, RequestBody.create(MediaType.parse("text/plain"), bilLng));
        map.put(Cons.KEY_ShippingLatitude, RequestBody.create(MediaType.parse("text/plain"), shpLat));
        map.put(Cons.KEY_ShippingLongitude, RequestBody.create(MediaType.parse("text/plain"), shpLng));
        Call<ResponseBody> response;
        response = apiInterface.createAccountDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Acc_Create" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            String accountID = object.getString(Cons.KEY_AccountID);
                            String accountNo = object.getString(Cons.KEY_AccountNo);
                            clickClose.callbackAccountCreateClose(back_type, true, accountID, accountNo);
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    public boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), CAMERA);
        int result2 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION);
        int result3 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_COARSE_LOCATION);
        int result4 = ContextCompat.checkSelfPermission(getActivity(), CALL_PHONE);
        int result5 = ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE);
        int result6 = ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE);
        return
                result1 == PackageManager.PERMISSION_GRANTED
                        && result2 == PackageManager.PERMISSION_GRANTED
                        && result3 == PackageManager.PERMISSION_GRANTED
                        && result4 == PackageManager.PERMISSION_GRANTED
                        && result5 == PackageManager.PERMISSION_GRANTED
                        && result6 == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{
                        CAMERA,
                        ACCESS_FINE_LOCATION,
                        ACCESS_COARSE_LOCATION,
                        CALL_PHONE,
                        READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean locationFineAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean locationCoarseAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean callAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean readAccepted = grantResults[4] == PackageManager.PERMISSION_GRANTED;
                    boolean writeAccepted = grantResults[5] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted &&
                            locationFineAccepted &&
                            locationCoarseAccepted &&
                            callAccepted &&
                            readAccepted &&
                            writeAccepted)
                        Utl.showToast(getActivity(), "Permission Granted.");
                    else {
                        Utl.showToast(getActivity(), "Permission Denied.");
                    }
                }
                break;
        }
    }

}