package com.fieldwise.fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.fieldwise.R;
import com.fieldwise.activities.ActivityMain;
import com.fieldwise.adapters.AdapterAccount;
import com.fieldwise.models.ModelSpinner;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;
import com.mindorks.placeholderview.PlaceHolderView;
import com.schibstedspain.leku.LocationPickerActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class FragmentWorkOrderCreate extends Fragment implements LocationListener,
        AdapterAccount.AddAccountClickListen {

    private static final int PERMISSION_REQUEST_CODE = 200;

    int back_type;

    public APIInterface apiInterface;

    private String strLat = "40.730610";
    private String strLng = "-73.935242";
    private String provider;
    private LocationManager locationManager;

    private String strAccount, strAccountName,
            strPrimaryContact, strPrimaryContactName,
            strParentWorkOrder, strParentWorkOrderName,
            strStartDateTimeDate, strNewStartDateTimeDate, strStartDateTimeTime, strNewStartDateTimeTime,
            strEndDateTimeDate, strNewEndDateTimeDate, strEndDateTimeTime, strNewEndDateTimeTime,
            strRecurring, strStartOn, strStartOnNew,
            strNewBilLat, strNewBilLng,
            strNewLat, strNewLng, strNewAddress, strNewCity, strNewState, strNewCountry, strNewPostal,
            strRepeatEvery, strNewRepeatEvery,
            strIntervalEvery, strNewIntervalEvery,
            strSun, strMon, strTue, strWed, strThu, strFri, strSat,
            strEnds, strEndsOn, strEndsOnNew, strEndsAfter,
            strRecurrenceStartTime, strRecurrenceNewStartTime, strRecurrenceEndTime, strRecurrenceNewEndTime;

    private boolean isAccountClicked = false;
    private boolean isContactClicked = false;
    private boolean isWorkOrderClicked = false;
    private boolean isBilClicked = false;
    private boolean isStartDateOnClicked = false;
    private boolean isStartDateClicked = false;
    private boolean isEndDateClicked = false;
    private boolean isRecurrenceStartDateClicked = false;
    private boolean isRecurrenceEndDateClicked = false;
    private boolean isRepeatEveryClicked = false;
    private boolean isIntervalClicked = false;
    private boolean isEndDateOnClicked = false;

    private Spinner spinnerAssignedTo,
            spinnerType,
            spinnerStatus,
            spinnerPriority,
            spinnerCategory;

    private List<ModelSpinner> listAssignedTo,
            listAccounts,
            listPrimaryContacts,
            listParentWorkOrders,
            listTypes,
            listStatus,
            listPriorities,
            listCategories;

    private ArrayAdapter<ModelSpinner> adapterAssignedTo,
            adapterPrimaryContact,
            adapterParentWorkOrder,
            adapterType,
            adapterStatus,
            adapterPriority,
            adapterCategory;

    private ImageButton buttonAccount;
    private LinearLayout layoutAccountAdd;
    private EditText edittextAccountAddSearch;
    private PlaceHolderView phvAccountAdd;

    private ImageButton buttonContact;
    private LinearLayout layoutContactAdd;
    private EditText edittextContactAddSearch;
    private PlaceHolderView phvContactAdd;

    private ImageButton buttonWorkOrder;
    private LinearLayout layoutWorkOrderAdd;
    private EditText edittextWorkOrderAddSearch;
    private PlaceHolderView phvWorkOrderAdd;

    private EditText edittextAccount, edittextContact, edittextWorkOrder,
            edittextSubject,
            edittextDescription,
            edittextPopupReminder,
            edittextBilAddress,
            edittextBilCity,
            edittextBilState,
            edittextBilCountry,
            edittextBilPostal;

    private TextView edittextStartDateTime,
            edittextEndDateTime;

    private CheckBox checkboxIsRecurring;
    private ImageButton buttonBilAddress;

    private RelativeLayout layoutRecurrence, layoutEndAfter;
    private LinearLayout layoutRecurrenceRepeatOn;
    private CheckBox checkboxSun, checkboxMon, checkboxTue, checkboxWed, checkboxThu, checkboxFri, checkboxSat;
    private Spinner spinnerRepeatsEvery, spinnerIntervalEvery, spinnerEndsAfter;
    private TextView textviewStartOnDate, textviewRepeatsEveryPostfix, textviewEndsOnDate, textviewEndsAfterPostfix,
            edittextRecurrenceStartTime, edittextRecurrenceEndTime;
    private RadioGroup radioGroupRecurrence;
    private RadioButton radioButtonRecurrenceNever, radioButtonRecurrenceOn, radioButtonRecurrenceAfter;
    private Button buttonRecurrenceSave, buttonRecurrenceClose;

    private List<String> listRecurrenceRepeat, listRecurrenceInterval, listRecurrenceEndAfter;
    private ArrayAdapter<String> adapterRecurrenceRepeat, adapterRecurrenceInterval, adapterRecurrenceEndAfter;

    private Button buttonSave, buttonCancel;

    public ClickListenWOCreateClose clickClose;

    public interface ClickListenWOCreateClose {
        void callbackWorkOrderCreateClose(int back_type, boolean isSave, String workOrderID, String workOrderNo);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_work_order_create, container, false);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            back_type = bundle.getInt("back_type", 0);
        }

        apiInterface = APIClient.getClient().create(APIInterface.class);

        clickClose = (ClickListenWOCreateClose) getActivity();

        spinnerAssignedTo = (Spinner) view.findViewById(R.id.spinner_assigned_to);
        spinnerType = (Spinner) view.findViewById(R.id.spinner_type);
        spinnerStatus = (Spinner) view.findViewById(R.id.spinner_status);
        spinnerPriority = (Spinner) view.findViewById(R.id.spinner_priority);
        spinnerCategory = (Spinner) view.findViewById(R.id.spinner_category);

        edittextAccount = (EditText) view.findViewById(R.id.edittext_account);
        edittextContact = (EditText) view.findViewById(R.id.edittext_contact);
        edittextWorkOrder = (EditText) view.findViewById(R.id.edittext_work_order);
        edittextSubject = (EditText) view.findViewById(R.id.edittext_subject);
        edittextDescription = (EditText) view.findViewById(R.id.edittext_description);
        edittextPopupReminder = (EditText) view.findViewById(R.id.edittext_popup_reminder);
        edittextBilAddress = (EditText) view.findViewById(R.id.edittext_bil_address);
        edittextBilCity = (EditText) view.findViewById(R.id.edittext_bil_city);
        edittextBilState = (EditText) view.findViewById(R.id.edittext_bil_state);
        edittextBilCountry = (EditText) view.findViewById(R.id.edittext_bil_country);
        edittextBilPostal = (EditText) view.findViewById(R.id.edittext_bil_postal);

        edittextStartDateTime = (TextView) view.findViewById(R.id.edittext_start_date_time);
        edittextEndDateTime = (TextView) view.findViewById(R.id.edittext_end_date_time);

        checkboxIsRecurring = (CheckBox) view.findViewById(R.id.checkbox_is_recurring);
        buttonBilAddress = (ImageButton) view.findViewById(R.id.button_bil_address);

        buttonSave = (Button) view.findViewById(R.id.button_save);
        buttonCancel = (Button) view.findViewById(R.id.button_cancel);

        buttonAccount = (ImageButton) view.findViewById(R.id.button_account);
        layoutAccountAdd = (LinearLayout) view.findViewById(R.id.layout_account_add);
        edittextAccountAddSearch = (EditText) view.findViewById(R.id.edittext_account_add_search);
        phvAccountAdd = (PlaceHolderView) view.findViewById(R.id.phv_account_add);

        buttonContact = (ImageButton) view.findViewById(R.id.button_contact);
        layoutContactAdd = (LinearLayout) view.findViewById(R.id.layout_contact_add);
        edittextContactAddSearch = (EditText) view.findViewById(R.id.edittext_contact_add_search);
        phvContactAdd = (PlaceHolderView) view.findViewById(R.id.phv_contact_add);

        buttonWorkOrder = (ImageButton) view.findViewById(R.id.button_work_order);
        layoutWorkOrderAdd = (LinearLayout) view.findViewById(R.id.layout_work_order_add);
        edittextWorkOrderAddSearch = (EditText) view.findViewById(R.id.edittext_work_order_add_search);
        phvWorkOrderAdd = (PlaceHolderView) view.findViewById(R.id.phv_work_order_add);

        layoutRecurrence = (RelativeLayout) view.findViewById(R.id.layout_recurrence);
        spinnerRepeatsEvery = (Spinner) view.findViewById(R.id.spinner_repeats_every);
        spinnerIntervalEvery = (Spinner) view.findViewById(R.id.spinner_interval_every);
        textviewRepeatsEveryPostfix = (TextView) view.findViewById(R.id.textview_repeats_every_postfix);
        layoutRecurrenceRepeatOn = (LinearLayout) view.findViewById(R.id.layout_recurrence_repeat_on);
        checkboxSun = (CheckBox) view.findViewById(R.id.checkbox_sun);
        checkboxMon = (CheckBox) view.findViewById(R.id.checkbox_mon);
        checkboxTue = (CheckBox) view.findViewById(R.id.checkbox_tue);
        checkboxWed = (CheckBox) view.findViewById(R.id.checkbox_wed);
        checkboxThu = (CheckBox) view.findViewById(R.id.checkbox_thu);
        checkboxFri = (CheckBox) view.findViewById(R.id.checkbox_fri);
        checkboxSat = (CheckBox) view.findViewById(R.id.checkbox_sat);
        textviewStartOnDate = (TextView) view.findViewById(R.id.textview_start_on_date);
        textviewEndsOnDate = (TextView) view.findViewById(R.id.textview_ends_on_date);
        edittextRecurrenceStartTime = (TextView) view.findViewById(R.id.textview_recurrence_start_time);
        edittextRecurrenceEndTime = (TextView) view.findViewById(R.id.textview_recurrence_end_time);
        radioGroupRecurrence = (RadioGroup) view.findViewById(R.id.radio_group_recurrence);
        radioButtonRecurrenceNever = (RadioButton) view.findViewById(R.id.radio_button_recurrence_never);
        radioButtonRecurrenceOn = (RadioButton) view.findViewById(R.id.radio_button_recurrence_on);
        radioButtonRecurrenceAfter = (RadioButton) view.findViewById(R.id.radio_button_recurrence_after);
        layoutEndAfter = (RelativeLayout) view.findViewById(R.id.layout_end_after);
        spinnerEndsAfter = (Spinner) view.findViewById(R.id.spinner_end_after);
        textviewEndsAfterPostfix = (TextView) view.findViewById(R.id.textview_ends_after_postfix);
        buttonRecurrenceSave = (Button) view.findViewById(R.id.button_recurrence_save);
        buttonRecurrenceClose = (Button) view.findViewById(R.id.button_recurrence_close);

        strNewBilLat = "0";
        strNewBilLng = "0";
        strNewLat = "0";
        strNewLng = "0";
        strNewAddress = "0";
        strNewCity = "0";
        strNewState = "0";
        strNewCountry = "0";
        strNewPostal = "0";

        strRepeatEvery = "";
        strNewRepeatEvery = "";
        strIntervalEvery = "";
        strNewIntervalEvery = "";
        strEnds = "";
        strEndsAfter = "";
        strSun = "";
        strMon = "";
        strTue = "";
        strWed = "";
        strThu = "";
        strFri = "";
        strSat = "";

        listAssignedTo = new ArrayList<>();
        listAccounts = new ArrayList<>();
        listPrimaryContacts = new ArrayList<>();
        listParentWorkOrders = new ArrayList<>();
        listTypes = new ArrayList<>();
        listStatus = new ArrayList<>();
        listPriorities = new ArrayList<>();
        listCategories = new ArrayList<>();
        adapterAssignedTo = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listAssignedTo);
        adapterAssignedTo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterPrimaryContact = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listPrimaryContacts);
        adapterPrimaryContact.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterParentWorkOrder = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listParentWorkOrders);
        adapterParentWorkOrder.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterType = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listTypes);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterStatus = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listStatus);
        adapterStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterPriority = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listPriorities);
        adapterPriority.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterCategory = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listCategories);
        adapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        getAllUsers();
        //getAccounts();
        //getContacts();
        //getWorkOrders();
        getTypes();
        getStatus();
        getPriorities();
        getCategories();

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DATE);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        final int hour = calendar.get(Calendar.HOUR);
        final int minute = calendar.get(Calendar.MINUTE);
        final int AP = calendar.get(Calendar.AM_PM);
        String AM_PM;
        if (AP == 0) {
            AM_PM = "AM;";
        } else {
            AM_PM = "PM";
        }

        strStartOn = selectSimpleDate(day, month, year);
        strStartOnNew = selectSimpleDate(day, month, year);
        strStartDateTimeDate = setDate(minute, hour, day, month, year, AM_PM);
        strNewStartDateTimeDate = setDate(minute, hour, day, month, year, AM_PM);
        strStartDateTimeTime = setTime(minute, hour, AM_PM, 0);
        strNewStartDateTimeTime = setTime(minute, hour, AM_PM, 0);
        strEndDateTimeDate = setDate(minute, hour, day, month, year, AM_PM);
        strNewEndDateTimeDate = setDate(minute, hour, day, month, year, AM_PM);
        strEndDateTimeTime = setTime(minute, hour, AM_PM, 0);
        strNewEndDateTimeTime = setTime(minute, hour, AM_PM, 0);
        strRecurrenceStartTime = setTime(minute, hour, AM_PM, 1);
        strRecurrenceNewStartTime = setTime(minute, hour, AM_PM, 1);
        strRecurrenceEndTime = setTime(minute, hour, AM_PM, 1);
        strRecurrenceNewEndTime = setTime(minute, hour, AM_PM, 1);
        strEndsOn = selectSimpleDate(day, month, year);
        strEndsOnNew = selectSimpleDate(day, month, year);

        edittextStartDateTime.setText(strStartDateTimeDate);
        edittextEndDateTime.setText(strEndDateTimeDate);

        buttonAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isAccountClicked = true;
                isContactClicked = false;
                isWorkOrderClicked = false;
                layoutAccountAdd.setVisibility(View.VISIBLE);
                listAccounts.clear();
                getAccounts();
            }
        });

        edittextAccountAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvAccountAdd.removeAllViews();
                    ArrayList<ModelSpinner> modle = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listAccounts) {
                        if (m.getName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvAccountAdd
                                .addView(new AdapterAccount(getActivity(), FragmentWorkOrderCreate.this, i, modle.get(i)));
                    }
                } else {
                    phvAccountAdd.removeAllViews();
                    for (int i = 0; i < listAccounts.size(); i++) {
                        phvAccountAdd
                                .addView(new AdapterAccount(getActivity(), FragmentWorkOrderCreate.this, i, listAccounts.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        buttonContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isAccountClicked = false;
                isContactClicked = true;
                isWorkOrderClicked = false;
                layoutContactAdd.setVisibility(View.VISIBLE);
                listPrimaryContacts.clear();
                getContacts();
            }
        });

        edittextContactAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvContactAdd.removeAllViews();
                    ArrayList<ModelSpinner> modle = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listPrimaryContacts) {
                        if (m.getName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvContactAdd
                                .addView(new AdapterAccount(getActivity(), FragmentWorkOrderCreate.this, i, modle.get(i)));
                    }
                } else {
                    phvContactAdd.removeAllViews();
                    for (int i = 0; i < listPrimaryContacts.size(); i++) {
                        phvContactAdd
                                .addView(new AdapterAccount(getActivity(), FragmentWorkOrderCreate.this, i, listPrimaryContacts.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        buttonWorkOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isAccountClicked = false;
                isContactClicked = false;
                isWorkOrderClicked = true;
                layoutWorkOrderAdd.setVisibility(View.VISIBLE);
                listParentWorkOrders.clear();
                getWorkOrders();
            }
        });

        edittextWorkOrderAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvWorkOrderAdd.removeAllViews();
                    ArrayList<ModelSpinner> modle = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listParentWorkOrders) {
                        if (m.getName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvWorkOrderAdd
                                .addView(new AdapterAccount(getActivity(), FragmentWorkOrderCreate.this, i, modle.get(i)));
                    }
                } else {
                    phvWorkOrderAdd.removeAllViews();
                    for (int i = 0; i < listParentWorkOrders.size(); i++) {
                        phvWorkOrderAdd
                                .addView(new AdapterAccount(getActivity(), FragmentWorkOrderCreate.this, i, listParentWorkOrders.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        edittextStartDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                final int hour = calendar.get(Calendar.HOUR);
                final int minute = calendar.get(Calendar.MINUTE);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hr, int min) {
                                isStartDateClicked = true;
                                Calendar c = Calendar.getInstance();
                                c.set(0, 0, 0, hr, min);
                                String AM_PM = (String) android.text.format.DateFormat.format("aaa", c);
                                if (hr > 12) {
                                    hr = hr - 12;
                                }
                                strNewStartDateTimeDate = setDate(min, hr, d, m, y, AM_PM);
                                strNewStartDateTimeTime = setTime(min, hr, AM_PM, 0);
                                edittextStartDateTime.setText(formatDate(strNewStartDateTimeDate));
                            }
                        }, hour, minute, false);
                        timePickerDialog.show();
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        edittextEndDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                final int hour = calendar.get(Calendar.HOUR);
                final int minute = calendar.get(Calendar.MINUTE);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hr, int min) {
                                isEndDateClicked = true;
                                Calendar c = Calendar.getInstance();
                                c.set(0, 0, 0, hr, min);
                                String AM_PM = (String) android.text.format.DateFormat.format("aaa", c);
                                if (hr > 12) {
                                    hr = hr - 12;
                                }
                                strNewEndDateTimeDate = setDate(min, hr, d, m, y, AM_PM);
                                strNewEndDateTimeTime = setTime(min, hr, AM_PM, 0);
                                edittextEndDateTime.setText(formatDate(strNewEndDateTimeDate));
                            }
                        }, hour, minute, false);
                        timePickerDialog.show();
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        buttonBilAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!checkPermission()) {
                        requestPermission();
                    } else {
                        isBilClicked = true;
                        double lat = Double.valueOf(strLat);
                        double lng = Double.valueOf(strLng);
                        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                                .withLocation(lat, lng)
                                .withGeolocApiKey(getString(R.string.google_maps_key))
                                .withGooglePlacesEnabled()
                                .withSatelliteViewHidden()
                                .withVoiceSearchHidden()
                                .shouldReturnOkOnBackPressed()
                                .build(getActivity());
                        startActivityForResult(locationPickerIntent, 99);
                    }
                } else {
                    isBilClicked = true;
                    double lat = Double.valueOf(strLat);
                    double lng = Double.valueOf(strLng);
                    Intent locationPickerIntent = new LocationPickerActivity.Builder()
                            .withLocation(lat, lng)
                            .withGeolocApiKey(getString(R.string.google_maps_key))
                            .withGooglePlacesEnabled()
                            .withSatelliteViewHidden()
                            .withVoiceSearchHidden()
                            .shouldReturnOkOnBackPressed()
                            .build(getActivity());
                    startActivityForResult(locationPickerIntent, 99);
                }
            }
        });

        listRecurrenceRepeat = new ArrayList<>();
        listRecurrenceRepeat.add("Daily");
        listRecurrenceRepeat.add("Weekly");
        listRecurrenceRepeat.add("Monthly");
        listRecurrenceRepeat.add("Yearly");
        listRecurrenceRepeat.add("Periodically");
        listRecurrenceInterval = new ArrayList<>();
        listRecurrenceInterval.add("1");
        listRecurrenceInterval.add("2");
        listRecurrenceInterval.add("3");
        listRecurrenceInterval.add("4");
        listRecurrenceInterval.add("5");
        listRecurrenceEndAfter = new ArrayList<>();
        listRecurrenceEndAfter.add("1");
        listRecurrenceEndAfter.add("2");
        listRecurrenceEndAfter.add("3");
        listRecurrenceEndAfter.add("4");
        listRecurrenceEndAfter.add("5");
        adapterRecurrenceRepeat = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listRecurrenceRepeat);
        adapterRecurrenceRepeat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterRecurrenceInterval = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listRecurrenceInterval);
        adapterRecurrenceInterval.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterRecurrenceEndAfter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listRecurrenceEndAfter);
        adapterRecurrenceEndAfter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        checkboxIsRecurring.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    ActivityMain.isRecurringOn = true;
                    layoutRecurrence.setVisibility(View.VISIBLE);
                    showView(layoutRecurrence);

                    isStartDateOnClicked = false;
                    textviewStartOnDate.setText(formatSimpleDate(strStartOn));
                    strStartOnNew = "";

                    spinnerRepeatsEvery.setAdapter(adapterRecurrenceRepeat);
                    spinnerRepeatsEvery.setSelection(0);
                    setStrSpinnerDropDownHeight(spinnerRepeatsEvery);
                    strRepeatEvery = listRecurrenceRepeat.get(0);
                    textviewRepeatsEveryPostfix.setText(listRecurrenceRepeat.get(0) + " On");

                    spinnerIntervalEvery.setAdapter(adapterRecurrenceInterval);
                    spinnerIntervalEvery.setSelection(0);
                    setStrSpinnerDropDownHeight(spinnerIntervalEvery);
                    strIntervalEvery = listRecurrenceInterval.get(0);

                    checkboxSun.setChecked(false);
                    checkboxMon.setChecked(false);
                    checkboxTue.setChecked(false);
                    checkboxWed.setChecked(false);
                    checkboxThu.setChecked(false);
                    checkboxFri.setChecked(false);
                    checkboxSat.setChecked(false);

                    isRepeatEveryClicked = false;
                    isIntervalClicked = false;

                    radioButtonRecurrenceNever.setChecked(true);
                    strEnds = "Never";

                    isEndDateOnClicked = false;
                    textviewEndsOnDate.setText(formatSimpleDate(strEndsOn));
                    strEndsOnNew = "";

                    spinnerEndsAfter.setAdapter(adapterRecurrenceEndAfter);
                    spinnerEndsAfter.setSelection(0);
                    setStrSpinnerDropDownHeight(spinnerEndsAfter);
                    strEndsAfter = listRecurrenceEndAfter.get(0);
                    strEndsAfter = "";

                    edittextRecurrenceStartTime.setText(strRecurrenceStartTime);
                    edittextRecurrenceEndTime.setText(strRecurrenceEndTime);
                }
            }
        });

        textviewStartOnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                final int day = calendar.get(Calendar.DATE);
                final int month = calendar.get(Calendar.MONTH);
                final int year = calendar.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        isStartDateOnClicked = true;
                        strStartOnNew = selectSimpleDate(d, m, y);
                        textviewStartOnDate.setText(formatSimpleDate(strStartOnNew));
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        spinnerRepeatsEvery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                isRepeatEveryClicked = true;
                if (position == 1) {
                    layoutRecurrenceRepeatOn.setVisibility(View.VISIBLE);
                } else {
                    layoutRecurrenceRepeatOn.setVisibility(View.GONE);
                }
                strNewRepeatEvery = listRecurrenceRepeat.get(position);
                textviewRepeatsEveryPostfix.setText(strNewRepeatEvery + " On");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        spinnerIntervalEvery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                isIntervalClicked = true;
                strNewIntervalEvery = listRecurrenceInterval.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        checkboxSun.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strSun = "Sun";
                } else {
                    strSun = "";
                }
            }
        });

        checkboxMon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strMon = "Mon";
                } else {
                    strMon = "";
                }
            }
        });

        checkboxTue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strTue = "Tue";
                } else {
                    strTue = "";
                }
            }
        });

        checkboxWed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strWed = "Wed";
                } else {
                    strWed = "";
                }
            }
        });

        checkboxThu.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strThu = "Thu";
                } else {
                    strThu = "";
                }
            }
        });

        checkboxFri.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strFri = "Fri";
                } else {
                    strFri = "";
                }
            }
        });

        checkboxSat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strSat = "Sat";
                } else {
                    strSat = "";
                }
            }
        });

        radioButtonRecurrenceNever.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strEnds = "Never";
                    strEndsOnNew = "";
                    isEndDateOnClicked = false;
                    textviewEndsOnDate.setVisibility(View.GONE);
                    spinnerEndsAfter.setEnabled(false);
                    layoutEndAfter.setVisibility(View.GONE);
                    textviewEndsAfterPostfix.setVisibility(View.GONE);
                }
            }
        });

        radioButtonRecurrenceOn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strEnds = "On";
                    strEndsOnNew = "";
                    isEndDateOnClicked = false;
                    textviewEndsOnDate.setVisibility(View.VISIBLE);
                    if (isEndDateOnClicked) {
                        textviewEndsOnDate.setText(formatSimpleDate(strEndsOnNew));
                    } else {
                        Calendar calendar = Calendar.getInstance();
                        int day = calendar.get(Calendar.DATE);
                        int month = calendar.get(Calendar.MONTH);
                        int year = calendar.get(Calendar.YEAR);
                        strEndsOn = selectSimpleDate(day, month, year);
                        textviewEndsOnDate.setText(formatSimpleDate(strEndsOn));
                    }
                    spinnerEndsAfter.setEnabled(false);
                    layoutEndAfter.setVisibility(View.GONE);
                    textviewEndsAfterPostfix.setVisibility(View.GONE);
                }
            }
        });

        radioButtonRecurrenceAfter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strEnds = "After";
                    strEndsOn = "";
                    strEndsOnNew = "";
                    isEndDateOnClicked = false;

                    spinnerEndsAfter.setAdapter(adapterRecurrenceEndAfter);
                    spinnerEndsAfter.setSelection(0);
                    setStrSpinnerDropDownHeight(spinnerEndsAfter);
                    strEndsAfter = listRecurrenceEndAfter.get(0);

                    textviewEndsOnDate.setVisibility(View.GONE);
                    spinnerEndsAfter.setEnabled(true);
                    layoutEndAfter.setVisibility(View.VISIBLE);
                    textviewEndsAfterPostfix.setVisibility(View.VISIBLE);
                }
            }
        });

        textviewEndsOnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                final int day = calendar.get(Calendar.DATE);
                final int month = calendar.get(Calendar.MONTH);
                final int year = calendar.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        isEndDateOnClicked = true;
                        strEndsOnNew = selectSimpleDate(d, m, y);
                        textviewEndsOnDate.setText(formatSimpleDate(strEndsOnNew));
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        edittextRecurrenceStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                final int hour = calendar.get(Calendar.HOUR);
                final int minute = calendar.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hr, int min) {
                        isRecurrenceStartDateClicked = true;
                        Calendar c = Calendar.getInstance();
                        c.set(0, 0, 0, hr, min);
                        String AM_PM = (String) android.text.format.DateFormat.format("aaa", c);
                        if (hr > 12) {
                            hr = hr - 12;
                        }
                        strRecurrenceNewStartTime = setTime(min, hr, AM_PM, 1);
                        edittextRecurrenceStartTime.setText(strRecurrenceNewStartTime);
                    }
                }, hour, minute, false);
                timePickerDialog.show();
            }
        });

        edittextRecurrenceEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                final int hour = calendar.get(Calendar.HOUR);
                final int minute = calendar.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hr, int min) {
                        isRecurrenceEndDateClicked = true;
                        Calendar c = Calendar.getInstance();
                        c.set(0, 0, 0, hr, min);
                        String AM_PM = (String) android.text.format.DateFormat.format("aaa", c);
                        if (hr > 12) {
                            hr = hr - 12;
                        }
                        strRecurrenceNewStartTime = setTime(min, hr, AM_PM, 1);
                        edittextRecurrenceEndTime.setText(strRecurrenceNewStartTime);
                    }
                }, hour, minute, false);
                timePickerDialog.show();
            }
        });

        buttonRecurrenceSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityMain.isRecurringOn = false;
                layoutRecurrence.setVisibility(View.GONE);
            }
        });

        buttonRecurrenceClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityMain.isRecurringOn = false;
                layoutRecurrence.setVisibility(View.GONE);
                checkboxIsRecurring.setChecked(false);
                strStartOn = "";
                strRepeatEvery = "";
                strIntervalEvery = "";
                strEnds = "Never";
                strEndsOn = "";
                strEndsAfter = "";
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String assignedTo = listAssignedTo.get(spinnerAssignedTo.getSelectedItemPosition()).getId();
                String account = strAccount;
                String primaryContact = strPrimaryContact;
                String parentWorkOrder = strParentWorkOrder;

                String subject = edittextSubject.getText().toString().trim();
                String description = edittextDescription.getText().toString().trim();
                String popUpReminder = edittextPopupReminder.getText().toString().trim();

                String types = listTypes.get(spinnerType.getSelectedItemPosition()).getId();
                String status = listStatus.get(spinnerStatus.getSelectedItemPosition()).getId();
                String priority = listPriorities.get(spinnerPriority.getSelectedItemPosition()).getId();
                String category = listCategories.get(spinnerCategory.getSelectedItemPosition()).getId();

                String startDate = "";
                String startTime = "";
                String endDate = "";
                String endTime = "";
                if (isStartDateClicked) {
                    startDate = strNewStartDateTimeDate;
                    startTime = strNewStartDateTimeTime;
                } else {
                    startDate = strStartDateTimeDate;
                    startTime = strStartDateTimeTime;
                }
                if (isEndDateClicked) {
                    endDate = strNewEndDateTimeDate;
                    endTime = strNewEndDateTimeTime;
                } else {
                    endDate = strEndDateTimeDate;
                    endTime = strEndDateTimeTime;
                }

                if (checkboxIsRecurring.isChecked()) {
                    strRecurring = "1";
                } else {
                    strRecurring = "0";

                }

                String startOn = "";
                String repeatEvery = "";
                String intervalEvery = "";
                StringBuilder repeatOn = new StringBuilder();
                String ends = strEnds;
                String endsOn = "";
                String endsAfter = "";
                String recurrenceStartTime = "";
                String recurrenceEndTime = "";

                if (isStartDateOnClicked) {
                    startOn = strStartOnNew;
                } else {
                    startOn = strStartOn;
                }

                if (isRepeatEveryClicked) {
                    repeatEvery = strNewRepeatEvery;
                } else {
                    repeatEvery = strRepeatEvery;
                }

                if (isIntervalClicked) {
                    intervalEvery = strNewIntervalEvery;
                } else {
                    intervalEvery = strIntervalEvery;
                }

                repeatOn.append("");
                if (!strSun.equals("")) {
                    repeatOn.append(strSun).append(",");
                }
                if (!strMon.equals("")) {
                    repeatOn.append(strMon).append(",");
                }
                if (!strTue.equals("")) {
                    repeatOn.append(strTue).append(",");
                }
                if (!strWed.equals("")) {
                    repeatOn.append(strWed).append(",");
                }
                if (!strThu.equals("")) {
                    repeatOn.append(strThu).append(",");
                }
                if (!strFri.equals("")) {
                    repeatOn.append(strFri).append(",");
                }
                if (!strSat.equals("")) {
                    repeatOn.append(strSat).append(",");
                }

                if (repeatOn.toString().endsWith(",")) {
                    String str = repeatOn.substring(0, repeatOn.length() - 1);
                    repeatOn = new StringBuilder(str);
                }

                if (!strEnds.equals("Never")) {
                    if (ends.equals("On")) {
                        if (isEndDateOnClicked) {
                            endsOn = strEndsOnNew;
                        } else {
                            endsOn = strEndsOn;
                        }
                    } else if (ends.equals("After")) {
                        endsAfter = strEndsAfter;
                    }
                } else {
                    endsOn = "";
                    endsAfter = "";
                }

                if (isRecurrenceStartDateClicked) {
                    recurrenceStartTime = strRecurrenceNewStartTime;
                } else {
                    recurrenceStartTime = strRecurrenceStartTime;
                }
                if (isRecurrenceEndDateClicked) {
                    recurrenceEndTime = strRecurrenceNewEndTime;
                } else {
                    recurrenceEndTime = strRecurrenceEndTime;
                }

                String bilLat = strNewBilLat;
                String bilLng = strNewBilLng;
                String bilAddress = edittextBilAddress.getText().toString().trim();
                String bilCity = edittextBilCity.getText().toString().trim();
                String bilState = edittextBilState.getText().toString().trim();
                String bilCountry = edittextBilCountry.getText().toString().trim();
                String bilPostal = edittextBilPostal.getText().toString().trim();

                if (TextUtils.isEmpty(account)) {
                    Utl.showToast(getActivity(), "Select Account");
                } else if (TextUtils.isEmpty(subject)) {
                    Utl.showToast(getActivity(), "Enter Subject");
                } else if (TextUtils.isEmpty(description)) {
                    Utl.showToast(getActivity(), "Enter Description");
                } else if (TextUtils.isEmpty(bilAddress)) {
                    Utl.showToast(getActivity(), "Enter Address");
                } else if (TextUtils.isEmpty(bilCity)) {
                    Utl.showToast(getActivity(), "Enter City");
                } else if (TextUtils.isEmpty(bilState)) {
                    Utl.showToast(getActivity(), "Enter State");
                } else if (TextUtils.isEmpty(bilCountry)) {
                    Utl.showToast(getActivity(), "Enter Country");
                } else if (TextUtils.isEmpty(bilPostal)) {
                    Utl.showToast(getActivity(), "Enter Postal Code");
                } else {
                    createWorkOrder(assignedTo,
                            account,
                            primaryContact,
                            parentWorkOrder,
                            subject,
                            description,
                            popUpReminder,
                            types,
                            status,
                            priority,
                            category,
                            startDate,
                            startTime,
                            endDate,
                            endTime,
                            strRecurring, startOn,
                            bilLat, bilLng,
                            bilAddress, bilCity, bilState, bilCountry, bilPostal,
                            repeatEvery, intervalEvery,
                            repeatOn.toString(), ends, endsOn, endsAfter, recurrenceStartTime, recurrenceEndTime);
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickClose.callbackWorkOrderCreateClose(back_type, false, "", "");
            }
        });

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(provider);
        if (location != null) {
            Log.d("Provider: ", provider + " has been selected.");
            onLocationChanged(location);
        }

        return view;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99) {
            if (data != null) {
                double latitude = data.getDoubleExtra("latitude", 0.0);
                strNewLat = String.valueOf(latitude);
                double longitude = data.getDoubleExtra("longitude", 0.0);
                strNewLng = String.valueOf(longitude);
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> addresses = new ArrayList<>();
                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                strNewAddress = addresses.get(0).getSubThoroughfare() + ", " + addresses.get(0).getThoroughfare();
                if (addresses.get(0).getLocality() != null) {
                    strNewCity = addresses.get(0).getLocality();
                } else {
                    strNewCity = addresses.get(0).getSubLocality();
                }
                if (addresses.get(0).getAdminArea() != null) {
                    strNewState = addresses.get(0).getAdminArea();
                } else {
                    strNewState = addresses.get(0).getSubAdminArea();
                }
                strNewCountry = addresses.get(0).getCountryName();
                strNewPostal = addresses.get(0).getPostalCode();
                Log.d("TAG_LATITUDE****", strNewLat);
                Log.d("TAG_LONGITUDE****", strNewLng);
                Log.d("TAG_ADDRESS****", strNewAddress);
                Log.d("TAG_CITY****", strNewCity);
                Log.d("TAG_STATE****", strNewState);
                Log.d("TAG_COUNTRY****", strNewCountry);
                Log.d("TAG_POSTAL****", strNewPostal);
                strNewBilLat = strNewLat;
                strNewBilLng = strNewLng;
                edittextBilAddress.setText(strNewAddress);
                edittextBilCity.setText(strNewCity);
                edittextBilState.setText(strNewState);
                edittextBilCountry.setText(strNewCountry);
                edittextBilPostal.setText(strNewPostal);
            } else {
                Log.d("RESULT", "CANCELLED");
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        strLat = String.valueOf(location.getLatitude());
        strLng = String.valueOf(location.getLongitude());
        Log.d("TAG_LAT*****", strLat);
        Log.d("TAG_LNG*****", strLng);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
        Toast.makeText(getActivity(), "Enabled New Provider " + provider, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String s) {
        Toast.makeText(getActivity(), "Disabled New Provider " + provider, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    private void setStrSpinnerDropDownHeight(Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 70);
    }

    private void setSpinnerDropDownHeight(List<ModelSpinner> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 70);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    public int getListIndex(List<ModelSpinner> list, String name) {
        for (int i = 0; i < list.size(); i++) {
            ModelSpinner model = list.get(i);
            if (name.equals(model.getName())) {
                return i;
            }
        }
        return -1;
    }

    public String selectSimpleDate(int d, int m, int y) {
        String time = "" + y + "-" + (m + 1) + "-" + d;
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String formatSimpleDate(String strDate) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(strDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String setTime(int min, int hr, String AM_PM, int format) {
        String time = "" + hr + ":" + min + " " + AM_PM;
        String inputPattern;
        String outputPattern;
        if (format == 0) {
            inputPattern = "hh:mm aaa";
            outputPattern = "hh:mm aaa";
        } else {
            inputPattern = "hh:mm aaa";
            outputPattern = "kk:mm:ss";
        }
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String setDate(int min, int hr, int d, int m, int y, String AM_PM) {
        String time = "" + (m + 1) + "/" + d + "/" + y + " " + hr + ":" + min + " " + AM_PM;
        String inputPattern = "MM/dd/yyyy hh:mm aaa";
        String outputPattern = "MM/dd/yyyy hh:mm aaa";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String formatDate(String strDate) {
        String inputPattern = "MM/dd/yyyy hh:mm aaa";
        String outputPattern = "dd MMM yyyy hh:mm aaa";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(strDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void getAllUsers() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getWorkOrderAllUsers(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_AllUsers" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAssignedTo.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_UserID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listAssignedTo.add(model);
                                setSpinnerDropDownHeight(listAssignedTo, spinnerAssignedTo);
                                spinnerAssignedTo.setAdapter(adapterAssignedTo);
                                spinnerAssignedTo.setSelection(0);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getAccounts() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getWorkOrderAccounts(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Accounts" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAccounts.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_AccountID));
                                model.setName(dataObject.getString(Cons.KEY_AccountName));
                                listAccounts.add(model);
                            }
                            for (int i = 0; i < listAccounts.size(); i++) {
                                phvAccountAdd
                                        .addView(new AdapterAccount(getActivity(), FragmentWorkOrderCreate.this, i, listAccounts.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getContacts() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getWorkOrderContacts(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Contacts" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listPrimaryContacts.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_ContactID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listPrimaryContacts.add(model);
                            }
                            for (int i = 0; i < listPrimaryContacts.size(); i++) {
                                phvContactAdd
                                        .addView(new AdapterAccount(getActivity(), FragmentWorkOrderCreate.this, i, listPrimaryContacts.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getWorkOrders() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getWorkOrderParentWorkOrders(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_ParentWO" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listParentWorkOrders.clear();
                            ModelSpinner m = new ModelSpinner();
                            m.setId("");
                            m.setName("None");
                            listParentWorkOrders.add(m);
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_WorkOrderID));
                                model.setName(dataObject.getString(Cons.KEY_Subject));
                                listParentWorkOrders.add(model);
                            }
                            for (int i = 0; i < listParentWorkOrders.size(); i++) {
                                phvWorkOrderAdd
                                        .addView(new AdapterAccount(getActivity(), FragmentWorkOrderCreate.this, i, listParentWorkOrders.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getTypes() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getWorkOrderTypes(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Types" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listTypes.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_WorkOrderTypeID));
                                model.setName(dataObject.getString(Cons.KEY_WorkOrderType));
                                listTypes.add(model);
                                setSpinnerDropDownHeight(listTypes, spinnerType);
                                spinnerType.setAdapter(adapterType);
                                spinnerType.setSelection(0);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getStatus() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getWorkOrderStatus(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Status" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listStatus.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_WorkOrderStatusID));
                                model.setName(dataObject.getString(Cons.KEY_Status));
                                listStatus.add(model);
                                setSpinnerDropDownHeight(listStatus, spinnerStatus);
                                spinnerStatus.setAdapter(adapterStatus);
                                spinnerStatus.setSelection(0);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getPriorities() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getWorkOrderPriorities(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Priorities" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listPriorities.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_WorkOrderPriorityID));
                                model.setName(dataObject.getString(Cons.KEY_Priority));
                                listPriorities.add(model);
                                setSpinnerDropDownHeight(listPriorities, spinnerPriority);
                                spinnerPriority.setAdapter(adapterPriority);
                                spinnerPriority.setSelection(0);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getCategories() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getWorkOrderCategories(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Categories" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listCategories.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_WorkOrderCategoryID));
                                model.setName(dataObject.getString(Cons.KEY_CategoryName));
                                listCategories.add(model);
                                setSpinnerDropDownHeight(listCategories, spinnerCategory);
                                spinnerCategory.setAdapter(adapterCategory);
                                spinnerCategory.setSelection(0);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void createWorkOrder(String assignedTo,
                                 String account,
                                 String primaryContact,
                                 String parentWorkOrder,
                                 String subject,
                                 String description,
                                 String popUpReminder,
                                 String types,
                                 String status,
                                 String priority,
                                 String category,
                                 String startDate,
                                 String startTime,
                                 String endDate,
                                 String endTime,
                                 String strRecurring, String startOn,
                                 String bilLat, String bilLng,
                                 String bilAddress, String bilCity, String bilState, String bilCountry, String bilPostal,
                                 String repeatEvery, String intervalEvery, String repeatOn,
                                 String ends, String endsOn, String endsAfter,
                                 String recurrenceStartTime, String recurrenceEndTime) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AssignedTo, RequestBody.create(MediaType.parse("text/plain"), assignedTo));
        map.put(Cons.KEY_Account, RequestBody.create(MediaType.parse("text/plain"), account));
        map.put(Cons.KEY_PrimaryContact, RequestBody.create(MediaType.parse("text/plain"), primaryContact));
        map.put(Cons.KEY_ParentWorkOrder, RequestBody.create(MediaType.parse("text/plain"), parentWorkOrder));
        map.put(Cons.KEY_Subject, RequestBody.create(MediaType.parse("text/plain"), subject));
        map.put(Cons.KEY_Description, RequestBody.create(MediaType.parse("text/plain"), description));
        map.put(Cons.KEY_PopUpReminder, RequestBody.create(MediaType.parse("text/plain"), popUpReminder));
        map.put(Cons.KEY_WorkOrderType, RequestBody.create(MediaType.parse("text/plain"), types));
        map.put(Cons.KEY_WOStatus, RequestBody.create(MediaType.parse("text/plain"), status));
        map.put(Cons.KEY_WOPriority, RequestBody.create(MediaType.parse("text/plain"), priority));
        map.put(Cons.KEY_WOCategory, RequestBody.create(MediaType.parse("text/plain"), category));
        map.put(Cons.KEY_StartDate, RequestBody.create(MediaType.parse("text/plain"), startDate));
        map.put(Cons.KEY_WOStartTime, RequestBody.create(MediaType.parse("text/plain"), startTime));
        map.put(Cons.KEY_EndDate, RequestBody.create(MediaType.parse("text/plain"), endDate));
        map.put(Cons.KEY_WOEndTime, RequestBody.create(MediaType.parse("text/plain"), endTime));
        map.put(Cons.KEY_IsRecurring, RequestBody.create(MediaType.parse("text/plain"), strRecurring));
        map.put(Cons.KEY_StartOn, RequestBody.create(MediaType.parse("text/plain"), startOn));
        map.put(Cons.KEY_RepeatEvery, RequestBody.create(MediaType.parse("text/plain"), repeatEvery));
        map.put(Cons.KEY_RepeatOn, RequestBody.create(MediaType.parse("text/plain"), repeatOn));
        map.put(Cons.KEY_IntervalEvery, RequestBody.create(MediaType.parse("text/plain"), intervalEvery));
        map.put(Cons.KEY_Ends, RequestBody.create(MediaType.parse("text/plain"), ends));
        map.put(Cons.KEY_EndsOnDate, RequestBody.create(MediaType.parse("text/plain"), endsOn));
        map.put(Cons.KEY_EndsAfterOccurrences, RequestBody.create(MediaType.parse("text/plain"), endsAfter));
        map.put(Cons.KEY_StartTime, RequestBody.create(MediaType.parse("text/plain"), recurrenceStartTime));
        map.put(Cons.KEY_EndTime, RequestBody.create(MediaType.parse("text/plain"), recurrenceEndTime));
        map.put(Cons.KEY_Address, RequestBody.create(MediaType.parse("text/plain"), bilAddress));
        map.put(Cons.KEY_City, RequestBody.create(MediaType.parse("text/plain"), bilCity));
        map.put(Cons.KEY_State, RequestBody.create(MediaType.parse("text/plain"), bilState));
        map.put(Cons.KEY_Country, RequestBody.create(MediaType.parse("text/plain"), bilCountry));
        map.put(Cons.KEY_PostalCode, RequestBody.create(MediaType.parse("text/plain"), bilPostal));
        map.put(Cons.KEY_Latitude, RequestBody.create(MediaType.parse("text/plain"), bilLat));
        map.put(Cons.KEY_Longitude, RequestBody.create(MediaType.parse("text/plain"), bilLng));
        Call<ResponseBody> response;
        response = apiInterface.createWorkOrderDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Create" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            String workOrderID = object.getString(Cons.KEY_WorkOrderID);
                            String workOrderNo = object.getString(Cons.KEY_WorkOrderNo);
                            clickClose.callbackWorkOrderCreateClose(back_type, true, workOrderID, workOrderNo);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    public boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), CAMERA);
        int result2 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION);
        int result3 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_COARSE_LOCATION);
        int result4 = ContextCompat.checkSelfPermission(getActivity(), CALL_PHONE);
        int result5 = ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE);
        int result6 = ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE);
        return
                result1 == PackageManager.PERMISSION_GRANTED
                        && result2 == PackageManager.PERMISSION_GRANTED
                        && result3 == PackageManager.PERMISSION_GRANTED
                        && result4 == PackageManager.PERMISSION_GRANTED
                        && result5 == PackageManager.PERMISSION_GRANTED
                        && result6 == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{
                        CAMERA,
                        ACCESS_FINE_LOCATION,
                        ACCESS_COARSE_LOCATION,
                        CALL_PHONE,
                        READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean locationFineAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean locationCoarseAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean callAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean readAccepted = grantResults[4] == PackageManager.PERMISSION_GRANTED;
                    boolean writeAccepted = grantResults[5] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted &&
                            locationFineAccepted &&
                            locationCoarseAccepted &&
                            callAccepted &&
                            readAccepted &&
                            writeAccepted)
                        Utl.showToast(getActivity(), "Permission Granted.");
                    else {
                        Utl.showToast(getActivity(), "Permission Denied.");
                    }
                }
                break;
        }
    }

    private void showView(final View view) {
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_right);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }
        });
        view.startAnimation(animation);
    }

    @Override
    public void callbackAddAccountListen(int pos, ModelSpinner model) {
        if (isAccountClicked) {
            phvAccountAdd.removeAllViews();
            layoutAccountAdd.setVisibility(View.GONE);
            strAccount = model.getId();
            strAccountName = model.getName();
            edittextAccount.setText(strAccountName);
        } else if (isContactClicked) {
            phvContactAdd.removeAllViews();
            layoutContactAdd.setVisibility(View.GONE);
            strPrimaryContact = model.getId();
            strPrimaryContactName = model.getName();
            edittextContact.setText(strPrimaryContactName);
        } else if (isWorkOrderClicked) {
            phvWorkOrderAdd.removeAllViews();
            layoutWorkOrderAdd.setVisibility(View.GONE);
            strParentWorkOrder = model.getId();
            strParentWorkOrderName = model.getName();
            edittextWorkOrder.setText(strParentWorkOrderName);
        }
    }

}