package com.fieldwise.fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.adapters.AdapterOwner;
import com.fieldwise.adapters.AdapterAccount;
import com.fieldwise.models.ModelSpinner;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;
import com.mindorks.placeholderview.PlaceHolderView;
import com.schibstedspain.leku.LocationPickerActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class FragmentInvoiceCreate extends Fragment implements
        AdapterAccount.AddAccountClickListen,
        AdapterOwner.AddOwnerClickListen {

    private static final int PERMISSION_REQUEST_CODE = 200;

    int back_type;

    public APIInterface apiInterface;

    private String strOwner, strOwnerName,
            strAccount, strAccountName,
            strWorkOrder, strWorkOrderName,
            strContact, strContactName,
            strDescription,
            strAdditionalInfo,
            strStatus, strStatusName,
            strInvoiceDate,
            strPaymentTerms, strPaymentTermsName,
            strDueDate,
            strBilLat, strBilLng,
            strBilAddress, strBilCity, strBilState, strBilCountry, strBilPostal;

    private Spinner spinnerWorkOrder, spinnerContact,
            spinnerStatus, spinnerPaymentTerms;

    private List<ModelSpinner> listOwners, listAccounts,
            listWorkOrder, listContacts,
            listStatus, listPaymentTerms;

    private ArrayAdapter<ModelSpinner> adapterWorkOrder, adapterContact,
            adapterStatus, adapterPaymentTerms;

    private EditText edittextOwner,
            edittextAccount,
            edittextDescription,
            edittextAdditionalInfo,
            edittextBilAddress,
            edittextBilCity,
            edittextBilState,
            edittextBilCountry,
            edittextBilPostal;

    private TextView textviewHeaderStatus,
            textviewInvoiceNo,
            textviewOwner,
            textviewAccount,
            textviewWorkOrder,
            textviewContact,
            textviewDescription,
            textviewAdditionalInfo,
            textviewStatus,
            textviewPaymentTerms,
            edittextInvoiceDate,
            edittextDueDate,
            textviewBilAddress,
            textviewBilCity,
            textviewBilState,
            textviewBilCountry,
            textviewBilPostal;

    private ImageButton buttonOwner;
    private LinearLayout layoutOwnerAdd;
    private EditText edittextOwnerAddSearch;
    private PlaceHolderView phvOwnerAdd;

    private ImageButton buttonAccount;
    private LinearLayout layoutAccountAdd;
    private EditText edittextAccountAddSearch;
    private PlaceHolderView phvAccountAdd;

    private ImageButton buttonBilAddress;

    private Button buttonSave, buttonCancel;

    public ClickListenInvoiceCreateClose clickClose;

    public interface ClickListenInvoiceCreateClose {
        void callbackInvoiceCreateClose(int back_type, boolean isSave, String invoiceID, String invoiceNo);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_invoice_create, container, false);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            back_type = bundle.getInt("back_type", 0);
        }

        apiInterface = APIClient.getClient().create(APIInterface.class);

        clickClose = (ClickListenInvoiceCreateClose) getActivity();

        spinnerWorkOrder = (Spinner) view.findViewById(R.id.spinner_work_order);
        spinnerContact = (Spinner) view.findViewById(R.id.spinner_contact);
        spinnerStatus = (Spinner) view.findViewById(R.id.spinner_status);
        spinnerPaymentTerms = (Spinner) view.findViewById(R.id.spinner_payment_terms);

        edittextOwner = (EditText) view.findViewById(R.id.edittext_owner);
        edittextAccount = (EditText) view.findViewById(R.id.edittext_account);
        edittextDescription = (EditText) view.findViewById(R.id.edittext_description);
        edittextAdditionalInfo = (EditText) view.findViewById(R.id.edittext_additional_info);

        edittextBilAddress = (EditText) view.findViewById(R.id.edittext_bil_address);
        edittextBilCity = (EditText) view.findViewById(R.id.edittext_bil_city);
        edittextBilState = (EditText) view.findViewById(R.id.edittext_bil_state);
        edittextBilCountry = (EditText) view.findViewById(R.id.edittext_bil_country);
        edittextBilPostal = (EditText) view.findViewById(R.id.edittext_bil_postal);

        textviewInvoiceNo = (TextView) view.findViewById(R.id.textview_invoice_no);
        textviewOwner = (TextView) view.findViewById(R.id.textview_owner);
        textviewAccount = (TextView) view.findViewById(R.id.textview_account);
        textviewWorkOrder = (TextView) view.findViewById(R.id.textview_work_order);
        textviewContact = (TextView) view.findViewById(R.id.textview_contact);
        textviewDescription = (TextView) view.findViewById(R.id.textview_description);
        textviewAdditionalInfo = (TextView) view.findViewById(R.id.textview_additional_info);
        textviewStatus = (TextView) view.findViewById(R.id.textview_status);
        textviewPaymentTerms = (TextView) view.findViewById(R.id.textview_payment_terms);
        edittextInvoiceDate = (TextView) view.findViewById(R.id.edittext_invoice_date);
        edittextDueDate = (TextView) view.findViewById(R.id.edittext_due_date);
        textviewBilAddress = (TextView) view.findViewById(R.id.textview_bil_address);
        textviewBilCity = (TextView) view.findViewById(R.id.textview_bil_city);
        textviewBilState = (TextView) view.findViewById(R.id.textview_bil_state);
        textviewBilCountry = (TextView) view.findViewById(R.id.textview_bil_country);
        textviewBilPostal = (TextView) view.findViewById(R.id.textview_bil_postal);

        buttonBilAddress = (ImageButton) view.findViewById(R.id.button_bil_address);

        buttonSave = (Button) view.findViewById(R.id.button_save);
        buttonCancel = (Button) view.findViewById(R.id.button_cancel);

        buttonOwner = (ImageButton) view.findViewById(R.id.button_owner);
        layoutOwnerAdd = (LinearLayout) view.findViewById(R.id.layout_owner_add);
        edittextOwnerAddSearch = (EditText) view.findViewById(R.id.edittext_owner_add_search);
        phvOwnerAdd = (PlaceHolderView) view.findViewById(R.id.phv_owner_add);

        buttonAccount = (ImageButton) view.findViewById(R.id.button_account);
        layoutAccountAdd = (LinearLayout) view.findViewById(R.id.layout_account_add);
        edittextAccountAddSearch = (EditText) view.findViewById(R.id.edittext_account_add_search);
        phvAccountAdd = (PlaceHolderView) view.findViewById(R.id.phv_account_add);

        strBilLat = "0.00";
        strBilLng = "0.00";
        strBilAddress = "";
        strBilCity = "";
        strBilState = "";
        strBilCountry = "";
        strBilPostal = "";

        Calendar calendar = Calendar.getInstance();
        Date date = Calendar.getInstance().getTime();
        calendar.setTime(date);
        int day = calendar.get(Calendar.DATE);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);
        calendar.set(year, month, day, hour, minute);
        String AM_PM = (String) android.text.format.DateFormat.format("aaa", calendar);
        if (hour > 12) {
            hour = hour - 12;
        }
        strInvoiceDate = setDate(day, month, year);
        edittextInvoiceDate.setText(formatDate(strInvoiceDate));
        strDueDate = setDate(day, month, year);
        edittextDueDate.setText(formatDate(strDueDate));

        listOwners = new ArrayList<>();
        listAccounts = new ArrayList<>();
        listWorkOrder = new ArrayList<>();
        listContacts = new ArrayList<>();
        listStatus = new ArrayList<>();
        listPaymentTerms = new ArrayList<>();
        adapterWorkOrder = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listWorkOrder);
        adapterWorkOrder.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterContact = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listContacts);
        adapterContact.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterStatus = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listStatus);
        adapterStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterPaymentTerms = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listPaymentTerms);
        adapterPaymentTerms.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        getOwners();
        getAccounts();
        getWorkOrders();
        getContacts();
        getStatus();
        getPaymentTerms();

        buttonAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutAccountAdd.setVisibility(View.VISIBLE);
                listAccounts.clear();
                getAccounts();
            }
        });

        buttonOwner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutOwnerAdd.setVisibility(View.VISIBLE);
                listOwners.clear();
                getOwners();
            }
        });

        edittextOwnerAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvOwnerAdd.removeAllViews();
                    ArrayList<ModelSpinner> modle = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listOwners) {
                        if (m.getName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvOwnerAdd
                                .addView(new AdapterOwner(getActivity(), FragmentInvoiceCreate.this, i, modle.get(i)));
                    }
                } else {
                    phvOwnerAdd.removeAllViews();
                    for (int i = 0; i < listOwners.size(); i++) {
                        phvOwnerAdd
                                .addView(new AdapterOwner(getActivity(), FragmentInvoiceCreate.this, i, listOwners.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        edittextAccountAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvAccountAdd.removeAllViews();
                    ArrayList<ModelSpinner> modle = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listAccounts) {
                        if (m.getName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvAccountAdd
                                .addView(new AdapterAccount(getActivity(), FragmentInvoiceCreate.this, i, modle.get(i)));
                    }
                } else {
                    phvAccountAdd.removeAllViews();
                    for (int i = 0; i < listAccounts.size(); i++) {
                        phvAccountAdd
                                .addView(new AdapterAccount(getActivity(), FragmentInvoiceCreate.this, i, listAccounts.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        edittextInvoiceDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                Date date = null;
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                try {
                    date = sdf.parse(strInvoiceDate);
                } catch (ParseException ex) {
                    Log.v("Exception", ex.getLocalizedMessage());
                }
                calendar.setTime(date);
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                final int hour = calendar.get(Calendar.HOUR);
                final int minute = calendar.get(Calendar.MINUTE);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        strInvoiceDate = setDate(d, m, y);
                        edittextInvoiceDate.setText(formatDate(strInvoiceDate));
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        edittextDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                Date date = null;
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                try {
                    date = sdf.parse(strDueDate);
                } catch (ParseException ex) {
                    Log.v("Exception", ex.getLocalizedMessage());
                }
                calendar.setTime(date);
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                final int hour = calendar.get(Calendar.HOUR);
                final int minute = calendar.get(Calendar.MINUTE);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        strDueDate = setDate(d, m, y);
                        edittextDueDate.setText(formatDate(strDueDate));
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        buttonBilAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!checkPermission()) {
                        requestPermission();
                    } else {
                        double lat = Double.valueOf(strBilLat);
                        double lng = Double.valueOf(strBilLng);
                        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                                .withLocation(lat, lng)
                                .withGeolocApiKey(getString(R.string.google_maps_key))
                                .withGooglePlacesEnabled()
                                .withSatelliteViewHidden()
                                .withVoiceSearchHidden()
                                .shouldReturnOkOnBackPressed()
                                .build(getActivity());
                        startActivityForResult(locationPickerIntent, 99);
                    }
                } else {
                    double lat = Double.valueOf(strBilLat);
                    double lng = Double.valueOf(strBilLng);
                    Intent locationPickerIntent = new LocationPickerActivity.Builder()
                            .withLocation(lat, lng)
                            .withGeolocApiKey(getString(R.string.google_maps_key))
                            .withGooglePlacesEnabled()
                            .withSatelliteViewHidden()
                            .withVoiceSearchHidden()
                            .shouldReturnOkOnBackPressed()
                            .build(getActivity());
                    startActivityForResult(locationPickerIntent, 99);
                }
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String owner = strOwner;
                String account = strAccount;
                String workOrder = listWorkOrder.get(spinnerWorkOrder.getSelectedItemPosition()).getId();
                String contact = listContacts.get(spinnerContact.getSelectedItemPosition()).getId();
                String description = edittextDescription.getText().toString().trim();
                String additionalInfo = edittextAdditionalInfo.getText().toString().trim();
                String status = listStatus.get(spinnerStatus.getSelectedItemPosition()).getId();
                String invoiceDate = strInvoiceDate;
                String paymentTerms = listPaymentTerms.get(spinnerPaymentTerms.getSelectedItemPosition()).getId();
                String dueDate = strDueDate;
                String bilAddress = edittextBilAddress.getText().toString().trim();
                String bilCity = edittextBilCity.getText().toString().trim();
                String bilState = edittextBilState.getText().toString().trim();
                String bilCountry = edittextBilCountry.getText().toString().trim();
                String bilPostal = edittextBilPostal.getText().toString().trim();
                String bilLat = strBilLat;
                String bilLng = strBilLng;
                if (TextUtils.isEmpty(description)) {
                    Utl.showToast(getActivity(), "Enter Description");
                } else if (TextUtils.isEmpty(bilAddress)) {
                    Utl.showToast(getActivity(), "Enter BillingAddress");
                } else if (TextUtils.isEmpty(bilCity)) {
                    Utl.showToast(getActivity(), "Enter Billing City");
                } else if (TextUtils.isEmpty(bilState)) {
                    Utl.showToast(getActivity(), "Enter Billing State");
                } else if (TextUtils.isEmpty(bilCountry)) {
                    Utl.showToast(getActivity(), "Enter Billing Country");
                } else if (TextUtils.isEmpty(bilPostal)) {
                    Utl.showToast(getActivity(), "Enter Billing Postal Code");
                } else {
                    createInvoice(owner,
                            account,
                            workOrder,
                            contact,
                            description,
                            additionalInfo,
                            status,
                            invoiceDate,
                            paymentTerms,
                            dueDate,
                            bilAddress, bilCity, bilState, bilCountry, bilPostal,
                            bilLat, bilLng);
                }

            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickClose.callbackInvoiceCreateClose(back_type, false, "", "");
            }
        });

        return view;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99) {
            if (data != null) {
                double latitude = data.getDoubleExtra("latitude", 0.0);
                strBilLat = String.valueOf(latitude);
                double longitude = data.getDoubleExtra("longitude", 0.0);
                strBilLng = String.valueOf(longitude);
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> addresses = new ArrayList<>();
                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                strBilAddress = addresses.get(0).getSubThoroughfare() + ", " + addresses.get(0).getThoroughfare();
                if (addresses.get(0).getLocality() != null) {
                    strBilCity = addresses.get(0).getLocality();
                } else {
                    strBilCity = addresses.get(0).getSubLocality();
                }
                if (addresses.get(0).getAdminArea() != null) {
                    strBilState = addresses.get(0).getAdminArea();
                } else {
                    strBilState = addresses.get(0).getSubAdminArea();
                }
                strBilCountry = addresses.get(0).getCountryName();
                strBilPostal = addresses.get(0).getPostalCode();
                Log.d("TAG_LATITUDE****", strBilLat);
                Log.d("TAG_LONGITUDE****", strBilLng);
                Log.d("TAG_ADDRESS****", strBilAddress);
                Log.d("TAG_CITY****", strBilCity);
                Log.d("TAG_STATE****", strBilState);
                Log.d("TAG_COUNTRY****", strBilCountry);
                Log.d("TAG_POSTAL****", strBilPostal);
                edittextBilAddress.setText(strBilAddress);
                edittextBilCity.setText(strBilCity);
                edittextBilState.setText(strBilState);
                edittextBilCountry.setText(strBilCountry);
                edittextBilPostal.setText(strBilPostal);
            } else {
                Log.d("RESULT", "CANCELLED");
            }
        }
    }

    private void setSpinnerDropDownHeight(List<ModelSpinner> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 70);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    public int getListIndex(List<ModelSpinner> list, String name) {
        for (int i = 0; i < list.size(); i++) {
            ModelSpinner model = list.get(i);
            if (name.equals(model.getName())) {
                return i;
            }
        }
        return -1;
    }

    public String formatDate(String time) {
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String setTime(int min, int hr, String AM_PM, int format) {
        String time = "" + hr + ":" + min + " " + AM_PM;
        String inputPattern;
        String outputPattern;
        if (format == 0) {
            inputPattern = "hh:mm aaa";
            outputPattern = "hh:mm aaa";
        } else {
            inputPattern = "hh:mm aaa";
            outputPattern = "kk:mm:ss";
        }
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String setDate(int d, int m, int y) {
        String time = "" + (m + 1) + "/" + d + "/" + y;
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "MM/dd/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void getOwners() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getInvoiceOwners(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Invoice_AllU" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listOwners.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_UserID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listOwners.add(model);
                            }
                            for (int i = 0; i < listOwners.size(); i++) {
                                phvOwnerAdd
                                        .addView(new AdapterOwner(getActivity(), FragmentInvoiceCreate.this, i, listOwners.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getAccounts() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getInvoiceAccounts(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Accounts" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAccounts.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_AccountID));
                                model.setName(dataObject.getString(Cons.KEY_AccountName));
                                listAccounts.add(model);
                            }
                            for (int i = 0; i < listAccounts.size(); i++) {
                                phvAccountAdd
                                        .addView(new AdapterAccount(getActivity(), FragmentInvoiceCreate.this, i, listAccounts.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getWorkOrders() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getInvoiceWorkOrders(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_WorkOrder" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listWorkOrder.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_WorkOrderID));
                                model.setName(dataObject.getString(Cons.KEY_Subject));
                                listWorkOrder.add(model);
                            }
                            spinnerWorkOrder.setAdapter(adapterWorkOrder);
                            setSpinnerDropDownHeight(listWorkOrder, spinnerWorkOrder);
                            spinnerWorkOrder.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getContacts() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getInvoiceContacts(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Contacts" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listContacts.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_ContactID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listContacts.add(model);
                            }
                            spinnerContact.setAdapter(adapterContact);
                            setSpinnerDropDownHeight(listContacts, spinnerContact);
                            spinnerContact.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getStatus() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getInvoiceStatus(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Status" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listStatus.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_InvoiceStatusID));
                                model.setName(dataObject.getString(Cons.KEY_InvoiceStatus));
                                listStatus.add(model);
                            }
                            spinnerStatus.setAdapter(adapterStatus);
                            setSpinnerDropDownHeight(listStatus, spinnerStatus);
                            spinnerStatus.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }


    private void getPaymentTerms() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getInvoicePaymentTerms(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Status" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listPaymentTerms.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_InvoicePaymentTermID));
                                model.setName(dataObject.getString(Cons.KEY_PaymentTerms));
                                listPaymentTerms.add(model);
                            }
                            spinnerPaymentTerms.setAdapter(adapterPaymentTerms);
                            setSpinnerDropDownHeight(listPaymentTerms, spinnerPaymentTerms);
                            spinnerPaymentTerms.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void createInvoice(String owner,
                               String account,
                               String workOrder,
                               String contact,
                               String description,
                               String additionalInfo,
                               String status,
                               String invoiceDate,
                               String paymentTerms,
                               String dueDate,
                               String bilAddress, String bilCity, String bilState, String bilCountry, String bilPostal,
                               String bilLat, String bilLng) {

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AssignedTo, RequestBody.create(MediaType.parse("text/plain"), owner));
        map.put(Cons.KEY_Account, RequestBody.create(MediaType.parse("text/plain"), account));
        map.put(Cons.KEY_WorkOrder, RequestBody.create(MediaType.parse("text/plain"), workOrder));
        map.put(Cons.KEY_Contact, RequestBody.create(MediaType.parse("text/plain"), contact));
        map.put(Cons.KEY_Description, RequestBody.create(MediaType.parse("text/plain"), description));
        if (!TextUtils.isEmpty(additionalInfo)) {
            map.put(Cons.KEY_AdditionalInformation, RequestBody.create(MediaType.parse("text/plain"), additionalInfo));
        }
        map.put(Cons.KEY_InvoiceStatus, RequestBody.create(MediaType.parse("text/plain"), status));
        map.put(Cons.KEY_InvoiceDate, RequestBody.create(MediaType.parse("text/plain"), invoiceDate));
        map.put(Cons.KEY_PaymentTerms, RequestBody.create(MediaType.parse("text/plain"), paymentTerms));
        map.put(Cons.KEY_DueDate, RequestBody.create(MediaType.parse("text/plain"), dueDate));
        map.put(Cons.KEY_Latitude, RequestBody.create(MediaType.parse("text/plain"), bilLat));
        map.put(Cons.KEY_Longitude, RequestBody.create(MediaType.parse("text/plain"), bilLng));
        map.put(Cons.KEY_Address, RequestBody.create(MediaType.parse("text/plain"), bilAddress));
        map.put(Cons.KEY_City, RequestBody.create(MediaType.parse("text/plain"), bilCity));
        map.put(Cons.KEY_State, RequestBody.create(MediaType.parse("text/plain"), bilState));
        map.put(Cons.KEY_Country, RequestBody.create(MediaType.parse("text/plain"), bilCountry));
        map.put(Cons.KEY_PostalCode, RequestBody.create(MediaType.parse("text/plain"), bilPostal));
        Call<ResponseBody> response;
        response = apiInterface.createInvoiceDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Edit" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            //JSONObject dataObject = object.getJSONObject("data");
                            String invoiceID = object.getString(Cons.KEY_InvoiceID);
                            String invoiceNo = object.getString(Cons.KEY_InvoiceNo);
                            clickClose.callbackInvoiceCreateClose(back_type, true, invoiceID, invoiceNo);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    public boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), CAMERA);
        int result2 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION);
        int result3 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_COARSE_LOCATION);
        int result4 = ContextCompat.checkSelfPermission(getActivity(), CALL_PHONE);
        int result5 = ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE);
        int result6 = ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE);
        return
                result1 == PackageManager.PERMISSION_GRANTED
                        && result2 == PackageManager.PERMISSION_GRANTED
                        && result3 == PackageManager.PERMISSION_GRANTED
                        && result4 == PackageManager.PERMISSION_GRANTED
                        && result5 == PackageManager.PERMISSION_GRANTED
                        && result6 == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{
                        CAMERA,
                        ACCESS_FINE_LOCATION,
                        ACCESS_COARSE_LOCATION,
                        CALL_PHONE,
                        READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean locationFineAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean locationCoarseAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean callAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean readAccepted = grantResults[4] == PackageManager.PERMISSION_GRANTED;
                    boolean writeAccepted = grantResults[5] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted &&
                            locationFineAccepted &&
                            locationCoarseAccepted &&
                            callAccepted &&
                            readAccepted &&
                            writeAccepted)
                        Utl.showToast(getActivity(), "Permission Granted.");
                    else {
                        Utl.showToast(getActivity(), "Permission Denied.");
                    }
                }
                break;
        }
    }

    @Override
    public void callbackAddOwnerListen(int pos, ModelSpinner model) {
        phvOwnerAdd.removeAllViews();
        layoutOwnerAdd.setVisibility(View.GONE);
        strOwner = model.getId();
        strOwnerName = model.getName();
        edittextOwner.setText(strOwnerName);
    }

    @Override
    public void callbackAddAccountListen(int pos, ModelSpinner model) {
        phvAccountAdd.removeAllViews();
        layoutAccountAdd.setVisibility(View.GONE);
        strAccount = model.getId();
        strAccountName = model.getName();
        edittextAccount.setText(strAccountName);
    }

}