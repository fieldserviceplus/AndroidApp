package com.fieldwise.fragments;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.fieldwise.R;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;
import com.williamww.silkysignature.views.SignaturePad;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentEstimateSignature extends Fragment {

    public APIInterface apiInterface;

    private String estimateId, estimateNo;

    private SignaturePad signaturePad;
    private Button buttonSignSave, buttonSignCancel;

    public AddEstimateSignatureListener clickSignature;

    public interface AddEstimateSignatureListener {
        void callbackAddEstimateSignatureListener(String estimateId, String estimateNo);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signature, container, false);

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        clickSignature = (AddEstimateSignatureListener) getActivity();

        estimateId = getArguments().getString(Cons.KEY_EstimateID);
        estimateNo = getArguments().getString(Cons.KEY_EstimateNo);

        signaturePad = (SignaturePad) view.findViewById(R.id.signature_pad);
        buttonSignSave = (Button) view.findViewById(R.id.button_sign_save);
        buttonSignCancel = (Button) view.findViewById(R.id.button_sign_cancel);

        buttonSignSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                Bitmap bitmap = signaturePad.getSignatureBitmap();
                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), false);
                scaledBitmap.compress(Bitmap.CompressFormat.PNG, 90, outputStream);
                String signature = "data:image/png;base64," + Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
                addEstimateSignature(estimateId, signature);
            }
        });

        buttonSignCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signaturePad.clear();
                if (clickSignature != null) {
                    clickSignature.callbackAddEstimateSignatureListener(estimateId, estimateNo);
                }
            }
        });
        return view;

    }

    private void addEstimateSignature(final String estimateId, final String signature) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_EstimateID, RequestBody.create(MediaType.parse("text/plain"), estimateId));
        map.put(Cons.KEY_Signature, RequestBody.create(MediaType.parse("text/plain"), signature));
        Call<ResponseBody> response = apiInterface.addEstimateSignature(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        int intCode = object.getInt("ResponseCode");
                        if (intCode == 1) {
                            signaturePad.clear();
                            if (clickSignature != null) {
                                clickSignature.callbackAddEstimateSignatureListener(estimateId, estimateNo);
                            }
                        }
                        String strMessage = object.getString("ResponseMsg");
                        Utl.showToast(getActivity(), strMessage);
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

}