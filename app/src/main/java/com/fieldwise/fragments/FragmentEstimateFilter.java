package com.fieldwise.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.fieldwise.R;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentEstimateFilter extends Fragment {

    public APIInterface apiInterface;

    private String estimateView;

    private Button buttonCancel, buttonApply, buttonAddFilter, buttonClearAll;

    private LinearLayout layoutAddFilter;

    private String[] strFilterConditions = {"Equals",
            "Contains",
            "StartsWith",
            "DoesNotContain",
            "NotEqualTo",
            "LessThan",
            "GreaterThan",
            "LessOREqualTo",
            "GreaterOREqualTo"};
    private ArrayAdapter adapterFilterConditions;

    private List<String> listFilterFields;
    private ArrayAdapter adapterFilterFields;

    public ClickListenEstimateFilterCancel clickFilterCancel;

    public interface ClickListenEstimateFilterCancel {
        void callbackEstimateFilterCancel();
    }

    public ClickListenEstimateFilterAdd clickFilterAdd;

    public interface ClickListenEstimateFilterAdd {
        void callbackEstimateFilterAdd(String accountView, String sbFields, String sbConditions, String sbValues);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_estimate_filter, container, false);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        clickFilterCancel = (ClickListenEstimateFilterCancel) getActivity();
        clickFilterAdd = (ClickListenEstimateFilterAdd) getActivity();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            estimateView = bundle.getString(Cons.KEY_EstimateViewID, "");
        }

        buttonCancel = (Button) view.findViewById(R.id.button_cancel);
        buttonApply = (Button) view.findViewById(R.id.button_apply);
        buttonAddFilter = (Button) view.findViewById(R.id.button_add_filter);
        buttonClearAll = (Button) view.findViewById(R.id.button_clear_all);

        layoutAddFilter = (LinearLayout) view.findViewById(R.id.layout_add_filter);

        adapterFilterConditions = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, strFilterConditions);
        adapterFilterConditions.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        listFilterFields = new ArrayList<>();
        adapterFilterFields = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, listFilterFields);
        adapterFilterFields.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        getFields();

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickFilterCancel != null) {
                    clickFilterCancel.callbackEstimateFilterCancel();
                }
            }
        });

        buttonApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickFilterAdd != null) {
                    StringBuilder sbFields = new StringBuilder();
                    StringBuilder sbConditions = new StringBuilder();
                    StringBuilder sbValues = new StringBuilder();
                    int size = layoutAddFilter.getChildCount();
                    for (int i = 0; i < size; i++) {
                        View v = layoutAddFilter.getChildAt(i);
                        Spinner spinnerFilterFields = (Spinner) v.findViewById(R.id.spinner_filter_fields);
                        Spinner spinnerFilterConditions = (Spinner) v.findViewById(R.id.spinner_filter_conditions);
                        EditText edittextFilterValues = (EditText) v.findViewById(R.id.edittext_filter_values);
                        sbFields.append(spinnerFilterFields.getSelectedItem() + ",");
                        sbConditions.append(spinnerFilterConditions.getSelectedItem() + ",");
                        sbValues.append(edittextFilterValues.getText().toString().trim() + ",");
                    }
                    String strFields = sbFields.toString();
                    String strConditions = sbConditions.toString();
                    String strValues = sbValues.toString();
                    if (strFields.endsWith(",")) {
                        strFields = strFields.substring(0, strFields.length() - 1);
                    }
                    if (strConditions.endsWith(",")) {
                        strConditions = strConditions.substring(0, strConditions.length() - 1);
                    }
                    if (strValues.endsWith(",")) {
                        strValues = strValues.substring(0, strValues.length() - 1);
                    }
                    clickFilterAdd.callbackEstimateFilterAdd(estimateView, strFields, strConditions, strValues);
                }
            }
        });

        buttonAddFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = layoutInflater.inflate(R.layout.layout_filter, null);

                layoutAddFilter.addView(v);

                int size = layoutAddFilter.getChildCount();
                final View viw = layoutAddFilter.getChildAt(size - 1);

                Spinner spinnerFilterFields = (Spinner) viw.findViewById(R.id.spinner_filter_fields);
                setSpinnerDropDownHeight(listFilterFields, spinnerFilterFields);
                spinnerFilterFields.setAdapter(adapterFilterFields);

                Spinner spinnerFilterConditions = (Spinner) viw.findViewById(R.id.spinner_filter_conditions);
                setSimpleSpinnerDropDownHeight(strFilterConditions, spinnerFilterConditions);
                spinnerFilterConditions.setAdapter(adapterFilterConditions);

                EditText edittextFilterValues = (EditText) viw.findViewById(R.id.edittext_filter_values);

                Button btnCancel = (Button) viw.findViewById(R.id.btn_cancel);
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        layoutAddFilter.removeView(viw);
                    }
                });

            }
        });

        buttonClearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutAddFilter.removeAllViews();
            }
        });

        return view;
    }

    private void setSimpleSpinnerDropDownHeight(String[] str, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 140);
        if (str.length > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    private void setSpinnerDropDownHeight(List<String> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 140);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    private void getFields() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_EstimateViewID, RequestBody.create(MediaType.parse("text/plain"), estimateView));
        Call<ResponseBody> response = apiInterface.getEstimateFilter(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Filter" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listFilterFields.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                listFilterFields.add(dataObject.getString(Cons.KEY_FieldName));
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

}