package com.fieldwise.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.activities.ActivityMain;
import com.fieldwise.adapters.AdapterAssignedTo;
import com.fieldwise.models.ModelSpinner;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;
import com.fxn.pix.Pix;
import com.mindorks.placeholderview.PlaceHolderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class FragmentFileInfo extends Fragment implements
        AdapterAssignedTo.AddAssignedToClickListen {

    private static final int PERMISSION_REQUEST_CODE = 200;

    public APIInterface apiInterface;

    private String fileId;

    private String strSubject,
            strAssignedTo, strAssignedToNew, strAssignedToName, strAssignedToNameNew,
            strRelatedTo,
            strWhat, strWhatName,
            strDescription,
            strFileName, strFileNameNew,
            strCreatedDate, strLastModifiedDate, strCreatedBy, strLastModifiedBy;

    private boolean isCloneMode = false;
    private boolean isAssignedToClicked = false;
    private boolean isFileClicked = false;

    private RelativeLayout layoutAssignedTo,
            layoutRelatedTo,
            layoutWhat,
            layoutFileName;

    private LinearLayout layoutSystemInfo;

    private Spinner spinnerRelatedTo,
            spinnerWhat;

    private List<ModelSpinner> listAssignedTo,
            listRelatedTo,
            listWhat;

    private ArrayAdapter<ModelSpinner> adapterRelatedTo,
            adapterWhat;

    private EditText editTextSubject,
            edittextAssignedTo,
            edittextDescription;

    private TextView textviewSubject,
            textviewAssignedTo,
            textviewRelatedTo,
            textviewWhat,
            edittextFileName, textviewFileName,
            textviewDescription,
            textviewCreatedDate,
            textviewCreatedBy,
            textviewLastModifiedDate,
            textviewLastModifiedBy;

    private Button buttonFileBrowse;

    private LinearLayout layoutNavigation, layoutSaveCancel;
    private ImageButton buttonCall, buttonComment, buttonDate, buttonEdit, buttonMore;
    private Button buttonSave, buttonCancel;

    private ImageButton buttonAssignedTo;
    private LinearLayout layoutAssignedToAdd;
    private EditText edittextAssignedToAddSearch;
    private PlaceHolderView phvAssignedToAdd;

    private String attachmentPath;

    public GoToRecent clickGoToRecent;

    public interface GoToRecent {
        void callbackGoToRecentFile(String strObject);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_file_detailed, container, false);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        clickGoToRecent = (GoToRecent) getActivity();

        layoutAssignedTo = (RelativeLayout) view.findViewById(R.id.layout_assigned_to);
        layoutRelatedTo = (RelativeLayout) view.findViewById(R.id.layout_related_to);
        layoutWhat = (RelativeLayout) view.findViewById(R.id.layout_what);
        layoutFileName = (RelativeLayout) view.findViewById(R.id.layout_file_name);
        layoutSystemInfo = (LinearLayout) view.findViewById(R.id.layout_system_info);

        spinnerRelatedTo = (Spinner) view.findViewById(R.id.spinner_related_to);
        spinnerWhat = (Spinner) view.findViewById(R.id.spinner_what);

        editTextSubject = (EditText) view.findViewById(R.id.edittext_subject);
        edittextAssignedTo = (EditText) view.findViewById(R.id.edittext_assigned_to);
        edittextDescription = (EditText) view.findViewById(R.id.edittext_description);

        textviewSubject = (TextView) view.findViewById(R.id.textview_subject);
        textviewAssignedTo = (TextView) view.findViewById(R.id.textview_assigned_to);
        textviewRelatedTo = (TextView) view.findViewById(R.id.textview_related_to);
        textviewWhat = (TextView) view.findViewById(R.id.textview_what);
        edittextFileName = (TextView) view.findViewById(R.id.edittext_file_name);
        textviewFileName = (TextView) view.findViewById(R.id.textview_file_name);
        buttonFileBrowse = (Button) view.findViewById(R.id.button_file_browse);
        textviewDescription = (TextView) view.findViewById(R.id.textview_description);
        textviewCreatedDate = (TextView) view.findViewById(R.id.textview_created_date);
        textviewCreatedBy = (TextView) view.findViewById(R.id.textview_created_by);
        textviewLastModifiedDate = (TextView) view.findViewById(R.id.textview_last_modified_date);
        textviewLastModifiedBy = (TextView) view.findViewById(R.id.textview_last_modified_by);

        layoutNavigation = (LinearLayout) view.findViewById(R.id.layout_navigation);
        buttonCall = (ImageButton) view.findViewById(R.id.button_call);
        buttonComment = (ImageButton) view.findViewById(R.id.button_comment);
        buttonDate = (ImageButton) view.findViewById(R.id.button_date);
        buttonEdit = (ImageButton) view.findViewById(R.id.button_edit);
        buttonMore = (ImageButton) view.findViewById(R.id.button_more);
        layoutSaveCancel = (LinearLayout) view.findViewById(R.id.layout_save_cancel);
        buttonSave = (Button) view.findViewById(R.id.button_save);
        buttonCancel = (Button) view.findViewById(R.id.button_cancel);

        buttonAssignedTo = (ImageButton) view.findViewById(R.id.button_assigned_to);
        layoutAssignedToAdd = (LinearLayout) view.findViewById(R.id.layout_assigned_to_add);
        edittextAssignedToAddSearch = (EditText) view.findViewById(R.id.edittext_assigned_to_add_search);
        phvAssignedToAdd = (PlaceHolderView) view.findViewById(R.id.phv_assigned_to_add);

        listAssignedTo = new ArrayList<>();
        listRelatedTo = new ArrayList<>();
        listWhat = new ArrayList<>();
        adapterRelatedTo = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listRelatedTo);
        adapterRelatedTo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterWhat = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listWhat);
        adapterWhat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        editMode(false, false);

        fileId = getArguments().getString(Cons.KEY_FileID);

        getFileDetailed(fileId);

        getAssignedTo();
        getRelatedTo();

        buttonAssignedTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isAssignedToClicked = true;
                layoutAssignedToAdd.setVisibility(View.VISIBLE);
                listAssignedTo.clear();
                getAssignedTo();
            }
        });

        spinnerRelatedTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String str = listRelatedTo.get(position).getName();
                getWhat(str);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        buttonFileBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!checkPermission()) {
                        requestPermission();
                    } else {
                        dialogBrowse();
                    }
                } else {
                    dialogBrowse();
                }
            }
        });

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editMode(true, true);
            }
        });

        buttonMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String subject = editTextSubject.getText().toString().trim();
                String assignedTo = "";
                if (isAssignedToClicked) {
                    assignedTo = strAssignedToNew;
                } else {
                    assignedTo = strAssignedTo;
                }
                String relatedTo = listRelatedTo.get(spinnerRelatedTo.getSelectedItemPosition()).getName();
                String what = listWhat.get(spinnerWhat.getSelectedItemPosition()).getId();
                String description = edittextDescription.getText().toString().trim();

                if (TextUtils.isEmpty(subject)) {
                    Utl.showToast(getActivity(), "Enter Subject");
                } else if (!isFileClicked) {
                    Utl.showToast(getActivity(), "Select File");
                } else {
                    if (isCloneMode) {
                        createFile(subject,
                                assignedTo,
                                relatedTo,
                                what,
                                description);
                    } else {
                        editFile(fileId,
                                subject,
                                assignedTo,
                                relatedTo,
                                what,
                                description);
                    }
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCloneMode = false;
                editMode(false, true);
            }
        });

        edittextAssignedToAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvAssignedToAdd.removeAllViews();
                    ArrayList<ModelSpinner> modle = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listAssignedTo) {
                        if (m.getName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvAssignedToAdd
                                .addView(new AdapterAssignedTo(getActivity(), FragmentFileInfo.this, i, modle.get(i)));
                    }
                } else {
                    phvAssignedToAdd.removeAllViews();
                    for (int i = 0; i < listAssignedTo.size(); i++) {
                        phvAssignedToAdd
                                .addView(new AdapterAssignedTo(getActivity(), FragmentFileInfo.this, i, listAssignedTo.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        return view;

    }

    private void editMode(boolean bolEdit, boolean bolResponse) {
        if (!bolEdit) {
            editTextSubject.setVisibility(View.GONE);
            textviewSubject.setVisibility(View.VISIBLE);

            layoutAssignedTo.setVisibility(View.GONE);
            textviewAssignedTo.setVisibility(View.VISIBLE);

            layoutRelatedTo.setVisibility(View.GONE);
            spinnerRelatedTo.setVisibility(View.GONE);
            spinnerRelatedTo.setAdapter(null);
            textviewRelatedTo.setVisibility(View.VISIBLE);

            layoutWhat.setVisibility(View.GONE);
            spinnerWhat.setVisibility(View.GONE);
            spinnerWhat.setAdapter(null);
            textviewWhat.setVisibility(View.VISIBLE);

            layoutFileName.setVisibility(View.GONE);
            buttonFileBrowse.setVisibility(View.GONE);
            edittextFileName.setVisibility(View.GONE);
            textviewFileName.setVisibility(View.VISIBLE);

            edittextDescription.setVisibility(View.GONE);
            textviewDescription.setVisibility(View.VISIBLE);

            buttonCall.setEnabled(true);
            buttonComment.setEnabled(true);
            buttonDate.setEnabled(true);
            buttonEdit.setEnabled(true);

            layoutSystemInfo.setVisibility(View.VISIBLE);
            layoutSaveCancel.setVisibility(View.GONE);
            layoutNavigation.setVisibility(View.VISIBLE);
            if (bolResponse) {
                setViewInfo();
            }
        } else {

            editTextSubject.setVisibility(View.VISIBLE);
            textviewSubject.setVisibility(View.GONE);

            layoutAssignedTo.setVisibility(View.VISIBLE);
            textviewAssignedTo.setVisibility(View.GONE);

            layoutRelatedTo.setVisibility(View.VISIBLE);
            spinnerRelatedTo.setVisibility(View.VISIBLE);
            spinnerRelatedTo.setAdapter(adapterRelatedTo);
            textviewRelatedTo.setVisibility(View.GONE);

            layoutWhat.setVisibility(View.VISIBLE);
            spinnerWhat.setVisibility(View.VISIBLE);
            spinnerWhat.setAdapter(adapterWhat);
            textviewWhat.setVisibility(View.GONE);

            layoutFileName.setVisibility(View.VISIBLE);
            buttonFileBrowse.setVisibility(View.VISIBLE);
            edittextFileName.setVisibility(View.VISIBLE);
            textviewFileName.setVisibility(View.GONE);

            edittextDescription.setVisibility(View.VISIBLE);
            textviewDescription.setVisibility(View.GONE);

            buttonCall.setEnabled(false);
            buttonComment.setEnabled(false);
            buttonDate.setEnabled(false);
            buttonEdit.setEnabled(false);

            layoutSystemInfo.setVisibility(View.GONE);
            layoutSaveCancel.setVisibility(View.VISIBLE);
            layoutNavigation.setVisibility(View.GONE);
            if (bolResponse) {
                setEditInfo();
            }
        }
    }

    private void cloneMode() {

        editTextSubject.setVisibility(View.VISIBLE);
        textviewSubject.setVisibility(View.GONE);

        layoutAssignedTo.setVisibility(View.VISIBLE);
        textviewAssignedTo.setVisibility(View.GONE);

        layoutRelatedTo.setVisibility(View.VISIBLE);
        spinnerRelatedTo.setVisibility(View.VISIBLE);
        spinnerRelatedTo.setAdapter(adapterRelatedTo);
        textviewRelatedTo.setVisibility(View.GONE);

        layoutWhat.setVisibility(View.VISIBLE);
        spinnerWhat.setVisibility(View.VISIBLE);
        spinnerWhat.setAdapter(adapterWhat);
        textviewWhat.setVisibility(View.GONE);

        layoutFileName.setVisibility(View.VISIBLE);
        buttonFileBrowse.setVisibility(View.VISIBLE);
        edittextFileName.setVisibility(View.VISIBLE);
        textviewFileName.setVisibility(View.GONE);

        edittextDescription.setVisibility(View.VISIBLE);
        textviewDescription.setVisibility(View.GONE);

        buttonCall.setEnabled(false);
        buttonComment.setEnabled(false);
        buttonDate.setEnabled(false);
        buttonEdit.setEnabled(false);

        layoutSystemInfo.setVisibility(View.GONE);
        layoutSaveCancel.setVisibility(View.VISIBLE);
        layoutNavigation.setVisibility(View.GONE);

        setCloneInfo();
    }

    private void setViewInfo() {
        ActivityMain.textviewCenter.setText(strSubject);
        textviewSubject.setText(strSubject);
        textviewAssignedTo.setText(strAssignedToName);
        textviewRelatedTo.setText(strRelatedTo);
        textviewWhat.setText(strWhatName);
        textviewFileName.setText(strFileName);
        textviewDescription.setText(strDescription);
        textviewCreatedDate.setText(strCreatedDate);
        textviewCreatedBy.setText(strCreatedBy);
        textviewLastModifiedDate.setText(strLastModifiedDate);
        textviewLastModifiedBy.setText(strLastModifiedBy);
    }

    private void setEditInfo() {
        editTextSubject.setText(strSubject);
        if (isAssignedToClicked) {
            edittextAssignedTo.setText(strAssignedToNameNew);
        } else {
            edittextAssignedTo.setText(strAssignedToName);
        }
        setSpinnerDropDownHeight(listRelatedTo, spinnerRelatedTo);
        setSpinnerDropDownHeight(listWhat, spinnerWhat);
        spinnerRelatedTo.setSelection(getListIndex(listRelatedTo, strRelatedTo));
        spinnerWhat.setSelection(getListIndex(listWhat, strWhatName));
        if (isFileClicked) {
            edittextFileName.setText(strFileNameNew);
        } else {
            edittextFileName.setText(strFileName);
        }
        edittextDescription.setText(strDescription);
    }

    private void setCloneInfo() {
        isCloneMode = true;
        ActivityMain.textviewCenter.setText(getString(R.string.file_create));
        editTextSubject.setText(strSubject);
        if (isAssignedToClicked) {
            edittextAssignedTo.setText(strAssignedToNameNew);
        } else {
            edittextAssignedTo.setText(strAssignedToName);
        }
        setSpinnerDropDownHeight(listRelatedTo, spinnerRelatedTo);
        setSpinnerDropDownHeight(listWhat, spinnerWhat);
        spinnerRelatedTo.setSelection(getListIndex(listRelatedTo, strRelatedTo));
        spinnerWhat.setSelection(getListIndex(listWhat, strWhatName));
        if (isFileClicked) {
            edittextFileName.setText(strFileNameNew);
        } else {
            edittextFileName.setText(strFileName);
        }
        edittextDescription.setText(strDescription);
    }

    private void setSpinnerDropDownHeight(List<ModelSpinner> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 70);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    public int getListIndex(List<ModelSpinner> list, String name) {
        for (int i = 0; i < list.size(); i++) {
            ModelSpinner model = list.get(i);
            if (name.equals(model.getName())) {
                return i;
            }
        }
        return -1;
    }

    private void dialogBrowse() {
        final Dialog dialogMore = new Dialog(getActivity(), R.style.DialogFullScreen);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMore.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        dialogMore.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMore.setCancelable(true);
        dialogMore.setCanceledOnTouchOutside(true);
        dialogMore.setContentView(R.layout.dialog_file_browse);
        TextView buttonCamera = (TextView) dialogMore.findViewById(R.id.button_camera);
        TextView buttonGallery = (TextView) dialogMore.findViewById(R.id.button_gallery);
        TextView buttonFile = (TextView) dialogMore.findViewById(R.id.button_file);
        Button buttonClose = (Button) dialogMore.findViewById(R.id.button_close);

        buttonCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Pix.start(getActivity(), 9001, 1);
                dialogMore.dismiss();
            }
        });

        buttonGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent, 9002);
                dialogMore.dismiss();
            }
        });

        buttonFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("*/*");
                startActivityForResult(intent, 9003);
                dialogMore.dismiss();
            }
        });

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
            }
        });

        dialogMore.show();
        dialogMore.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialogMore.getWindow().setDimAmount(0.5f);
        dialogMore.getWindow().setAttributes(lp);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 9001:
                if (resultCode == Pix.RESULT_OK) {
                    //isFileClicked = true;
                    //ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    attachmentPath = cursor.getString(columnIndex);
                    cursor.close();
                } else if (resultCode == Pix.RESULT_CANCELED) {
                    Utl.showToast(getActivity(), "Cancelled");
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
                break;
            case 9002:
                if (resultCode == RESULT_OK) {
                    isFileClicked = true;
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    attachmentPath = cursor.getString(columnIndex);
                    cursor.close();
                } else if (resultCode == RESULT_CANCELED) {
                    Utl.showToast(getActivity(), "Cancelled");
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
                break;
            case 9003:
                if (resultCode == RESULT_OK) {
                    isFileClicked = true;
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    attachmentPath = cursor.getString(columnIndex);
                    cursor.close();
                } else if (resultCode == RESULT_CANCELED) {
                    Utl.showToast(getActivity(), "Cancelled");
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
                break;
        }
    }

    private void dialogMore() {
        final Dialog dialogMore = new Dialog(getActivity(), R.style.DialogFullScreen);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMore.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        dialogMore.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMore.setCancelable(true);
        dialogMore.setCanceledOnTouchOutside(true);
        dialogMore.setContentView(R.layout.dialog_more);

        TextView buttonMoreCall = (TextView) dialogMore.findViewById(R.id.button_more_call);
        TextView buttonMoreText = (TextView) dialogMore.findViewById(R.id.button_more_text);
        TextView buttonMoreEmail = (TextView) dialogMore.findViewById(R.id.button_more_email);
        TextView buttonMoreEdit = (TextView) dialogMore.findViewById(R.id.button_more_edit);
        TextView buttonMoreSign = (TextView) dialogMore.findViewById(R.id.button_more_sign);
        TextView buttonMoreDoc = (TextView) dialogMore.findViewById(R.id.button_more_doc);
        TextView buttonMoreNewLine = (TextView) dialogMore.findViewById(R.id.button_more_new_line);
        TextView buttonMoreChemical = (TextView) dialogMore.findViewById(R.id.button_more_new_chemical);
        TextView buttonMoreEvent = (TextView) dialogMore.findViewById(R.id.button_more_new_event);
        TextView buttonMoreInvoice = (TextView) dialogMore.findViewById(R.id.button_more_new_invoice);
        TextView buttonMoreFile = (TextView) dialogMore.findViewById(R.id.button_more_new_file);
        TextView buttonMoreNote = (TextView) dialogMore.findViewById(R.id.button_more_new_note);
        TextView buttonMoreTask = (TextView) dialogMore.findViewById(R.id.button_more_new_task);
        TextView buttonMoreConvert = (TextView) dialogMore.findViewById(R.id.button_more_convert);
        TextView buttonMoreClone = (TextView) dialogMore.findViewById(R.id.button_more_clone);
        TextView buttonMoreDelete = (TextView) dialogMore.findViewById(R.id.button_more_delete);
        View buttomViewCall = (View) dialogMore.findViewById(R.id.view_more_call);
        View buttomViewText = (View) dialogMore.findViewById(R.id.view_more_text);
        View buttomViewEmail = (View) dialogMore.findViewById(R.id.view_more_email);
        View buttomViewEdit = (View) dialogMore.findViewById(R.id.view_more_edit);
        View buttomViewSign = (View) dialogMore.findViewById(R.id.view_more_sign);
        View buttomViewDoc = (View) dialogMore.findViewById(R.id.view_more_doc);
        View buttomViewNewLine = (View) dialogMore.findViewById(R.id.view_more_new_line);
        View buttomViewChemical = (View) dialogMore.findViewById(R.id.view_more_new_chemical);
        View buttomViewEvent = (View) dialogMore.findViewById(R.id.view_more_new_event);
        View buttomViewInvoice = (View) dialogMore.findViewById(R.id.view_more_new_invoice);
        View buttomViewFile = (View) dialogMore.findViewById(R.id.view_more_new_file);
        View buttomViewNote = (View) dialogMore.findViewById(R.id.view_more_new_note);
        View buttomViewTask = (View) dialogMore.findViewById(R.id.view_more_new_task);
        View buttomViewConvert = (View) dialogMore.findViewById(R.id.view_more_convert);
        View buttomViewClone = (View) dialogMore.findViewById(R.id.view_more_clone);
        Button buttonMoreClose = (Button) dialogMore.findViewById(R.id.button_more_close);

        buttonMoreEmail.setVisibility(View.GONE);
        buttomViewEmail.setVisibility(View.GONE);
        buttonMoreSign.setVisibility(View.GONE);
        buttomViewSign.setVisibility(View.GONE);
        buttonMoreDoc.setVisibility(View.GONE);
        buttomViewDoc.setVisibility(View.GONE);
        buttonMoreNewLine.setVisibility(View.GONE);
        buttomViewNewLine.setVisibility(View.GONE);
        buttonMoreChemical.setVisibility(View.GONE);
        buttomViewChemical.setVisibility(View.GONE);
        buttonMoreEvent.setVisibility(View.GONE);
        buttomViewEvent.setVisibility(View.GONE);
        buttonMoreInvoice.setVisibility(View.GONE);
        buttomViewInvoice.setVisibility(View.GONE);
        buttonMoreFile.setVisibility(View.GONE);
        buttomViewFile.setVisibility(View.GONE);
        buttonMoreNote.setVisibility(View.GONE);
        buttomViewNote.setVisibility(View.GONE);
        buttonMoreTask.setVisibility(View.GONE);
        buttomViewTask.setVisibility(View.GONE);
        buttonMoreConvert.setVisibility(View.GONE);
        buttomViewConvert.setVisibility(View.GONE);

        buttonMoreCall.setText(getString(R.string.open));
        buttonMoreText.setText(getString(R.string.share));
        buttonMoreClone.setText(getString(R.string.clone_file));
        buttonMoreDelete.setText(getString(R.string.delete_file));

        buttonMoreClone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cloneMode();
                dialogMore.dismiss();
            }
        });

        buttonMoreDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteObject();
                dialogMore.dismiss();
            }
        });

        buttonMoreClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
            }
        });

        dialogMore.show();
        dialogMore.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialogMore.getWindow().setDimAmount(0.5f);
        dialogMore.getWindow().setAttributes(lp);
    }

    private void getAssignedTo() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getFileAssignedTo(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_File_AllU" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAssignedTo.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_UserID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listAssignedTo.add(model);
                            }
                            for (int i = 0; i < listAssignedTo.size(); i++) {
                                phvAssignedToAdd
                                        .addView(new AdapterAssignedTo(getActivity(), FragmentFileInfo.this, i, listAssignedTo.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedTo() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getFileRelatedTo(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_File_RelatedTo" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listRelatedTo.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId("" + i);
                                model.setName(dataObject.getString(Cons.KEY_Name));
                                listRelatedTo.add(model);
                            }
                            spinnerRelatedTo.setAdapter(adapterRelatedTo);
                            setSpinnerDropDownHeight(listRelatedTo, spinnerRelatedTo);
                            spinnerRelatedTo.setSelection(0);
                            getWhat(listRelatedTo.get(0).getName());
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getWhat(String relatedTo) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), relatedTo));
        Call<ResponseBody> response = apiInterface.getFileWhat(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_File_What" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listWhat.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_ID));
                                model.setName(dataObject.getString(Cons.KEY_Name));
                                listWhat.add(model);
                            }
                            spinnerWhat.setAdapter(adapterWhat);
                            setSpinnerDropDownHeight(listWhat, spinnerWhat);
                            spinnerWhat.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getFileDetailed(final String taskId) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_FileID, RequestBody.create(MediaType.parse("text/plain"), taskId));
        Call<ResponseBody> response;
        response = apiInterface.getFileDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_FileInfo" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            JSONObject dataObject = object.getJSONObject("data");
                            strSubject = dataObject.getString(Cons.KEY_Subject);
                            strAssignedTo = dataObject.getString(Cons.KEY_AssignedTo);
                            strAssignedToName = dataObject.getString(Cons.KEY_AssignedToName);
                            strRelatedTo = dataObject.getString(Cons.KEY_RelatedTo);
                            strWhat = dataObject.getString(Cons.KEY_What);
                            strWhatName = dataObject.getString(Cons.KEY_RelatedToName);
                            strFileName = dataObject.getString(Cons.KEY_FileName);
                            strDescription = dataObject.getString(Cons.KEY_Description);
                            strCreatedDate = dataObject.getString(Cons.KEY_CreatedDate);
                            strCreatedBy = dataObject.getString(Cons.KEY_CreatedBy);
                            strLastModifiedDate = dataObject.getString(Cons.KEY_LastModifiedDate);
                            strLastModifiedBy = dataObject.getString(Cons.KEY_LastModifiedBy);
                            editMode(false, true);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void createFile(String subject,
                            String assignedTo,
                            String relatedTo,
                            String what,
                            String description) {

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_Subject, RequestBody.create(MediaType.parse("text/plain"), subject));
        map.put(Cons.KEY_AssignedTo, RequestBody.create(MediaType.parse("text/plain"), assignedTo));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), relatedTo));
        map.put(Cons.KEY_What, RequestBody.create(MediaType.parse("text/plain"), what));
        if (!TextUtils.isEmpty(description)) {
            map.put(Cons.KEY_Description, RequestBody.create(MediaType.parse("text/plain"), description));
        }

        File file = new File(attachmentPath);
        RequestBody reqFile = RequestBody.create(MediaType.parse("*/*"), file);
        MultipartBody.Part attachment = MultipartBody.Part.createFormData("FileName", file.getName(), reqFile);
        Call<ResponseBody> response = apiInterface.createFileDetailedWithFile(header, map, attachment);

        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_File_Create" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            isCloneMode = false;
                            String strFileID = object.getString("FileID");
                            fileId = strFileID;
                            Utl.showToast(getActivity(), strMessage);
                            getFileDetailed(strFileID);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void editFile(final String fileId,
                          String subject,
                          String assignedTo,
                          String relatedTo,
                          String what,
                          String description) {

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_FileID, RequestBody.create(MediaType.parse("text/plain"), fileId));
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_Subject, RequestBody.create(MediaType.parse("text/plain"), subject));
        map.put(Cons.KEY_AssignedTo, RequestBody.create(MediaType.parse("text/plain"), assignedTo));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), relatedTo));
        map.put(Cons.KEY_What, RequestBody.create(MediaType.parse("text/plain"), what));
        if (!TextUtils.isEmpty(description)) {
            map.put(Cons.KEY_Description, RequestBody.create(MediaType.parse("text/plain"), description));
        }
        Call<ResponseBody> response;
        if (isFileClicked) {
            File file = new File(attachmentPath);
            RequestBody reqFile = RequestBody.create(MediaType.parse("*/*"), file);
            MultipartBody.Part attachment = MultipartBody.Part.createFormData("FileName", file.getName(), reqFile);
            response = apiInterface.editFileDetailedWithFile(header, map, attachment);
        } else {
            response = apiInterface.editFileDetailed(header, map);
        }
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_File_Edit" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            getFileDetailed(fileId);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });

    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getActivity().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private void deleteObject() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), "File"));
        map.put(Cons.KEY_What, RequestBody.create(MediaType.parse("text/plain"), fileId));
        Call<ResponseBody> response = apiInterface.deleteObject(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_File_Create" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            clickGoToRecent.callbackGoToRecentFile(getString(R.string.file));
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });

    }

    @Override
    public void callbackAddAssignedToListen(int pos, ModelSpinner model) {
        phvAssignedToAdd.removeAllViews();
        layoutAssignedToAdd.setVisibility(View.GONE);
        strAssignedToNew = model.getId();
        strAssignedToNameNew = model.getName();
        edittextAssignedTo.setText(strAssignedToNameNew);
    }

    public boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), CAMERA);
        int result2 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION);
        int result3 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_COARSE_LOCATION);
        int result4 = ContextCompat.checkSelfPermission(getActivity(), CALL_PHONE);
        int result5 = ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE);
        int result6 = ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE);
        return
                result1 == PackageManager.PERMISSION_GRANTED
                        && result2 == PackageManager.PERMISSION_GRANTED
                        && result3 == PackageManager.PERMISSION_GRANTED
                        && result4 == PackageManager.PERMISSION_GRANTED
                        && result5 == PackageManager.PERMISSION_GRANTED
                        && result6 == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{
                        CAMERA,
                        ACCESS_FINE_LOCATION,
                        ACCESS_COARSE_LOCATION,
                        CALL_PHONE,
                        READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean locationFineAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean locationCoarseAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean callAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean readAccepted = grantResults[4] == PackageManager.PERMISSION_GRANTED;
                    boolean writeAccepted = grantResults[5] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted &&
                            locationFineAccepted &&
                            locationCoarseAccepted &&
                            callAccepted &&
                            readAccepted &&
                            writeAccepted)
                        Utl.showToast(getActivity(), "Permission Granted.");
                    else {
                        Utl.showToast(getActivity(), "Permission Denied.");
                    }
                }
                break;
        }
    }

}