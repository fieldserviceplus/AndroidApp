package com.fieldwise.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fieldwise.R;
import com.fieldwise.adapters.AdapterRecentAccounts;
import com.fieldwise.models.ModelDynamic;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;
import com.mindorks.placeholderview.PlaceHolderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentAccountViewAll extends Fragment implements
        AdapterRecentAccounts.ClickListen {

    public APIInterface apiInterface;

    private String viewAllType;
    private String accountId;

    private List<ModelDynamic> list;
    private PlaceHolderView phv;

    public ClickListenAccountRelatedViewAllWorkOrderOpen clickWorkOrderClick;

    public interface ClickListenAccountRelatedViewAllWorkOrderOpen {
        void callbackAccountRelatedViewAllWorkOrderOpen(ModelDynamic model);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_view_all, container, false);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        clickWorkOrderClick = (ClickListenAccountRelatedViewAllWorkOrderOpen) getActivity();

        viewAllType = getArguments().getString("view_all_type");
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            accountId = bundle.getString(Cons.KEY_AccountID, "");
        }

        phv = (PlaceHolderView) view.findViewById(R.id.phv);

        list = new ArrayList<>();

        if (viewAllType.equals(getString(R.string.work_order))) {
            getRelatedAccountWorkOrders(accountId);
        } else if (viewAllType.equals(getString(R.string.contact))) {
            getRelatedAccountContacts(accountId);
        } else if (viewAllType.equals(getString(R.string.event))) {
            getRelatedAccountEvents(accountId);
        } else if (viewAllType.equals(getString(R.string.estimate))) {
            getRelatedAccountEstimates(accountId);
        } else if (viewAllType.equals(getString(R.string.invoice))) {
            getRelatedAccountInvoices(accountId);
        } else if (viewAllType.equals(getString(R.string.file))) {
            getRelatedAccountFiles(accountId);
        } else if (viewAllType.equals(getString(R.string.location))) {
            getRelatedAccountLocations(accountId);
        } else if (viewAllType.equals(getString(R.string.task))) {
            getRelatedAccountTasks(accountId);
        } else if (viewAllType.equals(getString(R.string.chemical))) {
            getRelatedAccountChemicals(accountId);
        } else if (viewAllType.equals(getString(R.string.product))) {
            getRelatedAccountProducts(accountId);
        }

        return view;

    }

    private void getRelatedAccountWorkOrders(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountWorkOrders(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_RecAcc" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            list.clear();
                            phv.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                Iterator<String> iterator = dataObject.keys();
                                ModelDynamic model = new ModelDynamic();
                                Map<String, String> map = new LinkedHashMap<>();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    String value = dataObject.optString(key);
                                    map.put(key, value);
                                    model.setMap(map);
                                }
                                list.add(model);
                            }
                            for (int i = 0; i < list.size(); i++) {
                                phv.addView(new AdapterRecentAccounts(getActivity(), FragmentAccountViewAll.this, list.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountContacts(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountContacts(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_RecAcc" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            list.clear();
                            phv.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                Iterator<String> iterator = dataObject.keys();
                                ModelDynamic model = new ModelDynamic();
                                Map<String, String> map = new LinkedHashMap<>();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    String value = dataObject.optString(key);
                                    map.put(key, value);
                                    model.setMap(map);
                                }
                                list.add(model);
                            }
                            for (int i = 0; i < list.size(); i++) {
                                phv.addView(new AdapterRecentAccounts(getActivity(), list.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountEvents(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountEvents(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_RecAcc" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            list.clear();
                            phv.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                Iterator<String> iterator = dataObject.keys();
                                ModelDynamic model = new ModelDynamic();
                                Map<String, String> map = new LinkedHashMap<>();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    String value = dataObject.optString(key);
                                    map.put(key, value);
                                    model.setMap(map);
                                }
                                list.add(model);
                            }
                            for (int i = 0; i < list.size(); i++) {
                                phv.addView(new AdapterRecentAccounts(getActivity(), list.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountEstimates(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountEstimates(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_RecAcc" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            list.clear();
                            phv.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                Iterator<String> iterator = dataObject.keys();
                                ModelDynamic model = new ModelDynamic();
                                Map<String, String> map = new LinkedHashMap<>();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    String value = dataObject.optString(key);
                                    map.put(key, value);
                                    model.setMap(map);
                                }
                                list.add(model);
                            }
                            for (int i = 0; i < list.size(); i++) {
                                phv.addView(new AdapterRecentAccounts(getActivity(), list.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountInvoices(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountInvoices(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_RecAcc" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            list.clear();
                            phv.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                Iterator<String> iterator = dataObject.keys();
                                ModelDynamic model = new ModelDynamic();
                                Map<String, String> map = new LinkedHashMap<>();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    String value = dataObject.optString(key);
                                    map.put(key, value);
                                    model.setMap(map);
                                }
                                list.add(model);
                            }
                            for (int i = 0; i < list.size(); i++) {
                                phv.addView(new AdapterRecentAccounts(getActivity(), list.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountFiles(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountFiles(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_RecAcc" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            list.clear();
                            phv.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                Iterator<String> iterator = dataObject.keys();
                                ModelDynamic model = new ModelDynamic();
                                Map<String, String> map = new LinkedHashMap<>();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    String value = dataObject.optString(key);
                                    map.put(key, value);
                                    model.setMap(map);
                                }
                                list.add(model);
                            }
                            for (int i = 0; i < list.size(); i++) {
                                phv.addView(new AdapterRecentAccounts(getActivity(), list.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountLocations(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountLocation(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_RecAcc" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            list.clear();
                            phv.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                Iterator<String> iterator = dataObject.keys();
                                ModelDynamic model = new ModelDynamic();
                                Map<String, String> map = new LinkedHashMap<>();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    String value = dataObject.optString(key);
                                    map.put(key, value);
                                    model.setMap(map);
                                }
                                list.add(model);
                            }
                            for (int i = 0; i < list.size(); i++) {
                                phv.addView(new AdapterRecentAccounts(getActivity(), list.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountTasks(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountTasks(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_RecAcc" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            list.clear();
                            phv.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                Iterator<String> iterator = dataObject.keys();
                                ModelDynamic model = new ModelDynamic();
                                Map<String, String> map = new LinkedHashMap<>();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    String value = dataObject.optString(key);
                                    map.put(key, value);
                                    model.setMap(map);
                                }
                                list.add(model);
                            }
                            for (int i = 0; i < list.size(); i++) {
                                phv.addView(new AdapterRecentAccounts(getActivity(), list.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountChemicals(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountChemicals(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_RecAcc" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            list.clear();
                            phv.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                Iterator<String> iterator = dataObject.keys();
                                ModelDynamic model = new ModelDynamic();
                                Map<String, String> map = new LinkedHashMap<>();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    String value = dataObject.optString(key);
                                    map.put(key, value);
                                    model.setMap(map);
                                }
                                list.add(model);
                            }
                            for (int i = 0; i < list.size(); i++) {
                                phv.addView(new AdapterRecentAccounts(getActivity(), list.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedAccountProducts(String accountId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AccountID, RequestBody.create(MediaType.parse("text/plain"), accountId));
        Call<ResponseBody> response = apiInterface.getRelatedAccountProducts(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_RecAcc" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            list.clear();
                            phv.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                Iterator<String> iterator = dataObject.keys();
                                ModelDynamic model = new ModelDynamic();
                                Map<String, String> map = new LinkedHashMap<>();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    String value = dataObject.optString(key);
                                    map.put(key, value);
                                    model.setMap(map);
                                }
                                list.add(model);
                            }
                            for (int i = 0; i < list.size(); i++) {
                                phv.addView(new AdapterRecentAccounts(getActivity(), list.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    @Override
    public void callbackAccount(ModelDynamic model) {
        if (clickWorkOrderClick != null) {
            clickWorkOrderClick.callbackAccountRelatedViewAllWorkOrderOpen(model);
        }
    }

}