package com.fieldwise.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.adapters.AdapterCreateViewFieldSelect;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;
import com.jmedeisis.draglinearlayout.DragLinearLayout;
import com.mindorks.placeholderview.PlaceHolderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentCreateView extends Fragment implements AdapterCreateViewFieldSelect.ClickListen {

    public APIInterface apiInterface;

    private String objectName;

    private Button buttonCancel, buttonSave, buttonAddFilter, buttonClearAll, buttonSelectFields, buttonFieldsClose;
    private EditText editTextViewName;

    private LinearLayout layoutAddFilter;
    private DragLinearLayout layoutAddFields;

    private String[] strFilterConditions = {"Equals",
            "Contains",
            "StartsWith",
            "DoesNotContain",
            "NotEqualTo",
            "LessThan",
            "GreaterThan",
            "LessOREqualTo",
            "GreaterOREqualTo"};
    private ArrayAdapter adapterFilterConditions;

    private List<String> listFilterFields;
    private ArrayAdapter adapterFilterFields;

    private RadioGroup radioGroupRestrictVisibility;
    private RadioButton radioButtonMe, radioButtonEveryone;

    private RelativeLayout layoutFields;
    private PlaceHolderView phvFields;
    private List<String> listFilterFieldsFront, listFilterFieldsBack;

    public ClickListenCreateViewCancel clickCreateViewCancel;

    public interface ClickListenCreateViewCancel {
        void callbackCreateViewCancel();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_create_view, container, false);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        clickCreateViewCancel = (ClickListenCreateViewCancel) getActivity();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            objectName = bundle.getString("objectName", "");
        }

        buttonCancel = (Button) view.findViewById(R.id.button_cancel);
        buttonSave = (Button) view.findViewById(R.id.button_save);
        buttonAddFilter = (Button) view.findViewById(R.id.button_add_filter);
        buttonClearAll = (Button) view.findViewById(R.id.button_clear_all);
        buttonSelectFields = (Button) view.findViewById(R.id.button_select_fields);
        buttonFieldsClose = (Button) view.findViewById(R.id.button_fields_close);

        editTextViewName = (EditText) view.findViewById(R.id.edittext_view_name);

        layoutAddFilter = (LinearLayout) view.findViewById(R.id.layout_add_filter);
        layoutAddFields = (DragLinearLayout) view.findViewById(R.id.layout_add_fields);

        radioGroupRestrictVisibility = (RadioGroup) view.findViewById(R.id.radio_group_restrict_visibility);
        radioButtonMe = (RadioButton) view.findViewById(R.id.radio_button_me);
        radioButtonEveryone = (RadioButton) view.findViewById(R.id.radio_button_everyone);

        layoutFields = (RelativeLayout) view.findViewById(R.id.layout_fields);
        phvFields = (PlaceHolderView) view.findViewById(R.id.phv_fields);

        adapterFilterConditions = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, strFilterConditions);
        adapterFilterConditions.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        listFilterFields = new ArrayList<>();
        adapterFilterFields = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, listFilterFields);
        adapterFilterFields.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        getFields();

        listFilterFieldsFront = new ArrayList<>();
        listFilterFieldsBack = new ArrayList<>();

        layoutAddFields.setOnViewSwapListener(new DragLinearLayout.OnViewSwapListener() {
            @Override
            public void onSwap(View firstView, int firstPosition, View secondView, int secondPosition) {

            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickCreateViewCancel != null) {
                    clickCreateViewCancel.callbackCreateViewCancel();
                }
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String viewName = editTextViewName.getText().toString();

                StringBuilder sbSpecifyFields = new StringBuilder();

                StringBuilder sbFields = new StringBuilder();
                StringBuilder sbConditions = new StringBuilder();
                StringBuilder sbValues = new StringBuilder();

                int sizeFields = layoutAddFields.getChildCount();
                for (int i = 0; i < sizeFields; i++) {
                    View v = layoutAddFields.getChildAt(i);
                    TextView textviewFieldsValues = (TextView) v.findViewById(R.id.textview_field);
                    sbSpecifyFields.append(textviewFieldsValues.getText().toString().trim() + ",");
                }
                String strSpecifyFields = sbSpecifyFields.toString();
                if (strSpecifyFields.endsWith(",")) {
                    strSpecifyFields = strSpecifyFields.substring(0, strSpecifyFields.length() - 1);
                }

                int sizeFilter = layoutAddFilter.getChildCount();
                for (int i = 0; i < sizeFilter; i++) {
                    View v = layoutAddFilter.getChildAt(i);
                    Spinner spinnerFilterFields = (Spinner) v.findViewById(R.id.spinner_filter_fields);
                    Spinner spinnerFilterConditions = (Spinner) v.findViewById(R.id.spinner_filter_conditions);
                    EditText edittextFilterValues = (EditText) v.findViewById(R.id.edittext_filter_values);
                    sbFields.append(spinnerFilterFields.getSelectedItem() + ",");
                    sbConditions.append(spinnerFilterConditions.getSelectedItem() + ",");
                    sbValues.append(edittextFilterValues.getText().toString().trim() + ",");
                }
                String strFields = sbFields.toString();
                String strConditions = sbConditions.toString();
                String strValues = sbValues.toString();
                if (strFields.endsWith(",")) {
                    strFields = strFields.substring(0, strFields.length() - 1);
                }
                if (strConditions.endsWith(",")) {
                    strConditions = strConditions.substring(0, strConditions.length() - 1);
                }
                if (strValues.endsWith(",")) {
                    strValues = strValues.substring(0, strValues.length() - 1);
                }

                String restrictVisibility = "VisibleToEveryone";
                if (radioButtonMe.isChecked()) {
                    restrictVisibility = "VisibleOnlyToMe";
                } else {
                    restrictVisibility = "VisibleToEveryone";
                }

                Log.d("TAG_ObjectName", objectName);
                Log.d("TAG_ViewName", viewName);
                Log.d("TAG_RestrictVisibility", restrictVisibility);
                Log.d("TAG_SpecifyFields", strSpecifyFields);
                Log.d("TAG_FilterFields", strFields);
                Log.d("TAG_FilterConditions", strConditions);
                Log.d("TAG_FilterValues", strValues);

                if (TextUtils.isEmpty(viewName)) {
                    Utl.showToast(getActivity(), "Enter View Name");
                } else {
                    addCreateView(objectName,
                            viewName,
                            strFields,
                            strConditions,
                            strValues,
                            strSpecifyFields,
                            restrictVisibility);
                }

            }
        });

        buttonAddFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = layoutInflater.inflate(R.layout.layout_filter, null);

                layoutAddFilter.addView(v);

                int size = layoutAddFilter.getChildCount();
                final View viw = layoutAddFilter.getChildAt(size - 1);

                Spinner spinnerFilterFields = (Spinner) viw.findViewById(R.id.spinner_filter_fields);
                setSpinnerDropDownHeight(listFilterFields, spinnerFilterFields);
                spinnerFilterFields.setAdapter(adapterFilterFields);

                Spinner spinnerFilterConditions = (Spinner) viw.findViewById(R.id.spinner_filter_conditions);
                setSimpleSpinnerDropDownHeight(strFilterConditions, spinnerFilterConditions);
                spinnerFilterConditions.setAdapter(adapterFilterConditions);

                EditText edittextFilterValues = (EditText) viw.findViewById(R.id.edittext_filter_values);

                Button btnCancel = (Button) viw.findViewById(R.id.btn_cancel);
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        layoutAddFilter.removeView(viw);
                    }
                });

            }
        });

        buttonClearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutAddFilter.removeAllViews();
            }
        });

        buttonSelectFields.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutFields.setVisibility(View.VISIBLE);
                for (int i = 0; i < listFilterFieldsFront.size(); i++) {
                    phvFields.addView(new AdapterCreateViewFieldSelect(getActivity(), FragmentCreateView.this, i, listFilterFieldsFront.get(i)));
                }
            }
        });

        buttonFieldsClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                phvFields.removeAllViews();
                layoutFields.setVisibility(View.GONE);
            }
        });

        return view;

    }

    private void setSimpleSpinnerDropDownHeight(String[] str, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 140);
        if (str.length > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    private void setSpinnerDropDownHeight(List<String> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 140);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    @Override
    public void callbackSelectField(int pos, String field) {

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viw = layoutInflater.inflate(R.layout.layout_create_view_select_field, null);

        TextView textViewField = (TextView) viw.findViewById(R.id.textview_field);
        Button btnDelete = (Button) viw.findViewById(R.id.btn_delete);

        listFilterFieldsFront.remove(field);
        listFilterFieldsBack.add(field);
        phvFields.removeView(pos);
        phvFields.getAdapter().notifyDataSetChanged();

        layoutAddFields.addView(viw);

        layoutAddFields.setViewDraggable(viw, viw);

        textViewField.setText(field);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listFilterFieldsFront.add(field);
                listFilterFieldsBack.remove(field);
                layoutAddFields.removeDragView(viw);
            }
        });

    }

    private void getFields() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));

        Call<ResponseBody> response = null;
        if (objectName.equals(getString(R.string.account))) {
            map.put(Cons.KEY_AccountViewID, RequestBody.create(MediaType.parse("text/plain"), objectName));
            response = apiInterface.getAccFilter(header, map);
        } else if (objectName.equals(getString(R.string.contact))) {
            map.put(Cons.KEY_ContactViewID, RequestBody.create(MediaType.parse("text/plain"), objectName));
            response = apiInterface.getContactFilter(header, map);
        } else if (objectName.equals(getString(R.string.workorder))) {
            map.put(Cons.KEY_WorkOrderViewID, RequestBody.create(MediaType.parse("text/plain"), objectName));
            response = apiInterface.getWOFilter(header, map);
        } else if (objectName.equals(getString(R.string.estimate))) {
            map.put(Cons.KEY_EstimateViewID, RequestBody.create(MediaType.parse("text/plain"), objectName));
            response = apiInterface.getEstimateFilter(header, map);
        } else if (objectName.equals(getString(R.string.invoice))) {
            map.put(Cons.KEY_InvoiceViewID, RequestBody.create(MediaType.parse("text/plain"), objectName));
            response = apiInterface.getInvoiceFilter(header, map);
        } else if (objectName.equals(getString(R.string.file))) {
            map.put(Cons.KEY_FileViewID, RequestBody.create(MediaType.parse("text/plain"), objectName));
            response = apiInterface.getFileFilter(header, map);
        } else if (objectName.equals(getString(R.string.task))) {
            map.put(Cons.KEY_TaskViewID, RequestBody.create(MediaType.parse("text/plain"), objectName));
            response = apiInterface.getTaskFilter(header, map);
        }
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Filter" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listFilterFields.clear();
                            listFilterFieldsFront.clear();
                            listFilterFieldsBack.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                listFilterFields.add(dataObject.getString(Cons.KEY_FieldName));
                                listFilterFieldsFront.add(dataObject.getString(Cons.KEY_FieldName));
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void addCreateView(String objectName,
                               String viewName,
                               String filterFields,
                               String filterConditions,
                               String filterValues,
                               String specifyFields,
                               String restrictVisibility) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_Object, RequestBody.create(MediaType.parse("text/plain"), objectName));
        map.put(Cons.KEY_ViewName, RequestBody.create(MediaType.parse("text/plain"), viewName));
        map.put(Cons.KEY_FilterFields, RequestBody.create(MediaType.parse("text/plain"), filterFields));
        map.put(Cons.KEY_FilterConditions, RequestBody.create(MediaType.parse("text/plain"), filterConditions));
        map.put(Cons.KEY_FilterValues, RequestBody.create(MediaType.parse("text/plain"), filterValues));
        map.put(Cons.KEY_SpecifyFieldsDisplay, RequestBody.create(MediaType.parse("text/plain"), specifyFields));
        map.put(Cons.KEY_RestrictVisibility, RequestBody.create(MediaType.parse("text/plain"), restrictVisibility));
        Call<ResponseBody> response = apiInterface.addCreateView(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_CreateView" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            if (clickCreateViewCancel != null) {
                                clickCreateViewCancel.callbackCreateViewCancel();
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

}