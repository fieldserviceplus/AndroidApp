package com.fieldwise.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.adapters.AdapterRecentContacts;
import com.fieldwise.models.ModelDynamic;
import com.fieldwise.models.ModelSpinner;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;
import com.mindorks.placeholderview.PlaceHolderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentContact extends Fragment implements
        AdapterRecentContacts.ClickListen {

    public APIInterface apiInterface;

    private String strContactView;

    private List<ModelSpinner> listContactSpinner;
    private ArrayAdapter<ModelSpinner> adapterContactSpinner;
    private Spinner spinnerContactSpinner;

    private List<ModelDynamic> listRecentContactsDetailed;
    private LinearLayout layoutRecentContacts;
    private PlaceHolderView phvRecentContacts;
    private TextView textViewContactHeader;

    private ImageButton buttonFilter, buttonSort, buttonAdd, buttonMore;

    private boolean isFilter = false;

    public ClickListenContact click;

    public interface ClickListenContact {
        void callbackContact(ModelDynamic model);
    }

    public ClickListenContactFilter clickFilter;

    public interface ClickListenContactFilter {
        void callbackContactFilter(String contactView);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact, container, false);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        click = (ClickListenContact) getActivity();
        clickFilter = (ClickListenContactFilter) getActivity();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            isFilter = bundle.getBoolean("isContactFilter", false);
        }

        spinnerContactSpinner = (Spinner) view.findViewById(R.id.spinner_contact_spinner);
        layoutRecentContacts = (LinearLayout) view.findViewById(R.id.layout_recent_contacts);
        phvRecentContacts = (PlaceHolderView) view.findViewById(R.id.phv_recent_contacts);
        textViewContactHeader = (TextView) view.findViewById(R.id.textview_contact_title);

        buttonFilter = (ImageButton) view.findViewById(R.id.button_filter);
        buttonSort = (ImageButton) view.findViewById(R.id.button_sort);
        buttonAdd = (ImageButton) view.findViewById(R.id.button_add);
        buttonMore = (ImageButton) view.findViewById(R.id.button_more);

        listContactSpinner = new ArrayList<>();
        listRecentContactsDetailed = new ArrayList<>();
        adapterContactSpinner = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listContactSpinner);
        adapterContactSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerContactSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position != 0 && !isFilter) {
                    getRecentContact(listContactSpinner.get(position).getId());
                    textViewContactHeader.setText(listContactSpinner.get(position).getName());
                } else if (position == 0 && !isFilter) {
                    getRecentContact("");
                    textViewContactHeader.setText(getString(R.string.contacts_recent));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        getContactSpinner();
        if (isFilter) {
            strContactView = bundle.getString(Cons.KEY_ContactViewID, "");
            String strFields = bundle.getString(Cons.KEY_FilterFields, "");
            String strConditions = bundle.getString(Cons.KEY_FilterConditions, "");
            String strValues = bundle.getString(Cons.KEY_FilterValues, "");
            addContactFilter(strContactView, strFields, strConditions, strValues, "ContactName", "asc");
        } else {
            getRecentContact("");
        }

        buttonFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickFilter != null) {
                    int position = spinnerContactSpinner.getSelectedItemPosition();
                    if (position == 1) {
                        clickFilter.callbackContactFilter("MyActiveContacts");
                    } else if (position == 2) {
                        clickFilter.callbackContactFilter("AllActiveContacts");
                    } else if (position == 3) {
                        clickFilter.callbackContactFilter("ContactsCreatedThisWeek");
                    } else {
                        Utl.showToast(getActivity(), "Select View");
                    }
                }
            }
        });

        buttonSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        return view;
    }

    private void setSpinnerDropDownHeight(List<ModelSpinner> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 70);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    public int getListIndex(List<ModelSpinner> list, String name) {
        for (int i = 0; i < list.size(); i++) {
            ModelSpinner model = list.get(i);
            if (name.equals(model.getName())) {
                return i;
            }
        }
        return -1;
    }

    private void getContactSpinner() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getContactsSpinner(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_ContactSpinner" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listContactSpinner.clear();
                            ModelSpinner m = new ModelSpinner();
                            m.setId("0");
                            m.setName("Select View");
                            listContactSpinner.add(m);
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_ContactViewID));
                                model.setName(dataObject.getString(Cons.KEY_ContactViewName));
                                listContactSpinner.add(model);
                            }
                            spinnerContactSpinner.setAdapter(adapterContactSpinner);
                            setSpinnerDropDownHeight(listContactSpinner, spinnerContactSpinner);
                            if (isFilter) {
                                if (strContactView.equals("MyActiveContacts")) {
                                    spinnerContactSpinner.setSelection(1);
                                    textViewContactHeader.setText(listContactSpinner.get(1).getName());
                                } else if (strContactView.equals("AllActiveContacts")) {
                                    spinnerContactSpinner.setSelection(2);
                                    textViewContactHeader.setText(listContactSpinner.get(2).getName());
                                } else if (strContactView.equals("ContactsCreatedThisWeek")) {
                                    spinnerContactSpinner.setSelection(3);
                                    textViewContactHeader.setText(listContactSpinner.get(3).getName());
                                } else {
                                    spinnerContactSpinner.setSelection(0);
                                    textViewContactHeader.setText(getString(R.string.contacts_recent));
                                }
                            } else {
                                spinnerContactSpinner.setSelection(0);
                                textViewContactHeader.setText(getString(R.string.contacts_recent));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRecentContact(final String contactViewID) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        if (!contactViewID.equalsIgnoreCase("")) {
            map.put(Cons.KEY_ContactViewID, RequestBody.create(MediaType.parse("text/plain"), contactViewID));
        }
        Call<ResponseBody> response;
        if (contactViewID.equalsIgnoreCase("")) {
            response = apiInterface.getRecentContacts(header, map);
        } else {
            response = apiInterface.getRecentContactsById(header, map);
        }
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_RecAcc" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listRecentContactsDetailed.clear();
                            phvRecentContacts.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                Iterator<String> iterator = dataObject.keys();
                                ModelDynamic model = new ModelDynamic();
                                Map<String, String> map = new LinkedHashMap<>();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    String value = dataObject.optString(key);
                                    map.put(key, value);
                                    model.setMap(map);
                                }
                                listRecentContactsDetailed.add(model);
                            }
                            layoutRecentContacts.setVisibility(View.VISIBLE);
                            for (int i = 0; i < listRecentContactsDetailed.size(); i++) {
                                phvRecentContacts.addView(new AdapterRecentContacts(getActivity(), FragmentContact.this, listRecentContactsDetailed.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void addContactFilter(final String contactViewID,
                                  String filterFields,
                                  String filterConditions,
                                  String filterValues,
                                  String sortByField,
                                  String sortByValue) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_ContactViewID, RequestBody.create(MediaType.parse("text/plain"), contactViewID));
        map.put(Cons.KEY_FilterFields, RequestBody.create(MediaType.parse("text/plain"), filterFields));
        map.put(Cons.KEY_FilterConditions, RequestBody.create(MediaType.parse("text/plain"), filterConditions));
        map.put(Cons.KEY_FilterValues, RequestBody.create(MediaType.parse("text/plain"), filterValues));
        map.put(Cons.KEY_SortByField, RequestBody.create(MediaType.parse("text/plain"), sortByField));
        map.put(Cons.KEY_SortByValue, RequestBody.create(MediaType.parse("text/plain"), sortByValue));
        Call<ResponseBody> response = apiInterface.addAccFilter(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Filter" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listRecentContactsDetailed.clear();
                            phvRecentContacts.removeAllViews();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                Iterator<String> iterator = dataObject.keys();
                                ModelDynamic model = new ModelDynamic();
                                Map<String, String> map = new LinkedHashMap<>();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    String value = dataObject.optString(key);
                                    map.put(key, value);
                                    model.setMap(map);
                                }
                                listRecentContactsDetailed.add(model);
                            }
                            layoutRecentContacts.setVisibility(View.VISIBLE);
                            for (int i = 0; i < listRecentContactsDetailed.size(); i++) {
                                phvRecentContacts.addView(new AdapterRecentContacts(getActivity(), FragmentContact.this, listRecentContactsDetailed.get(i)));
                            }
                            isFilter = false;
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    @Override
    public void callbackContact(ModelDynamic model) {
        if (click != null) {
            click.callbackContact(model);
        }
    }

}