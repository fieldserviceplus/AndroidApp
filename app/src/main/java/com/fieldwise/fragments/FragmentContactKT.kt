package com.fieldwise.fragments

import android.app.Dialog
import android.app.ProgressDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.*

import com.fieldwise.R
import com.fieldwise.adapters.AdapterRecentContacts
import com.fieldwise.add.AdapterShortView
import com.fieldwise.models.ModelDynamic
import com.fieldwise.models.ModelSpinner
import com.fieldwise.utils.APIClient
import com.fieldwise.utils.APIInterface
import com.fieldwise.utils.Cons
import com.fieldwise.utils.Utl
import com.mindorks.placeholderview.PlaceHolderView

import org.json.JSONException
import org.json.JSONObject

import java.io.IOException
import java.util.ArrayList
import java.util.HashMap
import java.util.LinkedHashMap

import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentContactKT : Fragment(), AdapterRecentContacts.ClickListen, AdapterShortView.ClickListenShortView {

    var apiInterface = APIClient.getClient().create(APIInterface::class.java)

    var strContactView: String? = null

    var listContactSpinner: MutableList<ModelSpinner>? = null
    var adapterContactSpinner: ArrayAdapter<ModelSpinner>? = null
    var spinnerContactSpinner: Spinner? = null

    var listRecentContactsDetailed: MutableList<ModelDynamic>? = null
    var layoutRecentContacts: LinearLayout? = null
    var phvRecentContacts: PlaceHolderView? = null
    var textViewContactHeader: TextView? = null

    var buttonFilter: ImageButton? = null
    var buttonSort: ImageButton? = null
    var buttonAdd: ImageButton? = null
    var buttonMore: ImageButton? = null

    var layoutShortView: LinearLayout? = null
    var buttonShortViewCancel: Button? = null
    var buttonShortViewSave:Button? = null
    var phvShortView: PlaceHolderView? = null
    val listShortFieldName = ArrayList<String>()
    val listShortPosition = ArrayList<Int>()

    var isFilter = false

    var click: ClickListenContact? = null

    interface ClickListenContact {
        fun callbackContact(model: ModelDynamic)
    }

    var clickFilter: ClickListenContactFilter? = null

    interface ClickListenContactFilter {
        fun callbackContactFilter(contactView: String)
    }

    var clickCreateView: ClickListenCreateView? = null

    interface ClickListenCreateView {
        fun callbackContactCreateView(objectName: String)
    }

    var clickCopyView: ClickListenCopyView? = null

    interface ClickListenCopyView {
        fun callbackContactCopyView()
    }

    var clickDeleteView: ClickListenDeleteView? = null

    interface ClickListenDeleteView {
        fun callbackContactDeleteView()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_contact, container, false)

        click = activity as ClickListenContact?
        clickFilter = activity as ClickListenContactFilter?
        clickCreateView = activity as ClickListenCreateView?
        clickCopyView = activity as ClickListenCopyView?
        clickDeleteView = activity as ClickListenDeleteView?

        val bundle = this.arguments
        if (bundle != null) {
            isFilter = bundle.getBoolean("isContactFilter", false)
        }

        spinnerContactSpinner = view.findViewById<View>(R.id.spinner_contact_spinner) as Spinner
        layoutRecentContacts = view.findViewById<View>(R.id.layout_recent_contacts) as LinearLayout
        phvRecentContacts = view.findViewById<View>(R.id.phv_recent_contacts) as PlaceHolderView
        textViewContactHeader = view.findViewById<View>(R.id.textview_contact_title) as TextView

        buttonFilter = view.findViewById<View>(R.id.button_filter) as ImageButton
        buttonSort = view.findViewById<View>(R.id.button_sort) as ImageButton
        buttonAdd = view.findViewById<View>(R.id.button_add) as ImageButton
        buttonMore = view.findViewById<View>(R.id.button_more) as ImageButton

        layoutShortView = view.findViewById<View>(R.id.layout_short_view) as LinearLayout
        buttonShortViewCancel = view.findViewById<View>(R.id.button_short_view_cancel) as Button
        buttonShortViewSave = view.findViewById<View>(R.id.button_short_view_save) as Button
        phvShortView = view.findViewById<View>(R.id.phv_short_view) as PlaceHolderView

        listContactSpinner = ArrayList()
        listRecentContactsDetailed = ArrayList()
        adapterContactSpinner = ArrayAdapter(activity!!, android.R.layout.simple_spinner_dropdown_item, listContactSpinner!!)
        adapterContactSpinner!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerContactSpinner!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                if (position != 0 && !isFilter) {
                    getRecentContact(listContactSpinner!![position].id)
                    textViewContactHeader!!.text = listContactSpinner!![position].name
                } else if (position == 0 && !isFilter) {
                    getRecentContact("")
                    textViewContactHeader!!.text = getString(R.string.contacts_recent)
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {}
        }

        getContactSpinner()
        if (isFilter) {
            strContactView = bundle!!.getString(Cons.KEY_ContactViewID, "")
            val strFields = bundle.getString(Cons.KEY_FilterFields, "")
            val strConditions = bundle.getString(Cons.KEY_FilterConditions, "")
            val strValues = bundle.getString(Cons.KEY_FilterValues, "")
            addContactFilter(strContactView, strFields, strConditions, strValues, "Name", "asc")
        } else {
            getRecentContact("")
        }

        buttonFilter!!.setOnClickListener {
            if (clickFilter != null) {
                val position = spinnerContactSpinner!!.selectedItemPosition
                if (position == 1) {
                    clickFilter!!.callbackContactFilter("MyActiveContacts")
                } else if (position == 2) {
                    clickFilter!!.callbackContactFilter("AllActiveContacts")
                } else if (position == 3) {
                    clickFilter!!.callbackContactFilter("ContactsCreatedThisWeek")
                } else {
                    Utl.showToast(activity, "Select View")
                }
            }
        }

        buttonSort!!.setOnClickListener(View.OnClickListener {
            layoutShortView!!.setVisibility(View.VISIBLE)
            val pos = spinnerContactSpinner!!.getSelectedItemPosition()
            val id = listContactSpinner!!.get(pos).getId()
            getShortViewList(getString(R.string.contact), id)
            buttonShortViewCancel!!.setOnClickListener(View.OnClickListener {
                listShortFieldName.clear()
                listShortPosition.clear()
                phvShortView!!.removeAllViews()
                layoutShortView!!.setVisibility(View.GONE)
            })
            buttonShortViewSave!!.setOnClickListener(View.OnClickListener {
                var sortByField = ""
                var sortByValue = ""
                for (i in listShortFieldName.indices) {
                    val pos = listShortPosition[i]
                    if (pos == 1) {
                        sortByField = listShortFieldName[i]
                        sortByValue = "asc"
                    } else if (pos == 2) {
                        sortByField = listShortFieldName[i]
                        sortByValue = "desc"
                    }
                }
                setShortView(getString(R.string.contact), id, sortByField, sortByValue)
            })
        })

        buttonAdd!!.setOnClickListener(View.OnClickListener {
            if (clickCreateView != null) {
                clickCreateView!!.callbackContactCreateView(getString(R.string.contact))
            }
        })

        buttonMore!!.setOnClickListener { dialogMore() }

        return view
    }

    private fun setSpinnerDropDownHeight(list: List<ModelSpinner>, spinner: Spinner) {
        val displayMetrics = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        spinner.dropDownWidth = width - 70
        if (list.size > 4) {
            try {
                val popup = Spinner::class.java.getDeclaredField("mPopup")
                popup.isAccessible = true
                val popupWindow = popup.get(spinner) as android.widget.ListPopupWindow
                popupWindow.height = 800
            } catch (e: NoClassDefFoundError) {
            } catch (e: ClassCastException) {
            } catch (e: NoSuchFieldException) {
            } catch (e: IllegalAccessException) {
            }

        }
    }

    fun getListIndex(list: List<ModelSpinner>, name: String): Int {
        for (i in list.indices) {
            val model = list[i]
            if (name == model.name) {
                return i
            }
        }
        return -1
    }

    private fun getContactSpinner() {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        val response = apiInterface.getContactsSpinner(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_ContactSpinner" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            listContactSpinner!!.clear()
                            val m = ModelSpinner()
                            m.id = "0"
                            m.name = "Select View"
                            listContactSpinner!!.add(m)
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val model = ModelSpinner()
                                model.id = dataObject.getString(Cons.KEY_ContactViewID)
                                model.name = dataObject.getString(Cons.KEY_ContactViewName)
                                listContactSpinner!!.add(model)
                            }
                            spinnerContactSpinner!!.adapter = adapterContactSpinner
                            setSpinnerDropDownHeight(listContactSpinner!!, spinnerContactSpinner!!)
                            if (isFilter) {
                                if (strContactView == "MyActiveContacts") {
                                    spinnerContactSpinner!!.setSelection(1)
                                    textViewContactHeader!!.text = listContactSpinner!![1].name
                                } else if (strContactView == "AllActiveContacts") {
                                    spinnerContactSpinner!!.setSelection(2)
                                    textViewContactHeader!!.text = listContactSpinner!![2].name
                                } else if (strContactView == "ContactsCreatedThisWeek") {
                                    spinnerContactSpinner!!.setSelection(3)
                                    textViewContactHeader!!.text = listContactSpinner!![3].name
                                } else {
                                    spinnerContactSpinner!!.setSelection(0)
                                    textViewContactHeader!!.text = getString(R.string.contacts_recent)
                                }
                            } else {
                                spinnerContactSpinner!!.setSelection(0)
                                textViewContactHeader!!.text = getString(R.string.contacts_recent)
                            }
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getRecentContact(contactViewID: String) {
        val dialog = ProgressDialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setMessage(getString(R.string.please_wait))
        dialog.show()
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        if (!contactViewID.equals("", ignoreCase = true)) {
            map[Cons.KEY_ContactViewID] = RequestBody.create(MediaType.parse("text/plain"), contactViewID)
        }
        val response: Call<ResponseBody>
        if (contactViewID.equals("", ignoreCase = true)) {
            response = apiInterface.getRecentContacts(header, map)
        } else {
            response = apiInterface.getRecentContactsById(header, map)
        }
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (dialog.isShowing) {
                    dialog.dismiss()
                }
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_RecAcc" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            listRecentContactsDetailed!!.clear()
                            phvRecentContacts!!.removeAllViews()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val iterator = dataObject.keys()
                                val model = ModelDynamic()
                                val map = LinkedHashMap<String, String>()
                                while (iterator.hasNext()) {
                                    val key = iterator.next()
                                    val value = dataObject.optString(key)
                                    map[key] = value
                                    model.map = map
                                }
                                listRecentContactsDetailed!!.add(model)
                            }
                            layoutRecentContacts!!.visibility = View.VISIBLE
                            for (i in listRecentContactsDetailed!!.indices) {
                                phvRecentContacts!!.addView(AdapterRecentContacts(activity, this@FragmentContactKT, listRecentContactsDetailed!![i]))
                            }
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (dialog.isShowing) {
                    dialog.dismiss()
                }
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun addContactFilter(contactViewID: String?,
                                 filterFields: String,
                                 filterConditions: String,
                                 filterValues: String,
                                 sortByField: String,
                                 sortByValue: String) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_ContactViewID] = RequestBody.create(MediaType.parse("text/plain"), contactViewID!!)
        map[Cons.KEY_FilterFields] = RequestBody.create(MediaType.parse("text/plain"), filterFields)
        map[Cons.KEY_FilterConditions] = RequestBody.create(MediaType.parse("text/plain"), filterConditions)
        map[Cons.KEY_FilterValues] = RequestBody.create(MediaType.parse("text/plain"), filterValues)
        map[Cons.KEY_SortByField] = RequestBody.create(MediaType.parse("text/plain"), sortByField)
        map[Cons.KEY_SortByValue] = RequestBody.create(MediaType.parse("text/plain"), sortByValue)
        val response = apiInterface.addContactFilter(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_Filter" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            listRecentContactsDetailed!!.clear()
                            phvRecentContacts!!.removeAllViews()
                            val dataArray = `object`.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                val dataObject = dataArray.getJSONObject(i)
                                val iterator = dataObject.keys()
                                val model = ModelDynamic()
                                val map = LinkedHashMap<String, String>()
                                while (iterator.hasNext()) {
                                    val key = iterator.next()
                                    val value = dataObject.optString(key)
                                    map[key] = value
                                    model.map = map
                                }
                                listRecentContactsDetailed!!.add(model)
                            }
                            layoutRecentContacts!!.visibility = View.VISIBLE
                            for (i in listRecentContactsDetailed!!.indices) {
                                phvRecentContacts!!.addView(AdapterRecentContacts(activity, this@FragmentContactKT, listRecentContactsDetailed!![i]))
                            }
                            isFilter = false
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    override fun callbackContact(model: ModelDynamic) {
        if (click != null) {
            click!!.callbackContact(model)
        }
    }

    private fun dialogMore() {
        val dialogMore = Dialog(activity!!, R.style.DialogFullScreen)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialogMore.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.BOTTOM
        dialogMore.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogMore.setCancelable(true)
        dialogMore.setCanceledOnTouchOutside(true)
        dialogMore.setContentView(R.layout.dialog_more_recent)
        val buttonMore1 = dialogMore.findViewById<View>(R.id.button_more_1) as TextView
        val buttonMore2 = dialogMore.findViewById<View>(R.id.button_more_2) as TextView
        val buttonMore3 = dialogMore.findViewById<View>(R.id.button_more_3) as TextView
        val buttonMore4 = dialogMore.findViewById<View>(R.id.button_more_4) as TextView
        val buttonMore5 = dialogMore.findViewById<View>(R.id.button_more_5) as TextView
        val buttonMore6 = dialogMore.findViewById<View>(R.id.button_more_6) as TextView
        val buttonMore7 = dialogMore.findViewById<View>(R.id.button_more_7) as TextView
        val buttonView1 = dialogMore.findViewById(R.id.view_more_1) as View
        val buttonView2 = dialogMore.findViewById(R.id.view_more_2) as View
        val buttonView3 = dialogMore.findViewById(R.id.view_more_3) as View
        val buttonView4 = dialogMore.findViewById(R.id.view_more_4) as View
        val buttonView5 = dialogMore.findViewById(R.id.view_more_5) as View
        val buttonView6 = dialogMore.findViewById(R.id.view_more_6) as View
        val buttonView7 = dialogMore.findViewById(R.id.view_more_7) as View
        val buttonMoreClose = dialogMore.findViewById<View>(R.id.button_more_close) as Button
        if (spinnerContactSpinner!!.getSelectedItemPosition() < 4) {
            buttonMore2.visibility = View.GONE
            buttonMore3.visibility = View.GONE
            buttonMore4.visibility = View.GONE
            buttonMore5.visibility = View.GONE
            buttonMore6.visibility = View.GONE
            buttonMore7.visibility = View.GONE
            buttonView2.visibility = View.GONE
            buttonView3.visibility = View.GONE
            buttonView4.visibility = View.GONE
            buttonView5.visibility = View.GONE
            buttonView6.visibility = View.GONE
            buttonView7.visibility = View.GONE
        } else {
            buttonMore2.visibility = View.VISIBLE
            buttonMore3.visibility = View.VISIBLE
            buttonMore4.visibility = View.VISIBLE
            buttonMore5.visibility = View.VISIBLE
            buttonMore6.visibility = View.VISIBLE
            buttonMore7.visibility = View.VISIBLE
            buttonView2.visibility = View.VISIBLE
            buttonView3.visibility = View.VISIBLE
            buttonView4.visibility = View.VISIBLE
            buttonView5.visibility = View.VISIBLE
            buttonView6.visibility = View.VISIBLE
            buttonView7.visibility = View.VISIBLE
        }
        buttonMore1.setOnClickListener { }
        buttonMore2.setOnClickListener {
            dialogMore.dismiss()
            val pos = spinnerContactSpinner!!.getSelectedItemPosition()
            val id = listContactSpinner!!.get(pos).getId()
            addCopyView(getString(R.string.contact), id)
        }
        buttonMore3.setOnClickListener { }
        buttonMore4.setOnClickListener { }
        buttonMore5.setOnClickListener { }
        buttonMore6.setOnClickListener { }
        buttonMore7.setOnClickListener {
            dialogMore.dismiss()
            val pos = spinnerContactSpinner!!.getSelectedItemPosition()
            val id = listContactSpinner!!.get(pos).getId()
            addDeleteView(getString(R.string.contact), id)
        }
        buttonMoreClose.setOnClickListener { dialogMore.dismiss() }
        dialogMore.show()
        dialogMore.window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        dialogMore.window!!.setDimAmount(0.5f)
        dialogMore.window!!.attributes = lp
    }

    private fun addCopyView(objectName: String,
                            viewID: String) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_Object] = RequestBody.create(MediaType.parse("text/plain"), objectName)
        map[Cons.KEY_ViewID] = RequestBody.create(MediaType.parse("text/plain"), viewID)
        val response = apiInterface.addCopyView(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_CreateView" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            Utl.showToast(activity, strMessage)
                            if (clickCopyView != null) {
                                clickCopyView!!.callbackContactCopyView()
                            }
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun addDeleteView(objectName: String,
                              viewID: String) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_Object] = RequestBody.create(MediaType.parse("text/plain"), objectName)
        map[Cons.KEY_ViewID] = RequestBody.create(MediaType.parse("text/plain"), viewID)
        val response = apiInterface.addDeleteView(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        Log.d("TAG_CreateView" + "_Suc", response.body()!!.string().toString())
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            Utl.showToast(activity, strMessage)
                            if (clickDeleteView != null) {
                                clickDeleteView!!.callbackContactDeleteView()
                            }
                        }
                    } catch (e: JSONException) {
                        Log.d("TAG_err_1", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Log.d("TAG_err_2", "" + e.message)
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Log.d("TAG_err_3", "null")
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG_err_4", "" + t.message)
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun getShortViewList(objectName: String, viewID: String) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_Object] = RequestBody.create(MediaType.parse("text/plain"), objectName)
        map[Cons.KEY_ViewID] = RequestBody.create(MediaType.parse("text/plain"), viewID)
        val response = apiInterface.getViewDetails(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            listShortFieldName.clear()
                            listShortPosition.clear()
                            val objectData = `object`.getJSONObject("data")
                            val DisplayedColumns = objectData.getJSONArray("DisplayedColumns")
                            for (i in 0 until DisplayedColumns.length()) {
                                val FieldName = DisplayedColumns.getJSONObject(i)
                                val strFieldName = FieldName.getString("FieldName")
                                listShortFieldName.add(strFieldName)
                                listShortPosition.add(0)
                            }
                            listShortPosition[0] = 1
                            for (i in listShortFieldName.indices) {
                                phvShortView!!.addView(AdapterShortView(activity, this@FragmentContactKT, i, listShortPosition, listShortFieldName))
                            }
                            for (i in listShortFieldName.indices) {
                                Log.d("TAG_listShortPosition_$i", "" + listShortPosition[i])
                            }
                        }
                    } catch (e: JSONException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    private fun setShortView(objectName: String, viewID: String, sortByField: String, sortByValue: String) {
        val header = HashMap<String, String>()
        header["token"] = Utl.getSPStr(activity!!, Cons.KEY_TOKEN)
        val map = HashMap<String, RequestBody>()
        map[Cons.KEY_UserID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_UserID))
        map[Cons.KEY_ORGANIZATION_ID] = RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(activity!!, Cons.KEY_ORGANIZATION_ID))
        map[Cons.KEY_Object] = RequestBody.create(MediaType.parse("text/plain"), objectName)
        map[Cons.KEY_ViewID] = RequestBody.create(MediaType.parse("text/plain"), viewID)
        map[Cons.KEY_SortByField] = RequestBody.create(MediaType.parse("text/plain"), sortByField)
        map[Cons.KEY_SortByValue] = RequestBody.create(MediaType.parse("text/plain"), sortByValue)
        val response = apiInterface.shortCustomView(header, map)
        response.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val strResponse = response.body()!!.string().toString()
                        val `object` = JSONObject(strResponse)
                        val strMessage = `object`.getString("ResponseMsg")
                        val responseCode = `object`.getInt("ResponseCode")
                        if (responseCode == 200) {
                            Utl.showToast(activity, strMessage)
                        } else if (responseCode == 1) {
                            listShortFieldName.clear()
                            listShortPosition.clear()
                            phvShortView!!.removeAllViews()
                            layoutShortView!!.setVisibility(View.GONE)
                            Utl.showToast(activity, strMessage)
                            val pos = spinnerContactSpinner!!.getSelectedItemPosition()
                            getRecentContact(listContactSpinner!!.get(pos).getId())
                        }
                    } catch (e: JSONException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    } catch (e: IOException) {
                        Utl.showToast(activity, getString(R.string.server_error))
                    }

                } else {
                    Utl.showToast(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Utl.showToast(activity, getString(R.string.server_error))
            }
        })
    }

    override fun callbackShortView(pos: Int, objectName: String) {
        for (i in listShortFieldName.indices) {
            if (pos == i) {
                listShortPosition[i] = 1
            } else {
                listShortPosition[i] = 0
            }
        }
        phvShortView!!.getViewAdapter().notifyDataSetChanged()
        for (i in listShortFieldName.indices) {
            Log.d("TAG_listShortPosition_$i", "" + listShortPosition[i])
        }
    }

    override fun callbackShortViewToggle(pos: Int, objectName: String, checked: Boolean) {
        for (i in listShortFieldName.indices) {
            if (pos == i) {
                if (checked) {
                    listShortPosition[i] = 1
                } else {
                    listShortPosition[i] = 2
                }
            } else {
                listShortPosition[i] = 0
            }
        }
        for (i in listShortFieldName.indices) {
            Log.d("TAG_listShortPosition_$i", "" + listShortPosition[i])
        }
    }

}