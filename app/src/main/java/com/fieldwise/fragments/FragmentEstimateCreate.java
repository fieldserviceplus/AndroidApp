package com.fieldwise.fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.fieldwise.R;
import com.fieldwise.adapters.AdapterAccount;
import com.fieldwise.adapters.AdapterContact;
import com.fieldwise.adapters.AdapterOwner;
import com.fieldwise.models.ModelSpinner;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;
import com.mindorks.placeholderview.PlaceHolderView;
import com.schibstedspain.leku.LocationPickerActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class FragmentEstimateCreate extends Fragment implements
        AdapterAccount.AddAccountClickListen,
        AdapterContact.AddContactClickListen,
        AdapterOwner.AddOwnerClickListen {

    private static final int PERMISSION_REQUEST_CODE = 200;

    int back_type;

    public APIInterface apiInterface;

    private String strEstimateName,
            strAccount, strAccountName,
            strContact, strContactName,
            strDescription,
            strOwner, strOwnerName,
            strStatus,
            strExpirationDate,
            strPhone,
            strEmail,
            strBilLat, strBilLng,
            strBilName, strBilAddress, strBilCity, strBilState, strBilCountry, strBilPostal, strShpLat, strShpLng,
            strShpName, strShpAddress, strShpCity, strShpState, strShpCountry, strShpPostal;

    private boolean isBilClicked = false;
    private boolean isShpClicked = false;

    private Spinner spinnerStatus;

    private List<ModelSpinner> listAccounts,
            listContacts,
            listOwners,
            listStatus;

    private ArrayAdapter<ModelSpinner> adapterStatus;

    private EditText edittextName,
            edittextAccount,
            edittextContact,
            edittextOwner,
            edittextDescription,
            edittextPhone,
            edittextEmail,
            edittextBilName,
            edittextBilAddress,
            edittextBilCity,
            edittextBilState,
            edittextBilCountry,
            edittextBilPostal,
            edittextShpName,
            edittextShpAddress,
            edittextShpCity,
            edittextShpState,
            edittextShpCountry,
            edittextShpPostal;

    private TextView edittextExpirationDate;

    private ImageButton buttonAccount;
    private LinearLayout layoutAccountAdd;
    private EditText edittextAccountAddSearch;
    private PlaceHolderView phvAccountAdd;

    private ImageButton buttonContact;
    private LinearLayout layoutContactAdd;
    private EditText edittextContactAddSearch;
    private PlaceHolderView phvContactAdd;

    private ImageButton buttonOwner;
    private LinearLayout layoutOwnerAdd;
    private EditText edittextOwnerAddSearch;
    private PlaceHolderView phvOwnerAdd;

    private ImageButton buttonBilAddress, buttonShpAddress;

    private Button buttonSave, buttonCancel;

    public ClickListenEstimateCreateClose clickClose;

    public interface ClickListenEstimateCreateClose {
        void callbackEstimateCreateClose(int back_type, boolean isSave, String estimateID, String estimateNo);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_estimate_create, container, false);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            back_type = bundle.getInt("back_type", 0);
        }

        apiInterface = APIClient.getClient().create(APIInterface.class);

        clickClose = (ClickListenEstimateCreateClose) getActivity();

        spinnerStatus = (Spinner) view.findViewById(R.id.spinner_status);

        edittextName = (EditText) view.findViewById(R.id.edittext_estimate_name);
        edittextAccount = (EditText) view.findViewById(R.id.edittext_account);
        edittextContact = (EditText) view.findViewById(R.id.edittext_contact);
        edittextOwner = (EditText) view.findViewById(R.id.edittext_owner);
        edittextDescription = (EditText) view.findViewById(R.id.edittext_description);
        edittextPhone = (EditText) view.findViewById(R.id.edittext_estimate_phone);
        edittextEmail = (EditText) view.findViewById(R.id.edittext_estimate_email);

        edittextBilName = (EditText) view.findViewById(R.id.edittext_bil_name);
        edittextBilAddress = (EditText) view.findViewById(R.id.edittext_bil_address);
        edittextBilCity = (EditText) view.findViewById(R.id.edittext_bil_city);
        edittextBilState = (EditText) view.findViewById(R.id.edittext_bil_state);
        edittextBilCountry = (EditText) view.findViewById(R.id.edittext_bil_country);
        edittextBilPostal = (EditText) view.findViewById(R.id.edittext_bil_postal);

        edittextShpName = (EditText) view.findViewById(R.id.edittext_shp_name);
        edittextShpAddress = (EditText) view.findViewById(R.id.edittext_shp_address);
        edittextShpCity = (EditText) view.findViewById(R.id.edittext_shp_city);
        edittextShpState = (EditText) view.findViewById(R.id.edittext_shp_state);
        edittextShpCountry = (EditText) view.findViewById(R.id.edittext_shp_country);
        edittextShpPostal = (EditText) view.findViewById(R.id.edittext_shp_postal);

        edittextExpirationDate = (TextView) view.findViewById(R.id.edittext_expiration_date);

        buttonBilAddress = (ImageButton) view.findViewById(R.id.button_bil_address);
        buttonShpAddress = (ImageButton) view.findViewById(R.id.button_shp_address);

        buttonSave = (Button) view.findViewById(R.id.button_save);
        buttonCancel = (Button) view.findViewById(R.id.button_cancel);

        buttonAccount = (ImageButton) view.findViewById(R.id.button_account);
        layoutAccountAdd = (LinearLayout) view.findViewById(R.id.layout_account_add);
        edittextAccountAddSearch = (EditText) view.findViewById(R.id.edittext_account_add_search);
        phvAccountAdd = (PlaceHolderView) view.findViewById(R.id.phv_account_add);

        buttonContact = (ImageButton) view.findViewById(R.id.button_contact);
        layoutContactAdd = (LinearLayout) view.findViewById(R.id.layout_contact_add);
        edittextContactAddSearch = (EditText) view.findViewById(R.id.edittext_contact_add_search);
        phvContactAdd = (PlaceHolderView) view.findViewById(R.id.phv_contact_add);

        buttonOwner = (ImageButton) view.findViewById(R.id.button_owner);
        layoutOwnerAdd = (LinearLayout) view.findViewById(R.id.layout_owner_add);
        edittextOwnerAddSearch = (EditText) view.findViewById(R.id.edittext_owner_add_search);
        phvOwnerAdd = (PlaceHolderView) view.findViewById(R.id.phv_owner_add);

        Calendar calendar = Calendar.getInstance();
        Date date = Calendar.getInstance().getTime();
        calendar.setTime(date);
        int day = calendar.get(Calendar.DATE);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);
        calendar.set(year, month, day, hour, minute);
        String AM_PM = (String) android.text.format.DateFormat.format("aaa", calendar);
        if (hour > 12) {
            hour = hour - 12;
        }
        strExpirationDate = setDate(minute, hour, day, month, year, AM_PM);
        edittextExpirationDate.setText(formatDate(strExpirationDate));

        strBilLat = "0";
        strBilLng = "0";
        strBilAddress = "0";
        strBilCity = "0";
        strBilState = "0";
        strBilCountry = "0";
        strBilPostal = "0";
        strShpLat = "0";
        strShpLng = "0";
        strShpAddress = "0";
        strShpCity = "0";
        strShpState = "0";
        strShpCountry = "0";
        strShpPostal = "0";

        listAccounts = new ArrayList<>();
        listContacts = new ArrayList<>();
        listOwners = new ArrayList<>();
        listStatus = new ArrayList<>();
        adapterStatus = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listStatus);
        adapterStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        getAccounts();
        getContacts();
        getOwner();
        getStatus();

        buttonAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutAccountAdd.setVisibility(View.VISIBLE);
                listAccounts.clear();
                getAccounts();
            }
        });

        edittextAccountAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvAccountAdd.removeAllViews();
                    ArrayList<ModelSpinner> model = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listAccounts) {
                        if (m.getName().toLowerCase().contains(search)) {
                            model.add(m);
                        }
                    }
                    for (int i = 0; i < model.size(); i++) {
                        phvAccountAdd
                                .addView(new AdapterAccount(getActivity(), FragmentEstimateCreate.this, i, model.get(i)));
                    }
                } else {
                    phvAccountAdd.removeAllViews();
                    for (int i = 0; i < listAccounts.size(); i++) {
                        phvAccountAdd
                                .addView(new AdapterAccount(getActivity(), FragmentEstimateCreate.this, i, listAccounts.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        buttonContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutContactAdd.setVisibility(View.VISIBLE);
                listContacts.clear();
                getAccounts();
            }
        });

        edittextContactAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvContactAdd.removeAllViews();
                    ArrayList<ModelSpinner> model = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listContacts) {
                        if (m.getName().toLowerCase().contains(search)) {
                            model.add(m);
                        }
                    }
                    for (int i = 0; i < model.size(); i++) {
                        phvContactAdd
                                .addView(new AdapterContact(getActivity(), FragmentEstimateCreate.this, i, model.get(i)));
                    }
                } else {
                    phvContactAdd.removeAllViews();
                    for (int i = 0; i < listContacts.size(); i++) {
                        phvContactAdd
                                .addView(new AdapterContact(getActivity(), FragmentEstimateCreate.this, i, listContacts.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        buttonOwner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutOwnerAdd.setVisibility(View.VISIBLE);
                listOwners.clear();
                getOwner();
            }
        });

        edittextOwnerAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvOwnerAdd.removeAllViews();
                    ArrayList<ModelSpinner> model = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listOwners) {
                        if (m.getName().toLowerCase().contains(search)) {
                            model.add(m);
                        }
                    }
                    for (int i = 0; i < model.size(); i++) {
                        phvOwnerAdd
                                .addView(new AdapterOwner(getActivity(), FragmentEstimateCreate.this, i, model.get(i)));
                    }
                } else {
                    phvOwnerAdd.removeAllViews();
                    for (int i = 0; i < listOwners.size(); i++) {
                        phvOwnerAdd
                                .addView(new AdapterOwner(getActivity(), FragmentEstimateCreate.this, i, listOwners.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        edittextExpirationDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                Date date = null;
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm aaa");
                try {
                    date = sdf.parse(strExpirationDate);
                } catch (ParseException ex) {
                    Log.v("Exception", ex.getLocalizedMessage());
                }
                calendar.setTime(date);
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                final int hour = calendar.get(Calendar.HOUR);
                final int minute = calendar.get(Calendar.MINUTE);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hr, int min) {
                                Calendar c = Calendar.getInstance();
                                c.set(0, 0, 0, hr, min);
                                String AM_PM = (String) android.text.format.DateFormat.format("aaa", c);
                                if (hr > 12) {
                                    hr = hr - 12;
                                }
                                strExpirationDate = setDate(min, hr, d, m, y, AM_PM);
                                edittextExpirationDate.setText(formatDate(strExpirationDate));
                            }
                        }, hour, minute, false);
                        timePickerDialog.show();
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        buttonBilAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!checkPermission()) {
                        requestPermission();
                    } else {
                        isBilClicked = true;
                        double lat = Double.valueOf(strBilLat);
                        double lng = Double.valueOf(strBilLng);
                        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                                .withLocation(lat, lng)
                                .withGeolocApiKey(getString(R.string.google_maps_key))
                                .withGooglePlacesEnabled()
                                .withSatelliteViewHidden()
                                .withVoiceSearchHidden()
                                .shouldReturnOkOnBackPressed()
                                .build(getActivity());
                        startActivityForResult(locationPickerIntent, 99);
                    }
                } else {
                    isBilClicked = true;
                    double lat = Double.valueOf(strBilLat);
                    double lng = Double.valueOf(strBilLng);
                    Intent locationPickerIntent = new LocationPickerActivity.Builder()
                            .withLocation(lat, lng)
                            .withGeolocApiKey(getString(R.string.google_maps_key))
                            .withGooglePlacesEnabled()
                            .withSatelliteViewHidden()
                            .withVoiceSearchHidden()
                            .shouldReturnOkOnBackPressed()
                            .build(getActivity());
                    startActivityForResult(locationPickerIntent, 99);
                }
            }
        });

        buttonShpAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!checkPermission()) {
                        requestPermission();
                    } else {
                        isShpClicked = true;
                        double lat = Double.valueOf(strShpLat);
                        double lng = Double.valueOf(strShpLng);
                        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                                .withLocation(lat, lng)
                                .withGeolocApiKey(getString(R.string.google_maps_key))
                                .withGooglePlacesEnabled()
                                .withSatelliteViewHidden()
                                .withVoiceSearchHidden()
                                .shouldReturnOkOnBackPressed()
                                .build(getActivity());
                        startActivityForResult(locationPickerIntent, 99);
                    }
                } else {
                    isShpClicked = true;
                    double lat = Double.valueOf(strShpLat);
                    double lng = Double.valueOf(strShpLng);
                    Intent locationPickerIntent = new LocationPickerActivity.Builder()
                            .withLocation(lat, lng)
                            .withGeolocApiKey(getString(R.string.google_maps_key))
                            .withGooglePlacesEnabled()
                            .withSatelliteViewHidden()
                            .withVoiceSearchHidden()
                            .shouldReturnOkOnBackPressed()
                            .build(getActivity());
                    startActivityForResult(locationPickerIntent, 99);
                }
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = edittextName.getText().toString().trim();
                String account = strAccount;
                String contact = strContact;
                String description = edittextDescription.getText().toString().trim();
                String owner = strOwner;
                String status = listStatus.get(spinnerStatus.getSelectedItemPosition()).getId();
                String expiration_date = strExpirationDate;
                String phone = edittextPhone.getText().toString().trim();
                String email = edittextEmail.getText().toString().trim();

                String bilName = edittextBilName.getText().toString().trim();
                String bilAddress = edittextBilAddress.getText().toString().trim();
                String bilCity = edittextBilCity.getText().toString().trim();
                String bilState = edittextBilState.getText().toString().trim();
                String bilCountry = edittextBilCountry.getText().toString().trim();
                String bilPostal = edittextBilPostal.getText().toString().trim();
                String bilLat = strBilLat;
                String bilLng = strBilLng;
                String shpName = edittextShpName.getText().toString().trim();
                String shpAddress = edittextShpAddress.getText().toString().trim();
                String shpCity = edittextShpCity.getText().toString().trim();
                String shpState = edittextShpState.getText().toString().trim();
                String shpCountry = edittextShpCountry.getText().toString().trim();
                String shpPostal = edittextShpPostal.getText().toString().trim();
                String shpLat = strShpLat;
                String shpLng = strShpLng;
                if (TextUtils.isEmpty(account)) {
                    Utl.showToast(getActivity(), "Select Account");
                } else if (TextUtils.isEmpty(contact)) {
                    Utl.showToast(getActivity(), "Select Contact");
                } else if (TextUtils.isEmpty(description)) {
                    Utl.showToast(getActivity(), "Enter Description");
                } else if (TextUtils.isEmpty(owner)) {
                    Utl.showToast(getActivity(), "Select Owner");
                } else if (TextUtils.isEmpty(phone)) {
                    Utl.showToast(getActivity(), "Enter Phone");
                } else if (TextUtils.isEmpty(email)) {
                    Utl.showToast(getActivity(), "Enter Email");
                } else if (TextUtils.isEmpty(bilName)) {
                    Utl.showToast(getActivity(), "Enter Billing Name");
                } else if (TextUtils.isEmpty(bilAddress)) {
                    Utl.showToast(getActivity(), "Enter Billing Address");
                } else if (TextUtils.isEmpty(bilCity)) {
                    Utl.showToast(getActivity(), "Enter Billing City");
                } else if (TextUtils.isEmpty(bilState)) {
                    Utl.showToast(getActivity(), "Enter Billing State");
                } else if (TextUtils.isEmpty(bilCountry)) {
                    Utl.showToast(getActivity(), "Enter Billing Country");
                } else if (TextUtils.isEmpty(bilPostal)) {
                    Utl.showToast(getActivity(), "Enter Billing Postal Code");
                } else if (TextUtils.isEmpty(shpName)) {
                    Utl.showToast(getActivity(), "Enter Shipping Name");
                } else if (TextUtils.isEmpty(shpAddress)) {
                    Utl.showToast(getActivity(), "Enter Shipping Address");
                } else if (TextUtils.isEmpty(shpCity)) {
                    Utl.showToast(getActivity(), "Enter Shipping City");
                } else if (TextUtils.isEmpty(shpState)) {
                    Utl.showToast(getActivity(), "Enter Shipping State");
                } else if (TextUtils.isEmpty(shpCountry)) {
                    Utl.showToast(getActivity(), "Enter Shipping Country");
                } else if (TextUtils.isEmpty(shpPostal)) {
                    Utl.showToast(getActivity(), "Enter Shipping Postal Code");
                } else {
                    createEstimate(name,
                            account,
                            contact,
                            description,
                            owner,
                            status,
                            expiration_date,
                            phone,
                            email,
                            bilName, bilAddress, bilCity, bilState, bilCountry, bilPostal,
                            bilLat, bilLng,
                            shpName, shpAddress, shpCity, shpState, shpCountry, shpPostal,
                            shpLat, shpLng);
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickClose.callbackEstimateCreateClose(back_type, false, "", "");
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99) {
            if (data != null) {
                if (isBilClicked && !isShpClicked) {
                    double latitude = data.getDoubleExtra("latitude", 0.0);
                    strBilLat = String.valueOf(latitude);
                    double longitude = data.getDoubleExtra("longitude", 0.0);
                    strBilLng = String.valueOf(longitude);
                    Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    List<Address> addresses = new ArrayList<>();
                    try {
                        addresses = geocoder.getFromLocation(latitude, longitude, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    strBilAddress = addresses.get(0).getSubThoroughfare() + ", " + addresses.get(0).getThoroughfare();
                    if (addresses.get(0).getLocality() != null) {
                        strBilCity = addresses.get(0).getLocality();
                    } else {
                        strBilCity = addresses.get(0).getSubLocality();
                    }
                    if (addresses.get(0).getAdminArea() != null) {
                        strBilState = addresses.get(0).getAdminArea();
                    } else {
                        strBilState = addresses.get(0).getSubAdminArea();
                    }
                    strBilCountry = addresses.get(0).getCountryName();
                    strBilPostal = addresses.get(0).getPostalCode();
                    Log.d("TAG_LATITUDE****", strBilLat);
                    Log.d("TAG_LONGITUDE****", strBilLng);
                    Log.d("TAG_ADDRESS****", strBilAddress);
                    Log.d("TAG_CITY****", strBilCity);
                    Log.d("TAG_STATE****", strBilState);
                    Log.d("TAG_COUNTRY****", strBilCountry);
                    Log.d("TAG_POSTAL****", strBilPostal);
                    edittextBilAddress.setText(strBilAddress);
                    edittextBilCity.setText(strBilCity);
                    edittextBilState.setText(strBilState);
                    edittextBilCountry.setText(strBilCountry);
                    edittextBilPostal.setText(strBilPostal);
                    isBilClicked = false;
                    isShpClicked = false;
                } else if (!isBilClicked && isShpClicked) {
                    double latitude = data.getDoubleExtra("latitude", 0.0);
                    strShpLat = String.valueOf(latitude);
                    double longitude = data.getDoubleExtra("longitude", 0.0);
                    strShpLng = String.valueOf(longitude);
                    Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    List<Address> addresses = new ArrayList<>();
                    try {
                        addresses = geocoder.getFromLocation(latitude, longitude, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    strShpAddress = addresses.get(0).getSubThoroughfare() + ", " + addresses.get(0).getThoroughfare();
                    if (addresses.get(0).getLocality() != null) {
                        strShpCity = addresses.get(0).getLocality();
                    } else {
                        strShpCity = addresses.get(0).getSubLocality();
                    }
                    if (addresses.get(0).getAdminArea() != null) {
                        strShpState = addresses.get(0).getAdminArea();
                    } else {
                        strShpState = addresses.get(0).getSubAdminArea();
                    }
                    strShpCountry = addresses.get(0).getCountryName();
                    strShpPostal = addresses.get(0).getPostalCode();
                    Log.d("TAG_LATITUDE****", strShpLat);
                    Log.d("TAG_LONGITUDE****", strShpLng);
                    Log.d("TAG_ADDRESS****", strShpAddress);
                    Log.d("TAG_CITY****", strShpCity);
                    Log.d("TAG_STATE****", strShpState);
                    Log.d("TAG_COUNTRY****", strShpCountry);
                    Log.d("TAG_POSTAL****", strShpPostal);
                    edittextShpAddress.setText(strShpAddress);
                    edittextShpCity.setText(strShpCity);
                    edittextShpState.setText(strShpState);
                    edittextShpCountry.setText(strShpCountry);
                    edittextShpPostal.setText(strShpPostal);
                    isBilClicked = false;
                    isShpClicked = false;
                }
            } else {
                Log.d("RESULT", "CANCELLED");
            }
        }
    }

    private void setSpinnerDropDownHeight(List<ModelSpinner> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 70);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    public int getListIndex(List<ModelSpinner> list, String name) {
        for (int i = 0; i < list.size(); i++) {
            ModelSpinner model = list.get(i);
            if (name.equals(model.getName())) {
                return i;
            }
        }
        return -1;
    }

    public String formatDate(String time) {
        String inputPattern = "MM/dd/yyyy hh:mm aaa";
        String outputPattern = "dd MMM yyyy hh:mm aaa";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String setTime(int min, int hr, String AM_PM, int format) {
        String time = "" + hr + ":" + min + " " + AM_PM;
        String inputPattern;
        String outputPattern;
        if (format == 0) {
            inputPattern = "hh:mm aaa";
            outputPattern = "hh:mm aaa";
        } else {
            inputPattern = "hh:mm aaa";
            outputPattern = "kk:mm:ss";
        }
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String setDate(int min, int hr, int d, int m, int y, String AM_PM) {
        String time = "" + (m + 1) + "/" + d + "/" + y + " " + hr + ":" + min + " " + AM_PM;
        String inputPattern = "MM/dd/yyyy hh:mm aaa";
        String outputPattern = "MM/dd/yyyy hh:mm aaa";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void getAccounts() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getEstimateAccounts(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Accounts" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAccounts.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_AccountID));
                                model.setName(dataObject.getString(Cons.KEY_AccountName));
                                listAccounts.add(model);
                            }
                            for (int i = 0; i < listAccounts.size(); i++) {
                                phvAccountAdd
                                        .addView(new AdapterAccount(getActivity(), FragmentEstimateCreate.this, i, listAccounts.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getContacts() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getEstimateContacts(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Contacts" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listContacts.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_ContactID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listContacts.add(model);
                            }
                            for (int i = 0; i < listContacts.size(); i++) {
                                phvContactAdd
                                        .addView(new AdapterContact(getActivity(), FragmentEstimateCreate.this, i, listContacts.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getOwner() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getEstimateOwners(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_AllUsers" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listOwners.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_UserID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listOwners.add(model);
                            }
                            for (int i = 0; i < listOwners.size(); i++) {
                                phvOwnerAdd
                                        .addView(new AdapterOwner(getActivity(), FragmentEstimateCreate.this, i, listOwners.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getStatus() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getEstimateStatus(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Status" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listStatus.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_EstimateStatusID));
                                model.setName(dataObject.getString(Cons.KEY_Status));
                                listStatus.add(model);
                            }
                        }
                        spinnerStatus.setAdapter(adapterStatus);
                        setSpinnerDropDownHeight(listStatus, spinnerStatus);
                        spinnerStatus.setSelection(0);
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void createEstimate(String name,
                                String account,
                                String contact,
                                String description,
                                String owner,
                                String status,
                                String expirationDate,
                                String phone,
                                String email,
                                String bilName, String bilAddress, String bilCity, String bilState, String bilCountry, String bilPostal,
                                String bilLat, String bilLng,
                                String shpName, String shpAddress, String shpCity, String shpState, String shpCountry, String shpPostal,
                                String shpLat, String shpLng) {

        Log.d("TAG_estimateName", name);
        Log.d("TAG_account", account);
        Log.d("TAG_contact", contact);
        Log.d("TAG_description", description);
        Log.d("TAG_owner", owner);
        Log.d("TAG_status", status);
        Log.d("TAG_expirationDate", expirationDate);
        Log.d("TAG_phone", phone);
        Log.d("TAG_email", email);
        Log.d("TAG_bilLat", bilLat);
        Log.d("TAG_bilLng", bilLng);
        Log.d("TAG_bilName", bilName);
        Log.d("TAG_bilAddress", bilAddress);
        Log.d("TAG_bilCity", bilCity);
        Log.d("TAG_bilState", bilState);
        Log.d("TAG_bilCountry", bilCountry);
        Log.d("TAG_bilPostal", bilPostal);
        Log.d("TAG_shpLat", shpLat);
        Log.d("TAG_shpLng", shpLng);
        Log.d("TAG_shpName", shpName);
        Log.d("TAG_shpAddress", shpAddress);
        Log.d("TAG_shpCity", shpCity);
        Log.d("TAG_shpState", shpState);
        Log.d("TAG_shpCountry", shpCountry);
        Log.d("TAG_shpPostal", shpPostal);

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_EstimateName, RequestBody.create(MediaType.parse("text/plain"), name));
        map.put(Cons.KEY_Account, RequestBody.create(MediaType.parse("text/plain"), account));
        map.put(Cons.KEY_Contact, RequestBody.create(MediaType.parse("text/plain"), contact));
        map.put(Cons.KEY_Description, RequestBody.create(MediaType.parse("text/plain"), description));
        map.put(Cons.KEY_Owner, RequestBody.create(MediaType.parse("text/plain"), owner));
        map.put(Cons.KEY_EstimateStatus, RequestBody.create(MediaType.parse("text/plain"), status));
        map.put(Cons.KEY_ExpirationDate, RequestBody.create(MediaType.parse("text/plain"), expirationDate));
        map.put(Cons.KEY_Phone, RequestBody.create(MediaType.parse("text/plain"), phone));
        map.put(Cons.KEY_Email, RequestBody.create(MediaType.parse("text/plain"), email));
        map.put(Cons.KEY_BillingLatitude, RequestBody.create(MediaType.parse("text/plain"), bilLat));
        map.put(Cons.KEY_BillingLongitude, RequestBody.create(MediaType.parse("text/plain"), bilLng));
        map.put(Cons.KEY_BillingName, RequestBody.create(MediaType.parse("text/plain"), bilName));
        map.put(Cons.KEY_BillingAddress, RequestBody.create(MediaType.parse("text/plain"), bilAddress));
        map.put(Cons.KEY_BillingCity, RequestBody.create(MediaType.parse("text/plain"), bilCity));
        map.put(Cons.KEY_BillingState, RequestBody.create(MediaType.parse("text/plain"), bilState));
        map.put(Cons.KEY_BillingCountry, RequestBody.create(MediaType.parse("text/plain"), bilCountry));
        map.put(Cons.KEY_BillingPostalCode, RequestBody.create(MediaType.parse("text/plain"), bilPostal));
        map.put(Cons.KEY_ShippingLatitude, RequestBody.create(MediaType.parse("text/plain"), shpLat));
        map.put(Cons.KEY_ShippingLongitude, RequestBody.create(MediaType.parse("text/plain"), shpLng));
        map.put(Cons.KEY_ShippingName, RequestBody.create(MediaType.parse("text/plain"), shpName));
        map.put(Cons.KEY_ShippingAddress, RequestBody.create(MediaType.parse("text/plain"), shpAddress));
        map.put(Cons.KEY_ShippingCity, RequestBody.create(MediaType.parse("text/plain"), shpCity));
        map.put(Cons.KEY_ShippingState, RequestBody.create(MediaType.parse("text/plain"), shpState));
        map.put(Cons.KEY_ShippingCountry, RequestBody.create(MediaType.parse("text/plain"), shpCountry));
        map.put(Cons.KEY_ShippingPostalCode, RequestBody.create(MediaType.parse("text/plain"), shpPostal));
        Call<ResponseBody> response;
        response = apiInterface.createEstimateDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Estimate_Create" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            //JSONObject dataObject = object.getJSONObject("data");
                            String estimateID = object.getString(Cons.KEY_EstimateID);
                            String estimateNo = object.getString(Cons.KEY_EstimateNo);
                            clickClose.callbackEstimateCreateClose(back_type, true, estimateID, estimateNo);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    public boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), CAMERA);
        int result2 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION);
        int result3 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_COARSE_LOCATION);
        int result4 = ContextCompat.checkSelfPermission(getActivity(), CALL_PHONE);
        int result5 = ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE);
        int result6 = ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE);
        return
                result1 == PackageManager.PERMISSION_GRANTED
                        && result2 == PackageManager.PERMISSION_GRANTED
                        && result3 == PackageManager.PERMISSION_GRANTED
                        && result4 == PackageManager.PERMISSION_GRANTED
                        && result5 == PackageManager.PERMISSION_GRANTED
                        && result6 == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{
                        CAMERA,
                        ACCESS_FINE_LOCATION,
                        ACCESS_COARSE_LOCATION,
                        CALL_PHONE,
                        READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean locationFineAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean locationCoarseAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean callAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean readAccepted = grantResults[4] == PackageManager.PERMISSION_GRANTED;
                    boolean writeAccepted = grantResults[5] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted &&
                            locationFineAccepted &&
                            locationCoarseAccepted &&
                            callAccepted &&
                            readAccepted &&
                            writeAccepted)
                        Utl.showToast(getActivity(), "Permission Granted.");
                    else {
                        Utl.showToast(getActivity(), "Permission Denied.");
                    }
                }
                break;
        }
    }

    @Override
    public void callbackAddAccountListen(int pos, ModelSpinner model) {
        phvAccountAdd.removeAllViews();
        layoutAccountAdd.setVisibility(View.GONE);
        strAccount = model.getId();
        strAccountName = model.getName();
        edittextAccount.setText(strAccountName);
    }

    @Override
    public void callbackAddContactListen(int pos, ModelSpinner model) {
        phvContactAdd.removeAllViews();
        layoutContactAdd.setVisibility(View.GONE);
        strContact = model.getId();
        strContactName = model.getName();
        edittextContact.setText(strContactName);
    }

    @Override
    public void callbackAddOwnerListen(int pos, ModelSpinner model) {
        phvOwnerAdd.removeAllViews();
        layoutOwnerAdd.setVisibility(View.GONE);
        strOwner = model.getId();
        strOwnerName = model.getName();
        edittextOwner.setText(strOwnerName);
    }

}