package com.fieldwise.fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.fieldwise.R;
import com.fieldwise.activities.ActivityMain;
import com.fieldwise.adapters.AdapterAssignedTo;
import com.fieldwise.models.ModelSpinner;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;
import com.mindorks.placeholderview.PlaceHolderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentTaskCreate extends Fragment implements
        AdapterAssignedTo.AddAssignedToClickListen {

    int back_type;

    public APIInterface apiInterface;

    private String strSubject,
            strAssignedTo, strAssignedToNew, strAssignedToName, strAssignedToNameNew,
            strRelatedTo,
            strWhat, strWhatName,
            strWho, strWhoName,
            strStartDateTimeDate, strNewStartDateTimeDate, strStartDateTimeTime, strNewStartDateTimeTime,
            strEndDateTimeDate, strNewEndDateTimeDate, strEndDateTimeTime, strNewEndDateTimeTime,
            strDueDate, strDueDateNew,
            strDescription,
            strEmail,
            strPhone,
            strType, strTypeName,
            strStatus, strStatusName,
            strPriority, strPriorityName;

    private boolean isAssignedToClicked = false;
    private boolean isWhatClicked = false;
    private boolean isWhoClicked = false;
    private boolean isStartDateClicked = false;
    private boolean isEndDateClicked = false;
    private boolean isDueDateClicked = false;

    private boolean isRecurrenceStartDateClicked = false;
    private boolean isRecurrenceEndDateClicked = false;
    private boolean isRepeatEveryClicked = false;
    private boolean isIntervalClicked = false;
    private boolean isStartDateOnClicked = false;
    private boolean isEndDateOnClicked = false;

    private Spinner spinnerRelatedTo,
            spinnerWhat, spinnerWho,
            spinnerType, spinnerStatus, spinnerPriority;

    private List<ModelSpinner> listAssignedTo,
            listRelatedTo,
            listWhat, listWho,
            listType, listStatus, listPriority;

    private ArrayAdapter<ModelSpinner> adapterRelatedTo,
            adapterWhat, adapterWho,
            adapterType, adapterStatus, adapterPriority;

    private EditText editTextSubject,
            edittextAssignedTo,
            edittextDescription,
            edittextEmail,
            edittextPhone;

    private TextView edittextStartDateTime, edittextEndDateTime, edittextDueDate;

    private CheckBox checkboxIsRecurring;

    private String strRecurring,
            strRepeatEvery, strNewRepeatEvery,
            strIntervalEvery, strNewIntervalEvery,
            strSun, strMon, strTue, strWed, strThu, strFri, strSat,
            strStartOn, strStartOnNew,
            strEnds,
            strEndsOn, strEndsOnNew,
            strEndsAfter,
            strRecurrenceStartTime, strRecurrenceNewStartTime, strRecurrenceEndTime, strRecurrenceNewEndTime;

    private RelativeLayout layoutRecurrence, layoutEndAfter;
    private LinearLayout layoutRecurrenceRepeatOn;
    private CheckBox checkboxSun, checkboxMon, checkboxTue, checkboxWed, checkboxThu, checkboxFri, checkboxSat;
    private Spinner spinnerRepeatsEvery, spinnerIntervalEvery, spinnerEndsAfter;
    private TextView textviewStartOnDate, textviewRepeatsEveryPostfix, textviewEndsOnDate, textviewEndsAfterPostfix,
            edittextRecurrenceStartTime, edittextRecurrenceEndTime;
    private RadioGroup radioGroupRecurrence;
    private RadioButton radioButtonRecurrenceNever, radioButtonRecurrenceOn, radioButtonRecurrenceAfter;
    private Button buttonRecurrenceSave, buttonRecurrenceClose;

    private List<String> listRecurrenceRepeat, listRecurrenceInterval, listRecurrenceEndAfter;
    private ArrayAdapter<String> adapterRecurrenceRepeat, adapterRecurrenceInterval, adapterRecurrenceEndAfter;

    private Button buttonSave, buttonCancel;

    private ImageButton buttonAssignedTo;
    private LinearLayout layoutAssignedToAdd;
    private EditText edittextAssignedToAddSearch;
    private PlaceHolderView phvAssignedToAdd;

    public ClickListenTaskCreateClose clickClose;

    public interface ClickListenTaskCreateClose {
        void callbackTaskCreateClose(int back_type, boolean isSave, String taskID, String subject);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_task_create, container, false);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            back_type = bundle.getInt("back_type", 0);
        }

        apiInterface = APIClient.getClient().create(APIInterface.class);

        clickClose = (ClickListenTaskCreateClose) getActivity();

        spinnerRelatedTo = (Spinner) view.findViewById(R.id.spinner_related_to);
        spinnerWhat = (Spinner) view.findViewById(R.id.spinner_what);
        spinnerWho = (Spinner) view.findViewById(R.id.spinner_who);
        spinnerType = (Spinner) view.findViewById(R.id.spinner_type);
        spinnerStatus = (Spinner) view.findViewById(R.id.spinner_status);
        spinnerPriority = (Spinner) view.findViewById(R.id.spinner_priority);

        editTextSubject = (EditText) view.findViewById(R.id.edittext_subject);
        edittextAssignedTo = (EditText) view.findViewById(R.id.edittext_assigned_to);
        edittextDescription = (EditText) view.findViewById(R.id.edittext_description);
        edittextEmail = (EditText) view.findViewById(R.id.edittext_email);
        edittextPhone = (EditText) view.findViewById(R.id.edittext_phone);

        edittextStartDateTime = (TextView) view.findViewById(R.id.edittext_start_date_time);
        edittextEndDateTime = (TextView) view.findViewById(R.id.edittext_end_date_time);
        edittextDueDate = (TextView) view.findViewById(R.id.edittext_due_date);

        buttonSave = (Button) view.findViewById(R.id.button_save);
        buttonCancel = (Button) view.findViewById(R.id.button_cancel);

        buttonAssignedTo = (ImageButton) view.findViewById(R.id.button_assigned_to);
        layoutAssignedToAdd = (LinearLayout) view.findViewById(R.id.layout_assigned_to_add);
        edittextAssignedToAddSearch = (EditText) view.findViewById(R.id.edittext_assigned_to_add_search);
        phvAssignedToAdd = (PlaceHolderView) view.findViewById(R.id.phv_assigned_to_add);

        checkboxIsRecurring = (CheckBox) view.findViewById(R.id.checkbox_is_recurring);

        layoutRecurrence = (RelativeLayout) view.findViewById(R.id.layout_recurrence);
        spinnerRepeatsEvery = (Spinner) view.findViewById(R.id.spinner_repeats_every);
        spinnerIntervalEvery = (Spinner) view.findViewById(R.id.spinner_interval_every);
        textviewRepeatsEveryPostfix = (TextView) view.findViewById(R.id.textview_repeats_every_postfix);
        layoutRecurrenceRepeatOn = (LinearLayout) view.findViewById(R.id.layout_recurrence_repeat_on);
        checkboxSun = (CheckBox) view.findViewById(R.id.checkbox_sun);
        checkboxMon = (CheckBox) view.findViewById(R.id.checkbox_mon);
        checkboxTue = (CheckBox) view.findViewById(R.id.checkbox_tue);
        checkboxWed = (CheckBox) view.findViewById(R.id.checkbox_wed);
        checkboxThu = (CheckBox) view.findViewById(R.id.checkbox_thu);
        checkboxFri = (CheckBox) view.findViewById(R.id.checkbox_fri);
        checkboxSat = (CheckBox) view.findViewById(R.id.checkbox_sat);
        textviewStartOnDate = (TextView) view.findViewById(R.id.textview_start_on_date);
        textviewEndsOnDate = (TextView) view.findViewById(R.id.textview_ends_on_date);
        edittextRecurrenceStartTime = (TextView) view.findViewById(R.id.textview_recurrence_start_time);
        edittextRecurrenceEndTime = (TextView) view.findViewById(R.id.textview_recurrence_end_time);
        radioGroupRecurrence = (RadioGroup) view.findViewById(R.id.radio_group_recurrence);
        radioButtonRecurrenceNever = (RadioButton) view.findViewById(R.id.radio_button_recurrence_never);
        radioButtonRecurrenceOn = (RadioButton) view.findViewById(R.id.radio_button_recurrence_on);
        radioButtonRecurrenceAfter = (RadioButton) view.findViewById(R.id.radio_button_recurrence_after);
        layoutEndAfter = (RelativeLayout) view.findViewById(R.id.layout_end_after);
        spinnerEndsAfter = (Spinner) view.findViewById(R.id.spinner_end_after);
        textviewEndsAfterPostfix = (TextView) view.findViewById(R.id.textview_ends_after_postfix);
        buttonRecurrenceSave = (Button) view.findViewById(R.id.button_recurrence_save);
        buttonRecurrenceClose = (Button) view.findViewById(R.id.button_recurrence_close);

        listAssignedTo = new ArrayList<>();
        listRelatedTo = new ArrayList<>();
        listWhat = new ArrayList<>();
        listWho = new ArrayList<>();
        listType = new ArrayList<>();
        listStatus = new ArrayList<>();
        listPriority = new ArrayList<>();
        adapterRelatedTo = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listRelatedTo);
        adapterRelatedTo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterWhat = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listWhat);
        adapterWhat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterWho = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listWho);
        adapterWho.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterType = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listType);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterStatus = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listStatus);
        adapterStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterPriority = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listPriority);
        adapterPriority.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        listRecurrenceRepeat = new ArrayList<>();
        listRecurrenceRepeat.add("Daily");
        listRecurrenceRepeat.add("Weekly");
        listRecurrenceRepeat.add("Monthly");
        listRecurrenceRepeat.add("Yearly");
        listRecurrenceRepeat.add("Periodically");
        listRecurrenceInterval = new ArrayList<>();
        listRecurrenceInterval.add("1");
        listRecurrenceInterval.add("2");
        listRecurrenceInterval.add("3");
        listRecurrenceInterval.add("4");
        listRecurrenceInterval.add("5");
        listRecurrenceEndAfter = new ArrayList<>();
        listRecurrenceEndAfter.add("1");
        listRecurrenceEndAfter.add("2");
        listRecurrenceEndAfter.add("3");
        listRecurrenceEndAfter.add("4");
        listRecurrenceEndAfter.add("5");
        adapterRecurrenceRepeat = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listRecurrenceRepeat);
        adapterRecurrenceRepeat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterRecurrenceInterval = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listRecurrenceInterval);
        adapterRecurrenceInterval.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterRecurrenceEndAfter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listRecurrenceEndAfter);
        adapterRecurrenceEndAfter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DATE);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        final int hour = calendar.get(Calendar.HOUR);
        final int minute = calendar.get(Calendar.MINUTE);
        final int AP = calendar.get(Calendar.AM_PM);
        String AM_PM;
        if (AP == 0) {
            AM_PM = "AM;";
        } else {
            AM_PM = "PM";
        }

        strStartDateTimeDate = setDate(minute, hour, day, month, year, AM_PM);
        strNewStartDateTimeDate = setDate(minute, hour, day, month, year, AM_PM);
        strStartDateTimeTime = setTime(minute, hour, AM_PM, 0);
        strNewStartDateTimeTime = setTime(minute, hour, AM_PM, 0);
        strEndDateTimeDate = setDate(minute, hour, day, month, year, AM_PM);
        strNewEndDateTimeDate = setDate(minute, hour, day, month, year, AM_PM);
        strEndDateTimeTime = setTime(minute, hour, AM_PM, 0);
        strNewEndDateTimeTime = setTime(minute, hour, AM_PM, 0);
        strDueDate = setDueDate(day, month, year);

        strRecurrenceStartTime = setTime(minute, hour, AM_PM, 1);
        strRecurrenceNewStartTime = setTime(minute, hour, AM_PM, 1);
        strRecurrenceEndTime = setTime(minute, hour, AM_PM, 1);
        strRecurrenceNewEndTime = setTime(minute, hour, AM_PM, 1);
        strStartOn = selectSimpleDate(day, month, year);
        strStartOnNew = selectSimpleDate(day, month, year);
        strEndsOn = selectSimpleDate(day, month, year);
        strEndsOnNew = selectSimpleDate(day, month, year);

        strRepeatEvery = "";
        strNewRepeatEvery = "";
        strIntervalEvery = "";
        strNewIntervalEvery = "";
        strEnds = "";
        strEndsAfter = "";
        strSun = "";
        strMon = "";
        strTue = "";
        strWed = "";
        strThu = "";
        strFri = "";
        strSat = "";

        edittextStartDateTime.setText(formatDate(strStartDateTimeDate) + " " + strStartDateTimeTime);
        edittextEndDateTime.setText(formatDate(strEndDateTimeDate) + " " + strEndDateTimeTime);
        edittextDueDate.setText(formatDate(strDueDate));

        getAssignedTo();
        getRelatedTo();
        getWho();
        getType();
        getStatue();
        getPriority();

        buttonAssignedTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isAssignedToClicked = true;
                layoutAssignedToAdd.setVisibility(View.VISIBLE);
                listAssignedTo.clear();
                getAssignedTo();
            }
        });

        spinnerRelatedTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String str = listRelatedTo.get(position).getName();
                getWhat(str);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        edittextStartDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                final int hour = calendar.get(Calendar.HOUR);
                final int minute = calendar.get(Calendar.MINUTE);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hr, int min) {
                                isStartDateClicked = true;
                                Calendar c = Calendar.getInstance();
                                c.set(0, 0, 0, hr, min);
                                String AM_PM = (String) android.text.format.DateFormat.format("aaa", c);
                                if (hr > 12) {
                                    hr = hr - 12;
                                }
                                strNewStartDateTimeDate = setDate(min, hr, d, m, y, AM_PM);
                                strNewStartDateTimeTime = setTime(min, hr, AM_PM, 0);
                                edittextStartDateTime.setText(formatDate(strNewStartDateTimeDate) +
                                        " " + strNewStartDateTimeTime);
                            }
                        }, hour, minute, false);
                        timePickerDialog.show();
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        edittextEndDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                final int hour = calendar.get(Calendar.HOUR);
                final int minute = calendar.get(Calendar.MINUTE);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hr, int min) {
                                isEndDateClicked = true;
                                Calendar c = Calendar.getInstance();
                                c.set(0, 0, 0, hr, min);
                                String AM_PM = (String) android.text.format.DateFormat.format("aaa", c);
                                if (hr > 12) {
                                    hr = hr - 12;
                                }
                                strNewEndDateTimeDate = setDate(min, hr, d, m, y, AM_PM);
                                strNewEndDateTimeTime = setTime(min, hr, AM_PM, 0);
                                edittextEndDateTime.setText(formatDate(strNewEndDateTimeDate) +
                                        " " + strNewEndDateTimeTime);
                            }
                        }, hour, minute, false);
                        timePickerDialog.show();
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        edittextDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                Date date = null;
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                if (isDueDateClicked) {
                    try {
                        date = sdf.parse(strDueDateNew);
                    } catch (ParseException ex) {
                        Log.v("Exception", ex.getLocalizedMessage());
                    }
                } else {
                    try {
                        date = sdf.parse(strDueDate);
                    } catch (ParseException ex) {
                        Log.v("Exception", ex.getLocalizedMessage());
                    }
                }
                calendar.setTime(date);
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                final int hour = calendar.get(Calendar.HOUR);
                final int minute = calendar.get(Calendar.MINUTE);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        isDueDateClicked = true;
                        strDueDateNew = setDueDate(d, m, y);
                        edittextDueDate.setText(formatDate(strDueDateNew));
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        checkboxIsRecurring.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    ActivityMain.isRecurringOn = true;
                    layoutRecurrence.setVisibility(View.VISIBLE);
                    showView(layoutRecurrence);

                    isStartDateOnClicked = false;
                    textviewStartOnDate.setText(formatSimpleDate(strStartOn));
                    strStartOnNew = "";

                    spinnerRepeatsEvery.setAdapter(adapterRecurrenceRepeat);
                    spinnerRepeatsEvery.setSelection(0);
                    setStrSpinnerDropDownHeight(spinnerRepeatsEvery);
                    strRepeatEvery = listRecurrenceRepeat.get(0);
                    textviewRepeatsEveryPostfix.setText(listRecurrenceRepeat.get(0) + " On");

                    spinnerIntervalEvery.setAdapter(adapterRecurrenceInterval);
                    spinnerIntervalEvery.setSelection(0);
                    setStrSpinnerDropDownHeight(spinnerIntervalEvery);
                    strIntervalEvery = listRecurrenceInterval.get(0);

                    checkboxSun.setChecked(false);
                    checkboxMon.setChecked(false);
                    checkboxTue.setChecked(false);
                    checkboxWed.setChecked(false);
                    checkboxThu.setChecked(false);
                    checkboxFri.setChecked(false);
                    checkboxSat.setChecked(false);

                    isRepeatEveryClicked = false;
                    isIntervalClicked = false;

                    radioButtonRecurrenceNever.setChecked(true);
                    strEnds = "Never";

                    isEndDateOnClicked = false;
                    textviewEndsOnDate.setText(formatSimpleDate(strEndsOn));
                    strEndsOnNew = "";

                    spinnerEndsAfter.setAdapter(adapterRecurrenceEndAfter);
                    spinnerEndsAfter.setSelection(0);
                    setStrSpinnerDropDownHeight(spinnerEndsAfter);
                    strEndsAfter = listRecurrenceEndAfter.get(0);
                    strEndsAfter = "";

                    edittextRecurrenceStartTime.setText(strRecurrenceStartTime);
                    edittextRecurrenceEndTime.setText(strRecurrenceEndTime);
                }
            }
        });

        textviewStartOnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                final int day = calendar.get(Calendar.DATE);
                final int month = calendar.get(Calendar.MONTH);
                final int year = calendar.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        isStartDateOnClicked = true;
                        strStartOnNew = selectSimpleDate(d, m, y);
                        textviewStartOnDate.setText(formatSimpleDate(strStartOnNew));
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        spinnerRepeatsEvery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                isRepeatEveryClicked = true;
                if (position == 1) {
                    layoutRecurrenceRepeatOn.setVisibility(View.VISIBLE);
                } else {
                    layoutRecurrenceRepeatOn.setVisibility(View.GONE);
                }
                strNewRepeatEvery = listRecurrenceRepeat.get(position);
                textviewRepeatsEveryPostfix.setText(strNewRepeatEvery + " On");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        spinnerIntervalEvery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                isIntervalClicked = true;
                strNewIntervalEvery = listRecurrenceInterval.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        checkboxSun.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strSun = "Sun";
                } else {
                    strSun = "";
                }
            }
        });

        checkboxMon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strMon = "Mon";
                } else {
                    strMon = "";
                }
            }
        });

        checkboxTue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strTue = "Tue";
                } else {
                    strTue = "";
                }
            }
        });

        checkboxWed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strWed = "Wed";
                } else {
                    strWed = "";
                }
            }
        });

        checkboxThu.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strThu = "Thu";
                } else {
                    strThu = "";
                }
            }
        });

        checkboxFri.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strFri = "Fri";
                } else {
                    strFri = "";
                }
            }
        });

        checkboxSat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strSat = "Sat";
                } else {
                    strSat = "";
                }
            }
        });

        radioButtonRecurrenceNever.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strEnds = "Never";
                    strEndsOnNew = "";
                    isEndDateOnClicked = false;
                    textviewEndsOnDate.setVisibility(View.GONE);
                    spinnerEndsAfter.setEnabled(false);
                    layoutEndAfter.setVisibility(View.GONE);
                    textviewEndsAfterPostfix.setVisibility(View.GONE);
                }
            }
        });

        radioButtonRecurrenceOn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strEnds = "On";
                    strEndsOnNew = "";
                    isEndDateOnClicked = false;
                    textviewEndsOnDate.setVisibility(View.VISIBLE);
                    if (isEndDateOnClicked) {
                        textviewEndsOnDate.setText(formatSimpleDate(strEndsOnNew));
                    } else {
                        Calendar calendar = Calendar.getInstance();
                        int day = calendar.get(Calendar.DATE);
                        int month = calendar.get(Calendar.MONTH);
                        int year = calendar.get(Calendar.YEAR);
                        strEndsOn = selectSimpleDate(day, month, year);
                        textviewEndsOnDate.setText(formatSimpleDate(strEndsOn));
                    }
                    spinnerEndsAfter.setEnabled(false);
                    layoutEndAfter.setVisibility(View.GONE);
                    textviewEndsAfterPostfix.setVisibility(View.GONE);
                }
            }
        });

        radioButtonRecurrenceAfter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strEnds = "After";
                    strEndsOn = "";
                    strEndsOnNew = "";
                    isEndDateOnClicked = false;

                    spinnerEndsAfter.setAdapter(adapterRecurrenceEndAfter);
                    spinnerEndsAfter.setSelection(0);
                    setStrSpinnerDropDownHeight(spinnerEndsAfter);
                    strEndsAfter = listRecurrenceEndAfter.get(0);

                    textviewEndsOnDate.setVisibility(View.GONE);
                    spinnerEndsAfter.setEnabled(true);
                    layoutEndAfter.setVisibility(View.VISIBLE);
                    textviewEndsAfterPostfix.setVisibility(View.VISIBLE);
                }
            }
        });

        textviewEndsOnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                final int day = calendar.get(Calendar.DATE);
                final int month = calendar.get(Calendar.MONTH);
                final int year = calendar.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        isEndDateOnClicked = true;
                        strEndsOnNew = selectSimpleDate(d, m, y);
                        textviewEndsOnDate.setText(formatSimpleDate(strEndsOnNew));
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        edittextRecurrenceStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                final int hour = calendar.get(Calendar.HOUR);
                final int minute = calendar.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hr, int min) {
                        isRecurrenceStartDateClicked = true;
                        Calendar c = Calendar.getInstance();
                        c.set(0, 0, 0, hr, min);
                        String AM_PM = (String) android.text.format.DateFormat.format("aaa", c);
                        if (hr > 12) {
                            hr = hr - 12;
                        }
                        strRecurrenceNewStartTime = setTime(min, hr, AM_PM, 1);
                        edittextRecurrenceStartTime.setText(strRecurrenceNewStartTime);
                    }
                }, hour, minute, false);
                timePickerDialog.show();
            }
        });

        edittextRecurrenceEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                final int hour = calendar.get(Calendar.HOUR);
                final int minute = calendar.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hr, int min) {
                        isRecurrenceEndDateClicked = true;
                        Calendar c = Calendar.getInstance();
                        c.set(0, 0, 0, hr, min);
                        String AM_PM = (String) android.text.format.DateFormat.format("aaa", c);
                        if (hr > 12) {
                            hr = hr - 12;
                        }
                        strRecurrenceNewStartTime = setTime(min, hr, AM_PM, 1);
                        edittextRecurrenceEndTime.setText(strRecurrenceNewStartTime);
                    }
                }, hour, minute, false);
                timePickerDialog.show();
            }
        });

        buttonRecurrenceSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityMain.isRecurringOn = false;
                layoutRecurrence.setVisibility(View.GONE);
            }
        });

        buttonRecurrenceClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityMain.isRecurringOn = false;
                layoutRecurrence.setVisibility(View.GONE);
                checkboxIsRecurring.setChecked(false);
                strStartOn = "";
                strRepeatEvery = "";
                strIntervalEvery = "";
                strEnds = "Never";
                strEndsOn = "";
                strEndsAfter = "";
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkboxIsRecurring.isChecked()) {
                    strRecurring = "1";
                } else {
                    strRecurring = "0";

                }

                String startOn = "";
                String repeatEvery = "";
                String intervalEvery = "";
                StringBuilder repeatOn = new StringBuilder();
                String ends = strEnds;
                String endsOn = "";
                String endsAfter = "";
                String recurrenceStartTime = "";
                String recurrenceEndTime = "";

                if (isStartDateOnClicked) {
                    startOn = strStartOnNew;
                } else {
                    startOn = strStartOn;
                }

                if (isRepeatEveryClicked) {
                    repeatEvery = strNewRepeatEvery;
                } else {
                    repeatEvery = strRepeatEvery;
                }

                if (isIntervalClicked) {
                    intervalEvery = strNewIntervalEvery;
                } else {
                    intervalEvery = strIntervalEvery;
                }

                repeatOn.append("");
                if (!strSun.equals("")) {
                    repeatOn.append(strSun).append(",");
                }
                if (!strMon.equals("")) {
                    repeatOn.append(strMon).append(",");
                }
                if (!strTue.equals("")) {
                    repeatOn.append(strTue).append(",");
                }
                if (!strWed.equals("")) {
                    repeatOn.append(strWed).append(",");
                }
                if (!strThu.equals("")) {
                    repeatOn.append(strThu).append(",");
                }
                if (!strFri.equals("")) {
                    repeatOn.append(strFri).append(",");
                }
                if (!strSat.equals("")) {
                    repeatOn.append(strSat).append(",");
                }

                if (repeatOn.toString().endsWith(",")) {
                    String str = repeatOn.substring(0, repeatOn.length() - 1);
                    repeatOn = new StringBuilder(str);
                }

                if (!strEnds.equals("Never")) {
                    if (ends.equals("On")) {
                        if (isEndDateOnClicked) {
                            endsOn = strEndsOnNew;
                        } else {
                            endsOn = strEndsOn;
                        }
                    } else if (ends.equals("After")) {
                        endsAfter = strEndsAfter;
                    }
                } else {
                    endsOn = "";
                    endsAfter = "";
                }

                if (isRecurrenceStartDateClicked) {
                    recurrenceStartTime = strRecurrenceNewStartTime;
                } else {
                    recurrenceStartTime = strRecurrenceStartTime;
                }
                if (isRecurrenceEndDateClicked) {
                    recurrenceEndTime = strRecurrenceNewEndTime;
                } else {
                    recurrenceEndTime = strRecurrenceEndTime;
                }

                String subject = editTextSubject.getText().toString().trim();
                String assignedTo = "";
                if (isAssignedToClicked) {
                    assignedTo = strAssignedToNew;
                } else {
                    assignedTo = strAssignedTo;
                }
                String relatedTo = listRelatedTo.get(spinnerRelatedTo.getSelectedItemPosition()).getName();
                String what = listWhat.get(spinnerWhat.getSelectedItemPosition()).getId();
                String who = listWho.get(spinnerWho.getSelectedItemPosition()).getId();
                String startDate = "";
                String endDate = "";
                String dueDate = "";
                if (isStartDateClicked) {
                    startDate = strNewStartDateTimeDate + " " + strNewStartDateTimeTime;
                } else {
                    startDate = strStartDateTimeDate + " " + strStartDateTimeTime;
                }
                if (isEndDateClicked) {
                    endDate = strNewEndDateTimeDate + " " + strNewEndDateTimeDate;
                } else {
                    endDate = strEndDateTimeDate + " " + strEndDateTimeTime;
                }
                if (isDueDateClicked) {
                    dueDate = strDueDateNew;
                } else {
                    dueDate = strDueDate;
                }
                String description = edittextDescription.getText().toString().trim();
                String email = edittextEmail.getText().toString().trim();
                String phone = edittextPhone.getText().toString().trim();
                String type = listType.get(spinnerType.getSelectedItemPosition()).getId();
                String status = listStatus.get(spinnerStatus.getSelectedItemPosition()).getId();
                String priority = listPriority.get(spinnerPriority.getSelectedItemPosition()).getId();

                if (TextUtils.isEmpty(subject)) {
                    Utl.showToast(getActivity(), "Enter Subject");
                } else if (!TextUtils.isEmpty(email)) {
                    if (!Patterns.EMAIL_ADDRESS.matcher(strEmail).matches()) {
                        Utl.showToast(getActivity(), getString(R.string.invalid_email));
                    }
                } else {
                    createTask(subject,
                            assignedTo,
                            relatedTo,
                            what,
                            who,
                            startDate, endDate, dueDate,
                            description,
                            email,
                            phone,
                            type,
                            status,
                            priority,
                            strRecurring, startOn,
                            repeatEvery, intervalEvery,
                            repeatOn.toString(), ends, endsOn, endsAfter, recurrenceStartTime, recurrenceEndTime);
                }

            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickClose.callbackTaskCreateClose(back_type, false, "", "");
            }
        });

        edittextAssignedToAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvAssignedToAdd.removeAllViews();
                    ArrayList<ModelSpinner> modle = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listAssignedTo) {
                        if (m.getName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvAssignedToAdd
                                .addView(new AdapterAssignedTo(getActivity(), FragmentTaskCreate.this, i, modle.get(i)));
                    }
                } else {
                    phvAssignedToAdd.removeAllViews();
                    for (int i = 0; i < listAssignedTo.size(); i++) {
                        phvAssignedToAdd
                                .addView(new AdapterAssignedTo(getActivity(), FragmentTaskCreate.this, i, listAssignedTo.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        return view;

    }

    private void showView(final View view) {
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_right);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }
        });
        view.startAnimation(animation);
    }

    private void setStrSpinnerDropDownHeight(Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 70);
    }

    private void setSpinnerDropDownHeight(List<ModelSpinner> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 70);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    public int getListIndex(List<ModelSpinner> list, String name) {
        for (int i = 0; i < list.size(); i++) {
            ModelSpinner model = list.get(i);
            if (name.equals(model.getName())) {
                return i;
            }
        }
        return -1;
    }

    public String selectSimpleDate(int d, int m, int y) {
        String time = "" + y + "-" + (m + 1) + "-" + d;
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String formatSimpleDate(String strDate) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(strDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String setTime(int min, int hr, String AM_PM, int format) {
        String time = "" + hr + ":" + min + " " + AM_PM;
        String inputPattern;
        String outputPattern;
        if (format == 0) {
            inputPattern = "hh:mm aaa";
            outputPattern = "hh:mm aaa";
        } else {
            inputPattern = "hh:mm aaa";
            outputPattern = "kk:mm:ss";
        }
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String setDate(int min, int hr, int d, int m, int y, String AM_PM) {
        String time = "" + (m + 1) + "/" + d + "/" + y + " " + hr + ":" + min + " " + AM_PM;
        String inputPattern = "MM/dd/yyyy hh:mm aaa";
        String outputPattern = "MM/dd/yyyy hh:mm aaa";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String formatDate(String time) {
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String setDueDate(int d, int m, int y) {
        String time = "" + (m + 1) + "/" + d + "/" + y;
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "MM/dd/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void getAssignedTo() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getTaskAssignedTo(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Invoice_AllU" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAssignedTo.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_UserID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listAssignedTo.add(model);
                            }
                            for (int i = 0; i < listAssignedTo.size(); i++) {
                                phvAssignedToAdd
                                        .addView(new AdapterAssignedTo(getActivity(), FragmentTaskCreate.this, i, listAssignedTo.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedTo() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getTaskRelatedTo(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Invoice_AllU" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listRelatedTo.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId("" + i);
                                model.setName(dataObject.getString(Cons.KEY_Name));
                                listRelatedTo.add(model);
                            }
                            spinnerRelatedTo.setAdapter(adapterRelatedTo);
                            setSpinnerDropDownHeight(listRelatedTo, spinnerRelatedTo);
                            spinnerRelatedTo.setSelection(0);
                            getWhat(listRelatedTo.get(0).getName());
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getWhat(String relatedTo) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), relatedTo));
        Call<ResponseBody> response = apiInterface.getTaskWhat(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Invoice_AllU" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listWhat.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_ID));
                                model.setName(dataObject.getString(Cons.KEY_Name));
                                listWhat.add(model);
                            }
                            spinnerWhat.setAdapter(adapterWhat);
                            setSpinnerDropDownHeight(listWhat, spinnerWhat);
                            spinnerWhat.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getWho() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getTaskWho(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Invoice_AllU" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listWho.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_ContactID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listWho.add(model);
                            }
                            spinnerWho.setAdapter(adapterWho);
                            setSpinnerDropDownHeight(listWho, spinnerWho);
                            spinnerWho.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getType() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getTaskType(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Invoice_AllU" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listType.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_TaskTypeID));
                                model.setName(dataObject.getString(Cons.KEY_TaskType));
                                listType.add(model);
                            }
                            spinnerType.setAdapter(adapterType);
                            setSpinnerDropDownHeight(listType, spinnerType);
                            spinnerType.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getStatue() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getTaskStatus(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Invoice_AllU" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listStatus.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_TaskStatusID));
                                model.setName(dataObject.getString(Cons.KEY_TaskStatus));
                                listStatus.add(model);
                            }
                            spinnerStatus.setAdapter(adapterStatus);
                            setSpinnerDropDownHeight(listStatus, spinnerStatus);
                            spinnerStatus.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getPriority() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getTaskPriority(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Invoice_AllU" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listPriority.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_TaskPriorityID));
                                model.setName(dataObject.getString(Cons.KEY_Priority));
                                listPriority.add(model);
                            }
                            spinnerPriority.setAdapter(adapterPriority);
                            setSpinnerDropDownHeight(listPriority, spinnerPriority);
                            spinnerPriority.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void createTask(String subject,
                            String assignedTo,
                            String relatedTo,
                            String what,
                            String who,
                            String startDate, String endDate, String dueDate,
                            String description,
                            String email,
                            String phone,
                            String type,
                            String status,
                            String priority,
                            String strRecurring, String startOn,
                            String repeatEvery, String intervalEvery, String repeatOn,
                            String ends, String endsOn, String endsAfter,
                            String recurrenceStartTime, String recurrenceEndTime) {

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_Subject, RequestBody.create(MediaType.parse("text/plain"), subject));
        map.put(Cons.KEY_AssignedTo, RequestBody.create(MediaType.parse("text/plain"), assignedTo));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), relatedTo));
        map.put(Cons.KEY_What, RequestBody.create(MediaType.parse("text/plain"), what));
        map.put(Cons.KEY_Who, RequestBody.create(MediaType.parse("text/plain"), who));
        map.put(Cons.KEY_StartDate, RequestBody.create(MediaType.parse("text/plain"), startDate));
        map.put(Cons.KEY_EndDate, RequestBody.create(MediaType.parse("text/plain"), endDate));
        map.put(Cons.KEY_DueDate, RequestBody.create(MediaType.parse("text/plain"), dueDate));
        if (!TextUtils.isEmpty(description)) {
            map.put(Cons.KEY_Description, RequestBody.create(MediaType.parse("text/plain"), description));
        }
        if (!TextUtils.isEmpty(email)) {
            map.put(Cons.KEY_Email, RequestBody.create(MediaType.parse("text/plain"), email));
        }
        if (!TextUtils.isEmpty(phone)) {
            map.put(Cons.KEY_Phone, RequestBody.create(MediaType.parse("text/plain"), phone));
        }
        map.put(Cons.KEY_TaskType, RequestBody.create(MediaType.parse("text/plain"), type));
        map.put(Cons.KEY_TaskStatus, RequestBody.create(MediaType.parse("text/plain"), status));
        map.put(Cons.KEY_TaskPriority, RequestBody.create(MediaType.parse("text/plain"), priority));

        map.put(Cons.KEY_IsRecurring, RequestBody.create(MediaType.parse("text/plain"), strRecurring));
        map.put(Cons.KEY_StartOn, RequestBody.create(MediaType.parse("text/plain"), startOn));
        map.put(Cons.KEY_RepeatEvery, RequestBody.create(MediaType.parse("text/plain"), repeatEvery));
        map.put(Cons.KEY_RepeatOn, RequestBody.create(MediaType.parse("text/plain"), repeatOn));
        map.put(Cons.KEY_IntervalEvery, RequestBody.create(MediaType.parse("text/plain"), intervalEvery));
        map.put(Cons.KEY_Ends, RequestBody.create(MediaType.parse("text/plain"), ends));
        map.put(Cons.KEY_EndsOnDate, RequestBody.create(MediaType.parse("text/plain"), endsOn));
        map.put(Cons.KEY_EndsAfterOccurrences, RequestBody.create(MediaType.parse("text/plain"), endsAfter));
        map.put(Cons.KEY_StartTime, RequestBody.create(MediaType.parse("text/plain"), recurrenceStartTime));
        map.put(Cons.KEY_EndTime, RequestBody.create(MediaType.parse("text/plain"), recurrenceEndTime));
        Call<ResponseBody> response;
        response = apiInterface.createTaskDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Task_Create" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            String taskID = object.getString(Cons.KEY_TaskID);
                            String subject = object.getString(Cons.KEY_Subject);
                            clickClose.callbackTaskCreateClose(back_type, true, taskID, subject);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_21", "" + e.getMessage());
                    } catch (IOException e) {
                        Log.d("TAG_err_22", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_23", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_24", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });

    }

    @Override
    public void callbackAddAssignedToListen(int pos, ModelSpinner model) {
        phvAssignedToAdd.removeAllViews();
        layoutAssignedToAdd.setVisibility(View.GONE);
        strAssignedToNew = model.getId();
        strAssignedToNameNew = model.getName();
        edittextAssignedTo.setText(strAssignedToNameNew);
    }

}