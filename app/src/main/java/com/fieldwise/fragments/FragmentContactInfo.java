package com.fieldwise.fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fieldwise.R;
import com.fieldwise.adapters.AdapterAccount;
import com.fieldwise.adapters.AdapterAssignedTo;
import com.fieldwise.adapters.AdapterContactAddItem;
import com.fieldwise.adapters.AdapterContactRelatedChild;
import com.fieldwise.adapters.AdapterContactRelatedParent;
import com.fieldwise.adapters.AdapterContactViewAll;
import com.fieldwise.models.ModelEstimate;
import com.fieldwise.models.ModelEvent;
import com.fieldwise.models.ModelFile;
import com.fieldwise.models.ModelInvoice;
import com.fieldwise.models.ModelNote;
import com.fieldwise.models.ModelSpinner;
import com.fieldwise.models.ModelTask;
import com.fieldwise.models.ModelWorkOrder;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;
import com.mindorks.placeholderview.ExpandablePlaceHolderView;
import com.mindorks.placeholderview.PlaceHolderView;
import com.schibstedspain.leku.LocationPickerActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class FragmentContactInfo extends Fragment implements LocationListener,
        AdapterContactViewAll.ViewAllListen,
        AdapterContactAddItem.AddItemListen,
        AdapterContactRelatedChild.ClickListen,
        AdapterAssignedTo.AddAssignedToClickListen,
        AdapterAccount.AddAccountClickListen {

    private static final int PERMISSION_REQUEST_CODE = 200;

    public APIInterface apiInterface;

    private String strLat = "40.730610";
    private String strLng = "-73.935242";
    private String provider;
    private LocationManager locationManager;

    private boolean isAssignedToClicked = false;
    private boolean isAccountClicked = false;
    private boolean isBirthDayClicked = false;
    private boolean isMilClicked = false;

    private RadioGroup radioGroupMain;
    private RadioButton radioButtonDetails, radioButtonRelated;

    private ScrollView layoutRecentContactDetails;
    private ExpandablePlaceHolderView phvRecentContactsRelated;

    private RelativeLayout layoutAssignedTo,
            layoutAccount,
            layoutTitle,
            layoutLeadSource;
    private LinearLayout layoutSalutation, layoutSystemInfo;

    private Spinner spinnerTitle,
            spinnerLeadSource,
            spinnerSalutation;

    private List<ModelSpinner> listAssignedTo,
            listAccount,
            listTitle,
            listLeadSource,
            listSalutation;

    private ArrayAdapter<ModelSpinner> adapterTitle,
            adapterLeadSource,
            adapterSalutation;

    private String contactId, strContactID, strContactName, strContactNo,
            strAssignedToId, strAssignedToName, strAssignedToIdNew, strAssignedToNameNew,
            strAccountId, strAccountIdNew, strAccountName, strAccountNameNew,
            strName, strSalutation, strFirstName, strLastName,
            strTitle,
            strLeadSource, strLeadSourceName,
            strBirthDate, strBirthDateNew,
            strEmail, strPhoneNo, strMobileNo,
            strNotes,
            strIsEmailOptOut, strIsDeleted, strIsDoNotCall, strIsActive,
            strMilLat, strMilLng,
            strMilLatNew, strMilLngNew,
            strMilAddress, strMilCity, strMilState, strMilCountry, strMilPostal,
            strMilAddressNew, strMilCityNew, strMilStateNew, strMilCountryNew, strMilPostalNew,
            strCreatedDate, strCreatedBy, strLastModifiedDate, strLastModifiedBy;

    private EditText edittextAssignedToName, edittextAccountName,
            edittextFirstname, edittextLastname,
            edittextEmail, edittextPhone, edittextMobile, edittextNotes,
            edittextMilAddress,
            edittextMilCity, edittextMilState, edittextMilCountry, edittextMilPostal;

    private TextView textviewHeaderLeadSource, textviewHeaderPhone,
            textviewAssignedToName,
            textviewAccountName,
            textViewName,
            textviewTitle,
            textviewLeadSource,
            textviewEmail, textviewPhone, textviewMobile, textviewNotes,
            textviewBirthDate, edittextBirthDate,
            textviewMilAddress,
            textviewMilCity, textviewMilState, textviewMilCountry, textviewMilPostal,
            textviewCreatedDate, textviewCreatedBy, textviewLastModifiedDate, textviewLastModifiedBy;

    private CheckBox checkboxIsDoNotCall, checkboxIsActive;
    private ImageButton buttonMilAddress;

    private ImageButton buttonAssignedTo;
    private LinearLayout layoutAssignedToAdd;
    private EditText edittextAssignedToAddSearch;
    private PlaceHolderView phvAssignedToAdd;

    private ImageButton buttonAccount;
    private LinearLayout layoutAccountAdd;
    private EditText edittextAccountAddSearch;
    private PlaceHolderView phvAccountAdd;

    private LinearLayout layoutNavigation, layoutSaveCancel;
    private ImageButton buttonCall, buttonComment, buttonDate, buttonEdit, buttonMore;
    private Button buttonSave, buttonCancel;

    private List<String> listRelatedContactList;
    private List<ModelWorkOrder> listRelatedContactWorkOrders;
    private List<ModelEvent> listRelatedContactEvents;
    private List<ModelEstimate> listRelatedContactEstimates;
    private List<ModelInvoice> listRelatedContactInvoices;
    private List<ModelFile> listRelatedContactFiles;
    private List<ModelNote> listRelatedContactNotes;
    private List<ModelTask> listRelatedContactTasks;

    public ContactViewAllListen click;

    public interface ContactViewAllListen {
        void callbackContactViewAllListen(String str, String accountId);
    }

    public ClickListenContactRelatedItemWorkOrderOpen clickContactRelatedItemWorkOrderOpen;

    public interface ClickListenContactRelatedItemWorkOrderOpen {
        void callbackContactRelatedWorkOrderOpen(String contactID, String contactNo);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_contact_detailed, container, false);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        click = (ContactViewAllListen) getActivity();
        clickContactRelatedItemWorkOrderOpen = (ClickListenContactRelatedItemWorkOrderOpen) getActivity();

        radioGroupMain = (RadioGroup) view.findViewById(R.id.radio_group_main);
        radioButtonDetails = (RadioButton) view.findViewById(R.id.radio_button_details);
        radioButtonRelated = (RadioButton) view.findViewById(R.id.radio_button_related);

        layoutRecentContactDetails = (ScrollView) view.findViewById(R.id.layout_recent_contact_details);
        phvRecentContactsRelated = (ExpandablePlaceHolderView) view.findViewById(R.id.phv_recent_contact_related);

        layoutAssignedTo = (RelativeLayout) view.findViewById(R.id.layout_assigned_to);
        layoutAccount = (RelativeLayout) view.findViewById(R.id.layout_account);
        layoutTitle = (RelativeLayout) view.findViewById(R.id.layout_title);
        layoutLeadSource = (RelativeLayout) view.findViewById(R.id.layout_lead_source);
        layoutSalutation = (LinearLayout) view.findViewById(R.id.layout_salutation);

        spinnerTitle = (Spinner) view.findViewById(R.id.spinner_title);
        spinnerLeadSource = (Spinner) view.findViewById(R.id.spinner_lead_source);
        spinnerSalutation = (Spinner) view.findViewById(R.id.spinner_salutation);

        edittextAssignedToName = (EditText) view.findViewById(R.id.edittext_assigned_to_name);
        edittextAccountName = (EditText) view.findViewById(R.id.edittext_account_name);
        edittextFirstname = (EditText) view.findViewById(R.id.edittext_firstname);
        edittextLastname = (EditText) view.findViewById(R.id.edittext_lastname);
        edittextEmail = (EditText) view.findViewById(R.id.edittext_email);
        edittextPhone = (EditText) view.findViewById(R.id.edittext_phone);
        edittextMobile = (EditText) view.findViewById(R.id.edittext_mobile);
        edittextNotes = (EditText) view.findViewById(R.id.edittext_notes);
        edittextMilAddress = (EditText) view.findViewById(R.id.edittext_mil_address);
        edittextMilCity = (EditText) view.findViewById(R.id.edittext_mil_city);
        edittextMilState = (EditText) view.findViewById(R.id.edittext_mil_state);
        edittextMilCountry = (EditText) view.findViewById(R.id.edittext_mil_country);
        edittextMilPostal = (EditText) view.findViewById(R.id.edittext_mil_postal);

        textviewHeaderLeadSource = (TextView) view.findViewById(R.id.textview_header_lead_source);
        textviewHeaderPhone = (TextView) view.findViewById(R.id.textview_header_phone);
        textviewAssignedToName = (TextView) view.findViewById(R.id.textview_assigned_to_name);
        textviewAccountName = (TextView) view.findViewById(R.id.textview_account_name);
        textViewName = (TextView) view.findViewById(R.id.textview_name);
        textviewTitle = (TextView) view.findViewById(R.id.textview_title);
        textviewLeadSource = (TextView) view.findViewById(R.id.textview_lead_source);
        textviewEmail = (TextView) view.findViewById(R.id.textview_email);
        textviewPhone = (TextView) view.findViewById(R.id.textview_phone);
        textviewMobile = (TextView) view.findViewById(R.id.textview_mobile);
        textviewNotes = (TextView) view.findViewById(R.id.textview_notes);
        textviewBirthDate = (TextView) view.findViewById(R.id.textview_birth_date);
        edittextBirthDate = (TextView) view.findViewById(R.id.edittext_birth_date);
        textviewMilAddress = (TextView) view.findViewById(R.id.textview_mil_address);
        textviewMilCity = (TextView) view.findViewById(R.id.textview_mil_city);
        textviewMilState = (TextView) view.findViewById(R.id.textview_mil_state);
        textviewMilCountry = (TextView) view.findViewById(R.id.textview_mil_country);
        textviewMilPostal = (TextView) view.findViewById(R.id.textview_mil_postal);
        textviewCreatedDate = (TextView) view.findViewById(R.id.textview_created_date);
        textviewCreatedBy = (TextView) view.findViewById(R.id.textview_created_by);
        textviewLastModifiedDate = (TextView) view.findViewById(R.id.textview_last_modified_date);
        textviewLastModifiedBy = (TextView) view.findViewById(R.id.textview_last_modified_by);

        checkboxIsDoNotCall = (CheckBox) view.findViewById(R.id.checkbox_is_do_not_call);
        checkboxIsActive = (CheckBox) view.findViewById(R.id.checkbox_is_active);
        buttonMilAddress = (ImageButton) view.findViewById(R.id.button_mil_address);

        layoutSystemInfo = (LinearLayout) view.findViewById(R.id.layout_system_info);
        textviewCreatedDate = (TextView) view.findViewById(R.id.textview_created_date);
        textviewCreatedBy = (TextView) view.findViewById(R.id.textview_created_by);
        textviewLastModifiedDate = (TextView) view.findViewById(R.id.textview_last_modified_date);
        textviewLastModifiedBy = (TextView) view.findViewById(R.id.textview_last_modified_by);

        layoutNavigation = (LinearLayout) view.findViewById(R.id.layout_navigation);
        buttonCall = (ImageButton) view.findViewById(R.id.button_call);
        buttonComment = (ImageButton) view.findViewById(R.id.button_comment);
        buttonDate = (ImageButton) view.findViewById(R.id.button_date);
        buttonEdit = (ImageButton) view.findViewById(R.id.button_edit);
        buttonMore = (ImageButton) view.findViewById(R.id.button_more);

        layoutSaveCancel = (LinearLayout) view.findViewById(R.id.layout_save_cancel);
        buttonSave = (Button) view.findViewById(R.id.button_save);
        buttonCancel = (Button) view.findViewById(R.id.button_cancel);

        buttonAssignedTo = (ImageButton) view.findViewById(R.id.button_assigned_to);
        layoutAssignedToAdd = (LinearLayout) view.findViewById(R.id.layout_assigned_to_add);
        edittextAssignedToAddSearch = (EditText) view.findViewById(R.id.edittext_assigned_to_add_search);
        phvAssignedToAdd = (PlaceHolderView) view.findViewById(R.id.phv_assigned_to_add);

        buttonAccount = (ImageButton) view.findViewById(R.id.button_account);
        layoutAccountAdd = (LinearLayout) view.findViewById(R.id.layout_account_add);
        edittextAccountAddSearch = (EditText) view.findViewById(R.id.edittext_account_add_search);
        phvAccountAdd = (PlaceHolderView) view.findViewById(R.id.phv_account_add);

        strMilLatNew = "0";
        strMilLngNew = "0";
        strMilAddressNew = "";
        strMilCityNew = "";
        strMilStateNew = "";
        strMilCountryNew = "";
        strMilPostalNew = "";

        listAssignedTo = new ArrayList<>();
        listAccount = new ArrayList<>();
        listTitle = new ArrayList<>();
        listLeadSource = new ArrayList<>();
        listSalutation = new ArrayList<>();
        adapterTitle = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listTitle);
        adapterTitle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterLeadSource = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listLeadSource);
        adapterLeadSource.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterSalutation = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listSalutation);
        adapterSalutation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        radioButtonDetails.setChecked(true);
        radioButtonRelated.setChecked(false);
        layoutRecentContactDetails.setVisibility(View.VISIBLE);
        phvRecentContactsRelated.setVisibility(View.GONE);

        editMode(false, false);

        contactId = getArguments().getString(Cons.KEY_ContactID);
        getContactDetailed(contactId);

        getAssignedTo();
        getAccount();
        getTitle();
        getLeadSource();
        getSalutation();

        listRelatedContactList = new ArrayList<>();
        listRelatedContactWorkOrders = new ArrayList<>();
        listRelatedContactEvents = new ArrayList<>();
        listRelatedContactEstimates = new ArrayList<>();
        listRelatedContactInvoices = new ArrayList<>();
        listRelatedContactFiles = new ArrayList<>();
        listRelatedContactNotes = new ArrayList<>();
        listRelatedContactTasks = new ArrayList<>();
        getRelatedContactList(contactId);
        getRelatedContactWorkOrders(contactId);
        getRelatedContactEvents(contactId);
        getRelatedContactEstimates(contactId);
        getRelatedContactInvoices(contactId);
        getRelatedContactFiles(contactId);
        getRelatedContactNotes(contactId);
        getRelatedContactTasks(contactId);

        radioGroupMain.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radio_button_details) {
                    buttonCall.setEnabled(true);
                    buttonComment.setEnabled(true);
                    buttonDate.setEnabled(true);
                    buttonEdit.setEnabled(true);
                    layoutRecentContactDetails.setVisibility(View.VISIBLE);
                    phvRecentContactsRelated.setVisibility(View.GONE);
                } else if (checkedId == R.id.radio_button_related) {
                    buttonCall.setEnabled(false);
                    buttonComment.setEnabled(false);
                    buttonDate.setEnabled(false);
                    buttonEdit.setEnabled(false);
                    layoutRecentContactDetails.setVisibility(View.GONE);
                    phvRecentContactsRelated.setVisibility(View.VISIBLE);
                    phvRecentContactsRelated.removeAllViews();

                    for (int i = 0; i < listRelatedContactList.size(); i++) {
                        phvRecentContactsRelated
                                .addView(new AdapterContactRelatedParent(getActivity(), listRelatedContactList.get(i)));
                    }

                    int lengthWorkOrder;
                    if (listRelatedContactWorkOrders.size() > 3) {
                        lengthWorkOrder = 3;
                    } else {
                        lengthWorkOrder = listRelatedContactWorkOrders.size();
                    }
                    for (int i = 0; i < lengthWorkOrder; i++) {
                        phvRecentContactsRelated
                                .addChildView(0, new AdapterContactRelatedChild(getActivity(), FragmentContactInfo.this,
                                        listRelatedContactWorkOrders.get(i).getWorkOrderNo(), listRelatedContactWorkOrders.get(i).getWorkOrderID(),
                                        "Order: " +
                                                listRelatedContactWorkOrders.get(i).getWorkOrderNo() + "\n" +
                                                "Subject: " +
                                                listRelatedContactWorkOrders.get(i).getSubject() + "\n" +
                                                "Category: " +
                                                listRelatedContactWorkOrders.get(i).getCategoryName() + "\n" +
                                                "Priority: " +
                                                listRelatedContactWorkOrders.get(i).getPriority() + "\n" +
                                                "Status: " +
                                                listRelatedContactWorkOrders.get(i).getStatus()));
                    }
                    if (listRelatedContactWorkOrders.size() > 3) {
                        phvRecentContactsRelated.
                                addChildView(0, new AdapterContactViewAll(getActivity(), FragmentContactInfo.this, getString(R.string.work_order)));
                    } else if (listRelatedContactWorkOrders.size() == 0) {
                        phvRecentContactsRelated.
                                addChildView(0, new AdapterContactAddItem(getActivity(), FragmentContactInfo.this, getString(R.string.work_order)));
                    }

                    int lengthEvents;
                    if (listRelatedContactEvents.size() > 3) {
                        lengthEvents = 3;
                    } else {
                        lengthEvents = listRelatedContactEvents.size();
                    }
                    for (int i = 0; i < lengthEvents; i++) {
                        phvRecentContactsRelated
                                .addChildView(1, new AdapterContactRelatedChild(getActivity(),
                                        "Name: " +
                                                listRelatedContactEvents.get(i).getName() + "\n" +
                                                "Assigned To: " +
                                                listRelatedContactEvents.get(i).getAssignedTo() + "\n" +
                                                "Subject: " +
                                                listRelatedContactEvents.get(i).getSubject() + "\n" +
                                                "Event Start Date: " +
                                                listRelatedContactEvents.get(i).getEventStartDate() + "\n" +
                                                "Event End Date: " +
                                                listRelatedContactEvents.get(i).getEventEndDate() + "\n" +
                                                "Created By: " +
                                                listRelatedContactEvents.get(i).getCreatedBy() + "\n" +
                                                "Created Date: " +
                                                listRelatedContactEvents.get(i).getCreatedDate()));
                    }
                    if (listRelatedContactEvents.size() > 3) {
                        phvRecentContactsRelated.
                                addChildView(1, new AdapterContactViewAll(getActivity(), FragmentContactInfo.this, getString(R.string.event)));
                    } else if (listRelatedContactEvents.size() == 0) {
                        phvRecentContactsRelated.
                                addChildView(1, new AdapterContactAddItem(getActivity(), FragmentContactInfo.this, getString(R.string.event)));
                    }

                    int lengthEstimates;
                    if (listRelatedContactEstimates.size() > 3) {
                        lengthEstimates = 3;
                    } else {
                        lengthEstimates = listRelatedContactEstimates.size();
                    }
                    for (int i = 0; i < lengthEstimates; i++) {
                        phvRecentContactsRelated
                                .addChildView(2, new AdapterContactRelatedChild(getActivity(),
                                        "Owner Name: " +
                                                listRelatedContactEstimates.get(i).getOwnerName() + "\n" +
                                                "ET#: " +
                                                listRelatedContactEstimates.get(i).getEstimateNo() + "\n" +
                                                "Estimate Name: " +
                                                listRelatedContactEstimates.get(i).getEstimateName() + "\n" +
                                                "Status: " +
                                                listRelatedContactEstimates.get(i).getStatus() + "\n" +
                                                "Grand Total: " +
                                                listRelatedContactEstimates.get(i).getGrandTotal() + "\n" +
                                                "Created Date: " +
                                                listRelatedContactEstimates.get(i).getCreatedDate() + "\n" +
                                                "Expiration Date: " +
                                                listRelatedContactEstimates.get(i).getExpirationDate()));
                    }
                    if (listRelatedContactEstimates.size() > 3) {
                        phvRecentContactsRelated.
                                addChildView(2, new AdapterContactViewAll(getActivity(), FragmentContactInfo.this, getString(R.string.estimate)));
                    } else if (listRelatedContactEstimates.size() == 0) {
                        phvRecentContactsRelated.
                                addChildView(2, new AdapterContactAddItem(getActivity(), FragmentContactInfo.this, getString(R.string.estimate)));
                    }

                    int lengthInvoices;
                    if (listRelatedContactInvoices.size() > 3) {
                        lengthInvoices = 3;
                    } else {
                        lengthInvoices = listRelatedContactInvoices.size();
                    }
                    for (int i = 0; i < lengthInvoices; i++) {
                        phvRecentContactsRelated
                                .addChildView(3, new AdapterContactRelatedChild(getActivity(),
                                        "Invoice Number: " +
                                                listRelatedContactInvoices.get(i).getInvoiceNumber() + "\n" +
                                                "Invoice Status: " +
                                                listRelatedContactInvoices.get(i).getInvoiceStatus() + "\n" +
                                                "Subject: " +
                                                listRelatedContactInvoices.get(i).getSubject() + "\n" +
                                                "Sub Total: " +
                                                listRelatedContactInvoices.get(i).getSubTotal() + "\n" +
                                                "Total Price: " +
                                                listRelatedContactInvoices.get(i).getTotalPrice() + "\n" +
                                                "Due Date: " +
                                                listRelatedContactInvoices.get(i).getDueDate()
                                ));
                    }
                    if (listRelatedContactInvoices.size() > 3) {
                        phvRecentContactsRelated.
                                addChildView(3, new AdapterContactViewAll(getActivity(), FragmentContactInfo.this, getString(R.string.invoice)));
                    } else if (listRelatedContactInvoices.size() == 0) {
                        phvRecentContactsRelated.
                                addChildView(3, new AdapterContactAddItem(getActivity(), FragmentContactInfo.this, getString(R.string.invoice)));
                    }

                    int lengthFiles;
                    if (listRelatedContactFiles.size() > 3) {
                        lengthFiles = 3;
                    } else {
                        lengthFiles = listRelatedContactFiles.size();
                    }
                    for (int i = 0; i < lengthFiles; i++) {
                        phvRecentContactsRelated
                                .addChildView(4, new AdapterContactRelatedChild(getActivity(),
                                        "File Name: " +
                                                listRelatedContactFiles.get(i).getFileName() + "\n" +
                                                "Subject: " +
                                                listRelatedContactFiles.get(i).getSubject() + "\n" +
                                                "Content Type: " +
                                                listRelatedContactFiles.get(i).getContentType() + "\n" +
                                                "Created By: " +
                                                listRelatedContactFiles.get(i).getCreatedBy() + "\n" +
                                                "Created Date: " +
                                                listRelatedContactFiles.get(i).getCreatedDate()
                                ));
                    }
                    if (listRelatedContactFiles.size() > 3) {
                        phvRecentContactsRelated.
                                addChildView(4, new AdapterContactViewAll(getActivity(), FragmentContactInfo.this, getString(R.string.file)));
                    } else if (listRelatedContactFiles.size() == 0) {
                        phvRecentContactsRelated.
                                addChildView(4, new AdapterContactAddItem(getActivity(), FragmentContactInfo.this, getString(R.string.file)));
                    }

                    int lengthNotes;
                    if (listRelatedContactNotes.size() > 3) {
                        lengthNotes = 3;
                    } else {
                        lengthNotes = listRelatedContactNotes.size();
                    }
                    for (int i = 0; i < lengthNotes; i++) {
                        phvRecentContactsRelated
                                .addChildView(5, new AdapterContactRelatedChild(getActivity(),
                                        "Subject: " +
                                                listRelatedContactNotes.get(i).getSubject() + "\n" +
                                                "Created Date: " +
                                                listRelatedContactNotes.get(i).getCreatedDate() + "\n" +
                                                "Owner: " +
                                                listRelatedContactNotes.get(i).getOwnerName()
                                ));
                    }
                    if (listRelatedContactNotes.size() > 3) {
                        phvRecentContactsRelated.
                                addChildView(5, new AdapterContactViewAll(getActivity(), FragmentContactInfo.this, getString(R.string.note)));
                    } else if (listRelatedContactNotes.size() == 0) {
                        phvRecentContactsRelated.
                                addChildView(5, new AdapterContactAddItem(getActivity(), FragmentContactInfo.this, getString(R.string.note)));
                    }

                    int lengthTasks;
                    if (listRelatedContactTasks.size() > 3) {
                        lengthTasks = 3;
                    } else {
                        lengthTasks = listRelatedContactTasks.size();
                    }
                    for (int i = 0; i < lengthTasks; i++) {
                        phvRecentContactsRelated
                                .addChildView(6, new AdapterContactRelatedChild(getActivity(),
                                        "Name: " +
                                                listRelatedContactTasks.get(i).getName() + "\n" +
                                                "Assigned To: " +
                                                listRelatedContactTasks.get(i).getAssignedTo() + "\n" +
                                                "Subject: " +
                                                listRelatedContactTasks.get(i).getSubject() + "\n" +
                                                "Task Type: " +
                                                listRelatedContactTasks.get(i).getTaskType() + "\n" +
                                                "Task Status: " +
                                                listRelatedContactTasks.get(i).getTaskStatus() + "\n" +
                                                "Priority: " +
                                                listRelatedContactTasks.get(i).getPriority() + "\n" +
                                                "Date: " +
                                                listRelatedContactTasks.get(i).getDate()
                                ));
                    }
                    if (listRelatedContactTasks.size() > 3) {
                        phvRecentContactsRelated.
                                addChildView(6, new AdapterContactViewAll(getActivity(), FragmentContactInfo.this, getString(R.string.task)));
                    } else if (listRelatedContactTasks.size() == 0) {
                        phvRecentContactsRelated.
                                addChildView(6, new AdapterContactAddItem(getActivity(), FragmentContactInfo.this, getString(R.string.task)));
                    }
                }
            }
        });

        edittextBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] strSplit = strBirthDate.split("/");
                int day = Integer.valueOf(strSplit[1]);
                int month = Integer.valueOf(strSplit[0]) - 1;
                int year = Integer.valueOf(strSplit[2]);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                        isBirthDayClicked = true;
                        edittextBirthDate.setText(selectDate(d, m, y));
                        strBirthDateNew = selectDateEdit(d, m, y);
                        Log.d("TAG_BirthDate", strBirthDateNew);
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        buttonMilAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!checkPermission()) {
                        requestPermission();
                    } else {
                        isMilClicked = true;
                        double lat = Double.valueOf(strLat);
                        double lng = Double.valueOf(strLng);
                        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                                .withLocation(lat, lng)
                                .withGeolocApiKey(getString(R.string.google_maps_key))
                                .withGooglePlacesEnabled()
                                .withSatelliteViewHidden()
                                .withVoiceSearchHidden()
                                .shouldReturnOkOnBackPressed()
                                .build(getActivity());
                        startActivityForResult(locationPickerIntent, 99);
                    }
                } else {
                    isMilClicked = true;
                    double lat = Double.valueOf(strLat);
                    double lng = Double.valueOf(strLng);
                    Intent locationPickerIntent = new LocationPickerActivity.Builder()
                            .withLocation(lat, lng)
                            .withGeolocApiKey(getString(R.string.google_maps_key))
                            .withGooglePlacesEnabled()
                            .withSatelliteViewHidden()
                            .withVoiceSearchHidden()
                            .shouldReturnOkOnBackPressed()
                            .build(getActivity());
                    startActivityForResult(locationPickerIntent, 99);
                }
            }
        });

        buttonAssignedTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutAssignedToAdd.setVisibility(View.VISIBLE);
                listAssignedTo.clear();
                getAssignedTo();
            }
        });

        edittextAssignedToAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvAssignedToAdd.removeAllViews();
                    ArrayList<ModelSpinner> modle = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listAssignedTo) {
                        if (m.getName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvAssignedToAdd
                                .addView(new AdapterAssignedTo(getActivity(), FragmentContactInfo.this, i, modle.get(i)));
                    }
                } else {
                    phvAssignedToAdd.removeAllViews();
                    for (int i = 0; i < listAssignedTo.size(); i++) {
                        phvAssignedToAdd
                                .addView(new AdapterAssignedTo(getActivity(), FragmentContactInfo.this, i, listAssignedTo.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        buttonAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutAccountAdd.setVisibility(View.VISIBLE);
                listAccount.clear();
                getAccount();
            }
        });

        edittextAccountAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvAccountAdd.removeAllViews();
                    ArrayList<ModelSpinner> modle = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listAccount) {
                        if (m.getName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvAccountAdd
                                .addView(new AdapterAccount(getActivity(), FragmentContactInfo.this, i, modle.get(i)));
                    }
                } else {
                    phvAccountAdd.removeAllViews();
                    for (int i = 0; i < listAccount.size(); i++) {
                        phvAccountAdd
                                .addView(new AdapterAccount(getActivity(), FragmentContactInfo.this, i, listAccount.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonDetails.setEnabled(false);
                radioButtonRelated.setEnabled(false);
                editMode(true, true);
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String assignedTo = strAssignedToId;
                if (isAssignedToClicked) {
                    assignedTo = strAssignedToIdNew;
                }
                String account = strAccountId;
                if (isAccountClicked) {
                    account = strAccountIdNew;
                }
                String salutation = listSalutation.get(spinnerSalutation.getSelectedItemPosition()).getName();
                String firstName = edittextFirstname.getText().toString().trim();
                String lastName = edittextLastname.getText().toString().trim();
                String title = listTitle.get(spinnerTitle.getSelectedItemPosition()).getName();
                String leadSource = listLeadSource.get(spinnerLeadSource.getSelectedItemPosition()).getId();
                String birthDate = strBirthDate;
                if (isBirthDayClicked) {
                    birthDate = strBirthDateNew;
                }
                String email = edittextEmail.getText().toString().trim();
                String phone = edittextPhone.getText().toString().trim();
                String mobile = edittextMobile.getText().toString().trim();
                String notes = edittextNotes.getText().toString().trim();
                String isDoNotCall = null;
                if (checkboxIsDoNotCall.isChecked()) {
                    isDoNotCall = "1";
                } else {
                    isDoNotCall = "0";
                }
                String isActive = null;
                if (checkboxIsActive.isChecked()) {
                    isActive = "1";
                } else {
                    isActive = "0";
                }
                String milLat = strMilLat;
                String milLng = strMilLng;
                if (isMilClicked) {
                    milLat = strMilLatNew;
                    milLng = strMilLngNew;
                }
                String milAddress = edittextMilAddress.getText().toString().trim();
                String milCity = edittextMilCity.getText().toString().trim();
                String milState = edittextMilState.getText().toString().trim();
                String milCountry = edittextMilCountry.getText().toString().trim();
                String milPostal = edittextMilPostal.getText().toString().trim();

                if (TextUtils.isEmpty(assignedTo)) {
                    Utl.showToast(getActivity(), "Select AssignedTo");
                } else if (TextUtils.isEmpty(account)) {
                    Utl.showToast(getActivity(), "Select Account");
                } else if (TextUtils.isEmpty(firstName)) {
                    Utl.showToast(getActivity(), "Enter First Name");
                } else if (TextUtils.isEmpty(lastName)) {
                    Utl.showToast(getActivity(), "Enter Last Name");
                } else if (TextUtils.isEmpty(milAddress)) {
                    Utl.showToast(getActivity(), "Enter Mailing Address");
                } else if (TextUtils.isEmpty(milCity)) {
                    Utl.showToast(getActivity(), "Enter Mailing City");
                } else if (TextUtils.isEmpty(milState)) {
                    Utl.showToast(getActivity(), "Enter Mailing State");
                } else if (TextUtils.isEmpty(milCountry)) {
                    Utl.showToast(getActivity(), "Enter Mailing Country");
                } else if (TextUtils.isEmpty(milPostal)) {
                    Utl.showToast(getActivity(), "Enter Mailing Postal Code");
                } else {
                    editContact(contactId,
                            assignedTo,
                            account,
                            salutation,
                            firstName,
                            lastName,
                            title,
                            leadSource,
                            birthDate,
                            email,
                            phone,
                            mobile,
                            notes,
                            isDoNotCall,
                            isActive,
                            milLat, milLng,
                            milAddress, milCity, milState, milCountry, milPostal);
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editMode(false, true);
                radioButtonDetails.setEnabled(true);
                radioButtonRelated.setEnabled(true);
            }
        });

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(provider);
        if (location != null) {
            Log.d("Provider: ", provider + " has been selected.");
            onLocationChanged(location);
        }

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99) {
            if (data != null) {
                double latitude = data.getDoubleExtra("latitude", 0.0);
                strMilLat = String.valueOf(latitude);
                double longitude = data.getDoubleExtra("longitude", 0.0);
                strMilLng = String.valueOf(longitude);
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> addresses = new ArrayList<>();
                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                strMilAddress = addresses.get(0).getSubThoroughfare() + ", " + addresses.get(0).getThoroughfare();
                if (addresses.get(0).getLocality() != null) {
                    strMilCity = addresses.get(0).getLocality();
                } else {
                    strMilCity = addresses.get(0).getSubLocality();
                }
                if (addresses.get(0).getAdminArea() != null) {
                    strMilState = addresses.get(0).getAdminArea();
                } else {
                    strMilState = addresses.get(0).getSubAdminArea();
                }
                strMilCountry = addresses.get(0).getCountryName();
                strMilPostal = addresses.get(0).getPostalCode();
                Log.d("TAG_LATITUDE****", strMilLat);
                Log.d("TAG_LONGITUDE****", strMilLng);
                Log.d("TAG_ADDRESS****", strMilAddress);
                Log.d("TAG_CITY****", strMilCity);
                Log.d("TAG_STATE****", strMilState);
                Log.d("TAG_COUNTRY****", strMilCountry);
                Log.d("TAG_POSTAL****", strMilPostal);
                edittextMilAddress.setText(strMilAddress);
                edittextMilCity.setText(strMilCity);
                edittextMilState.setText(strMilState);
                edittextMilCountry.setText(strMilCountry);
                edittextMilPostal.setText(strMilPostal);
            } else {
                Log.d("RESULT", "CANCELLED");
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        strLat = String.valueOf(location.getLatitude());
        strLng = String.valueOf(location.getLongitude());
        Log.d("TAG_LAT*****", strLat);
        Log.d("TAG_LNG*****", strLng);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
        Toast.makeText(getActivity(), "Enabled New Provider " + provider, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String s) {
        Toast.makeText(getActivity(), "Disabled New Provider " + provider, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    private void editMode(boolean bolEdit, boolean bolResponse) {
        if (!bolEdit) {
            textviewHeaderLeadSource.setVisibility(View.VISIBLE);
            textviewHeaderPhone.setVisibility(View.VISIBLE);

            textviewAssignedToName.setVisibility(View.VISIBLE);
            layoutAssignedTo.setVisibility(View.GONE);
            textviewAccountName.setVisibility(View.VISIBLE);
            layoutAccount.setVisibility(View.GONE);
            textViewName.setVisibility(View.VISIBLE);
            layoutSalutation.setVisibility(View.GONE);
            textviewTitle.setVisibility(View.VISIBLE);
            layoutTitle.setVisibility(View.GONE);
            spinnerTitle.setAdapter(null);
            textviewLeadSource.setVisibility(View.VISIBLE);
            layoutLeadSource.setVisibility(View.GONE);
            spinnerLeadSource.setAdapter(null);
            textviewEmail.setVisibility(View.VISIBLE);
            edittextEmail.setVisibility(View.GONE);
            textviewPhone.setVisibility(View.VISIBLE);
            edittextPhone.setVisibility(View.GONE);
            textviewMobile.setVisibility(View.VISIBLE);
            edittextMobile.setVisibility(View.GONE);
            textviewBirthDate.setVisibility(View.VISIBLE);
            edittextBirthDate.setVisibility(View.GONE);
            textviewNotes.setVisibility(View.VISIBLE);
            edittextNotes.setVisibility(View.GONE);

            checkboxIsDoNotCall.setEnabled(false);
            checkboxIsActive.setEnabled(false);

            textviewMilAddress.setVisibility(View.VISIBLE);
            edittextMilAddress.setVisibility(View.GONE);
            textviewMilCity.setVisibility(View.GONE);
            edittextMilCity.setVisibility(View.GONE);
            textviewMilState.setVisibility(View.GONE);
            edittextMilState.setVisibility(View.GONE);
            textviewMilCountry.setVisibility(View.GONE);
            edittextMilCountry.setVisibility(View.GONE);
            textviewMilPostal.setVisibility(View.GONE);
            edittextMilPostal.setVisibility(View.GONE);
            buttonMilAddress.setVisibility(View.GONE);

            layoutSaveCancel.setVisibility(View.GONE);
            layoutNavigation.setVisibility(View.VISIBLE);
            buttonCall.setEnabled(true);
            buttonComment.setEnabled(true);
            buttonDate.setEnabled(true);
            buttonEdit.setEnabled(true);

            layoutSystemInfo.setVisibility(View.VISIBLE);
            if (bolResponse) {
                setViewInfo();
            }
        } else {
            textviewHeaderLeadSource.setVisibility(View.GONE);
            textviewHeaderPhone.setVisibility(View.GONE);

            textviewAssignedToName.setVisibility(View.GONE);
            layoutAssignedTo.setVisibility(View.VISIBLE);
            textviewAccountName.setVisibility(View.GONE);
            layoutAccount.setVisibility(View.VISIBLE);
            textViewName.setVisibility(View.GONE);
            layoutSalutation.setVisibility(View.VISIBLE);
            spinnerSalutation.setAdapter(adapterSalutation);
            textviewTitle.setVisibility(View.GONE);
            layoutTitle.setVisibility(View.VISIBLE);
            spinnerTitle.setAdapter(adapterTitle);
            textviewLeadSource.setVisibility(View.GONE);
            layoutLeadSource.setVisibility(View.VISIBLE);
            spinnerLeadSource.setAdapter(adapterLeadSource);
            textviewEmail.setVisibility(View.GONE);
            edittextEmail.setVisibility(View.VISIBLE);
            textviewPhone.setVisibility(View.GONE);
            edittextPhone.setVisibility(View.VISIBLE);
            textviewMobile.setVisibility(View.GONE);
            edittextMobile.setVisibility(View.VISIBLE);
            textviewBirthDate.setVisibility(View.GONE);
            edittextBirthDate.setVisibility(View.VISIBLE);
            textviewNotes.setVisibility(View.GONE);
            edittextNotes.setVisibility(View.VISIBLE);

            checkboxIsDoNotCall.setEnabled(true);
            checkboxIsActive.setEnabled(true);

            textviewMilAddress.setVisibility(View.GONE);
            edittextMilAddress.setVisibility(View.VISIBLE);
            textviewMilCity.setVisibility(View.VISIBLE);
            edittextMilCity.setVisibility(View.VISIBLE);
            textviewMilState.setVisibility(View.VISIBLE);
            edittextMilState.setVisibility(View.VISIBLE);
            textviewMilCountry.setVisibility(View.VISIBLE);
            edittextMilCountry.setVisibility(View.VISIBLE);
            textviewMilPostal.setVisibility(View.VISIBLE);
            edittextMilPostal.setVisibility(View.VISIBLE);
            buttonMilAddress.setVisibility(View.VISIBLE);

            layoutSaveCancel.setVisibility(View.VISIBLE);
            layoutNavigation.setVisibility(View.GONE);
            buttonCall.setEnabled(false);
            buttonComment.setEnabled(false);
            buttonDate.setEnabled(false);
            buttonEdit.setEnabled(false);

            layoutSystemInfo.setVisibility(View.GONE);
            if (bolResponse) {
                setEditInfo();
            }
        }
    }

    private void setViewInfo() {
        textviewHeaderLeadSource.setText("Lead Source: " + strLeadSourceName);
        textviewHeaderPhone.setText("Phone: " + strPhoneNo);

        textviewAssignedToName.setText(strAssignedToName);
        textviewAccountName.setText(strAccountName);
        textViewName.setText(strSalutation + " " + strFirstName + " " + strLastName);
        textviewTitle.setText(strTitle);
        textviewLeadSource.setText(strLeadSourceName);
        textviewBirthDate.setText(formatDate(strBirthDate));
        textviewEmail.setText(strEmail);
        textviewPhone.setText(strPhoneNo);
        textviewMobile.setText(strMobileNo);
        textviewNotes.setText(strNotes);
        if (strIsDoNotCall.equals("1")) {
            checkboxIsDoNotCall.setChecked(true);
        } else {
            checkboxIsDoNotCall.setChecked(false);
        }
        if (strIsActive.equals("1")) {
            checkboxIsActive.setChecked(true);
        } else {
            checkboxIsActive.setChecked(false);
        }
        textviewMilAddress.setText(strMilAddress + "\n" +
                strMilCity + ", " +
                strMilState + ", " +
                strMilPostal + "\n" +
                strMilCountry);
        textviewCreatedDate.setText(strCreatedDate);
        textviewCreatedBy.setText(strCreatedBy);
        textviewLastModifiedDate.setText(strLastModifiedDate);
        textviewLastModifiedBy.setText(strLastModifiedBy);
    }

    private void setEditInfo() {
        edittextAssignedToName.setText(strAssignedToName);
        edittextAccountName.setText(strAccountName);
        setSpinnerDropDownHeight(listSalutation, spinnerSalutation);
        spinnerSalutation.setSelection(getListIndex(listSalutation, strSalutation));
        setSpinnerDropDownHeight(listTitle, spinnerTitle);
        edittextFirstname.setText(strFirstName);
        edittextLastname.setText(strLastName);
        spinnerTitle.setSelection(getListIndex(listTitle, strTitle));
        setSpinnerDropDownHeight(listLeadSource, spinnerLeadSource);
        spinnerLeadSource.setSelection(getListIndex(listLeadSource, strLeadSource));
        edittextBirthDate.setText(formatDate(strBirthDate));
        edittextEmail.setText(strEmail);
        edittextPhone.setText(strPhoneNo);
        edittextMobile.setText(strMobileNo);
        edittextNotes.setText(strNotes);
        if (strIsDoNotCall.equals("1")) {
            checkboxIsDoNotCall.setChecked(true);
        } else {
            checkboxIsDoNotCall.setChecked(false);
        }
        if (strIsActive.equals("1")) {
            checkboxIsActive.setChecked(true);
        } else {
            checkboxIsActive.setChecked(false);
        }
        edittextMilAddress.setText(strMilAddress);
        edittextMilCity.setText(strMilCity);
        edittextMilState.setText(strMilState);
        edittextMilCountry.setText(strMilCountry);
        edittextMilPostal.setText(strMilPostal);
    }

    private void setSpinnerDropDownHeight(List<ModelSpinner> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 70);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    public int getListIndex(List<ModelSpinner> list, String name) {
        for (int i = 0; i < list.size(); i++) {
            ModelSpinner model = list.get(i);
            if (name.equals(model.getName())) {
                return i;
            }
        }
        return -1;
    }

    public String formatDate(String time) {
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String selectDate(int d, int m, int y) {
        String time = "" + (m + 1) + "/" + d + "/" + y;
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String selectDateEdit(int d, int m, int y) {
        String time = "" + (m + 1) + "/" + d + "/" + y;
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void getContactDetailed(String contactId) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_ContactID, RequestBody.create(MediaType.parse("text/plain"), contactId));
        Call<ResponseBody> response;
        response = apiInterface.getContactDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_ContactInfo" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            JSONObject dataObject = object.getJSONObject("data");
                            strContactID = dataObject.getString(Cons.KEY_ContactID);
                            strContactName = dataObject.getString(Cons.KEY_ContactName);
                            strContactNo = dataObject.getString(Cons.KEY_ContactNo);
                            strAssignedToId = dataObject.getString(Cons.KEY_AssignedTo);
                            strAssignedToName = dataObject.getString(Cons.KEY_AssignedToName);
                            strAccountId = dataObject.getString(Cons.KEY_Account);
                            strAccountName = dataObject.getString(Cons.KEY_AccountName);
                            strSalutation = dataObject.getString(Cons.KEY_Salutation);
                            strName = dataObject.getString(Cons.KEY_Name);
                            strFirstName = dataObject.getString(Cons.KEY_FirstName);
                            strLastName = dataObject.getString(Cons.KEY_LastName);
                            strTitle = dataObject.getString(Cons.KEY_Title);
                            strLeadSource = dataObject.getString(Cons.KEY_LeadSource);
                            strLeadSourceName = dataObject.getString(Cons.KEY_LeadSourceName);
                            strBirthDate = dataObject.getString(Cons.KEY_BirthDate);
                            strEmail = dataObject.getString(Cons.KEY_Email);
                            strIsEmailOptOut = dataObject.getString(Cons.KEY_EmailOptOut);
                            strPhoneNo = dataObject.getString(Cons.KEY_PhoneNo);
                            strMobileNo = dataObject.getString(Cons.KEY_MobileNo);
                            strNotes = dataObject.getString(Cons.KEY_Notes);
                            strIsDeleted = dataObject.getString(Cons.KEY_IsDeleted);
                            strIsDoNotCall = dataObject.getString(Cons.KEY_DoNotCall);
                            strIsActive = dataObject.getString(Cons.KEY_IsActive);
                            strMilAddress = dataObject.getString(Cons.KEY_MailingAddress);
                            strMilCity = dataObject.getString(Cons.KEY_MailingCity);
                            strMilState = dataObject.getString(Cons.KEY_MailingState);
                            strMilCountry = dataObject.getString(Cons.KEY_MailingCountry);
                            strMilPostal = dataObject.getString(Cons.KEY_MailingPostalCode);
                            strMilLat = dataObject.getString(Cons.KEY_MailingLatitude);
                            strMilLng = dataObject.getString(Cons.KEY_MailingLongitude);
                            strCreatedDate = dataObject.getString(Cons.KEY_CreatedDate);
                            strCreatedBy = dataObject.getString(Cons.KEY_CreatedBy);
                            strLastModifiedDate = dataObject.getString(Cons.KEY_LastModifiedDate);
                            strLastModifiedBy = dataObject.getString(Cons.KEY_LastModifiedBy);
                            editMode(false, true);
                            radioButtonDetails.setEnabled(true);
                            radioButtonRelated.setEnabled(true);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void editContact(final String contactId,
                             String assignedTo,
                             String account,
                             String salutation,
                             String firstName,
                             String lastName,
                             String title,
                             String leadSource,
                             String birthDate,
                             String email,
                             String phone,
                             String mobile,
                             String notes,
                             String isDoNotCall,
                             String isActive,
                             String milLat,
                             String milLng,
                             String milAddress,
                             String milCity,
                             String milState,
                             String milCountry,
                             String milPostal) {

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_ContactID, RequestBody.create(MediaType.parse("text/plain"), contactId));
        map.put(Cons.KEY_AssignedTo, RequestBody.create(MediaType.parse("text/plain"), assignedTo));
        map.put(Cons.KEY_Account, RequestBody.create(MediaType.parse("text/plain"), account));
        map.put(Cons.KEY_Salutation, RequestBody.create(MediaType.parse("text/plain"), salutation));
        map.put(Cons.KEY_FirstName, RequestBody.create(MediaType.parse("text/plain"), firstName));
        map.put(Cons.KEY_LastName, RequestBody.create(MediaType.parse("text/plain"), lastName));
        map.put(Cons.KEY_Title, RequestBody.create(MediaType.parse("text/plain"), title));
        map.put(Cons.KEY_LeadSource, RequestBody.create(MediaType.parse("text/plain"), leadSource));
        if (!TextUtils.isEmpty(birthDate)) {
            map.put(Cons.KEY_BirthDate, RequestBody.create(MediaType.parse("text/plain"), birthDate));
        }
        if (!TextUtils.isEmpty(email)) {
            map.put(Cons.KEY_Email, RequestBody.create(MediaType.parse("text/plain"), email));
        }
        if (!TextUtils.isEmpty(phone)) {
            map.put(Cons.KEY_PhoneNo, RequestBody.create(MediaType.parse("text/plain"), phone));
        }
        if (!TextUtils.isEmpty(mobile)) {
            map.put(Cons.KEY_MobileNo, RequestBody.create(MediaType.parse("text/plain"), mobile));
        }
        if (!TextUtils.isEmpty(notes)) {
            map.put(Cons.KEY_Notes, RequestBody.create(MediaType.parse("text/plain"), notes));
        }
        map.put(Cons.KEY_DoNotCall, RequestBody.create(MediaType.parse("text/plain"), isDoNotCall));
        map.put(Cons.KEY_IsActive, RequestBody.create(MediaType.parse("text/plain"), isActive));
        map.put(Cons.KEY_MailingLatitude, RequestBody.create(MediaType.parse("text/plain"), milLat));
        map.put(Cons.KEY_MailingLongitude, RequestBody.create(MediaType.parse("text/plain"), milLng));
        map.put(Cons.KEY_MailingAddress, RequestBody.create(MediaType.parse("text/plain"), milAddress));
        map.put(Cons.KEY_MailingCity, RequestBody.create(MediaType.parse("text/plain"), milCity));
        map.put(Cons.KEY_MailingState, RequestBody.create(MediaType.parse("text/plain"), milState));
        map.put(Cons.KEY_MailingCountry, RequestBody.create(MediaType.parse("text/plain"), milCountry));
        map.put(Cons.KEY_MailingPostalCode, RequestBody.create(MediaType.parse("text/plain"), milPostal));
        Call<ResponseBody> response;
        response = apiInterface.editContactDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Contact_Create" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            getContactDetailed(contactId);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getAssignedTo() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getContactAssignedTo(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_AllUser" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAssignedTo.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_UserID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listAssignedTo.add(model);
                            }
                            for (int i = 0; i < listAssignedTo.size(); i++) {
                                phvAssignedToAdd
                                        .addView(new AdapterAssignedTo(getActivity(), FragmentContactInfo.this, i, listAssignedTo.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getAccount() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getContactAccount(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Account" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAccount.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_AccountID));
                                model.setName(dataObject.getString(Cons.KEY_AccountName));
                                listAccount.add(model);
                            }
                            for (int i = 0; i < listAccount.size(); i++) {
                                phvAccountAdd
                                        .addView(new AdapterAccount(getActivity(), FragmentContactInfo.this, i, listAccount.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getTitle() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getContactTitle(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_ContactTitle" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listTitle.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_TitleOfPeopleID));
                                model.setName(dataObject.getString(Cons.KEY_Title));
                                listTitle.add(model);
                            }
                            setSpinnerDropDownHeight(listTitle, spinnerTitle);
                            spinnerTitle.setAdapter(adapterTitle);
                            spinnerTitle.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getLeadSource() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getContactLeadSource(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_ContactLeadSour" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listLeadSource.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_LeadSourceID));
                                model.setName(dataObject.getString(Cons.KEY_LeadSource));
                                listLeadSource.add(model);
                            }
                            setSpinnerDropDownHeight(listLeadSource, spinnerLeadSource);
                            spinnerLeadSource.setAdapter(adapterLeadSource);
                            spinnerLeadSource.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getSalutation() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getContactSalutation(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_ContactSalutati" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAssignedTo.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_SalutationID));
                                model.setName(dataObject.getString(Cons.KEY_Salutation));
                                listSalutation.add(model);
                            }
                            setSpinnerDropDownHeight(listSalutation, spinnerSalutation);
                            spinnerSalutation.setAdapter(adapterSalutation);
                            spinnerSalutation.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedContactList(String contactId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_ContactID, RequestBody.create(MediaType.parse("text/plain"), contactId));
        Call<ResponseBody> response = apiInterface.getRelatedContactList(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONObject dataObject = object.getJSONObject("data");
                        JSONObject WorkOrderObject = dataObject.getJSONObject("WorkOrder");
                        JSONObject EventsObject = dataObject.getJSONObject("Event");
                        JSONObject EstimatesObject = dataObject.getJSONObject("Estimate");
                        JSONObject InvoicesObject = dataObject.getJSONObject("Invoice");
                        JSONObject FilesObject = dataObject.getJSONObject("File");
                        JSONObject NoteObject = dataObject.getJSONObject("Note");
                        JSONObject TasksObject = dataObject.getJSONObject("Task");
                        listRelatedContactList.add(WorkOrderObject.getString("title"));
                        listRelatedContactList.add(EventsObject.getString("title"));
                        listRelatedContactList.add(EstimatesObject.getString("title"));
                        listRelatedContactList.add(InvoicesObject.getString("title"));
                        listRelatedContactList.add(FilesObject.getString("title"));
                        listRelatedContactList.add(NoteObject.getString("title"));
                        listRelatedContactList.add(TasksObject.getString("title"));
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedContactWorkOrders(String ContactId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_ContactID, RequestBody.create(MediaType.parse("text/plain"), ContactId));
        Call<ResponseBody> response = apiInterface.getRelatedContactWorkOrders(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelWorkOrder model = new ModelWorkOrder();
                            model.setWorkOrderID(dataObject.getString(Cons.KEY_WorkOrderID));
                            model.setWorkOrderNo(dataObject.getString(Cons.KEY_WorkOrderNo));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            model.setPriority(dataObject.getString(Cons.KEY_Priority));
                            model.setStatus(dataObject.getString(Cons.KEY_Status));
                            model.setCategoryName(dataObject.getString(Cons.KEY_CategoryName));
                            listRelatedContactWorkOrders.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedContactEvents(String ContactId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_ContactID, RequestBody.create(MediaType.parse("text/plain"), ContactId));
        Call<ResponseBody> response = apiInterface.getRelatedContactEvents(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelEvent model = new ModelEvent();
                            model.setAssignedTo(dataObject.getString(Cons.KEY_AssignedTo));
                            model.setCreatedBy(dataObject.getString(Cons.KEY_CreatedBy));
                            model.setCreatedDate(dataObject.getString(Cons.KEY_CreatedDate));
                            model.setEventEndDate(dataObject.getString(Cons.KEY_EventEndDate));
                            model.setEventStartDate(dataObject.getString(Cons.KEY_EventStartDate));
                            model.setName(dataObject.getString(Cons.KEY_Name));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            listRelatedContactEvents.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedContactEstimates(String ContactId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_ContactID, RequestBody.create(MediaType.parse("text/plain"), ContactId));
        Call<ResponseBody> response = apiInterface.getRelatedContactEstimates(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelEstimate model = new ModelEstimate();
                            model.setCreatedDate(dataObject.getString(Cons.KEY_CreatedDate));
                            //model.setEstimateID(dataObject.getString(Cons.KEY_EstimateID));
                            model.setEstimateNo(dataObject.getString(Cons.KEY_EstimateNo));
                            model.setEstimateName(dataObject.getString(Cons.KEY_EstimateName));
                            model.setExpirationDate(dataObject.getString(Cons.KEY_ExpirationDate));
                            model.setGrandTotal(dataObject.getString(Cons.KEY_GrandTotal));
                            model.setOwnerName(dataObject.getString(Cons.KEY_OwnerName));
                            model.setStatus(dataObject.getString(Cons.KEY_Status));
                            listRelatedContactEstimates.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedContactInvoices(String ContactId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_ContactID, RequestBody.create(MediaType.parse("text/plain"), ContactId));
        Call<ResponseBody> response = apiInterface.getRelatedContactInvoices(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelInvoice model = new ModelInvoice();
                            model.setDueDate(dataObject.getString(Cons.KEY_DueDate));
                            model.setInvoiceNumber(dataObject.getString(Cons.KEY_InvoiceNumber));
                            model.setInvoiceStatus(dataObject.getString(Cons.KEY_InvoiceStatus));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            model.setSubTotal(dataObject.getString(Cons.KEY_SubTotal));
                            model.setTotalPrice(dataObject.getString(Cons.KEY_TotalPrice));
                            listRelatedContactInvoices.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedContactFiles(String ContactId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_ContactID, RequestBody.create(MediaType.parse("text/plain"), ContactId));
        Call<ResponseBody> response = apiInterface.getRelatedContactFiles(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelFile model = new ModelFile();
                            model.setContentType(dataObject.getString(Cons.KEY_ContentType));
                            model.setCreatedBy(dataObject.getString(Cons.KEY_CreatedBy));
                            model.setCreatedDate(dataObject.getString(Cons.KEY_CreatedDate));
                            model.setFileName(dataObject.getString(Cons.KEY_FileName));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            listRelatedContactFiles.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedContactNotes(String ContactId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_ContactID, RequestBody.create(MediaType.parse("text/plain"), ContactId));
        Call<ResponseBody> response = apiInterface.getRelatedContactNote(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelNote model = new ModelNote();
                            model.setNoteID(dataObject.getString(Cons.KEY_NoteID));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            model.setCreatedDate(dataObject.getString(Cons.KEY_CreatedDate));
                            model.setOwnerName(dataObject.getString(Cons.KEY_OwnerName));
                            listRelatedContactNotes.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedContactTasks(String ContactId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_ContactID, RequestBody.create(MediaType.parse("text/plain"), ContactId));
        Call<ResponseBody> response = apiInterface.getRelatedContactTasks(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelTask model = new ModelTask();
                            model.setAssignedTo(dataObject.getString(Cons.KEY_AssignedTo));
                            model.setDate(dataObject.getString(Cons.KEY_Date));
                            model.setName(dataObject.getString(Cons.KEY_Name));
                            model.setPriority(dataObject.getString(Cons.KEY_Priority));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            model.setTaskType(dataObject.getString(Cons.KEY_TaskType));
                            model.setTaskStatus(dataObject.getString(Cons.KEY_TaskStatus));
                            listRelatedContactTasks.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    public boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), CAMERA);
        int result2 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION);
        int result3 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_COARSE_LOCATION);
        int result4 = ContextCompat.checkSelfPermission(getActivity(), CALL_PHONE);
        int result5 = ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE);
        int result6 = ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE);
        return
                result1 == PackageManager.PERMISSION_GRANTED
                        && result2 == PackageManager.PERMISSION_GRANTED
                        && result3 == PackageManager.PERMISSION_GRANTED
                        && result4 == PackageManager.PERMISSION_GRANTED
                        && result5 == PackageManager.PERMISSION_GRANTED
                        && result6 == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{
                        CAMERA,
                        ACCESS_FINE_LOCATION,
                        ACCESS_COARSE_LOCATION,
                        CALL_PHONE,
                        READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean locationFineAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean locationCoarseAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean callAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean readAccepted = grantResults[4] == PackageManager.PERMISSION_GRANTED;
                    boolean writeAccepted = grantResults[5] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted &&
                            locationFineAccepted &&
                            locationCoarseAccepted &&
                            callAccepted &&
                            readAccepted &&
                            writeAccepted)
                        Utl.showToast(getActivity(), "Permission Granted.");
                    else {
                        Utl.showToast(getActivity(), "Permission Denied.");
                    }
                }
                break;
        }
    }

    @Override
    public void callbackAddAssignedToListen(int pos, ModelSpinner model) {
        isAssignedToClicked = true;
        phvAssignedToAdd.removeAllViews();
        layoutAssignedToAdd.setVisibility(View.GONE);
        strAssignedToIdNew = model.getId();
        strAssignedToNameNew = model.getName();
        edittextAssignedToName.setText(strAssignedToNameNew);
    }

    @Override
    public void callbackAddAccountListen(int pos, ModelSpinner model) {
        isAccountClicked = true;
        phvAccountAdd.removeAllViews();
        layoutAccountAdd.setVisibility(View.GONE);
        strAccountIdNew = model.getId();
        strAccountNameNew = model.getName();
        edittextAccountName.setText(strAccountNameNew);
    }

    @Override
    public void callbackContactRelatedWorkOrderOpen(String workOrderNo, String workOrderID) {
        if (clickContactRelatedItemWorkOrderOpen != null) {
            clickContactRelatedItemWorkOrderOpen.callbackContactRelatedWorkOrderOpen(workOrderID, workOrderNo);
        }
    }

    @Override
    public void callbackViewAllListen(String str) {
        if (click != null) {
            click.callbackContactViewAllListen(str, contactId);
        }
    }

    @Override
    public void callbackAddItemListen(String str) {

    }

}