package com.fieldwise.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.ListPopupWindow;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.fieldwise.R;
import com.fieldwise.activities.ActivityMain;
import com.fieldwise.adapters.AdapterInvoiceAddItem;
import com.fieldwise.adapters.AdapterInvoiceProductChild;
import com.fieldwise.adapters.AdapterInvoiceProductParent;
import com.fieldwise.adapters.AdapterInvoiceRelatedChild;
import com.fieldwise.adapters.AdapterInvoiceRelatedParent;
import com.fieldwise.adapters.AdapterInvoiceViewAll;
import com.fieldwise.adapters.AdapterOwner;
import com.fieldwise.adapters.AdapterAccount;
import com.fieldwise.models.ModelInvoiceEvent;
import com.fieldwise.models.ModelInvoiceFile;
import com.fieldwise.models.ModelInvoiceLineItem;
import com.fieldwise.models.ModelInvoiceNote;
import com.fieldwise.models.ModelInvoiceTask;
import com.fieldwise.models.ModelProduct;
import com.fieldwise.models.ModelSpinner;
import com.fieldwise.utils.APIClient;
import com.fieldwise.utils.APIInterface;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Singleton;
import com.fieldwise.utils.Utl;
import com.mindorks.placeholderview.ExpandablePlaceHolderView;
import com.mindorks.placeholderview.PlaceHolderView;
import com.schibstedspain.leku.LocationPickerActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class FragmentInvoiceInfo extends Fragment implements
        AdapterAccount.AddAccountClickListen,
        AdapterOwner.AddOwnerClickListen,
        AdapterInvoiceViewAll.InvoiceViewAllListen,
        AdapterInvoiceAddItem.InvoiceAddItemListen,
        AdapterInvoiceProductParent.AddProductInvoiceCheckBoxListen,
        AdapterInvoiceRelatedChild.ClickListenNote {

    private static final int PERMISSION_REQUEST_CODE = 200;

    private RadioGroup radioGroupMain;
    private RadioButton radioButtonDetails, radioButtonRelated;

    private ScrollView layoutRecentInvoicesDetails;
    private ExpandablePlaceHolderView phvRecentInvoicesRelated;

    public APIInterface apiInterface;

    private String invoiceId;

    private String strInvoiceID, strInvoiceNo,
            strOwner, strOwnerNew, strOwnerName, strOwnerNameNew,
            strAccount, strAccountNew, strAccountName, strAccountNameNew,
            strWorkOrder, strWorkOrderName,
            strContact, strContactName,
            strDescription,
            strAdditionalInfo,
            strStatus, strStatusName,
            strInvoiceDate, strInvoiceDateNew,
            strPaymentTerms, strPaymentTermsName,
            strDueDate, strDueDateNew,
            strBilLat, strBilLng,
            strBilAddress, strBilCity, strBilState, strBilCountry, strBilPostal,
            strNewBilLat, strNewBilLng,
            strNewBilAddress, strNewBilCity, strNewBilState, strNewBilCountry, strNewBilPostal,
            strSubTotal,
            strDiscount,
            strTax,
            strTotalPrice,
            strGrandTotal,
            strCreatedDate, strLastModifiedDate, strCreatedBy, strLastModifiedBy,
            strPhoneNo;

    private boolean isCloneMode = false;
    private boolean isOwnerClicked = false;
    private boolean isAccountClicked = false;
    private boolean isInvoiceDateClicked = false;
    private boolean isDueDateClicked = false;
    private boolean isBilClicked = false;

    private RelativeLayout layoutOwner,
            layoutAccount,
            layoutWorkOrder,
            layoutContact,
            layoutStatus,
            layoutPaymentTerms;

    private LinearLayout layoutHeader, layoutInvoiceNo, layoutFinancialInfo, layoutSystemInfo;

    private Spinner spinnerWorkOrder, spinnerContact,
            spinnerStatus, spinnerPaymentTerms;

    private List<ModelSpinner> listOwners, listAccounts,
            listWorkOrder, listContacts,
            listStatus, listPaymentTerms;

    private ArrayAdapter<ModelSpinner> adapterWorkOrder, adapterContact,
            adapterStatus, adapterPaymentTerms;

    private EditText edittextOwner,
            edittextAccount,
            edittextDescription,
            edittextAdditionalInfo,
            edittextBilAddress,
            edittextBilCity,
            edittextBilState,
            edittextBilCountry,
            edittextBilPostal;

    private TextView textviewHeaderStatus,
            textviewInvoiceNo,
            textviewOwner,
            textviewAccount,
            textviewWorkOrder,
            textviewContact,
            textviewDescription,
            textviewAdditionalInfo,
            textviewStatus,
            textviewPaymentTerms,
            edittextInvoiceDate, textviewInvoiceDate,
            edittextDueDate, textviewDueDate,
            textviewBilAddress,
            textviewBilCity,
            textviewBilState,
            textviewBilCountry,
            textviewBilPostal,
            textviewSubTotal,
            textviewDiscount,
            textviewTax,
            textviewTotalPrice,
            textviewGrandTotal,
            textviewCreatedDate,
            textviewCreatedBy,
            textviewLastModifiedDate,
            textviewLastModifiedBy;

    private ImageButton buttonBilAddress;

    private LinearLayout layoutNavigation, layoutSaveCancel;
    private ImageButton buttonCall, buttonComment, buttonDate, buttonEdit, buttonMore;
    private Button buttonSave, buttonCancel;

    private ImageButton buttonOwner;
    private LinearLayout layoutOwnerAdd;
    private EditText edittextOwnerAddSearch;
    private PlaceHolderView phvOwnerAdd;

    private ImageButton buttonAccount;
    private LinearLayout layoutAccountAdd;
    private EditText edittextAccountAddSearch;
    private PlaceHolderView phvAccountAdd;

    private LinearLayout layoutProductAdd, layoutProductEdit, layoutProductEditChange;
    private RelativeLayout layoutProductFilter;
    private EditText edittextProductAddSearch, edittextProductFilterCode;
    private Button buttonProductAddMenu, buttonProductEditMenu, buttonProductFilterAdd, buttonProductFilterCancel, buttonProductFilterApply, buttonProductFilterClear;

    private ExpandablePlaceHolderView phvProductAdd;
    private List<ModelProduct> listProduct;
    private List<String> listProductEditChange;

    private RelativeLayout layoutNewNote;
    private EditText edittextNewNoteSubject, edittextNewNoteBody;
    private Button buttonNewNoteSave, buttonNewNoteCancel;

    private List<ModelSpinner> listProductFilterFamily;
    private ArrayAdapter<ModelSpinner> adapterProductFilterFamily;
    private Spinner spinnerProductFilterFamily;
    private TextView textviewProductEditGrandTotal;

    private List<String> listRelatedInvoiceList;
    private List<ModelInvoiceEvent> listRelatedInvoiceEvents;
    private List<ModelInvoiceFile> listRelatedInvoiceFiles;
    private List<ModelInvoiceLineItem> listRelatedInvoiceLineItems;
    private List<ModelInvoiceNote> listRelatedInvoiceNotes;
    private List<ModelInvoiceTask> listRelatedInvoiceTasks;

    private RelativeLayout layoutWebView;
    private WebView webView;
    private Button buttonWebViewClose;

    public InvoiceViewAllListen click;

    public interface InvoiceViewAllListen {
        void callbackInvoiceViewAllListen(String str, String invoiceId);
    }

    public CreateFileListener clickCrateFile;

    public interface CreateFileListener {
        void callbackInvoiceCreateFile(String strObject);
    }

    public CreateEventListener clickCrateEvent;

    public interface CreateEventListener {
        void callbackInvoiceCreateEvent(String strWhoName, String strWhatID, String strWhatName);
    }

    public SendEmailListener clickSendEmail;

    public interface SendEmailListener {
        void callbackInvoiceSendEmail(String what, String header);
    }

    public GoToRecent clickGoToRecent;

    public interface GoToRecent {
        void callbackGoToRecentInvoice(String strObject);
    }

    //Create Interface
    public ClickListenTaskCreate clickTaskCreate;

    public interface ClickListenTaskCreate {
        void callbackInvoiceTaskCreate();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invoice_detailed, container, false);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        //click = (InvoiceViewAllListen) getActivity();
        clickCrateFile = (CreateFileListener) getActivity();
        clickCrateEvent = (CreateEventListener) getActivity();
        clickSendEmail = (SendEmailListener) getActivity();
        clickGoToRecent = (GoToRecent) getActivity();

        clickTaskCreate = (ClickListenTaskCreate) getActivity();

        textviewHeaderStatus = (TextView) view.findViewById(R.id.textview_header_status);

        radioGroupMain = (RadioGroup) view.findViewById(R.id.radio_group_main);
        radioButtonDetails = (RadioButton) view.findViewById(R.id.radio_button_details);
        radioButtonRelated = (RadioButton) view.findViewById(R.id.radio_button_related);

        layoutRecentInvoicesDetails = (ScrollView) view.findViewById(R.id.layout_recent_invoice_details);
        phvRecentInvoicesRelated = (ExpandablePlaceHolderView) view.findViewById(R.id.phv_recent_invoice_related);

        layoutHeader = (LinearLayout) view.findViewById(R.id.layout_header);
        layoutInvoiceNo = (LinearLayout) view.findViewById(R.id.layout_invoice_no);
        layoutOwner = (RelativeLayout) view.findViewById(R.id.layout_owner);
        layoutAccount = (RelativeLayout) view.findViewById(R.id.layout_account);
        layoutWorkOrder = (RelativeLayout) view.findViewById(R.id.layout_work_order);
        layoutContact = (RelativeLayout) view.findViewById(R.id.layout_contact);
        layoutStatus = (RelativeLayout) view.findViewById(R.id.layout_status);
        layoutPaymentTerms = (RelativeLayout) view.findViewById(R.id.layout_payment_terms);
        layoutFinancialInfo = (LinearLayout) view.findViewById(R.id.layout_financial_info);
        layoutSystemInfo = (LinearLayout) view.findViewById(R.id.layout_system_info);

        spinnerWorkOrder = (Spinner) view.findViewById(R.id.spinner_work_order);
        spinnerContact = (Spinner) view.findViewById(R.id.spinner_contact);
        spinnerStatus = (Spinner) view.findViewById(R.id.spinner_status);
        spinnerPaymentTerms = (Spinner) view.findViewById(R.id.spinner_payment_terms);

        edittextOwner = (EditText) view.findViewById(R.id.edittext_owner);
        edittextAccount = (EditText) view.findViewById(R.id.edittext_account);
        edittextDescription = (EditText) view.findViewById(R.id.edittext_description);
        edittextAdditionalInfo = (EditText) view.findViewById(R.id.edittext_additional_info);

        edittextBilAddress = (EditText) view.findViewById(R.id.edittext_bil_address);
        edittextBilCity = (EditText) view.findViewById(R.id.edittext_bil_city);
        edittextBilState = (EditText) view.findViewById(R.id.edittext_bil_state);
        edittextBilCountry = (EditText) view.findViewById(R.id.edittext_bil_country);
        edittextBilPostal = (EditText) view.findViewById(R.id.edittext_bil_postal);

        textviewInvoiceNo = (TextView) view.findViewById(R.id.textview_invoice_no);
        textviewOwner = (TextView) view.findViewById(R.id.textview_owner);
        textviewAccount = (TextView) view.findViewById(R.id.textview_account);
        textviewWorkOrder = (TextView) view.findViewById(R.id.textview_work_order);
        textviewContact = (TextView) view.findViewById(R.id.textview_contact);
        textviewDescription = (TextView) view.findViewById(R.id.textview_description);
        textviewAdditionalInfo = (TextView) view.findViewById(R.id.textview_additional_info);
        textviewStatus = (TextView) view.findViewById(R.id.textview_status);
        textviewPaymentTerms = (TextView) view.findViewById(R.id.textview_payment_terms);
        edittextInvoiceDate = (TextView) view.findViewById(R.id.edittext_invoice_date);
        textviewInvoiceDate = (TextView) view.findViewById(R.id.textview_invoice_date);
        edittextDueDate = (TextView) view.findViewById(R.id.edittext_due_date);
        textviewDueDate = (TextView) view.findViewById(R.id.textview_due_date);
        textviewBilAddress = (TextView) view.findViewById(R.id.textview_bil_address);
        textviewBilCity = (TextView) view.findViewById(R.id.textview_bil_city);
        textviewBilState = (TextView) view.findViewById(R.id.textview_bil_state);
        textviewBilCountry = (TextView) view.findViewById(R.id.textview_bil_country);
        textviewBilPostal = (TextView) view.findViewById(R.id.textview_bil_postal);
        textviewSubTotal = (TextView) view.findViewById(R.id.textview_sub_total);
        textviewDiscount = (TextView) view.findViewById(R.id.textview_discount);
        textviewTax = (TextView) view.findViewById(R.id.textview_tax);
        textviewTotalPrice = (TextView) view.findViewById(R.id.textview_total_price);
        textviewGrandTotal = (TextView) view.findViewById(R.id.textview_grand_total);
        textviewCreatedDate = (TextView) view.findViewById(R.id.textview_created_date);
        textviewCreatedBy = (TextView) view.findViewById(R.id.textview_created_by);
        textviewLastModifiedDate = (TextView) view.findViewById(R.id.textview_last_modified_date);
        textviewLastModifiedBy = (TextView) view.findViewById(R.id.textview_last_modified_by);

        buttonBilAddress = (ImageButton) view.findViewById(R.id.button_bil_address);

        layoutNavigation = (LinearLayout) view.findViewById(R.id.layout_navigation);
        buttonCall = (ImageButton) view.findViewById(R.id.button_call);
        buttonComment = (ImageButton) view.findViewById(R.id.button_comment);
        buttonDate = (ImageButton) view.findViewById(R.id.button_date);
        buttonEdit = (ImageButton) view.findViewById(R.id.button_edit);
        buttonMore = (ImageButton) view.findViewById(R.id.button_more);
        layoutSaveCancel = (LinearLayout) view.findViewById(R.id.layout_save_cancel);
        buttonSave = (Button) view.findViewById(R.id.button_save);
        buttonCancel = (Button) view.findViewById(R.id.button_cancel);

        buttonOwner = (ImageButton) view.findViewById(R.id.button_owner);
        layoutOwnerAdd = (LinearLayout) view.findViewById(R.id.layout_owner_add);
        edittextOwnerAddSearch = (EditText) view.findViewById(R.id.edittext_owner_add_search);
        phvOwnerAdd = (PlaceHolderView) view.findViewById(R.id.phv_owner_add);

        buttonAccount = (ImageButton) view.findViewById(R.id.button_account);
        layoutAccountAdd = (LinearLayout) view.findViewById(R.id.layout_account_add);
        edittextAccountAddSearch = (EditText) view.findViewById(R.id.edittext_account_add_search);
        phvAccountAdd = (PlaceHolderView) view.findViewById(R.id.phv_account_add);

        layoutProductAdd = (LinearLayout) view.findViewById(R.id.layout_product_add);
        edittextProductAddSearch = (EditText) view.findViewById(R.id.edittext_product_add_search);
        buttonProductFilterAdd = (Button) view.findViewById(R.id.button_product_add_filter);
        buttonProductAddMenu = (Button) view.findViewById(R.id.button_product_add_menu);
        phvProductAdd = (ExpandablePlaceHolderView) view.findViewById(R.id.phv_product_add);

        layoutProductEdit = (LinearLayout) view.findViewById(R.id.layout_product_edit);
        buttonProductEditMenu = (Button) view.findViewById(R.id.button_product_edit_menu);
        layoutProductEditChange = (LinearLayout) view.findViewById(R.id.layout_product_edit_change);

        layoutProductFilter = (RelativeLayout) view.findViewById(R.id.layout_product_filter);
        spinnerProductFilterFamily = (Spinner) view.findViewById(R.id.spinner_product_filter_family);
        edittextProductFilterCode = (EditText) view.findViewById(R.id.edittext_product_filter_code);
        buttonProductFilterCancel = (Button) view.findViewById(R.id.button_product_filter_cancel);
        buttonProductFilterApply = (Button) view.findViewById(R.id.button_product_filter_apply);
        buttonProductFilterClear = (Button) view.findViewById(R.id.button_product_filter_clear);

        textviewProductEditGrandTotal = (TextView) view.findViewById(R.id.textview_product_edit_grand_total);

        layoutNewNote = (RelativeLayout) view.findViewById(R.id.layout_new_note);
        edittextNewNoteSubject = (EditText) view.findViewById(R.id.edittext_new_note_subject);
        edittextNewNoteBody = (EditText) view.findViewById(R.id.edittext_new_note_body);
        buttonNewNoteSave = (Button) view.findViewById(R.id.button_new_note_save);
        buttonNewNoteCancel = (Button) view.findViewById(R.id.button_new_note_cancel);

        layoutWebView = (RelativeLayout) view.findViewById(R.id.layout_webview);
        webView = (WebView) view.findViewById(R.id.webview);
        buttonWebViewClose = (Button) view.findViewById(R.id.button_webview_close);
        webView.getSettings().setJavaScriptEnabled(true);
        buttonWebViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutWebView.setVisibility(View.GONE);
            }
        });

        strNewBilLat = "0.00";
        strNewBilLng = "0.00";
        strNewBilAddress = "";
        strNewBilCity = "";
        strNewBilState = "";
        strNewBilCountry = "";
        strNewBilPostal = "";

        listOwners = new ArrayList<>();
        listAccounts = new ArrayList<>();
        listWorkOrder = new ArrayList<>();
        listContacts = new ArrayList<>();
        listStatus = new ArrayList<>();
        listPaymentTerms = new ArrayList<>();
        adapterWorkOrder = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listWorkOrder);
        adapterWorkOrder.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterContact = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listContacts);
        adapterContact.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterStatus = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listStatus);
        adapterStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterPaymentTerms = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listPaymentTerms);
        adapterPaymentTerms.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Singleton.getInstance().listInvoiceProduct.clear();
        listProduct = new ArrayList<>();
        listProductEditChange = new ArrayList<>();

        radioButtonDetails.setChecked(true);
        radioButtonRelated.setChecked(false);
        layoutRecentInvoicesDetails.setVisibility(View.VISIBLE);
        phvRecentInvoicesRelated.setVisibility(View.GONE);

        editMode(false, false);

        invoiceId = getArguments().getString(Cons.KEY_InvoiceID);

        getInvoiceDetailed(invoiceId);

        getOwners();
        getAccounts();
        getWorkOrders();
        getContacts();
        getStatus();
        getPaymentTerms();

        listRelatedInvoiceList = new ArrayList<>();
        listRelatedInvoiceEvents = new ArrayList<>();
        listRelatedInvoiceFiles = new ArrayList<>();
        listRelatedInvoiceLineItems = new ArrayList<>();
        listRelatedInvoiceNotes = new ArrayList<>();
        listRelatedInvoiceTasks = new ArrayList<>();

        getRelatedInvoiceList(invoiceId);
        getRelatedInvoiceEvents(invoiceId);
        getRelatedInvoiceFiles(invoiceId);
        getRelatedInvoiceLineItems(invoiceId);
        getRelatedInvoiceTasks(invoiceId);
        getRelatedInvoiceNotes(invoiceId);
        getRelatedInvoiceTasks(invoiceId);

        radioGroupMain.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radio_button_details) {
                    buttonCall.setEnabled(true);
                    buttonComment.setEnabled(true);
                    buttonDate.setEnabled(true);
                    buttonEdit.setEnabled(true);
                    layoutRecentInvoicesDetails.setVisibility(View.VISIBLE);
                    phvRecentInvoicesRelated.setVisibility(View.GONE);
                } else if (checkedId == R.id.radio_button_related) {
                    buttonCall.setEnabled(false);
                    buttonComment.setEnabled(false);
                    buttonDate.setEnabled(false);
                    buttonEdit.setEnabled(false);
                    layoutRecentInvoicesDetails.setVisibility(View.GONE);
                    phvRecentInvoicesRelated.setVisibility(View.VISIBLE);
                    phvRecentInvoicesRelated.removeAllViews();

                    for (int i = 0; i < listRelatedInvoiceList.size(); i++) {
                        phvRecentInvoicesRelated
                                .addView(new AdapterInvoiceRelatedParent(getActivity(), listRelatedInvoiceList.get(i)));
                    }

                    int lengthLineItem;
                    if (listRelatedInvoiceLineItems.size() > 3) {
                        lengthLineItem = 3;
                    } else {
                        lengthLineItem = listRelatedInvoiceLineItems.size();
                    }
                    for (int i = 0; i < lengthLineItem; i++) {
                        phvRecentInvoicesRelated
                                .addChildView(0, new AdapterInvoiceRelatedChild(getActivity(),
                                        "IN Line#: " + listRelatedInvoiceLineItems.get(i).getInvoiceLineNo() + "\n" +
                                                "Product: " +
                                                listRelatedInvoiceLineItems.get(i).getProductName() + "\n" +
                                                "Unit Price: $" +
                                                listRelatedInvoiceLineItems.get(i).getUnitPrice() + "\n" +
                                                "Quantity: " +
                                                listRelatedInvoiceLineItems.get(i).getQuantity() + "\n" +
                                                "Net Total: $" +
                                                listRelatedInvoiceLineItems.get(i).getSubTotal()
                                ));
                    }
                    if (listRelatedInvoiceLineItems.size() > 3) {
                        phvRecentInvoicesRelated.
                                addChildView(0, new AdapterInvoiceViewAll(getActivity(), FragmentInvoiceInfo.this, getString(R.string.line_item)));
                    } else if (listRelatedInvoiceLineItems.size() == 0) {
                        phvRecentInvoicesRelated.
                                addChildView(0, new AdapterInvoiceAddItem(getActivity(), FragmentInvoiceInfo.this, getString(R.string.line_item)));
                    }

                    int lengthEvent;
                    if (listRelatedInvoiceEvents.size() > 3) {
                        lengthEvent = 3;
                    } else {
                        lengthEvent = listRelatedInvoiceEvents.size();
                    }
                    for (int i = 0; i < lengthEvent; i++) {
                        phvRecentInvoicesRelated
                                .addChildView(1, new AdapterInvoiceRelatedChild(getActivity(), FragmentInvoiceInfo.this, getString(R.string.event), listRelatedInvoiceEvents.get(i).getEventID(),
                                        "Subject: " +
                                                listRelatedInvoiceEvents.get(i).getSubject() + "\n" +
                                                "Status: " +
                                                listRelatedInvoiceEvents.get(i).getEventStatus() + "\n" +
                                                "Type: " +
                                                listRelatedInvoiceEvents.get(i).getEventTypeName() + "\n" +
                                                "Start: " +
                                                listRelatedInvoiceEvents.get(i).getEventStartDate() + " " + listRelatedInvoiceEvents.get(i).getEventStartTime() + "\n" +
                                                "End: " +
                                                listRelatedInvoiceEvents.get(i).getEventEndDate() + " " + listRelatedInvoiceEvents.get(i).getEventEndTime()
                                ));
                    }
                    if (listRelatedInvoiceEvents.size() > 3) {
                        phvRecentInvoicesRelated.
                                addChildView(1, new AdapterInvoiceViewAll(getActivity(), FragmentInvoiceInfo.this, getString(R.string.event)));
                    } else if (listRelatedInvoiceEvents.size() == 0) {
                        phvRecentInvoicesRelated.
                                addChildView(1, new AdapterInvoiceAddItem(getActivity(), FragmentInvoiceInfo.this, getString(R.string.events)));
                    }

                    int lengthFile;
                    if (listRelatedInvoiceFiles.size() > 3) {
                        lengthFile = 3;
                    } else {
                        lengthFile = listRelatedInvoiceFiles.size();
                    }
                    for (int i = 0; i < lengthFile; i++) {
                        phvRecentInvoicesRelated
                                .addChildView(2, new AdapterInvoiceRelatedChild(getActivity(),
                                        "Name: " +
                                                listRelatedInvoiceFiles.get(i).getFileName() + "\n" +
                                                "Subject: " +
                                                listRelatedInvoiceFiles.get(i).getSubject()
                                ));
                    }
                    if (listRelatedInvoiceFiles.size() > 3) {
                        phvRecentInvoicesRelated.
                                addChildView(2, new AdapterInvoiceViewAll(getActivity(), FragmentInvoiceInfo.this, getString(R.string.note)));
                    } else if (listRelatedInvoiceFiles.size() == 0) {
                        phvRecentInvoicesRelated.
                                addChildView(2, new AdapterInvoiceAddItem(getActivity(), FragmentInvoiceInfo.this, getString(R.string.note)));
                    }

                    int lengthTask;
                    if (listRelatedInvoiceTasks.size() > 3) {
                        lengthTask = 3;
                    } else {
                        lengthTask = listRelatedInvoiceTasks.size();
                    }
                    for (int i = 0; i < lengthTask; i++) {
                        phvRecentInvoicesRelated
                                .addChildView(3, new AdapterInvoiceRelatedChild(getActivity(),
                                        "Subject: " +
                                                listRelatedInvoiceTasks.get(i).getSubject() + "\n" +
                                                "Type: " +
                                                listRelatedInvoiceTasks.get(i).getTaskType() + "\n" +
                                                "Status: " +
                                                listRelatedInvoiceTasks.get(i).getTaskStatus() + "\n" +
                                                "Priority: " +
                                                listRelatedInvoiceTasks.get(i).getPriority()/* + "\n" +
                                                "CallDisposition: " +
                                                listRelatedInvoiceTasks.get(i).getCallDisposition()*/
                                ));
                    }
                    if (listRelatedInvoiceTasks.size() > 3) {
                        phvRecentInvoicesRelated.
                                addChildView(3, new AdapterInvoiceViewAll(getActivity(), FragmentInvoiceInfo.this, getString(R.string.file)));
                    } else if (listRelatedInvoiceTasks.size() == 0) {
                        phvRecentInvoicesRelated.
                                addChildView(3, new AdapterInvoiceAddItem(getActivity(), FragmentInvoiceInfo.this, getString(R.string.file)));
                    }

                    int lengthNote;
                    if (listRelatedInvoiceNotes.size() > 3) {
                        lengthNote = 3;
                    } else {
                        lengthNote = listRelatedInvoiceNotes.size();
                    }
                    for (int i = 0; i < lengthNote; i++) {
                        phvRecentInvoicesRelated
                                .addChildView(4, new AdapterInvoiceRelatedChild(getActivity(), FragmentInvoiceInfo.this, getString(R.string.note), listRelatedInvoiceNotes.get(i).getNoteID(),
                                        "Subject: " +
                                                listRelatedInvoiceNotes.get(i).getSubject() + "\n" +
                                                "Created Date: " +
                                                listRelatedInvoiceNotes.get(i).getCreatedDate() + "\n" +
                                                "Owner: " +
                                                listRelatedInvoiceNotes.get(i).getOwnerName()
                                ));
                    }
                    if (listRelatedInvoiceNotes.size() > 3) {
                        phvRecentInvoicesRelated.
                                addChildView(4, new AdapterInvoiceViewAll(getActivity(), FragmentInvoiceInfo.this, getString(R.string.note)));
                    } else if (listRelatedInvoiceNotes.size() == 0) {
                        phvRecentInvoicesRelated.
                                addChildView(4, new AdapterInvoiceAddItem(getActivity(), FragmentInvoiceInfo.this, getString(R.string.note)));
                    }

                }
            }
        });

        buttonAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isAccountClicked = true;
                layoutAccountAdd.setVisibility(View.VISIBLE);
                listAccounts.clear();
                getAccounts();
            }
        });

        buttonOwner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isOwnerClicked = true;
                layoutOwnerAdd.setVisibility(View.VISIBLE);
                listOwners.clear();
                getOwners();
            }
        });

        edittextOwnerAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvOwnerAdd.removeAllViews();
                    ArrayList<ModelSpinner> modle = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listOwners) {
                        if (m.getName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvOwnerAdd
                                .addView(new AdapterOwner(getActivity(), FragmentInvoiceInfo.this, i, modle.get(i)));
                    }
                } else {
                    phvOwnerAdd.removeAllViews();
                    for (int i = 0; i < listOwners.size(); i++) {
                        phvOwnerAdd
                                .addView(new AdapterOwner(getActivity(), FragmentInvoiceInfo.this, i, listOwners.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        edittextAccountAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvAccountAdd.removeAllViews();
                    ArrayList<ModelSpinner> modle = new ArrayList<ModelSpinner>();
                    for (ModelSpinner m : listAccounts) {
                        if (m.getName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvAccountAdd
                                .addView(new AdapterAccount(getActivity(), FragmentInvoiceInfo.this, i, modle.get(i)));
                    }
                } else {
                    phvAccountAdd.removeAllViews();
                    for (int i = 0; i < listAccounts.size(); i++) {
                        phvAccountAdd
                                .addView(new AdapterAccount(getActivity(), FragmentInvoiceInfo.this, i, listAccounts.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        edittextInvoiceDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                Date date = null;
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                if (isInvoiceDateClicked) {
                    try {
                        date = sdf.parse(strInvoiceDateNew);
                    } catch (ParseException ex) {
                        Log.v("Exception", ex.getLocalizedMessage());
                    }
                } else {
                    try {
                        date = sdf.parse(strInvoiceDate);
                    } catch (ParseException ex) {
                        Log.v("Exception", ex.getLocalizedMessage());
                    }
                }
                calendar.setTime(date);
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                final int hour = calendar.get(Calendar.HOUR);
                final int minute = calendar.get(Calendar.MINUTE);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        isInvoiceDateClicked = true;
                        strInvoiceDateNew = setDate(d, m, y);
                        edittextInvoiceDate.setText(formatDate(strInvoiceDateNew));
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        edittextDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                Date date = null;
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                if (isDueDateClicked) {
                    try {
                        date = sdf.parse(strDueDateNew);
                    } catch (ParseException ex) {
                        Log.v("Exception", ex.getLocalizedMessage());
                    }
                } else {
                    try {
                        date = sdf.parse(strDueDate);
                    } catch (ParseException ex) {
                        Log.v("Exception", ex.getLocalizedMessage());
                    }
                }
                calendar.setTime(date);
                int day = calendar.get(Calendar.DATE);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                final int hour = calendar.get(Calendar.HOUR);
                final int minute = calendar.get(Calendar.MINUTE);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int y, final int m, final int d) {
                        isDueDateClicked = true;
                        strDueDateNew = setDate(d, m, y);
                        edittextDueDate.setText(formatDate(strDueDateNew));
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        buttonBilAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!checkPermission()) {
                        requestPermission();
                    } else {
                        isBilClicked = true;
                        double lat = Double.valueOf(strBilLat);
                        double lng = Double.valueOf(strBilLng);
                        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                                .withLocation(lat, lng)
                                .withGeolocApiKey(getString(R.string.google_maps_key))
                                .withGooglePlacesEnabled()
                                .withSatelliteViewHidden()
                                .withVoiceSearchHidden()
                                .shouldReturnOkOnBackPressed()
                                .build(getActivity());
                        startActivityForResult(locationPickerIntent, 99);
                    }
                } else {
                    isBilClicked = true;
                    double lat = Double.valueOf(strBilLat);
                    double lng = Double.valueOf(strBilLng);
                    Intent locationPickerIntent = new LocationPickerActivity.Builder()
                            .withLocation(lat, lng)
                            .withGeolocApiKey(getString(R.string.google_maps_key))
                            .withGooglePlacesEnabled()
                            .withSatelliteViewHidden()
                            .withVoiceSearchHidden()
                            .shouldReturnOkOnBackPressed()
                            .build(getActivity());
                    startActivityForResult(locationPickerIntent, 99);
                }
            }
        });

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonDetails.setEnabled(false);
                radioButtonRelated.setEnabled(false);
                editMode(true, true);
            }
        });

        buttonMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String owner = "";
                if (isOwnerClicked) {
                    owner = strOwnerNew;
                } else {
                    owner = strOwner;
                }
                String account = "";
                if (isAccountClicked) {
                    account = strAccountNew;
                } else {
                    account = strAccount;
                }
                String workOrder = listWorkOrder.get(spinnerWorkOrder.getSelectedItemPosition()).getId();
                String contact = listContacts.get(spinnerContact.getSelectedItemPosition()).getId();
                String description = edittextDescription.getText().toString().trim();
                String additionalInfo = edittextAdditionalInfo.getText().toString().trim();
                String status = listStatus.get(spinnerStatus.getSelectedItemPosition()).getId();
                String invoiceDate = "";
                if (isInvoiceDateClicked) {
                    invoiceDate = strInvoiceDateNew;
                } else {
                    invoiceDate = strInvoiceDate;
                }
                String paymentTerms = listPaymentTerms.get(spinnerPaymentTerms.getSelectedItemPosition()).getId();
                String dueDate = "";
                if (isDueDateClicked) {
                    dueDate = strDueDateNew;
                } else {
                    dueDate = strDueDate;
                }
                String bilAddress = edittextBilAddress.getText().toString().trim();
                String bilCity = edittextBilCity.getText().toString().trim();
                String bilState = edittextBilState.getText().toString().trim();
                String bilCountry = edittextBilCountry.getText().toString().trim();
                String bilPostal = edittextBilPostal.getText().toString().trim();
                String bilLat = "";
                String bilLng = "";
                if (isBilClicked) {
                    bilLat = strNewBilLat;
                    bilLng = strNewBilLng;
                } else {
                    bilLat = strBilLat;
                    bilLng = strBilLng;
                }

                if (TextUtils.isEmpty(description)) {
                    Utl.showToast(getActivity(), "Enter Description");
                } else if (TextUtils.isEmpty(bilAddress)) {
                    Utl.showToast(getActivity(), "Enter BillingAddress");
                } else if (TextUtils.isEmpty(bilCity)) {
                    Utl.showToast(getActivity(), "Enter Billing City");
                } else if (TextUtils.isEmpty(bilState)) {
                    Utl.showToast(getActivity(), "Enter Billing State");
                } else if (TextUtils.isEmpty(bilCountry)) {
                    Utl.showToast(getActivity(), "Enter Billing Country");
                } else if (TextUtils.isEmpty(bilPostal)) {
                    Utl.showToast(getActivity(), "Enter Billing Postal Code");
                } else {
                    if (isCloneMode) {
                        createInvoice(owner,
                                account,
                                workOrder,
                                contact,
                                description,
                                additionalInfo,
                                status,
                                invoiceDate,
                                paymentTerms,
                                dueDate,
                                bilAddress, bilCity, bilState, bilCountry, bilPostal,
                                bilLat, bilLng);
                    } else {
                        editInvoice(invoiceId,
                                owner,
                                account,
                                workOrder,
                                contact,
                                description,
                                additionalInfo,
                                status,
                                invoiceDate,
                                paymentTerms,
                                dueDate,
                                bilAddress, bilCity, bilState, bilCountry, bilPostal,
                                bilLat, bilLng);
                    }
                }

            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCloneMode = false;
                editMode(false, true);
                radioButtonDetails.setEnabled(true);
                radioButtonRelated.setEnabled(true);
            }
        });

        buttonProductAddMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenuPopupProductAdd(view);
            }
        });

        buttonProductEditMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenuPopupProductEdit(view);
            }
        });

        layoutProductFilter.setOnClickListener(null);

        buttonProductFilterAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutProductFilter.setVisibility(View.VISIBLE);
                listProductFilterFamily = new ArrayList<>();
                adapterProductFilterFamily = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, listProductFilterFamily);
                adapterProductFilterFamily.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                getProductFamilySpinner();
            }
        });

        buttonProductFilterApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String productFamily = "";
                if (spinnerProductFilterFamily.getSelectedItemPosition() != 0) {
                    productFamily = listProductFilterFamily.get(spinnerProductFilterFamily.getSelectedItemPosition()).getId();
                }
                String productCode = edittextProductFilterCode.getText().toString().trim();
                listProduct.clear();
                phvProductAdd.removeAllViews();
                getRelatedInvoiceGetProduct(invoiceId, productFamily, productCode);
                spinnerProductFilterFamily.setAdapter(null);
                edittextProductFilterCode.setText("");
                layoutProductFilter.setVisibility(View.GONE);
            }
        });

        buttonProductFilterCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinnerProductFilterFamily.setSelection(0);
                edittextProductFilterCode.setText("");
                layoutProductFilter.setVisibility(View.GONE);
            }
        });

        buttonProductFilterClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinnerProductFilterFamily.setSelection(0);
                edittextProductFilterCode.setText("");
            }
        });

        edittextProductAddSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                String search = charSequence.toString();
                if (!TextUtils.isEmpty(search)) {
                    phvProductAdd.removeAllViews();
                    ArrayList<ModelProduct> modle = new ArrayList<ModelProduct>();
                    for (ModelProduct m : listProduct) {
                        if (m.getProductName().toLowerCase().contains(search)) {
                            modle.add(m);
                        }
                    }
                    for (int i = 0; i < modle.size(); i++) {
                        phvProductAdd
                                .addView(new AdapterInvoiceProductParent(getActivity(), FragmentInvoiceInfo.this, modle.get(i)));
                        phvProductAdd
                                .addChildView(i, new AdapterInvoiceProductChild(getActivity(), modle.get(i)));
                    }
                } else {
                    phvProductAdd.removeAllViews();
                    for (int i = 0; i < listProduct.size(); i++) {
                        phvProductAdd
                                .addView(new AdapterInvoiceProductParent(getActivity(), FragmentInvoiceInfo.this, listProduct.get(i)));
                        phvProductAdd
                                .addChildView(i, new AdapterInvoiceProductChild(getActivity(), listProduct.get(i)));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        buttonNewNoteCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutNewNote.setVisibility(View.GONE);
            }
        });

        buttonNewNoteSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String subject = edittextNewNoteSubject.getText().toString().trim();
                String body = edittextNewNoteBody.getText().toString().trim();
                if (TextUtils.isEmpty(subject)) {
                    Utl.showToast(getActivity(), "Enter Subject");
                } else if (TextUtils.isEmpty(body)) {
                    Utl.showToast(getActivity(), "Enter Body");
                } else {
                    addNewNote(subject, body);
                }
            }
        });


        buttonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    if (!TextUtils.isEmpty(strPhoneNo)) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + strPhoneNo));
                        startActivity(callIntent);
                    } else {
                        Utl.showToast(getActivity(), "No Phone Number Available.");
                    }
                }
            }
        });

        buttonComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    if (!TextUtils.isEmpty(strPhoneNo)) {
                        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                        smsIntent.setData(Uri.parse("sms:" + strPhoneNo));
                        startActivity(smsIntent);
                    } else {
                        Utl.showToast(getActivity(), "No Phone Number Available.");
                    }
                }
            }
        });

        buttonDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickSendEmail != null) {
                    clickSendEmail.callbackInvoiceSendEmail(invoiceId, strInvoiceNo);
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99) {
            if (data != null) {
                double latitude = data.getDoubleExtra("latitude", 0.0);
                strNewBilLat = String.valueOf(latitude);
                double longitude = data.getDoubleExtra("longitude", 0.0);
                strNewBilLng = String.valueOf(longitude);
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> addresses = new ArrayList<>();
                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                strNewBilAddress = addresses.get(0).getSubThoroughfare() + ", " + addresses.get(0).getThoroughfare();
                if (addresses.get(0).getLocality() != null) {
                    strNewBilCity = addresses.get(0).getLocality();
                } else {
                    strNewBilCity = addresses.get(0).getSubLocality();
                }
                if (addresses.get(0).getAdminArea() != null) {
                    strNewBilState = addresses.get(0).getAdminArea();
                } else {
                    strNewBilState = addresses.get(0).getSubAdminArea();
                }
                strNewBilCountry = addresses.get(0).getCountryName();
                strNewBilPostal = addresses.get(0).getPostalCode();
                Log.d("TAG_LATITUDE****", strNewBilLat);
                Log.d("TAG_LONGITUDE****", strNewBilLng);
                Log.d("TAG_ADDRESS****", strNewBilAddress);
                Log.d("TAG_CITY****", strNewBilCity);
                Log.d("TAG_STATE****", strNewBilState);
                Log.d("TAG_COUNTRY****", strNewBilCountry);
                Log.d("TAG_POSTAL****", strNewBilPostal);
                edittextBilAddress.setText(strNewBilAddress);
                edittextBilCity.setText(strNewBilCity);
                edittextBilState.setText(strNewBilState);
                edittextBilCountry.setText(strNewBilCountry);
                edittextBilPostal.setText(strNewBilPostal);
                isBilClicked = false;
            } else {
                Log.d("RESULT", "CANCELLED");
            }
        }
    }

    private void editMode(boolean bolEdit, boolean bolResponse) {
        if (!bolEdit) {
            layoutHeader.setVisibility(View.VISIBLE);
            radioGroupMain.setVisibility(View.VISIBLE);
            textviewHeaderStatus.setVisibility(View.VISIBLE);

            layoutInvoiceNo.setVisibility(View.VISIBLE);

            layoutOwner.setVisibility(View.GONE);
            textviewOwner.setVisibility(View.VISIBLE);

            layoutAccount.setVisibility(View.GONE);
            textviewAccount.setVisibility(View.VISIBLE);

            layoutWorkOrder.setVisibility(View.GONE);
            spinnerWorkOrder.setVisibility(View.GONE);
            spinnerWorkOrder.setAdapter(null);
            textviewWorkOrder.setVisibility(View.VISIBLE);

            layoutContact.setVisibility(View.GONE);
            spinnerContact.setVisibility(View.GONE);
            spinnerContact.setAdapter(null);
            textviewContact.setVisibility(View.VISIBLE);

            edittextDescription.setVisibility(View.GONE);
            textviewDescription.setVisibility(View.VISIBLE);

            edittextAdditionalInfo.setVisibility(View.GONE);
            textviewAdditionalInfo.setVisibility(View.VISIBLE);

            layoutStatus.setVisibility(View.GONE);
            spinnerStatus.setVisibility(View.GONE);
            spinnerStatus.setAdapter(null);
            textviewStatus.setVisibility(View.VISIBLE);

            layoutPaymentTerms.setVisibility(View.GONE);
            spinnerPaymentTerms.setVisibility(View.GONE);
            spinnerPaymentTerms.setAdapter(null);
            textviewPaymentTerms.setVisibility(View.VISIBLE);

            edittextInvoiceDate.setVisibility(View.GONE);
            textviewInvoiceDate.setVisibility(View.VISIBLE);

            edittextDueDate.setVisibility(View.GONE);
            textviewDueDate.setVisibility(View.VISIBLE);

            edittextBilAddress.setVisibility(View.GONE);
            edittextBilCity.setVisibility(View.GONE);
            edittextBilState.setVisibility(View.GONE);
            edittextBilCountry.setVisibility(View.GONE);
            edittextBilPostal.setVisibility(View.GONE);
            buttonBilAddress.setVisibility(View.GONE);

            buttonCall.setEnabled(true);
            buttonComment.setEnabled(true);
            buttonDate.setEnabled(true);
            buttonEdit.setEnabled(true);

            textviewBilAddress.setVisibility(View.VISIBLE);
            textviewBilCity.setVisibility(View.GONE);
            textviewBilState.setVisibility(View.GONE);
            textviewBilCountry.setVisibility(View.GONE);
            textviewBilPostal.setVisibility(View.GONE);

            textviewSubTotal.setVisibility(View.VISIBLE);
            textviewDiscount.setVisibility(View.VISIBLE);
            textviewTax.setVisibility(View.VISIBLE);
            textviewTotalPrice.setVisibility(View.VISIBLE);
            textviewGrandTotal.setVisibility(View.VISIBLE);

            layoutFinancialInfo.setVisibility(View.VISIBLE);
            layoutSystemInfo.setVisibility(View.VISIBLE);
            layoutSaveCancel.setVisibility(View.GONE);
            layoutNavigation.setVisibility(View.VISIBLE);
            if (bolResponse) {
                setViewInfo();
            }
        } else {
            layoutHeader.setVisibility(View.VISIBLE);
            radioGroupMain.setVisibility(View.VISIBLE);
            textviewHeaderStatus.setVisibility(View.GONE);

            layoutInvoiceNo.setVisibility(View.GONE);

            layoutOwner.setVisibility(View.VISIBLE);
            textviewOwner.setVisibility(View.GONE);

            layoutAccount.setVisibility(View.VISIBLE);
            textviewAccount.setVisibility(View.GONE);

            layoutWorkOrder.setVisibility(View.VISIBLE);
            spinnerWorkOrder.setVisibility(View.VISIBLE);
            spinnerWorkOrder.setAdapter(adapterWorkOrder);
            textviewWorkOrder.setVisibility(View.GONE);

            layoutContact.setVisibility(View.VISIBLE);
            spinnerContact.setVisibility(View.VISIBLE);
            spinnerContact.setAdapter(adapterContact);
            textviewContact.setVisibility(View.GONE);

            edittextDescription.setVisibility(View.VISIBLE);
            textviewDescription.setVisibility(View.GONE);

            edittextAdditionalInfo.setVisibility(View.VISIBLE);
            textviewAdditionalInfo.setVisibility(View.GONE);

            edittextInvoiceDate.setVisibility(View.VISIBLE);
            textviewInvoiceDate.setVisibility(View.GONE);

            edittextDueDate.setVisibility(View.VISIBLE);
            textviewDueDate.setVisibility(View.GONE);

            layoutStatus.setVisibility(View.VISIBLE);
            spinnerStatus.setVisibility(View.VISIBLE);
            spinnerStatus.setAdapter(adapterStatus);
            textviewStatus.setVisibility(View.GONE);

            layoutPaymentTerms.setVisibility(View.VISIBLE);
            spinnerPaymentTerms.setVisibility(View.VISIBLE);
            spinnerPaymentTerms.setAdapter(adapterPaymentTerms);
            textviewPaymentTerms.setVisibility(View.GONE);

            edittextBilAddress.setVisibility(View.VISIBLE);
            edittextBilCity.setVisibility(View.VISIBLE);
            edittextBilState.setVisibility(View.VISIBLE);
            edittextBilCountry.setVisibility(View.VISIBLE);
            edittextBilPostal.setVisibility(View.VISIBLE);
            buttonBilAddress.setVisibility(View.VISIBLE);

            buttonCall.setEnabled(false);
            buttonComment.setEnabled(false);
            buttonDate.setEnabled(false);
            buttonEdit.setEnabled(false);

            textviewBilAddress.setVisibility(View.GONE);
            textviewBilCity.setVisibility(View.VISIBLE);
            textviewBilState.setVisibility(View.VISIBLE);
            textviewBilCountry.setVisibility(View.VISIBLE);
            textviewBilPostal.setVisibility(View.VISIBLE);

            textviewSubTotal.setVisibility(View.GONE);
            textviewDiscount.setVisibility(View.GONE);
            textviewTax.setVisibility(View.GONE);
            textviewTotalPrice.setVisibility(View.GONE);
            textviewGrandTotal.setVisibility(View.GONE);

            layoutFinancialInfo.setVisibility(View.GONE);
            layoutSystemInfo.setVisibility(View.GONE);
            layoutSaveCancel.setVisibility(View.VISIBLE);
            layoutNavigation.setVisibility(View.GONE);
            if (bolResponse) {
                setEditInfo();
            }
        }
    }

    private void cloneMode() {
        layoutHeader.setVisibility(View.GONE);
        radioGroupMain.setVisibility(View.GONE);

        textviewHeaderStatus.setVisibility(View.GONE);

        layoutInvoiceNo.setVisibility(View.GONE);

        layoutOwner.setVisibility(View.VISIBLE);
        textviewOwner.setVisibility(View.GONE);

        layoutAccount.setVisibility(View.VISIBLE);
        textviewAccount.setVisibility(View.GONE);

        layoutWorkOrder.setVisibility(View.VISIBLE);
        spinnerWorkOrder.setVisibility(View.VISIBLE);
        spinnerWorkOrder.setAdapter(adapterWorkOrder);
        textviewWorkOrder.setVisibility(View.GONE);

        layoutContact.setVisibility(View.VISIBLE);
        spinnerContact.setVisibility(View.VISIBLE);
        spinnerContact.setAdapter(adapterContact);
        textviewContact.setVisibility(View.GONE);

        edittextDescription.setVisibility(View.VISIBLE);
        textviewDescription.setVisibility(View.GONE);

        edittextAdditionalInfo.setVisibility(View.VISIBLE);
        textviewAdditionalInfo.setVisibility(View.GONE);

        edittextInvoiceDate.setVisibility(View.VISIBLE);
        textviewInvoiceDate.setVisibility(View.GONE);

        edittextDueDate.setVisibility(View.VISIBLE);
        textviewDueDate.setVisibility(View.GONE);

        layoutStatus.setVisibility(View.VISIBLE);
        spinnerStatus.setVisibility(View.VISIBLE);
        spinnerStatus.setAdapter(adapterStatus);
        textviewStatus.setVisibility(View.GONE);

        layoutPaymentTerms.setVisibility(View.VISIBLE);
        spinnerPaymentTerms.setVisibility(View.VISIBLE);
        spinnerPaymentTerms.setAdapter(adapterPaymentTerms);
        textviewPaymentTerms.setVisibility(View.GONE);

        edittextBilAddress.setVisibility(View.VISIBLE);
        edittextBilCity.setVisibility(View.VISIBLE);
        edittextBilState.setVisibility(View.VISIBLE);
        edittextBilCountry.setVisibility(View.VISIBLE);
        edittextBilPostal.setVisibility(View.VISIBLE);
        buttonBilAddress.setVisibility(View.VISIBLE);

        buttonCall.setEnabled(false);
        buttonComment.setEnabled(false);
        buttonDate.setEnabled(false);
        buttonEdit.setEnabled(false);

        textviewBilAddress.setVisibility(View.GONE);
        textviewBilCity.setVisibility(View.VISIBLE);
        textviewBilState.setVisibility(View.VISIBLE);
        textviewBilCountry.setVisibility(View.VISIBLE);
        textviewBilPostal.setVisibility(View.VISIBLE);

        textviewSubTotal.setVisibility(View.GONE);
        textviewDiscount.setVisibility(View.GONE);
        textviewTax.setVisibility(View.GONE);
        textviewTotalPrice.setVisibility(View.GONE);
        textviewGrandTotal.setVisibility(View.GONE);

        layoutFinancialInfo.setVisibility(View.GONE);
        layoutSystemInfo.setVisibility(View.GONE);
        layoutSaveCancel.setVisibility(View.VISIBLE);
        layoutNavigation.setVisibility(View.GONE);

        setCloneInfo();
    }

    private void setViewInfo() {
        ActivityMain.textviewCenter.setText(strInvoiceNo);
        textviewHeaderStatus.setText("Status: " + strStatusName);
        textviewInvoiceNo.setText(strInvoiceNo);
        textviewOwner.setText(strOwnerName);
        textviewAccount.setText(strAccountName);
        textviewWorkOrder.setText(strWorkOrderName);
        textviewContact.setText(strContactName);
        textviewDescription.setText(strDescription);
        textviewAdditionalInfo.setText(strAdditionalInfo);
        textviewStatus.setText(strStatusName);
        textviewPaymentTerms.setText(strPaymentTermsName);
        textviewInvoiceDate.setText(formatDate(strInvoiceDate));
        textviewDueDate.setText(formatDate(strDueDate));
        textviewBilAddress.setText(strBilAddress + "\n" +
                strBilCity + ", " +
                strBilState + ", " +
                strBilPostal + "\n" +
                strBilCountry);
        textviewSubTotal.setText("$ " + strSubTotal);
        textviewDiscount.setText(strDiscount + " %");
        textviewTax.setText("$ " + strTax);
        textviewTotalPrice.setText("$ " + strTotalPrice);
        textviewGrandTotal.setText("$ " + strGrandTotal);
        textviewCreatedDate.setText(strCreatedDate);
        textviewCreatedBy.setText(strCreatedBy);
        textviewLastModifiedDate.setText(strLastModifiedDate);
        textviewLastModifiedBy.setText(strLastModifiedBy);
    }

    private void setEditInfo() {
        if (isOwnerClicked) {
            edittextOwner.setText(strOwnerNameNew);
        } else {
            edittextOwner.setText(strOwnerName);
        }
        if (isAccountClicked) {
            edittextAccount.setText(strAccountNameNew);
        } else {
            edittextAccount.setText(strAccountName);
        }
        setSpinnerDropDownHeight(listWorkOrder, spinnerWorkOrder);
        spinnerWorkOrder.setSelection(getListIndex(listWorkOrder, strWorkOrderName));
        setSpinnerDropDownHeight(listContacts, spinnerContact);
        spinnerContact.setSelection(getListIndex(listContacts, strContactName));
        edittextDescription.setText(strDescription);
        edittextAdditionalInfo.setText(strAdditionalInfo);
        setSpinnerDropDownHeight(listStatus, spinnerStatus);
        spinnerStatus.setSelection(getListIndex(listStatus, strStatusName));
        setSpinnerDropDownHeight(listPaymentTerms, spinnerPaymentTerms);
        spinnerPaymentTerms.setSelection(getListIndex(listPaymentTerms, strPaymentTermsName));
        if (isInvoiceDateClicked) {
            edittextInvoiceDate.setText(formatDate(strInvoiceDateNew));
        } else {
            edittextInvoiceDate.setText(formatDate(strInvoiceDate));
        }
        if (isDueDateClicked) {
            edittextDueDate.setText(formatDate(strDueDateNew));
        } else {
            edittextDueDate.setText(formatDate(strDueDate));
        }
        edittextBilAddress.setText(strBilAddress);
        edittextBilCity.setText(strBilCity);
        edittextBilState.setText(strBilState);
        edittextBilCountry.setText(strBilCountry);
        edittextBilPostal.setText(strBilPostal);
    }

    private void setCloneInfo() {
        isCloneMode = true;
        ActivityMain.textviewCenter.setText(getString(R.string.invoice_create));
        if (isOwnerClicked) {
            edittextOwner.setText(strOwnerNameNew);
        } else {
            edittextOwner.setText(strOwnerName);
        }
        if (isAccountClicked) {
            edittextAccount.setText(strAccountNameNew);
        } else {
            edittextAccount.setText(strAccountName);
        }
        setSpinnerDropDownHeight(listWorkOrder, spinnerWorkOrder);
        spinnerWorkOrder.setSelection(getListIndex(listWorkOrder, strWorkOrderName));
        setSpinnerDropDownHeight(listContacts, spinnerContact);
        spinnerContact.setSelection(getListIndex(listContacts, strContactName));
        edittextDescription.setText(strDescription);
        edittextAdditionalInfo.setText(strAdditionalInfo);
        setSpinnerDropDownHeight(listStatus, spinnerStatus);
        spinnerStatus.setSelection(getListIndex(listStatus, strStatusName));
        setSpinnerDropDownHeight(listPaymentTerms, spinnerPaymentTerms);
        spinnerPaymentTerms.setSelection(getListIndex(listPaymentTerms, strPaymentTermsName));
        if (isInvoiceDateClicked) {
            edittextInvoiceDate.setText(formatDate(strInvoiceDateNew));
        } else {
            edittextInvoiceDate.setText(formatDate(strInvoiceDate));
        }
        if (isDueDateClicked) {
            edittextDueDate.setText(formatDate(strDueDateNew));
        } else {
            edittextDueDate.setText(formatDate(strDueDate));
        }
        edittextBilAddress.setText(strBilAddress);
        edittextBilCity.setText(strBilCity);
        edittextBilState.setText(strBilState);
        edittextBilCountry.setText(strBilCountry);
        edittextBilPostal.setText(strBilPostal);
    }

    private void setSpinnerDropDownHeight(List<ModelSpinner> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        spinner.setDropDownWidth(width - 70);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    private void setFilterSpinnerDropDownHeight(List<ModelSpinner> list, Spinner spinner) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        if (list.size() > 4) {
            try {
                Field popup = Spinner.class.getDeclaredField("mPopup");
                popup.setAccessible(true);
                android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);
                popupWindow.setHeight(800);
            } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            }
        }
    }

    public int getListIndex(List<ModelSpinner> list, String name) {
        for (int i = 0; i < list.size(); i++) {
            ModelSpinner model = list.get(i);
            if (name.equals(model.getName())) {
                return i;
            }
        }
        return -1;
    }

    public String selectSimpleDate(int d, int m, int y) {
        String time = "" + y + "-" + (m + 1) + "-" + d;
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String formatSimpleDate(String strDate) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(strDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String selectTimefromDate(String strDate) {
        String inputPattern = "MM/dd/yyyy hh:mm aaa";
        ;
        String outputPattern = "hh:mm aaa";
        ;
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(strDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String formatTime(String time) {
        String inputPattern = "kk:mm:ss";
        String outputPattern = "hh:mm aaa";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String formatDate(String time) {
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String setTime(int min, int hr, String AM_PM, int format) {
        String time = "" + hr + ":" + min + " " + AM_PM;
        String inputPattern;
        String outputPattern;
        if (format == 0) {
            inputPattern = "hh:mm aaa";
            outputPattern = "hh:mm aaa";
        } else {
            inputPattern = "hh:mm aaa";
            outputPattern = "kk:mm:ss";
        }
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String setDate(int d, int m, int y) {
        String time = "" + (m + 1) + "/" + d + "/" + y;
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "MM/dd/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void showMenuPopupProductAdd(View view) {
        final List<HashMap<String, Object>> data = new ArrayList<>();
        HashMap<String, Object> map = new HashMap<>();
        map.put("title", getString(R.string.save));
        map.put("icon", R.drawable.bg_home);
        data.add(map);

        map = new HashMap<>();
        map.put("title", getString(R.string.select_n_add_more));
        map.put("icon", R.drawable.bg_home);
        data.add(map);

        map = new HashMap<>();
        map.put("title", getString(R.string.cancel));
        map.put("icon", R.drawable.bg_home);
        data.add(map);

        final ListPopupWindow popupWindow = new ListPopupWindow(getActivity());
        ListAdapter adapter = new SimpleAdapter(
                getActivity(),
                data,
                R.layout.menu_popup,
                new String[]{"title"},
                new int[]{R.id.title});

        popupWindow.setAnchorView(view);
        popupWindow.setAdapter(adapter);
        popupWindow.setContentWidth(dialogWidth(getActivity(), adapter));
        popupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                switch (position) {
                    case 0:
                        phvProductAdd.removeAllViews();
                        layoutProductAdd.setVisibility(View.GONE);
                        layoutProductEdit.setVisibility(View.VISIBLE);

                        for (int i = 0; i < Singleton.getInstance().listInvoiceProduct.size(); i++) {

                            LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View v = layoutInflater.inflate(R.layout.layout_product_edit_change, null);

                            layoutProductEditChange.addView(v);

                            int size = layoutProductEditChange.getChildCount();
                            View viw = layoutProductEditChange.getChildAt(size - 1);

                            final CheckBox checkboxProductId = (CheckBox) viw.findViewById(R.id.checkbox_product_id);
                            final ToggleButton toggleIcon = (ToggleButton) viw.findViewById(R.id.toggle_icon);

                            final TextView textviewName = (TextView) viw.findViewById(R.id.textview_name);
                            final TextView textviewQuantity = (TextView) viw.findViewById(R.id.textview_quantity);
                            final TextView textviewNetTotal = (TextView) viw.findViewById(R.id.textview_net_total);
                            final LinearLayout layoutToggle = (LinearLayout) viw.findViewById(R.id.layout_toggle);

                            final EditText edittextListPrice = (EditText) viw.findViewById(R.id.edittext_list_price);
                            final EditText edittextDiscount = (EditText) viw.findViewById(R.id.edittext_discount);
                            final EditText edittextUnitPrice = (EditText) viw.findViewById(R.id.edittext_unit_price);
                            final EditText edittextQuantity = (EditText) viw.findViewById(R.id.edittext_quantity);
                            final EditText edittextSubTotal = (EditText) viw.findViewById(R.id.edittext_sub_total);
                            final CheckBox checkboxTexable = (CheckBox) viw.findViewById(R.id.checkbox_texable);
                            final EditText edittextNetTotal = (EditText) viw.findViewById(R.id.edittext_net_total);

                            checkboxProductId.setChecked(false);
                            checkboxProductId.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                                    if (isChecked) {
                                        listProductEditChange.add(Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getProductID());
                                    } else {
                                        int idx = listProductEditChange.indexOf(Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getProductID());
                                        listProductEditChange.remove(idx);
                                    }
                                }
                            });

                            layoutToggle.setVisibility(View.GONE);
                            toggleIcon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                                    if (isChecked) {
                                        layoutToggle.setVisibility(View.VISIBLE);
                                        textviewQuantity.setVisibility(View.GONE);
                                        textviewNetTotal.setVisibility(View.GONE);
                                    } else {
                                        layoutToggle.setVisibility(View.GONE);
                                        textviewQuantity.setVisibility(View.VISIBLE);
                                        textviewNetTotal.setVisibility(View.VISIBLE);
                                    }
                                }
                            });

                            double listPrice = Double.valueOf(Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getListPrice());
                            double discount = Double.valueOf(Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getDiscount());
                            double unitPrice = getUnitPrice(Double.valueOf(Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getUnitPrice()), discount);
                            int quantity = Integer.valueOf(Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getDefaultQuantity());
                            double subtotal = getSubTotal(listPrice, discount, quantity);
                            int isTaxable = Integer.valueOf(Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getTaxable());
                            final double tax = Double.valueOf(Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getTax());
                            double netTotal = getNetTotal(listPrice, discount, quantity, isTaxable, tax);

                            double grandTotal = 0;
                            for (int k = 0; k < Singleton.getInstance().listInvoiceProduct.size(); k++) {
                                double net = Double.valueOf(Singleton.getInstance().listInvoiceProduct.get(k).getNetTotal());
                                grandTotal = grandTotal + net;
                            }
                            textviewProductEditGrandTotal.setText("Grand Total: ($) " + String.format("%.2f", grandTotal));

                            textviewName.setText("Product: " + Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getProductName());
                            textviewQuantity.setText("Quantity: " + String.valueOf(quantity));
                            textviewNetTotal.setText("Net Total: $" + String.format("%.2f", netTotal));

                            edittextListPrice.setText(String.format("%.2f", listPrice));
                            edittextDiscount.setText(String.format("%.2f", discount));
                            edittextUnitPrice.setText(String.format("%.2f", unitPrice));
                            edittextQuantity.setText(String.valueOf(quantity));
                            edittextSubTotal.setText(String.format("%.2f", subtotal));
                            if (isTaxable == 1) {
                                checkboxTexable.setChecked(true);
                            } else {
                                checkboxTexable.setChecked(false);
                            }
                            edittextNetTotal.setText(String.format("%.2f", netTotal));

                            edittextListPrice.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {

                                }

                                @Override
                                public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {

                                    String strListPrice = edittextListPrice.getText().toString();
                                    if (TextUtils.isEmpty(strListPrice)) {
                                        strListPrice = "0";
                                        edittextListPrice.setText("0.00");
                                    }
                                    String strDiscount = edittextDiscount.getText().toString();
                                    if (TextUtils.isEmpty(strDiscount)) {
                                        strDiscount = "0";
                                        edittextDiscount.setText("0.00");
                                    }
                                    String strQuantity = edittextQuantity.getText().toString();
                                    if (TextUtils.isEmpty(strQuantity)) {
                                        strQuantity = "0";
                                        edittextQuantity.setText("0");
                                    }

                                    double listPrice = Double.valueOf(strListPrice);
                                    double discount = Double.valueOf(strDiscount);
                                    double unitPrice = getUnitPrice(listPrice, discount);
                                    int quantity = Integer.valueOf(strQuantity);
                                    double subtotal = getSubTotal(listPrice, discount, quantity);
                                    int isTaxable;
                                    if (checkboxTexable.isChecked()) {
                                        isTaxable = 1;
                                    } else {
                                        isTaxable = 0;
                                    }
                                    double netTotal = getNetTotal(listPrice, discount, quantity, isTaxable, tax);

                                    textviewNetTotal.setText("Net Total: $" + String.format("%.2f", netTotal));
                                    edittextUnitPrice.setText(String.format("%.2f", unitPrice));
                                    edittextSubTotal.setText(String.format("%.2f", subtotal));
                                    edittextNetTotal.setText(String.format("%.2f", netTotal));

                                    double grandTotal = 0;
                                    for (int k = 0; k < Singleton.getInstance().listInvoiceProduct.size(); k++) {
                                        if (k != layoutProductEditChange.indexOfChild(v)) {
                                            double net = Double.valueOf(Singleton.getInstance().listInvoiceProduct.get(k).getNetTotal());
                                            grandTotal = grandTotal + net;
                                        }
                                    }
                                    textviewProductEditGrandTotal.setText("Grand Total: ($) " + String.format("%.2f", grandTotal + netTotal));

                                    layoutProductEditChange.invalidate();
                                    updateModelProduct(layoutProductEditChange.indexOfChild(v),
                                            Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getProductID(),
                                            Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getProductName(),
                                            String.format("%.2f", listPrice),
                                            String.format("%.2f", discount),
                                            String.format("%.2f", unitPrice),
                                            String.valueOf(quantity),
                                            String.format("%.2f", subtotal),
                                            String.valueOf(isTaxable),
                                            Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getTax(),
                                            String.format("%.2f", netTotal));

                                }

                                @Override
                                public void afterTextChanged(Editable editable) {

                                }
                            });

                            edittextDiscount.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {

                                }

                                @Override
                                public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {

                                    String strListPrice = edittextListPrice.getText().toString();
                                    if (TextUtils.isEmpty(strListPrice)) {
                                        strListPrice = "0";
                                        edittextListPrice.setText("0.00");
                                    }
                                    String strDiscount = edittextDiscount.getText().toString();
                                    if (TextUtils.isEmpty(strDiscount)) {
                                        strDiscount = "0";
                                        edittextDiscount.setText("0.00");
                                    }
                                    String strQuantity = edittextQuantity.getText().toString();
                                    if (TextUtils.isEmpty(strQuantity)) {
                                        strQuantity = "0";
                                        edittextQuantity.setText("0");
                                    }

                                    double listPrice = Double.valueOf(strListPrice);
                                    double discount = Double.valueOf(strDiscount);
                                    double unitPrice = getUnitPrice(listPrice, discount);
                                    int quantity = Integer.valueOf(strQuantity);
                                    double subtotal = getSubTotal(listPrice, discount, quantity);
                                    int isTaxable;
                                    if (checkboxTexable.isChecked()) {
                                        isTaxable = 1;
                                    } else {
                                        isTaxable = 0;
                                    }
                                    double netTotal = getNetTotal(listPrice, discount, quantity, isTaxable, tax);

                                    textviewNetTotal.setText("Net Total: $" + String.format("%.2f", netTotal));
                                    edittextUnitPrice.setText(String.format("%.2f", unitPrice));
                                    edittextSubTotal.setText(String.format("%.2f", subtotal));
                                    edittextNetTotal.setText(String.format("%.2f", netTotal));

                                    double grandTotal = 0;
                                    for (int k = 0; k < Singleton.getInstance().listInvoiceProduct.size(); k++) {
                                        if (k != layoutProductEditChange.indexOfChild(v)) {
                                            double net = Double.valueOf(Singleton.getInstance().listInvoiceProduct.get(k).getNetTotal());
                                            grandTotal = grandTotal + net;
                                        }
                                    }
                                    textviewProductEditGrandTotal.setText("Grand Total: ($) " + String.format("%.2f", grandTotal + netTotal));

                                    layoutProductEditChange.invalidate();
                                    updateModelProduct(layoutProductEditChange.indexOfChild(v),
                                            Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getProductID(),
                                            Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getProductName(),
                                            String.format("%.2f", listPrice),
                                            String.format("%.2f", discount),
                                            String.format("%.2f", unitPrice),
                                            String.valueOf(quantity),
                                            String.format("%.2f", subtotal),
                                            String.valueOf(isTaxable),
                                            Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getTax(),
                                            String.format("%.2f", netTotal));
                                }

                                @Override
                                public void afterTextChanged(Editable editable) {

                                }
                            });

                            edittextQuantity.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {

                                }

                                @Override
                                public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {

                                    String strListPrice = edittextListPrice.getText().toString();
                                    if (TextUtils.isEmpty(strListPrice)) {
                                        strListPrice = "0";
                                        edittextListPrice.setText("0.00");
                                    }
                                    String strDiscount = edittextDiscount.getText().toString();
                                    if (TextUtils.isEmpty(strDiscount)) {
                                        strDiscount = "0";
                                        edittextDiscount.setText("0.00");
                                    }
                                    String strQuantity = edittextQuantity.getText().toString();
                                    if (TextUtils.isEmpty(strQuantity)) {
                                        strQuantity = "0";
                                        edittextQuantity.setText("0");
                                    }

                                    double listPrice = Double.valueOf(strListPrice);
                                    double discount = Double.valueOf(strDiscount);
                                    double unitPrice = getUnitPrice(listPrice, discount);
                                    int quantity = Integer.valueOf(strQuantity);
                                    double subtotal = getSubTotal(listPrice, discount, quantity);
                                    int isTaxable;
                                    if (checkboxTexable.isChecked()) {
                                        isTaxable = 1;
                                    } else {
                                        isTaxable = 0;
                                    }
                                    double netTotal = getNetTotal(listPrice, discount, quantity, isTaxable, tax);

                                    textviewQuantity.setText("Quantity: " + String.valueOf(quantity));
                                    textviewNetTotal.setText("Net Total: $" + String.format("%.2f", netTotal));
                                    edittextSubTotal.setText(String.format("%.2f", subtotal));
                                    edittextNetTotal.setText(String.format("%.2f", netTotal));

                                    double grandTotal = 0;
                                    for (int k = 0; k < Singleton.getInstance().listInvoiceProduct.size(); k++) {
                                        if (k != layoutProductEditChange.indexOfChild(v)) {
                                            double net = Double.valueOf(Singleton.getInstance().listInvoiceProduct.get(k).getNetTotal());
                                            grandTotal = grandTotal + net;
                                        }
                                    }
                                    textviewProductEditGrandTotal.setText("Grand Total: ($) " + String.format("%.2f", grandTotal + netTotal));

                                    layoutProductEditChange.invalidate();
                                    updateModelProduct(layoutProductEditChange.indexOfChild(v),
                                            Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getProductID(),
                                            Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getProductName(),
                                            String.format("%.2f", listPrice),
                                            String.format("%.2f", discount),
                                            String.format("%.2f", unitPrice),
                                            String.valueOf(quantity),
                                            String.format("%.2f", subtotal),
                                            String.valueOf(isTaxable),
                                            Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getTax(),
                                            String.format("%.2f", netTotal));
                                }

                                @Override
                                public void afterTextChanged(Editable editable) {

                                }
                            });

                            checkboxTexable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                                    int isTaxable;
                                    if (isChecked) {
                                        isTaxable = 1;
                                    } else {
                                        isTaxable = 0;
                                    }

                                    String strListPrice = edittextListPrice.getText().toString();
                                    if (TextUtils.isEmpty(strListPrice)) {
                                        strListPrice = "0";
                                        edittextListPrice.setText("0.00");
                                    }
                                    String strDiscount = edittextDiscount.getText().toString();
                                    if (TextUtils.isEmpty(strDiscount)) {
                                        strDiscount = "0";
                                        edittextDiscount.setText("0.00");
                                    }
                                    String strQuantity = edittextQuantity.getText().toString();
                                    if (TextUtils.isEmpty(strQuantity)) {
                                        strQuantity = "0";
                                        edittextQuantity.setText("0");
                                    }

                                    double listPrice = Double.valueOf(strListPrice);
                                    double discount = Double.valueOf(strDiscount);
                                    double unitPrice = getUnitPrice(listPrice, discount);
                                    int quantity = Integer.valueOf(strQuantity);
                                    double subtotal = getSubTotal(listPrice, discount, quantity);
                                    double netTotal = getNetTotal(listPrice, discount, quantity, isTaxable, tax);

                                    textviewNetTotal.setText("Net Total: $" + String.format("%.2f", netTotal));
                                    edittextNetTotal.setText(String.format("%.2f", netTotal));

                                    double grandTotal = 0;
                                    for (int k = 0; k < Singleton.getInstance().listInvoiceProduct.size(); k++) {
                                        if (k != layoutProductEditChange.indexOfChild(v)) {
                                            double net = Double.valueOf(Singleton.getInstance().listInvoiceProduct.get(k).getNetTotal());
                                            grandTotal = grandTotal + net;
                                        }
                                    }
                                    textviewProductEditGrandTotal.setText("Grand Total: ($) " + String.format("%.2f", grandTotal + netTotal));

                                    layoutProductEditChange.invalidate();
                                    updateModelProduct(layoutProductEditChange.indexOfChild(v),
                                            Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getProductID(),
                                            Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getProductName(),
                                            String.format("%.2f", listPrice),
                                            String.format("%.2f", discount),
                                            String.format("%.2f", unitPrice),
                                            String.valueOf(quantity),
                                            String.format("%.2f", subtotal),
                                            String.valueOf(isTaxable),
                                            Singleton.getInstance().listInvoiceProduct.get(layoutProductEditChange.indexOfChild(v)).getTax(),
                                            String.format("%.2f", netTotal));
                                }
                            });
                        }

                        popupWindow.dismiss();
                        break;
                    case 1:
                        addInvoiceLineItem(0, false, Singleton.getInstance().listInvoiceProduct);
                        popupWindow.dismiss();
                        break;
                    case 2:
                        listProduct.clear();
                        Singleton.getInstance().listInvoiceProduct.clear();
                        phvProductAdd.removeAllViews();
                        layoutProductAdd.setVisibility(View.GONE);
                        popupWindow.dismiss();
                        break;
                }
            }
        });
        popupWindow.show();
    }

    private void showMenuPopupProductEdit(View view) {
        final List<HashMap<String, Object>> data = new ArrayList<>();
        HashMap<String, Object> map = new HashMap<>();
        map.put("title", getString(R.string.save));
        map.put("icon", R.drawable.bg_home);
        data.add(map);

        map = new HashMap<>();
        map.put("title", getString(R.string.quick_select));
        map.put("icon", R.drawable.bg_home);
        data.add(map);

        map = new HashMap<>();
        map.put("title", getString(R.string.add_products));
        map.put("icon", R.drawable.bg_home);
        data.add(map);

        map = new HashMap<>();
        map.put("title", getString(R.string.delete_lines));
        map.put("icon", R.drawable.bg_home);
        data.add(map);

        map = new HashMap<>();
        map.put("title", getString(R.string.cancel));
        map.put("icon", R.drawable.bg_home);
        data.add(map);

        final ListPopupWindow popupWindow = new ListPopupWindow(getActivity());
        ListAdapter adapter = new SimpleAdapter(
                getActivity(),
                data,
                R.layout.menu_popup,
                new String[]{"title"},
                new int[]{R.id.title});

        popupWindow.setAnchorView(view);
        popupWindow.setAdapter(adapter);
        popupWindow.setContentWidth(dialogWidth(getActivity(), adapter));
        popupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        addInvoiceLineItem(1, false, Singleton.getInstance().listInvoiceProduct);
                        popupWindow.dismiss();
                        break;
                    case 1:
                        addInvoiceLineItem(1, true, Singleton.getInstance().listInvoiceProduct);
                        popupWindow.dismiss();
                        break;
                    case 2:
                        layoutProductEditChange.removeAllViews();
                        layoutProductEdit.setVisibility(View.GONE);
                        layoutProductAdd.setVisibility(View.VISIBLE);
                        for (int i = 0; i < listProduct.size(); i++) {
                            phvProductAdd
                                    .addView(new AdapterInvoiceProductParent(getActivity(), FragmentInvoiceInfo.this, listProduct.get(i)));
                            phvProductAdd
                                    .addChildView(i, new AdapterInvoiceProductChild(getActivity(), listProduct.get(i)));
                        }
                        popupWindow.dismiss();
                        break;
                    case 3:
                        for (int i = 0; i < Singleton.getInstance().listInvoiceProduct.size(); i++) {
                            String strId = Singleton.getInstance().listInvoiceProduct.get(i).getProductID();
                            if (listProductEditChange.contains(strId)) {
                                Singleton.getInstance().listInvoiceProduct.remove(i);
                                layoutProductEditChange.removeViewAt(i);
                            }
                        }
                        layoutProductEditChange.invalidate();
                        double grandTotal = 0;
                        for (int k = 0; k < Singleton.getInstance().listInvoiceProduct.size(); k++) {
                            double net = Double.valueOf(Singleton.getInstance().listInvoiceProduct.get(k).getNetTotal());
                            grandTotal = grandTotal + net;
                        }
                        textviewProductEditGrandTotal.setText("Grand Total: ($) " + String.format("%.2f", grandTotal));
                        popupWindow.dismiss();
                        break;
                    case 4:
                        listProduct.clear();
                        listProductEditChange.clear();
                        Singleton.getInstance().listInvoiceProduct.clear();
                        layoutProductEditChange.removeAllViews();
                        layoutProductEdit.setVisibility(View.GONE);

                        popupWindow.dismiss();
                        break;
                }
            }
        });
        popupWindow.show();
    }

    private int dialogWidth(Context c, ListAdapter la) {
        ViewGroup viewGroup = null;
        int maxWidth = 0;
        View itemView = null;
        int itemType = 0;
        final ListAdapter adapter = la;
        final int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int count = adapter.getCount();
        for (int i = 0; i < count; i++) {
            final int positionType = adapter.getItemViewType(i);
            if (positionType != itemType) {
                itemType = positionType;
                itemView = null;
            }
            if (viewGroup == null) {
                viewGroup = new FrameLayout(c);
            }
            itemView = adapter.getView(i, itemView, viewGroup);
            itemView.measure(widthMeasureSpec, heightMeasureSpec);
            final int itemWidth = itemView.getMeasuredWidth();
            if (itemWidth > maxWidth) {
                maxWidth = itemWidth;
            }
        }
        return maxWidth;
    }

    private double getUnitPrice(double listPrice, double discount) {
        double subPrice, totalPrice;
        subPrice = listPrice;
        totalPrice = subPrice - ((subPrice / 100.0f) * discount);
        return totalPrice;
    }

    private double getSubTotal(double listPrice, double discount, int quantity) {
        double subTotal, total;
        subTotal = listPrice * quantity;
        total = subTotal - ((subTotal / 100.0f) * discount);
        return total;
    }

    private double getNetTotal(double listPrice, double discount, int quantity, int isTaxable, double tax) {
        double subTotal, netTotal, total = 0;
        subTotal = listPrice * quantity;
        netTotal = subTotal - ((subTotal / 100.0f) * discount);
        if (isTaxable == 1) {
            total = netTotal + ((netTotal / 100.0f) * tax);
        } else {
            total = netTotal;
        }
        return total;
    }

    private void updateModelProduct(int position,
                                    String productId,
                                    String productName,
                                    String listPrice,
                                    String discount,
                                    String unitPrice,
                                    String quantity,
                                    String subTotal,
                                    String taxable,
                                    String tax,
                                    String netTotal) {
        ModelProduct mdl = new ModelProduct();
        mdl.setProductID(productId);
        mdl.setProductName(productName);
        mdl.setListPrice(listPrice);
        mdl.setDiscount(discount);
        mdl.setUnitPrice(unitPrice);
        mdl.setDefaultQuantity(quantity);
        mdl.setSubTotal(subTotal);
        mdl.setTaxable(taxable);
        mdl.setTax(tax);
        mdl.setNetTotal(netTotal);
        Singleton.getInstance().listInvoiceProduct.set(position, mdl);
    }

    private void dialogMore() {
        final Dialog dialogMore = new Dialog(getActivity(), R.style.DialogFullScreen);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMore.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        dialogMore.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMore.setCancelable(true);
        dialogMore.setCanceledOnTouchOutside(true);
        dialogMore.setContentView(R.layout.dialog_more);

        TextView buttonMoreCall = (TextView) dialogMore.findViewById(R.id.button_more_call);
        TextView buttonMoreText = (TextView) dialogMore.findViewById(R.id.button_more_text);
        TextView buttonMoreEmail = (TextView) dialogMore.findViewById(R.id.button_more_edit);
        TextView buttonMoreEdit = (TextView) dialogMore.findViewById(R.id.button_more_email);
        TextView buttonMoreSign = (TextView) dialogMore.findViewById(R.id.button_more_sign);
        TextView buttonMoreDoc = (TextView) dialogMore.findViewById(R.id.button_more_doc);
        TextView buttonMoreNewLine = (TextView) dialogMore.findViewById(R.id.button_more_new_line);
        TextView buttonMoreChemical = (TextView) dialogMore.findViewById(R.id.button_more_new_chemical);
        TextView buttonMoreEvent = (TextView) dialogMore.findViewById(R.id.button_more_new_event);
        TextView buttonMoreInvoice = (TextView) dialogMore.findViewById(R.id.button_more_new_invoice);
        TextView buttonMoreFile = (TextView) dialogMore.findViewById(R.id.button_more_new_file);
        TextView buttonMoreNote = (TextView) dialogMore.findViewById(R.id.button_more_new_note);
        TextView buttonMoreTask = (TextView) dialogMore.findViewById(R.id.button_more_new_task);
        TextView buttonMoreConvert = (TextView) dialogMore.findViewById(R.id.button_more_convert);
        TextView buttonMoreClone = (TextView) dialogMore.findViewById(R.id.button_more_clone);
        TextView buttonMoreDelete = (TextView) dialogMore.findViewById(R.id.button_more_delete);
        View buttomViewCall = (View) dialogMore.findViewById(R.id.view_more_call);
        View buttomViewText = (View) dialogMore.findViewById(R.id.view_more_text);
        View buttomViewEmail = (View) dialogMore.findViewById(R.id.view_more_email);
        View buttomViewEdit = (View) dialogMore.findViewById(R.id.view_more_edit);
        View buttomViewSign = (View) dialogMore.findViewById(R.id.view_more_sign);
        View buttomViewDoc = (View) dialogMore.findViewById(R.id.view_more_doc);
        View buttomViewNewLine = (View) dialogMore.findViewById(R.id.view_more_new_line);
        View buttomViewChemical = (View) dialogMore.findViewById(R.id.view_more_new_chemical);
        View buttomViewEvent = (View) dialogMore.findViewById(R.id.view_more_new_event);
        View buttomViewInvoice = (View) dialogMore.findViewById(R.id.view_more_new_invoice);
        View buttomViewFile = (View) dialogMore.findViewById(R.id.view_more_new_file);
        View buttomViewNote = (View) dialogMore.findViewById(R.id.view_more_new_note);
        View buttomViewTask = (View) dialogMore.findViewById(R.id.view_more_new_task);
        View buttomViewConvert = (View) dialogMore.findViewById(R.id.view_more_convert);
        View buttomViewClone = (View) dialogMore.findViewById(R.id.view_more_clone);
        Button buttonMoreClose = (Button) dialogMore.findViewById(R.id.button_more_close);

        buttonMoreChemical.setVisibility(View.GONE);
        buttomViewChemical.setVisibility(View.GONE);
        buttonMoreInvoice.setVisibility(View.GONE);
        buttomViewInvoice.setVisibility(View.GONE);
        buttonMoreConvert.setVisibility(View.GONE);
        buttomViewConvert.setVisibility(View.GONE);
        buttonMoreClone.setText(getString(R.string.clone_invoice));
        buttonMoreDelete.setText(getString(R.string.delete_invoice));

        buttonMoreCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    if (!TextUtils.isEmpty(strPhoneNo)) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + strPhoneNo));
                        startActivity(callIntent);
                    } else {
                        Utl.showToast(getActivity(), "No Phone Number Available.");
                    }
                }
            }
        });

        buttonMoreText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    if (!TextUtils.isEmpty(strPhoneNo)) {
                        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                        smsIntent.setData(Uri.parse("sms:" + strPhoneNo));
                        startActivity(smsIntent);
                    } else {
                        Utl.showToast(getActivity(), "No Phone Number Available.");
                    }
                }
            }
        });

        buttonMoreEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonDetails.setEnabled(false);
                radioButtonRelated.setEnabled(false);
                editMode(true, true);
                dialogMore.dismiss();
            }
        });

        buttonMoreEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickSendEmail != null) {
                    clickSendEmail.callbackInvoiceSendEmail(invoiceId, strInvoiceNo);
                }
                dialogMore.dismiss();
            }
        });

        buttonMoreDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogDocument();
                dialogMore.dismiss();
            }
        });

        buttonMoreEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickCrateEvent != null) {
                    clickCrateEvent.callbackInvoiceCreateEvent(getString(R.string.invoice), invoiceId, strInvoiceNo);
                }
                dialogMore.dismiss();
            }
        });

        buttonMoreFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickCrateFile != null) {
                    clickCrateFile.callbackInvoiceCreateFile(getString(R.string.invoice));
                }
                dialogMore.dismiss();
            }
        });

        buttonMoreNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutNewNote.setVisibility(View.VISIBLE);
                dialogMore.dismiss();
            }
        });

        buttonMoreTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
                if (clickTaskCreate != null) {
                    clickTaskCreate.callbackInvoiceTaskCreate();
                }
            }
        });

        buttonMoreClone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cloneMode();
                dialogMore.dismiss();
            }
        });

        buttonMoreDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteObject();
                dialogMore.dismiss();
            }
        });

        buttonMoreClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMore.dismiss();
            }
        });

        dialogMore.show();
        dialogMore.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialogMore.getWindow().setDimAmount(0.5f);
        dialogMore.getWindow().setAttributes(lp);
    }

    private void dialogDocument() {
        final Dialog dialogDocument = new Dialog(getActivity(), R.style.DialogFullScreen);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogDocument.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialogDocument.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogDocument.setCancelable(true);
        dialogDocument.setCanceledOnTouchOutside(true);
        dialogDocument.setContentView(R.layout.dialog_document_more);

        Spinner spinnerTemplate = (Spinner) dialogDocument.findViewById(R.id.spinner_template);
        Spinner spinnerDocFormat = (Spinner) dialogDocument.findViewById(R.id.spinner_document_format);
        List<ModelSpinner> listTemplate = new ArrayList<>();
        List<ModelSpinner> listDocFormat = new ArrayList<>();
        listDocFormat.add(new ModelSpinner("0", "PDF"));
        listDocFormat.add(new ModelSpinner("1", "Word"));
        ArrayAdapter<ModelSpinner> adapterTemplate = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listTemplate);
        adapterTemplate.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<ModelSpinner> adapterDocFormat = new ArrayAdapter<ModelSpinner>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listDocFormat);
        adapterDocFormat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDocFormat.setAdapter(adapterDocFormat);
        getTemplateList(spinnerTemplate, adapterTemplate, listTemplate);
        CheckBox checkboxIsSaveToObject = (CheckBox) dialogDocument.findViewById(R.id.checkbox_is_save_to_object);
        Button buttonSave = (Button) dialogDocument.findViewById(R.id.button_save);
        Button buttonCancel = (Button) dialogDocument.findViewById(R.id.button_cancel);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String templateID = listTemplate.get(spinnerTemplate.getSelectedItemPosition()).getId();
                ;
                String isSaveToObject = "";
                if (checkboxIsSaveToObject.isChecked()) {
                    isSaveToObject = "1";
                } else {
                    isSaveToObject = "0";
                }
                String outputFormat = listDocFormat.get(spinnerDocFormat.getSelectedItemPosition()).getId();
                createDocument(dialogDocument, templateID, isSaveToObject, outputFormat);
            }
        });
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogDocument.dismiss();
            }
        });

        dialogDocument.show();
        dialogDocument.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialogDocument.getWindow().setDimAmount(0.5f);
        dialogDocument.getWindow().setAttributes(lp);
    }

    private void getTemplateList(Spinner spinnerTemplate, ArrayAdapter<ModelSpinner> adapterTemplate, List<ModelSpinner> listTemplate) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), getString(R.string.invoice)));
        Call<ResponseBody> response = apiInterface.getTemplateList(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listTemplate.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_GenDocTemplateID));
                                model.setName(dataObject.getString(Cons.KEY_TemplateName));
                                listTemplate.add(model);
                            }
                            spinnerTemplate.setAdapter(adapterTemplate);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void createDocument(Dialog dialogDocument, String templateID, String isSaveToObject, String outputFormat) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), getString(R.string.invoice)));
        map.put(Cons.KEY_ObjectID, RequestBody.create(MediaType.parse("text/plain"), invoiceId));
        map.put(Cons.KEY_GenDocTemplateID, RequestBody.create(MediaType.parse("text/plain"), templateID));
        map.put(Cons.KEY_SaveToObject, RequestBody.create(MediaType.parse("text/plain"), isSaveToObject));
        map.put(Cons.KEY_OutputFormat, RequestBody.create(MediaType.parse("text/plain"), outputFormat));
        Call<ResponseBody> response = apiInterface.createDocument(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            JSONObject dataObject = object.getJSONObject("data");
                            String generatedDocumentURL = dataObject.getString(Cons.KEY_GenerateDocumentURL);
                            dialogDocument.dismiss();
                            webView.setWebViewClient(new WebViewClient() {
                                @Override
                                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                                    super.onPageStarted(view, url, favicon);
                                }

                                @Override
                                public void onPageFinished(WebView view, String url) {
                                    super.onPageFinished(view, url);
                                    layoutWebView.setVisibility(View.VISIBLE);
                                }
                            });
                            webView.loadUrl("https://docs.google.com/viewer?url=" + generatedDocumentURL);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void deleteObject() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), "Invoice"));
        map.put(Cons.KEY_What, RequestBody.create(MediaType.parse("text/plain"), invoiceId));
        Call<ResponseBody> response = apiInterface.deleteObject(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_File_Create" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            clickGoToRecent.callbackGoToRecentInvoice(getString(R.string.invoice));
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });

    }

    private void getOwners() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getInvoiceOwners(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Invoice_AllU" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listOwners.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_UserID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listOwners.add(model);
                            }
                            for (int i = 0; i < listOwners.size(); i++) {
                                phvOwnerAdd
                                        .addView(new AdapterOwner(getActivity(), FragmentInvoiceInfo.this, i, listOwners.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getAccounts() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getInvoiceAccounts(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Accounts" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listAccounts.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_AccountID));
                                model.setName(dataObject.getString(Cons.KEY_AccountName));
                                listAccounts.add(model);
                            }
                            for (int i = 0; i < listAccounts.size(); i++) {
                                phvAccountAdd
                                        .addView(new AdapterAccount(getActivity(), FragmentInvoiceInfo.this, i, listAccounts.get(i)));
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getWorkOrders() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getInvoiceWorkOrders(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_WorkOrder" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listWorkOrder.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_WorkOrderID));
                                model.setName(dataObject.getString(Cons.KEY_Subject));
                                listWorkOrder.add(model);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getContacts() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getInvoiceContacts(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Contacts" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listContacts.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_ContactID));
                                model.setName(dataObject.getString(Cons.KEY_FullName));
                                listContacts.add(model);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getStatus() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getInvoiceStatus(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Status" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listStatus.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_InvoiceStatusID));
                                model.setName(dataObject.getString(Cons.KEY_InvoiceStatus));
                                listStatus.add(model);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }


    private void getPaymentTerms() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getInvoicePaymentTerms(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Status" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listPaymentTerms.clear();
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_InvoicePaymentTermID));
                                model.setName(dataObject.getString(Cons.KEY_PaymentTerms));
                                listPaymentTerms.add(model);
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getInvoiceDetailed(final String InvoiceId) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_InvoiceID, RequestBody.create(MediaType.parse("text/plain"), InvoiceId));
        Call<ResponseBody> response;
        response = apiInterface.getInvoiceDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_AccInfo" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            JSONObject dataObject = object.getJSONObject("data");
                            strInvoiceNo = dataObject.getString(Cons.KEY_InvoiceNo);
                            strOwner = dataObject.getString(Cons.KEY_AssignedTo);
                            strOwnerName = dataObject.getString(Cons.KEY_AssignedToName);
                            strAccount = dataObject.getString(Cons.KEY_Account);
                            strAccountName = dataObject.getString(Cons.KEY_AccountName);
                            strWorkOrder = dataObject.getString(Cons.KEY_WorkOrder);
                            strWorkOrderName = dataObject.getString(Cons.KEY_WorkOrderSubject);
                            strContact = dataObject.getString(Cons.KEY_Contact);
                            strContactName = dataObject.getString(Cons.KEY_ContactName);
                            strDescription = dataObject.getString(Cons.KEY_Description);
                            strAdditionalInfo = dataObject.getString(Cons.KEY_AdditionalInformation);
                            strStatus = dataObject.getString(Cons.KEY_InvoiceStatusID);
                            strStatusName = dataObject.getString(Cons.KEY_InvoiceStatus);
                            strPaymentTerms = dataObject.getString(Cons.KEY_InvoicePaymentTermID);
                            strPaymentTermsName = dataObject.getString(Cons.KEY_PaymentTerms);
                            strInvoiceDate = dataObject.getString(Cons.KEY_InvoiceDate);
                            strDueDate = dataObject.getString(Cons.KEY_DueDate);
                            strBilAddress = dataObject.getString(Cons.KEY_Address);
                            strBilCity = dataObject.getString(Cons.KEY_City);
                            strBilState = dataObject.getString(Cons.KEY_State);
                            strBilCountry = dataObject.getString(Cons.KEY_Country);
                            strBilPostal = dataObject.getString(Cons.KEY_PostalCode);
                            strBilLat = dataObject.getString(Cons.KEY_Latitude);
                            strBilLng = dataObject.getString(Cons.KEY_Longitude);
                            double subTotal = Double.valueOf(dataObject.getString(Cons.KEY_SubTotal));
                            strSubTotal = String.format("%.2f", subTotal);
                            double discount = Double.valueOf(dataObject.getString(Cons.KEY_Discount));
                            strDiscount = String.format("%.2f", discount);
                            double tax = Double.valueOf(dataObject.getString(Cons.KEY_Tax));
                            strTax = String.format("%.2f", tax);
                            double totalPrice = Double.valueOf(dataObject.getString(Cons.KEY_TotalPrice));
                            strTotalPrice = String.format("%.2f", totalPrice);
                            double grandTotal = Double.valueOf(dataObject.getString(Cons.KEY_GrandTotal));
                            strGrandTotal = String.format("%.2f", grandTotal);
                            strCreatedDate = dataObject.getString(Cons.KEY_CreatedDate);
                            strCreatedBy = dataObject.getString(Cons.KEY_CreatedBy);
                            strLastModifiedDate = dataObject.getString(Cons.KEY_LastModifiedDate);
                            strLastModifiedBy = dataObject.getString(Cons.KEY_LastModifiedBy);
                            strPhoneNo = dataObject.getString(Cons.KEY_PhoneNo);
                            editMode(false, true);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void createInvoice(String owner,
                               String account,
                               String workOrder,
                               String contact,
                               String description,
                               String additionalInfo,
                               String status,
                               String invoiceDate,
                               String paymentTerms,
                               String dueDate,
                               String bilAddress, String bilCity, String bilState, String bilCountry, String bilPostal,
                               String bilLat, String bilLng) {

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AssignedTo, RequestBody.create(MediaType.parse("text/plain"), owner));
        map.put(Cons.KEY_Account, RequestBody.create(MediaType.parse("text/plain"), account));
        map.put(Cons.KEY_WorkOrder, RequestBody.create(MediaType.parse("text/plain"), workOrder));
        map.put(Cons.KEY_Contact, RequestBody.create(MediaType.parse("text/plain"), contact));
        map.put(Cons.KEY_Description, RequestBody.create(MediaType.parse("text/plain"), description));
        if (!TextUtils.isEmpty(additionalInfo)) {
            map.put(Cons.KEY_AdditionalInformation, RequestBody.create(MediaType.parse("text/plain"), additionalInfo));
        }
        map.put(Cons.KEY_InvoiceStatus, RequestBody.create(MediaType.parse("text/plain"), status));
        map.put(Cons.KEY_InvoiceDate, RequestBody.create(MediaType.parse("text/plain"), invoiceDate));
        map.put(Cons.KEY_PaymentTerms, RequestBody.create(MediaType.parse("text/plain"), paymentTerms));
        map.put(Cons.KEY_DueDate, RequestBody.create(MediaType.parse("text/plain"), dueDate));
        map.put(Cons.KEY_Latitude, RequestBody.create(MediaType.parse("text/plain"), bilLat));
        map.put(Cons.KEY_Longitude, RequestBody.create(MediaType.parse("text/plain"), bilLng));
        map.put(Cons.KEY_Address, RequestBody.create(MediaType.parse("text/plain"), bilAddress));
        map.put(Cons.KEY_City, RequestBody.create(MediaType.parse("text/plain"), bilCity));
        map.put(Cons.KEY_State, RequestBody.create(MediaType.parse("text/plain"), bilState));
        map.put(Cons.KEY_Country, RequestBody.create(MediaType.parse("text/plain"), bilCountry));
        map.put(Cons.KEY_PostalCode, RequestBody.create(MediaType.parse("text/plain"), bilPostal));
        Call<ResponseBody> response;
        response = apiInterface.createInvoiceDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Edit" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            isCloneMode = false;
                            String strInvoiceID = object.getString("InvoiceID");
                            invoiceId = strInvoiceID;
                            Utl.showToast(getActivity(), strMessage);
                            getInvoiceDetailed(strInvoiceID);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void editInvoice(final String invoiceId,
                             String owner,
                             String account,
                             String workOrder,
                             String contact,
                             String description,
                             String additionalInfo,
                             String status,
                             String invoiceDate,
                             String paymentTerms,
                             String dueDate,
                             String bilAddress, String bilCity, String bilState, String bilCountry, String bilPostal,
                             String bilLat, String bilLng) {

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_InvoiceID, RequestBody.create(MediaType.parse("text/plain"), invoiceId));
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_AssignedTo, RequestBody.create(MediaType.parse("text/plain"), owner));
        map.put(Cons.KEY_Account, RequestBody.create(MediaType.parse("text/plain"), account));
        map.put(Cons.KEY_WorkOrder, RequestBody.create(MediaType.parse("text/plain"), workOrder));
        map.put(Cons.KEY_Contact, RequestBody.create(MediaType.parse("text/plain"), contact));
        map.put(Cons.KEY_Description, RequestBody.create(MediaType.parse("text/plain"), description));
        if (!TextUtils.isEmpty(additionalInfo)) {
            map.put(Cons.KEY_AdditionalInformation, RequestBody.create(MediaType.parse("text/plain"), additionalInfo));
        }
        map.put(Cons.KEY_InvoiceStatus, RequestBody.create(MediaType.parse("text/plain"), status));
        map.put(Cons.KEY_InvoiceDate, RequestBody.create(MediaType.parse("text/plain"), invoiceDate));
        map.put(Cons.KEY_PaymentTerms, RequestBody.create(MediaType.parse("text/plain"), paymentTerms));
        map.put(Cons.KEY_DueDate, RequestBody.create(MediaType.parse("text/plain"), dueDate));
        map.put(Cons.KEY_Latitude, RequestBody.create(MediaType.parse("text/plain"), bilLat));
        map.put(Cons.KEY_Longitude, RequestBody.create(MediaType.parse("text/plain"), bilLng));
        map.put(Cons.KEY_Address, RequestBody.create(MediaType.parse("text/plain"), bilAddress));
        map.put(Cons.KEY_City, RequestBody.create(MediaType.parse("text/plain"), bilCity));
        map.put(Cons.KEY_State, RequestBody.create(MediaType.parse("text/plain"), bilState));
        map.put(Cons.KEY_Country, RequestBody.create(MediaType.parse("text/plain"), bilCountry));
        map.put(Cons.KEY_PostalCode, RequestBody.create(MediaType.parse("text/plain"), bilPostal));
        Call<ResponseBody> response;
        response = apiInterface.editInvoiceDetailed(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_WO_Edit" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            getInvoiceDetailed(invoiceId);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedInvoiceList(String invoiceId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_InvoiceID, RequestBody.create(MediaType.parse("text/plain"), invoiceId));
        Call<ResponseBody> response = apiInterface.getRelatedInvoiceList(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONObject dataObject = object.getJSONObject("data");
                        JSONObject InvoiceLineObject = dataObject.getJSONObject("InvoiceLineItems");
                        JSONObject EventObject = dataObject.getJSONObject("Event");
                        JSONObject TaskObject = dataObject.getJSONObject("Task");
                        JSONObject FileObject = dataObject.getJSONObject("File");
                        JSONObject NoteObject = dataObject.getJSONObject("Note");
                        listRelatedInvoiceList.add(InvoiceLineObject.getString("title"));
                        listRelatedInvoiceList.add(EventObject.getString("title"));
                        listRelatedInvoiceList.add(FileObject.getString("title"));
                        listRelatedInvoiceList.add(TaskObject.getString("title"));
                        listRelatedInvoiceList.add(NoteObject.getString("title"));
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedInvoiceEvents(String invoiceId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_InvoiceID, RequestBody.create(MediaType.parse("text/plain"), invoiceId));
        Call<ResponseBody> response = apiInterface.getRelatedInvoiceEvent(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelInvoiceEvent model = new ModelInvoiceEvent();
                            model.setEventEndDate(dataObject.getString(Cons.KEY_EventEndDate));
                            model.setEventEndTime(dataObject.getString(Cons.KEY_EventEndTime));
                            model.setEventID(dataObject.getString(Cons.KEY_EventID));
                            model.setEventStartDate(dataObject.getString(Cons.KEY_EventStartDate));
                            model.setEventStartTime(dataObject.getString(Cons.KEY_EventStartTime));
                            model.setEventStatus(dataObject.getString(Cons.KEY_EventStatus));
                            model.setEventTypeName(dataObject.getString(Cons.KEY_EventTypeName));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            listRelatedInvoiceEvents.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedInvoiceFiles(String invoiceId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_InvoiceID, RequestBody.create(MediaType.parse("text/plain"), invoiceId));
        Call<ResponseBody> response = apiInterface.getRelatedInvoiceFile(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelInvoiceFile model = new ModelInvoiceFile();
                            model.setContentType(dataObject.getString(Cons.KEY_ContentType));
                            model.setFileID(dataObject.getString(Cons.KEY_FileID));
                            model.setFileName(dataObject.getString(Cons.KEY_FileName));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            listRelatedInvoiceFiles.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedInvoiceLineItems(String invoiceId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_InvoiceID, RequestBody.create(MediaType.parse("text/plain"), invoiceId));
        Call<ResponseBody> response = apiInterface.getRelatedInvoiceLineItem(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelInvoiceLineItem model = new ModelInvoiceLineItem();
                            model.setInvoiceLineNo(dataObject.getString(Cons.KEY_InvoiceLineNo));
                            model.setProductName(dataObject.getString(Cons.KEY_ProductName));
                            model.setQuantity(dataObject.getString(Cons.KEY_Quantity));
                            model.setUnitPrice(dataObject.getString(Cons.KEY_UnitPrice));
                            model.setSubTotal(dataObject.getString(Cons.KEY_SubTotal));
                            listRelatedInvoiceLineItems.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedInvoiceNotes(String invoiceId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_InvoiceID, RequestBody.create(MediaType.parse("text/plain"), invoiceId));
        Call<ResponseBody> response = apiInterface.getRelatedInvoiceNote(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelInvoiceNote model = new ModelInvoiceNote();
                            model.setNoteID(dataObject.getString(Cons.KEY_NoteID));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            model.setCreatedDate(dataObject.getString(Cons.KEY_CreatedDate));
                            model.setOwnerName(dataObject.getString(Cons.KEY_OwnerName));
                            listRelatedInvoiceNotes.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedInvoiceTasks(String invoiceId) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_InvoiceID, RequestBody.create(MediaType.parse("text/plain"), invoiceId));
        Call<ResponseBody> response = apiInterface.getRelatedInvoiceTask(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelInvoiceTask model = new ModelInvoiceTask();
                            //model.setCallDisposition(dataObject.getString(Cons.KEY_CallDisposition));
                            model.setPriority(dataObject.getString(Cons.KEY_Priority));
                            model.setSubject(dataObject.getString(Cons.KEY_Subject));
                            model.setTaskID(dataObject.getString(Cons.KEY_TaskID));
                            model.setTaskStatus(dataObject.getString(Cons.KEY_TaskStatus));
                            model.setTaskType(dataObject.getString(Cons.KEY_TaskType));
                            listRelatedInvoiceTasks.add(model);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getRelatedInvoiceGetProduct(String invoiceId,
                                             String productFamily, String productCode) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_InvoiceID, RequestBody.create(MediaType.parse("text/plain"), invoiceId));
        map.put(Cons.KEY_ProductFamily, RequestBody.create(MediaType.parse("text/plain"), productFamily));
        map.put(Cons.KEY_ProductCode, RequestBody.create(MediaType.parse("text/plain"), productCode));
        Call<ResponseBody> response = apiInterface.getRelatedInvoiceGetProduct(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            ModelProduct model = new ModelProduct();
                            model.setProductID(dataObject.getString(Cons.KEY_ProductID));
                            model.setProductCode(dataObject.getString(Cons.KEY_ProductCode));
                            model.setProductName(dataObject.getString(Cons.KEY_ProductName));
                            model.setDescription(dataObject.getString(Cons.KEY_Description));
                            model.setListPrice(dataObject.getString(Cons.KEY_ListPrice));
                            model.setDefaultQuantity(dataObject.getString(Cons.KEY_DefaultQuantity));
                            model.setTaxable(dataObject.getString(Cons.KEY_Taxable));
                            model.setIsListPriceEditable(dataObject.getString(Cons.KEY_IsListPriceEditable));
                            model.setIsQuantityEditable(dataObject.getString(Cons.KEY_IsQuantityEditable));
                            model.setTax(dataObject.getString(Cons.KEY_Tax));

                            double listPrice = Double.valueOf(dataObject.getString(Cons.KEY_ListPrice));
                            double discount = 0;
                            double unitPrice = getUnitPrice(Double.valueOf(dataObject.getString(Cons.KEY_ListPrice)), discount);
                            int quantity = Integer.valueOf(dataObject.getString(Cons.KEY_DefaultQuantity));
                            double subtotal = getSubTotal(listPrice, discount, quantity);
                            int isTaxable = Integer.valueOf(dataObject.getString(Cons.KEY_Taxable));
                            double tax = Double.valueOf(dataObject.getString(Cons.KEY_Tax));
                            double netTotal = getNetTotal(listPrice, discount, quantity, isTaxable, tax);
                            model.setDiscount("0");
                            model.setUnitPrice(String.format("%.2f", unitPrice));
                            model.setSubTotal(String.format("%.2f", subtotal));
                            model.setNetTotal(String.format("%.2f", netTotal));

                            listProduct.add(model);
                        }
                        for (int i = 0; i < listProduct.size(); i++) {
                            phvProductAdd
                                    .addView(new AdapterInvoiceProductParent(getActivity(), FragmentInvoiceInfo.this, listProduct.get(i)));
                            phvProductAdd
                                    .addChildView(i, new AdapterInvoiceProductChild(getActivity(), listProduct.get(i)));
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void getProductFamilySpinner() {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        Call<ResponseBody> response = apiInterface.getInvoiceProductFamily(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_ProductFamily" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            listProductFilterFamily.clear();
                            ModelSpinner m = new ModelSpinner();
                            m.setId("");
                            m.setName("None");
                            listProductFilterFamily.add(m);
                            JSONArray dataArray = object.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                ModelSpinner model = new ModelSpinner();
                                model.setId(dataObject.getString(Cons.KEY_ProductFamilyID));
                                model.setName(dataObject.getString(Cons.KEY_ProductFamily));
                                listProductFilterFamily.add(model);
                            }
                            spinnerProductFilterFamily.setAdapter(adapterProductFilterFamily);
                            setFilterSpinnerDropDownHeight(listProductFilterFamily, spinnerProductFilterFamily);
                            spinnerProductFilterFamily.setSelection(0);
                        }
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void addInvoiceLineItem(final int isAddEdit, final boolean isQuickEdit,
                                    final ArrayList<ModelProduct> listProduct) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new LinkedHashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_InvoiceID, RequestBody.create(MediaType.parse("text/plain"), invoiceId));
        for (int i = 0; i < listProduct.size(); i++) {
            map.put("Product[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listProduct.get(i).getProductID()));
            map.put("ListPrice[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listProduct.get(i).getListPrice()));
            map.put("Discount[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listProduct.get(i).getDiscount()));
            map.put("UnitPrice[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listProduct.get(i).getUnitPrice()));
            map.put("Quantity[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listProduct.get(i).getDefaultQuantity()));
            map.put("SubTotal[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listProduct.get(i).getSubTotal()));
            map.put("Taxable[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listProduct.get(i).getTaxable()));
            map.put("TotalPrice[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), listProduct.get(i).getNetTotal()));
        }
        Call<ResponseBody> response = apiInterface.addInvoiceLineItem(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_Filter" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        if (responseCode == 200) {
                            Utl.showToast(getActivity(), strMessage);
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            if (isAddEdit == 0) {
                                listProduct.clear();
                                listProductEditChange.clear();
                                Singleton.getInstance().listInvoiceProduct.clear();
                                phvProductAdd.removeAllViews();
                                edittextProductAddSearch.setText("");
                                getRelatedInvoiceGetProduct(invoiceId, "", "");
                            } else {
                                if (!isQuickEdit) {
                                    listProduct.clear();
                                    listProductEditChange.clear();
                                    Singleton.getInstance().listInvoiceProduct.clear();
                                    layoutProductEditChange.removeAllViews();
                                    layoutProductEdit.setVisibility(View.GONE);
                                }
                            }
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    private void addNewNote(final String subject, final String body) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new LinkedHashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_RelatedTo, RequestBody.create(MediaType.parse("text/plain"), getString(R.string.invoice)));
        map.put(Cons.KEY_What, RequestBody.create(MediaType.parse("text/plain"), invoiceId));
        map.put(Cons.KEY_Subject, RequestBody.create(MediaType.parse("text/plain"), subject));
        map.put(Cons.KEY_Body, RequestBody.create(MediaType.parse("text/plain"), body));
        Call<ResponseBody> response = apiInterface.addNewNote(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_NewNote" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        String strMessage = object.getString("ResponseMsg");
                        int responseCode = object.getInt("ResponseCode");
                        Utl.showToast(getActivity(), strMessage);
                        if (responseCode == 200) {
                        } else if (responseCode == 1) {
                            Utl.showToast(getActivity(), strMessage);
                            layoutNewNote.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

    public boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(getActivity(), CAMERA);
        int result2 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION);
        int result3 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_COARSE_LOCATION);
        int result4 = ContextCompat.checkSelfPermission(getActivity(), CALL_PHONE);
        int result5 = ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE);
        int result6 = ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE);
        return
                result1 == PackageManager.PERMISSION_GRANTED
                        && result2 == PackageManager.PERMISSION_GRANTED
                        && result3 == PackageManager.PERMISSION_GRANTED
                        && result4 == PackageManager.PERMISSION_GRANTED
                        && result5 == PackageManager.PERMISSION_GRANTED
                        && result6 == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{
                        CAMERA,
                        ACCESS_FINE_LOCATION,
                        ACCESS_COARSE_LOCATION,
                        CALL_PHONE,
                        READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean locationFineAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean locationCoarseAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean callAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean readAccepted = grantResults[4] == PackageManager.PERMISSION_GRANTED;
                    boolean writeAccepted = grantResults[5] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted &&
                            locationFineAccepted &&
                            locationCoarseAccepted &&
                            callAccepted &&
                            readAccepted &&
                            writeAccepted)
                        Utl.showToast(getActivity(), "Permission Granted.");
                    else {
                        Utl.showToast(getActivity(), "Permission Denied.");
                    }
                }
                break;
        }
    }

    @Override
    public void callbackAddOwnerListen(int pos, ModelSpinner model) {
        phvOwnerAdd.removeAllViews();
        layoutOwnerAdd.setVisibility(View.GONE);
        strOwnerNew = model.getId();
        strOwnerNameNew = model.getName();
        edittextOwner.setText(strOwnerNameNew);
    }

    @Override
    public void callbackAddAccountListen(int pos, ModelSpinner model) {
        phvAccountAdd.removeAllViews();
        layoutAccountAdd.setVisibility(View.GONE);
        strAccountNew = model.getId();
        strAccountNameNew = model.getName();
        edittextAccount.setText(strAccountNameNew);
    }

    @Override
    public void callbackInvoiceViewAllListen(String str) {
        if (click != null) {
            click.callbackInvoiceViewAllListen(str, invoiceId);
        }
    }

    @Override
    public void callbackInvoiceAddItemListen(String str) {
        if (str.equals(getString(R.string.line_item))) {
            layoutProductAdd.setVisibility(View.VISIBLE);
            getRelatedInvoiceGetProduct(invoiceId, "", "");
        }
    }

    @Override
    public void callbackAddProductInvoiceCheckBoxListen(String productId, boolean b) {
        if (b) {
            for (int i = 0; i < listProduct.size(); i++) {
                String strId = listProduct.get(i).getProductID();
                if (strId.equals(productId)) {
                    Singleton.getInstance().listInvoiceProduct.add(listProduct.get(i));
                }
            }
            for (int j = 0; j < Singleton.getInstance().listInvoiceProduct.size(); j++) {
                Log.d("TAG_", Singleton.getInstance().listInvoiceProduct.get(j).getProductID());
            }
        } else {
            for (int i = 0; i < Singleton.getInstance().listInvoiceProduct.size(); i++) {
                String strId = Singleton.getInstance().listInvoiceProduct.get(i).getProductID();
                if (strId.equals(productId)) {
                    Singleton.getInstance().listInvoiceProduct.remove(Singleton.getInstance().listInvoiceProduct.get(i));
                }
            }
            for (int j = 0; j < Singleton.getInstance().listInvoiceProduct.size(); j++) {
                Log.d("TAG_", Singleton.getInstance().listInvoiceProduct.get(j).getProductID());
            }
        }
    }

    @Override
    public void callbackInvoiceRelatedDetails(String whoName, String whatID) {
        if (whoName.equals(getString(R.string.event))) {
            getEventDetails(whatID);
        } else if (whoName.equals(getString(R.string.note))) {
            getNoteDetails(whatID);
        }
    }

    private void getEventDetails(String whatID) {

    }

    private void getNoteDetails(String whatID) {
        Map<String, String> header = new HashMap<>();
        header.put("token", Utl.getSPStr(getActivity(), Cons.KEY_TOKEN));
        final Map<String, RequestBody> map = new HashMap<>();
        map.put(Cons.KEY_UserID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_UserID)));
        map.put(Cons.KEY_ORGANIZATION_ID, RequestBody.create(MediaType.parse("text/plain"), Utl.getSPStr(getActivity(), Cons.KEY_ORGANIZATION_ID)));
        map.put(Cons.KEY_NoteID, RequestBody.create(MediaType.parse("text/plain"), whatID));
        Call<ResponseBody> response = apiInterface.getNoteDetails(header, map);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string().toString();
                        Log.d("TAG_" + "_Suc", response.body().string().toString());
                        JSONObject object = new JSONObject(strResponse);
                        JSONObject dataObject = object.getJSONObject("data");
                        String owner = dataObject.getString(Cons.KEY_OwnerName);
                        String relatedTo = dataObject.getString(Cons.KEY_RelatedTo);
                        String subject = dataObject.getString(Cons.KEY_Subject);
                        String body = dataObject.getString(Cons.KEY_Body);
                        String createdDate = dataObject.getString(Cons.KEY_CreatedByName);
                        String createdBy = dataObject.getString(Cons.KEY_CreatedDate);
                        String lastModifiedBy = dataObject.getString(Cons.KEY_LastModifiedByName);
                        String lateModifiedDate = dataObject.getString(Cons.KEY_LastModifiedDate);
                        final Dialog dialogDetails = new Dialog(getActivity(), R.style.DialogFullScreen);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        lp.copyFrom(dialogDetails.getWindow().getAttributes());
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.gravity = Gravity.BOTTOM;
                        dialogDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialogDetails.setCancelable(true);
                        dialogDetails.setCanceledOnTouchOutside(true);
                        dialogDetails.setContentView(R.layout.dialog_note_details);
                        TextView textviewOwner = (TextView) dialogDetails.findViewById(R.id.textview_owner);
                        TextView textviewRelatedTo = (TextView) dialogDetails.findViewById(R.id.textview_related_to);
                        TextView textviewSubject = (TextView) dialogDetails.findViewById(R.id.textview_subject);
                        TextView textviewBody = (TextView) dialogDetails.findViewById(R.id.textview_body);
                        TextView textviewCreatedDate = (TextView) dialogDetails.findViewById(R.id.textview_created_date);
                        TextView textviewCreatedBy = (TextView) dialogDetails.findViewById(R.id.textview_created_by);
                        TextView textviewLastModifiedDate = (TextView) dialogDetails.findViewById(R.id.textview_last_modified_date);
                        TextView textviewLastModifiedBy = (TextView) dialogDetails.findViewById(R.id.textview_last_modified_by);
                        Button buttonClose = (Button) dialogDetails.findViewById(R.id.button_details_close);
                        textviewOwner.setText(owner);
                        textviewRelatedTo.setText(relatedTo);
                        textviewSubject.setText(subject);
                        textviewBody.setText(body);
                        textviewCreatedBy.setText(createdBy);
                        textviewCreatedDate.setText(createdDate);
                        textviewLastModifiedBy.setText(lastModifiedBy);
                        textviewLastModifiedDate.setText(lateModifiedDate);
                        buttonClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogDetails.dismiss();
                            }
                        });

                        dialogDetails.show();
                        dialogDetails.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                        dialogDetails.getWindow().setDimAmount(0.5f);
                        dialogDetails.getWindow().setAttributes(lp);
                    } catch (JSONException e) {
                        Log.d("TAG_err_1", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    } catch (IOException e) {
                        Log.d("TAG_err_2", "" + e.getMessage());
                        Utl.showToast(getActivity(), getString(R.string.server_error));
                    }
                } else {
                    Log.d("TAG_err_3", "null");
                    Utl.showToast(getActivity(), getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG_err_4", "" + t.getMessage());
                Utl.showToast(getActivity(), getString(R.string.server_error));
            }
        });
    }

}