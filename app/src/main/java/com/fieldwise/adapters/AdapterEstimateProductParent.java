package com.fieldwise.adapters;

import android.content.Context;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.models.ModelProduct;
import com.fieldwise.utils.Singleton;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.expand.Collapse;
import com.mindorks.placeholderview.annotations.expand.Expand;
import com.mindorks.placeholderview.annotations.expand.Parent;
import com.mindorks.placeholderview.annotations.expand.ParentPosition;
import com.mindorks.placeholderview.annotations.expand.SingleTop;
import com.mindorks.placeholderview.annotations.expand.Toggle;

@Parent
@SingleTop
@Layout(R.layout.item_product_estimate_parent)
public class AdapterEstimateProductParent {

    @View(R.id.checkbox_product_id)
    CheckBox checkboxProductId;

    @View(R.id.textview_name)
    TextView textviewName;

    @View(R.id.textview_list_price)
    TextView textviewListPrice;

    @View(R.id.textview_description)
    TextView textviewDescription;

    @View(R.id.toggle_icon)
    ImageView toggleIcon;

    @Toggle(R.id.toggle_view)
    LinearLayout toggleView;

    @ParentPosition
    int parentPosition;

    private Context ctx;
    private ModelProduct mdl;

    public AddProductEstimateCheckBoxListen click;

    public interface AddProductEstimateCheckBoxListen {
        void callbackAddProductEstimateCheckBoxListen(String productId, boolean b);
    }

    public AdapterEstimateProductParent(Context c, AddProductEstimateCheckBoxListen click, ModelProduct m) {
        this.ctx = c;
        this.click = (AddProductEstimateCheckBoxListen) click;
        this.mdl = m;
    }

    @Resolve
    public void onResolved() {
        toggleIcon.setImageDrawable(ctx.getResources().getDrawable(R.drawable.bg_open));
        textviewName.setText("Product: " + mdl.getProductName());
        textviewListPrice.setText("List Price: $" + String.format("%.2f", Double.valueOf(mdl.getListPrice())));
        textviewDescription.setText("Description: " + mdl.getDescription());

        for (int i = 0; i < Singleton.getInstance().listEstimateProduct.size(); i++) {
            String strId = Singleton.getInstance().listEstimateProduct.get(i).getProductID();
            if (strId.equals(mdl.getProductID())) {
                checkboxProductId.setChecked(true);
            }
        }

        checkboxProductId.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (click != null) {
                    click.callbackAddProductEstimateCheckBoxListen(mdl.getProductID(), b);
                }
            }
        });
    }

    @Expand
    public void onExpand() {
        toggleIcon.setImageDrawable(ctx.getResources().getDrawable(R.drawable.bg_down));
        textviewListPrice.setVisibility(android.view.View.GONE);
        textviewDescription.setVisibility(android.view.View.GONE);
    }

    @Collapse
    public void onCollapse() {
        toggleIcon.setImageDrawable(ctx.getResources().getDrawable(R.drawable.bg_open));
        textviewListPrice.setVisibility(android.view.View.VISIBLE);
        textviewDescription.setVisibility(android.view.View.VISIBLE);
    }

}