package com.fieldwise.adapters;

import android.content.Context;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.models.ModelSpinner;
import com.fieldwise.utils.Singleton;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

@Layout(R.layout.item_calendar_assigned_to)
public class AdapterCalendarAssignedTo {

    public CalendarAssignedToCheckBoxListen click;

    public interface CalendarAssignedToCheckBoxListen {
        void callbackCalendarAssignedToCheckBoxListen(boolean isChecked, ModelSpinner model);
    }

    @View(R.id.checkbox_id)
    CheckBox checkboxId;

    @View(R.id.textview_name)
    TextView textviewName;

    private Context ctx;
    private ModelSpinner mdl;

    public AdapterCalendarAssignedTo(Context c, CalendarAssignedToCheckBoxListen click, ModelSpinner m) {
        this.ctx = c;
        this.click = (CalendarAssignedToCheckBoxListen) click;
        this.mdl = m;
    }

    @Resolve
    public void onResolved() {
        if (Singleton.getInstance().listCalendarAssignedToID.contains(mdl.getId())) {
            checkboxId.setChecked(true);
        } else {
            checkboxId.setChecked(false);
        }
        checkboxId.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (click != null) {
                    click.callbackCalendarAssignedToCheckBoxListen(isChecked, mdl);
                }
            }
        });
        textviewName.setText(mdl.getName());
    }

}