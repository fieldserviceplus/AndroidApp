package com.fieldwise.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.models.ModelHomeSchedule;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

@Layout(R.layout.item_home_schedule)
public class AdapterSchedule {

    @View(R.id.card_view)
    CardView cardView;

    @View(R.id.textview_srid)
    TextView textViewSrId;

    @View(R.id.textview_time)
    TextView textViewTime;

    @View(R.id.textview_type)
    TextView textViewType;

    @View(R.id.textview_location)
    TextView textViewLocation;

    private Context ctx;
    private ModelHomeSchedule model;

    public AdapterSchedule(Context ctx, ModelHomeSchedule model) {
        this.ctx = ctx;
        this.model = model;
    }

    @Resolve
    public void onResolved() {
        textViewSrId.setText("Related Object: "+model.getRelatedObjNo());
        textViewTime.setText("Time: "+model.getTime());
        textViewType.setText("Subject: "+model.getSubject());
        textViewLocation.setText("Address: "+model.getAddress());
    }

    @Click(R.id.card_view)
    public void onViewClick() {
    }

}