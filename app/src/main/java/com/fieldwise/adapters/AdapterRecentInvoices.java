package com.fieldwise.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.models.ModelDynamic;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

import java.util.ArrayList;
import java.util.List;

@Layout(R.layout.item_recent_invoices)
public class AdapterRecentInvoices {

    public ClickListen click;

    public interface ClickListen {
        void callbackInvoice(ModelDynamic model);
    }

    @View(R.id.card_view)
    CardView cardView;

    @View(R.id.layout_map)
    LinearLayout layoutMap;

    @View(R.id.textview)
    TextView textView;

    private Context ctx;
    private ModelDynamic model;

    public AdapterRecentInvoices(Context ctx, ModelDynamic model) {
        this.ctx = ctx;
        this.model = model;
    }

    public AdapterRecentInvoices(Context ctx, ClickListen click, ModelDynamic model) {
        this.ctx = ctx;
        this.click = (ClickListen) click;
        this.model = model;
    }

    @Resolve
    public void onResolved() {

        List<String> entry = new ArrayList<>(model.getMap().keySet());
        StringBuilder str = new StringBuilder().append("");
        for (int i = 0; i < entry.size(); i++) {
            String key = entry.get(i);
            String cleanKey;
            if (key.equals("IN#")) {
                cleanKey = key;
            } else if (key.equals("InvoiceNo")) {
                cleanKey = "IN#";
            } else {
                cleanKey = key.replaceAll("\\d+", "").replaceAll("(.)([A-Z])", "$1 $2");
            }
            if (!cleanKey.contains("ID")) {
                if (i == (entry.size() - 1)) {
                    str.append(cleanKey + ": " + model.getMap().get(entry.get(i)));
                } else {
                    str.append(cleanKey + ": " + model.getMap().get(entry.get(i)) + "\n");
                }
            }
            //String value = entry.getValue();
            /*
            MyTextViewR textView = new MyTextViewR(ctx);
            textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            textView.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            textView.setLines(1);
            textView.setTextAppearance(ctx, R.style.MyTextView);
            textView.setText(cleanKey + ": ");
            layoutMap.addView(textView);
            */
        }
        textView.setText(str);

        /*
        for (Map.Entry<String, String> entry : model.getMap().entrySet()) {
            String key = entry.getKey();
            String cleanKey = key.replaceAll("\\d+", "").replaceAll("(.)([A-Z])", "$1 $2");
            String value = entry.getValue();
            if (!cleanKey.contains("ID")) {
                Log.d("TAG_" + cleanKey + ": ", "" + value);
                MyTextViewR textView = new MyTextViewR(ctx);
                textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                textView.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                textView.setLines(1);
                textView.setTextAppearance(ctx, R.style.MyTextView);
                textView.setText(cleanKey + ": " + value);
                layoutMap.addView(textView);
            }
        }
        */
    }

    @Click(R.id.card_view)
    public void onViewClick() {
        if (click != null) {
            click.callbackInvoice(model);
        }
    }

}