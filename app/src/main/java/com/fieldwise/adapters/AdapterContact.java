package com.fieldwise.adapters;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.models.ModelSpinner;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

@Layout(R.layout.item_contact)
public class AdapterContact {

    public AddContactClickListen click;

    public interface AddContactClickListen {
        void callbackAddContactListen(int pos, ModelSpinner model);
    }

    @View(R.id.textview_name)
    TextView textviewName;

    @View(R.id.toggle_icon)
    ImageView toggleIcon;

    private Context ctx;
    private int pos;
    private ModelSpinner mdl;

    public AdapterContact(Context c, AddContactClickListen click, int pos, ModelSpinner m) {
        this.ctx = c;
        this.pos = pos;
        this.click = (AddContactClickListen) click;
        this.mdl = m;
    }

    @Resolve
    public void onResolved() {
        toggleIcon.setImageDrawable(ctx.getResources().getDrawable(R.drawable.bg_open));
        textviewName.setText("Contact: " + mdl.getName());
    }

    @Click(R.id.toggle_icon)
    public void onViewClick() {
        if (click != null) {
            click.callbackAddContactListen(pos, mdl);
        }
    }

}