package com.fieldwise.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.models.ModelHomeRecent;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Layout(R.layout.item_home_recent)
public class AdapterRecent {

    @View(R.id.card_view)
    CardView cardView;

    @View(R.id.textview_date)
    TextView textViewDate;

    @View(R.id.textview_name)
    TextView textViewName;

    private Context ctx;
    private ModelHomeRecent model;

    public ClickListenHomeItem clickHomeItem;

    public interface ClickListenHomeItem {
        void callbackHomeItem(String object, String id, String name);
    }

    public AdapterRecent(Context ctx, ClickListenHomeItem clickHomeItem, ModelHomeRecent model) {
        this.ctx = ctx;
        this.clickHomeItem = clickHomeItem;
        this.model = model;
    }

    @Resolve
    public void onResolved() {
        textViewDate.setText(formatDate(model.getDate()));
        textViewName.setText("Name: "+ model.getName());
    }

    @Click(R.id.card_view)
    public void onViewClick() {
        if (clickHomeItem != null) {
            clickHomeItem.callbackHomeItem(model.getObject(), model.getID(), model.getName());
        }
    }

    public String formatDate(String time) {
        String inputPattern = "yyyy-MM-dd kk:mm:ss";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}