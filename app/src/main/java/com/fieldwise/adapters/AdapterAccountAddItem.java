package com.fieldwise.adapters;

import android.content.Context;
import android.widget.TextView;

import com.fieldwise.R;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.expand.ChildPosition;
import com.mindorks.placeholderview.annotations.expand.ParentPosition;

@Layout(R.layout.item_account_add_item)
public class AdapterAccountAddItem {

    public AddItemListen click;

    public interface AddItemListen {
        void callbackAddItemListen(String str);
    }

    @ParentPosition
    int parentPosition;

    @ChildPosition
    int childPosition;

    @View(R.id.textview_add_item)
    TextView textViewAddItem;

    Context ctx;
    String str;

    public AdapterAccountAddItem(Context ctx, AddItemListen click, String str) {
        this.ctx = ctx;
        this.click = (AddItemListen) click;
        this.str = str;
    }

    @Resolve
    public void onResolved() {
        textViewAddItem.setText("Add " + str);
    }

    @Click(R.id.textview_add_item)
    public void onViewClick() {
        if (click != null) {
            click.callbackAddItemListen(str);
        }
    }

}