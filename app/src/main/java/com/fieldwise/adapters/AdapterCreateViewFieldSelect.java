package com.fieldwise.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.widget.ImageView;
import android.widget.TextView;

import com.fieldwise.R;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Position;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

@Layout(R.layout.item_create_view_field_select)
public class AdapterCreateViewFieldSelect {

    public ClickListen click;

    public interface ClickListen {
        void callbackSelectField(int pos, String field);
    }

    @View(R.id.card_view)
    CardView cardView;

    @View(R.id.textview_name)
    TextView textViewName;

    @View(R.id.imageview_add)
    ImageView imageViewAdd;

    @Position
    int pos;
    private Context ctx;
    private String field;

    public AdapterCreateViewFieldSelect(Context ctx, ClickListen click, int pos, String field) {
        this.ctx = ctx;
        this.click = (ClickListen) click;
        this.pos = pos;
        this.field = field;
    }

    @Resolve
    public void onResolved() {
        textViewName.setText(field);
    }

    @Click(R.id.card_view)
    public void onViewClick() {
        if (click != null) {
            click.callbackSelectField(pos, field);
        }
    }

}