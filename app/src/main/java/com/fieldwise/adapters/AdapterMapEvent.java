package com.fieldwise.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.models.ModelMap;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Layout(R.layout.item_map_event)
public class AdapterMapEvent {

    @View(R.id.card_view)
    CardView cardView;

    @View(R.id.lay_lin_clr)
    RelativeLayout layLinClr;

    @View(R.id.textview_date)
    TextView textviewDate;

    @View(R.id.textview_time)
    TextView textviewTime;

    @View(R.id.layout_event)
    LinearLayout layoutEvent;

    @View(R.id.textview_title)
    TextView textviewTitle;

    @View(R.id.imageview_shape)
    ImageView imageviewShape;

    private Context ctx;
    private Date calendarDate;
    private ModelMap model;

    public MapEventClickListen click;

    public interface MapEventClickListen {
        void callbackMapEvent(ModelMap model);
    }

    public AdapterMapEvent(Context ctx, MapEventClickListen click, Date date, ModelMap model) {
        this.ctx = ctx;
        this.click = click;
        this.calendarDate = date;
        this.model = model;
    }

    @Resolve
    public void onResolved() {

        layLinClr.setBackgroundColor(Color.parseColor(model.getColorCode()));

        String startDay = getDay(model.getStartDate());
        String endDay = getDay(model.getEndDate());
        if (!startDay.equals(endDay)) {
            textviewDate.setText(startDay + " - " + endDay);
        } else {
            textviewDate.setText(startDay);
        }

        if (!model.getTime().equals("All-Day")) {
            String startTime = getTime(model.getStartTime());
            String endTime = getTime(model.getEndTime());
            textviewTime.setText(startTime + " - " + endTime);
        } else {
            textviewTime.setText("All-Day");
        }

        textviewTitle.setText(model.getTitle());

        if (model.getShapeType().equals("SQUARE")) {
            imageviewShape.setImageDrawable(ContextCompat.getDrawable(ctx, R.drawable.ic_map_sqr));
            imageviewShape.setColorFilter(new PorterDuffColorFilter(Color.parseColor(model.getColorCode()), PorterDuff.Mode.SRC_IN));
        }
        if (model.getShapeType().equals("CIRCLE")) {
            imageviewShape.setImageDrawable(ContextCompat.getDrawable(ctx, R.drawable.ic_map_cir));
            imageviewShape.setColorFilter(new PorterDuffColorFilter(Color.parseColor(model.getColorCode()), PorterDuff.Mode.SRC_IN));
        }
        if (model.getShapeType().equals("STAR")) {
            imageviewShape.setImageDrawable(ContextCompat.getDrawable(ctx, R.drawable.ic_map_str));
            imageviewShape.setColorFilter(new PorterDuffColorFilter(Color.parseColor(model.getColorCode()), PorterDuff.Mode.SRC_IN));
        }
    }

    @Click(R.id.card_view)
    public void onClick() {
        if (click != null) {
            click.callbackMapEvent(model);
        }
    }

    public String getTime(String strTime) {
        String inputPattern = "HH:mm";
        String outputPattern = "hh:mmaa";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(strTime);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String getDay(String strDate) {
        boolean isDateFromCurrentYear = getYearOfDate(strDate);
        boolean isDateFromCurrentMonth = getMonthOfDate(strDate);
        String inputPattern = "dd-MM-yyyy";
        String outputPattern = "dd";
        if (!isDateFromCurrentYear) {
            outputPattern = "ddMMMyy";
        } else if (isDateFromCurrentYear && !isDateFromCurrentMonth) {
            outputPattern = "ddMMM";
        } else {
            outputPattern = "dd";
        }
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(strDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private boolean getYearOfDate(String strDate) {
        String inputPattern = "dd-MM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        boolean isTrue = true;
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        Date date = null;
        try {
            date = inputFormat.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        cal1.setTime(date);
        cal2.setTime(calendarDate);
        cal2.add(Calendar.MONTH, -1);
        if (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)) {
            isTrue = true;
        } else {
            isTrue = false;
        }
        return isTrue;
    }

    private boolean getMonthOfDate(String strDate) {
        String inputPattern = "dd-MM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        boolean isTrue = true;
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        Date date = null;
        try {
            date = inputFormat.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        cal1.setTime(date);
        cal2.setTime(calendarDate);
        cal2.add(Calendar.MONTH, -1);
        if (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)) {
            if (cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH)) {
                isTrue = true;
            } else {
                isTrue = false;
            }
        }
        return isTrue;
    }

}