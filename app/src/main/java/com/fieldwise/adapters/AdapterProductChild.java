package com.fieldwise.adapters;

import android.content.Context;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.models.ModelProduct;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.expand.ChildPosition;
import com.mindorks.placeholderview.annotations.expand.ParentPosition;

@Layout(R.layout.item_product_child)
public class AdapterProductChild {

    @ParentPosition
    int parentPosition;

    @ChildPosition
    int childPosition;

    @View(R.id.textview_code)
    TextView textviewCode;

    @View(R.id.textview_list_price)
    TextView textviewListPrice;

    @View(R.id.textview_description)
    TextView textviewDescription;

    Context context;
    ModelProduct mdl;

    public AdapterProductChild(Context context, ModelProduct m) {
        this.context = context;
        this.mdl = m;
    }

    @Resolve
    public void onResolved() {
        textviewCode.setText("Code: " + mdl.getProductCode());
        textviewListPrice.setText("List Price: $" + String.format("%.2f", Double.valueOf(mdl.getListPrice())));
        textviewDescription.setText("Description: " + mdl.getDescription());
    }

}