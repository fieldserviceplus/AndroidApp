package com.fieldwise.adapters;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.models.ModelChemical;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

@Layout(R.layout.item_chemical)
public class AdapterChemical {

    public AddChemicalClickListen click;

    public interface AddChemicalClickListen {
        void callbackAddChemicalListen(String pos, ModelChemical model);
    }

    @View(R.id.textview_name)
    TextView textviewName;

    @View(R.id.textview_description)
    TextView textviewDescription;

    @View(R.id.toggle_icon)
    ImageView toggleIcon;

    private Context ctx;
    private String pos;
    private ModelChemical mdl;

    public AdapterChemical(Context c, AddChemicalClickListen click, String pos, ModelChemical m) {
        this.ctx = c;
        this.pos = pos;
        this.click = (AddChemicalClickListen) click;
        this.mdl = m;
    }

    @Resolve
    public void onResolved() {
        toggleIcon.setImageDrawable(ctx.getResources().getDrawable(R.drawable.bg_open));
        textviewName.setText("Chemical: " + mdl.getProductName());
        textviewDescription.setText("Description: " + mdl.getDescription());
    }

    @Click(R.id.toggle_icon)
    public void onViewClick() {
        if (click != null) {
            click.callbackAddChemicalListen(pos, mdl);
        }
    }

}