package com.fieldwise.adapters;

import android.content.Context;
import android.widget.TextView;

import com.fieldwise.R;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.expand.ChildPosition;
import com.mindorks.placeholderview.annotations.expand.ParentPosition;

@Layout(R.layout.item_contact_related_child)
public class AdapterContactRelatedChild {

    @ParentPosition
    int parentPosition;

    @ChildPosition
    int childPosition;

    @View(R.id.textview_info)
    TextView textViewInfo;

    public ClickListen click;

    public interface ClickListen {
        void callbackContactRelatedWorkOrderOpen(String workOrderNo, String workOrderID);
    }

    public ClickListenNote clickNote;

    public interface ClickListenNote {
        void callbackContactRelatedDetails(String whoName, String whatID);
    }

    String info, workOrderID, workOrderNo;
    Context context;

    public AdapterContactRelatedChild(Context context, String info) {
        this.context = context;
        this.info = info;
    }

    public AdapterContactRelatedChild(Context context, ClickListen click, String workOrderNo, String workOrderID, String info) {
        this.context = context;
        if (workOrderNo.equals(context.getString(R.string.event)) || workOrderNo.equals(context.getString(R.string.note))) {
            this.clickNote = (ClickListenNote) click;
        } else {
            this.click = (ClickListen) click;
        }
        this.workOrderNo = workOrderNo;
        this.workOrderID = workOrderID;
        this.info = info;
    }

    @Resolve
    public void onResolved() {
        textViewInfo.setText(info);
    }

    @Click(R.id.textview_info)
    public void onViewClick() {
        if (workOrderNo.equals(context.getString(R.string.event)) || workOrderNo.equals(context.getString(R.string.note))) {
            if (clickNote != null) {
                clickNote.callbackContactRelatedDetails(workOrderNo, workOrderID);
            }
        } else {
            if (click != null) {
                click.callbackContactRelatedWorkOrderOpen(workOrderNo, workOrderID);
            }
        }
    }

}