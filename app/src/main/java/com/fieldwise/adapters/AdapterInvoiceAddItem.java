package com.fieldwise.adapters;

import android.content.Context;
import android.widget.TextView;

import com.fieldwise.R;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.expand.ChildPosition;
import com.mindorks.placeholderview.annotations.expand.ParentPosition;

@Layout(R.layout.item_invoice_add_item)
public class AdapterInvoiceAddItem {

    public InvoiceAddItemListen click;

    public interface InvoiceAddItemListen {
        void callbackInvoiceAddItemListen(String str);
    }

    @ParentPosition
    int parentPosition;

    @ChildPosition
    int childPosition;

    @View(R.id.textview_add_item)
    TextView textViewAddItem;

    Context ctx;
    String str;

    public AdapterInvoiceAddItem(Context ctx, InvoiceAddItemListen click, String str) {
        this.ctx = ctx;
        this.click = (InvoiceAddItemListen) click;
        this.str = str;
    }

    @Resolve
    public void onResolved() {
        textViewAddItem.setText("Add " + str);
    }

    @Click(R.id.textview_add_item)
    public void onViewClick() {
        if (click != null) {
            click.callbackInvoiceAddItemListen(str);
        }
    }

}