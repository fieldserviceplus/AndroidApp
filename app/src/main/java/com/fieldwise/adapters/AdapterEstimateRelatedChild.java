package com.fieldwise.adapters;

import android.content.Context;
import android.widget.TextView;

import com.fieldwise.R;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.expand.ChildPosition;
import com.mindorks.placeholderview.annotations.expand.ParentPosition;

@Layout(R.layout.item_estimate_related_child)
public class AdapterEstimateRelatedChild {

    @ParentPosition
    int parentPosition;

    @ChildPosition
    int childPosition;

    @View(R.id.textview_info)
    TextView textViewInfo;

    public ClickListenNote click;

    public interface ClickListenNote {
        void callbackEstimateRelatedDetails(String whoName, String whatID);
    }

    String info, whoName, whatID;
    Context context;

    public AdapterEstimateRelatedChild(Context context, String info) {
        this.context = context;
        this.info = info;
    }

    public AdapterEstimateRelatedChild(Context context, ClickListenNote click, String whoName, String whatID, String info) {
        this.context = context;
        this.click = (ClickListenNote) click;
        this.whoName = whoName;
        this.whatID = whatID;
        this.info = info;
    }

    @Resolve
    public void onResolved() {
        textViewInfo.setText(info);
    }

    @Click(R.id.textview_info)
    public void onViewClick() {
        if (whatID != null && click != null) {
            click.callbackEstimateRelatedDetails(whoName, whatID);
        }
    }

}