package com.fieldwise.adapters;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fieldwise.R;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.expand.Collapse;
import com.mindorks.placeholderview.annotations.expand.Expand;
import com.mindorks.placeholderview.annotations.expand.Parent;
import com.mindorks.placeholderview.annotations.expand.ParentPosition;
import com.mindorks.placeholderview.annotations.expand.SingleTop;
import com.mindorks.placeholderview.annotations.expand.Toggle;

@Parent
@SingleTop
@Layout(R.layout.item_invoice_related_parent)
public class AdapterInvoiceRelatedParent {

    @View(R.id.textview_heading)
    TextView headingTxt;

    @View(R.id.toggle_icon)
    ImageView toggleIcon;

    @Toggle(R.id.toggle_view)
    LinearLayout toggleView;

    @ParentPosition
    int parentPosition;

    private Context ctx;
    private String heading;

    public AdapterInvoiceRelatedParent(Context c, String s) {
        ctx = c;
        heading = s;
    }

    @Resolve
    public void onResolved() {
        toggleIcon.setImageDrawable(ctx.getResources().getDrawable(R.drawable.bg_open));
        headingTxt.setText(heading);
    }

    @Expand
    public void onExpand() {
        toggleIcon.setImageDrawable(ctx.getResources().getDrawable(R.drawable.bg_down));
    }

    @Collapse
    public void onCollapse() {
        toggleIcon.setImageDrawable(ctx.getResources().getDrawable(R.drawable.bg_open));
    }

}