package com.fieldwise.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.models.ModelDynamic;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

import java.util.ArrayList;
import java.util.List;

@Layout(R.layout.item_recent_work_orders)
public class AdapterRecentWorkOrders {

    public ClickListen click;

    public interface ClickListen {
        void callbackWorkOrder(ModelDynamic model);
    }

    @View(R.id.card_view)
    CardView cardView;

    @View(R.id.layout_map)
    LinearLayout layoutMap;

    @View(R.id.textview)
    TextView textView;

    private Context ctx;
    private ModelDynamic model;

    public AdapterRecentWorkOrders(Context ctx, ClickListen click, ModelDynamic model) {
        this.ctx = ctx;
        this.click = (ClickListen) click;
        this.model = model;
    }

    @Resolve
    public void onResolved() {
        List<String> entry = new ArrayList<>(model.getMap().keySet());
        StringBuilder str = new StringBuilder().append("");
        for (int i = 0; i < entry.size(); i++) {
            String key = entry.get(i);
            String cleanKey;
            if (key.equals("WO#")) {
                cleanKey = key;
            } else if (key.equals("WorkOrderNo")) {
                cleanKey = "WO#";
            } else {
                cleanKey = key.replaceAll("\\d+", "").replaceAll("(.)([A-Z])", "$1 $2");
            }
            if (!cleanKey.contains("ID")) {
                if (i == (entry.size() - 1)) {
                    str.append(cleanKey + ": " + model.getMap().get(entry.get(i)));
                } else {
                    str.append(cleanKey + ": " + model.getMap().get(entry.get(i)) + "\n");
                }
            }
        }
        textView.setText(str);
    }

    @Click(R.id.card_view)
    public void onViewClick() {
        if (click != null) {
            click.callbackWorkOrder(model);
        }
    }

}