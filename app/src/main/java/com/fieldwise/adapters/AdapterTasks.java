package com.fieldwise.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.models.ModelTasks;
import com.fieldwise.utils.Utl;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

@Layout(R.layout.item_tasks)
public class AdapterTasks {

    @View(R.id.card_view)
    CardView cardView;

    @View(R.id.textview_subject)
    TextView textViewSubject;

    @View(R.id.textview_details)
    TextView textViewDetails;

    @View(R.id.textview_date)
    TextView textViewDate;

    private Context ctx;
    private ModelTasks model;

    public AdapterTasks(Context ctx, ModelTasks model) {
        this.ctx = ctx;
        this.model = model;
    }

    @Resolve
    public void onResolved() {
        textViewSubject.setText(model.getSubject());
        textViewDetails.setText(model.getDetails());
        textViewDate.setText(model.getDate());
    }

    @Click(R.id.card_view)
    public void onViewClick() {
        Utl.showToast(ctx, "" + model.getId());
    }

}