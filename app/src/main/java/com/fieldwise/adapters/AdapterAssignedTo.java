package com.fieldwise.adapters;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.fieldwise.R;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

import com.fieldwise.models.ModelSpinner;

@Layout(R.layout.item_assigned_to)
public class AdapterAssignedTo {

    public AddAssignedToClickListen click;

    public interface AddAssignedToClickListen {
        void callbackAddAssignedToListen(int pos, ModelSpinner model);
    }

    @View(R.id.textview_name)
    TextView textviewName;

    @View(R.id.toggle_icon)
    ImageView toggleIcon;

    private Context ctx;
    private int pos;
    private ModelSpinner mdl;

    public AdapterAssignedTo(Context c, AddAssignedToClickListen click, int pos, ModelSpinner m) {
        this.ctx = c;
        this.pos = pos;
        this.click = (AddAssignedToClickListen) click;
        this.mdl = m;
    }

    @Resolve
    public void onResolved() {
        toggleIcon.setImageDrawable(ctx.getResources().getDrawable(R.drawable.bg_open));
        textviewName.setText("AssignTo: " + mdl.getName());
    }

    @Click(R.id.toggle_icon)
    public void onViewClick() {
        if (click != null) {
            click.callbackAddAssignedToListen(pos, mdl);
        }
    }

}