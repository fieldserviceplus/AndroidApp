package com.fieldwise.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.models.ModelHomeTask;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Layout(R.layout.item_home_task)
public class AdapterHomeTask {

    @View(R.id.card_view)
    CardView cardView;

    @View(R.id.textview_date)
    TextView textViewDate;

    @View(R.id.textview_subject)
    TextView textViewSubject;

    @View(R.id.textview_related_to)
    TextView textViewRelatedTo;

    private Context ctx;
    private ModelHomeTask model;

    public AdapterHomeTask(Context ctx, ModelHomeTask model) {
        this.ctx = ctx;
        this.model = model;
    }

    @Resolve
    public void onResolved() {
        textViewDate.setText(formatDate(model.getDate()));
        textViewSubject.setText("Subject: " + model.getSubject());
        textViewRelatedTo.setText("Related Object: " + model.getRelatedObjNo());
    }

    @Click(R.id.card_view)
    public void onViewClick() {
    }

    public String formatDate(String time) {
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}