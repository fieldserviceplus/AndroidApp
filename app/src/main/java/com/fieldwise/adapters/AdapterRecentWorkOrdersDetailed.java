package com.fieldwise.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.models.ModelRecentWorkOrdersDetailed;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

@Layout(R.layout.item_recent_work_orders_detailed)
public class AdapterRecentWorkOrdersDetailed {

    public ClickListenDetailed clickDetailed;
    public interface ClickListenDetailed {
        void callbackWorkOrderDetailed(ModelRecentWorkOrdersDetailed model);
    }
    @View(R.id.card_view)
    CardView cardView;

    @View(R.id.textview_no)
    TextView textViewNo;

    @View(R.id.textview_subject)
    TextView textViewSubject;

    @View(R.id.textview_priority)
    TextView textViewPriority;

    @View(R.id.textview_status)
    TextView textViewStatus;

    private Context ctx;
    private ModelRecentWorkOrdersDetailed model;

    public AdapterRecentWorkOrdersDetailed(Context ctx, ClickListenDetailed click, ModelRecentWorkOrdersDetailed model) {
        this.ctx = ctx;
        this.clickDetailed = (ClickListenDetailed) click;
        this.model = model;
    }

    @Resolve
    public void onResolved() {
        textViewNo.setText(model.getWorkOrderNo());
        textViewSubject.setText(model.getSubject());
        textViewPriority.setText(model.getPriority());
        textViewStatus.setText(model.getStatus());
    }

    @Click(R.id.card_view)
    public void onViewClick() {
        if (clickDetailed != null) {
            clickDetailed.callbackWorkOrderDetailed(model);
        }
    }

}