package com.fieldwise.adapters;

import android.content.Context;
import android.widget.TextView;

import com.fieldwise.R;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.expand.ChildPosition;
import com.mindorks.placeholderview.annotations.expand.ParentPosition;

@Layout(R.layout.item_invoice_view_all)
public class AdapterInvoiceViewAll {

    public InvoiceViewAllListen click;

    public interface InvoiceViewAllListen {
        void callbackInvoiceViewAllListen(String str);
    }

    @ParentPosition
    int parentPosition;

    @ChildPosition
    int childPosition;

    @View(R.id.textview_view_all)
    TextView textViewViewAll;

    Context ctx;
    String str;

    public AdapterInvoiceViewAll(Context ctx, InvoiceViewAllListen click, String str) {
        this.ctx = ctx;
        this.click = (InvoiceViewAllListen) click;
        this.str = str;
    }

    @Click(R.id.textview_view_all)
    public void onViewClick() {
        if (click != null) {
            click.callbackInvoiceViewAllListen(str);
        }
    }

}