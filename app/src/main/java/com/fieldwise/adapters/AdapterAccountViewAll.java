package com.fieldwise.adapters;

import android.content.Context;
import android.widget.TextView;

import com.fieldwise.R;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.expand.ChildPosition;
import com.mindorks.placeholderview.annotations.expand.ParentPosition;

@Layout(R.layout.item_account_view_all)
public class AdapterAccountViewAll {

    public ViewAllListen click;

    public interface ViewAllListen {
        void callbackViewAllListen(String str);
    }

    @ParentPosition
    int parentPosition;

    @ChildPosition
    int childPosition;

    @View(R.id.textview_view_all)
    TextView textViewViewAll;

    Context ctx;
    String str;

    public AdapterAccountViewAll(Context ctx, ViewAllListen click, String str) {
        this.ctx = ctx;
        this.click = (ViewAllListen) click;
        this.str = str;
    }

    @Click(R.id.textview_view_all)
    public void onViewClick() {
        if (click != null) {
            click.callbackViewAllListen(str);
        }
    }

}