package com.fieldwise.adapters;

import android.content.Context;
import android.widget.TextView;

import com.fieldwise.R;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.expand.ChildPosition;
import com.mindorks.placeholderview.annotations.expand.ParentPosition;

@Layout(R.layout.item_estimate_add_item)
public class AdapterEstimateAddItem {

    public EstimateAddItemListen click;

    public interface EstimateAddItemListen {
        void callbackEstimateAddItemListen(String str);
    }

    @ParentPosition
    int parentPosition;

    @ChildPosition
    int childPosition;

    @View(R.id.textview_add_item)
    TextView textViewAddItem;

    Context ctx;
    String str;

    public AdapterEstimateAddItem(Context ctx, EstimateAddItemListen click, String str) {
        this.ctx = ctx;
        this.click = (EstimateAddItemListen) click;
        this.str = str;
    }

    @Resolve
    public void onResolved() {
        textViewAddItem.setText("Add " + str);
    }

    @Click(R.id.textview_add_item)
    public void onViewClick() {
        if (click != null) {
            click.callbackEstimateAddItemListen(str);
        }
    }

}