package com.fieldwise.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.utils.BaseActivity;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityLogin extends BaseActivity {

    private Activity act;
    private Context ctx;

    private LinearLayout toolbar;
    private ImageButton buttonLeft, buttonRight;
    private TextView textViewCenter;

    private EditText editTextUsername, editTextPassword;
    private Button buttonLogin;
    private TextView textViewForgotLogin, textViewNotLogin;

    private String username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        act = ActivityLogin.this;
        ctx = getApplicationContext();

        toolbar = (LinearLayout) findViewById(R.id.toolbar);
        buttonLeft = (ImageButton) toolbar.findViewById(R.id.button_left);
        textViewCenter = (TextView) toolbar.findViewById(R.id.textview_center);
        buttonRight = (ImageButton) toolbar.findViewById(R.id.button_right);

        editTextUsername = (EditText) findViewById(R.id.edittext_username);
        editTextPassword = (EditText) findViewById(R.id.edittext_password);
        buttonLogin = (Button) findViewById(R.id.button_login);
        textViewForgotLogin = (TextView) findViewById(R.id.textview_forgot_login);
        textViewNotLogin = (TextView) findViewById(R.id.textview_not_login);

        /*
        editTextUsername.setText("jojo@rhyta.com");
        editTextPassword.setText("012993");
        */

        buttonLeft.setBackgroundResource(R.drawable.bg_back);
        buttonLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx, ActivitySignIn.class);
                startActivity(intent);
                finish();
            }
        });
        textViewCenter.setText(getString(R.string.login));
        buttonRight.setBackgroundResource(0);
        buttonRight.setOnClickListener(null);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strEmail = editTextUsername.getText().toString().trim();
                String strPassword = editTextPassword.getText().toString().trim();
                if (TextUtils.isEmpty(strEmail)) {
                    Utl.showToast(ctx, getString(R.string.enter_email));
                } else if (!Patterns.EMAIL_ADDRESS.matcher(strEmail).matches()) {
                    Utl.showToast(ctx, getString(R.string.invalid_email));
                } else if (TextUtils.isEmpty(strPassword)) {
                    Utl.showToast(ctx, getString(R.string.enter_password));
                } else if (strPassword.length() < 6) {
                    Utl.showToast(ctx, getString(R.string.invalid_password));
                } else {
                    final ProgressDialog dialog = new ProgressDialog(act);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setMessage(getString(R.string.please_wait));
                    dialog.show();
                    Map<String, RequestBody> map = new HashMap<>();
                    map.put(Cons.KEY_Email, RequestBody.create(MediaType.parse("text/plain"), strEmail));
                    map.put(Cons.KEY_Password, RequestBody.create(MediaType.parse("text/plain"), strPassword));
                    map.put(Cons.KEY_DeviceUDID, RequestBody.create(MediaType.parse("text/plain"), ""));
                    map.put(Cons.KEY_DeviceType, RequestBody.create(MediaType.parse("text/plain"), "Android"));
                    Call<ResponseBody> response = apiInterface.getLogin(map);
                    response.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                            if (response.isSuccessful()) {
                                try {
                                    String strResponse = response.body().string().toString();
                                    Log.d("TAG_Login" + "_Suc", response.body().string().toString());
                                    JSONObject object = new JSONObject(strResponse);
                                    String strMessage = object.getString("ResponseMsg");
                                    if (object.getInt("ResponseCode") != 0) {
                                        Utl.showToast(ctx, strMessage);
                                        JSONObject dataObject = object.getJSONObject("data");
                                        Utl.setSPBol(ctx, Cons.IS_LoggedIn, true);
                                        Utl.setSPStr(ctx, Cons.KEY_UserID, dataObject.getString(Cons.KEY_UserID));
                                        Utl.setSPStr(ctx, Cons.KEY_Salutation, dataObject.getString(Cons.KEY_Salutation));
                                        Utl.setSPStr(ctx, Cons.KEY_FirstName, dataObject.getString(Cons.KEY_FirstName));
                                        Utl.setSPStr(ctx, Cons.KEY_LastName, dataObject.getString(Cons.KEY_LastName));
                                        Utl.setSPStr(ctx, Cons.KEY_Email, dataObject.getString(Cons.KEY_Email));
                                        Utl.setSPStr(ctx, Cons.KEY_AboutMe, dataObject.getString(Cons.KEY_AboutMe));
                                        Utl.setSPStr(ctx, Cons.KEY_Manager, dataObject.getString(Cons.KEY_Manager));
                                        Utl.setSPStr(ctx, Cons.KEY_ReceiveAdminEmails, dataObject.getString(Cons.KEY_ReceiveAdminEmails));
                                        Utl.setSPStr(ctx, Cons.KEY_Alias, dataObject.getString(Cons.KEY_Alias));
                                        Utl.setSPStr(ctx, Cons.KEY_MobileNo, dataObject.getString(Cons.KEY_MobileNo));
                                        Utl.setSPStr(ctx, Cons.KEY_PhoneNo, dataObject.getString(Cons.KEY_PhoneNo));
                                        Utl.setSPStr(ctx, Cons.KEY_CompanyName, dataObject.getString(Cons.KEY_CompanyName));
                                        Utl.setSPStr(ctx, Cons.KEY_DefaultGrpNotificationFreq, dataObject.getString(Cons.KEY_DefaultGrpNotificationFreq));
                                        Utl.setSPStr(ctx, Cons.KEY_DepartmentName, dataObject.getString(Cons.KEY_DepartmentName));
                                        Utl.setSPStr(ctx, Cons.KEY_DivisionName, dataObject.getString(Cons.KEY_DivisionName));
                                        Utl.setSPStr(ctx, Cons.KEY_Address, dataObject.getString(Cons.KEY_Address));
                                        //Utl.setSPStr(ctx, Cons.KEY_Street, dataObject.getString(Cons.KEY_Street));
                                        Utl.setSPStr(ctx, Cons.KEY_City, dataObject.getString(Cons.KEY_City));
                                        Utl.setSPStr(ctx, Cons.KEY_State, dataObject.getString(Cons.KEY_State));
                                        Utl.setSPStr(ctx, Cons.KEY_Country, dataObject.getString(Cons.KEY_Country));
                                        Utl.setSPStr(ctx, Cons.KEY_PostalCode, dataObject.getString(Cons.KEY_PostalCode));
                                        Utl.setSPStr(ctx, Cons.KEY_CreatedBy, dataObject.getString(Cons.KEY_CreatedBy));
                                        Utl.setSPStr(ctx, Cons.KEY_LastModifiedBy, dataObject.getString(Cons.KEY_LastModifiedBy));
                                        Utl.setSPStr(ctx, Cons.KEY_TOKEN, dataObject.getString(Cons.KEY_TOKEN));
                                        Utl.setSPStr(ctx, Cons.KEY_ORGANIZATION_ID, dataObject.getString(Cons.KEY_ORGANIZATION_ID));
                                        Intent intent = new Intent(ctx, ActivityMain.class);
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        Utl.showToast(ctx, strMessage);
                                    }
                                } catch (JSONException e) {
                                    Log.d("TAG_err_1", "" + e.getMessage());
                                    Utl.showToast(getApplicationContext(), getString(R.string.server_error));
                                } catch (IOException e) {
                                    Log.d("TAG_err_2", "" + e.getMessage());
                                    Utl.showToast(getApplicationContext(), getString(R.string.server_error));
                                }
                            } else {
                                Log.d("TAG_err_3", "null");
                                Utl.showToast(getApplicationContext(), getString(R.string.server_error));
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Log.d("TAG_err_4", "" + t.getMessage());
                            Utl.showToast(getApplicationContext(), getString(R.string.server_error));
                        }
                    });
                }
            }
        });

        String strForgot = getString(R.string.forgot_login) + " " +
                getString(R.string.click_here);
        SpannableString ssForgot = new SpannableString(strForgot);
        ClickableSpan csForgot = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                String email = editTextUsername.getText().toString().trim();
                dialogForgot(email);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                //ds.setUnderlineText(false);
            }
        };
        ssForgot.setSpan(csForgot, strForgot.length() - getString(R.string.click_here).length(), strForgot.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textViewForgotLogin.setHighlightColor(getResources().getColor(R.color.colorNull));
        textViewForgotLogin.setMovementMethod(LinkMovementMethod.getInstance());
        textViewForgotLogin.setText(ssForgot);

        String strNotLogin = getString(R.string.not_login_info) + " " +
                getString(R.string.signup);
        SpannableString ssNotLogin = new SpannableString(strNotLogin);
        ClickableSpan csNotLogin = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent = new Intent(ctx, ActivitySignUp.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                //ds.setUnderlineText(false);
            }
        };
        ssNotLogin.setSpan(csNotLogin, strNotLogin.length() - getString(R.string.signup).length(), strNotLogin.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textViewNotLogin.setHighlightColor(getResources().getColor(R.color.colorNull));
        textViewNotLogin.setMovementMethod(LinkMovementMethod.getInstance());
        textViewNotLogin.setText(ssNotLogin);

    }

    private void dialogForgot(final String strEmail) {
        final Dialog dialogForgot = new Dialog(act);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogForgot.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        dialogForgot.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogForgot.setContentView(R.layout.dialog_forgot);
        final EditText edittextEmail = (EditText) dialogForgot.findViewById(R.id.edittext_email);
        Button buttonSave = (Button) dialogForgot.findViewById(R.id.button_save);
        Button buttonCancel = (Button) dialogForgot.findViewById(R.id.button_cancel);

        if (!TextUtils.isEmpty(strEmail) && Patterns.EMAIL_ADDRESS.matcher(strEmail).matches()) {
            edittextEmail.setText(strEmail);
        }

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = edittextEmail.getText().toString().trim();
                if (TextUtils.isEmpty(email)) {
                    Utl.showToast(ctx, getString(R.string.enter_email));
                } else if (!Patterns.EMAIL_ADDRESS.matcher(strEmail).matches()) {
                    Utl.showToast(ctx, getString(R.string.invalid_email));
                } else {
                    final ProgressDialog dialog = new ProgressDialog(act);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setMessage(getString(R.string.please_wait));
                    dialog.show();
                    Map<String, RequestBody> map = new HashMap<>();
                    map.put(Cons.KEY_Email, RequestBody.create(MediaType.parse("text/plain"), strEmail));
                    Call<ResponseBody> response = apiInterface.getForgot(map);
                    response.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                            if (response.isSuccessful()) {
                                dialogForgot.cancel();
                                try {
                                    String strResponse = response.body().string().toString();
                                    Log.d("TAG_Forgot" + "_Suc", response.body().string().toString());
                                    JSONObject object = new JSONObject(strResponse);
                                    String strMessage = object.getString("ResponseMsg");
                                    Utl.showToast(ctx, strMessage);
                                } catch (JSONException e) {
                                    Utl.showToast(ctx, getString(R.string.server_error));
                                } catch (IOException e) {
                                    Utl.showToast(ctx, getString(R.string.server_error));
                                }
                            } else {
                                Utl.showToast(ctx, getString(R.string.server_error));
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            dialogForgot.cancel();
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                            Utl.showToast(ctx, getString(R.string.server_error));
                        }
                    });
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogForgot.cancel();
            }
        });
        dialogForgot.show();
        dialogForgot.getWindow().setAttributes(lp);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(ctx, ActivitySignIn.class);
        startActivity(intent);
        finish();
    }

}