package com.fieldwise.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.utils.BaseActivity;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySignUp extends BaseActivity {

    private Activity act;
    private Context ctx;

    private LinearLayout toolbar;
    private ImageButton buttonLeft, buttonRight;
    private TextView textViewCenter;

    private EditText editTextFirstname,
            editTextLastname,
            editTextCompany,
            editTextEmail,
            editTextPhone,
            editTextPassword;
    private Button buttonSignUp;
    private TextView textViewNotSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singup);

        act = ActivitySignUp.this;
        ctx = getApplicationContext();

        toolbar = (LinearLayout) findViewById(R.id.toolbar);
        buttonLeft = (ImageButton) toolbar.findViewById(R.id.button_left);
        textViewCenter = (TextView) toolbar.findViewById(R.id.textview_center);
        buttonRight = (ImageButton) toolbar.findViewById(R.id.button_right);

        editTextFirstname = (EditText) findViewById(R.id.edittext_firstname);
        editTextLastname = (EditText) findViewById(R.id.edittext_lastname);
        editTextCompany = (EditText) findViewById(R.id.edittext_company);
        editTextEmail = (EditText) findViewById(R.id.edittext_email);
        editTextPhone = (EditText) findViewById(R.id.edittext_phone);
        editTextPassword = (EditText) findViewById(R.id.edittext_password);
        buttonSignUp = (Button) findViewById(R.id.button_signup);
        textViewNotSignUp = (TextView) findViewById(R.id.textview_not_signup);

        buttonLeft.setBackgroundResource(R.drawable.bg_back);
        buttonLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx, ActivitySignIn.class);
                startActivity(intent);
                finish();
            }
        });
        textViewCenter.setText(getString(R.string.signup));
        buttonRight.setBackgroundResource(0);
        buttonRight.setOnClickListener(null);

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strFirstName = editTextFirstname.getText().toString().trim();
                String strLastName = editTextLastname.getText().toString().trim();
                String strCompany = editTextCompany.getText().toString().trim();
                String strEmail = editTextEmail.getText().toString().trim();
                String strPhone = editTextPhone.getText().toString().trim();
                String strPassword = editTextPassword.getText().toString().trim();

                if (TextUtils.isEmpty(strFirstName)) {
                    Utl.showToast(ctx, getString(R.string.enter_fname));
                } else if (TextUtils.isEmpty(strLastName)) {
                    Utl.showToast(ctx, getString(R.string.enter_lname));
                } else if (TextUtils.isEmpty(strCompany)) {
                    Utl.showToast(ctx, getString(R.string.enter_company));
                } else if (TextUtils.isEmpty(strEmail)) {
                    Utl.showToast(ctx, getString(R.string.enter_email));
                } else if (!Patterns.EMAIL_ADDRESS.matcher(strEmail).matches()) {
                    Utl.showToast(ctx, getString(R.string.invalid_email));
                } else if (TextUtils.isEmpty(strPhone)) {
                    Utl.showToast(ctx, getString(R.string.enter_phone));
                } else if (TextUtils.isEmpty(strPassword)) {
                    Utl.showToast(ctx, getString(R.string.enter_password));
                } else if (strPassword.length() < 6) {
                    Utl.showToast(ctx, getString(R.string.invalid_password));
                } else {
                    final ProgressDialog dialog = new ProgressDialog(act);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setMessage(getString(R.string.please_wait));
                    dialog.show();
                    Map<String, RequestBody> map = new HashMap<>();
                    map.put(Cons.KEY_FirstName, RequestBody.create(MediaType.parse("text/plain"), strFirstName));
                    map.put(Cons.KEY_LastName, RequestBody.create(MediaType.parse("text/plain"), strLastName));
                    map.put(Cons.KEY_CompanyName, RequestBody.create(MediaType.parse("text/plain"), strCompany));
                    map.put(Cons.KEY_Email, RequestBody.create(MediaType.parse("text/plain"), strEmail));
                    map.put(Cons.KEY_PhoneNo, RequestBody.create(MediaType.parse("text/plain"), strPhone));
                    map.put(Cons.KEY_Password, RequestBody.create(MediaType.parse("text/plain"), strPassword));
                    map.put(Cons.KEY_City, RequestBody.create(MediaType.parse("text/plain"), "1"));
                    map.put(Cons.KEY_DeviceUDID, RequestBody.create(MediaType.parse("text/plain"), ""));
                    map.put(Cons.KEY_DeviceType, RequestBody.create(MediaType.parse("text/plain"), "Android"));
                    Call<ResponseBody> response = apiInterface.setSignUp(map);
                    response.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                            if (response.isSuccessful()) {
                                try {
                                    String strResponse = response.body().string().toString();
                                    Log.d("TAG_SignUp" + "_Suc", response.body().string().toString());
                                    JSONObject object = new JSONObject(strResponse);
                                    String strMessage = object.getString("ResponseMsg");
                                    if (object.getInt("ResponseCode") != 0) {
                                        Utl.showToast(ctx, strMessage);
                                        JSONObject dataObject = object.getJSONObject("data");
                                        Utl.setSPBol(ctx, Cons.IS_LoggedIn, true);
                                        Utl.setSPStr(ctx, Cons.KEY_UserID, dataObject.getString(Cons.KEY_UserID));
                                        Utl.setSPStr(ctx, Cons.KEY_Salutation, dataObject.getString(Cons.KEY_Salutation));
                                        Utl.setSPStr(ctx, Cons.KEY_FirstName, dataObject.getString(Cons.KEY_FirstName));
                                        Utl.setSPStr(ctx, Cons.KEY_LastName, dataObject.getString(Cons.KEY_LastName));
                                        Utl.setSPStr(ctx, Cons.KEY_Email, dataObject.getString(Cons.KEY_Email));
                                        Utl.setSPStr(ctx, Cons.KEY_AboutMe, dataObject.getString(Cons.KEY_AboutMe));
                                        Utl.setSPStr(ctx, Cons.KEY_Manager, dataObject.getString(Cons.KEY_Manager));
                                        Utl.setSPStr(ctx, Cons.KEY_ReceiveAdminEmails, dataObject.getString(Cons.KEY_ReceiveAdminEmails));
                                        Utl.setSPStr(ctx, Cons.KEY_Alias, dataObject.getString(Cons.KEY_Alias));
                                        Utl.setSPStr(ctx, Cons.KEY_MobileNo, dataObject.getString(Cons.KEY_MobileNo));
                                        Utl.setSPStr(ctx, Cons.KEY_PhoneNo, dataObject.getString(Cons.KEY_PhoneNo));
                                        Utl.setSPStr(ctx, Cons.KEY_CompanyName, dataObject.getString(Cons.KEY_CompanyName));
                                        Utl.setSPStr(ctx, Cons.KEY_DefaultGrpNotificationFreq, dataObject.getString(Cons.KEY_DefaultGrpNotificationFreq));
                                        Utl.setSPStr(ctx, Cons.KEY_DepartmentName, dataObject.getString(Cons.KEY_DepartmentName));
                                        Utl.setSPStr(ctx, Cons.KEY_DivisionName, dataObject.getString(Cons.KEY_DivisionName));
                                        Utl.setSPStr(ctx, Cons.KEY_Address, dataObject.getString(Cons.KEY_Address));
                                        //Utl.setSPStr(ctx, Cons.KEY_Street, dataObject.getString(Cons.KEY_Street));
                                        Utl.setSPStr(ctx, Cons.KEY_City, dataObject.getString(Cons.KEY_City));
                                        Utl.setSPStr(ctx, Cons.KEY_State, dataObject.getString(Cons.KEY_State));
                                        Utl.setSPStr(ctx, Cons.KEY_Country, dataObject.getString(Cons.KEY_Country));
                                        Utl.setSPStr(ctx, Cons.KEY_PostalCode, dataObject.getString(Cons.KEY_PostalCode));
                                        Utl.setSPStr(ctx, Cons.KEY_CreatedBy, dataObject.getString(Cons.KEY_CreatedBy));
                                        Utl.setSPStr(ctx, Cons.KEY_LastModifiedBy, dataObject.getString(Cons.KEY_LastModifiedBy));
                                        Utl.setSPStr(ctx, Cons.KEY_TOKEN, dataObject.getString(Cons.KEY_TOKEN));
                                        Utl.setSPStr(ctx, Cons.KEY_ORGANIZATION_ID, dataObject.getString(Cons.KEY_ORGANIZATION_ID));
                                        Intent intent = new Intent(ctx, ActivityMain.class);
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        Utl.showToast(ctx, strMessage);
                                    }
                                } catch (JSONException e) {
                                    Log.d("TAG_err_1", "" + e.getMessage());
                                    Utl.showToast(getApplicationContext(), getString(R.string.server_error));
                                } catch (IOException e) {
                                    Log.d("TAG_err_2", "" + e.getMessage());
                                    Utl.showToast(getApplicationContext(), getString(R.string.server_error));
                                }
                            } else {
                                Log.d("TAG_err_3", "null");
                                Utl.showToast(getApplicationContext(), getString(R.string.server_error));
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Log.d("TAG_err_4", "" + t.getMessage());
                            Utl.showToast(getApplicationContext(), getString(R.string.server_error));
                        }
                    });
                }

            }
        });

        String str = getString(R.string.not_signin_info) + " " +
                getString(R.string.login);
        SpannableString ss = new SpannableString(str);
        ClickableSpan cs = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent = new Intent(ctx, ActivityLogin.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                //ds.setUnderlineText(false);
            }
        };
        ss.setSpan(cs, str.length() - getString(R.string.login).length(), str.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textViewNotSignUp.setHighlightColor(getResources().getColor(R.color.colorNull));
        textViewNotSignUp.setMovementMethod(LinkMovementMethod.getInstance());
        textViewNotSignUp.setText(ss);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(ctx, ActivitySignIn.class);
        startActivity(intent);
        finish();
    }

}