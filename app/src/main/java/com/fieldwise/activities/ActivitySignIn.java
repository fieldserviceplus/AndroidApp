package com.fieldwise.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.fieldwise.R;
import com.fieldwise.utils.BaseActivity;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;

public class ActivitySignIn extends BaseActivity {

    private Context ctx;

    private Button buttonLogin, buttonSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singin);

        ctx = getApplicationContext();

        buttonLogin = (Button) findViewById(R.id.button_login);
        buttonSignUp = (Button) findViewById(R.id.button_signup);

        if (Utl.getSPBol(ctx, Cons.IS_LoggedIn)){
            Intent intent = new Intent(ctx, ActivityMain.class);
            startActivity(intent);
            finish();
        }

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx, ActivityLogin.class);
                startActivity(intent);
                finish();
            }
        });

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx, ActivitySignUp.class);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}