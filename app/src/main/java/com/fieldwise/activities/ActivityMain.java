package com.fieldwise.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.ListPopupWindow;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.fieldwise.R;
import com.fieldwise.fragments.FragmentAccount;
import com.fieldwise.fragments.FragmentAccountCreate;
import com.fieldwise.fragments.FragmentAccountFilter;
import com.fieldwise.fragments.FragmentAccountInfo;
import com.fieldwise.fragments.FragmentAccountViewAll;
import com.fieldwise.fragments.FragmentCalendar;

//import com.fieldwise.fragments.FragmentContact;
import com.fieldwise.fragments.FragmentContactKT;

//import com.fieldwise.fragments.FragmentContactFilter;
import com.fieldwise.fragments.FragmentContactFilterKT;

//import com.fieldwise.fragments.FragmentContactInfo;
import com.fieldwise.fragments.FragmentContactInfoKT;

//import com.fieldwise.fragments.FragmentContactCreate;
import com.fieldwise.fragments.FragmentContactCreateKT;

//import com.fieldwise.fragments.FragmentContactViewAll;
import com.fieldwise.fragments.FragmentContactViewAllKT;

import com.fieldwise.fragments.FragmentCreateView;
import com.fieldwise.fragments.FragmentEmail;
import com.fieldwise.fragments.FragmentEstimate;
import com.fieldwise.fragments.FragmentEstimateCreate;
import com.fieldwise.fragments.FragmentEstimateFilter;
import com.fieldwise.fragments.FragmentEstimateInfo;
import com.fieldwise.fragments.FragmentEstimateSignature;
import com.fieldwise.fragments.FragmentEstimateViewAll;
import com.fieldwise.fragments.FragmentEventCreate;
import com.fieldwise.fragments.FragmentFile;
import com.fieldwise.fragments.FragmentFileCreate;
import com.fieldwise.fragments.FragmentFileFilter;
import com.fieldwise.fragments.FragmentFileInfo;
import com.fieldwise.fragments.FragmentHome;
import com.fieldwise.fragments.FragmentInvoice;
import com.fieldwise.fragments.FragmentInvoiceCreate;
import com.fieldwise.fragments.FragmentInvoiceFilter;
import com.fieldwise.fragments.FragmentInvoiceInfo;
import com.fieldwise.fragments.FragmentTask;
import com.fieldwise.fragments.FragmentTaskCreate;
import com.fieldwise.fragments.FragmentTaskFilter;
import com.fieldwise.fragments.FragmentTaskInfo;
import com.fieldwise.fragments.FragmentWorkOrder;
import com.fieldwise.fragments.FragmentWorkOrderCreate;
import com.fieldwise.fragments.FragmentWorkOrderFilter;
import com.fieldwise.fragments.FragmentWorkOrderInfo;
import com.fieldwise.fragments.FragmentWorkOrderSignature;
import com.fieldwise.fragments.FragmentWorkOrderViewAll;
import com.fieldwise.models.ModelCalendar;
import com.fieldwise.models.ModelDynamic;
import com.fieldwise.models.ModelMap;
import com.fieldwise.utils.BaseActivity;
import com.fieldwise.utils.Cons;
import com.fieldwise.utils.Utl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class ActivityMain extends BaseActivity implements

        //Account Interfaces
        FragmentAccount.ClickListenAccount,
        FragmentAccount.ClickListenAccountFilter,
        FragmentAccountInfo.AccountViewAllListen,
        FragmentAccountInfo.ClickListenAccountRelatedItemWorkOrderOpen,
        FragmentAccountViewAll.ClickListenAccountRelatedViewAllWorkOrderOpen,

        FragmentAccountCreate.ClickListenAccCreateClose,
        FragmentAccountFilter.ClickListenAccountFilterCancel,
        FragmentAccountFilter.ClickListenAccountFilterAdd,

        //Contact Interfaces
        FragmentContactKT.ClickListenContact,
        FragmentContactKT.ClickListenContactFilter,
        FragmentContactInfoKT.ContactViewAllListen,
        FragmentContactInfoKT.ClickListenContactRelatedItemWorkOrderOpen,
        FragmentContactViewAllKT.ClickListenContactRelatedViewAllWorkOrderOpen,

        FragmentContactCreateKT.ClickListenContactCreateClose,
        FragmentContactFilterKT.ClickListenContactFilterCancel,
        FragmentContactFilterKT.ClickListenContactFilterAdd,

        //Calendar Interfaces
        FragmentCalendar.CalendarEventClickListen,
        FragmentCalendar.MapEventClickListen,

        //WorkOrder Interfaces
        FragmentWorkOrder.ClickListenWorkOrder,
        FragmentWorkOrder.ClickListenWorkOrderFilter,
        FragmentWorkOrderInfo.WorkOrderViewAllListen,
        FragmentWorkOrderInfo.WorkOrderSignatureListener,

        FragmentWorkOrderCreate.ClickListenWOCreateClose,
        FragmentWorkOrderFilter.ClickListenWorkOrderFilterCancel,
        FragmentWorkOrderFilter.ClickListenWorkOrderFilterAdd,
        FragmentWorkOrderSignature.AddWorkOrderSignatureListener,

        //Estimate Interfaces
        FragmentEstimate.ClickListenEstimate,
        FragmentEstimate.ClickListenEstimateFilter,
        FragmentEstimateInfo.EstimateViewAllListen,
        FragmentEstimateInfo.EstimateSignatureListener,

        FragmentEstimateCreate.ClickListenEstimateCreateClose,
        FragmentEstimateFilter.ClickListenEstimateFilterCancel,
        FragmentEstimateFilter.ClickListenEstimateFilterAdd,
        FragmentEstimateSignature.AddEstimateSignatureListener,

        //Invoice Interfaces
        FragmentInvoice.ClickListenInvoice,
        FragmentInvoice.ClickListenInvoiceFilter,

        FragmentInvoiceCreate.ClickListenInvoiceCreateClose,
        FragmentInvoiceFilter.ClickListenInvoiceFilterCancel,
        FragmentInvoiceFilter.ClickListenInvoiceFilterAdd,

        //File Interfaces
        FragmentFile.ClickListenFile,
        FragmentFile.ClickListenFileFilter,

        FragmentFileCreate.ClickListenFileCreateClose,
        FragmentFileFilter.ClickListenFileFilterCancel,
        FragmentFileFilter.ClickListenFileFilterAdd,

        //Task Interfaces
        FragmentTask.ClickListenTask,
        FragmentTask.ClickListenTaskFilter,

        FragmentTaskCreate.ClickListenTaskCreateClose,
        FragmentTaskFilter.ClickListenTaskFilterCancel,
        FragmentTaskFilter.ClickListenTaskFilterAdd,

        //Common Interfaces
        FragmentAccountInfo.CreateEventListener,
        FragmentContactInfoKT.CreateEventListener,
        FragmentWorkOrderInfo.CreateEventListener,
        FragmentEstimateInfo.CreateEventListener,
        FragmentInvoiceInfo.CreateEventListener,
        FragmentEventCreate.ClickListenEventCreateClose,

        FragmentAccountInfo.CreateFileListener,
        FragmentWorkOrderInfo.CreateFileListener,
        FragmentContactInfoKT.CreateFileListener,
        FragmentEstimateInfo.CreateFileListener,
        FragmentInvoiceInfo.CreateFileListener,
        FragmentTaskInfo.CreateFileListener,

        FragmentAccountInfo.GoToRecent,
        FragmentWorkOrderInfo.GoToRecent,
        FragmentContactInfoKT.GoToRecent,
        FragmentEstimateInfo.GoToRecent,
        FragmentInvoiceInfo.GoToRecent,
        FragmentFileInfo.GoToRecent,
        FragmentTaskInfo.GoToRecent,

        FragmentEmail.ClickListenSendEmailClose,
        FragmentWorkOrderInfo.SendEmailListener,
        FragmentContactInfoKT.SendEmailListener,
        FragmentEstimateInfo.SendEmailListener,
        FragmentInvoiceInfo.SendEmailListener,

        FragmentAccount.ClickListenCreateView,
        FragmentContactKT.ClickListenCreateView,
        FragmentWorkOrder.ClickListenCreateView,
        FragmentEstimate.ClickListenCreateView,
        FragmentInvoice.ClickListenCreateView,
        FragmentFile.ClickListenCreateView,
        FragmentTask.ClickListenCreateView,

        FragmentAccount.ClickListenCopyView,
        FragmentContactKT.ClickListenCopyView,
        FragmentWorkOrder.ClickListenCopyView,
        FragmentEstimate.ClickListenCopyView,
        FragmentInvoice.ClickListenCopyView,
        FragmentFile.ClickListenCopyView,
        FragmentTask.ClickListenCopyView,

        FragmentAccount.ClickListenDeleteView,
        FragmentContactKT.ClickListenDeleteView,
        FragmentWorkOrder.ClickListenDeleteView,
        FragmentEstimate.ClickListenDeleteView,
        FragmentInvoice.ClickListenDeleteView,
        FragmentFile.ClickListenDeleteView,
        FragmentTask.ClickListenDeleteView,

        FragmentCreateView.ClickListenCreateViewCancel,

        FragmentAccountInfo.ClickListenContactCreate,
        FragmentAccountInfo.ClickListenWorkOrderCreate,
        FragmentAccountInfo.ClickListenEstimateCreate,
        FragmentAccountInfo.ClickListenInvoiceCreate,
        FragmentAccountInfo.ClickListenTaskCreate,

        FragmentContactInfoKT.ClickListenWorkOrderCreate,
        FragmentContactInfoKT.ClickListenEstimateCreate,
        FragmentContactInfoKT.ClickListenInvoiceCreate,
        FragmentContactInfoKT.ClickListenTaskCreate,

        FragmentWorkOrderInfo.ClickListenInvoiceCreate,
        FragmentWorkOrderInfo.ClickListenTaskCreate,

        FragmentEstimateInfo.ClickListenInvoiceCreate,
        FragmentEstimateInfo.ClickListenTaskCreate,

        FragmentInvoiceInfo.ClickListenTaskCreate,

        FragmentHome.ClickListenHomeItem {

    public static Activity activity;
    private Context ctx;

    private DrawerLayout navDrawer;
    private ActionBarDrawerToggle drawerToggle;
    private FrameLayout layoutFragment;
    private LinearLayout layoutMain, layoutDrawer;
    private Button buttonNavHome,
            buttonNavAccounts,
            buttonNavContacts,
            buttonNavCalendar,
            buttonNavWorkOrders,
            buttonNavEstimates,
            buttonNavInvoices,
            buttonNavFiles,
            buttonNavTasks,
            buttonNavSettings,
            buttonNavHelp,
            buttonNavLogout;

    private LinearLayout toolbar;
    private ImageButton buttonLeft, buttonRight;
    public static TextView textviewCenter;
    public static TextView textviewCalenderDate;

    private boolean account_create_home_back, account_info_account_back,
            contact_create_home_back, contact_info_contact_back,
            work_order_create_home_back, work_order_info_work_order_back, work_order_info_signature_back,
            estimate_create_home_back, estimate_info_estimate_back, estimate_info_signature_back,
            invoice_create_home_back, invoice_info_invoice_back,
            task_create_home_back, task_info_task_back,
            file_create_home_back, file_info_file_back,
            event_create_home_back, event_info_event_back;

    public static boolean isRecurringOn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activity = this;
        ctx = getApplicationContext();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!checkPermission()) {
                requestPermission();
            }
        }

        Log.d("TAG_TOKEN", Utl.getSPStr(ctx, Cons.KEY_TOKEN));
        Log.d("TAG_ORGANIZATION_ID", Utl.getSPStr(ctx, Cons.KEY_ORGANIZATION_ID));

        navDrawer = (DrawerLayout) findViewById(R.id.nav_drawer);

        toolbar = (LinearLayout) findViewById(R.id.toolbar);
        buttonLeft = (ImageButton) toolbar.findViewById(R.id.button_left);
        textviewCenter = (TextView) toolbar.findViewById(R.id.textview_center);
        textviewCalenderDate = (TextView) toolbar.findViewById(R.id.textview_calender_date);
        buttonRight = (ImageButton) toolbar.findViewById(R.id.button_right);

        layoutMain = (LinearLayout) findViewById(R.id.layout_main);
        layoutFragment = (FrameLayout) findViewById(R.id.layout_fragment);

        layoutDrawer = (LinearLayout) findViewById(R.id.layout_drawer);
        buttonNavHome = (Button) findViewById(R.id.button_nav_home);
        buttonNavAccounts = (Button) findViewById(R.id.button_nav_accounts);
        buttonNavContacts = (Button) findViewById(R.id.button_nav_contacts);
        buttonNavCalendar = (Button) findViewById(R.id.button_nav_calendar);
        buttonNavWorkOrders = (Button) findViewById(R.id.button_nav_work_orders);
        buttonNavEstimates = (Button) findViewById(R.id.button_nav_estimates);
        buttonNavInvoices = (Button) findViewById(R.id.button_nav_invoices);
        buttonNavFiles = (Button) findViewById(R.id.button_nav_files);
        buttonNavTasks = (Button) findViewById(R.id.button_nav_tasks);
        buttonNavSettings = (Button) findViewById(R.id.button_nav_settings);
        buttonNavHelp = (Button) findViewById(R.id.button_nav_help);
        buttonNavLogout = (Button) findViewById(R.id.button_nav_logout);

        account_create_home_back = true;
        account_info_account_back = false;
        contact_create_home_back = true;
        contact_info_contact_back = false;
        work_order_create_home_back = true;
        work_order_info_work_order_back = false;
        work_order_info_signature_back = false;
        estimate_create_home_back = true;
        estimate_info_estimate_back = false;
        estimate_info_signature_back = false;
        invoice_create_home_back = true;
        invoice_info_invoice_back = false;
        task_create_home_back = true;
        task_info_task_back = false;
        file_create_home_back = true;
        file_info_file_back = false;
        event_create_home_back = true;
        event_info_event_back = false;

        drawerToggle = new ActionBarDrawerToggle(this, navDrawer, R.drawable.ic_menu, R.string.app_name) {
            @Override
            public void onDrawerClosed(View v) {
                supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View v) {
                supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View v, float offset) {
                super.onDrawerSlide(v, offset);
                layoutMain.setTranslationX(offset * v.getWidth());
                navDrawer.bringChildToFront(v);
                navDrawer.requestLayout();
            }
        };
        navDrawer.setDrawerListener(drawerToggle);

        buttonNavHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setHomFragment();
            }
        });

        buttonNavAccounts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new FragmentAccount(), getString(R.string.accounts_recent));
            }
        });

        buttonNavContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new FragmentContactKT(), getString(R.string.contacts_recent));
            }
        });

        buttonNavCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new FragmentCalendar(), getString(R.string.calendar));
                buttonRight.setVisibility(View.GONE);
                Calendar c = Calendar.getInstance();
                c.setTime(Calendar.getInstance().getTime());
                int day = c.get(Calendar.DATE);
                textviewCalenderDate.setVisibility(View.VISIBLE);
                textviewCalenderDate.setText("" + day);
            }
        });

        buttonNavWorkOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new FragmentWorkOrder(), getString(R.string.work_orders_recent));
            }
        });

        buttonNavEstimates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new FragmentEstimate(), getString(R.string.estimates_recent));
            }
        });

        buttonNavInvoices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new FragmentInvoice(), getString(R.string.invoices_recent));
            }
        });

        buttonNavFiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new FragmentFile(), getString(R.string.files_recent));
            }
        });

        buttonNavTasks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new FragmentTask(), getString(R.string.tasks_recent));
            }
        });

        buttonNavSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navDrawer.closeDrawers();
            }
        });

        buttonNavHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navDrawer.closeDrawers();
            }
        });

        buttonNavLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navDrawer.closeDrawers();
                Utl.clearSP(ctx);
                finish();
            }
        });

        setHomFragment();
    }

    private void showMenuPopup(View view) {
        final List<HashMap<String, Object>> data = new ArrayList<>();

        HashMap<String, Object> map = new HashMap<>();
        map.put("title", getString(R.string.create_new));
        map.put("icon", 0);
        data.add(map);

        map = new HashMap<>();
        map.put("title", getString(R.string.account));
        map.put("icon", R.drawable.bg_account);
        data.add(map);

        map = new HashMap<>();
        map.put("title", getString(R.string.contact));
        map.put("icon", R.drawable.bg_contact);
        data.add(map);

        map = new HashMap<>();
        map.put("title", getString(R.string.event));
        map.put("icon", R.drawable.bg_calendar);
        data.add(map);

        map = new HashMap<>();
        map.put("title", getString(R.string.work_order));
        map.put("icon", R.drawable.bg_work_order);
        data.add(map);

        map = new HashMap<>();
        map.put("title", getString(R.string.estimate));
        map.put("icon", R.drawable.bg_estimate);
        data.add(map);

        map = new HashMap<>();
        map.put("title", getString(R.string.invoice));
        map.put("icon", R.drawable.bg_invoice);
        data.add(map);

        map = new HashMap<>();
        map.put("title", getString(R.string.file));
        map.put("icon", R.drawable.bg_file);
        data.add(map);

        map = new HashMap<>();
        map.put("title", getString(R.string.task));
        map.put("icon", R.drawable.bg_task);
        data.add(map);

        final ListPopupWindow popupWindow = new ListPopupWindow(ActivityMain.this);
        ListAdapter adapter = new SimpleAdapter(
                ActivityMain.this,
                data,
                R.layout.menu_popup,
                new String[]{"title", "icon"},
                new int[]{R.id.title, R.id.icon});

        popupWindow.setAnchorView(view);
        popupWindow.setAdapter(adapter);
        popupWindow.setContentWidth(dialogWidth(ctx, adapter));
        popupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                } else {
                    String strItem = data.get(position).get("title").toString();
                    if (strItem.equals(getString(R.string.account))) {
                        FragmentAccountCreate fragment = new FragmentAccountCreate();
                        Bundle bundle = new Bundle();
                        bundle.putInt("back_type", 0);
                        fragment.setArguments(bundle);
                        setFragment(fragment, getString(R.string.account_create));
                        account_create_home_back = true;
                        buttonRight.setVisibility(View.GONE);
                    } else if (strItem.equals(getString(R.string.work_order))) {
                        FragmentWorkOrderCreate fragment = new FragmentWorkOrderCreate();
                        Bundle bundle = new Bundle();
                        bundle.putInt("back_type", 0);
                        fragment.setArguments(bundle);
                        setFragment(fragment, getString(R.string.work_order_create));
                        work_order_create_home_back = true;
                    } else if (strItem.equals(getString(R.string.contact))) {
                        FragmentContactCreateKT fragment = new FragmentContactCreateKT();
                        Bundle bundle = new Bundle();
                        bundle.putInt("back_type", 0);
                        fragment.setArguments(bundle);
                        setFragment(fragment, getString(R.string.contact_create));
                        contact_create_home_back = true;
                    } else if (strItem.equals(getString(R.string.estimate))) {
                        FragmentEstimateCreate fragment = new FragmentEstimateCreate();
                        Bundle bundle = new Bundle();
                        bundle.putInt("back_type", 0);
                        fragment.setArguments(bundle);
                        setFragment(fragment, getString(R.string.estimate_create));
                        estimate_create_home_back = true;
                    } else if (strItem.equals(getString(R.string.invoice))) {
                        FragmentInvoiceCreate fragment = new FragmentInvoiceCreate();
                        Bundle bundle = new Bundle();
                        bundle.putInt("back_type", 0);
                        fragment.setArguments(bundle);
                        setFragment(fragment, getString(R.string.invoice_create));
                        invoice_create_home_back = true;
                    } else if (strItem.equals(getString(R.string.file))) {
                        FragmentFileCreate fragment = new FragmentFileCreate();
                        Bundle bundle = new Bundle();
                        bundle.putInt("back_type", 0);
                        fragment.setArguments(bundle);
                        setFragment(fragment, getString(R.string.file_create));
                        file_create_home_back = true;
                    } else if (strItem.equals(getString(R.string.task))) {
                        FragmentTaskCreate fragment = new FragmentTaskCreate();
                        Bundle bundle = new Bundle();
                        bundle.putInt("back_type", 0);
                        fragment.setArguments(bundle);
                        setFragment(fragment, getString(R.string.task_create));
                        task_create_home_back = true;
                    } else {
                        Utl.showToast(ctx, data.get(position).get("title").toString() + " Clicked");
                    }
                    popupWindow.dismiss();
                }
            }
        });
        popupWindow.show();
    }

    private void setHomFragment() {
        navDrawer.closeDrawers();
        account_create_home_back = true;
        contact_create_home_back = true;
        work_order_create_home_back = true;
        estimate_create_home_back = true;
        invoice_create_home_back = true;
        task_create_home_back = true;
        file_create_home_back = true;
        event_create_home_back = true;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.layout_fragment, new FragmentHome());
        fragmentTransaction.commit();
        buttonLeft.setBackgroundResource(R.drawable.bg_menu);
        buttonLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navDrawer.openDrawer(Gravity.START);
            }
        });
        textviewCenter.setText(getString(R.string.home));
        buttonRight.setBackgroundResource(R.drawable.bg_add);
        buttonRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenuPopup(view);
            }
        });
    }

    private void setFragment(final Fragment fragment, final String str) {
        navDrawer.closeDrawers();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.layout_fragment, fragment);
        fragmentTransaction.commit();
        buttonLeft.setBackgroundResource(R.drawable.bg_back);
        buttonLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        textviewCenter.setText(str);
        buttonRight.setVisibility(View.VISIBLE);
        textviewCalenderDate.setVisibility(View.GONE);
        buttonRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (str.equals(getString(R.string.accounts_recent))) {
                    FragmentAccountCreate fragment = new FragmentAccountCreate();
                    Bundle bundle = new Bundle();
                    bundle.putInt("back_type", 1);
                    fragment.setArguments(bundle);
                    setFragment(fragment, getString(R.string.account_create));
                    account_create_home_back = false;
                    buttonRight.setVisibility(View.GONE);
                } else if (str.equals(getString(R.string.contacts_recent))) {
                    FragmentContactCreateKT fragment = new FragmentContactCreateKT();
                    Bundle bundle = new Bundle();
                    bundle.putInt("back_type", 1);
                    fragment.setArguments(bundle);
                    setFragment(fragment, getString(R.string.contact_create));
                    contact_create_home_back = false;
                    buttonRight.setVisibility(View.GONE);
                } else if (str.equals(getString(R.string.work_orders_recent))) {
                    FragmentWorkOrderCreate fragment = new FragmentWorkOrderCreate();
                    Bundle bundle = new Bundle();
                    bundle.putInt("back_type", 1);
                    fragment.setArguments(bundle);
                    setFragment(fragment, getString(R.string.work_order_create));
                    work_order_create_home_back = false;
                } else if (str.equals(getString(R.string.estimates_recent))) {
                    FragmentEstimateCreate fragment = new FragmentEstimateCreate();
                    Bundle bundle = new Bundle();
                    bundle.putInt("back_type", 1);
                    fragment.setArguments(bundle);
                    setFragment(fragment, getString(R.string.estimate_create));
                    estimate_create_home_back = false;
                } else if (str.equals(getString(R.string.invoices_recent))) {
                    FragmentInvoiceCreate fragment = new FragmentInvoiceCreate();
                    Bundle bundle = new Bundle();
                    bundle.putInt("back_type", 1);
                    fragment.setArguments(bundle);
                    setFragment(fragment, getString(R.string.invoice_create));
                    invoice_create_home_back = false;
                } else if (str.equals(getString(R.string.tasks_recent))) {
                    FragmentTaskCreate fragment = new FragmentTaskCreate();
                    Bundle bundle = new Bundle();
                    bundle.putInt("back_type", 1);
                    fragment.setArguments(bundle);
                    setFragment(fragment, getString(R.string.task_create));
                    task_create_home_back = false;
                } else if (str.equals(getString(R.string.files_recent))) {
                    FragmentFileCreate fragment = new FragmentFileCreate();
                    Bundle bundle = new Bundle();
                    bundle.putInt("back_type", 1);
                    fragment.setArguments(bundle);
                    setFragment(fragment, getString(R.string.file_create));
                    file_create_home_back = false;
                }
            }
        });
    }

    private int dialogWidth(Context c, ListAdapter la) {
        ViewGroup viewGroup = null;
        int maxWidth = 0;
        View itemView = null;
        int itemType = 0;
        final ListAdapter adapter = la;
        final int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int count = adapter.getCount();
        for (int i = 0; i < count; i++) {
            final int positionType = adapter.getItemViewType(i);
            if (positionType != itemType) {
                itemType = positionType;
                itemView = null;
            }
            if (viewGroup == null) {
                viewGroup = new FrameLayout(c);
            }
            itemView = adapter.getView(i, itemView, viewGroup);
            itemView.measure(widthMeasureSpec, heightMeasureSpec);
            final int itemWidth = itemView.getMeasuredWidth();
            if (itemWidth > maxWidth) {
                maxWidth = itemWidth;
            }
        }
        return maxWidth;
    }

    //Recent Account List Item Open
    @Override
    public void callbackAccount(ModelDynamic model) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_AccountID, model.getMap().get(Cons.KEY_AccountID));
        FragmentAccountInfo fragment = new FragmentAccountInfo();
        fragment.setArguments(bundle);
        setFragment(fragment, model.getMap().get(Cons.KEY_AccountName));
        account_info_account_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    //Recent Contact List Item Open
    @Override
    public void callbackContact(ModelDynamic model) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_ContactID, model.getMap().get(Cons.KEY_ContactID));
        FragmentContactInfoKT fragment = new FragmentContactInfoKT();
        fragment.setArguments(bundle);
        setFragment(fragment, model.getMap().get(Cons.KEY_ContactName));
        contact_info_contact_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    //Recent WorkOrder List Item Open
    @Override
    public void callbackWorkOrder(ModelDynamic model) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_WorkOrderID, model.getMap().get(Cons.KEY_WorkOrderID));
        FragmentWorkOrderInfo fragment = new FragmentWorkOrderInfo();
        fragment.setArguments(bundle);
        if (model.getMap().containsKey(Cons.KEY_WONo)) {
            setFragment(fragment, model.getMap().get(Cons.KEY_WONo));
        } else {
            setFragment(fragment, model.getMap().get(Cons.KEY_WorkOrderNo));
        }
        work_order_info_work_order_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    //Recent Estimate List Item Open
    @Override
    public void callbackEstimate(ModelDynamic model) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_EstimateID, model.getMap().get(Cons.KEY_EstimateID));
        FragmentEstimateInfo fragment = new FragmentEstimateInfo();
        fragment.setArguments(bundle);
        if (model.getMap().containsKey(Cons.KEY_ETNo)) {
            setFragment(fragment, model.getMap().get(Cons.KEY_ETNo));
        } else {
            setFragment(fragment, model.getMap().get(Cons.KEY_EstimateNo));
        }
        estimate_info_estimate_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    //Recent Invoice List Item Open
    @Override
    public void callbackInvoice(ModelDynamic model) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_InvoiceID, model.getMap().get(Cons.KEY_InvoiceID));
        FragmentInvoiceInfo fragment = new FragmentInvoiceInfo();
        fragment.setArguments(bundle);
        if (model.getMap().containsKey(Cons.KEY_INNo)) {
            setFragment(fragment, model.getMap().get(Cons.KEY_INNo));
        } else {
            setFragment(fragment, model.getMap().get(Cons.KEY_InvoiceNo));
        }
        invoice_info_invoice_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    //Recent Task List Item Open
    @Override
    public void callbackTask(ModelDynamic model) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_TaskID, model.getMap().get(Cons.KEY_TaskID));
        FragmentTaskInfo fragment = new FragmentTaskInfo();
        fragment.setArguments(bundle);
        setFragment(fragment, model.getMap().get(Cons.KEY_Subject));
        task_info_task_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    //Recent File List Item Open
    @Override
    public void callbackFile(ModelDynamic model) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_FileID, model.getMap().get(Cons.KEY_FileID));
        FragmentFileInfo fragment = new FragmentFileInfo();
        fragment.setArguments(bundle);
        setFragment(fragment, model.getMap().get(Cons.KEY_Subject));
        file_info_file_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    //Account Filter Open
    @Override
    public void callbackAccountFilter(String accountView) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_AccountViewID, accountView);
        FragmentAccountFilter fragment = new FragmentAccountFilter();
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.filter));
        account_info_account_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackAccountFilterCancel() {
        onBackPressed();
    }

    //Contact Filter Open
    @Override
    public void callbackContactFilter(String contactView) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_ContactViewID, contactView);
        FragmentContactFilterKT fragment = new FragmentContactFilterKT();
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.filter));
        contact_info_contact_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackContactFilterCancel() {
        onBackPressed();
    }

    //WorkOrder Filter Open
    @Override
    public void callbackWorkOrderFilter(String workOrderView) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_WorkOrderViewID, workOrderView);
        FragmentWorkOrderFilter fragment = new FragmentWorkOrderFilter();
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.filter));
        work_order_info_work_order_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackWorkOrderFilterCancel() {
        onBackPressed();
    }

    //Estimate Filter Open
    @Override
    public void callbackEstimateFilter(String estimateView) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_EstimateViewID, estimateView);
        FragmentEstimateFilter fragment = new FragmentEstimateFilter();
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.filter));
        estimate_info_estimate_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackEstimateFilterCancel() {
        onBackPressed();
    }

    //Invoice Filter Open
    @Override
    public void callbackInvoiceFilter(String invoiceView) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_InvoiceViewID, invoiceView);
        FragmentInvoiceFilter fragment = new FragmentInvoiceFilter();
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.filter));
        invoice_info_invoice_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackInvoiceFilterCancel() {
        onBackPressed();
    }

    //Task Filter Open
    @Override
    public void callbackTaskFilter(String taskView) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_TaskViewID, taskView);
        FragmentTaskFilter fragment = new FragmentTaskFilter();
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.filter));
        task_info_task_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackTaskFilterCancel() {
        onBackPressed();
    }

    @Override
    public void callbackFileFilter(String fileView) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_FileViewID, fileView);
        FragmentFileFilter fragment = new FragmentFileFilter();
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.filter));
        file_info_file_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackFileFilterCancel() {
        onBackPressed();
    }

    @Override
    public void callbackAccountRelatedWorkOrderOpen(String workOrderID, String workOrderNo) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_WorkOrderID, workOrderID);
        FragmentWorkOrderInfo fragment = new FragmentWorkOrderInfo();
        fragment.setArguments(bundle);
        setFragment(fragment, workOrderNo);
        work_order_info_work_order_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackAccountRelatedViewAllWorkOrderOpen(ModelDynamic model) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_WorkOrderID, model.getMap().get(Cons.KEY_WorkOrderID));
        FragmentWorkOrderInfo fragment = new FragmentWorkOrderInfo();
        fragment.setArguments(bundle);
        setFragment(fragment, model.getMap().get(Cons.KEY_WorkOrderNo));
        work_order_info_work_order_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackContactRelatedViewAllWorkOrderOpen(ModelDynamic model) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_WorkOrderID, model.getMap().get(Cons.KEY_WorkOrderID));
        FragmentWorkOrderInfo fragment = new FragmentWorkOrderInfo();
        fragment.setArguments(bundle);
        setFragment(fragment, model.getMap().get(Cons.KEY_WorkOrderNo));
        work_order_info_work_order_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackContactRelatedWorkOrderOpen(String workOrderID, String workOrderNo) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_WorkOrderID, workOrderID);
        FragmentWorkOrderInfo fragment = new FragmentWorkOrderInfo();
        fragment.setArguments(bundle);
        setFragment(fragment, workOrderNo);
        work_order_info_work_order_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackAccountFilterAdd(String accountView, String sbFields, String sbConditions, String sbValues) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("isAccountFilter", true);
        bundle.putString(Cons.KEY_AccountViewID, accountView);
        bundle.putString(Cons.KEY_FilterFields, sbFields);
        bundle.putString(Cons.KEY_FilterConditions, sbConditions);
        bundle.putString(Cons.KEY_FilterValues, sbValues);
        FragmentAccount fragment = new FragmentAccount();
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.accounts_recent));
    }

    @Override
    public void callbackContactFilterAdd(String contactView, String sbFields, String sbConditions, String sbValues) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("isContactFilter", true);
        bundle.putString(Cons.KEY_ContactViewID, contactView);
        bundle.putString(Cons.KEY_FilterFields, sbFields);
        bundle.putString(Cons.KEY_FilterConditions, sbConditions);
        bundle.putString(Cons.KEY_FilterValues, sbValues);
        FragmentContactKT fragment = new FragmentContactKT();
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.contacts_recent));
    }

    @Override
    public void callbackWorkOrderFilterAdd(String wokrOrderView, String sbFields, String sbConditions, String sbValues) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("isWorkOrderFilter", true);
        bundle.putString(Cons.KEY_WorkOrderViewID, wokrOrderView);
        bundle.putString(Cons.KEY_FilterFields, sbFields);
        bundle.putString(Cons.KEY_FilterConditions, sbConditions);
        bundle.putString(Cons.KEY_FilterValues, sbValues);
        FragmentWorkOrder fragment = new FragmentWorkOrder();
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.work_orders_recent));
    }

    @Override
    public void callbackEstimateFilterAdd(String estimateView, String sbFields, String sbConditions, String sbValues) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("isEstimateFilter", true);
        bundle.putString(Cons.KEY_EstimateViewID, estimateView);
        bundle.putString(Cons.KEY_FilterFields, sbFields);
        bundle.putString(Cons.KEY_FilterConditions, sbConditions);
        bundle.putString(Cons.KEY_FilterValues, sbValues);
        FragmentEstimate fragment = new FragmentEstimate();
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.estimates_recent));
    }

    @Override
    public void callbackInvoiceFilterAdd(String invoiceView, String sbFields, String sbConditions, String sbValues) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("isInvoiceFilter", true);
        bundle.putString(Cons.KEY_InvoiceViewID, invoiceView);
        bundle.putString(Cons.KEY_FilterFields, sbFields);
        bundle.putString(Cons.KEY_FilterConditions, sbConditions);
        bundle.putString(Cons.KEY_FilterValues, sbValues);
        FragmentInvoice fragment = new FragmentInvoice();
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.invoices_recent));
    }

    @Override
    public void callbackTaskFilterAdd(String taskView, String sbFields, String sbConditions, String sbValues) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("isTaskFilter", true);
        bundle.putString(Cons.KEY_TaskViewID, taskView);
        bundle.putString(Cons.KEY_FilterFields, sbFields);
        bundle.putString(Cons.KEY_FilterConditions, sbConditions);
        bundle.putString(Cons.KEY_FilterValues, sbValues);
        FragmentTask fragment = new FragmentTask();
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.tasks_recent));
    }

    @Override
    public void callbackFileFilterAdd(String fileView, String sbFields, String sbConditions, String sbValues) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("isFileFilter", true);
        bundle.putString(Cons.KEY_FileViewID, fileView);
        bundle.putString(Cons.KEY_FilterFields, sbFields);
        bundle.putString(Cons.KEY_FilterConditions, sbConditions);
        bundle.putString(Cons.KEY_FilterValues, sbValues);
        FragmentFile fragment = new FragmentFile();
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.files_recent));
    }

    @Override
    public void callbackAccountCreateClose(int back_type, boolean isSave, String accountID, String accountNo) {
        if (buttonRight.getVisibility() == View.GONE) {
            buttonRight.setVisibility(View.VISIBLE);
        }
        if (back_type == 1) {
            if (isSave) {
                Bundle bundle = new Bundle();
                bundle.putString(Cons.KEY_AccountID, accountID);
                FragmentAccountInfo fragment = new FragmentAccountInfo();
                fragment.setArguments(bundle);
                setFragment(fragment, accountNo);
                account_info_account_back = true;
                buttonRight.setVisibility(View.GONE);
            } else {
                setFragment(new FragmentContactKT(), getString(R.string.accounts_recent));
            }
        } else {
            if (isSave) {
                Bundle bundle = new Bundle();
                bundle.putString(Cons.KEY_AccountID, accountID);
                FragmentAccountInfo fragment = new FragmentAccountInfo();
                fragment.setArguments(bundle);
                setFragment(fragment, accountNo);
                account_info_account_back = true;
                buttonRight.setVisibility(View.GONE);
            } else {
                setHomFragment();
            }
        }
    }

    @Override
    public void callbackContactCreateClose(int back_type, boolean isSave, String contactID, String contactNo) {
        if (buttonRight.getVisibility() == View.GONE) {
            buttonRight.setVisibility(View.VISIBLE);
        }
        if (back_type == 1) {
            if (isSave) {
                Bundle bundle = new Bundle();
                bundle.putString(Cons.KEY_ContactID, contactID);
                FragmentContactInfoKT fragment = new FragmentContactInfoKT();
                fragment.setArguments(bundle);
                setFragment(fragment, contactNo);
                contact_info_contact_back = true;
                buttonRight.setVisibility(View.GONE);
            } else {
                setFragment(new FragmentContactKT(), getString(R.string.contacts_recent));
            }
        } else {
            if (isSave) {
                Bundle bundle = new Bundle();
                bundle.putString(Cons.KEY_ContactID, contactID);
                FragmentContactInfoKT fragment = new FragmentContactInfoKT();
                fragment.setArguments(bundle);
                setFragment(fragment, contactNo);
                contact_info_contact_back = true;
                buttonRight.setVisibility(View.GONE);
            } else {
                setHomFragment();
            }
        }
    }

    @Override
    public void callbackWorkOrderCreateClose(int back_type, boolean isSave, String workOrderID, String workOrderNo) {
        if (buttonRight.getVisibility() == View.GONE) {
            buttonRight.setVisibility(View.VISIBLE);
        }
        if (back_type == 1) {
            if (isSave) {
                Bundle bundle = new Bundle();
                bundle.putString(Cons.KEY_WorkOrderID, workOrderID);
                FragmentWorkOrderInfo fragment = new FragmentWorkOrderInfo();
                fragment.setArguments(bundle);
                setFragment(fragment, workOrderNo);
                work_order_info_work_order_back = true;
                buttonRight.setVisibility(View.GONE);
            } else {
                setFragment(new FragmentWorkOrder(), getString(R.string.work_orders_recent));
            }
        } else {
            if (isSave) {
                Bundle bundle = new Bundle();
                bundle.putString(Cons.KEY_WorkOrderID, workOrderID);
                FragmentWorkOrderInfo fragment = new FragmentWorkOrderInfo();
                fragment.setArguments(bundle);
                setFragment(fragment, workOrderNo);
                work_order_info_work_order_back = true;
                buttonRight.setVisibility(View.GONE);
            } else {
                setHomFragment();
            }
        }
    }

    @Override
    public void callbackEstimateCreateClose(int back_type, boolean isSave, String estimateID, String estimateNo) {
        if (buttonRight.getVisibility() == View.GONE) {
            buttonRight.setVisibility(View.VISIBLE);
        }
        if (back_type == 1) {
            if (isSave) {
                Bundle bundle = new Bundle();
                bundle.putString(Cons.KEY_EstimateID, estimateID);
                FragmentEstimateInfo fragment = new FragmentEstimateInfo();
                fragment.setArguments(bundle);
                setFragment(fragment, estimateNo);
                estimate_info_estimate_back = true;
                buttonRight.setVisibility(View.GONE);
            } else {
                setFragment(new FragmentEstimate(), getString(R.string.estimates_recent));
            }
        } else {
            if (isSave) {
                Bundle bundle = new Bundle();
                bundle.putString(Cons.KEY_EstimateID, estimateID);
                FragmentEstimateInfo fragment = new FragmentEstimateInfo();
                fragment.setArguments(bundle);
                setFragment(fragment, estimateNo);
                estimate_info_estimate_back = true;
                buttonRight.setVisibility(View.GONE);
            } else {
                setHomFragment();
            }
        }
    }

    @Override
    public void callbackInvoiceCreateClose(int back_type, boolean isSave, String invoiceID, String invoiceNo) {
        if (buttonRight.getVisibility() == View.GONE) {
            buttonRight.setVisibility(View.VISIBLE);
        }
        if (back_type == 1) {
            if (isSave) {
                Bundle bundle = new Bundle();
                bundle.putString(Cons.KEY_InvoiceID, invoiceID);
                FragmentInvoiceInfo fragment = new FragmentInvoiceInfo();
                fragment.setArguments(bundle);
                setFragment(fragment, invoiceNo);
                invoice_info_invoice_back = true;
                buttonRight.setVisibility(View.GONE);
            } else {
                setFragment(new FragmentInvoice(), getString(R.string.invoices_recent));
            }
        } else {
            if (isSave) {
                Bundle bundle = new Bundle();
                bundle.putString(Cons.KEY_InvoiceID, invoiceID);
                FragmentInvoiceInfo fragment = new FragmentInvoiceInfo();
                fragment.setArguments(bundle);
                setFragment(fragment, invoiceNo);
                invoice_info_invoice_back = true;
                buttonRight.setVisibility(View.GONE);
            } else {
                setHomFragment();
            }
        }
    }

    @Override
    public void callbackFileCreateClose(int back_type, boolean isSave, String fileID, String subject) {
        if (buttonRight.getVisibility() == View.GONE) {
            buttonRight.setVisibility(View.VISIBLE);
        }
        if (back_type == 1) {
            if (isSave) {
                Bundle bundle = new Bundle();
                bundle.putString(Cons.KEY_FileID, fileID);
                FragmentFileInfo fragment = new FragmentFileInfo();
                fragment.setArguments(bundle);
                setFragment(fragment, subject);
                file_info_file_back = true;
                buttonRight.setVisibility(View.GONE);
            } else {
                if (fileID.equals(getString(R.string.account))) {
                    setFragment(new FragmentAccount(), getString(R.string.accounts_recent));
                } else if (fileID.equals(getString(R.string.work_order))) {
                    setFragment(new FragmentWorkOrder(), getString(R.string.work_orders_recent));
                } else if (fileID.equals(getString(R.string.contact))) {
                    setFragment(new FragmentContactKT(), getString(R.string.contacts_recent));
                } else if (fileID.equals(getString(R.string.estimate))) {
                    setFragment(new FragmentEstimate(), getString(R.string.estimates_recent));
                } else if (fileID.equals(getString(R.string.invoice))) {
                    setFragment(new FragmentInvoice(), getString(R.string.invoices_recent));
                } else {
                    setFragment(new FragmentFile(), getString(R.string.files_recent));
                }
            }
        } else {
            if (isSave) {
                Bundle bundle = new Bundle();
                bundle.putString(Cons.KEY_TaskID, fileID);
                FragmentFileInfo fragment = new FragmentFileInfo();
                fragment.setArguments(bundle);
                setFragment(fragment, subject);
                file_info_file_back = true;
                buttonRight.setVisibility(View.GONE);
            } else {
                setHomFragment();
            }
        }
    }

    @Override
    public void callbackTaskCreateClose(int back_type, boolean isSave, String taskID, String subject) {
        if (buttonRight.getVisibility() == View.GONE) {
            buttonRight.setVisibility(View.VISIBLE);
        }
        if (back_type == 1) {
            if (isSave) {
                Bundle bundle = new Bundle();
                bundle.putString(Cons.KEY_TaskID, taskID);
                FragmentTaskInfo fragment = new FragmentTaskInfo();
                fragment.setArguments(bundle);
                setFragment(fragment, subject);
                task_info_task_back = true;
                buttonRight.setVisibility(View.GONE);
            } else {
                setFragment(new FragmentTask(), getString(R.string.tasks_recent));
            }
        } else {
            if (isSave) {
                Bundle bundle = new Bundle();
                bundle.putString(Cons.KEY_TaskID, taskID);
                FragmentTaskInfo fragment = new FragmentTaskInfo();
                fragment.setArguments(bundle);
                setFragment(fragment, subject);
                task_info_task_back = true;
                buttonRight.setVisibility(View.GONE);
            } else {
                setHomFragment();
            }
        }
    }

    @Override
    public void callbackEventCreateClose(String strWhoName, String strWhatID, String strWhatName) {
        if (buttonRight.getVisibility() == View.GONE) {
            buttonRight.setVisibility(View.VISIBLE);
        }
        if (strWhoName.equals(getString(R.string.account))) {
            Bundle bundle = new Bundle();
            bundle.putString(Cons.KEY_AccountID, strWhatID);
            FragmentAccountInfo fragment = new FragmentAccountInfo();
            fragment.setArguments(bundle);
            setFragment(fragment, strWhatName);
            account_info_account_back = true;
            buttonRight.setVisibility(View.GONE);
        } else if (strWhoName.equals(getString(R.string.contact))) {
            Bundle bundle = new Bundle();
            bundle.putString(Cons.KEY_ContactID, strWhatID);
            FragmentContactInfoKT fragment = new FragmentContactInfoKT();
            fragment.setArguments(bundle);
            setFragment(fragment, strWhatName);
            account_info_account_back = true;
            buttonRight.setVisibility(View.GONE);
        } else if (strWhoName.equals(getString(R.string.workorder))) {
            Bundle bundle = new Bundle();
            bundle.putString(Cons.KEY_WorkOrderID, strWhatID);
            FragmentWorkOrderInfo fragment = new FragmentWorkOrderInfo();
            fragment.setArguments(bundle);
            setFragment(fragment, strWhatName);
            work_order_info_work_order_back = true;
            buttonRight.setVisibility(View.GONE);
        } else if (strWhoName.equals(getString(R.string.estimate))) {
            Bundle bundle = new Bundle();
            bundle.putString(Cons.KEY_EstimateID, strWhatID);
            FragmentEstimateInfo fragment = new FragmentEstimateInfo();
            fragment.setArguments(bundle);
            setFragment(fragment, strWhatName);
            estimate_info_estimate_back = true;
            buttonRight.setVisibility(View.GONE);
        } else if (strWhoName.equals(getString(R.string.invoice))) {
            Bundle bundle = new Bundle();
            bundle.putString(Cons.KEY_InvoiceID, strWhatID);
            FragmentInvoiceInfo fragment = new FragmentInvoiceInfo();
            fragment.setArguments(bundle);
            setFragment(fragment, strWhatName);
            invoice_info_invoice_back = true;
            buttonRight.setVisibility(View.GONE);
        }
    }

    @Override
    public void callbackAccountViewAllListen(String str, String accountId) {
        if (str.equals(getString(R.string.work_order))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.work_order));
            bundle.putString(Cons.KEY_AccountID, accountId);
            FragmentAccountViewAll fragment = new FragmentAccountViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.work_orders));
        } else if (str.equals(getString(R.string.contact))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.contact));
            bundle.putString(Cons.KEY_AccountID, accountId);
            FragmentAccountViewAll fragment = new FragmentAccountViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.contacts));
        } else if (str.equals(getString(R.string.event))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.event));
            bundle.putString(Cons.KEY_AccountID, accountId);
            FragmentAccountViewAll fragment = new FragmentAccountViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.events));
        } else if (str.equals(getString(R.string.estimate))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.estimate));
            bundle.putString(Cons.KEY_AccountID, accountId);
            FragmentAccountViewAll fragment = new FragmentAccountViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.estimates));
        } else if (str.equals(getString(R.string.invoice))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.invoice));
            bundle.putString(Cons.KEY_AccountID, accountId);
            FragmentAccountViewAll fragment = new FragmentAccountViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.invoices));
        } else if (str.equals(getString(R.string.file))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.file));
            bundle.putString(Cons.KEY_AccountID, accountId);
            FragmentAccountViewAll fragment = new FragmentAccountViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.files));
        } else if (str.equals(getString(R.string.location))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.location));
            bundle.putString(Cons.KEY_AccountID, accountId);
            FragmentAccountViewAll fragment = new FragmentAccountViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.locations));
        } else if (str.equals(getString(R.string.task))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.task));
            bundle.putString(Cons.KEY_AccountID, accountId);
            FragmentAccountViewAll fragment = new FragmentAccountViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.tasks));
        } else if (str.equals(getString(R.string.chemical))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.chemical));
            bundle.putString(Cons.KEY_AccountID, accountId);
            FragmentAccountViewAll fragment = new FragmentAccountViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.chemicals));
        } else if (str.equals(getString(R.string.product))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.product));
            bundle.putString(Cons.KEY_AccountID, accountId);
            FragmentAccountViewAll fragment = new FragmentAccountViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.products));
        }
    }

    @Override
    public void callbackContactViewAllListen(String str, String contactId) {
        if (str.equals(getString(R.string.work_order))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.work_order));
            bundle.putString(Cons.KEY_ContactID, contactId);
            FragmentContactViewAllKT fragment = new FragmentContactViewAllKT();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.work_orders));
        } else if (str.equals(getString(R.string.event))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.event));
            bundle.putString(Cons.KEY_ContactID, contactId);
            FragmentContactViewAllKT fragment = new FragmentContactViewAllKT();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.events));
        } else if (str.equals(getString(R.string.estimate))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.estimate));
            bundle.putString(Cons.KEY_ContactID, contactId);
            FragmentContactViewAllKT fragment = new FragmentContactViewAllKT();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.estimates));
        } else if (str.equals(getString(R.string.invoice))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.invoice));
            bundle.putString(Cons.KEY_ContactID, contactId);
            FragmentContactViewAllKT fragment = new FragmentContactViewAllKT();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.invoices));
        } else if (str.equals(getString(R.string.file))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.file));
            bundle.putString(Cons.KEY_ContactID, contactId);
            FragmentContactViewAllKT fragment = new FragmentContactViewAllKT();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.files));
        } else if (str.equals(getString(R.string.note))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.note));
            bundle.putString(Cons.KEY_ContactID, contactId);
            FragmentContactViewAllKT fragment = new FragmentContactViewAllKT();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.notes));
        } else if (str.equals(getString(R.string.task))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.task));
            bundle.putString(Cons.KEY_ContactID, contactId);
            FragmentContactViewAllKT fragment = new FragmentContactViewAllKT();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.tasks));
        }
    }

    @Override
    public void callbackWorkOrderViewAllListen(String str, String workOrderId) {
        if (str.equals(getString(R.string.line_item))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.line_item));
            bundle.putString(Cons.KEY_WorkOrderID, workOrderId);
            FragmentWorkOrderViewAll fragment = new FragmentWorkOrderViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.line_items));
        } else if (str.equals(getString(R.string.chemical))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.chemical));
            bundle.putString(Cons.KEY_WorkOrderID, workOrderId);
            FragmentWorkOrderViewAll fragment = new FragmentWorkOrderViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.chemicals));
        } else if (str.equals(getString(R.string.event))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.event));
            bundle.putString(Cons.KEY_WorkOrderID, workOrderId);
            FragmentWorkOrderViewAll fragment = new FragmentWorkOrderViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.events));
        } else if (str.equals(getString(R.string.file))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.file));
            bundle.putString(Cons.KEY_WorkOrderID, workOrderId);
            FragmentWorkOrderViewAll fragment = new FragmentWorkOrderViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.files));
        } else if (str.equals(getString(R.string.task))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.task));
            bundle.putString(Cons.KEY_WorkOrderID, workOrderId);
            FragmentWorkOrderViewAll fragment = new FragmentWorkOrderViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.tasks));
        } else if (str.equals(getString(R.string.invoice))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.invoice));
            bundle.putString(Cons.KEY_WorkOrderID, workOrderId);
            FragmentWorkOrderViewAll fragment = new FragmentWorkOrderViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.invoices));
        } else if (str.equals(getString(R.string.note))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.note));
            bundle.putString(Cons.KEY_WorkOrderID, workOrderId);
            FragmentWorkOrderViewAll fragment = new FragmentWorkOrderViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.notes));
        }
    }

    @Override
    public void callbackEstimateViewAllListen(String str, String estimateId) {
        if (str.equals(getString(R.string.line_item))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.line_item));
            bundle.putString(Cons.KEY_EstimateID, estimateId);
            FragmentEstimateViewAll fragment = new FragmentEstimateViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.line_items));
        } else if (str.equals(getString(R.string.event))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.event));
            bundle.putString(Cons.KEY_EstimateID, estimateId);
            FragmentEstimateViewAll fragment = new FragmentEstimateViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.events));
        } else if (str.equals(getString(R.string.task))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.task));
            bundle.putString(Cons.KEY_EstimateID, estimateId);
            FragmentEstimateViewAll fragment = new FragmentEstimateViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.tasks));
        } else if (str.equals(getString(R.string.file))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.file));
            bundle.putString(Cons.KEY_EstimateID, estimateId);
            FragmentEstimateViewAll fragment = new FragmentEstimateViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.files));
        } else if (str.equals(getString(R.string.note))) {
            Bundle bundle = new Bundle();
            bundle.putString("view_all_type", getString(R.string.note));
            bundle.putString(Cons.KEY_EstimateID, estimateId);
            FragmentEstimateViewAll fragment = new FragmentEstimateViewAll();
            fragment.setArguments(bundle);
            setFragment(fragment, "All " + getString(R.string.notes));
        }
    }

    @Override
    public void callbackWorkOrderSignatureListener(String workOrderId, String workOrderNo) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_WorkOrderID, workOrderId);
        bundle.putString(Cons.KEY_WorkOrderNo, workOrderNo);
        FragmentWorkOrderSignature fragment = new FragmentWorkOrderSignature();
        fragment.setArguments(bundle);
        setFragment(fragment, workOrderNo);
        work_order_info_signature_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackEstimateSignatureListener(String estimateId, String estimateNo) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_EstimateID, estimateId);
        bundle.putString(Cons.KEY_EstimateNo, estimateNo);
        FragmentEstimateSignature fragment = new FragmentEstimateSignature();
        fragment.setArguments(bundle);
        setFragment(fragment, estimateNo);
        estimate_info_estimate_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackAddWorkOrderSignatureListener(String workOrderId, String workOrderNo) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_WorkOrderID, workOrderId);
        FragmentWorkOrderInfo fragment = new FragmentWorkOrderInfo();
        fragment.setArguments(bundle);
        setFragment(fragment, workOrderNo);
        work_order_info_work_order_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackAddEstimateSignatureListener(String estimateId, String estimateNo) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_EstimateID, estimateId);
        FragmentEstimateInfo fragment = new FragmentEstimateInfo();
        fragment.setArguments(bundle);
        setFragment(fragment, estimateNo);
        estimate_info_estimate_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackCalendarEvent(ModelCalendar model) {
        if (model.getRelatedTo().equals("WorkOrder")) {
            Bundle bundle = new Bundle();
            bundle.putString(Cons.KEY_WorkOrderID, model.getRelatedObjID());
            FragmentWorkOrderInfo fragment = new FragmentWorkOrderInfo();
            fragment.setArguments(bundle);
            setFragment(fragment, model.getRelatedObjNo());
            work_order_info_work_order_back = true;
            buttonRight.setVisibility(View.GONE);
        } else if (model.getRelatedTo().equals("Estimate")) {
            Bundle bundle = new Bundle();
            bundle.putString(Cons.KEY_EstimateID, model.getRelatedObjID());
            FragmentEstimateInfo fragment = new FragmentEstimateInfo();
            fragment.setArguments(bundle);
            setFragment(fragment, model.getRelatedObjNo());
            estimate_info_estimate_back = true;
            buttonRight.setVisibility(View.GONE);
        }
    }

    @Override
    public void callbackMapEvent(ModelMap model) {
        Bundle bundle = new Bundle();
        bundle.putString(Cons.KEY_WorkOrderID, model.getRelatedObjID());
        FragmentWorkOrderInfo fragment = new FragmentWorkOrderInfo();
        fragment.setArguments(bundle);
        setFragment(fragment, model.getRelatedObjNo());
        work_order_info_work_order_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        if (buttonRight.getVisibility() == View.GONE) {
            buttonRight.setVisibility(View.VISIBLE);
        }

        String strCenter = textviewCenter.getText().toString();

        if (navDrawer.isDrawerOpen(GravityCompat.START)) {
            navDrawer.closeDrawers();
        } else if (strCenter.equals(getString(R.string.home))) {
            finish();
        } else if (strCenter.equals(getString(R.string.accounts_recent))) {
            setHomFragment();
        } else if (strCenter.equals(getString(R.string.account_create))) {
            if (!account_create_home_back) {
                setFragment(new FragmentAccount(), getString(R.string.accounts_recent));
            } else {
                setHomFragment();
            }
        } else if (account_info_account_back) {
            setFragment(new FragmentAccount(), getString(R.string.accounts_recent));
            account_info_account_back = false;
            buttonRight.setVisibility(View.VISIBLE);
        } else if (strCenter.equals(getString(R.string.contacts_recent))) {
            setHomFragment();
        } else if (strCenter.equals(getString(R.string.contact_create))) {
            if (!contact_create_home_back) {
                setFragment(new FragmentContactKT(), getString(R.string.contacts_recent));
            } else {
                setHomFragment();
            }
        } else if (contact_info_contact_back) {
            setFragment(new FragmentContactKT(), getString(R.string.contacts_recent));
            contact_info_contact_back = false;
            buttonRight.setVisibility(View.VISIBLE);
        } else if (strCenter.equals(getString(R.string.work_orders_recent))) {
            setHomFragment();
        } else if (strCenter.equals(getString(R.string.work_order_create))) {
            if (!work_order_create_home_back) {
                setFragment(new FragmentWorkOrder(), getString(R.string.work_orders_recent));
            } else {
                setHomFragment();
            }
        } else if (work_order_info_work_order_back) {
            if (!isRecurringOn) {
                setFragment(new FragmentWorkOrder(), getString(R.string.work_orders_recent));
                work_order_info_work_order_back = false;
                buttonRight.setVisibility(View.VISIBLE);
            } else {
                buttonRight.setVisibility(View.GONE);
            }
        } else if (work_order_info_signature_back) {
            setFragment(new FragmentWorkOrder(), getString(R.string.work_orders_recent));
        } else if (strCenter.equals(getString(R.string.estimates_recent))) {
            setHomFragment();
        } else if (strCenter.equals(getString(R.string.estimate_create))) {
            if (!estimate_create_home_back) {
                setFragment(new FragmentEstimate(), getString(R.string.estimates_recent));
            } else {
                setHomFragment();
            }
        } else if (estimate_info_estimate_back) {
            if (!isRecurringOn) {
                setFragment(new FragmentEstimate(), getString(R.string.estimates_recent));
                estimate_info_estimate_back = false;
                buttonRight.setVisibility(View.VISIBLE);
            } else {
                buttonRight.setVisibility(View.GONE);
            }
        } else if (estimate_info_signature_back) {
            setFragment(new FragmentEstimate(), getString(R.string.estimates_recent));
        } else if (strCenter.equals(getString(R.string.invoices_recent))) {
            setHomFragment();
        } else if (strCenter.equals(getString(R.string.invoice_create))) {
            if (!invoice_create_home_back) {
                setFragment(new FragmentInvoice(), getString(R.string.invoices_recent));
            } else {
                setHomFragment();
            }
        } else if (invoice_info_invoice_back) {
            setFragment(new FragmentInvoice(), getString(R.string.invoices_recent));
            invoice_info_invoice_back = false;
            buttonRight.setVisibility(View.VISIBLE);
        } else if (strCenter.equals(getString(R.string.tasks_recent))) {
            setHomFragment();
        } else if (strCenter.equals(getString(R.string.task_create))) {
            if (!task_create_home_back) {
                setFragment(new FragmentTask(), getString(R.string.tasks_recent));
            } else {
                setHomFragment();
            }
        } else if (task_info_task_back) {
            setFragment(new FragmentTask(), getString(R.string.tasks_recent));
            task_info_task_back = false;
            buttonRight.setVisibility(View.VISIBLE);
        } else if (strCenter.equals(getString(R.string.files_recent))) {
            setHomFragment();
        } else if (strCenter.equals(getString(R.string.file_create))) {
            if (!file_create_home_back) {
                setFragment(new FragmentFile(), getString(R.string.files_recent));
            } else {
                setHomFragment();
            }
        } else if (file_info_file_back) {
            setFragment(new FragmentFile(), getString(R.string.files_recent));
            file_info_file_back = false;
            buttonRight.setVisibility(View.VISIBLE);
        } else if (strCenter.equals(getString(R.string.calendar))) {
            setHomFragment();
            buttonRight.setVisibility(View.VISIBLE);
            textviewCalenderDate.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void callbackAccountCreateFile(String strObject) {
        FragmentFileCreate fragment = new FragmentFileCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        bundle.putString("object", strObject);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.file_create));
        file_create_home_back = false;
    }

    @Override
    public void callbackWorkOrderCreateFile(String strObject) {
        FragmentFileCreate fragment = new FragmentFileCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        bundle.putString("object", strObject);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.file_create));
        file_create_home_back = false;
    }

    @Override
    public void callbackContactCreateFile(String strObject) {
        FragmentFileCreate fragment = new FragmentFileCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        bundle.putString("object", strObject);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.file_create));
        file_create_home_back = false;
    }

    @Override
    public void callbackEstimateCreateFile(String strObject) {
        FragmentFileCreate fragment = new FragmentFileCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        bundle.putString("object", strObject);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.file_create));
        file_create_home_back = false;
    }

    @Override
    public void callbackInvoiceCreateFile(String strObject) {
        FragmentFileCreate fragment = new FragmentFileCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        bundle.putString("object", strObject);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.file_create));
        file_create_home_back = false;
    }

    @Override
    public void callbackTaskCreateFile(String strObject) {
        FragmentFileCreate fragment = new FragmentFileCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        bundle.putString("object", strObject);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.file_create));
        file_create_home_back = false;
    }


    @Override
    public void callbackAccountCreateEvent(String strWhoName, String strWhatID, String strWhatName) {
        FragmentEventCreate fragment = new FragmentEventCreate();
        Bundle bundle = new Bundle();
        bundle.putString("whoName", strWhoName);
        bundle.putString("whatID", strWhatID);
        bundle.putString("whatName", strWhatName);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.event_create));
        event_create_home_back = false;
    }

    @Override
    public void callbackContactCreateEvent(String strWhoName, String strWhatID, String strWhatName) {
        FragmentEventCreate fragment = new FragmentEventCreate();
        Bundle bundle = new Bundle();
        bundle.putString("whoName", strWhoName);
        bundle.putString("whatID", strWhatID);
        bundle.putString("whatName", strWhatName);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.event_create));
        event_create_home_back = false;
    }

    @Override
    public void callbackWorkOrderCreateEvent(String strWhoName, String strWhatID, String strWhatName) {
        FragmentEventCreate fragment = new FragmentEventCreate();
        Bundle bundle = new Bundle();
        bundle.putString("whoName", strWhoName);
        bundle.putString("whatID", strWhatID);
        bundle.putString("whatName", strWhatName);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.event_create));
        event_create_home_back = false;
    }

    @Override
    public void callbackEstimateCreateEvent(String strWhoName, String strWhatID, String strWhatName) {
        FragmentEventCreate fragment = new FragmentEventCreate();
        Bundle bundle = new Bundle();
        bundle.putString("whoName", strWhoName);
        bundle.putString("whatID", strWhatID);
        bundle.putString("whatName", strWhatName);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.event_create));
        event_create_home_back = false;
    }

    @Override
    public void callbackInvoiceCreateEvent(String strWhoName, String strWhatID, String strWhatName) {
        FragmentEventCreate fragment = new FragmentEventCreate();
        Bundle bundle = new Bundle();
        bundle.putString("whoName", strWhoName);
        bundle.putString("whatID", strWhatID);
        bundle.putString("whatName", strWhatName);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.event_create));
        event_create_home_back = false;
    }

    @Override
    public void callbackGoToRecentAccount(String strObject) {
        setFragment(new FragmentAccount(), getString(R.string.accounts_recent));
    }

    @Override
    public void callbackGoToRecentWorkOrder(String strObject) {
        setFragment(new FragmentWorkOrder(), getString(R.string.work_orders_recent));
    }

    @Override
    public void callbackGoToRecentContact(String strObject) {
        setFragment(new FragmentContactKT(), getString(R.string.contacts_recent));
    }

    @Override
    public void callbackGoToRecentEstimate(String strObject) {
        setFragment(new FragmentEstimate(), getString(R.string.estimates_recent));
    }

    @Override
    public void callbackGoToRecentInvoice(String strObject) {
        setFragment(new FragmentInvoice(), getString(R.string.invoices_recent));
    }

    @Override
    public void callbackGoToRecentTask(String strObject) {
        setFragment(new FragmentTask(), getString(R.string.tasks_recent));
    }

    @Override
    public void callbackGoToRecentFile(String strObject) {
        setFragment(new FragmentFile(), getString(R.string.files_recent));
    }

    @Override
    public void callbackSendEmailClose(String relatedTo, String what, String header) {
        if (relatedTo.equals("WorkOrder")) {
            FragmentWorkOrderInfo fragment = new FragmentWorkOrderInfo();
            Bundle bundle = new Bundle();
            bundle.putString(Cons.KEY_WorkOrderID, what);
            fragment.setArguments(bundle);
            setFragment(fragment, header);
            work_order_info_work_order_back = true;
            buttonRight.setVisibility(View.GONE);
        } else if (relatedTo.equals("Contact")) {
            FragmentContactInfoKT fragment = new FragmentContactInfoKT();
            Bundle bundle = new Bundle();
            bundle.putString(Cons.KEY_ContactID, what);
            fragment.setArguments(bundle);
            setFragment(fragment, header);
            contact_info_contact_back = true;
            buttonRight.setVisibility(View.GONE);
        } else if (relatedTo.equals("Estimate")) {
            FragmentEstimateInfo fragment = new FragmentEstimateInfo();
            Bundle bundle = new Bundle();
            bundle.putString(Cons.KEY_EstimateID, what);
            fragment.setArguments(bundle);
            setFragment(fragment, header);
            estimate_info_estimate_back = true;
            buttonRight.setVisibility(View.GONE);
        } else if (relatedTo.equals("Invoice")) {
            FragmentInvoiceInfo fragment = new FragmentInvoiceInfo();
            Bundle bundle = new Bundle();
            bundle.putString(Cons.KEY_InvoiceID, what);
            fragment.setArguments(bundle);
            setFragment(fragment, header);
            invoice_info_invoice_back = true;
            buttonRight.setVisibility(View.GONE);
        } else {
            setHomFragment();
        }
    }

    @Override
    public void callbackWorkOrderSendEmail(String what, String header) {
        FragmentEmail fragment = new FragmentEmail();
        Bundle bundle = new Bundle();
        bundle.putString("related", "WorkOrder");
        bundle.putString("what", what);
        bundle.putString("header", header);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.send_email));
    }

    @Override
    public void callbackContactSendEmail(String what, String header) {
        FragmentEmail fragment = new FragmentEmail();
        Bundle bundle = new Bundle();
        bundle.putString("related", "Contact");
        bundle.putString("what", what);
        bundle.putString("header", header);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.send_email));
    }

    @Override
    public void callbackEstimateSendEmail(String what, String header) {
        FragmentEmail fragment = new FragmentEmail();
        Bundle bundle = new Bundle();
        bundle.putString("related", "Estimate");
        bundle.putString("what", what);
        bundle.putString("header", header);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.send_email));
    }

    @Override
    public void callbackInvoiceSendEmail(String what, String header) {
        FragmentEmail fragment = new FragmentEmail();
        Bundle bundle = new Bundle();
        bundle.putString("related", "Invoice");
        bundle.putString("what", what);
        bundle.putString("header", header);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.send_email));
    }

    @Override
    public void callbackAccountCreateView(String objectName) {
        FragmentCreateView fragment = new FragmentCreateView();
        Bundle bundle = new Bundle();
        bundle.putString("objectName", objectName);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.create_view));
        account_info_account_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackContactCreateView(String objectName) {
        FragmentCreateView fragment = new FragmentCreateView();
        Bundle bundle = new Bundle();
        bundle.putString("objectName", objectName);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.create_view));
        contact_info_contact_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackWorkOrderCreateView(String objectName) {
        FragmentCreateView fragment = new FragmentCreateView();
        Bundle bundle = new Bundle();
        bundle.putString("objectName", objectName);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.create_view));
        work_order_info_work_order_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackEstimateCreateView(String objectName) {
        FragmentCreateView fragment = new FragmentCreateView();
        Bundle bundle = new Bundle();
        bundle.putString("objectName", objectName);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.create_view));
        estimate_info_estimate_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackInvoiceCreateView(String objectName) {
        FragmentCreateView fragment = new FragmentCreateView();
        Bundle bundle = new Bundle();
        bundle.putString("objectName", objectName);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.create_view));
        invoice_info_invoice_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackFileCreateView(String objectName) {
        FragmentCreateView fragment = new FragmentCreateView();
        Bundle bundle = new Bundle();
        bundle.putString("objectName", objectName);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.create_view));
        file_info_file_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackTaskCreateView(String objectName) {
        FragmentCreateView fragment = new FragmentCreateView();
        Bundle bundle = new Bundle();
        bundle.putString("objectName", objectName);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.create_view));
        task_info_task_back = true;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackCreateViewCancel() {
        onBackPressed();
    }

    @Override
    public void callbackAccountCopyView() {
        setFragment(new FragmentAccount(), getString(R.string.accounts_recent));
    }

    @Override
    public void callbackAccountDeleteView() {
        setFragment(new FragmentAccount(), getString(R.string.accounts_recent));
    }

    @Override
    public void callbackContactCopyView() {
        setFragment(new FragmentContactKT(), getString(R.string.contacts_recent));
    }

    @Override
    public void callbackContactDeleteView() {
        setFragment(new FragmentContactKT(), getString(R.string.contacts_recent));
    }

    @Override
    public void callbackWorkOrderCopyView() {
        setFragment(new FragmentWorkOrder(), getString(R.string.work_orders_recent));
    }

    @Override
    public void callbackWorkOrderDeleteView() {
        setFragment(new FragmentWorkOrder(), getString(R.string.work_orders_recent));
    }

    @Override
    public void callbackEstimateCopyView() {
        setFragment(new FragmentEstimate(), getString(R.string.estimates_recent));
    }

    @Override
    public void callbackEstimateDeleteView() {
        setFragment(new FragmentEstimate(), getString(R.string.estimates_recent));
    }

    @Override
    public void callbackInvoiceCopyView() {
        setFragment(new FragmentInvoice(), getString(R.string.invoices_recent));
    }

    @Override
    public void callbackInvoiceDeleteView() {
        setFragment(new FragmentInvoice(), getString(R.string.invoices_recent));
    }

    @Override
    public void callbackFileCopyView() {
        setFragment(new FragmentFile(), getString(R.string.files_recent));
    }

    @Override
    public void callbackFileDeleteView() {
        setFragment(new FragmentFile(), getString(R.string.files_recent));
    }

    @Override
    public void callbackTaskCopyView() {
        setFragment(new FragmentTask(), getString(R.string.tasks_recent));
    }

    @Override
    public void callbackTaskDeleteView() {
        setFragment(new FragmentTask(), getString(R.string.tasks_recent));
    }

    @Override
    public void callbackAccountContactCreate() {
        FragmentContactCreateKT fragment = new FragmentContactCreateKT();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.contact_create));
        contact_create_home_back = false;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackAccountWorkOrderCreate() {
        FragmentWorkOrderCreate fragment = new FragmentWorkOrderCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.work_order_create));
        work_order_create_home_back = false;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackAccountEstimateCreate() {
        FragmentEstimateCreate fragment = new FragmentEstimateCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.estimate_create));
        estimate_create_home_back = false;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackAccountInvoiceCreate() {
        FragmentInvoiceCreate fragment = new FragmentInvoiceCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.invoice_create));
        invoice_create_home_back = false;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackAccountTaskCreate() {
        FragmentTaskCreate fragment = new FragmentTaskCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.task_create));
        task_create_home_back = false;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackContactWorkOrderCreate() {
        FragmentWorkOrderCreate fragment = new FragmentWorkOrderCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.work_order_create));
        work_order_create_home_back = false;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackContactEstimateCreate() {
        FragmentEstimateCreate fragment = new FragmentEstimateCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.estimate_create));
        estimate_create_home_back = false;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackContactInvoiceCreate() {
        FragmentInvoiceCreate fragment = new FragmentInvoiceCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.invoice_create));
        invoice_create_home_back = false;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackContactTaskCreate() {
        FragmentTaskCreate fragment = new FragmentTaskCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.task_create));
        task_create_home_back = false;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackWorkOrderInvoiceCreate() {
        FragmentInvoiceCreate fragment = new FragmentInvoiceCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.invoice_create));
        invoice_create_home_back = false;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackWorkOrderTaskCreate() {
        FragmentTaskCreate fragment = new FragmentTaskCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.task_create));
        task_create_home_back = false;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackEstimateInvoiceCreate() {
        FragmentInvoiceCreate fragment = new FragmentInvoiceCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.invoice_create));
        invoice_create_home_back = false;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackEstimateTaskCreate() {
        FragmentTaskCreate fragment = new FragmentTaskCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.task_create));
        task_create_home_back = false;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackInvoiceTaskCreate() {
        FragmentTaskCreate fragment = new FragmentTaskCreate();
        Bundle bundle = new Bundle();
        bundle.putInt("back_type", 1);
        fragment.setArguments(bundle);
        setFragment(fragment, getString(R.string.task_create));
        task_create_home_back = false;
        buttonRight.setVisibility(View.GONE);
    }

    @Override
    public void callbackHomeItem(String object, String id, String name) {
        if (object.equalsIgnoreCase(getString(R.string.account))) {
            Bundle bundle = new Bundle();
            bundle.putString(Cons.KEY_AccountID, id);
            FragmentAccountInfo fragment = new FragmentAccountInfo();
            fragment.setArguments(bundle);
            setFragment(fragment, name);
            account_info_account_back = true;
            buttonRight.setVisibility(View.GONE);
        } else if (object.equalsIgnoreCase(getString(R.string.contact))) {
            Bundle bundle = new Bundle();
            bundle.putString(Cons.KEY_ContactID, id);
            FragmentContactInfoKT fragment = new FragmentContactInfoKT();
            fragment.setArguments(bundle);
            setFragment(fragment, name);
            contact_info_contact_back = true;
            buttonRight.setVisibility(View.GONE);
        } else if (object.equalsIgnoreCase(getString(R.string.workorder))) {
            Bundle bundle = new Bundle();
            bundle.putString(Cons.KEY_WorkOrderID, id);
            FragmentWorkOrderInfo fragment = new FragmentWorkOrderInfo();
            fragment.setArguments(bundle);
            setFragment(fragment, name);
            work_order_info_work_order_back = true;
            buttonRight.setVisibility(View.GONE);
        } else if (object.equalsIgnoreCase(getString(R.string.estimate))) {
            Bundle bundle = new Bundle();
            bundle.putString(Cons.KEY_EstimateID, id);
            FragmentEstimateInfo fragment = new FragmentEstimateInfo();
            fragment.setArguments(bundle);
            setFragment(fragment, name);
            estimate_info_estimate_back = true;
            buttonRight.setVisibility(View.GONE);
        } else if (object.equalsIgnoreCase(getString(R.string.invoice))) {
            Bundle bundle = new Bundle();
            bundle.putString(Cons.KEY_InvoiceID, id);
            FragmentInvoiceInfo fragment = new FragmentInvoiceInfo();
            fragment.setArguments(bundle);
            setFragment(fragment, name);
            invoice_info_invoice_back = true;
            buttonRight.setVisibility(View.GONE);
        } else if (object.equalsIgnoreCase(getString(R.string.file))) {
            Bundle bundle = new Bundle();
            bundle.putString(Cons.KEY_TaskID, id);
            FragmentTaskInfo fragment = new FragmentTaskInfo();
            fragment.setArguments(bundle);
            setFragment(fragment, name);
            task_info_task_back = true;
            buttonRight.setVisibility(View.GONE);
        } else if (object.equalsIgnoreCase(getString(R.string.task))) {
            Bundle bundle = new Bundle();
            bundle.putString(Cons.KEY_FileID, id);
            FragmentFileInfo fragment = new FragmentFileInfo();
            fragment.setArguments(bundle);
            setFragment(fragment, name);
            file_info_file_back = true;
            buttonRight.setVisibility(View.GONE);
        }
    }

}