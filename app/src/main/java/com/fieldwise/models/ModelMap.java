package com.fieldwise.models;

public class ModelMap {

    private String WorkOrderID;
    private String Title;
    private String Time;
    private String StartDate;
    private String EndDate;
    private String StartTime;
    private String EndTime;
    private String Latitude;
    private String Longitude;
    private String ShapeType;
    private String ColorCode;
    private String RelatedObjID;
    private String RelatedObjNo;

    public ModelMap() {
    }

    public String getWorkOrderID() {
        return WorkOrderID;
    }

    public void setWorkOrderID(String workOrderID) {
        WorkOrderID = workOrderID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getShapeType() {
        return ShapeType;
    }

    public void setShapeType(String shapeType) {
        ShapeType = shapeType;
    }

    public String getColorCode() {
        return ColorCode;
    }

    public void setColorCode(String colorCode) {
        ColorCode = colorCode;
    }

    public String getRelatedObjID() {
        return RelatedObjID;
    }

    public void setRelatedObjID(String relatedObjID) {
        RelatedObjID = relatedObjID;
    }

    public String getRelatedObjNo() {
        return RelatedObjNo;
    }

    public void setRelatedObjNo(String relatedObjNo) {
        RelatedObjNo = relatedObjNo;
    }

}