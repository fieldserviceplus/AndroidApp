package com.fieldwise.models;

public class ModelWorkOrderChemical {

    private String ApplicationAmount;
    private String ApplicationUnitOfMeasure;
    private String ChemicalID;
    private String ChemicalNo;
    private String ProductID;
    private String TestedUnitOfMeasure;
    private String TestConcentration;
    private String ProductName;

    public ModelWorkOrderChemical() {
    }

    public String getApplicationAmount() {
        return ApplicationAmount;
    }

    public void setApplicationAmount(String applicationAmount) {
        ApplicationAmount = applicationAmount;
    }

    public String getApplicationUnitOfMeasure() {
        return ApplicationUnitOfMeasure;
    }

    public void setApplicationUnitOfMeasure(String applicationUnitOfMeasure) {
        ApplicationUnitOfMeasure = applicationUnitOfMeasure;
    }

    public String getChemicalID() {
        return ChemicalID;
    }

    public void setChemicalID(String chemicalID) {
        ChemicalID = chemicalID;
    }

    public String getChemicalNo() {
        return ChemicalNo;
    }

    public void setChemicalNo(String chemicalNo) {
        ChemicalNo = chemicalNo;
    }

    public String getTestConcentration() {
        return TestConcentration;
    }

    public void setTestConcentration(String testConcentration) {
        TestConcentration = testConcentration;
    }

    public String getTestedUnitOfMeasure() {
        return TestedUnitOfMeasure;
    }

    public void setTestedUnitOfMeasure(String testedUnitOfMeasure) {
        TestedUnitOfMeasure = testedUnitOfMeasure;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getProductID() {
        return ProductID;
    }

    public void setProductID(String productID) {
        ProductID = productID;
    }

}