package com.fieldwise.models;

public class ModelHomeTask {

    private String Date;
    private String RelatedObjID;
    private String RelatedObjNo;
    private String RelatedTo;
    private String Subject;
    private String TaskID;

    public ModelHomeTask() {
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getRelatedObjID() {
        return RelatedObjID;
    }

    public void setRelatedObjID(String relatedObjID) {
        RelatedObjID = relatedObjID;
    }

    public String getRelatedObjNo() {
        return RelatedObjNo;
    }

    public void setRelatedObjNo(String relatedObjNo) {
        RelatedObjNo = relatedObjNo;
    }

    public String getRelatedTo() {
        return RelatedTo;
    }

    public void setRelatedTo(String relatedTo) {
        RelatedTo = relatedTo;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getTaskID() {
        return TaskID;
    }

    public void setTaskID(String taskID) {
        TaskID = taskID;
    }

}