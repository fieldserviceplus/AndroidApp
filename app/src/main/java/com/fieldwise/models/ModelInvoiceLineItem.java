package com.fieldwise.models;

public class ModelInvoiceLineItem {

    private String AssignedTo;
    private String CreatedBy;
    private String CreatedDate;
    private String Description;
    private String Discount;
    private String Invoice;
    private String InvoiceLineItemID;
    private String InvoiceLineNo;
    private String IsDeleted;
    private String IsListPriceEditable;
    private String IsQuantityEditable;
    private String LastModifiedBy;
    private String LastModifiedDate;
    private String ListPrice;
    private String OrganizationID;
    private String Product;
    private String ProductName;
    private String Quantity;
    private String SubTotal;
    private String Tax;
    private String Taxable;
    private String TotalPrice;
    private String UnitPrice;

    public ModelInvoiceLineItem() {
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }

    public String getIsDeleted() {
        return IsDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        IsDeleted = isDeleted;
    }

    public String getIsListPriceEditable() {
        return IsListPriceEditable;
    }

    public void setIsListPriceEditable(String isListPriceEditable) {
        IsListPriceEditable = isListPriceEditable;
    }

    public String getIsQuantityEditable() {
        return IsQuantityEditable;
    }

    public void setIsQuantityEditable(String isQuantityEditable) {
        IsQuantityEditable = isQuantityEditable;
    }

    public String getLastModifiedBy() {
        return LastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        LastModifiedBy = lastModifiedBy;
    }

    public String getLastModifiedDate() {
        return LastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        LastModifiedDate = lastModifiedDate;
    }

    public String getListPrice() {
        return ListPrice;
    }

    public void setListPrice(String listPrice) {
        ListPrice = listPrice;
    }

    public String getOrganizationID() {
        return OrganizationID;
    }

    public void setOrganizationID(String organizationID) {
        OrganizationID = organizationID;
    }

    public String getProduct() {
        return Product;
    }

    public void setProduct(String product) {
        Product = product;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(String subTotal) {
        SubTotal = subTotal;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public String getTaxable() {
        return Taxable;
    }

    public void setTaxable(String taxable) {
        Taxable = taxable;
    }

    public String getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        TotalPrice = totalPrice;
    }

    public String getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        UnitPrice = unitPrice;
    }

    public String getAssignedTo() {
        return AssignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        AssignedTo = assignedTo;
    }

    public String getInvoice() {
        return Invoice;
    }

    public void setInvoice(String invoice) {
        Invoice = invoice;
    }

    public String getInvoiceLineItemID() {
        return InvoiceLineItemID;
    }

    public void setInvoiceLineItemID(String invoiceLineItemID) {
        InvoiceLineItemID = invoiceLineItemID;
    }

    public String getInvoiceLineNo() {
        return InvoiceLineNo;
    }

    public void setInvoiceLineNo(String invoiceLineNo) {
        InvoiceLineNo = invoiceLineNo;
    }

}