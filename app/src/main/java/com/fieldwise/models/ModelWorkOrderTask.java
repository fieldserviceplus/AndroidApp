package com.fieldwise.models;

public class ModelWorkOrderTask {

    private String CallDisposition;
    private String Priority;
    private String Subject;
    private String TaskID;
    private String TaskStatus;
    private String TaskType;

    public ModelWorkOrderTask() {
    }

    public String getCallDisposition() {
        return CallDisposition;
    }

    public void setCallDisposition(String callDisposition) {
        CallDisposition = callDisposition;
    }

    public String getPriority() {
        return Priority;
    }

    public void setPriority(String priority) {
        Priority = priority;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getTaskID() {
        return TaskID;
    }

    public void setTaskID(String taskID) {
        TaskID = taskID;
    }

    public String getTaskStatus() {
        return TaskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        TaskStatus = taskStatus;
    }

    public String getTaskType() {
        return TaskType;
    }

    public void setTaskType(String taskType) {
        TaskType = taskType;
    }

}