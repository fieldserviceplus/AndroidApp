package com.fieldwise.models;

public class ModelCalendar {

    private String ID;
    private String Title;
    private String Type;
    private String Time;
    private String StartDate;
    private String EndDate;
    private String StartTime;
    private String EndTime;
    private String ColorCode;
    private String RelatedTo;
    private String RelatedObjID;
    private String RelatedObjNo;

    public ModelCalendar() {
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getColorCode() {
        return ColorCode;
    }

    public void setColorCode(String colorCode) {
        ColorCode = colorCode;
    }

    public String getRelatedTo() {
        return RelatedTo;
    }

    public void setRelatedTo(String relatedTo) {
        RelatedTo = relatedTo;
    }

    public String getRelatedObjID() {
        return RelatedObjID;
    }

    public void setRelatedObjID(String relatedObjID) {
        RelatedObjID = relatedObjID;
    }

    public String getRelatedObjNo() {
        return RelatedObjNo;
    }

    public void setRelatedObjNo(String relatedObjNo) {
        RelatedObjNo = relatedObjNo;
    }

}