package com.fieldwise.models;

public class ModelRecentWorkOrdersDetailed {

    private String WorkOrderID;
    private String WorkOrderNo;
    private String Subject;
    private String Priority;
    private String Status;

    public ModelRecentWorkOrdersDetailed() {
    }

    public String getWorkOrderID() {
        return WorkOrderID;
    }

    public void setWorkOrderID(String workOrderID) {
        WorkOrderID = workOrderID;
    }

    public String getWorkOrderNo() {
        return WorkOrderNo;
    }

    public void setWorkOrderNo(String workOrderNo) {
        WorkOrderNo = workOrderNo;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getPriority() {
        return Priority;
    }

    public void setPriority(String priority) {
        Priority = priority;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}