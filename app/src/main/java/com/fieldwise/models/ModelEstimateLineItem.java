package com.fieldwise.models;

public class ModelEstimateLineItem {

    private String CreatedBy;
    private String CreatedDate;
    private String Description;
    private String Discount;
    private String Estimate;
    private String EstimateLineID;
    private String EstimateLineNo;
    private String IsDeleted;
    private String IsListPriceEditable;
    private String IsQuantityEditable;
    private String LastModifiedBy;
    private String LastModifiedDate;
    private String LineNumber;
    private String ListPrice;
    private String OrganizationID;
    private String Product;
    private String ProductName;
    private String Quantity;
    private String ShippingHandling;
    private String SubTotal;
    private String Tax;
    private String Taxable;
    private String TotalPrice;
    private String UnitPrice;

    public ModelEstimateLineItem() {
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }

    public String getEstimate() {
        return Estimate;
    }

    public void setEstimate(String estimate) {
        Estimate = estimate;
    }

    public String getEstimateLineID() {
        return EstimateLineID;
    }

    public void setEstimateLineID(String estimateLineID) {
        EstimateLineID = estimateLineID;
    }

    public String getEstimateLineNo() {
        return EstimateLineNo;
    }

    public void setEstimateLineNo(String estimateLineNo) {
        EstimateLineNo = estimateLineNo;
    }

    public String getIsDeleted() {
        return IsDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        IsDeleted = isDeleted;
    }

    public String getIsListPriceEditable() {
        return IsListPriceEditable;
    }

    public void setIsListPriceEditable(String isListPriceEditable) {
        IsListPriceEditable = isListPriceEditable;
    }

    public String getIsQuantityEditable() {
        return IsQuantityEditable;
    }

    public void setIsQuantityEditable(String isQuantityEditable) {
        IsQuantityEditable = isQuantityEditable;
    }

    public String getLastModifiedBy() {
        return LastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        LastModifiedBy = lastModifiedBy;
    }

    public String getLastModifiedDate() {
        return LastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        LastModifiedDate = lastModifiedDate;
    }

    public String getLineNumber() {
        return LineNumber;
    }

    public void setLineNumber(String lineNumber) {
        LineNumber = lineNumber;
    }

    public String getListPrice() {
        return ListPrice;
    }

    public void setListPrice(String listPrice) {
        ListPrice = listPrice;
    }

    public String getOrganizationID() {
        return OrganizationID;
    }

    public void setOrganizationID(String organizationID) {
        OrganizationID = organizationID;
    }

    public String getProduct() {
        return Product;
    }

    public void setProduct(String product) {
        Product = product;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getShippingHandling() {
        return ShippingHandling;
    }

    public void setShippingHandling(String shippingHandling) {
        ShippingHandling = shippingHandling;
    }

    public String getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(String subTotal) {
        SubTotal = subTotal;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public String getTaxable() {
        return Taxable;
    }

    public void setTaxable(String taxable) {
        Taxable = taxable;
    }

    public String getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        TotalPrice = totalPrice;
    }

    public String getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        UnitPrice = unitPrice;
    }

}