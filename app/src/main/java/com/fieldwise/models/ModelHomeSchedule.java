package com.fieldwise.models;

public class ModelHomeSchedule {

    private String Address;
    private String EventID;
    private String RelatedObjID;
    private String RelatedObjNo;
    private String RelatedTo;
    private String Subject;
    private String Time;

    public ModelHomeSchedule() {
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getEventID() {
        return EventID;
    }

    public void setEventID(String eventID) {
        EventID = eventID;
    }

    public String getRelatedObjID() {
        return RelatedObjID;
    }

    public void setRelatedObjID(String relatedObjID) {
        RelatedObjID = relatedObjID;
    }

    public String getRelatedObjNo() {
        return RelatedObjNo;
    }

    public void setRelatedObjNo(String relatedObjNo) {
        RelatedObjNo = relatedObjNo;
    }

    public String getRelatedTo() {
        return RelatedTo;
    }

    public void setRelatedTo(String relatedTo) {
        RelatedTo = relatedTo;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

}