package com.fieldwise.models;

public class ModelChemical {

    private String Account;
    private String AdditionalNotes;
    private String ApplicationArea;
    private String ApplicationAmount;
    private String ApplicationUnitOfMeasure;
    private String ChemicalNo;
    private String CreatedBy;
    private String CreatedDate;
    private String DefaultQuantity;
    private String Description;
    private String ProductCode;
    private String ProductID;
    private String ProductName;
    private String ListPrice;
    private String Taxable;
    private String TestConcentration;
    private String TestedUnitOfMeasure;
    private String WorkOrder;

    public ModelChemical() {
    }

    public String getAccount() {
        return Account;
    }

    public void setAccount(String account) {
        Account = account;
    }

    public String getAdditionalNotes() {
        return AdditionalNotes;
    }

    public void setAdditionalNotes(String additionalNotes) {
        AdditionalNotes = additionalNotes;
    }

    public String getApplicationArea() {
        return ApplicationArea;
    }

    public void setApplicationArea(String applicationArea) {
        ApplicationArea = applicationArea;
    }

    public String getApplicationAmount() {
        return ApplicationAmount;
    }

    public void setApplicationAmount(String applicationAmount) {
        ApplicationAmount = applicationAmount;
    }

    public String getApplicationUnitOfMeasure() {
        return ApplicationUnitOfMeasure;
    }

    public void setApplicationUnitOfMeasure(String applicationUnitOfMeasure) {
        ApplicationUnitOfMeasure = applicationUnitOfMeasure;
    }

    public String getChemicalNo() {
        return ChemicalNo;
    }

    public void setChemicalNo(String chemicalNo) {
        ChemicalNo = chemicalNo;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getDefaultQuantity() {
        return DefaultQuantity;
    }

    public void setDefaultQuantity(String defaultQuantity) {
        DefaultQuantity = defaultQuantity;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    public String getProductID() {
        return ProductID;
    }

    public void setProductID(String productID) {
        ProductID = productID;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getListPrice() {
        return ListPrice;
    }

    public void setListPrice(String listPrice) {
        ListPrice = listPrice;
    }

    public String getTaxable() {
        return Taxable;
    }

    public void setTaxable(String taxable) {
        Taxable = taxable;
    }

    public String getTestConcentration() {
        return TestConcentration;
    }

    public void setTestConcentration(String testConcentration) {
        TestConcentration = testConcentration;
    }

    public String getTestedUnitOfMeasure() {
        return TestedUnitOfMeasure;
    }

    public void setTestedUnitOfMeasure(String testedUnitOfMeasure) {
        TestedUnitOfMeasure = testedUnitOfMeasure;
    }

    public String getWorkOrder() {
        return WorkOrder;
    }

    public void setWorkOrder(String workOrder) {
        WorkOrder = workOrder;
    }

}