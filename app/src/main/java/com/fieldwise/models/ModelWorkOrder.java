package com.fieldwise.models;

public class ModelWorkOrder {

    private String CategoryName;
    private String Priority;
    private String Status;
    private String Subject;
    private String WorkOrderID;
    private String WorkOrderNo;

    public ModelWorkOrder() {
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getPriority() {
        return Priority;
    }

    public void setPriority(String priority) {
        Priority = priority;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getWorkOrderID() {
        return WorkOrderID;
    }

    public void setWorkOrderID(String workOrderID) {
        WorkOrderID = workOrderID;
    }

    public String getWorkOrderNo() {
        return WorkOrderNo;
    }

    public void setWorkOrderNo(String workOrderNo) {
        WorkOrderNo = workOrderNo;
    }

}