package com.fieldwise.models;

public class ModelProduct {

    private String CreatedBy;
    private String CreatedDate;
    private String DatePurchased;
    private String DefaultQuantity;
    private String Description;
    private String Discount;
    private String IsListPriceEditable;
    private String IsQuantityEditable;
    private String ListPrice;
    private String NetTotal;
    private String ProductCode;
    private String ProductID;
    private String ProductName;
    private String SubTotal;
    private String Tax;
    private String Taxable;
    private String UnitPrice;
    private boolean IsChecked;

    public ModelProduct() {
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getDatePurchased() {
        return DatePurchased;
    }

    public void setDatePurchased(String datePurchased) {
        DatePurchased = datePurchased;
    }

    public String getDefaultQuantity() {
        return DefaultQuantity;
    }

    public void setDefaultQuantity(String defaultQuantity) {
        DefaultQuantity = defaultQuantity;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }

    public String getIsListPriceEditable() {
        return IsListPriceEditable;
    }

    public void setIsListPriceEditable(String isListPriceEditable) {
        IsListPriceEditable = isListPriceEditable;
    }

    public String getIsQuantityEditable() {
        return IsQuantityEditable;
    }

    public void setIsQuantityEditable(String isQuantityEditable) {
        IsQuantityEditable = isQuantityEditable;
    }

    public String getListPrice() {
        return ListPrice;
    }

    public void setListPrice(String listPrice) {
        ListPrice = listPrice;
    }

    public String getNetTotal() {
        return NetTotal;
    }

    public void setNetTotal(String netTotal) {
        NetTotal = netTotal;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    public String getProductName() {
        return ProductName;
    }

    public String getProductID() {
        return ProductID;
    }

    public void setProductID(String productID) {
        ProductID = productID;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(String subTotal) {
        SubTotal = subTotal;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public String getTaxable() {
        return Taxable;
    }

    public void setTaxable(String taxable) {
        Taxable = taxable;
    }

    public String getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        UnitPrice = unitPrice;
    }

    public boolean isChecked() {
        return IsChecked;
    }

    public void setChecked(boolean checked) {
        IsChecked = checked;
    }

}