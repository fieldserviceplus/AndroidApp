package com.fieldwise.models;

public class ModelRecentItem {

    private int id;
    private boolean style;
    private String srid;
    private String sTime;
    private String eTime;
    private String type;
    private String location;
    private String subject;
    private String details;
    private String date;

    public ModelRecentItem() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean getStyle() {
        return style;
    }

    public void setStyle(boolean style) {
        this.style = style;
    }

    public String getSrId() {
        return srid;
    }

    public void setSrId(String srid) {
        this.srid = srid;
    }

    public String getSTime() {
        return sTime;
    }

    public void setSTime(String sTime) {
        this.sTime = sTime;
    }

    public String getETime() {
        return eTime;
    }

    public void setETime(String eTime) {
        this.eTime = eTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}