package com.fieldwise.models;

public class ModelWorkOrderInvoice {

    private String DueDate;
    private String InvoiceDate;
    private String InvoiceNumber;
    private String InvoiceStatus;
    private String SubTotal;
    private String TotalPrice;
    private String WorkOrderName;

    public ModelWorkOrderInvoice() {
    }

    public String getDueDate() {
        return DueDate;
    }

    public void setDueDate(String dueDate) {
        DueDate = dueDate;
    }

    public String getInvoiceDate() {
        return InvoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        InvoiceDate = invoiceDate;
    }

    public String getInvoiceNumber() {
        return InvoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        InvoiceNumber = invoiceNumber;
    }

    public String getInvoiceStatus() {
        return InvoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        InvoiceStatus = invoiceStatus;
    }

    public String getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(String subTotal) {
        SubTotal = subTotal;
    }

    public String getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        TotalPrice = totalPrice;
    }

    public String getWorkOrderName() {
        return WorkOrderName;
    }

    public void setWorkOrderName(String workOrderName) {
        WorkOrderName = workOrderName;
    }

}