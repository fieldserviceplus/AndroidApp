package com.fieldwise.models;

import java.util.Map;

public class ModelDynamic {

    private Map<String, String> map;

    public ModelDynamic() {
    }

    public Map<String, String> getMap() {
        return map;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }

}