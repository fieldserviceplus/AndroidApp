package com.fieldwise.models;

public class ModelWorkOrderLineItem {

    private String ProductID;
    private String LineItemNo;
    private String NetTotal;
    private String ProductName;
    private String Quantity;
    private String UnitPrice;

    public ModelWorkOrderLineItem() {
    }

    public String getLineItemNo() {
        return LineItemNo;
    }

    public void setLineItemNo(String lineItemNo) {
        LineItemNo = lineItemNo;
    }

    public String getNetTotal() {
        return NetTotal;
    }

    public void setNetTotal(String netTotal) {
        NetTotal = netTotal;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        UnitPrice = unitPrice;
    }

    public String getProductID() {
        return ProductID;
    }

    public void setProductID(String productID) {
        ProductID = productID;
    }

}