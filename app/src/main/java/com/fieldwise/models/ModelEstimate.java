package com.fieldwise.models;

public class ModelEstimate {

    private String CreatedDate;
    private String EstimateID;
    private String EstimateNo;
    private String EstimateName;
    private String ExpirationDate;
    private String GrandTotal;
    private String OwnerName;
    private String Status;

    public ModelEstimate() {
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getEstimateID() {
        return EstimateID;
    }

    public void setEstimateID(String estimateID) {
        EstimateID = estimateID;
    }

    public String getEstimateNo() {
        return EstimateNo;
    }

    public void setEstimateNo(String estimateNo) {
        EstimateNo = estimateNo;
    }

    public String getEstimateName() {
        return EstimateName;
    }

    public void setEstimateName(String estimateName) {
        EstimateName = estimateName;
    }

    public String getExpirationDate() {
        return ExpirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        ExpirationDate = expirationDate;
    }

    public String getGrandTotal() {
        return GrandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        GrandTotal = grandTotal;
    }

    public String getOwnerName() {
        return OwnerName;
    }

    public void setOwnerName(String ownerName) {
        OwnerName = ownerName;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}