package com.fieldwise.add;

import android.content.Context;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.fieldwise.R;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

import java.util.List;

@Layout(R.layout.item_short_view)
public class AdapterShortView {

    @View(R.id.text_sortby)
    TextView textSortBy;

    @View(R.id.button_sortby)
    ToggleButton buttonSortBy;

    public ClickListenShortView clickShortView;

    public interface ClickListenShortView {
        void callbackShortView(int pos, String objectName);
        void callbackShortViewToggle(int pos, String objectName, boolean isChecked);
    }

    private Context ctx;
    private int pos;
    private boolean checked;
    private List<Integer> sortPos;
    private List<String> sortBy;

    public AdapterShortView(Context ctx, ClickListenShortView clickShortView, int pos,
                            List<Integer> sortPos,
                            List<String> sortBy) {
        this.ctx = ctx;
        this.clickShortView = clickShortView;
        this.pos = pos;
        this.sortPos = sortPos;
        this.sortBy = sortBy;
    }

    @Resolve
    public void onResolved() {
        textSortBy.setText(sortBy.get(pos));
        if (sortPos.get(pos) == 1) {
            buttonSortBy.setVisibility(android.view.View.VISIBLE);
        } else if (sortPos.get(pos) == 2) {
            buttonSortBy.setVisibility(android.view.View.VISIBLE);
        } else {
            buttonSortBy.setVisibility(android.view.View.GONE);
        }
        buttonSortBy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                checked = isChecked;
                if (clickShortView != null) {
                    clickShortView.callbackShortViewToggle(pos, sortBy.get(pos), checked);
                }
            }
        });
    }

    @Click(R.id.text_sortby)
    public void onViewClick() {
        if (clickShortView != null) {
            clickShortView.callbackShortView(pos, sortBy.get(pos));
        }
    }

}